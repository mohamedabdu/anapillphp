<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitTaskAreaDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visit_task_area_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('visit_task_detail_id');
            $table->integer('category_id');
            $table->string('width');
            $table->string('height');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visit_task_area_details');
    }
}
