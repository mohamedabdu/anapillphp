<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReportCompetitorsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_competitors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type');
            $table->integer('competitor_id');
            $table->integer('product_id');
            $table->string('price_for_company');
            $table->string('price_for_competitor');
            $table->text('note');
            $table->string('number_of_promo');
            $table->text('promo_note');
            $table->string('promo_image');
            $table->string('category');
            $table->text('width_for_company');
            $table->text('width_for_competitor');
            $table->text('height_for_company');
            $table->text('height_for_competitor');
            $table->string('store');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_competitors');
    }
}
