<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoresTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_ar');
            $table->string('title_en');
            $table->text('description_ar');
            $table->text('description_en');
            $table->integer('category_id');
            $table->integer('country_id');
            $table->integer('region_id');
            $table->integer('city_id');
            $table->string('logo');
            $table->string('lat');
            $table->string('lng');
            $table->string('address_note');
            $table->integer('is_main');
            $table->integer('parent_id');
            $table->string('mobile');
            $table->string('email');
            $table->string('phone');
            $table->string('manager_name');
            $table->string('manager_email');
            $table->string('manager_mobile');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stores');
    }
}
