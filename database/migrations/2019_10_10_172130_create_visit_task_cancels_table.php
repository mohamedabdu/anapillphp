<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitTaskCancelsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visit_task_cancels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('visit_id');
            $table->integer('task_id');
            $table->integer('visit_detail_id');
            $table->string('note');
            $table->integer('is_agree');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visit_task_cancels');
    }
}
