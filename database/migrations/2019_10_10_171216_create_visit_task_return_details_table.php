<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitTaskReturnDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visit_task_return_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('visit_task_detail_id');
            $table->string('products_id');
            $table->string('bill_id');
            $table->string('number_piece');
            $table->string('number_of_carton');
            $table->integer('return_reason_id');
            $table->string('expire_end');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visit_task_return_details');
    }
}
