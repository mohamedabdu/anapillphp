<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitTaskInventoryDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visit_task_inventory_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('visit_task_detail_id');
            $table->integer('product_id');
            $table->string('price');
            $table->string('stock');
            $table->string('inventory');
            $table->string('num_of_stock');
            $table->string('num_of_inventory');
            $table->text('note');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visit_task_inventory_details');
    }
}
