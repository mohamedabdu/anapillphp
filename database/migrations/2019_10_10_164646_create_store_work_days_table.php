<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoreWorkDaysTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_work_days', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id');
            $table->string('day');
            $table->string('work_from');
            $table->string('work_to');
            $table->integer('is_custom');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('store_work_days');
    }
}
