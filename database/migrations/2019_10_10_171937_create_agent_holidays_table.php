<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgentHolidaysTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_holidays', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agent_id');
            $table->string('title');
            $table->text('reason');
            $table->string('start_date');
            $table->string('end_date');
            $table->integer('status');
            $table->string('refuse_note');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agent_holidays');
    }
}
