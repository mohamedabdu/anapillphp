<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitCancelsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visit_cancels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agent_id');
            $table->integer('visit_id');
            $table->integer('visit_store_id');
            $table->text('note');
            $table->integer('is_agree');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visit_cancels');
    }
}
