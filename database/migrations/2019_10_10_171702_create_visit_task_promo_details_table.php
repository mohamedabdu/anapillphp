<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitTaskPromoDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visit_task_promo_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('visit_task_detail_id');
            $table->integer('promo_id');
            $table->string('num_of_promo');
            $table->text('promo_image');
            $table->text('promo_note');
            $table->text('note');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visit_task_promo_details');
    }
}
