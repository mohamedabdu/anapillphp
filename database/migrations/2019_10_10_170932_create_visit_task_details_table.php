<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitTaskDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visit_task_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('visit_detail_id');
            $table->integer('task_id');
            $table->string('start_date');
            $table->string('end_date');
            $table->string('start_lat');
            $table->string('start_lng');
            $table->string('end_lat');
            $table->string('end_lng');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visit_task_details');
    }
}
