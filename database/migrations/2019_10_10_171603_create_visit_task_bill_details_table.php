<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitTaskBillDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visit_task_bill_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('visit_task_detail_id');
            $table->string('bill_id');
            $table->string('bill_date');
            $table->string('driver_name');
            $table->string('bill_image');
            $table->string('bill_total');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visit_task_bill_details');
    }
}
