

<div class="map-adds">

    <!-- lng Field -->
    <div class="form-group row  form-md-line-input col-md-12">
            <input class="form-control"  value="{{ isset($lng) ? $lng : ''  }}" type="text" id="lngbox" name="lng">
        <label for="address_note">خط الطول</label>
        <span class="help-block"></span>
    </div>
    <!-- lat Field -->
    <div class="form-group row  form-md-line-input col-md-12">
            <input class="form-control" value="{{ isset($lat) ? $lat : ''  }}" type="text" id="latbox" name="lat">
        <label for="address_note">خط العرض</label>
        <span class="help-block"></span>

    </div>


    <input id="pac-input" class="controls" type="text"
           placeholder="ابحث عن موقع للاعلان على الخريطة">
    <div id="map" style="height: 500px; width:100%;"></div>
    <div id="infowindow-content">
        <span id="place-name"  class="title"></span><br>
        Place ID <span id="place-id"></span><br>
        <span id="place-address"></span>
    </div>


</div>