{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQxy6KaWYNPIR0ymzvi5_gUIPNu7QP8VU&libraries=places&language=ar"></script>--}}

{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQxy6KaWYNPIR0ymzvi5_gUIPNu7QP8VU&libraries=places&callback=initMap"--}}
        {{--async defer></script>--}}

{{--<script type="text/javascript" src="{{url("public/admin")}}/map/map.js"></script>--}}


{{--
<script>
    // This example adds a search box to a map, using the Google Place Autocomplete
    // feature. People can enter geographical searches. The search box will return a
    // pick list containing a mix of places and predicted search terms.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    function initAutocomplete() {
        var markers = [];
        var lat = 0;
        var lng = 0;

        @if(isset($lat) && isset($lng))
            lat = {{$lat}};
        lng = {{$lng}};
        @endif

        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 30.0594699, lng: 31.1884238},
            zoom: 15,
            mapTypeId: 'roadmap'
        });


        var geocoder;
        geocoder = new google.maps.Geocoder();


        if (navigator.geolocation && lat === 0) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                map.setCenter(pos);
                var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                setValue(pos.lat,pos.lng);
                placeMarker(latlng);
                geocodeLatLng(geocoder, latlng);
            });
        } else {
            var pos = {
                lat: lat,
                lng: lng
            };

            map.setCenter(pos);
            var latlng = new google.maps.LatLng(lat, lng);
            setValue(lat,lng);
            placeMarker(latlng);

        }




        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });

        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
        });

        function placeMarker(location) {
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            markers.push(marker);
            setValue(location.lat(),location.lng());
            geocodeLatLng(geocoder, location);
        }

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {
                console.log(place.geometry.location.lat());
                setValue(place.geometry.location.lat(),place.geometry.location.lng());

                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
    }
    var setValue = function(lat,lng){
        document.getElementById('latbox').value = lat;
        document.getElementById('lngbox').value = lng;
    }




    function geocodeLatLng(geocoder, pos) {
        // var input = document.getElementById('latlng').value;
        // var latlngStr = input.split(',', 2);
        // var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
        geocoder.geocode({'location': pos}, function(results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    console.log(results[0]);
                    document.getElementById("mapAddress").value = results[0].formatted_address;
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }

</script>

{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQxy6KaWYNPIR0ymzvi5_gUIPNu7QP8VU&libraries=places&callback=initAutocomplete"
        async defer></script>--}}

