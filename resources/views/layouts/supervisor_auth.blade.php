<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>@yield('title') | Anabile</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Anabile Supervisor Login" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
                
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{url('public/backend/plugins')}}/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('public/backend/plugins')}}/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('public/backend/plugins')}}/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('public/backend/plugins')}}/bootstrap-switch/css/bootstrap-switch-rtl.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{url('public/backend/plugins')}}/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('public/backend/plugins')}}/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{url('public/backend/css')}}/components-md-rtl.css" rel="stylesheet" type="text/css" />
        <link href="{{url('public/backend/css')}}/plugins-md-rtl.css" rel="stylesheet" type="text/css" />

        {{--<link href="{{url('public/backend/css')}}/components-rtl.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('public/backend/css')}}/plugins-rtl.min.css" rel="stylesheet" type="text/css" /> --}}
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="{{url('public/backend/css')}}/login.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('public/backend/css')}}/custom-rtl.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="icon" href="{{url('public/backend/images')}}/favicon.ico" />
        <script>
            var config = {
                url: " {{ url('') }}",
                asset_url: " {{ url('public//') }}",
            };
            var lang = {
               
            };
        </script>

        <style>
            .login .content{
                width: 70%;
            }
            input.code{
                display: inline;
                width: 24%;
            }
            .login .logo{
                margin-top: 0px
            }
           
        </style>

        
    </head>
    <!-- END HEAD -->

    <body class="login" style="background-color: #ecf0f1 !important;">
            <div class="logo">
                <img src="{{url('public/backend/images/logo.png')}}" alt="" style="width: 150px; height:170px" />
            </div>
            <div class="content">
                <!-- BEGIN LOGIN FORM -->
                @yield('content')
                <!-- END LOGIN -->
            </div>

            <div class="copyright"> 2020 © Anabile </div>
           

        <!-- BEGIN CORE PLUGINS -->
        <script src="{{url('public/backend/plugins')}}/jquery.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/bootbox/bootbox.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{url('public/backend/plugins')}}/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{url('public/backend/scripts')}}/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
        <script src="{{url('public/backend/js')}}/my.js" type="text/javascript"></script>
        @yield('scripts')
        
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>