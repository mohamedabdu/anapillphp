<li class="nav-item {{ session('sidebar.dashboard') ? 'active':'' }}">
    <a href="{{url('')}}" class="nav-link nav-toggle">
        <i class="icon-screen-desktop"></i>
        <span class="title">{{_lang('app.home')}}</span>
    </a>
</li>

<li class="nav-item {{ isset($parent_tab) && $parent_tab == 'general_settings' ? 'active':'' }}">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="fa fa-cog"></i>
        <span class="title">{{_lang('app.general_settings')}}</span>
        <span class="arrow"></span>
    </a>

    <ul class="sub-menu" style="{{isset($parent_tab) && $parent_tab == 'general_settings' ? 'display:block;':'' }}">

        @if(Permissions::check("configs"))
        <li class="nav-item {{ session('sidebar.configs') ? 'active':'' }}" data-name="configs">
            <a href="{!! route('configs.edit',1) !!}" class="nav-link">
                <i class="fa fa-cog"></i>
                <span class="title">{{_lang('app.settings')}}</span>
            </a>
        </li>
        @endif

        @if(Permissions::check("notifications"))
        <li class="nav-item {{ session('sidebar.notifications') ? 'active':'' }}" >
            <a href="{!! route('notifications.create') !!}" class="nav-link">
                <i class="fa fa-bullhorn"></i>
                <span class="title">{{_lang('app.notifications')}}</span>
            </a>
        </li>

        @endif

        @if(Permissions::check("countries"))
        <li class="nav-item {{ isset($tab) && $tab == 'countries' ? 'active':'' }}">
            <a href="{{ route('locations.index') }}" class="nav-link">
                <i class="fa fa-globe"></i>
                <span class="title">{{_lang('app.countries')}}</span>
            </a>
        </li>
        @endif

        @if(Permissions::check("retrieval_reasons"))
        <li class="nav-item {{ isset($tab) && $tab == 'retrieval_reasons' ? 'active':'' }}" >
            <a href="{!! route('retrieval_reasons.index') !!}" class="nav-link">
                <i class="fas fa-exchange-alt"></i>
                <span class="title">{{_lang('app.retrieval_reasons')}}</span>
            </a>
        </li>
        @endif

        @if(Permissions::check("store_types"))
        <li class="nav-item {{ isset($tab) && $tab == 'store_types' ? 'active':'' }}">
            <a href="{!! route('store_types.index') !!}" class="nav-link">
                <i class="fas fa-store"></i><i class="fas fa-hand-holding-usd"></i>
                <span class="title">{{_lang('app.store_types')}}</span>
            </a>
        </li>
        @endif

        @if(Permissions::check("roles"))

        <li class="nav-item {{ isset($tab) && $tab == 'roles' ? 'active':'' }}" data-name="roles">
            <a href="{{ route('roles.index') }}" class="nav-link">
                <i class="fas fa-users-cog"></i>
                <span class="title">{{_lang('app.roles')}}</span>
            </a>
        </li>

        @endif

        @if(Permissions::check("company_roles"))

        <li class="nav-item {{ isset($tab) && $tab == 'company_roles' ? 'active':'' }}">
            <a href="{{ route('roles.index',['is_company' => 1]) }}" class="nav-link">
                <i class="fas fa-users-cog"></i>
                <span class="title">{{_lang('app.company_roles')}}</span>
            </a>
        </li>

        @endif

    </ul>
</li>


@if(Permissions::check("product_categories") || Permissions::check("store_categories"))

<li class="nav-item {{ isset($parent_tab) && $parent_tab == 'categories' ? 'active':'' }}">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="fa fa-list-alt"></i>
        <span class="title">{{_lang('app.manage_categories')}}</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu" style="{{ isset($parent_tab) && $parent_tab == 'categories' ? 'display:block;':'' }}">

        @if(Permissions::check("product_categories"))
        <li class="nav-item {{ isset($tab) && $tab == 'product_categories' ? 'active':'' }}" data-name="categories">
            <a href="{{ route('categories.index',['type' => 'product']) }}" class="nav-link">
                <i class="fas fa-boxes"></i>
                <span class="title">{{_lang('app.product_categories')}}</span>
            </a>
        </li>

        @endif

        @if(Permissions::check("store_categories"))
        <li class="nav-item {{ isset($tab) && $tab == 'store_categories' ? 'active':'' }}" data-name="subCategories">
            <a href="{{ route('categories.index',['type' => 'store']) }}" class="nav-link">
                <i class="fas fa-store"></i>
                <span class="title">{{_lang('app.store_categories')}}</span>
            </a>
        </li>
        @endif
    </ul>
</li>
@endif

@if(Permissions::check("admins"))
<li class="nav-item {{ isset($tab) && $tab == 'admins' ? 'active':'' }}">
    <a href="{{ route('admins.index') }}" class="nav-link nav-toggle">
        <i class="fa fa-users"></i>
        <span class="title">{{_lang('app.admins')}}</span>
    </a>
</li>
@endif

@if(Permissions::check("companies"))
<li class="nav-item {{isset($tab) && $tab == 'companies' ? 'active':'' }}" data-name="companies">
    <a href="{{ route('companies.index') }}" class="nav-link nav-toggle">
        <i class="fas fa-building"></i>
        <span class="title">{{_lang('app.companies')}}</span>
    </a>
</li>
@endif

@if(Permissions::check("competitors"))
<li class="nav-item {{ isset($tab) && $tab == 'competitors' ? 'active':'' }}">
    <a href="{{ route('competitors.index') }}" class="nav-link nav-toggle">
        <i class="fas fa-people-arrows"></i>
        <span class="title">{{_lang('app.competitors')}}</span>
    </a>
</li>
@endif

@if(Permissions::check("competitor_products"))
<li class="nav-item {{ isset($tab) && $tab == 'competitor_products' ? 'active':'' }}">
    <a href="{{ route('competitor_products.index') }}" class="nav-link nav-toggle">
        <i class="fas fa-people-arrows"></i><i class="fas fa-boxes"></i>
        <span class="title">{{_lang('app.comp-products')}}</span>
    </a>
</li>
@endif

@if(Permissions::check("products"))
<li class="nav-item {{ isset($tab) && $tab == 'products' ? 'active':'' }}">
    <a href="{{ route('products.index') }}" class="nav-link nav-toggle">
        <i class="fas fa-boxes"></i>
        <span class="title">{{_lang('app.products')}}</span>
    </a>
</li>
@endif

@if(Permissions::check("promotions"))
<li class="nav-item {{ isset($tab) && $tab == 'promotions' ? 'active':'' }}" data-name="promotions">
    <a href="{!! route('promotions.index') !!}" class="nav-link nav-toggle">
        <i class="fas fa-gifts"></i>
        <span class="title">{{_lang('app.promotions')}}</span>
    </a>
</li>
@endif

@if(Permissions::check("promoDetails"))
<li class="nav-item {{ session('sidebar.promoDetails') ? 'active':'' }}" data-name="promoDetails">
    <a href="{!! route('promoDetails.index') !!}" class="nav-link nav-toggle">
        <i class="fas fa-gifts"></i><i class="fas fa-store"></i>
        <span class="title">{{_lang('app.store_offers')}}</span>
    </a>
</li>

@endif

@if(Permissions::check("tasks"))

<li class="nav-item {{ session('sidebar.tasks') ? 'active':'' }}" data-name="tasks">
    <a href="{!! route('tasks.index') !!}" class="nav-link nav-toggle">
        <i class="fas fa-tasks"></i>
        <span class="title">{{_lang('app.tasks')}}</span>
    </a>
</li>

@endif
@if(Permissions::check("taskInstructions"))

<li class="nav-item {{ session('sidebar.taskInstructions') ? 'active':'' }}" data-name="taskInstructions">
    <a href="{!! route('taskInstructions.index') !!}" class="nav-link nav-toggle">
        <i class="icon-badge"></i>
        <span class="title">{{_lang('app.task_instructions')}}</span>
    </a>
</li>


@endif




@if(Permissions::check("stores"))

<li class="nav-item {{ session('sidebar.stores') ? 'active':'' }}" data-name="stores">
    <a href="{!! route('stores.index') !!}" class="nav-link nav-toggle">
        <i class="fas fa-store"></i>
        <span class="title">{{_lang('app.stores')}}</span>
    </a>
</li>

<li class="nav-item {{ session('sidebar.map') ? 'active':'' }}" data-name="map">
    <a href="{!! route('stores.map') !!}" class="nav-link nav-toggle">
        <i class="fas fa-map-marked-alt"></i>
        <span class="title">{{_lang('app.view_map')}}</span>
    </a>
</li>
@endif







@if(Permissions::check("productVsProducts"))

<li class="nav-item {{ session('sidebar.productVsProducts') ? 'active':'' }}" data-name="productVsProducts">
    <a href="{!! route('productVsProducts.index') !!}" class="nav-link nav-toggle">
        <i class="fas fa-boxes"></i>
        <span class="title">{{_lang('app.competing_products')}}</span>
    </a>
</li>


@endif


@if(Permissions::check("agents") || Permissions::check("supervisors"))

<li class="nav-item {{ session('sidebar.users_section') ? 'active':'' }}" data-name="users_section">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="fa fa-users"></i>
        <span class="title">{{_lang('app.manage_users')}}</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu" style="{{ session('sidebar.users_section-options') ? 'display:block;':'' }}"
        data-name="users_section">



        @if(Permissions::check("agents"))
        <li class="nav-item {{ session('sidebar.agents') ? 'active':'' }}" data-name="agents">
            <a href="{!! route('agents.index') !!}" class="nav-link">
                <i class="fa fa-user"></i>
                <span class="title">{{_lang('app.employees')}}</span>
            </a>
        </li>

        @endif

        @if(Permissions::check("supervisors"))
        <li class="nav-item {{ session('sidebar.supervisors') ? 'active':'' }}" data-name="supervisors">
            <a href="{!! route('supervisors.index') !!}" class="nav-link">
                <i class="fas fa-user-tie"></i>
                <span class="title">{{_lang('app.supervisors')}}</span>
            </a>
        </li>

        @endif




    </ul>
</li>

@endif

@if(Permissions::check("visits"))

<li class="nav-item {{ session('sidebar.visitor_section') ? 'active':'' }}" data-name="visitor_section">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="fas fa-tasks"></i>
        <span class="title">{{_lang('app.manage_visits')}} </span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu" style="{{ session('sidebar.visitor_section_options') ? 'display:block;':'' }}"
        data-name="visitor_section_options">

        @if(Permissions::check("visits"))
        <li class="nav-item {{ session('sidebar.today_tasks') ? 'active':'' }}" data-name="today_tasks">
            <a href="{!! route('visits.today_tasks') !!}" class="nav-link">
                <i class="fas fa-calendar-day"></i>
                <span class="title">{{_lang('app.today\'s_visits')}}</span>
            </a>
        </li>

        @endif

        @if(Permissions::check("visits"))
        <li class="nav-item {{ session('sidebar.visits_scheduled') ? 'active':'' }}" data-name="visits_scheduled">
            <a href="{!! route('visits.index') !!}?type={{\App\Models\Visit::$types["scheduled"]}}" class="nav-link">
                <i class="fas fa-calendar-alt"></i>
                <span class="title">{{_lang('app.scheduled_visits')}}</span>
            </a>
        </li>

        @endif

        @if(Permissions::check("visits"))
        <li class="nav-item {{ session('sidebar.visits_normal') ? 'active':'' }}" data-name="visits_normal">
            <a href="{!! route('visits.index') !!}?type={{\App\Models\Visit::$types["normal"]}}" class="nav-link">
                <i class="far fa-calendar-check"></i>
                <span class="title">{{_lang('app.normal_visits')}}</span>
            </a>
        </li>

        @endif




    </ul>
</li>


@endif