<!-- BEGIN PAGE BAR -->
<div class="theme-panel hidden-xs hidden-sm">
    <div class="toggler" style="display: block;">
    </div>
    <div class="toggler-close" style="display: none;">
    </div>
    <div class="theme-options" style="display: none;">
       <div class="theme-option theme-colors clearfix">
           <span>{{_lang('app.settings')}}</span>
        </div>
        <div class="theme-option">
            <span>
                {{_lang('app.language')}} </span>
            <select class="layout-option form-control input-sm" id="change-lang">
                @if ($lang_code == 'ar')
                    <option value="ar">{{_lang('app.arabic')}}</option>
                    <option value="en">{{_lang('app.english')}}</option>
                @else
                    <option value="en">{{_lang('app.english')}}</option>
                    <option value="ar">{{_lang('app.arabic')}}</option>
                @endif

            </select>
        </div>

    </div>
</div>
<div class="page-bar">
    <ul class="page-breadcrumb">
        @yield('breadcrumb')
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->

<h1 class="page-title"> @yield('pageTitle')
    <small></small>
</h1>
<!-- END PAGE TITLE-->
<!--<div class="clearfix"></div>-->