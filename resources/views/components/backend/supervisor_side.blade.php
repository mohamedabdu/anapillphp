<li class="nav-item {{ isset($tab) && $tab == 'dashboard' ? 'active':'' }}" data-name="" data-type="supervisor">
    <a href="{{ route('supervisor.dashboard') }}" class="nav-link">
        <i class="fa fa-home" style="font-size: 1.5em;"></i><span class="title">الرئيسية</span>
    </a>
</li>

<li class="nav-item {{ isset($parent_tab) && $parent_tab == 'reports_section' ? 'active':'' }}" data-name="" data-type="supervisor">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="fa fa-exclamation-circle" style="font-size: 1.5em;"></i>
        <span class="title">التقارير</span>
        <span class="arrow {{ isset($parent_tab) && $parent_tab == 'reports_section' ? 'open':'' }}"></span>
    </a>

    <ul class="sub-menu" style="{{ isset($parent_tab) && $parent_tab == 'reports_section' ? 'display:block;':'' }}" data-name="">
        <li class="nav-item {{ isset($tab) && $tab == 'products_almost_finish_reports' ? 'active':'' }}" data-name="" data-type="supervisor">
            <a href="{{ route('supervisor.reports.products_almost_finish_reports') }}" class="nav-link">
                @if ($statistics_counters->unread_products_almost_finish_reports_count)
                    <span class="badge badge-warning" style="border-radius: 100% !important; width:10px; height:11px; position:absolute; right:40px"> </span>
                @endif
                <i class="fa fa-exclamation-circle"></i>
                <span class="title" style="font-size: 13px">منتجات شارفت على الإنتهاء</span>
            </a>
        </li>
        <li class="nav-item {{ isset($tab) && $tab == 'manual_reports' ? 'active':'' }}" data-name="" data-type="supervisor">
            <a href="{{ route('supervisor.reports.manual_reports') }}" class="nav-link">
                @if ($statistics_counters->unread_manual_reports_count)
                    <span class="badge badge-warning" style="border-radius: 50% !important; width:10px; height:11px; position:absolute; right:40px"> </span>
                @endif
                <i class="fa fa-exclamation-circle"></i>
                <span class="title" style="font-size: 13px">تقرير يدوي</span>
            </a>
        </li>
        <li class="nav-item {{ isset($sub_parent_tab) && $sub_parent_tab == 'competitor_reports_section' ? 'active':'' }}" data-name="" data-type="supervisor">
            <a href="javascript:;" class="nav-link nav-toggle">
                @if ($statistics_counters->unread_price_comparison_reports_count || $statistics_counters->unread_promotions_reports_count || $statistics_counters->unread_areas_reports_count || $statistics_counters->unread_reduced_prices_reports_count || $statistics_counters->unread_ordinary_reports_count)
                    <span class="badge badge-warning" style="border-radius: 100% !important; width:10px; height:11px; position:absolute; right:40px"> </span>
                @endif
                <i class="fa fa-exclamation-circle"></i>
                <span class="title">تقارير المنافسين</span>
                <span class="arrow {{ isset($sub_parent_tab) && $sub_parent_tab == 'competitor_reports_section' ? 'open':'' }}"></span>
            </a>
        
            <ul class="sub-menu" style="{{ isset($sub_parent_tab) && $sub_parent_tab == 'competitor_reports_section' ? 'display:block;':'' }}" data-name="">
        
                <li class="nav-item {{ isset($tab) && $tab == 'price_comparison_reports' ? 'active':'' }}" data-name="" data-type="supervisor">
                    <a href="{{ route('supervisor.reports.price_comparison_reports') }}" class="nav-link">
                        @if ($statistics_counters->unread_price_comparison_reports_count)
                            <span class="badge badge-warning" style="border-radius: 100% !important; width:10px; height:11px; position:absolute; top:11px; right:45px"> </span>
                        @endif
                        <span class="title">مقارنة أسعار</span>
                    </a>
                </li>

                 <li class="nav-item {{ isset($tab) && $tab == 'promotions_reports' ? 'active':'' }}" data-name="" data-type="supervisor">
                    <a href="{{ route('supervisor.reports.promotions_reports') }}" class="nav-link">
                        @if ($statistics_counters->unread_promotions_reports_count)
                            <span class="badge badge-warning" style="border-radius: 100% !important; width:10px; height:11px; position:absolute; top:11px; right:45px"> </span>
                        @endif
                        <span class="title">العروض الترويجية</span>
                    </a>
                </li>

                <li class="nav-item {{ isset($tab) && $tab == 'areas_reports' ? 'active':'' }}" data-name="" data-type="supervisor">
                    <a href="{{ route('supervisor.reports.areas_reports') }}" class="nav-link">
                        @if ($statistics_counters->unread_areas_reports_count)
                            <span class="badge badge-warning" style="border-radius: 100% !important; width:10px; height:11px; position:absolute; top:11px; right:45px"> </span>
                        @endif
                        <span class="title">مساحات</span>
                    </a>
                </li>

                <li class="nav-item {{ isset($tab) && $tab == 'reduced_prices_reports' ? 'active':'' }}" data-name="" data-type="supervisor">
                    <a href="{{ route('supervisor.reports.reduced_prices_reports') }}" class="nav-link">
                        @if ($statistics_counters->unread_reduced_prices_reports_count)
                            <span class="badge badge-warning" style="border-radius: 100% !important; width:10px; height:11px; position:absolute; top:11px; right:45px"> </span>
                        @endif
                        <span class="title">تقرير أسعار مخفضة</span>
                    </a>
                </li>

                <li class="nav-item {{ isset($tab) && $tab == 'ordinary_reports' ? 'active':'' }}" data-name="" data-type="supervisor">
                    <a href="{{ route('supervisor.reports.ordinary_reports') }}" class="nav-link">
                        @if ($statistics_counters->unread_ordinary_reports_count)
                            <span class="badge badge-warning" style="border-radius: 100% !important; width:10px; height:11px; position:absolute; top:11px; right:45px"> </span>
                        @endif
                        <span class="title"> تقرير عادي </span>
                    </a>
                </li>

            </ul>
        </li>
    </ul>
</li>

<li class="nav-item {{ isset($tab) && $tab == 'holiday_requests' ? 'active':'' }}" data-name="" data-type="supervisor">
    <a href="{{ route('supervisor.holiday_requests') }}" class="nav-link">
        @if ($statistics_counters->pending_holiday_requests_count)
            <span class="badge badge-warning" style="border-radius: 100% !important; width:10px; height:11px; position:absolute; left:100px"> </span>
        @endif
        <i class="fa fa-edit" style="font-size: 1.5em;"></i><span class="title">طلبات الإجازة</span>
    </a>
</li>

<li class="nav-item {{ isset($parent_tab) && $parent_tab == 'cancel_requests' ? 'active':'' }}" data-name="" data-type="supervisor">
    <a href="javascript:;" class="nav-link nav-toggle">
        @if ($statistics_counters->pending_visits_cancel_requests_count || $statistics_counters->pending_tasks_cancel_requests_count)
            <span class="badge badge-warning" style="border-radius: 100% !important; width:10px; height:11px; position:absolute; left:100px"> </span>
        @endif
        <i class="fa fa-times-circle" style="font-size: 1.5em;"></i>
        <span class="title">طلبات الإلغاء</span>
        <span class="arrow {{ isset($parent_tab) && $parent_tab == 'cancel_requests' ? 'open':'' }}"></span>
    </a>

    <ul class="sub-menu" style="{{ isset($parent_tab) && $parent_tab == 'cancel_requests' ? 'display:block;':'' }}" data-name="">

        <li class="nav-item {{ isset($tab) && $tab == 'visits_cancel_requests' ? 'active':'' }}" data-name="" data-type="supervisor">
            <a href="{{ route('supervisor.visits_cancel_requests') }}" class="nav-link">
                @if ($statistics_counters->pending_visits_cancel_requests_count)
                    <span class="badge badge-warning" style="border-radius: 100% !important; width:10px; height:11px; position:absolute; right:40px"> </span>
                @endif
                <span class="title" style="font-size: 13px">طلبات إلغاء الزيارات</span>
            </a>
        </li>
        <li class="nav-item {{ isset($tab) && $tab == 'tasks_cancel_requests' ? 'active':'' }}" data-name="" data-type="supervisor">
            <a href="{{ route('supervisor.tasks_cancel_requests') }}" class="nav-link">
                @if ($statistics_counters->pending_tasks_cancel_requests_count)
                    <span class="badge badge-warning" style="border-radius: 50% !important; width:10px; height:11px; position:absolute; right:40px"> </span>
                @endif
                <span class="title" style="font-size: 13px">طلبات إلغاء المهمات</span>
            </a>
        </li>
    </ul>
</li>
