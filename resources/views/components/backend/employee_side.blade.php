<li class="nav-item {{ session('sidebar.dashboard') ? 'active':'' }}" data-name="dashboard" data-type="employee">
    <a href="{{ route('employee.dashboard') }}" class="nav-link">
        <i class="fa fa-home" style="font-size: 1.5em;"></i><span class="title">الرئيسية</span>
    </a>
</li>
<li class="nav-item {{ session('sidebar.search') ? 'active':'' }}" data-name="search" data-type="employee">
    <a href="{{ route('employee.search') }}" class="nav-link">
        <i class="fa fa-search" style="font-size: 1.5em;"></i><span class="title">بحث</span>
    </a>
</li>
<li class="nav-item {{ session('sidebar.reports_section') ? 'active':'' }}" data-name="reports_section" data-type="employee">
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="fa fa-exclamation-circle" style="font-size: 1.5em;"></i>
        <span class="title">التقارير</span>
        <span class="arrow {{ session('sidebar.reports_section') ? 'open':'' }}"></span>
    </a>

    <ul class="sub-menu" style="{{ session('sidebar.reports_section_options') ? 'display:block;':'' }}"
        data-name="reports_section_options">

        <li class="nav-item {{ session('sidebar.products_almost_finish_report') ? 'active':'' }}" data-name="products_almost_finish_report" data-type="employee">
            <a href="{{ route('employee.reports.products_almost_finish_report') }}" class="nav-link">
                <span class="title">منتجات شارفت على الإنتهاء</span>
            </a>
        </li>

        <li class="nav-item {{ session('sidebar.manual_report') ? 'active':'' }}" data-name="manual_report"
            data-type="employee">
            <a href="{{ route('employee.reports.manual_report') }}" class="nav-link">
                <span class="title">تقرير يدوي</span>
            </a>
        </li>

        <li class="nav-item {{ session('sidebar.competitor_reports_section') ? 'active':'' }}" data-name="competitor_reports_section"
            data-type="employee">
            <a href="javascript:;" class="nav-link nav-toggle">
                <span class="title">تقارير المنافسين</span>
                <span class="arrow {{ session('sidebar.competitor_reports_section') ? 'open':'' }}"></span>
            </a>
        
            <ul class="sub-menu" style="{{ session('sidebar.competitor_reports_section') ? 'display:block;':'' }}"
                data-name="competitor_reports_section">
        
                <li class="nav-item {{ session('sidebar.price_comparison_report') ? 'active':'' }}"
                    data-name="price_comparison_report" data-type="employee">
                    <a href="{{ route('employee.reports.price_comparison_report') }}" class="nav-link">
                        <span class="title">مقارنة أسعار</span>
                    </a>
                </li>

                <li class="nav-item {{ session('sidebar.promotions_report') ? 'active':'' }}" data-name="promotions_report" data-type="employee">
                    <a href="{{ route('employee.reports.promotions_report') }}" class="nav-link">
                        <span class="title">العروض الترويجية</span>
                    </a>
                </li>

                <li class="nav-item {{ session('sidebar.areas_report') ? 'active':'' }}" data-name="areas_report"
                    data-type="employee">
                    <a href="{{ route('employee.reports.areas_report') }}" class="nav-link">
                        <span class="title">مساحات</span>
                    </a>
                </li>

                <li class="nav-item {{ session('sidebar.reduced_prices_report') ? 'active':'' }}" data-name="reduced_prices_report" data-type="employee">
                    <a href="{{ route('employee.reports.reduced_prices_report') }}" class="nav-link">
                        <span class="title">تقرير الأسعار المخفضة</span>
                    </a>
                </li>

                
                <li class="nav-item {{ session('sidebar.ordinary_report') ? 'active':'' }}" data-name="ordinary_report"
                    data-type="employee">
                    <a href="{{ route('employee.reports.ordinary_report') }}" class="nav-link">
                        <span class="title">تقرير عادي</span>
                    </a>
                </li>
  
            </ul>
        </li>

       

    </ul>
</li>