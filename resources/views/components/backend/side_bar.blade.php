<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu page-sidebar-menu-light" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->
            
           
            <li class="sidebar-search-wrapper">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
               <div class="page-logo text-center" style="margin-bottom:5%">
                <a href="{{route('employee.dashboard')}}">
                    <img src="{{url('public/backend/images/logo.png')}}" width="30%;" height="20%;" alt="logo"
                        class="logo-default" /> </a>
                </div>
                <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>
           

            @if ($user->type == 1)
                @include('components.backend.admin_side')
            @elseif($user->type == 2)
                @include('components.backend.employee_side')
            @elseif($user->type == 3)
                @include('components.backend.supervisor_side')
            @endif



        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>