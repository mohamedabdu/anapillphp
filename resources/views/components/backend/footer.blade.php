<!--[if lt IE 9]>
<script src="{{url('public/backend/plugins')}}/respond.min.js"></script>
<script src="{{url('public/backend/plugins')}}/excanvas.min.js"></script>
<script src="{{url('public/backend/plugins')}}/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="{{url('public/backend/plugins')}}/jquery.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/jquery-validation/js/messages.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/jquery-confirm/js/jquery-confirm.js" type="text/javascript"></script>


<script src="{{url('public/backend/plugins')}}/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/bootbox/bootbox.min.js" type="text/javascript"></script>
<!--<script src="{{url('public/backend/plugins')}}/bootstrap-colorpicker/js/bootstrap-colorpicker.js" type="text/javascript"></script>-->
<script src="{{url('public/backend/plugins')}}/js.cookie.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

<script src="{{url('public/backend/plugins')}}/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{url('public/backend/plugins')}}/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/moment.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/morris/morris.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/morris/raphael-min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/counterup/jquery.counterup.min.js" type="text/javascript"></script>

<script src="{{url('public/backend/plugins')}}/tinymce/tinymce.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript">
</script>
<script src="{{url('public/backend/plugins')}}/webcam.js"></script>

<script src="{{url('public/backend/plugins')}}/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/bootstrap-toggle/js/bootstrap-toggle.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/jquery-knob/js/jquery.knob.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/icheck/icheck.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script src="{{url('public/backend/plugins')}}/jquery.stopwatch.js" type="text/javascript"></script>


<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL GLOBAL -->
<script src="{{url('public/backend/scripts')}}/app.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL GLOBAL -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!--<script src="{{url('public/backend/scripts')}}/metronic.js" type="text/javascript"></script>-->
<script src="{{url('public/backend/scripts')}}/dashboard.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/scripts')}}/table-datatables-managed.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/scripts')}}/datatable.js" type="text/javascript"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->

<script src="{{url('public/backend/scripts')}}/components-bootstrap-tagsinput.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/scripts')}}/components-multi-select.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/scripts')}}/layout.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/scripts')}}/quick-sidebar.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/scripts')}}/demo.min.js" type="text/javascript"></script>
<script src="{{url('public/backend/js')}}/my.js" type="text/javascript"></script>
<script src="{{url('public/backend/js')}}/main.js" type="text/javascript"></script>


<!-- END THEME LAYOUT SCRIPTS -->

<script src="{{url('public/backend/plugins')}}/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
    type="text/javascript"></script>
<script type="text/javascript" src="{{url('public/backend/js')}}/jquery.jscroll.js"></script>

<script src="{{url('public/backend/plugins')}}/Grid-Gallery-Lightbox/js/grid-gallery.js" type="text/javascript">
</script>

<script type="text/javascript" src="{{url('public/backend/chosen')}}/chosen.jquery.js"></script>
<script type="text/javascript" src="{{url('public/backend/chosen')}}/docsupport/prism.js"></script>
<script type="text/javascript" src="{{url("public/backend/datepicker")}}/bootstrap-datetimepicker.js"></script>
<script src="https://kit.fontawesome.com/e1292b87a4.js" crossorigin="anonymous"></script>


@if(!isset($showStoresMap))
@include("layouts.libs.map.mapJs")
@endif