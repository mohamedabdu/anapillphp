<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>{{env("APP_NAME")}}</title>
    <!--favicon-->
    <link rel="icon" href="{{url("public/admin")}}/assets/images/favicon.ico" type="image/x-icon">
    <!-- Bootstrap core CSS-->
    <link href="{{url("public/admin")}}/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <!-- animate CSS-->
    <link href="{{url("public/admin")}}/assets/css/animate.css" rel="stylesheet" type="text/css"/>
    <!-- Icons CSS-->
    <link href="{{url("public/admin")}}/assets/css/icons.css" rel="stylesheet" type="text/css"/>
    <!-- Custom Style-->
    <link href="{{url("public/admin")}}/assets/css/app-style.css" rel="stylesheet"/>

</head>

<body class="bg-theme bg-theme2">

<!-- start loader -->
<div id="pageloader-overlay" class="visible incoming"><div class="loader-wrapper-outer"><div class="loader-wrapper-inner" ><div class="loader"></div></div></div></div>
<!-- end loader -->

<!-- Start wrapper-->
<div id="wrapper">

    <div class="loader-wrapper"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div>
    <div class="card card-authentication1 mx-auto my-5">
        <div class="card-body">
            <div class="card-content p-2">
                <div class="text-center">
                    <img src="{{url("public/admin")}}/assets/images/logo-icon.png" style="width:35px;" alt="logo icon">
                </div>
                <div class="card-title text-uppercase text-center py-3">نسيت كلمة السر</div>
                <form  id="loginform" method="post" action="{{ url('/admin/password/reset') }}">
                    {{csrf_field()}}
                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group">
                        <label for="exampleInputUsername" class="sr-only">البريد الالكتروني</label>
                        <div class="position-relative has-icon-right">
                            <input type="email" id="exampleInputUsername" value="{{ old('email') }}" name="email" class="form-control input-shadow" placeholder="Enter Email">
                            <div class="form-control-position">
                                <i class="icon-user"></i>
                            </div>
                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                        @endif

                    </div>

                    <div class="form-group">
                        <label for="exampleInputUsername" class="sr-only">كلمة السر</label>
                        <div class="position-relative has-icon-right">
                            <input type="password" id="exampleInputUsername" name="password" class="form-control input-shadow" placeholder="Enter Password">
                            <div class="form-control-position">
                                <i class="icon-user"></i>
                            </div>
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                        @endif

                    </div>

                    <div class="form-group">
                        <label for="exampleInputUsername" class="sr-only"> تاكيد كلمة السر</label>
                        <div class="position-relative has-icon-right">
                            <input type="password" id="exampleInputUsername" name="password_confirmation" class="form-control input-shadow" placeholder="Enter Password">
                            <div class="form-control-position">
                                <i class="icon-user"></i>
                            </div>
                        </div>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
                        @endif

                    </div>


                    <div class="form-row">
                        <div class="form-group col-6 text-left">
                            <a href="{{url("admin/login")}}">تسجيل دخول</a>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-light btn-block">استعادة</button>

                </form>
            </div>
        </div>
        {{--<div class="card-footer text-center py-3">--}}
            {{--<p class="text-warning mb-0">Do not have an account? <a href="#"> Sign Up here</a></p>--}}
        {{--</div>--}}
    </div>

</div><!--wrapper-->

<!-- Bootstrap core JavaScript-->
<script src="{{url("public/admin")}}/assets/js/jquery.min.js"></script>
<script src="{{url("public/admin")}}/assets/js/popper.min.js"></script>
<script src="{{url("public/admin")}}/assets/js/bootstrap.min.js"></script>

<!-- sidebar-menu js -->
<script src="{{url("public/admin")}}/assets/js/sidebar-menu.js"></script>

<!-- Custom scripts -->
<script src="{{url("public/admin")}}/assets/js/app-script.js"></script>

</body>
</html>
