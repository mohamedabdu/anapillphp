<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>{{config("app.name")}}</title>

    <link rel="icon" href="{{url("public/admin")}}/assets/images/favicon.ico" type="image/x-icon">
    <!-- Bootstrap core CSS-->
    <link href="{{url("public/admin")}}/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <!-- animate CSS-->
    <link href="{{url("public/admin")}}/assets/css/animate.css" rel="stylesheet" type="text/css"/>
    <!-- Icons CSS-->
    <link href="{{url("public/admin")}}/assets/css/icons.css" rel="stylesheet" type="text/css"/>
    <!-- Custom Style-->
    <link href="{{url("public/admin")}}/assets/css/app-style.css" rel="stylesheet"/>

</head>

<body class="bg-theme bg-theme1">

<!-- start loader -->
<div id="pageloader-overlay" class="visible incoming"><div class="loader-wrapper-outer"><div class="loader-wrapper-inner" ><div class="loader"></div></div></div></div>
<!-- end loader -->

<!-- Start wrapper-->
<div id="wrapper">

    <div class="card card-authentication1 mx-auto my-4">
        <div class="card-body">
            <div class="card-content p-2">
                <div class="text-center">
                    <img src="{{url("public/admin")}}/assets/images/logo-icon.png" alt="logo icon">
                </div>
                <div class="card-title text-uppercase text-center py-3">Sign Up</div>
                <form method="post" action="{{ url('/admin/register') }}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputName" class="sr-only">Name</label>
                        <div class="position-relative has-icon-right">
                            <input type="text" name="name" id="exampleInputName" class="form-control input-shadow" placeholder="Enter Your Name">
                            <div class="form-control-position">
                                <i class="icon-user"></i>
                            </div>
                        </div>
                        @if ($errors->has('name'))
                            <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                        @endif

                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmailId" class="sr-only">Email</label>
                        <div class="position-relative has-icon-right">
                            <input type="email" name="email" id="exampleInputEmailId" class="form-control input-shadow" placeholder="Enter Your Email ID">
                            <div class="form-control-position">
                                <i class="icon-envelope-open"></i>
                            </div>
                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword" class="sr-only">Password</label>
                        <div class="position-relative has-icon-right">
                            <input type="password" name="password" id="exampleInputPassword" class="form-control input-shadow" placeholder="Choose Password">
                            <div class="form-control-position">
                                <i class="icon-lock"></i>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="icheck-material-white">
                            <input type="checkbox" id="user-checkbox" checked="" />
                            <label for="user-checkbox">I Agree With Terms & Conditions</label>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-light btn-block waves-effect waves-light">Sign Up</button>


                </form>
            </div>
        </div>
        <div class="card-footer text-center py-3">
            <p class="text-warning mb-0">Already have an account? <a href="{{url("admin/login")}}"> Sign In here</a></p>
        </div>
    </div>



</div><!--wrapper-->

<!-- Bootstrap core JavaScript-->
<script src="{{url("public/admin")}}/assets/js/jquery.min.js"></script>
<script src="{{url("public/admin")}}/assets/js/popper.min.js"></script>
<script src="{{url("public/admin")}}/assets/js/bootstrap.min.js"></script>

<!-- sidebar-menu js -->
<script src="{{url("public/admin")}}/assets/js/sidebar-menu.js"></script>

<!-- Custom scripts -->
<script src="{{url("public/admin")}}/assets/js/app-script.js"></script>

</body>
</html>
