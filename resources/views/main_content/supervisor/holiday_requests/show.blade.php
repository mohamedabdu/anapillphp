@extends('layouts.backend')

@section('pageTitle', 'تفاصيل طلب إجازة')

@section('breadcrumb')
<li><span><a href="{{route('supervisor.dashboard')}}">الرئيسية</a></span><i class="fa fa-circle"></i></li>
<li><span>تفاصيل طلب إجازة</span></li>
@endsection

@section('content')
{{ csrf_field() }}
<div class="row">
    <div class="col-xs-12">
       
       
            <!-- BEGIN PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        {{$holiday_request->status_text}}
                    </div>
                
                </div>
                <div class="portlet-body">
                    <!--BEGIN TABS-->
                        <div class="row mb-5">
                            <div class="col-md-4 mb-3">
                                <span class="bold" style="font-size: 18px"> تاريخ الطلب : </span>
                                <span style="font-size: 16px">{{$holiday_request->created_at}}</span>
                            </div>

                            <div class="col-md-6 mb-3">
                                <span class="bold" style="font-size: 18px">اسم الموظف : </span>
                                <span style="font-size: 16px">{{$holiday_request->employee}}</span>
                            </div>

                            <div class="col-xs-12 mb-3">
                                <hr>
                                <span class="bold" style="font-size: 18px">عنوان الإجازة : </span>
                                <span style="font-size: 16px">{{$holiday_request->title}}</span>
                            </div>

                            <div class="col-xs-12 mb-3">
                                <div class="row">
                                    <div class="col-xs-12 mb-3">
                                        <hr>
                                        <span class="bold" style="font-size: 18px">وقت الإجازة : </span>
                                    </div>
                                    
                                    <div class="col-md-4 mb-3">
                                        <span class="bold" style="font-size: 18px">بداية الإجازة : </span>
                                        <span style="font-size: 16px">{{$holiday_request->start_date}}</span>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <span class="bold" style="font-size: 18px">نهاية الإجازة : </span>
                                        <span style="font-size: 16px">{{$holiday_request->end_date}}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 mb-3">
                                <hr>
                                <span class="bold" style="font-size: 18px">سبب الإجازة : </span>
                                <span style="font-size: 16px">{{$holiday_request->reason}}</span>
                            </div>
                            @if ($holiday_request->status == 2)
                                <div class="col-xs-12 mb-3">
                                    <hr>
                                    <span class="bold" style="font-size: 18px">سبب الرفض : </span>
                                    <span style="font-size: 16px">{{$holiday_request->rejection_reason}}</span>
                                </div>
                            @endif
                            @if ($holiday_request->status == 0)
                            <div class="row">
                                <div class="col-xs-12 mt-3 text-center">
                                    <hr>
                                    @if (!$holiday_request->reject_only)
                                        <button class="btn btn-primary" id="accept"> قبول </button>
                                    @endif
                                    <button class="btn btn-danger" id="reject"> رفض </button>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="row">
                            <form role="form" action="#" id="rejectionForm" style="display: none;">
                               
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label bold"> سبب الرفض </label>
                                        <textarea type="text" rows="5" class="form-control" name="rejection_reason"
                                            id="rejection_reason"></textarea>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            
                                <div class="col-xs-12 col-md-6 col-md-offset-3 text-center mt-3 mb-3">
                                    <button class="btn btn-block green submit-form" type="submit"> إرسال </button>
                                </div>
                            </form>
                        </div>
                        
                       
                    <!--END TABS-->
                </div>
            </div>
            <!-- END PORTLET-->
           
        
    </div>

</div>

@endsection
@section('js')
<script>
    var new_config = {
       holiday_request:"{{$holiday_request->id}}"
    }
</script>

<script src="{{url('public/backend/js/supervisor')}}/holiday_requests.js" type="text/javascript"></script>
@endsection