@extends('layouts.backend')

@section('pageTitle', 'طلبات الإجازة')

@section('breadcrumb')
<li><span><a href="{{route('supervisor.dashboard')}}">الرئيسية</a></span><i class="fa fa-circle"></i></li>
<li><span>طلبات الإجازة</span></li>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">البحث عن طلبات الإجازة</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <form role="form" action="#" id="filterForm">
                        {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">اسم الموظف</label>
                                <select name="employee" id="employee" class="form-control">
                                    <option value="">اختر</option>
                                    @foreach ($employees as $employee)
                                        <option value="{{encrypt($employee->id)}}">{{$employee->name}}</option>
                                    @endforeach
                        
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">حالة الطلب</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="">اختر</option>
                                    <option value="pending">قيد المراجعة</option>
                                    <option value="accepted">مقبول</option>
                                    <option value="rejected">مرفوض</option>
                                   
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <h4>تاريخ إرسال الطلب</h4>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"> من تاريخ </label>
                                <input type="text" class="form-control picker" autocomplete="off" name="from" id="from">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"> إلي تاريخ </label>
                                <input type="text" class="form-control picker" autocomplete="off" name="to" id="to">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <div class="col-xs-12">
                            <h4>تاريخ بدء الإجازة</h4>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"> من تاريخ </label>
                                <input type="text" class="form-control picker" autocomplete="off" name="start_from" id="start_from">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"> إلي تاريخ </label>
                                <input type="text" class="form-control picker" autocomplete="off" name="start_to" id="start_to">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <h4>تاريخ نهاية الإجازة</h4>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"> من تاريخ </label>
                                <input type="text" class="form-control picker" autocomplete="off" name="end_from" id="end_from">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"> إلي تاريخ </label>
                                <input type="text" class="form-control picker" autocomplete="off" name="end_to" id="end_to">
                                <span class="help-block"></span>
                            </div>
                        </div>
        
                        <div class="col-xs-12 col-md-6 col-md-offset-3 text-center mt-3 mb-3">
                            <button class="btn btn-block green submit-form" type="submit"> بحث </button>
                        </div>
                    </form>
                </div>
        
            </div>
        </div>
        <!-- END PORTLET-->

        
        <div id="holiday-requests-list">
            @foreach ($holiday_requests as $holiday_request)
            <!-- BEGIN PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        {{$holiday_request->status_text}}
                    </div>
                
                </div>
                <div class="portlet-body">
                    <!--BEGIN TABS-->
                        <div class="row mb-5">
                            <div class="col-md-4 mb-3">
                                <span class="bold" style="font-size: 18px"> تاريخ الطلب : </span>
                                <span style="font-size: 16px">{{$holiday_request->created_at}}</span>
                            </div>

                            <div class="col-md-6 mb-3">
                                <span class="bold" style="font-size: 18px">اسم الموظف : </span>
                                <span style="font-size: 16px">{{$holiday_request->employee}}</span>
                            </div>

                            <div class="col-md-6 mb-3">
                                <span class="bold" style="font-size: 18px">عنوان الإجازة : </span>
                                <span style="font-size: 16px">{{$holiday_request->title}}</span>
                            </div>

                            <div class="col-xs-12 mb-3">
                                <div class="row">
                                    <div class="col-xs-12 mb-3">
                                        <span class="bold" style="font-size: 18px">وقت الإجازة : </span>
                                    </div>
                                    
                                    <div class="col-md-4 mb-3">
                                        <span class="bold" style="font-size: 18px">بداية الإجازة : </span>
                                        <span style="font-size: 16px">{{$holiday_request->start_date}}</span>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <span class="bold" style="font-size: 18px">نهاية الإجازة : </span>
                                        <span style="font-size: 16px">{{$holiday_request->end_date}}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 mt-3">
                                <a href="{{route('supervisor.holiday_requests.show',$holiday_request->id)}}" class="btn btn-primary"> التفاصيل </a>
                            </div>
                        </div>
                       
                    <!--END TABS-->
                </div>
            </div>
            <!-- END PORTLET-->
            @endforeach
        </div>
        
        <div class="col-md-6 col-md-offset-3">
            <button class="btn btn-primary btn-block" id="load-more-button"> المزيد </button>
        </div>
    </div>

</div>

@endsection
@section('js')
<script>
    var new_config = {
       url : "{{route('supervisor.holiday_requests')}}"
    }
</script>

<script src="{{url('public/backend/js/supervisor')}}/holiday_requests.js" type="text/javascript"></script>
@endsection