@extends('layouts.backend')

@section('pageTitle', $page_title.' | '.$employee->name)

@section('breadcrumb')
<li><span><a href="{{route('supervisor.dashboard')}}">الرئيسية</a></span><i class="fa fa-circle"></i></li>
<li><span><a href="{{route('supervisor.reports.areas_reports')}}">{{$page_title}}</a></span><i class="fa fa-circle"></i></li>
<li><span>{{$employee->name}}</span></li>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <!-- BEGIN PORTLET-->
        <div id="reports-list">
            @foreach ($reports as $key => $report)
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        {{$report->created_at}}
                    </div>
                    
                </div>
                <div class="portlet-body">
                    <!--BEGIN TABS-->
                    
                        <div class="single-report">
                            <div class="row mb-5">
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <span class="bold" style="font-size: 18px">اسم الموظف : </span>
                                        <span style="font-size: 16px">{{$employee->name}}</span>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <span class="bold" style="font-size: 18px">تاريخ التقرير : </span>
                                        <span style="font-size: 16px">{{$report->date}}</span>
                                    </div>
                                </div>
                               
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <span class="bold" style="font-size: 18px">اسم المحل : </span>
                                        <span style="font-size: 16px">{{$report->store}}</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div>
                                        <span class="bold" style="font-size: 18px">اسم المنافس : </span>
                                        <span style="font-size: 16px">{{$report->competitor}}</span>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="mb-5">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <span class="caption-subject font-dark bold uppercase">التصنيفات</span>
                                            </div>
                                        </div>
                                        <div class="portlet-body flip-scroll">
                                            <table class="table table-striped table-bordered table-condensed flip-content">
                                                <thead class="flip-content">
                                                    <tr>
                                                        <th class="text-center"> التصنيف </th>
                                                        <th class="text-center"> مساحة المنافس </th>
                                                        <th class="text-center"> مساحة الشركة </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($report->categories as $category)
                                                    <tr>
                                                        <td class="text-center"> {{$category->category}} </td>
                                                        <td class="text-center"> 
                                                            العرض : {{$category->competitor_width}} -  
                                                            الطول : {{$category->competitor_height}} 
                                                        </td>
                                                        <td class="text-center">
                                                            العرض : {{$category->organization_width}} - 
                                                            الطول : {{$category->organization_height}} 
                                                        </td>
                                                    </tr>
                                                    @endforeach
                            
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                            
                                </div>
                            </div>
                            <div class="mb-3">
                                <span class="bold" style="font-size:20px;">ملاحظات : </span>
                                <span style="font-size:16px;">{{$report->notes}}
                                </span>
                            </div>
                            <div class="mb-3">
                                @if (isset($report->images))
                                <h3>صور : </h3>
                                <div class="row">
                                    @foreach ($report->images as $image)
                                    <div class="col-md-4 text-left">
                                        <img src="{{$image}}" style="height:250px; border:1px solid #dfdfdf; border-radius:20% !important;" alt="">
                                    </div>
                                    @endforeach
                                </div>
                                @endif
                            
                            </div>
                        </div>
                    
                    <!--END TABS-->
                </div>
            </div>
            @endforeach
        </div>
        <!-- END PORTLET-->
        <div class="col-md-6 col-md-offset-3">
            <button class="btn btn-primary btn-block" id="load-more-button"> المزيد </button>
        </div>
    </div>

</div>

@endsection
@section('js')
<script>
    var new_config = {
       url : "{{route('supervisor.reports.employee_areas_reports')}}",
       employee: "{{encrypt($employee->id)}}",
       employee_name: "{{$employee->name}}",
    }
</script>

<script src="{{url('public/backend/js/supervisor')}}/areas_reports.js" type="text/javascript"></script>
@endsection