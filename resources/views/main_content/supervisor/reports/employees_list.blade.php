@extends('layouts.backend')

@section('pageTitle', $page_title)

@section('breadcrumb')
<li><span>الرئيسية</span><i class="fa fa-circle"></i></li>
<li><span>{{$page_title}}</span></li>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered" style="min-height: 400px;">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase"> {{$page_title}} </span>
                </div>
            </div>
            <div class="portlet-body">
                <!--BEGIN TABS-->
                @if ($employees->count() > 0)
                    <ul class="feeds">
                        @foreach ($employees as $employee)
                        <li class="pt-1 col-md-4">
                            <a href="{{ route($route,['employee' => $employee->id]) }}">
                                <div class="col-lg-8 col-md-6 col-xs-8 col-sm-8">
                                    <div class="cont">
                                        <div class="cont-col2">
                                            <div class="desc">
                                                <span>
                                                    @if (!$employee->report_status)
                                                        <span class="badge badge-warning" 
                                                        style="border-radius: 50% !important; height:11px;margin-top: -7%; margin-left: -5%"> </span>
                                                    @endif
                                                    <i class="fa fa-exclamation-circle" style="font-size: 1.5em;"></i>
                                                </span>
                                                {{$employee->name}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-xs-4 col-sm-4" style="text-align:end">
                                    <span style="">{{$employee->report_date}}</span>
                                </div>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                @else
                    <h3 class="text-center text-default">لا يوجد تقارير ...</h3>
                @endif
               
               
                <!--END TABS-->
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    
</div>

@endsection
