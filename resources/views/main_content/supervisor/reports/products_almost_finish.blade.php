@extends('layouts.backend')

@section('pageTitle', $page_title.' | '.$employee->name)

@section('breadcrumb')
<li><span><a href="{{route('supervisor.dashboard')}}">الرئيسية</a></span><i class="fa fa-circle"></i></li>
<li><span><a href="{{route('supervisor.reports.products_almost_finish_reports')}}">{{$page_title}}</a></span><i class="fa fa-circle"></i></li>
<li><span>{{$employee->name}}</span></li>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-body">
                <div class="row">
                    <form role="form" action="#" id="filterForm">
                        {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">اسم المحل</label>
                                <select name="store" id="store" class="form-control multi-select-list" multiple="multiple">
                                    @foreach ($stores as $store)
                                        <option value="{{encrypt($store->id)}}">{{$store->title}}</option>
                                    @endforeach
                        
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">اسم المنتج</label>
                                <select name="product" id="product" class="form-control multi-select-list" multiple="multiple">
                                    @foreach ($products as $product)
                                        <option value="{{encrypt($product->id)}}">{{$product->title}}</option>
                                    @endforeach
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
        
                        
                        <div class="col-xs-12">
                            <h4>تاريخ  التقرير</h4>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"> من تاريخ </label>
                                <input type="text" class="form-control picker" autocomplete="off" name="from" id="from">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"> إلي تاريخ </label>
                                <input type="text" class="form-control picker" autocomplete="off" name="to" id="to">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-md-offset-3 text-center mt-3 mb-3">
                            <button class="btn btn-block green submit-form" type="submit"> بحث </button>
                        </div>
                    </form>
                </div>
        
            </div>
        </div>
        <!-- END PORTLET-->
        <!-- BEGIN PORTLET-->
        <div id="reports-list">
            @foreach ($reports as $key => $report)
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        {{$report->created_at}}
                    </div>
                    
                </div>
                <div class="portlet-body">
                    <!--BEGIN TABS-->
                    
                        <div class="single-report">
                            <div class="row mb-5">
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <span class="bold" style="font-size: 18px">اسم الموظف : </span>
                                        <span style="font-size: 16px">{{$employee->name}}</span>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div>
                                        <span class="bold" style="font-size: 18px">تاريخ التقرير : </span>
                                        <span style="font-size: 16px">{{$report->date}}</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div>
                                        <span class="bold" style="font-size: 18px">اسم المحل : </span>
                                        <span style="font-size: 16px">{{$report->store}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-5">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <span class="caption-subject font-dark bold uppercase">المنتجات</span>
                                            </div>
                                        </div>
                                        <div class="portlet-body flip-scroll">
                                            <table class="table table-striped table-bordered table-condensed flip-content">
                                                <thead class="flip-content">
                                                    <tr>
                                                        <th width="20%" class="text-center"> اسم المنتج </th>
                                                        <th class="numeric text-center"> عدد العلب </th>
                                                        <th class="numeric text-center"> عدد الكراتين </th>
                                                        <th class="numeric text-center"> تاريخ الإنتهاء </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($report->products as $product)
                                                    <tr>
                                                        <td class="text-center"> {{$product->name}} </td>
                                                        <td class="numeric text-center"> {{$product->pieces_number}} </td>
                                                        <td class="numeric text-center"> {{$product->cartons_number}} </td>
                                                        <td class="numeric text-center"> {{$product->date}} </td>
                                                    </tr>
                                                    @endforeach
                            
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                            
                                </div>
                            </div>
                            <div class="mb-3">
                                <span class="bold" style="font-size:20px;">ملاحظات : </span>
                                <span style="font-size:16px;">{{$report->notes}}
                                </span>
                            </div>
                            <div class="mb-3">
                                @if (isset($report->images))
                                <h3>صور : </h3>
                                <div class="row">
                                    @foreach ($report->images as $image)
                                    <div class="col-md-4 text-left">
                                        <img src="{{$image}}" style="height:250px; border:1px solid #dfdfdf; border-radius:20% !important;" alt="">
                                    </div>
                                    @endforeach
                                </div>
                                @endif
                            
                            </div>
                        </div>
                    
                    <!--END TABS-->
                </div>
            </div>
            @endforeach
        </div>
        <!-- END PORTLET-->
        <div class="col-md-6 col-md-offset-3">
            <button class="btn btn-primary btn-block" id="load-more-button"> المزيد </button>
        </div>
    </div>

</div>

@endsection
@section('js')
<script>
    var new_config = {
       url : "{{route('supervisor.reports.employee_products_almost_finish_reports')}}",
       employee: "{{encrypt($employee->id)}}",
       employee_name: "{{$employee->name}}"
    }
</script>

<script src="{{url('public/backend/js/supervisor')}}/products_almost_finish_reports.js" type="text/javascript"></script>
@endsection