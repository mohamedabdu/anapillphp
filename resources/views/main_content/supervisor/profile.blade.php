@extends('layouts.backend')

@section('pageTitle', 'الملف الشخصي')

@section('breadcrumb')
<li><span>لوحة التحكم</span><i class="fa fa-circle"></i></li>
<li><span>ملفي الشخصي</span></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="tabbable-custom nav-justified">
            <ul class="nav nav-tabs nav-justified">
                <li class="active">
                    <a href="#tab_1_1_1" data-toggle="tab">  </a>
                </li>
                
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1_1">
                    <div class="portlet light">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase"> الحساب الشخصي </span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab" aria-expanded="true">البيانات الشخصية</a>
                                </li>
                                <li class="">
                                    <a href="#tab_1_2" data-toggle="tab" aria-expanded="false">تغيير كلمة المرور</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <form role="form" action="#" id="updateProfile">
                                {{ csrf_field() }}
                                <div class="tab-content">
                                    <!-- PERSONAL INFO TAB -->
                    
                                    <div class="tab-pane active" id="tab_1_1">
                                       
                                        <div class="form-group form-md-line-input col-md-12 text-center">
                                            <div class="image_box text-center">
                                                <img style="border:5px solid #dfdfdf; border-radius: 20% !important" src="{{ isset($User->image) ? \Helper\Common\imageUrl($User->image) : url('no-image.png') }}" width="200"
                                                    height="200" class="image" />
                                            </div>
                                            <span class="text-primary"><i class="fa fa-camera fa-2x"></i></span>
                                            
                                            <input type="file" name="image" id="image" style="visibility: hidden">
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label class="control-label"><i class="fa fa-user"></i> الإسم</label>
                                            <input type="text" value="{{$User->name}}" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><i class="fa fa-envelope"></i> البريد الإلكتروني</label>
                                            <input type="email" value="{{$User->email}}" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><i class="fa fa-phone"></i> رقم الهاتف</label>
                                            <input type="text" value="{{$User->mobile}}" class="form-control" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><i class="fa fa-clock-o"></i> مواعيد العمل </label>
                                            <input type="text" class="form-control" value="{{' دوام '.$User->work_hours.' ساعات '}}"
                                                disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><i class="fa fa-calendar"></i> أيام العمل</label>
                                            <input type="text" class="form-control" value="{{ $User->work_days }}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><i class="fa fa-map-marker"></i> المدينة</label>
                                            <input type="text" class="form-control" value="{{$User->city}}" disabled>
                                        </div>
                    
                                        <div class="margiv-top-10">
                                            <button class="btn green submit-form" type="submit"> تعديل </button>
                                        </div>
                    
                                    </div>
                                    <!-- END PERSONAL INFO TAB -->
                                    <!-- CHANGE PASSWORD TAB -->
                                    <div class="tab-pane" id="tab_1_2">
                                        <div class="form-group">
                                            <label class="control-label">كلمة المرور الحالية</label>
                                            <input type="password" class="form-control" name="old_password">
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">كلمة المرور الجديدة</label>
                                            <input type="password" class="form-control" name="password" id="password">
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">أعد كتابة كلمة المرور الجديدة</label>
                                            <input type="password" class="form-control" name="password_confirmation">
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="margin-top-10">
                                            <button class="btn green submit-form" type="submit"> تعديل </button>
                                        </div>
                    
                                    </div>
                    
                                    <!-- END CHANGE PASSWORD TAB -->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>


@endsection

@section('js')
<script>
    var new_config = {
    }
</script>
<script src="{{url('public/backend/js/supervisor')}}/profile.js" type="text/javascript"></script>
@endsection