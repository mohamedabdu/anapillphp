@extends('layouts.backend')

@section('pageTitle', "تفاصيل زيارة -  $visit->store ")

@section('breadcrumb')
<li><span>لوحة التحكم</span><i class="fa fa-circle"></i></li>
<li><span>تفاصيل زيارة  -   {{ $visit->store }}</span></li>
@endsection

@section('content')

<div class="modal fade" id="addTask" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="addTaskLabel"></h4>
            </div>
            <div class="modal-body">
                <form role="form" id="addTaskForm">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">المهام</label>
                            <select class="form-control" id="task" name="task">
                                <option value="">اختر المهمة</option>
                                @foreach ($remaining_tasks as $item)
                                    <option value="{{ $item->process }}">{{$item->title}}</option>
                                @endforeach
                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <span class="margin-right-10 loading hide"><i class="fa fa-spin fa-spinner"></i></span>
                <button type="button" class="btn btn-info submit-form">اضافة</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">اغلاق</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="cancelVisit" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="cancelVisitLabel"></h4>
            </div>
            <div class="modal-body">
                <form role="form" id="cancelVisitForm">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">سبب الإلغاء</label>
                            <textarea name="cancellation_reason" rows="5" class="form-control"></textarea>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <span class="margin-right-10 loading hide"><i class="fa fa-spin fa-spinner"></i></span>
                <button type="button" class="btn btn-info submit-form">إرسال</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">اغلاق</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="cancelTask" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="cancelTaskLabel"></h4>
            </div>
            <div class="modal-body">
                <form role="form" id="cancelTaskForm">
                    {{ csrf_field() }}
                    <input type="hidden" name="task" id="process">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">سبب الإلغاء</label>
                            <textarea name="cancellation_reason" rows="5" class="form-control"></textarea>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <span class="margin-right-10 loading hide"><i class="fa fa-spin fa-spinner"></i></span>
                <button type="button" class="btn btn-info submit-form">إرسال</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">اغلاق</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">تفاصيل الزيارة</span>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_1_1" class="active" data-toggle="tab"> تفاصيل </a>
                    </li>
                    <li>
                        <a href="#tab_1_2" data-toggle="tab"> الخريطة </a>
                    </li>
                </ul>
            </div>
            <div class="portlet-body">
                <!--BEGIN TABS-->
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1_1">
                        <span>رقم الزيارة </span>
                        <h4 class="bold">{{$visit->code}}</h4>
                        <span>اخر زيارة </span>
                        <h4 class="bold">{{$visit->last_visit_date ?: '--'}}</h4>
                        <span>عنوان الزيارة </span>
                        <h4 class="bold" id="address"></h4>
                    </div>
                    <div class="tab-pane" id="tab_1_2">
                        <div id="map" style="height: 400px; width:100%;"></div>
                        <div id="infowindow-content">
                            <span id="place-name" class="title"></span><br>
                            Place ID <span id="place-id"></span><br>
                            <span id="place-address"></span>
                        </div>
                    </div>
                </div>
                <!--END TABS-->
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-md-6 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">المهام</span>
                </div>
                <h4 id="stopwatch" class="pull-right bold mt-3" style="{{ $visit->start_time ? 'display:block;' : 'display:none;' }}">00:00:00</h4>
            </div>
            <div class="text-right">
                <button id="addTaskButton" style="{{ !$visit->start_time ? 'display: none;' : '' }}" class="btn btn-default" onclick="Visits.addTask()"><i class="fa fa-plus-square"></i> اضافة مهمة</button>
            </div>
            <div class="portlet-body">
                <!--BEGIN TABS-->
                <div class="row">
                    <ul class="feeds col-md-9" id="tasks">
                        @foreach ($visit_tasks as $task)
                            <li class="pt-1">
                                <div class="col-lg-8 col-md-6 col-xs-8 col-sm-8">
                                    <div class="cont">
                                        <div class="cont-col2">
                                            <div class="desc">
                                                {{$task->title}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-xs-4 col-sm-4 task-actions" style="{{ !$visit->start_time ? 'display: none;' : '' }}">
                                    <span id="cancel-message" style="{{ $task->visit_task_cancel && $task->visit_task_cancel_status == 0 ? 'display:block;' : 'display:none;'}}">تم ارسال طلب إلغاء</span>
                                    @if ($task->status == 0 && !$task->start_date && (($task->visit_task_cancel && $task->visit_task_cancel_status == 2) || (!$task->visit_task_cancel && !$task->visit_task_cancel_status)))
                                        <button onclick="Visits.startTask(this)" data-task ="{{ $task->process }}" data-href="{{route($tasks_routes[$task->process],['code' => $visit->code])}}" class="btn btn-sm btn-primary" id="start-task">
                                            بدء
                                        </button>
                                        <button class="btn btn-sm btn-default" onclick="Visits.cancelTask(this)" data-task ="{{$task->process}}" >
                                            إلغاء
                                        </button>
                                    @elseif($task->status == 0 && $task->start_date)
                                        <a href="{{route($tasks_routes[$task->process],['code' => $visit->code])}}" class="btn btn-sm btn-warning">
                                            إكمال المهمة
                                        </a>
                                    @elseif($task->status == 1)
                                       <label for="" class="label label-primary">مكتملة</label> 
                                    @elseif($task->status == 2)
                                        <label for="" class="label label-danger">ملغاة</label> 
                                    @endif
                                    
                                </div>
                            </li>
                        @endforeach
                       
                    </ul>
                </div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>

<div class="row">
    <div class="container text-center">
        <h3 class="text-danger" id="cancle-alert" style="{{ $visit->visit_cancel && $visit->visit_cancel_status == 0 ? 'display:block;' : 'display:none;' }}">
            في إنتظار الرد على طلب إلغاء الزيارة
        </h3>

        @if (!$visit->start_time)
            @if (($visit->visit_cancel && $visit->visit_cancel_status == 2) || (!$visit->visit_cancel && !$visit->visit_cancel_status))
                <div class="col-md-6 col-xs-12 text-center mb-3">
                    <button id="start-visit" class="btn btn-primary btn-block" onclick="Visits.startVisit()">بدء
                        الزيارة
                    </button>
                </div>
                <div class="col-md-6 col-xs-12 text-center">
                    <button id="cancel-visit" class="btn btn-primary btn-block" onclick="Visits.cancelVisit()">إلغاء
                        الزيارة
                    </button>
                </div>
            @endif
        @endif

        <div id="finish-visit" style="{{ !$visit->start_time ? 'display: none;' : '' }}" class="col-md-6 col-xs-12 col-centered text-center">
            <button class="btn btn-primary btn-block" onclick="Visits.finishVisit()">إنهاء الزيارة</button>
        </div>

    </div>
</div>

@endsection

@section('js')
<script>
    var new_config = {
        lat : "{{$visit->lat}}",
        lng: "{{$visit->lng}}",
        title: "{{$visit->store}}",
        code : "{{$visit->code}}",
        token : "{{csrf_token()}}",
        start_time : "{{ isset($start_time) ? $start_time : ''  }}"
    }
    
    
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQxy6KaWYNPIR0ymzvi5_gUIPNu7QP8VU&libraries=places&language={{App::getLocale()}}" defer></script>
<script src="{{url('public/backend/js/employee')}}/visits.js" type="text/javascript"></script>
@endsection