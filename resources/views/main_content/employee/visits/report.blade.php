@extends('layouts.backend')

@section('pageTitle', "تقرير زيارة - $visit->store - $visit->code")

@section('breadcrumb')
<li><span>لوحة التحكم</span><i class="fa fa-circle"></i></li>
<li><span>تقرير زيارة - {{$visit->store}} - {{$visit->code}}</span></li>
@endsection

@section('content')
<div class="modal fade" id="finishVisit" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="finishVisitLabel"></h4>
            </div>
            <div class="modal-body">
                <form role="form" id="finishVisitForm">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">سبب الإنهاء</label>
                            <textarea name="termination_reason" rows="5" class="form-control"></textarea>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <span class="margin-right-10 loading hide"><i class="fa fa-spin fa-spinner"></i></span>
                <button type="button" class="btn btn-info submit-form">إرسال</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">اغلاق</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="col-lg-6 col-xs-12 col-centered text-center">
            <input type="text" value="{{$visit->completion_percentage}}" class="completed" data-linecap=round>
            <div class="portlet light bordered">
                <div class="portlet-body">
                   <div class="row">
                        <div class="col-md-4 text-center">
                            <span>وقت البدء </span>
                            <h4 class="bold">{{$visit->start_time}}</h4>
                        </div>
                        <div class="col-md-4 text-center">
                            <span>وقت الإنهاء </span>
                            <h4 class="bold">{{$visit->end_time}}</h4>
                        </div>
                        <div class="col-md-4 text-center">
                            <span>الوقت الكلي </span>
                            <h4 class="bold">{{$visit->total_time}}</h4>
                        </div>
                   </div>
                </div>
            </div>
        </div>
        
        <!-- END PORTLET-->
    </div>
    <div class="col-lg-6 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">الموقع</span>
                </div>
                
            </div>
            <div class="portlet-body">
                <span>عند بداية الزيارة </span>
                <h4 class="bold" id="start-address"></h4>
                <span>عند نهاية الزيارة</span>
                <h4 class="bold" id="end-address"></h4>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-lg-6 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">المهام</span>
                </div>
            </div>
            
            <div class="portlet-body">
                <!--BEGIN TABS-->
                <div class="row">
                    <ul class="feeds col-md-9">
                        <li class="">
                            <div class="col1">
                                <div class="cont">
                                    <div class="cont-col2">
                                        <div class="desc">
                                            المهام المكتملة
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col2">
                                <div class="date bold" style="color: #1BBC9B">
                                   {{count($finished_tasks)}}
                                </div>
                            </div>
                        </li>
                       @foreach ($finished_tasks as $finished_task)
                           <li class="mr-5">
                            <div class="col1">
                                <div class="cont">
                                    <div class="cont-col2">
                                        <div class="desc">
                                            <span class="bold">{{ $finished_task->task }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                       @endforeach
                            
                        
                        <li class="">
                            <div class="col1">
                                <div class="cont">
                                    <div class="cont-col2">
                                        <div class="desc">
                                            المهام الملغاة
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col2">
                                <div class="date bold" style="color: #ed6b75">
                                    {{count($cancelled_tasks)}}
                                </div>
                            </div>
                        </li>
                        @foreach ($cancelled_tasks as $cancelled_task)
                        <li class="mr-5">
                            <div class="col1">
                                <div class="cont">
                                    <div class="cont-col2">
                                        <div class="desc">
                                            <span class="bold">{{ $cancelled_task->task }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach

                        <li class="">
                            <div class="col1">
                                <div class="cont">
                                    <div class="cont-col2">
                                        <div class="desc">
                                            المهام الغير منجزة
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col2">
                                <div class="date bold" style="color: #F7CA18">
                                    {{count($unfinished_tasks)}}
                                </div>
                            </div>
                        </li>

                        @foreach ($unfinished_tasks as $unfinished_task)
                        <li class="mr-5">
                            <div class="col1">
                                <div class="cont">
                                    <div class="cont-col2">
                                        <div class="desc">
                                            <span class="bold">{{ $unfinished_task->task }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>

            </div>
        </div>
        <!-- END PORTLET-->
      
    </div>
    
    @if (!$finished_uncompleted)
        <div class="col-lg-6 col-xs-12 col-centered text-center">
            <button class="btn btn-primary btn-block" onclick="VisitReport.finishVisit()">تأكيد</button>
        </div>
    @endif
  
</div>
@endsection

@section('js')
<script>
    var new_config = {
        code : "{{$visit->code}}",
        start_lat : "{{$visit->start_lat}}",
        start_lng : "{{$visit->start_lng}}",
        end_date : "{{$visit->end_date}}",
        end_time : "{{$visit->end_time}}",
        end_lat : "{{$visit->end_lat ?: ''}}",
        end_lng : "{{$visit->end_lng ?: ''}}",
        unfinished_count: "{{count($unfinished_tasks)}}"
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQxy6KaWYNPIR0ymzvi5_gUIPNu7QP8VU&libraries=places&language={{App::getLocale()}}" defer></script>

<script src="{{url('public/backend/js/employee')}}/visit_report.js" type="text/javascript"></script>
@endsection