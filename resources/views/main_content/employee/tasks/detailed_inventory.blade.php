@extends('layouts.backend')

@section('pageTitle', $task_name)

@section('breadcrumb')
<li><span>الرئيسية</span><i class="fa fa-circle"></i></li>
<li><span><a href="{{route('employee.visits.show',['code' => $code])}}">زيارة - {{$store}} - {{$code}}</a></span><i class="fa fa-circle"></i></li>
<li><span>{{$task_name}}</span></li>
@endsection

@section('content')
<div class="modal fade" id="finshTask" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="finshTaskLabel"></h4>
            </div>
            <div class="modal-body">
                <form role="form" id="finshTaskForm">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label"> ملاحظات </label>
                            <textarea type="text" rows="7" class="form-control" name="notes"></textarea>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <span class="margin-right-10 loading hide"><i class="fa fa-spin fa-spinner"></i></span>
                <button type="button" class="btn btn-info submit-form">انهاء المهمة</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">اغلاق</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">{{$task_name}}</span>
                     <span class="text-right">
                        <a target="_blank" href="{{ route('employee.visits.tasks.description',['task' => str_replace(' ','-',$task_name)]) }}" class="btn btn-info">وصف المهمة</a>
                    </span>
                </div>
                <h4 id="stopwatch" class="pull-right bold">00:00:00</h4>
            </div>
            <div class="portlet-body">
                <form role="form" action="#" id="taskForm">
                    {{ csrf_field() }}
                    <div class="row">
                        @for ($i = 0; $i < $products->count(); $i++)
                            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-3 col-sm-3 col-xs-3 text-left">
                                                    <img src="{{  \Helper\Common\imageUrl($products[$i]->image) }}" style="width:100%; height:20%" />
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-8 text-left">
                                                    <h4>{{$products[$i]->title}}</h4>
                                                    <span>123654654</span>
                                                </div>
                                            </div>
                            
                                            <div class="col-md-12 col-sm-12 col-xs-12 mt-3">
                                                <input type="hidden" name="products[{{$i}}][product]" value="{{$products[$i]->id}}">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                    <label>
                                                                        <input type="checkbox" class="icheck" data-checkbox="icheckbox_flat-blue"
                                                                            name="products[{{$i}}][in_shelf]" value="1">
                                                                        موجود فى الشلف
                                                                    </label>
                            
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 count">
                                                                    <input type="text" disabled class="counter" value="0" name="products[{{$i}}][shelf_quantity]">
                                                                </div>
                                                            </div>
                            
                                                            <div class="row mt-2">
                                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                    <label>
                                                                        <input type="checkbox" class="icheck" data-checkbox="icheckbox_flat-blue"
                                                                            name="products[{{$i}}][in_warehouse]" value="1">
                                                                        موجود فى المستودع
                                                                    </label>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                    <input type="text" disabled class="counter" value="0" name="products[{{$i}}][warehouse_quantity]">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                            
                                            </div>
                                        </div>
                            
                                    </div>
                                </div>
                            </div>
                            @if (($i+1) % 3 == 0)
                                <div class="clearfix visible-lg-block"></div>
                            @endif
                            @if ($i % 2 != 0)
                                <div class="clearfix visible-md-block"></div>
                            @endif
                        @endfor
                       
                        
                    </div>
                    <div class="clearfix"></div>

                </form>
                <div class="form-group margiv-top-10 text-center">
                    <button class="btn btn-primary" onclick="DetailedInventory.fininshTask()"> انهاء </button>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>

</div>

@endsection

@section('js')
<script>
    var new_config = {
        code : '{{$code}}',
        task : '{{$task}}',
        start_time : '{{$start_time}}'
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQxy6KaWYNPIR0ymzvi5_gUIPNu7QP8VU&libraries=places&language={{App::getLocale()}}" defer></script>
<script src="{{url('public/backend/js/employee')}}/detailed_inventory_task.js" type="text/javascript"></script>
@endsection