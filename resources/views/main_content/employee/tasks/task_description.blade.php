@extends('layouts.backend')

@section('pageTitle', "وصف $task->title")

@section('breadcrumb')
<li><span>الرئيسية</span><i class="fa fa-circle"></i></li>
<li><span>وصف {{$task->title}}</span></li>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">وصف {{$task->title}}</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">

                   
                        <div class="col-md-6">
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                @for ($i = 0; $i < count($task->images); $i++)
                                    <li data-target="#myCarousel" data-slide-to="{{$i}}" class="{{ $i == 0 ? 'active' : '' }}"></li>
                                @endfor
                                </ol>
                            
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    @for ($i = 0; $i < count($task->images); $i++)
                                        <div class="item {{$i == 0 ? 'active' : ''}}">
                                            <img src="{{  \Helper\Common\imageUrl($task->images[$i]) }}" alt="task-image" style="width:100%; height:500px">
                                        </div>  
                                    @endfor
                                </div>
                            
                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portlet light bordered">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption">
                                        <i class="icon-globe font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">{{$task->title}}</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <p class="bold">
                                       {{$task->description}}
                                    </p>
                                </div>
                            </div>
                        </div>
                       
                    
                </div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>

@endsection

@section('js')

@endsection