@extends('layouts.backend')

@section('pageTitle', $task_name)

@section('breadcrumb')
<li><span>الرئيسية</span><i class="fa fa-circle"></i></li>
<li><span><a href="{{route('employee.visits.show',['code' => $code])}}">زيارة - {{$store}} - {{$code}}</a></span><i class="fa fa-circle"></i></li>
<li><span>{{$task_name}}</span></li>
@endsection

@section('content')
<div class="modal fade" id="finshTask" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="finshTaskLabel"></h4>
            </div>
            <div class="modal-body">
                <form role="form" id="finshTaskForm">
                    {{ csrf_field() }}
                    <div class="form-body">
                       <div class="form-group">
                            <label class="control-label"> ملاحظات </label>
                            <textarea type="text" rows="7" class="form-control" name="notes"></textarea>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <span class="margin-right-10 loading hide"><i class="fa fa-spin fa-spinner"></i></span>
                <button type="button" class="btn btn-info submit-form">انهاء المهمة</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">اغلاق</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">{{$task_name}}</span>
                    <span class="text-right">
                        <a target="_blank" href="{{ route('employee.visits.tasks.description',['task' => str_replace(' ','-',$task_name)]) }}" class="btn btn-info">وصف المهمة</a>
                    </span>
                </div>
               <h4 id="stopwatch" class="pull-right bold mt-3">00:00:00</h4>
            </div>
            <div class="text-right">
                <button class="btn btn-primary" onclick="OrganizingShelves.fininshTask()"> انهاء </button>
            </div>
            <div class="portlet-body">
                <form role="form" action="#" id="taskForm">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6">
                        <label class="control-label">صورة قبل الترتيب</label>
                        <div class="image_before_box">
                            <div class="snap-container"></div>
                            <img src="{{url('no-image.png')}}" height="400" style="width:100%"
                                class="image_before snap-camera-image" onclick="My.takeSnapShot(this)" />
                        </div>
                        <input type="hidden" name="image_before" id="image_before">
                        <span class="help-block"></span>
                        <input type=button value="التقط صورتك" class="take_snapshot_image_before" style="display:none;">
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label"> ملاحظات </label>
                        <textarea type="text" rows="7" class="form-control" name="notes_before"></textarea>
                    </div>

                    <div class="form-group col-md-6">
                        <label class="control-label">صورة بعد الترتيب</label>
                        <div class="image_after_box">
                            <div class="snap-container"></div>
                            <img src="{{url('no-image.png')}}" height="400"
                                class="image_after snap-camera-image" style="width:100%;" onclick="My.takeSnapShot(this)" />
                        </div>
                        <input type="hidden" name="image_after" id="image_after">
                        <span class="help-block"></span>
                        <input type=button value="التقط صورتك" class="take_snapshot_image_after" style="display:none;">
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label"> ملاحظات </label>
                        <textarea type="text" rows="7" class="form-control" name="notes_after"></textarea>
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group margiv-top-10 text-center">
                        <button class="btn green submit-form" type="submit"> اضافة </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>

</div>

@endsection

@section('js')
<script>
    var new_config = {
        code : "{{$code}}",
        task : "{{$task}}",
        start_time: "{{$start_time}}"
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQxy6KaWYNPIR0ymzvi5_gUIPNu7QP8VU&libraries=places&language={{App::getLocale()}}" defer></script>
<script src="{{url('public/backend/js/employee')}}/organizing_shelves_task.js" type="text/javascript"></script>
@endsection