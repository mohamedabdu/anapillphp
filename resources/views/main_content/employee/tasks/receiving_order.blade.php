@extends('layouts.backend')

@section('pageTitle', $task_name)

@section('breadcrumb')
<li><span>الرئيسية</span><i class="fa fa-circle"></i></li>
<li><span><a href="{{route('employee.visits.show',['code' => $code])}}">زيارة - {{$store}} - {{$code}}</a></span><i class="fa fa-circle"></i></li>
<li><span>{{$task_name}}</span></li>
@endsection

@section('content')
<div class="modal fade" id="finshTask" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="finshTaskLabel"></h4>
            </div>
            <div class="modal-body">
                <form role="form" id="finshTaskForm">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <h4>هل تريد إنهاء المهمة؟</h4>
                    </div>
                </form>

            </div>

            <div class="modal-footer">
                <span class="margin-right-10 loading hide"><i class="fa fa-spin fa-spinner"></i></span>
                <button type="button" class="btn btn-info submit-form">انهاء المهمة</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">اغلاق</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">{{$task_name}}</span>
                    <span class="text-right">
                        <a target="_blank" href="{{ route('employee.visits.tasks.description',['task' => str_replace(' ','-',$task_name)]) }}" class="btn btn-info">وصف المهمة</a>
                    </span>
                </div>
               <h4 id="stopwatch" class="pull-right bold mt-3">00:00:00</h4>
            </div>

            <div class="text-right">
                <button class="btn btn-primary" onclick="ReceivingOrder.fininshTask()">انهاء</button>
            </div>
            <div class="portlet-body">
                <form role="form" action="#" id="taskForm">
                    {{ csrf_field() }}
                    <div class="form-group col-md-3">
                        <label class="control-label"> رقم الفاتورة</label>
                        <input type="text" class="form-control" name="bill_number">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group col-md-3">
                        <label class="control-label"> تاريخ الفاتورة</label>
                        <input type="text" class="form-control picker" name="bill_date">
                        <span class="help-block"></span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                       
                        <div class="form-group col-md-6">
                            <label class="control-label">اسم المنتج</label>
                            <select  id="product" class="form-control" name="products[0][product]">
                                <option value="">اختر</option>
                                @foreach ($products as $product)
                                    <option value="{{$product->id}}">{{$product->title}}</option>
                                @endforeach
                            </select>
                            <span class="help-block"></span>
                        </div>

                        <div class="form-group col-md-3">
                            <label class="control-label"> عدد القطع</label>
                            <input type="number" class="form-control" name="products[0][pieces_number]">
                            <span class="help-block"></span>
                        </div>
                        
                        <div class="form-group col-md-3">
                            <label class="control-label"> عدد الكراتين</label>
                            <input type="number" class="form-control" name="products[0][cartons_number]">
                            <span class="help-block"></span>
                        </div>

                    </div>

                    <div class="row" id="products-container">

                    </div>

                    <div class="col-xs-12 mb-3">
                        <button class="btn btn-info" id="add-product" type="button"><i class="fa fa-plus-square-o"></i> اضافة منتج </button>
                    </div>
                    
                    <div class="form-group col-md-6">
                        <label class="control-label"> قيمة الفاتورة</label>
                        <input type="number" class="form-control" name="bill_total">
                        <span class="help-block"></span>
                    </div>

                    <div class="form-group col-md-6">
                        <label class="control-label"> اسم السائق</label>
                        <input type="text" class="form-control" name="driver_name">
                        <span class="help-block"></span>
                    </div>

                    <div class="form-group col-md-4">
                        <label class="control-label"> صورة الفاتورة </label>
                        <div class="bill_image_box">
                            <div class="snap-container"></div>
                            <img src="{{url('no-image.png')}}" style="width:100%" height="400" class="bill_image snap-camera-image"
                                onclick="My.takeSnapShot(this)" />
                        </div>
                        <input type="hidden" name="bill_image" id="bill_image">
                        <span class="help-block"></span>
                        <input type=button value="التقط صورتك" class="take_snapshot_bill_image" style="display:none;">
                    </div>

                    <div class="clearfix"></div>
                    @for ($i = 0; $i < 3; $i++)
                        <div class="form-group col-md-4">
                            <label class="control-label"> صورة </label>
                            <div class="image_{{$i}}_box">
                                <div class="snap-container"></div>
                                <img src="{{url('no-image.png')}}" style="width:100%" height="400" class="image_{{$i}} snap-camera-image"
                                    onclick="My.takeSnapShot(this)" />
                            </div>
                            <input type="hidden" name="images[{{$i}}]" id="image_{{$i}}">
                            <span class="help-block"></span>
                            <input type=button value="التقط صورتك" class="take_snapshot_image_{{$i}}" style="display:none;">
                        </div>
                    @endfor

                    <div class="clearfix"></div>
                    <div class="form-group margiv-top-10 text-center">
                        <button class="btn green submit-form" type="submit"> اضافة </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>

</div>

@endsection

@section('js')
<script>
    var new_config = {
        code : '{{$code}}',
        task : '{{$task}}',
        start_time : '{{$start_time}}',
        products: '{!! json_encode($products->toArray()) !!}'
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQxy6KaWYNPIR0ymzvi5_gUIPNu7QP8VU&libraries=places&language={{App::getLocale()}}" defer></script>
<script src="{{url('public/backend/js/employee')}}/receiving_order_task.js" type="text/javascript"></script>
@endsection