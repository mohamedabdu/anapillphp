@extends('layouts.backend')

@section('pageTitle', 'البحث')

@section('breadcrumb')
<li><span> البحث</span></li>
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">البحث عن الزيارات</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <form role="form" action="#" id="filterForm">
                        {{ csrf_field() }}
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label"> التاريخ </label>
                                <input type="text" class="form-control picker" name="date" id="date">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">المدينة</label>
                                <select name="city" id="city" class="form-control">
                                    <option value="">اختر</option>
                                    @foreach ($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->title }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">التصنيف</label>
                                <select name="category" id="category" class="form-control">
                                    <option value="">اختر</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->title }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label"> الزيارة</label>
                                <select name="code" id="code" class="form-control">
                                    <option value="">اختر</option>
                                    @foreach ($visits as $visit)
                                        <option value="{{ $visit->code }}">{{ $visit->code.' - '.$visit->store }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    
                        
                        <div class="col-xs-12">
                            <div class="form-group text-center">
                                <button class="btn green submit-form" type="submit"> بحث </button>
                            </div>
                        </div>
                    </form>
                </div>
              
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-xs-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered" style="min-height: 400px;">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase"> نتائج البحث </span>
                </div>
            </div>
            <div class="portlet-body">
                <!--BEGIN TABS-->
                <ul class="feeds" id="visits-list" style="display: none;">
                   
                </ul>
                <h3 class="text-center text-default" id="no-results">لا يوجد نتائج ...</h3>
                <!--END TABS-->
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-md-6 col-md-offset-3">
        <button class="btn btn-primary btn-block" id="load-more-button" style="display: none"> المزيد </button>
    </div>
</div>

@endsection

@section('js')
<script>
    var new_config = {
        limit : "{{ $limit }}"
    }
</script>
<script src="{{url('public/backend/js/employee')}}/search.js" type="text/javascript"></script>
@endsection