@extends('layouts.backend')

@section('pageTitle', 'تقرير مساحات')

@section('breadcrumb')
<li><span>الرئيسية</span><i class="fa fa-circle"></i></li>
<li><span>تقارير منافسين</span></li>
<li><span>تقرير مساحات</span></li>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase"> مساحات</span>
                </div>
            </div>
            <div class="portlet-body">
                <form role="form" action="#" id="reportForm">
                    {{ csrf_field() }}
                    
                    <div class="form-group col-md-6">
                        <label class="control-label">اسم المحل</label>
                        <select name="store" id="store" class="form-control">
                            <option value="">اختر</option>
                            @foreach ($stores as $store)
                                <option value="{{ $store->id }}">{{ $store->title }}</option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label">اسم المنافس</label>
                        <select name="competitor" id="competitor" class="form-control">
                            <option value="">اختر</option>
                            @foreach ($competitors as $competitor)
                            <option value="{{ $competitor->id }}">{{ $competitor->title }}</option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>
                    </div>

                    <div class="form-group col-md-6">
                        <label class="control-label"> التصنيف</label>
                        <select name="categories[0][category]" class="form-control store-categories">
                            <option value="">اختر</option>
                        </select>
                        <span class="help-block"></span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                        <label class="control-label"> مساحة المنافس</label>
                        <div class="clearfix"></div>
                        <div class="form-group col-md-3">
                            <input type="number" placeholder="العرض بالسم" class="form-control" name="categories[0][width_of_competitor]">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group col-md-3">
                            <input type="number" placeholder="الطول (عدد الشلفات)" class="form-control" name="categories[0][height_of_competitor]">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label class="control-label"> مساحة الشركة</label>
                        <div class="clearfix"></div>
                        <div class="form-group col-md-3">
                            <input type="number" placeholder="العرض بالسم" class="form-control" name="categories[0][width_of_organization]">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group col-md-3">
                            <input type="number" placeholder="الطول (عدد الشلفات)" class="form-control" name="categories[0][height_of_organization]">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div id="categories-container">
                    
                    </div>
                    
                    <div class="col-xs-12 mb-3">
                        <button class="btn btn-info" id="add-category" type="button"><i class="fa fa-plus-square-o"></i>
                            اضافة تصنيف
                        </button>
                    </div>
                   <div class="clearfix"></div>
                    <div class="form-group margiv-top-10 text-center">
                        <button class="btn green submit-form" type="submit"> ارسال </button>
                    </div>
                </form>
            </div>
    </div>
    <!-- END PORTLET-->
</div>

</div>

@endsection
<script>
    var new_config = {
        token : "{{ csrf_token() }}"
    }
</script>
@section('js')
<script src="{{url('public/backend/js/employee')}}/areas_report.js" type="text/javascript"></script>
@endsection