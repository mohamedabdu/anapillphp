@extends('layouts.backend')

@section('pageTitle', 'تقرير الأسعار المخفضة')

@section('breadcrumb')
<li><span>الرئيسية</span><i class="fa fa-circle"></i></li>
<li><span>تقارير منافسين</span></li>
<li><span>تقرير الأسعار المخفضة</span></li>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">الأسعار المخفضة</span>
                </div>
            </div>
            <div class="portlet-body">
                <form role="form" action="#" id="reportForm">
                    {{ csrf_field() }}
                   
                    <div class="form-group col-md-6">
                        <label class="control-label">اسم المحل</label>
                        <select name="store" id="store" class="form-control">
                            <option value="">اختر</option>
                            @foreach ($stores as $store)
                            <option value="{{ $store->id }}">{{ $store->title }}</option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label">اسم المنافس</label>
                        <select name="competitor" id="competitor" class="form-control">
                            <option value="">اختر</option>
                            @foreach ($competitors as $competitor)
                            <option value="{{ $competitor->id }}">{{ $competitor->title }}</option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>
                    </div>

                   <div class="single-product">
                        <div class="col-md-6">
                            <label>
                                <input type="checkbox" class="icheck" data-checkbox="icheckbox_flat-blue" value="1">
                                المنتج المنافس غير مدرج
                            </label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group col-md-6 competitor-product-list">
                            <label class="control-label">المنتج المنافس</label>
                            <select name="products[0][competitor_product]" class="form-control competitor-products">
                                <option value="">اختر</option>
                            </select>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group col-md-6 competitor-product-name" style="display: none">
                            <label class="control-label">اسم المنتج المنافس</label>
                            <input type="text" class="form-control" name="products[0][competitor_product_name]">
                            <span class="help-block"></span>
                        </div>
                        
                        <div class="form-group col-md-6">
                            <label class="control-label"> نوع العرض</label>
                            <select name="products[0][promo]" class="form-control promos">
                                <option value="">اختر</option>
                                @foreach ($promotions as $promotion)
                                    <option value="{{ $promotion->id }}">{{ $promotion->title }}</option>
                                @endforeach
                            </select>
                            <span class="help-block"></span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group col-md-6">
                            <label class="control-label"> السعر الحالي</label>
                            <input type="number" class="form-control" name="products[0][current_price]">
                            <span class="help-block"></span>
                        </div>
                        
                        <div class="form-group col-md-6">
                            <label class="control-label"> سعر العرض</label>
                            <input type="number" class="form-control" name="products[0][offer_price]">
                            <span class="help-block"></span>
                        </div>
                   </div>

                    <div class="clearfix"></div>
                    <div id="products-container">
                    
                    </div>
                    
                    <div class="col-xs-12 mb-3">
                        <button class="btn btn-info" id="add-product" type="button"><i class="fa fa-plus-square-o"></i>
                            اضافة منتج
                        </button>
                    </div>


                    <div class="clearfix"></div>
                    <div class="form-group margiv-top-10 text-center">
                        <button class="btn green submit-form" type="submit"> ارسال </button>
                    </div>
            </form>
        </div>
    </div>
    <!-- END PORTLET-->
</div>

</div>

@endsection

@section('js')
<script>
    var new_config = {
        promos: '{!! json_encode($promotions->toArray()) !!}',
        token : "{{ csrf_token() }}"
    }
</script>
<script src="{{url('public/backend/js/employee')}}/reduced_prices_report.js" type="text/javascript"></script>
@endsection