@extends('layouts.backend')

@section('pageTitle', 'تقرير يدوي')

@section('breadcrumb')
<li><span>الرئيسية</span><i class="fa fa-circle"></i></li>
<li><span>تقرير يدوي</span></li>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">تقرير يدوي</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <form role="form" action="#" id="reportForm">
                        {{ csrf_field() }}
                        <div class="form-group col-md-6">
                            <label class="control-label">اسم المحل</label>
                            <select name="store" id="store" class="form-control">
                                <option value="">اختر</option>
                                @foreach ($stores as $store)
                                <option value="{{ $store->id }}">{{ $store->title }}</option>
                                @endforeach
                            </select>
                            <span class="help-block"></span>
                        </div>
                    
                        <div class="form-group col-md-6">
                            <label class="control-label"> عنوان التقرير</label>
                            <input type="text" class="form-control" name="title">
                            <span class="help-block"></span>
                        </div>
                    
                    
                        <div class="form-group col-md-12">
                            <label class="control-label"> ملاحظات </label>
                            <textarea type="text" rows="7" class="form-control" name="notes"></textarea>
                        </div>
                        <div class="clearfix"></div>
                    
                        @for ($i = 0; $i < 3; $i++) 
                        <div class="form-group col-md-4">
                            <label class="control-label"> صورة </label>
                            <div class="image_{{$i}}_box">
                                <div class="snap-container"></div>
                                <img src="{{url('no-image.png')}}" style="width:100%" height="400" class="image_{{$i}} snap-camera-image"
                                    onclick="My.takeSnapShot(this)" />
                            </div>
                            <input type="hidden" name="images[{{$i}}]" id="image_{{$i}}">
                            <span class="help-block"></span>
                            <input type=button value="التقط صورتك" class="take_snapshot_image_{{$i}}" style="display:none;">
                        </div>
                            @endfor
                        <div class="clearfix"></div>
                    
                        <div class="form-group margiv-top-10 text-center">
                            <button class="btn green submit-form" type="submit"> ارسال </button>
                        </div>
                    </form>
                </div>
        </div>
    </div>
    <!-- END PORTLET-->
</div>

</div>

@endsection

@section('js')
<script src="{{url('public/backend/js/employee')}}/manual_report.js" type="text/javascript"></script>
@endsection