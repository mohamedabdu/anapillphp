@extends('layouts.backend')

@section('pageTitle', 'لوحة التحكم')

@section('breadcrumb')
<li><span>لوحة التحكم</span></li>
@endsection

@section('content')

<div class="row">
    <div class="col-lg-6 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">اعمال مجدولة</span>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_1_1" class="active" data-toggle="tab"> قائمة  </a>
                    </li>
                    <li>
                        <a href="#tab_1_2" data-toggle="tab"> الخريطة </a>
                    </li>
                </ul>
            </div>
            <div class="portlet-body">
                <!--BEGIN TABS-->
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1_1">
                        <div class="scroller" style="height: 300px;">
                            @if ($scheduleVisits->count() > 0)
                            <ul class="feeds">
                                @foreach ($scheduleVisits as $scheduleVisit)
                                    <li>
                                       <a href="{{ route('employee.visits.show',['code' => $scheduleVisit->code]) }}">
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col2">
                                                        <div class="desc">
                                                            {{$scheduleVisit->store .' - '.$scheduleVisit->code}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date">
                                                    <span class="label label-sm label-default">
                                                        <i class="fa fa-angle-left"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                            @else  
                                <h3 class="text-center text-default">لا يوجد زيارات..</h3>
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_1_2">
                        <div id="scheduledTasksMap" style="height: 400px; width:100%;"></div>
                        <div id="infowindow-content">
                            <span id="place-name" class="title"></span><br>
                            Place ID <span id="place-id"></span><br>
                            <span id="place-address"></span>
                        </div>
                    </div>
                </div>
                <!--END TABS-->
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-lg-6 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase"> اعمال مسندة </span>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_2_1" class="active" data-toggle="tab"> قائمة </a>
                    </li>
                    <li>
                        <a href="#tab_2_2" data-toggle="tab"> الخريطة </a>
                    </li>
                </ul>
            </div>
            <div class="portlet-body">
                <!--BEGIN TABS-->
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_2_1">
                        <div class="scroller" style="height: 300px;">
                            @if ($normalVisits->count() > 0)
                            <ul class="feeds">
                                @foreach ($normalVisits as $normalVisit)
                                <li>
                                    <a href="{{ route('employee.visits.show',['code' => $normalVisit->code]) }}">
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col2">
                                                    <div class="desc">
                                                        {{$normalVisit->store.' - '.$normalVisit->code}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date">
                                                <span class="label label-sm label-default">
                                                    <i class="fa fa-angle-left"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                            @else
                            <h3 class="text-center text-default">لا يوجد زيارات..</h3>
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_2_2">
                        <div id="normalTasksMap" style="height: 400px; width:100%;"></div>
                        <div id="infowindow-content">
                            <span id="place-name" class="title"></span><br>
                            Place ID <span id="place-id"></span><br>
                            <span id="place-address"></span>
                        </div>
                    </div>
                </div>
                <!--END TABS-->
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>

@endsection

@section('js')
<script>
    var new_config = {
        scheduleVisits :'{!! json_encode($scheduleVisits)  !!}',
        normalVisits:'{!! json_encode($normalVisits) !!}',
    }
</script>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQxy6KaWYNPIR0ymzvi5_gUIPNu7QP8VU&libraries=places&language={{App::getLocale()}}"
    defer></script>
<script src="{{url('public/backend/js/employee')}}/employee_dashboard.js" type="text/javascript"></script>
@endsection