@extends('layouts.backend')

@section('pageTitle', 'تقرير اليوم')

@section('breadcrumb')
<li><span>لوحة التحكم</span><i class="fa fa-circle"></i></li>
<li><span>تقرير اليوم</span></li>
@endsection

@section('content')
<div class="modal fade" id="finishAttendance" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="finishAttendanceLabel"></h4>
            </div>
            <div class="modal-body">
                <form role="form" id="finishAttendanceForm">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <h3 id="finish-attendance-message"></h3>
                        <div class="form-group" style="display: none;" id="termination-reason">
                            <label class="control-label"> سبب الإنهاء </label>
                            <textarea type="text" rows="7" class="form-control" name="termination_reason"></textarea>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </form>

            </div>

            <div class="modal-footer">
                <span class="margin-right-10 loading hide"><i class="fa fa-spin fa-spinner"></i></span>
                <button type="button" class="btn btn-info submit-form">تأكيد</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">اغلاق</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="col-lg-6 col-xs-12 col-centered text-center">
            <input type="text" value="{{$completion_percentage}}" class="completed" data-linecap=round>
            <div class="portlet light bordered">
                <div class="portlet-body">
                   <div class="row">
                        <div class="col-md-4 text-center">
                            <span>وقت البدء </span>
                            <h4 class="bold">{{$attendance_start_time}}</h4>
                        </div>
                        <div class="col-md-4 text-center">
                            <span>وقت الإنهاء </span>
                            <h4 class="bold">{{$attendance_end_time}}</h4>
                        </div>
                        <div class="col-md-4 text-center">
                            <span>الوقت الكلي </span>
                            <h4 class="bold">{{$total_time}}</h4>
                        </div>
                   </div>
                </div>
            </div>
        </div>
        
        <!-- END PORTLET-->
    </div>
    <div class="col-lg-6 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">العمل المنجز</span>
                </div>
                
            </div>
            <div class="portlet-body">
              <div class="row">
                <ul class="feeds col-md-9">
                    <li class="">
                        <div class="col1">
                            <div class="cont">
                                <div class="cont-col2">
                                    <div class="desc">
                                        اعمال مجدولة
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col2">
                            <div class="date bold" style="color: #000000">
                                {{$finished_scheduled_visits_count}}
                            </div>
                        </div>
                    </li>
                    <li class="">
                        <div class="col1">
                            <div class="cont">
                                <div class="cont-col2">
                                    <div class="desc">
                                        اعمال مسندة
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col2">
                            <div class="date bold" style="color: #000000">
                                {{$finished_normal_visits_count}}
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-lg-6 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">العمل الغير منجز</span>
                </div>
            </div>
            <div class="portlet-body">
                <!--BEGIN TABS-->
                <div class="row">
                   <ul class="feeds col-md-9">
                        <li class="">
                            <div class="col1">
                                <div class="cont">
                                    <div class="cont-col2">
                                        <div class="desc">
                                            اعمال مجدولة
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col2">
                                <div class="date bold" style="color: #000000">
                                    {{$unfinished_scheduled_visits_count}}
                                </div>
                            </div>
                        </li>
                        <li class="">
                            <div class="col1">
                                <div class="cont">
                                    <div class="cont-col2">
                                        <div class="desc">
                                            اعمال مسندة
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col2">
                                <div class="date bold" style="color: #000000">
                                    {{$unfinished_normal_visits_count}}
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
        <!-- END PORTLET-->
      
    </div>
    
</div>
<div class="col-lg-6 col-xs-12 col-centered text-center">
    <button class="btn btn-primary btn-block" type="button" onclick="DailyReport.finishAttendance()">إنهاء الدوام</button>
</div>
@endsection

@section('js')
<script>
    var new_config = {
        completion:'{{ $completion_percentage }}'
    }
</script>
<script src="{{url('public/backend/js/employee')}}/daily_report.js" type="text/javascript"></script>

@endsection