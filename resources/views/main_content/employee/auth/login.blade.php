@extends('layouts.employee_auth')
@section('title','تسجيل دخول')
@section('content')
<form class="form-horizontal" action="" id="loginForm" method="POST" novalidate="novalidate" style="width: 100%">
    <div class="form-wizard">
        <div class="form-body">
            <ul class="nav nav-pills nav-justified steps">
                <li class="active">
                    <a href="#tab1" data-toggle="tab" class="step active" aria-expanded="true">
                        <span class="number"> ١ </span>
                        <span class="desc">
                            <i class="fa fa-check"></i> التحقق </span>
                    </a>
                </li>
                <li>
                    <a href="#tab2" data-toggle="tab" class="step">
                        <span class="number"> ٢ </span>
                        <span class="desc">
                            <i class="fa fa-check"></i> تسجيل الحضور </span>
                    </a>
                </li>
                <li>
                    <a href="#tab3" data-toggle="tab" class="step">
                        <span class="number"> ٣ </span>
                        <span class="desc">
                            <i class="fa fa-check"></i> صورة الموظف </span>
                    </a>
                </li>
                <li>
                    <a href="#tab4" data-toggle="tab" class="step">
                        <span class="number"> ٤ </span>
                        <span class="desc">
                            <i class="fa fa-check"></i> بدء الدوام </span>
                    </a>
                </li>
            </ul>
            <div id="bar" class="progress progress-striped" role="progressbar">
                <div class="progress-bar progress-bar-success" style="width: 25%;"> </div>
            </div>

            <div class="tab-content">
                <div class="alert alert-danger display-hide" style="display: none;">
                    <button class="close" data-close="alert"></button>
                    <span></span>
                </div>
                {{ csrf_field() }}
                <div class="tab-pane" id="tab1">
                    <h3 class="block">أدخل تفاصيل حسابك</h3>
                    <div class="form-group">
                        <label class="control-label col-md-3">رقم الهاتف
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="number" class="form-control" name="mobile">
                            <span class="help-block"> ادخل رقم هاتفك </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">كلمة المرور
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="password" class="form-control" name="password" id="submit_form_password">
                            <span class="help-block"> أدخل كلمة مرورك </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">
                        </label>
                        <div class="col-md-4">
                            <a href="{{route('employee.password.reset')}}">هل نسيت كلمة المرور؟</a>
                        </div>
                    </div>
                   
                </div>
                <div class="tab-pane" id="tab2">
                    <h3 class="block"> صباح الخير <span id="name"></span></h3>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h4>
                                من الجيد رؤيتك مجددا.نتمني لك يوماً ملئ بالنشاط والامل
                            </h4>
                            <h4>
                                قم بتسجيل حضورك وابدأ جدول اعمالك
                            </h4>
                        </div>

                    </div>
                </div>
                <div class="tab-pane" id="tab3">
                    <h3 class="block">قم بإلتقاط صورة لك</h3>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <div id="my_camera"></div>
                            <br />
                            <input type=button value="التقط صورتك" id="take_snapshot">
                            <input type="hidden" name="image" class="image-tag">
                            <span class="help-block"> التقط صورة لك </span>
                        </div>
                        <div class="col-md-6">
                            <div class="resulted_image">صورتك سوف تظهر هنا</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <input type="hidden" name="lat" id="lat">
                            <input type="hidden" name="lng" id="lng">
                            <span class="help-block"></span>
                        </div>
                    </div>

                </div>

                <div class="tab-pane" id="tab4">
                    <h3 class="block">قم بتأكيد حضورك</h3>

                    <h4 class="form-section">بياناتك</h4>
                    <div class="form-group">
                        <label class="control-label col-md-3">صورتك</label>
                        <div class="col-md-4">
                            <div class="resulted_image"></div>
                        </div>
                    </div>
                    <div class="form-group text-center col-md-12">
                        <h4 id="address"></h4>
                        <h4 id="date"></h4>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <a href="javascript:;" onclick="Login.nextPrev(-1)" class="btn default button-previous"
                        style="display: none;">
                        <i class="fa fa-angle-right"></i> الرجوع </a>
                    <a href="javascript:;" onclick="Login.nextPrev(1)" class="btn btn-outline green button-next">
                        استمرار
                        <i class="fa fa-angle-left"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- END LOGIN FORM -->
@endsection

@section('scripts')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQxy6KaWYNPIR0ymzvi5_gUIPNu7QP8VU&libraries=places&language={{App::getLocale()}}"></script>
<script src="{{url('public/backend/js/employee')}}/login.js" type="text/javascript"></script>
@endsection