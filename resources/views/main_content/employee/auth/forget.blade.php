@extends('layouts.employee_auth')
@section('title','تغيير كلمة المرور')
@section('content')
<form class="form-horizontal" action="" id="forgetPasswordForm" method="POST" novalidate="novalidate" style="width: 100%">
    <div class="form-wizard">
        <div class="form-body">
            <ul class="nav nav-pills nav-justified steps">
                <li class="active">
                    <a href="#tab1" data-toggle="tab" class="step active" aria-expanded="true">
                        <span class="number"> ١ </span>
                        <span class="desc">
                            <i class="fa fa-check"></i> التحقق </span>
                    </a>
                </li>
                <li>
                    <a href="#tab2" data-toggle="tab" class="step">
                        <span class="number"> ٢ </span>
                        <span class="desc">
                            <i class="fa fa-check"></i> كود التفعيل </span>
                    </a>
                </li>
                <li>
                    <a href="#tab3" data-toggle="tab" class="step">
                        <span class="number"> ٣ </span>
                        <span class="desc">
                            <i class="fa fa-check"></i> إعادة تعيين كلمة المرور </span>
                    </a>
                </li>
            </ul>
            <div id="bar" class="progress progress-striped" role="progressbar">
                <div class="progress-bar progress-bar-success" style="width: 25%;"> </div>
            </div>

            <div class="tab-content">
                <div class="alert alert-danger display-hide" style="display: none;">
                    <button class="close" data-close="alert"></button>
                    <span></span>
                </div>
                <div class="alert alert-success display-hide" style="display: none;">
                    <button class="close" data-close="alert"></button>
                    <span></span>
                </div>
                {{ csrf_field() }}
                <div class="tab-pane" id="tab1">
                    <h3 class="block">أدخل تفاصيل حسابك</h3>
                    <div class="form-group">
                        <label class="control-label col-md-3">رقم الهاتف
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="number" class="form-control" name="mobile">
                            <span class="help-block"> ادخل رقم هاتفك </span>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab2">
                    
                    <h3 class="block"> سوف يتم ارسال كود التفعيل فى رسالة <span id="name"></span></h3>
                    <div class="text-center">
                        <h4>
                            قم بإدخال كود التفعيل
                        </h4>
                    </div>
                    <div class="text-center">
                        <div class="row">
                            <div class="col-md-3">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" maxlength="1" size="1" min="0" max="9" pattern="[0-9]{1}" class="code form-control text-center" name="code[1]">
                                <input type="text" maxlength="1" size="1" min="0" max="9" pattern="[0-9]{1}" class="code form-control text-center" name="code[2]">
                                <input type="text" maxlength="1" size="1" min="0" max="9" pattern="[0-9]{1}" class="code form-control text-center" name="code[3]">
                                <input type="text" maxlength="1" size="1" min="0" max="9" pattern="[0-9]{1}" class="code form-control text-center" name="code[4]">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>
                        
                        <div>
                            <br>
                            <a href="javascript:;" id="resendCode">
                                إعادة إرسال كود التفعيل
                            </a>
                        </div>
                       
                    </div>
                        
                </div>
                <div class="tab-pane" id="tab3">
                    <h3 class="block">أدخل كلمة المرور الجديدة</h3>
                    <div class="form-group">
                        <label class="control-label col-md-3"> كلمة المرور الجديدة
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="password" class="form-control" name="password" id="password">
                            <span class="help-block"> ادخل كلمة المرور الجديدة </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"> تأكيد كلمة المرور
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="password" class="form-control" name="password_confirmation">
                            <span class="help-block"> أعد إدخال كلمة المرور </span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <a href="javascript:;" onclick="ForgetPassword.nextPrev(-1)" class="btn default button-previous"
                        style="display: none;">
                        <i class="fa fa-angle-right"></i> الرجوع </a>
                    <a href="javascript:;" onclick="ForgetPassword.nextPrev(1)" class="btn btn-outline green button-next">
                        استمرار
                        <i class="fa fa-angle-left"></i>
                    </a>
                    
                </div>
            </div>
        </div>
    </div>
</form>
<!-- END LOGIN FORM -->
@endsection

@section('scripts')
<script src="{{url('public/backend/js/employee')}}/forget_password.js" type="text/javascript"></script>
@endsection