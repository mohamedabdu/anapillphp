@extends('layouts.backend')

@section('pageTitle',_lang('app.edit_retrieval_reason'))

@section('breadcrumb')
<li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>
<li><a href="{{route('retrieval_reasons.index')}}">{{_lang('app.retrieval_reasons')}}</a> <i class="fa fa-circle"></i></li>
<li><span>{{_lang('app.edit')}}</span></li>
@endsection


@section('js')
<script src="{{url('public/backend/js')}}/retrieval_reasons.js" type="text/javascript"></script>
@endsection
@section('content')
<form role="form"  id="addEditRetrievalReasonsForm" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.title') }}</h3>
        </div>
        <div class="panel-body">
            <div class="form-body">
                <input type="hidden" name="id" id="id" value="{{ en_de_crypt($retrieval_reason->id) }}">
                @foreach ($languages as $key => $value)
                <div class="form-group form-md-line-input col-md-6">
                    <input type="text" class="form-control" id="title[{{ $key }}]" name="title[{{ $key }}]" value="{{ $translations["$key"]->title }}">
                    <label for="title">{{_lang('app.title') }} {{ _lang('app.'.$value.'') }}</label>
                    <span class="help-block"></span>
                </div>
                @endforeach
              
               
            </div>
        </div>
       

    </div>

    <div class="panel panel-default">
       
        <div class="panel-body">
            <div class="form-body">
                <div class="form-group form-md-line-input col-md-3">
                    <input type="number" class="form-control" id="this_order" name="this_order" value="{{ $retrieval_reason->this_order }}">
                    <label for="this_order">{{_lang('app.this_order') }}</label>
                    <span class="help-block"></span>
                </div>
    
                <div class="form-group form-md-line-input col-md-3">
                    <select class="form-control" name="active" id="active">
                        <option value="0" {{$retrieval_reason->active == 0 ? 'selected' : ''}}>{{_lang('app.deactivated')}}</option>
                        <option value="1" {{$retrieval_reason->active == 1 ? 'selected' : ''}}>{{_lang('app.activated')}}</option>
                    </select>
                    <label for="status">{{_lang('app.status')}}</label>
                    <span class="help-block"></span>
                </div>
    
            </div>
        </div>
    
        <div class="panel-footer text-center">
            <button type="button" class="btn btn-info submit-form">{{_lang('app.save') }}</button>
        </div>
    
    
    </div>



</form>
<script>
var new_lang = {

};
var new_config = {
   
};

</script>
@endsection