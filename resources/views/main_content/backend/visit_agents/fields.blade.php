 <div class="form-body">
<!-- Visit Id Field -->
     <div class="form-group row">
     {!! Form::label('visit_id', 'Visit Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('visit_id', null, ['class' => 'form-control','id'=>"visit_id","required"]) !!}
    </div>
 </div>

<!-- Agent Id Field -->
     <div class="form-group row">
     {!! Form::label('agent_id', 'Agent Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('agent_id', null, ['class' => 'form-control','id'=>"agent_id","required"]) !!}
    </div>
 </div>

<!-- Status Field -->
     <div class="form-group row">
     {!! Form::label('status', 'Status:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('status', null, ['class' => 'form-control','id'=>"status","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('visitAgents.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
