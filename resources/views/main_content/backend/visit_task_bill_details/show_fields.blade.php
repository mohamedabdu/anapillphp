<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $visitTaskBillDetail->id !!}</p>
</div>

<!-- Visit Task Detail Id Field -->
<div class="form-group">
    {!! Form::label('visit_task_detail_id', 'Visit Task Detail Id:') !!}
    <p>{!! $visitTaskBillDetail->visit_task_detail_id !!}</p>
</div>

<!-- Bill Id Field -->
<div class="form-group">
    {!! Form::label('bill_id', 'Bill Id:') !!}
    <p>{!! $visitTaskBillDetail->bill_id !!}</p>
</div>

<!-- Bill Date Field -->
<div class="form-group">
    {!! Form::label('bill_date', 'Bill Date:') !!}
    <p>{!! $visitTaskBillDetail->bill_date !!}</p>
</div>

<!-- Driver Name Field -->
<div class="form-group">
    {!! Form::label('driver_name', 'Driver Name:') !!}
    <p>{!! $visitTaskBillDetail->driver_name !!}</p>
</div>

<!-- Bill Image Field -->
<div class="form-group">
    {!! Form::label('bill_image', 'Bill Image:') !!}
    <p>{!! $visitTaskBillDetail->bill_image !!}</p>
</div>

<!-- Bill Total Field -->
<div class="form-group">
    {!! Form::label('bill_total', 'Bill Total:') !!}
    <p>{!! $visitTaskBillDetail->bill_total !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $visitTaskBillDetail->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $visitTaskBillDetail->updated_at !!}</p>
</div>

