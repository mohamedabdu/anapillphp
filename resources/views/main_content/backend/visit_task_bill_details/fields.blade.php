 <div class="form-body">
<!-- Visit Task Detail Id Field -->
     <div class="form-group row">
     {!! Form::label('visit_task_detail_id', 'Visit Task Detail Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('visit_task_detail_id', null, ['class' => 'form-control','id'=>"visit_task_detail_id","required"]) !!}
    </div>
 </div>

<!-- Bill Id Field -->
     <div class="form-group row">
     {!! Form::label('bill_id', 'Bill Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('bill_id', null, ['class' => 'form-control','id'=>"bill_id","required"]) !!}
    </div>
 </div>

<!-- Bill Date Field -->
     <div class="form-group row">
     {!! Form::label('bill_date', 'Bill Date:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('bill_date', null, ['class' => 'form-control','id'=>"bill_date","required"]) !!}
    </div>
 </div>

<!-- Driver Name Field -->
     <div class="form-group row">
     {!! Form::label('driver_name', 'Driver Name:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('driver_name', null, ['class' => 'form-control','id'=>"driver_name","required"]) !!}
    </div>
 </div>

<!-- Bill Image Field -->
     <div class="form-group row">
     {!! Form::label('bill_image', 'Bill Image:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('bill_image', null, ['class' => 'form-control','id'=>"bill_image","required"]) !!}
    </div>
 </div>

<!-- Bill Total Field -->
     <div class="form-group row">
     {!! Form::label('bill_total', 'Bill Total:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('bill_total', null, ['class' => 'form-control','id'=>"bill_total","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('visitTaskBillDetails.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
