@extends('layouts.app')

@section('content')


  <div class="container-fluid">

        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9">
                <h4 class="page-title">Permission Roles</h4>
            </div>
        </div>
        <!-- End Breadcrumb-->

        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        @include('permission_roles.show_fields')
                        <a href="{!! route('permissionRoles.index') !!}" class="btn btn-light waves-effect waves-light m-1">رجوع</a>

                    </div>
                </div>

            </div>
        </div>

    </div>


@endsection


