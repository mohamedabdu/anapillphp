
@extends('layouts.app')

@section('content')

 <div class="container-fluid">

            <!-- Breadcrumb-->
            <div class="row pt-2 pb-2">
                <div class="col-sm-9">
                    <h4 class="page-title">Permission Roles</h4>
                </div>
            </div>
            <!-- End Breadcrumb-->

            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">

                           @include('adminlte-templates::common.errors')

                           {!! Form::open(['route' => 'permissionRoles.store','files' => true,"class"=>"floating-labels"]) !!}
                            <input type="hidden" name="role_id" value="{{$role_id}}">
                           @include('permission_roles.fields')

                           {!! Form::close() !!}

                        </div>
                    </div>

                </div>
            </div>

        </div>


@endsection


