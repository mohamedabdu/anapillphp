 <div class="form-body">
<!-- Role Id Field -->
     {{--<div class="form-group row">--}}
     {{--{!! Form::label('role_id', 'Role Id:',['class' => 'col-sm-2 col-form-label']) !!}--}}
    {{--<div class="col-sm-10">--}}
    {{--{!! Form::text('role_id', null, ['class' => 'form-control','id'=>"role_id","required"]) !!}--}}
    {{--</div>--}}
 {{--</div>--}}

<!-- Permission Id Field -->
     <div class="form-group row">
     {!! Form::label('permission_id', 'الصلاحية',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        @if(!isset($permissionRole))
    <select class="form-control" name="permissions[]" multiple>
        @foreach(\App\Models\Permission::all() as $value)
            <option value="{{$value->id}}">{{$value->permission }}</option>
            @endforeach
    </select>
            @else
            <select class="form-control" name="permissions_id">
                @foreach(\App\Models\Permission::all() as $value)
                    <option value="{{$value->id}}" @if($permissionRole->permission_id == $value->id) selected @endif>{{$value->permission}}</option>
                @endforeach
            </select>
        @endif
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('permissionRoles.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
