@php /** @var \App\Models\Store $store **/  @endphp


<!-- Logo Field -->
<div class="form-group">
    {!! Form::label('logo', 'الشعار') !!}<br>
    <img src="{{\Helper\Common\imageUrl($store->logo)}}" class="img-circle" width="100" height="100">
</div>


<!-- Title Ar Field -->
<div class="form-group">
    {!! Form::label('title_ar', 'اسم المحل') !!}
    <p>{!! $store->title_ar !!}</p>
</div>

<!-- Title En Field -->
<div class="form-group">
    {!! Form::label('title_en', 'اسم المحل انجليزي') !!}
    <p>{!! $store->title_en !!}</p>
</div>

<!-- Description Ar Field -->
<div class="form-group">
    {!! Form::label('description_ar', 'وصف المحل') !!}
    <p>{!! $store->description_ar !!}</p>
</div>

<!-- Description En Field -->
<div class="form-group">
    {!! Form::label('description_en', 'وصف المحل انجليزي') !!}
    <p>{!! $store->description_en !!}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'القسم الرئيسي') !!}
    <p>
    @if($store->category != null)
        {{$store->category->title_ar}}
        @endif
    </p>
</div>

<!-- Country Id Field -->
<div class="form-group">
    {!! Form::label('country_id', 'الدولة') !!}
    <p>
        @if($store->country != null)
            {{$store->country->title_ar}}
        @endif
    </p>
</div>

<!-- Region Id Field -->
<div class="form-group">
    {!! Form::label('region_id', 'المنطقة') !!}
    <p>
        @if($store->region != null)
            {{$store->region->title_ar}}
        @endif
    </p>
</div>

<!-- City Id Field -->
<div class="form-group">
    {!! Form::label('city_id', 'المدينة') !!}
    <p>
        @if($store->city != null)
            {{$store->city->title_ar}}
        @endif

    </p>
</div>

<!-- Is Main Field -->
<div class="form-group">
    {!! Form::label('is_main', 'نوع المحل') !!}
    @if($store->is_main == 0)
       <p>محل فرعي</p>
        @else
    <p>محل رئيسي</p>
    @endif
</div>

<!-- Mobile Field -->
<div class="form-group">
    {!! Form::label('mobile', 'رقم الجوال') !!}
    <p>{!! $store->mobile !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'البريد الألكتروني') !!}
    <p>{!! $store->email !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'رقم الهاتف') !!}
    <p>{!! $store->phone !!}</p>
</div>

<!-- Manager Name Field -->
<div class="form-group">
    {!! Form::label('manager_name', 'اسم المدير') !!}
    <p>{!! $store->manager_name !!}</p>
</div>

<!-- Manager Email Field -->
<div class="form-group">
    {!! Form::label('manager_email', 'بريد المدير') !!}
    <p>{!! $store->manager_email !!}</p>
</div>

<!-- Manager Mobile Field -->
<div class="form-group">
    {!! Form::label('manager_mobile', 'رقم جوال المدير') !!}
    <p>{!! $store->manager_mobile !!}</p>
</div>



<!-- Address Note Field -->
<div class="form-group">
    {!! Form::label('address_note', 'وصف العنوان') !!}
    <p>{!! $store->address_note !!}</p>
</div>


<div class="map-adds">


    <div id="map" style="height: 500px; width:100%;"></div>
    <div id="infowindow-content">
        <span id="place-name"  class="title"></span><br>
        Place ID <span id="place-id"></span><br>
        <span id="place-address"></span>
    </div>
    <input  style="display:none;" value="{{ isset($lng) ? $lng : ''  }}" type="text" id="lngbox" name="lng">
    <input  style="display:none;" value="{{ isset($lat) ? $lat : ''  }}" type="text" id="latbox" name="lat">


</div>


@if(isset($store))
    <br><br>
    <div class="row">
        @foreach($store->images()->get()->pluck("image")->toArray() as $image)
            <div class="col-md-4"> <img src="{{\Helper\Common\imageUrl($image)}}" class="img-responsive col-md-12" height="250"> </div>
        @endforeach
    </div>
    <br><br>

@endif