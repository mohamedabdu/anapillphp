

@extends('layouts.backend')

@section('pageTitle',"تعديل محل")

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

    <li><a href="{{route('stores.index')}}">المحلات</a> <i class="fa fa-circle"></i></li>

    <li><span> تعديل</span></li>

    <li><span> محل</span></li>

@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/stores.js" type="text/javascript"></script>
@endsection
@section('content')
    <form role="form"  id="addForm" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">العنوان</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">
                    <input type="hidden" name="id" id="id" value="{{$store->id}}">

                    <div class="form-body">

                        <!-- Role Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="title_ar" name="title_ar" value="{{$store->title_ar}}">
                            <label for="title">العنوان عربي</label>
                            <span class="help-block"></span>

                        </div>
                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="title_en" name="title_en" value="{{$store->title_en}}">
                            <label for="title_en">العنوان انجليزي</label>
                            <span class="help-block"></span>
                        </div>

                        <!-- Role Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <textarea name="description_ar" class="form-control">{{$store->description_ar}}</textarea>
                            <label for="title">الوصف عربي</label>
                            <span class="help-block"></span>

                        </div>

                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <textarea name="description_en" class="form-control">{{$store->description_en}}</textarea>
                            <label for="title_en">الوصف انجليزي</label>
                            <span class="help-block"></span>
                        </div>


                    </div>
                </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">مدينة المحل</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control" name="country_id" id="country_select">
                            <option value="">Select</option>
                            @foreach(\App\Models\Country::withTrashed()->get() as $country)
                                <option value="{{$country->id}}" @if(isset($store) && $store->country_id == $country->id) selected @endif>{{$country->title_ar}}</option>
                            @endforeach
                        </select>
                        <label for="title">الدولة</label>
                        <span class="help-block"></span>

                    </div>
                    <!-- title en Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control" id="region_select" name="region_id" >
                            <option value="">Select</option>
                            @foreach(\App\Models\Region::withTrashed()->get() as $region)
                                <option value="{{$region->id}}" @if(isset($store) && $store->region_id == $region->id) selected @endif>{{$region->title_ar}}</option>
                            @endforeach
                        </select>
                        <label for="title_en">المنطقة</label>
                        <span class="help-block"></span>
                    </div>

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control" id="city_select" name="city_id" >
                            <option value="">Select</option>
                            @foreach(\App\Models\City::withTrashed()->get() as $city)
                                <option value="{{$city->id}}" @if(isset($store) && $store->city_id == $city->id) selected @endif>{{$city->title_ar}}</option>
                            @endforeach
                        </select>
                        <label for="title">المدينة</label>
                        <span class="help-block"></span>

                    </div>


                </div>
            </div>

        </div>


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">القسم</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control" id="category_select" name="category_id" >
                            <option value="0">Select</option>
                            @foreach(\App\Models\Category::where("type",\App\Models\Category::$types["store"])->get() as $category)
                                <option value="{{$category->id}}" @if(isset($store) && $store->category_id == $category->id) selected @endif>{{$category->title_ar}}</option>
                            @endforeach
                        </select>
                        <label for="title">القسم</label>
                        <span class="help-block"></span>

                    </div>
                    <!-- title en Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control chosen-select" data-placeholder="اختار الأقسام الفرعية" name="sub_categories[]" id="sub_category_select" multiple >
                            @if($store->has_sub_sub == false)
                                @foreach(\App\Models\SubCategory::where("category_id",$store->category_id)->where("parent_id",0)->get() as $subCategory)
                                    <option value="{{$subCategory->id}}" @if(in_array($subCategory->id, $store->subCategories()->pluck("sub_category_id")->toArray())) selected @endif>{{$subCategory->title_ar}}</option>
                                @endforeach
                            @else
                                @foreach($store->subCategoriesParent as $subCategory)
                                    <option value="{{$subCategory->id}}"  selected >{{$subCategory->title_ar}}</option>
                                @endforeach
                            @endif
                        </select>
                        <label for="title_en">الأقسام الفرعية</label>
                        <span class="help-block"></span>
                    </div>

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control chosen-select" data-placeholder="اختار الأقسام الفرعية" name="sub_sub_categories[]" id="sub_sub_category_select" multiple>
                            @if($store->has_sub_sub == true)
                                @foreach(\App\Models\SubCategory::where("category_id",$store->category_id)->where("parent_id",">",0)->get() as $subCategory)
                                    <option value="{{$subCategory->id}}" @if(in_array($subCategory->id, $store->subCategories()->pluck("sub_category_id")->toArray())) selected @endif>{{$subCategory->title_ar}}</option>
                                @endforeach
                            @endif
                        </select>
                        <label for="title">الأقسام تحت الفرعية</label>
                        <span class="help-block"></span>

                    </div>


                </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">نوع المحل</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control" id="is_main" name="is_main" >
                            <option value="">Select</option>
                            <option value="1" @if(isset($store) && $store->is_main == 1) selected @endif>نعم</option>
                            <option value="0" @if(isset($store) && $store->is_main == 0) selected @endif>لا</option>
                        </select>
                        <label for="title">فرع رئيسي</label>
                        <span class="help-block"></span>

                    </div>

                    <!-- main branch Field -->
                    <div class="form-group form-md-line-input col-md-12" id="parent_store_div" style="display: none;">
                        <select class="form-control" id="parent_store" name="parent_id" >
                            <option value="0">Select</option>
                            @php
                                $stores = \App\Models\Store::whereNull("parent_id");
                               if(isset($store)) {
                                   $stores = $stores->where("id","<>",$store->id);
                               }
                               $stores = $stores->get();
                            @endphp
                            @foreach($stores as $item)
                                <option value="{{$item->id}}" @if(isset($store) && $store->parent_id == $item->id) selected @endif>{{$item->title_ar}}</option>
                            @endforeach
                        </select>
                        <label for="title">اختار الفرع الرئيسي</label>
                        <span class="help-block"></span>
                    </div>


                </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">تفاصيل الأتصال</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="mobile" name="mobile" value="{{$store->mobile}}">
                        <label for="mobile">رقم الجوال</label>
                        <span class="help-block"></span>

                    </div>
                    <!-- title en Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="email" name="email" value="{{$store->email}}">
                        <label for="email">البريد الألكتروني</label>
                        <span class="help-block"></span>
                    </div>

                    <!-- title en Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="phone" name="phone" value="{{$store->phone}}">
                        <label for="phone">رقم الهاتف</label>
                        <span class="help-block"></span>
                    </div>

                    <!-- title en Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="manager_name" name="manager_name" value="{{$store->manager_name}}">
                        <label for="manager_name">اسم المدير</label>
                        <span class="help-block"></span>
                    </div>

                    <!-- title en Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="manager_email" name="manager_email" value="{{$store->manager_email}}">
                        <label for="manager_email">بريد المدير</label>
                        <span class="help-block"></span>
                    </div>

                    <!-- title en Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="manager_mobile" name="manager_mobile" value="{{$store->manager_mobile}}">
                        <label for="manager_mobile">رقم المدير</label>
                        <span class="help-block"></span>
                    </div>


                </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">تفاصيل العنوان</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="mapAddress" name="address_note" value="">
                        <label for="address_note">وصف العنوان</label>
                        <span class="help-block"></span>

                    </div>


                    @include("layouts.libs.map.mapHTML")


                </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">الشعار</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">

                    <div class="form-group form-md-line-input col-md-2">
                        <div class="image_0_box">
                            <img src="{{ isset($store->logo) ? \Helper\Common\imageUrl($store->logo) : url('no-image.png') }}"
                                 width="100" height="80" class="image_0" />
                        </div>
                        <input type="file" name="logo" id="image_0" style="visibility: hidden" >
                        <span class="help-block"></span>
                    </div>



                    <div class="clearfix"></div>



                </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">الصور</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">

                    @php $counter = 0; @endphp
                    @for ($i = 1; $i < 6; $i++)
                        <div class="form-group form-md-line-input col-md-2">
                            <div class="image_{{$i}}_box">
                                @if (isset($store->images[$counter]) && $i != 0)
                                    <a href="javascript:;" class="remove_image_link" onclick="My.deleteImage(this);return false;"
                                       data-model="Store" data-folder="articles" data-col="images"
                                       data-image="{{ isset($store->images[$counter]) ? $store->images[$counter]->image : '' }}">
                                        <img src="{{ url('delete-btn.png') }}" /></a>
                                @endif
                                <img src="{{ isset($store->images[$counter]) ? \Helper\Common\imageUrl($store->images[$counter]->image) : url('no-image.png') }}"
                                     width="100" height="80" class="{{ isset($store->images[$counter]) && $i != 0 ? '' : 'image_'.$i.'' }}" />
                            </div>
                            <input type="file" name="images[{{$i}}]" id="image_{{$i}}" style="visibility: hidden">
                            <span class="help-block"></span>
                        </div>
                        @php $counter++ @endphp
                    @endfor



                    <div class="clearfix"></div>



                </div>
            </div>

            <div class="panel-footer text-center">
                <button type="button" class="btn btn-info submit-form"
                >{{_lang('app.save') }}</button>
            </div>

        </div>


    </form>
    <script>
        var new_lang = {

        };
        var new_config = {

        };

    </script>
@endsection
