 <div class="form-body">
<!-- Title Ar Field -->
     <div class="form-group row">
     {!! Form::label('title_ar', 'الأسم بالعربي',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('title_ar', null, ['class' => 'form-control','id'=>"title_ar","required"]) !!}
    </div>
 </div>

<!-- Title En Field -->
     <div class="form-group row">
     {!! Form::label('title_en', 'الأسم بالانجليزي',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('title_en', null, ['class' => 'form-control','id'=>"title_en","required"]) !!}
    </div>
 </div>


<!-- Description Ar Field -->
 <div class="form-group row">
     {!! Form::label('description_ar', 'الوصف بالعربي',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::textarea('description_ar', null, ['class' => 'form-control','id'=>"description_ar"]) !!}
    </div>
 </div>


<!-- Description En Field -->
 <div class="form-group row">
     {!! Form::label('description_en', 'الوصف بالانجليزي',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::textarea('description_en', null, ['class' => 'form-control','id'=>"description_en"]) !!}
    </div>
 </div>

<!-- Category Id Field -->
     <div class="form-group row">
     {!! Form::label('category_id', 'القسم الرئيسي',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    <select class="form-control" id="category_select" name="category_id" required>
        <option value="0">Select</option>
    @foreach(\App\Models\Category::where("type",\App\Models\Category::$types["store"])->get() as $category)
            <option value="{{$category->id}}" @if(isset($store) && $store->category_id == $category->id) selected @endif>{{$category->title_ar}}</option>
            @endforeach
    </select>
    </div>
 </div>
     <!-- sub Categories Id Field -->

     @if(!isset($store))
     <div class="form-group row">
         {!! Form::label('sub_category', 'الأقسام الفرعية',['class' => 'col-sm-2 col-form-label']) !!}
         <div class="col-sm-10">
             <select class="form-control" name="sub_categories[]" id="sub_category_select" multiple required>

             </select>
         </div>
     </div>
     <!-- sub sub Categories Id Field -->

     <div class="form-group row">
         {!! Form::label('sub_category', 'الأقسام تحت الفرعية',['class' => 'col-sm-2 col-form-label']) !!}
         <div class="col-sm-10">
             <select class="form-control" name="sub_sub_categories[]" id="sub_sub_category_select" multiple>

             </select>
         </div>
     </div>
     @else
         @php /** @var \App\Models\Store $store **/ @endphp
         <div class="form-group row">
             {!! Form::label('sub_category', 'الأقسام الفرعية',['class' => 'col-sm-2 col-form-label']) !!}
             <div class="col-sm-10">
                 <select class="form-control" name="sub_categories[]" id="sub_category_select" multiple required>
                    @if($store->has_sub_sub == false)
                        @foreach(\App\Models\SubCategory::where("category_id",$store->category_id)->where("parent_id",0)->get() as $subCategory)
                            <option value="{{$subCategory->id}}" @if(in_array($subCategory->id, $store->subCategories()->pluck("sub_category_id")->toArray())) selected @endif>{{$subCategory->title_ar}}</option>
                            @endforeach
                        @else
                         @foreach($store->subCategoriesParent as $subCategory)
                             <option value="{{$subCategory->id}}"  selected >{{$subCategory->title_ar}}</option>
                         @endforeach
                     @endif
                 </select>
             </div>
         </div>
         <!-- sub sub Categories Id Field -->
         <div class="form-group row">
             {!! Form::label('sub_category', 'الأقسام تحت الفرعية',['class' => 'col-sm-2 col-form-label']) !!}
             <div class="col-sm-10">
                 <select class="form-control" name="sub_sub_categories[]" id="sub_sub_category_select" multiple>
                     @if($store->has_sub_sub == true)
                         @foreach(\App\Models\SubCategory::where("category_id",$store->category_id)->where("parent_id",">",0)->get() as $subCategory)
                             <option value="{{$subCategory->id}}" @if(in_array($subCategory->id, $store->subCategories()->pluck("sub_category_id")->toArray())) selected @endif>{{$subCategory->title_ar}}</option>
                         @endforeach
                     @endif
                 </select>
             </div>
         </div>
         @endif

<!-- Country Id Field -->
     <div class="form-group row">
         {!! Form::label('country_id', 'الدولة',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        <select class="form-control" name="country_id" id="country_select" required>
            <option value="">Select</option>
            @foreach(\App\Models\Country::withTrashed()->get() as $country)
                <option value="{{$country->id}}" @if(isset($store) && $store->country_id == $country->id) selected @endif>{{$country->title_ar}}</option>
            @endforeach
        </select>
    </div>
 </div>

<!-- Region Id Field -->
     <div class="form-group row">
         {!! Form::label('region_id', 'المنطقة',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        <select class="form-control" id="region_select" name="region_id" required>
            <option value="">Select</option>
            @foreach(\App\Models\Region::withTrashed()->get() as $region)
                <option value="{{$region->id}}" @if(isset($store) && $store->region_id == $region->id) selected @endif>{{$region->title_ar}}</option>
            @endforeach
        </select>
    </div>
 </div>

<!-- City Id Field -->
     <div class="form-group row">
         {!! Form::label('city_id', 'المدينة',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        <select class="form-control" id="city_select" name="city_id" required>
            <option value="">Select</option>
            @foreach(\App\Models\City::withTrashed()->get() as $city)
                <option value="{{$city->id}}" @if(isset($store) && $store->city_id == $city->id) selected @endif>{{$city->title_ar}}</option>
            @endforeach
        </select>
    </div>
 </div>

<!-- Logo Field -->

  <div class="input-group mb-3">
   <div class="custom-file">
     <input type="file" name="logo" class="custom-file-input" id="inputGroupFile02">
     <label class="custom-file-label" for="inputGroupFile02">الشعار</label>
      </div>

   </div>

 @if(isset($store))
         <div class="row">
             <div class="col-md-4"> <img src="{{\Helper\Common\imageUrl($store->logo)}}" class="img-responsive col-md-12"> </div>
         </div>
         <br><br>

     @endif
     <!-- images Field -->

     <div class="input-group mb-3">
         <div class="custom-file">
             <input type="file" name="images[]" multiple class="custom-file-input" id="inputGroupFile02" max="3" accept="image/*">
             <label class="custom-file-label" for="inputGroupFile02">الصور</label>
         </div>

     </div>

     @if(isset($store))
         <div class="row">
             @foreach($store->images as $image)
             <div class="col-md-4"> <img src="{{\Helper\Common\imageUrl($image->image)}}" class="img-responsive col-md-12" height="200"> </div>
                 @endforeach
         </div>
         <br><br>

 @endif


<!-- Is Main Field -->
     <div class="form-group row">
     {!! Form::label('is_main', 'فرع رئيسي',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        <select class="form-control" id="is_main" name="is_main" required>
            <option value="0">Select</option>
            <option value="1" @if(isset($store) && $store->is_main == 1) selected @endif>نعم</option>
            <option value="0" @if(isset($store) && $store->is_main == 0) selected @endif>لا</option>
        </select>
    </div>
 </div>
     <!-- main branch Field -->
     <div class="form-group row" id="parent_store_div" style="display: none;">
         {!! Form::label('is_main_parent', 'اختار الفرع الرئيسي',['class' => 'col-sm-2 col-form-label']) !!}
         <div class="col-sm-10">
             <select class="form-control" id="parent_store" name="parent_id" required>
                 <option value="0">Select</option>
                 @php
                         $stores = \App\Models\Store::whereNull("parent_id");
                        if(isset($store)) {
                            $stores = $stores->where("id","<>",$store->id);
                        }
                        $stores = $stores->get();
                         @endphp
                 @foreach($stores as $item)
                     <option value="{{$item->id}}" @if(isset($store) && $store->parent_id == $item->id) selected @endif>{{$item->title_ar}}</option>
                 @endforeach
             </select>
         </div>
     </div>

<!-- Mobile Field -->
     <div class="form-group row">
     {!! Form::label('mobile', 'رقم الجوال',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('mobile', null, ['class' => 'form-control','id'=>"mobile"]) !!}
    </div>
 </div>

<!-- Email Field -->
     <div class="form-group row">
     {!! Form::label('email', 'البريد الألكتروني',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('email', null, ['class' => 'form-control','id'=>"email"]) !!}
    </div>
 </div>

<!-- Phone Field -->
     <div class="form-group row">
     {!! Form::label('phone', 'رقم الهاتف',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('phone', null, ['class' => 'form-control','id'=>"phone"]) !!}
    </div>
 </div>

<!-- Manager Name Field -->
     <div class="form-group row">
     {!! Form::label('manager_name', 'اسم المدير',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('manager_name', null, ['class' => 'form-control','id'=>"manager_name"]) !!}
    </div>
 </div>

<!-- Manager Email Field -->
     <div class="form-group row">
     {!! Form::label('manager_email', 'بريد المدير',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('manager_email', null, ['class' => 'form-control','id'=>"manager_email"]) !!}
    </div>
 </div>

<!-- Manager Mobile Field -->
     <div class="form-group row">
     {!! Form::label('manager_mobile', 'رقم جوال المدير',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('manager_mobile', null, ['class' => 'form-control','id'=>"manager_mobile"]) !!}
    </div>
 </div>
     <!-- Address Note Field -->
     <div class="form-group row">
         {!! Form::label('address_note', 'وصف العنوان',['class' => 'col-sm-2 col-form-label']) !!}
         <div class="col-sm-10">
             {!! Form::text('address_note', null, ['class' => 'form-control','id'=>"mapAddress","required"]) !!}
         </div>
     </div>


 @include("layouts.libs.map.mapHTML")





 </div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('stores.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>


 <script>
     $( document ).ready(function() {

         var is_main = $('#is_main').val();
         if (is_main > 0) {
             $("#parent_store_div").fadeOut(1000);
         }
         else {
             $("#parent_store_div").fadeIn(1000);
         }
         $('#is_main').on('change', function (e) {

             var is_main = e.target.value;
             if (is_main > 0) {
                 $("#parent_store_div").fadeOut(1000);
             }
             else {
                 $("#parent_store_div").fadeIn(1000);
             }

         });

         $('#country_select').on('change', function (e) {

             var country = e.target.value;
             if (country > 0) {
                 $.get(appUrl + '/countries/ajax/' + country, function (data) {

                     $('#region_select').empty();
                     $('#region_select').append($('<option>', {
                         value: '',
                         text: "اختار",
                         selected: true
                     }));

                     $('#city_select').empty();
                     $('#city_select').append($('<option>', {
                         value: '',
                         text: "اختار",
                         selected: true
                     }));


                     if (data.length !== 0) {
                         $.each(data, function (index, subCatObj) {

                             $('#region_select').append($('<option>', {
                                 value: subCatObj.id,
                                 text: subCatObj.title_ar
                             }));

                         });

                     } else {
                         $('#region_select').empty();

                         $('#region_select').append($('<option>', {
                             value: '',
                             text: "اختار",
                             selected: true
                         }));
                         $('#city_select').empty();
                         $('#city_select').append($('<option>', {
                             value: '',
                             text: "اختار",
                             selected: true
                         }));
                     }

                 });
             }
             else {

                 $('#region_select').empty();

                 $('#region_select').append($('<option>', {
                     value: '',
                     text: "اختار",
                     selected: true
                 }));
             }

         });
         $('#region_select').on('change', function (e) {

             var region = e.target.value;
             if (region > 0) {
                 $.get(appUrl + '/regions/ajax/' + region, function (data) {

                     $('#city_select').empty();

                     if (data.length !== 0) {
                         $.each(data, function (index, subCatObj) {

                             $('#city_select').append($('<option>', {
                                 value: subCatObj.id,
                                 text: subCatObj.title_ar
                             }));

                         });

                     } else {
                         $('#city_select').empty();

                         $('#city_select').append($('<option>', {
                             value: '',
                             text: "اختار",
                             selected: true
                         }));
                     }

                 });
             }
             else {

                 $('#city_select').empty();

                 $('#city_select').append($('<option>', {
                     value: '',
                     text: "اختار",
                     selected: true
                 }));
             }

         });
         $('#category_select').on('change', function (e) {

             var category = e.target.value;
             if (category > 0) {
                 $.get(appUrl + '/subCategories/ajax/' + category, function (data) {

                     $('#sub_category_select').empty();
                     $('#sub_sub_category_select').empty();

                     if (data.length !== 0) {
                         $.each(data, function (index, subCatObj) {

                             $('#sub_category_select').append($('<option>', {
                                 value: subCatObj.id,
                                 text: subCatObj.title_ar
                             }));

                         });

                     } else {
                         $('#sub_category_select').empty();
                     }

                 });
             }
             else {

                 $('#sub_category_select').empty();

             }

         });
         $('#sub_category_select').on('change', function (e) {

             var subIds = [];
             $('#sub_category_select').each(function() {
                 subIds.push($(this).val());
             });
             $.get(appUrl + '/subCategories/ajax/sub/' + subIds, function (data) {

                 $('#sub_sub_category_select').empty();
                 if (data.length !== 0) {
                     $.each(data, function (index, subCatObj) {

                         $('#sub_sub_category_select').append($('<option>', {
                             value: subCatObj.id,
                             text: subCatObj.title_ar
                         }));

                     });

                 } else {
                     $('#sub_sub_category_select').empty();


                 }

             });

         });
     });


 </script>
