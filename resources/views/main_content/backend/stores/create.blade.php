

@extends('layouts.backend')

@section('pageTitle',"اضافة محل")

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

    <li><a href="{{route('stores.index')}}">المحلات</a> <i class="fa fa-circle"></i></li>

    <li><span> اضافة</span></li>

    <li><span> محل</span></li>

@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/stores.js" type="text/javascript"></script>
@endsection
@section('content')
    <form role="form"  id="addForm" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">العنوان</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">
                    <input type="hidden" name="id" id="id" value="0">

                    <div class="form-body">

                        <!-- Role Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="title_ar" name="title_ar" value="">
                            <label for="title">العنوان عربي</label>
                            <span class="help-block"></span>

                        </div>
                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="title_en" name="title_en" value="">
                            <label for="title_en">العنوان انجليزي</label>
                            <span class="help-block"></span>
                        </div>

                        <!-- Role Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <textarea name="description_ar" class="form-control"></textarea>
                            <label for="title">الوصف عربي</label>
                            <span class="help-block"></span>

                        </div>

                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <textarea name="description_en" class="form-control"></textarea>
                            <label for="title_en">الوصف انجليزي</label>
                            <span class="help-block"></span>
                        </div>


                    </div>
                </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">مدينة المحل</h3>
            </div>
            <div class="panel-body">


                    <div class="form-body">

                        <!-- Role Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <select class="form-control" name="country_id" id="country_select">
                                <option value="">Select</option>
                                @foreach(\App\Models\Country::withTrashed()->get() as $country)
                                    <option value="{{$country->id}}" @if(isset($store) && $store->country_id == $country->id) selected @endif>{{$country->title_ar}}</option>
                                @endforeach
                            </select>
                            <label for="title">الدولة</label>
                            <span class="help-block"></span>

                        </div>
                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <select class="form-control" id="region_select" name="region_id" >
                                <option value="">Select</option>
                                @foreach(\App\Models\Region::withTrashed()->get() as $region)
                                    <option value="{{$region->id}}" @if(isset($store) && $store->region_id == $region->id) selected @endif>{{$region->title_ar}}</option>
                                @endforeach
                            </select>
                            <label for="title_en">المنطقة</label>
                            <span class="help-block"></span>
                        </div>

                        <!-- Role Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <select class="form-control" id="city_select" name="city_id" >
                                <option value="">Select</option>
                                @foreach(\App\Models\City::withTrashed()->get() as $city)
                                    <option value="{{$city->id}}" @if(isset($store) && $store->city_id == $city->id) selected @endif>{{$city->title_ar}}</option>
                                @endforeach
                            </select>
                            <label for="title">المدينة</label>
                            <span class="help-block"></span>

                        </div>


                    </div>
            </div>

        </div>


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">القسم</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control" id="category_select" name="category_id" >
                            <option value="0">Select</option>
                            @foreach(\App\Models\Category::where("type",\App\Models\Category::$types["store"])->get() as $category)
                                <option value="{{$category->id}}" @if(isset($store) && $store->category_id == $category->id) selected @endif>{{$category->title_ar}}</option>
                            @endforeach
                        </select>
                        <label for="title">القسم</label>
                        <span class="help-block"></span>

                    </div>
                    <!-- title en Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <br>
                        <select class="form-control chosen-select" data-placeholder="اختار الأقسام الفرعية" name="sub_categories[]" id="sub_category_select" multiple >
                            <option value="" disabled>اختار</option>

                        </select>
                        <label for="title_en">الأقسام الفرعية</label>
                        <span class="help-block"></span>
                    </div>

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <br>
                        <select class="form-control chosen-select" data-placeholder="اختار الأقسام الفرعية" name="sub_sub_categories[]" id="sub_sub_category_select" multiple>
                            <option value="" disabled>اختار</option>

                        </select>
                        <label for="title">الأقسام تحت الفرعية</label>
                        <span class="help-block"></span>

                    </div>


                </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">نوع المحل</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control" id="is_main" name="is_main" >
                            <option value="">Select</option>
                            <option value="1" @if(isset($store) && $store->is_main == 1) selected @endif>نعم</option>
                            <option value="0" @if(isset($store) && $store->is_main == 0) selected @endif>لا</option>
                        </select>
                        <label for="title">فرع رئيسي</label>
                        <span class="help-block"></span>

                    </div>

                    <!-- main branch Field -->
                    <div class="form-group form-md-line-input col-md-12" id="parent_store_div" style="display: none;">
                            <select class="form-control" id="parent_store" name="parent_id" >
                                <option value="0">Select</option>
                                @php
                                    $stores = \App\Models\Store::whereNull("parent_id");
                                   if(isset($store)) {
                                       $stores = $stores->where("id","<>",$store->id);
                                   }
                                   $stores = $stores->get();
                                @endphp
                                @foreach($stores as $item)
                                    <option value="{{$item->id}}" @if(isset($store) && $store->parent_id == $item->id) selected @endif>{{$item->title_ar}}</option>
                                @endforeach
                            </select>
                        <label for="title">اختار الفرع الرئيسي</label>
                        <span class="help-block"></span>
                    </div>


                </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">تفاصيل الأتصال</h3>
            </div>
            <div class="panel-body">


                    <div class="form-body">

                        <!-- Role Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="mobile" name="mobile" value="">
                            <label for="mobile">رقم الجوال</label>
                            <span class="help-block"></span>

                        </div>
                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="email" name="email" value="">
                            <label for="email">البريد الألكتروني</label>
                            <span class="help-block"></span>
                        </div>

                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="phone" name="phone" value="">
                            <label for="phone">رقم الهاتف</label>
                            <span class="help-block"></span>
                        </div>

                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="manager_name" name="manager_name" value="">
                            <label for="manager_name">اسم المدير</label>
                            <span class="help-block"></span>
                        </div>

                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="manager_email" name="manager_email" value="">
                            <label for="manager_email">بريد المدير</label>
                            <span class="help-block"></span>
                        </div>

                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="manager_mobile" name="manager_mobile" value="">
                            <label for="manager_mobile">رقم المدير</label>
                            <span class="help-block"></span>
                        </div>


                    </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">تفاصيل العنوان</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="mapAddress" name="address_note" value="">
                        <label for="address_note">وصف العنوان</label>
                        <span class="help-block"></span>

                    </div>


                    @include("layouts.libs.map.mapHTML")


                </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">الشعار</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">

                    <div class="form-group form-md-line-input col-md-2">
                        <div class="image_0_box">
                            <img src="{{url('no-image.png')}}" width="100" height="80" class="image_0" />
                        </div>
                        <input type="file" name="logo" id="image_0" style="visibility: hidden">
                        <span class="help-block"></span>
                    </div>

                    <div class="clearfix"></div>



                </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">الصور</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">


                    @for ($i = 1; $i < 6; $i++)
                        <div class="form-group form-md-line-input col-md-2">
                            <div class="image_{{$i}}_box">
                                <img src="{{url('no-image.png')}}" width="100" height="80" class="image_{{$i}}" />
                            </div>
                            <input type="file" name="images[{{$i}}]" id="image_{{$i}}" style="visibility: hidden">
                            <span class="help-block"></span>
                        </div>
                    @endfor

                    <div class="clearfix"></div>



                </div>
            </div>

            <div class="panel-footer text-center">
                <button type="button" class="btn btn-info submit-form"
                >{{_lang('app.save') }}</button>
            </div>

        </div>


    </form>
    <script>
        var new_lang = {

        };
        var new_config = {

        };

    </script>
@endsection
