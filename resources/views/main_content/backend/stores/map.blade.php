


@extends('layouts.backend')

@section('pageTitle', _lang('app.stores'))

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>


    <li><span>المحلات</span></li>


@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/stores.js" type="text/javascript"></script>
@endsection
@section('content')


    <div class = "panel panel-default">
        {{ csrf_field() }}
        <div class = "panel-body">


            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">
                            <a class="btn green" style="margin-bottom: 40px;" href = "{{ route('stores.create') }}" onclick="">{{ _lang('app.add_new')}}<i class="fa fa-plus"></i> </a>
                        </div>
                    </div>

                </div>
            </div>



            <div class="map-adds">




                <div id="map" style="height: 500px; width:100%;"></div>
                <div id="infowindow-content">
                    <span id="place-name"  class="title"></span><br>
                    Place ID <span id="place-id"></span><br>
                    <span id="place-address"></span>
                </div>


            </div>

            <!--Table Wrapper Finish-->
        </div>
    </div>
    <script>
        var new_lang = {

        };
        var new_config = {

        };
    </script>



    <script>
        // This example adds a search box to a map, using the Google Place Autocomplete
        // feature. People can enter geographical searches. The search box will return a
        // pick list containing a mix of places and predicted search terms.

        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        function initAutocomplete() {
            var markers = [];
            var lat = 0;
            var lng = 0;
            var stores = jQuery.parseJSON('{!! $stores !!}');


            @if(isset($lat) && isset($lng))
                lat = {{$lat}};
            lng = {{$lng}};
                    @endif

            var map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: 30.0594699, lng: 31.1884238},
                    zoom: 15,
                    mapTypeId: 'roadmap'
                });


            var geocoder;
            geocoder = new google.maps.Geocoder();


            if (navigator.geolocation && lat === 0) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    map.setCenter(pos);
                    var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    placeMarker(latlng);
                    geocodeLatLng(geocoder, latlng);
                });
            } else {
                var pos = {
                    lat: lat,
                    lng: lng
                };

                map.setCenter(pos);
                var latlng = new google.maps.LatLng(lat, lng);
                placeMarker(latlng);

            }




            // Create the search box and link it to the UI element.
            var infoWindows = [];
            var counter = 0;

            for(i = 0; i < stores.length; i++) {
                var latlng = new google.maps.LatLng(stores[i].lat, stores[i].lng);
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map
                });
                // markers.forEach(function(marker) {
                //     marker.setMap(null);
                // });
                // markers = [];


                var contentString = '<div id="content">' +
                    '<div id="siteNotice">' +
                    '</div>' +
                    '<p><center><h1 id="firstHeading" class="firstHeading">'
                    + stores[i].title_ar
                    + '</h1></center></p>' +
                    '<a class="btn btn-info" target="_blank" href="{{url("admin/stores")}}/'
                +stores[i].id+
                    '">التفاصيل</a>' +
                    '</div>' +
                    '</div>';

                var infoWindow = new google.maps.InfoWindow();
                infoWindow.setContent(contentString );

                markers.push(marker);
                infoWindows.push(infoWindow);

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {

                        console.log(markers[i]);
                        console.log(infoWindows[i]);
                        infoWindows[i].open(map, marker);

                    }
                })(marker, i));


                counter += 1;
            }





            function placeMarker(location) {
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                // markers.forEach(function(marker) {
                //     marker.setMap(null);
                // });
                // markers = [];

                markers.push(marker);
                //geocodeLatLng(geocoder, location);
            }

        }





        function geocodeLatLng(geocoder, pos) {
            // var input = document.getElementById('latlng').value;
            // var latlngStr = input.split(',', 2);
            // var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
            geocoder.geocode({'location': pos}, function(results, status) {
                if (status === 'OK') {
                    if (results[0]) {
                        console.log(results[0]);
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });
        }

    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQxy6KaWYNPIR0ymzvi5_gUIPNu7QP8VU&libraries=places&callback=initAutocomplete"
            async defer></script>



@endsection
