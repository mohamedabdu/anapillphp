
@extends('layouts.backend')

@section('pageTitle', _lang('app.stores'))

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

    <li><a href="{{route('stores.index')}}">المحلات</a> <i class="fa fa-circle"></i></li>


    <li><span>{{$store->title_ar}}</span></li>

@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/stores.js" type="text/javascript"></script>
@endsection
@section('content')
    @php /** @var \App\Models\Store $store **/  @endphp





    <div class="panel panel-default">
        {{ csrf_field() }}
        <div class="panel-body">
            <div class="container">

                <div class="card">
                    <div class="card-body">
                        <div class="col-md-6 col-md-offset-4">
                            <br>
                            <div class="details font" style="color: #94A0B2">
                                <h1>تفاصيل المحل</h1>
                                <p></p>
                                <div class="col-md-offset-0">
                                    <img src="{{\Helper\Common\imageUrl($store->logo)}}" class="img-circle" width="150" height="150">

                                </div>
                            </div>
                        </div>




                    </div>
                </div>




            </div>



        </div>

    </div>

    <div class="panel panel-default">
        {{ csrf_field() }}
        <div class="panel-body">
            <div class="container">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-6 col-md-offset-4">
                            <br>
                            <div class="details font-green">
                                <p></p>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details" style="color: #94A0B2">
                                    <h4>اسم المحل عربي</h4>
                                    <h4>{{$store->title_ar}}</h4>
                                </div>
                            </div>

                            <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details" style="color: #94A0B2">
                                    <h4>اسم المحل انجليزي</h4>
                                    <h4>{{$store->title_en}}</h4>
                                </div>
                            </div>

                            <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details" style="color: #94A0B2">
                                    <h4>وصف المحل عربي</h4>
                                    <h4>{{$store->description_ar}}</h4>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details" style="color: #94A0B2">
                                    <h4>وصف المحل انجليزي</h4>
                                    <h4>{{$store->description_en}}</h4>
                                </div>
                            </div>

                        </div>



                    </div>
                </div>


            </div>



        </div>

    </div>


    <div class="panel panel-default">
        {{ csrf_field() }}
        <div class="panel-body">
            <div class="container">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-6 col-md-offset-4">
                            <br>
                            <div class="details font" style="color: #94A0B2">
                                <h1>قسم المحل</h1>
                                <p></p>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details font" style="color: #94A0B2">
                                    <h4>القسم الرئيسي</h4>
                                    <h4>  @if($store->category != null)
                                            {{$store->category->title_ar}}
                                        @endif</h4>
                                </div>
                            </div>


<div class="col-md-3 col-md-offset-0">
                                    <br>
                                    <div class="details font-" style="color: #94A0B2">
                                        <h4>القسم الفرعي</h4>
                                        <h4>  @if($store->subCategory != null)
                                                @foreach($store->subCategory as $category)
                                                    <p>{{$category->title_ar}}</p>
                                                    <br>
                                                @endforeach
                                            @endif</h4>
                                    </div>
                                </div>
                                
                            @if($store->has_sub_sub)

                                <div class="col-md-3 col-md-offset-0">
                                    <br>
                                    <div class="details font" style="color: #94A0B2">
                                        <h4>القسم فرع الفرعي</h4>
                                        <h4>  @if($store->subSubCategory != null)
                                                @foreach($store->subSubCategory as $category)
                                                    <p>{{$category->title_ar}}</p>
                                                    <br>
                                                @endforeach
                                            @endif</h4>
                                    </div>
                                </div>

                                @endif

                            <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details font-" style="color: #94A0B2">
                                    <h4>نوع المحل</h4>
                                    <h4>   @if($store->is_main == 0)
                                            <p>محل فرعي</p>
                                        @else
                                            <p>محل رئيسي</p>
                                        @endif</h4>

                                </div>
                            </div>

                            @if($store->is_main == 0)
                                <div class="col-md-3 col-md-offset-0">
                                    <br>
                                    <div class="details font-" style="color: #94A0B2">
                                        <h4>المحل الرئيسي </h4>
                                        @php $parent = \App\Models\Store::find($store->parent_id) @endphp
                                        @if($parent != null)
                                            <h4>{{$parent->title_ar}}</h4>

                                        @endif

                                    </div>
                                </div>
                                @endif
                        </div>



                    </div>
                </div>


            </div>



        </div>

    </div>

    <div class="panel panel-default">
        {{ csrf_field() }}
        <div class="panel-body">
            <div class="container">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-6 col-md-offset-4">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h2>عنوان  المحل</h2>
                                <p></p>
                            </div>
                        </div>



                        <div class="row">

                            <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details font-" style="color: #94A0B2">
                                    <h4>الدولة</h4>
                                    <h4> @if($store->country != null)
                                            {{$store->country->title_ar}}
                                        @endif</h4>

                                </div>
                            </div>

                            <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details font-" style="color: #94A0B2">
                                    <h4>المنطقة</h4>
                                    <h4>    @if($store->region != null)
                                            {{$store->region->title_ar}}
                                        @endif</h4>

                                </div>
                            </div>

                            <div class="col-md-3 col-md-offset-0" >
                                <br>
                                <div class="details font-" style="color: #94A0B2">
                                    <h4>المدينة</h4>
                                    <h4>  @if($store->city != null)
                                            {{$store->city->title_ar}}
                                        @endif</h4>

                                </div>
                            </div>

                            <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details font-" style="color: #94A0B2">
                                    <h4>وصف العنوان</h4>
                                    <h4> {{$store->address_note}}</h4>

                                </div>
                            </div>


                            <div class="map-adds">


                                <div id="map" style="height: 500px; width:100%;"></div>
                                <div id="infowindow-content">
                                    <span id="place-name"  class="title"></span><br>
                                    Place ID <span id="place-id"></span><br>
                                    <span id="place-address"></span>
                                </div>
                                <input  style="display:none;" value="{{ isset($lng) ? $lng : ''  }}" type="text" id="lngbox" name="lng">
                                <input  style="display:none;" value="{{ isset($lat) ? $lat : ''  }}" type="text" id="latbox" name="lat">


                            </div>


                        </div>



                    </div>
                </div>


            </div>



        </div>

    </div>


    <div class="panel panel-default">
        {{ csrf_field() }}
        <div class="panel-body">
            <div class="container">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-6 col-md-offset-4">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h2>بيانات التواصل</h2>
                                <p></p>
                            </div>
                        </div>



                        <div class="row">
                            @if($store->mobile != null)
                            <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details font-" style="color: #94A0B2">
                                    <h4>رقم الجوال</h4>
                                    <h4>{{$store->mobile}}</h4>
                                </div>
                            </div>
                            @endif

                                @if($store->email != null)

                                <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details font-" style="color: #94A0B2">
                                    <h4>البريد الألكتروني </h4>
                                    <h4>{{$store->email}}</h4>
                                </div>
                            </div>
                                @endif

                                @if($store->phone != null)

                                <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details font-" style="color: #94A0B2">
                                    <h4>رقم الهاتف </h4>
                                    <h4>{{$store->phone}}</h4>
                                </div>
                            </div>
                                @endif


                                @if($store->manager_name != null)
                                <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details font-" style="color: #94A0B2">
                                    <h4>اسم المدير </h4>
                                    <h4>{{$store->manager_name}}</h4>
                                </div>
                            </div>
                                @endif

                                @if($store->manager_email != null)

                                <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details font-" style="color: #94A0B2">
                                    <h4> بريد المدير</h4>
                                    <h4>{{$store->manager_email}}</h4>
                                </div>
                            </div>

                                @endif

                                @if($store->manager_mobile != null)

                                <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details font-" style="color: #94A0B2">
                                    <h4>رقم المدير </h4>
                                    <h4>{{$store->manager_mobile}}</h4>
                                </div>
                            </div>

                                    @endif



                        </div>



                    </div>
                </div>


            </div>



        </div>

    </div>


    <div class="panel panel-default">
        {{ csrf_field() }}
        <div class="panel-body">
            <div class="container">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-6 col-md-offset-4">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h2>الصور</h2>
                                <p></p>
                            </div>
                        </div>



                        @if(isset($store))
                            <br><br>
                            <div class="row">
                                @foreach($store->images()->get()->pluck("image")->toArray() as $image)
                                    <div class="col-md-4"> <img src="{{\Helper\Common\imageUrl($image)}}" class="img-responsive col-md-12" height="250"> </div>
                                @endforeach
                            </div>
                            <br><br>

                        @endif




                    </div>
                </div>


            </div>



        </div>

    </div>




    <script>
        var new_lang = {

        };
        var new_config = {
        };
    </script>
@endsection
