 <div class="form-body">
<!-- Agent Id Field -->
     <div class="form-group row">
     {!! Form::label('agent_id', 'Agent Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('agent_id', null, ['class' => 'form-control','id'=>"agent_id","required"]) !!}
    </div>
 </div>

<!-- Title Field -->
     <div class="form-group row">
     {!! Form::label('title', 'Title:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('title', null, ['class' => 'form-control','id'=>"title","required"]) !!}
    </div>
 </div>

<!-- Reason Field -->
     <div class="form-group row">
     {!! Form::label('reason', 'Reason:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('reason', null, ['class' => 'form-control','id'=>"reason","required"]) !!}
    </div>
 </div>

<!-- Start Date Field -->
     <div class="form-group row">
     {!! Form::label('start_date', 'Start Date:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('start_date', null, ['class' => 'form-control','id'=>"start_date","required"]) !!}
    </div>
 </div>

<!-- End Date Field -->
     <div class="form-group row">
     {!! Form::label('end_date', 'End Date:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('end_date', null, ['class' => 'form-control','id'=>"end_date","required"]) !!}
    </div>
 </div>

<!-- Status Field -->
     <div class="form-group row">
     {!! Form::label('status', 'Status:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('status', null, ['class' => 'form-control','id'=>"status","required"]) !!}
    </div>
 </div>

<!-- Refuse Note Field -->
     <div class="form-group row">
     {!! Form::label('refuse_note', 'Refuse Note:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('refuse_note', null, ['class' => 'form-control','id'=>"refuse_note","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('agentHolidays.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
