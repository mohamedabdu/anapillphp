
@extends('layouts.app')

@section('content')

 <div class="container-fluid">

            <!-- Breadcrumb-->
            <div class="row pt-2 pb-2">
                <div class="col-sm-9">
                    <h4 class="page-title">Agent Holidays</h4>
                </div>
            </div>
            <!-- End Breadcrumb-->

            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">

                           @include('adminlte-templates::common.errors')

                           {!! Form::open(['route' => 'agentHolidays.store','files' => true,"class"=>"floating-labels"]) !!}

                           @include('agent_holidays.fields')

                           {!! Form::close() !!}

                        </div>
                    </div>

                </div>
            </div>

        </div>


@endsection


