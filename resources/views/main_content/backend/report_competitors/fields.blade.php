 <div class="form-body">
<!-- Type Field -->
     <div class="form-group row">
     {!! Form::label('type', 'Type:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('type', null, ['class' => 'form-control','id'=>"type","required"]) !!}
    </div>
 </div>

<!-- Competitor Id Field -->
     <div class="form-group row">
     {!! Form::label('competitor_id', 'Competitor Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('competitor_id', null, ['class' => 'form-control','id'=>"competitor_id","required"]) !!}
    </div>
 </div>

<!-- Product Id Field -->
     <div class="form-group row">
     {!! Form::label('product_id', 'Product Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('product_id', null, ['class' => 'form-control','id'=>"product_id","required"]) !!}
    </div>
 </div>

<!-- Price For Company Field -->
     <div class="form-group row">
     {!! Form::label('price_for_company', 'Price For Company:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('price_for_company', null, ['class' => 'form-control','id'=>"price_for_company","required"]) !!}
    </div>
 </div>

<!-- Price For Competitor Field -->
     <div class="form-group row">
     {!! Form::label('price_for_competitor', 'Price For Competitor:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('price_for_competitor', null, ['class' => 'form-control','id'=>"price_for_competitor","required"]) !!}
    </div>
 </div>

<!-- Note Field -->
     <div class="form-group row">
     {!! Form::label('note', 'Note:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('note', null, ['class' => 'form-control','id'=>"note","required"]) !!}
    </div>
 </div>

<!-- Number Of Promo Field -->
     <div class="form-group row">
     {!! Form::label('number_of_promo', 'Number Of Promo:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('number_of_promo', null, ['class' => 'form-control','id'=>"number_of_promo","required"]) !!}
    </div>
 </div>

<!-- Promo Note Field -->
     <div class="form-group row">
     {!! Form::label('promo_note', 'Promo Note:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('promo_note', null, ['class' => 'form-control','id'=>"promo_note","required"]) !!}
    </div>
 </div>

<!-- Promo Image Field -->
     <div class="form-group row">
     {!! Form::label('promo_image', 'Promo Image:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('promo_image', null, ['class' => 'form-control','id'=>"promo_image","required"]) !!}
    </div>
 </div>

<!-- Category Field -->
     <div class="form-group row">
     {!! Form::label('category', 'Category:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('category', null, ['class' => 'form-control','id'=>"category","required"]) !!}
    </div>
 </div>

<!-- Width For Company Field -->
     <div class="form-group row">
     {!! Form::label('width_for_company', 'Width For Company:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('width_for_company', null, ['class' => 'form-control','id'=>"width_for_company","required"]) !!}
    </div>
 </div>

<!-- Width For Competitor Field -->
     <div class="form-group row">
     {!! Form::label('width_for_competitor', 'Width For Competitor:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('width_for_competitor', null, ['class' => 'form-control','id'=>"width_for_competitor","required"]) !!}
    </div>
 </div>

<!-- Height For Company Field -->
     <div class="form-group row">
     {!! Form::label('height_for_company', 'Height For Company:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('height_for_company', null, ['class' => 'form-control','id'=>"height_for_company","required"]) !!}
    </div>
 </div>

<!-- Height For Competitor Field -->
     <div class="form-group row">
     {!! Form::label('height_for_competitor', 'Height For Competitor:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('height_for_competitor', null, ['class' => 'form-control','id'=>"height_for_competitor","required"]) !!}
    </div>
 </div>

<!-- Store Field -->
     <div class="form-group row">
     {!! Form::label('store', 'Store:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('store', null, ['class' => 'form-control','id'=>"store","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('reportCompetitors.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
