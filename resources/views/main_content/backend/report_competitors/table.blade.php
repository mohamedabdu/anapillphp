 <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><i class="fa fa-table"></i> Data Table Example</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="default-datatable" class="dataTable table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Type</th>
        <th>Competitor Id</th>
        <th>Product Id</th>
        <th>Price For Company</th>
        <th>Price For Competitor</th>
        <th>Note</th>
        <th>Number Of Promo</th>
        <th>Promo Note</th>
        <th>Promo Image</th>
        <th>Category</th>
        <th>Width For Company</th>
        <th>Width For Competitor</th>
        <th>Height For Company</th>
        <th>Height For Competitor</th>
        <th>Store</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>


                            <script>
                                $( document ).ready(function() {
                                    datatable = $('.dataTable').dataTable({
                                        //"processing": true,
                                        "serverSide": true,
                                        "ajax": {
                                            "url": "{{route('reportCompetitors.ajax')}}",
                                            "type": "GET",
                                        },
                                        "columns": [
                                            {"data": "id"},
               {"data": "type"},
               {"data": "competitor_id"},
               {"data": "product_id"},
               {"data": "price_for_company"},
               {"data": "price_for_competitor"},
               {"data": "note"},
               {"data": "number_of_promo"},
               {"data": "promo_note"},
               {"data": "promo_image"},
               {"data": "category"},
               {"data": "width_for_company"},
               {"data": "width_for_competitor"},
               {"data": "height_for_company"},
               {"data": "height_for_competitor"},
               {"data": "store"},

                                            {"data": "options", orderable: false, searchable: false}
                                        ],
                                        "order": [
                                            [0, "desc"]
                                        ],
                                        //"oLanguage": {"sUrl": config.url + '/datatable-lang-' + "ar" + '.json'}

                                    });
                                });
                            </script>

                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->