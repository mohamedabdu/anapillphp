<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $reportCompetitor->id !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $reportCompetitor->type !!}</p>
</div>

<!-- Competitor Id Field -->
<div class="form-group">
    {!! Form::label('competitor_id', 'Competitor Id:') !!}
    <p>{!! $reportCompetitor->competitor_id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $reportCompetitor->product_id !!}</p>
</div>

<!-- Price For Company Field -->
<div class="form-group">
    {!! Form::label('price_for_company', 'Price For Company:') !!}
    <p>{!! $reportCompetitor->price_for_company !!}</p>
</div>

<!-- Price For Competitor Field -->
<div class="form-group">
    {!! Form::label('price_for_competitor', 'Price For Competitor:') !!}
    <p>{!! $reportCompetitor->price_for_competitor !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $reportCompetitor->note !!}</p>
</div>

<!-- Number Of Promo Field -->
<div class="form-group">
    {!! Form::label('number_of_promo', 'Number Of Promo:') !!}
    <p>{!! $reportCompetitor->number_of_promo !!}</p>
</div>

<!-- Promo Note Field -->
<div class="form-group">
    {!! Form::label('promo_note', 'Promo Note:') !!}
    <p>{!! $reportCompetitor->promo_note !!}</p>
</div>

<!-- Promo Image Field -->
<div class="form-group">
    {!! Form::label('promo_image', 'Promo Image:') !!}
    <p>{!! $reportCompetitor->promo_image !!}</p>
</div>

<!-- Category Field -->
<div class="form-group">
    {!! Form::label('category', 'Category:') !!}
    <p>{!! $reportCompetitor->category !!}</p>
</div>

<!-- Width For Company Field -->
<div class="form-group">
    {!! Form::label('width_for_company', 'Width For Company:') !!}
    <p>{!! $reportCompetitor->width_for_company !!}</p>
</div>

<!-- Width For Competitor Field -->
<div class="form-group">
    {!! Form::label('width_for_competitor', 'Width For Competitor:') !!}
    <p>{!! $reportCompetitor->width_for_competitor !!}</p>
</div>

<!-- Height For Company Field -->
<div class="form-group">
    {!! Form::label('height_for_company', 'Height For Company:') !!}
    <p>{!! $reportCompetitor->height_for_company !!}</p>
</div>

<!-- Height For Competitor Field -->
<div class="form-group">
    {!! Form::label('height_for_competitor', 'Height For Competitor:') !!}
    <p>{!! $reportCompetitor->height_for_competitor !!}</p>
</div>

<!-- Store Field -->
<div class="form-group">
    {!! Form::label('store', 'Store:') !!}
    <p>{!! $reportCompetitor->store !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $reportCompetitor->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $reportCompetitor->updated_at !!}</p>
</div>

