<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $visitTaskDetailImage->id !!}</p>
</div>

<!-- Visit Detail Id Field -->
<div class="form-group">
    {!! Form::label('visit_detail_id', 'Visit Detail Id:') !!}
    <p>{!! $visitTaskDetailImage->visit_detail_id !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $visitTaskDetailImage->note !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $visitTaskDetailImage->image !!}</p>
</div>

