@extends('layouts.backend')
@section('pageTitle', '404')
@section('breadcrumb')
<li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>
@endsection

@section('js')
@endsection
@section('content')

<div class="panel panel-default">
    <div class="col-md-7 page-404 col-md-offset-4">
        <div class="number font-green"><h1>404</h1> </div>
        <div class="details">
            <h3>Oops! You're lost.</h3>
            <p> We can not find the page you're looking for.
                <br>
                <a href="{{url('admin')}}"> Return home </a> or try the search bar below. </p>
        </div>
    </div>
</div>

@endsection