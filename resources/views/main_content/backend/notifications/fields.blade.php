 <div class="form-body">


     <div class="form-group row">
         {!! Form::label('', 'ارسال الي',['class' => 'col-sm-2 col-form-label']) !!}
         <div class="col-sm-10">
             <select name="type_select"  class="form-control" id="type_select">
                 <option value="0">كل الموظفين</option>
                 <option value="1">كل المشرفيين</option>
             </select>

         </div>
     </div>


     <div class="form-group row" id="agents" >
         {!! Form::label('body_en', 'الموظفين',['class' => 'col-sm-2 col-form-label']) !!}
         <div class="col-sm-10">
             <select name="agents[]" data-placeholder="اختار الموظفين" class="chosen-select form-control" id="agents_select" >
                 <option value="">الكل</option>
                 @foreach(\App\Models\Agent::all() as $item)
                     <option value="{{$item->id}}" >{{$item->name}} </option>
                 @endforeach
             </select>
         </div>
     </div>

     <div class="form-group row" id="supervisors" >
         {!! Form::label('', 'المشرفيين',['class' => 'col-sm-2 col-form-label']) !!}
         <div class="col-sm-10">
             <select name="supervisors[]" data-placeholder="اختار المشرفيين" class="chosen-select form-control" id="supervisors_select" >
                 <option value="">الكل</option>
                 @foreach(\App\Models\Supervisor::all() as $item)
                     <option value="{{$item->id}}" >{{$item->name}} </option>
                 @endforeach
             </select>
         </div>
     </div>

<!-- Body Field -->
     <div class="form-group row">
     {!! Form::label('body', 'المحتوي',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('body', null, ['class' => 'form-control','id'=>"body","required"]) !!}
    </div>
 </div>

<!-- Type Field -->
{{--     <div class="form-group row">--}}
{{--     {!! Form::label('type', 'Type:',['class' => 'col-sm-2 col-form-label']) !!}--}}
{{--    <div class="col-sm-10">--}}
{{--    {!! Form::text('type', null, ['class' => 'form-control','id'=>"type","required"]) !!}--}}
{{--    </div>--}}
{{-- </div>--}}

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! url("admin") !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>


<script>
    $("#agents_select").chosen();
    $("#supervisors_select").chosen();

    $( document ).ready(function() {
        var check_type = $('#type_select').val();
        if (check_type == 0) {
            $("#agents").fadeIn(1000);
            $("#supervisors").fadeOut(1000);
        }
        else {
            $("#agents").fadeOut(1000);
            $("#supervisors").fadeIn(1000);
        }

        $('#type_select').on('change', function (e) {

            var type = e.target.value;
            if (type == 0) {
                $("#agents").fadeIn(1000);
                $("#supervisors").fadeOut(1000);
            }
            else {
                $("#agents").fadeOut(1000);
                $("#supervisors").fadeIn(1000);
            }

        });
    });

</script>