

@extends('layouts.backend')

@section('pageTitle',"ارسال اشعار")

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

    <li><span> ارسال اشعار</span></li>


@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/notifications.js" type="text/javascript"></script>
@endsection
@section('content')
    <form role="form"  id="addForm" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">العنوان</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">
                    <input type="hidden" name="id" id="id" value="0">


                        <div class="form-group form-md-line-input col-md-12">
                                <select name="type_select"  class="form-control" id="type_select">
                                    <option value="0">كل الموظفين</option>
                                    <option value="1">كل المشرفيين</option>
                                </select>
                                {!! Form::label('', 'ارسال الي',[]) !!}
                            <span class="help-block"></span>

                        </div>
                        <div class="form-group form-md-line-input col-md-12" id="agents" >
                                <select name="agents[]" data-placeholder="اختار الموظفين" class="chosen-select form-control" id="agents_select" multiple>
                                    <option value="">الكل</option>
                                    @foreach(\App\Models\Agent::all() as $item)
                                        <option value="{{$item->id}}" >{{$item->name}} </option>
                                    @endforeach
                                </select>
                                {!! Form::label('body_en', 'الموظفين',['class' => 'col-sm-2 col-form-label']) !!}
                            <span class="help-block"></span>

                        </div>
                        <div class="form-group form-md-line-input col-md-12" id="supervisors" >
                                <select name="supervisors[]" data-placeholder="اختار المشرفيين" class="chosen-select form-control" id="supervisors_select" multiple>
                                    <option value="">الكل</option>
                                    @foreach(\App\Models\Supervisor::all() as $item)
                                        <option value="{{$item->id}}" >{{$item->name}} </option>
                                    @endforeach
                                </select>
                                {!! Form::label('', 'المشرفيين',['class' => 'col-sm-2 col-form-label']) !!}
                                <span class="help-block"></span>

                            </div>

                        <!-- Body Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input class="form-control" name="body" type="text" >
                            {!! Form::label('body', 'المحتوي') !!}
                            <span class="help-block"></span>
                            <br>

                        </div>



                    </div>


                </div>

            <div class="panel-footer text-center">
                <button type="button" class="btn btn-info submit-form"
                >{{_lang('app.save') }}</button>
            </div>

            </div>





    </form>
    <script>
        var new_lang = {

        };
        var new_config = {

        };

    </script>
@endsection
