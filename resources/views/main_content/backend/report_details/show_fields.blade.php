<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $reportDetail->id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $reportDetail->product_id !!}</p>
</div>

<!-- Num Of Cans Field -->
<div class="form-group">
    {!! Form::label('num_of_cans', 'Num Of Cans:') !!}
    <p>{!! $reportDetail->num_of_cans !!}</p>
</div>

<!-- Num Cartons Field -->
<div class="form-group">
    {!! Form::label('num_cartons', 'Num Cartons:') !!}
    <p>{!! $reportDetail->num_cartons !!}</p>
</div>

<!-- End Date Field -->
<div class="form-group">
    {!! Form::label('end_date', 'End Date:') !!}
    <p>{!! $reportDetail->end_date !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $reportDetail->note !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $reportDetail->title !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $reportDetail->type !!}</p>
</div>

