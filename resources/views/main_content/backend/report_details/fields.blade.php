 <div class="form-body">
<!-- Product Id Field -->
     <div class="form-group row">
     {!! Form::label('product_id', 'Product Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('product_id', null, ['class' => 'form-control','id'=>"product_id","required"]) !!}
    </div>
 </div>

<!-- Num Of Cans Field -->
     <div class="form-group row">
     {!! Form::label('num_of_cans', 'Num Of Cans:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('num_of_cans', null, ['class' => 'form-control','id'=>"num_of_cans","required"]) !!}
    </div>
 </div>

<!-- Num Cartons Field -->
     <div class="form-group row">
     {!! Form::label('num_cartons', 'Num Cartons:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('num_cartons', null, ['class' => 'form-control','id'=>"num_cartons","required"]) !!}
    </div>
 </div>

<!-- End Date Field -->
     <div class="form-group row">
     {!! Form::label('end_date', 'End Date:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('end_date', null, ['class' => 'form-control','id'=>"end_date","required"]) !!}
    </div>
 </div>

<!-- Note Field -->
     <div class="form-group row">
     {!! Form::label('note', 'Note:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('note', null, ['class' => 'form-control','id'=>"note","required"]) !!}
    </div>
 </div>

<!-- Title Field -->
     <div class="form-group row">
     {!! Form::label('title', 'Title:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('title', null, ['class' => 'form-control','id'=>"title","required"]) !!}
    </div>
 </div>

<!-- Type Field -->
     <div class="form-group row">
     {!! Form::label('type', 'Type:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('type', null, ['class' => 'form-control','id'=>"type","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('reportDetails.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
