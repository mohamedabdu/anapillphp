@extends('layouts.backend')

@section('pageTitle',_lang('app.edit_company'))

@section('breadcrumb')
<li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>
<li><a href="{{route('companies.index')}}">{{_lang('app.companies')}}</a> <i class="fa fa-circle"></i></li>
<li><span> {{_lang('app.edit')}}</span></li>
@endsection

@section('js')
<script src="{{url('public/backend/js')}}/companies.js" type="text/javascript"></script>
@endsection
@section('content')
<form role="form" id="addEditCompaniesForm" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.login_data')}}</h3>
        </div>
        <div class="panel-body">

            <div class="form-body">
                <input type="hidden" name="id" id="id" value="{{en_de_crypt($admin->id)}}">
                <div class="col-md-12">
                    <div class="form-group form-md-line-input col-md-6">
                        <input type="email" class="form-control" id="email" name="email" value="{{$admin->email}}">
                        <label for="email">{{_lang('app.email')}}</label>
                        <span class="help-block"></span>
                    
                    </div>
                    
                    <div class="form-group form-md-line-input col-md-6">
                        <div class="input-group input-group-sm">
                            <div class="input-group-control">
                                <input type="password" class="form-control input-sm" id="password" name="password"
                                    placeholder="{{_lang('app.password')}}">
                                <label for="password">{{_lang('app.password')}}</label>
                            </div>
                            <span class="input-group-btn btn-right">
                                <button class="btn green-haze" type="button" id="show-password">{{_lang('app.show')}}</button>
                                <button class="btn green-haze" type="button" id="random-password">{{_lang('app.rondom')}}</button>
                            </span>
                        </div>
                        <span class="help-block"></span>
                    </div>
                </div>
                

                <div class="col-md-12">
                    <div class="form-group form-md-line-input col-md-6">
                        <select class="form-control" name="role" id="role">
                            @foreach($roles as $role)
                            <option value="{{$role->id}}" {{en_de_crypt($admin->role_id)  == $role->id ? 'selected' : ''}}>
                                {{$role->title}}
                            </option>
                            @endforeach
                        </select>
                        <label for="role">{{_lang('app.role')}}</label>
                        <span class="help-block"></span>
                    </div>
                    
                    
                    <div class="form-group form-md-line-input col-md-6">
                        <select class="form-control" name="active" id="active">
                            <option value="0" {{$admin->active == 0 ? 'selected' : ''}}>{{_lang('app.deactivated')}}</option>
                            <option value="1" {{$admin->active == 1 ? 'selected' : ''}}>{{_lang('app.activated')}}</option>
                        </select>
                        <label for="status">{{_lang('app.status')}}</label>
                        <span class="help-block"></span>
                    </div>
                </div>
                

            </div>
        </div>

    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.personal_information')}}</h3>
        </div>
        <div class="panel-body">

            <div class="form-body">

                <!-- Role Field -->
                <div class="form-group form-md-line-input col-md-6">
                    <input type="text" class="form-control" id="name" name="name" value="{{$admin->name}}">
                    <label for="name">{{_lang('app.name')}}</label>
                    <span class="help-block"></span>

                </div>

                <!-- Role Field -->
                <div class="form-group form-md-line-input col-md-6">
                    <input type="text" class="form-control" id="mobile" name="mobile" value="{{$admin->mobile}}">
                    <label for="mobile">{{_lang('app.mobile')}}</label>
                    <span class="help-block"></span>

                </div>
                <div class="clearfix"></div>
                <div class="form-group form-md-line-input col-md-2">
                    <div class="image_box">
                        <img src="{{url('public/uploads/users/'.$admin->image)}}" width="100" height="80" class="image" />
                    </div>
                    <input type="file" name="image" id="image" style="visibility: hidden">
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
        <div class="panel-footer text-center">
            <button type="button" class="btn btn-info submit-form">{{_lang('app.save') }}</button>
        </div>

    </div>



</form>
<script>
    var new_lang = {

    };
    var new_config = {

    };

</script>
@endsection