 <div class="form-body">
<!-- User Id Field -->
     <div class="form-group row">
     {!! Form::label('user_id', 'User Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('user_id', null, ['class' => 'form-control','id'=>"user_id","required"]) !!}
    </div>
 </div>

<!-- Day Field -->
     <div class="form-group row">
     {!! Form::label('day', 'Day:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('day', null, ['class' => 'form-control','id'=>"day","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('agentWorkDays.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
