@extends('layouts.backend')

@section('pageTitle',_lang('app.add_competitor'))

@section('breadcrumb')
<li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>
<li><a href="{{route('competitors.index')}}">{{_lang('app.competitors')}}</a> <i class="fa fa-circle"></i></li>
<li><span>{{_lang('app.create')}}</span></li>
@endsection

@section('js')
<script src="{{url('public/backend/js')}}/competitors.js" type="text/javascript"></script>
@endsection

@section('content')
<form role="form"  id="addEditCompetitorsForm" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.name') }}</h3>
        </div>
        <div class="panel-body">


            <div class="form-body">
                <input type="hidden" name="id" id="id" value="0">

                @foreach ($languages as $key => $value)

                <div class="form-group form-md-line-input col-md-6">
                    <input type="text" class="form-control" id="title[{{ $key }}]" name="title[{{ $key }}]" value="">
                    <label for="title">{{_lang('app.title') }} {{ _lang('app. '.$value.'') }}</label>
                    <span class="help-block"></span>
                </div>

                @endforeach
               

            </div>
        </div>
      

    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.location') }}</h3>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div class="form-group form-md-line-input col-md-3 select">
                    <select class="form-control edited" id="country" name="country">
                        <option value="">{{ _lang('app.choose') }}</option>
                        @foreach ($countries as $country)
                            <option value="{{$country->id}}">{{ $country->title }}</option>
                        @endforeach
                    </select>
                    <label for="country">{{_lang('app.country') }}</label>
                    <span class="help-block"></span>
                </div>

                <div class="form-group form-md-line-input col-md-3 select">
                    <select class="form-control edited" id="region" name="region">
                        <option value="">{{ _lang('app.choose') }}</option>
                    </select>
                    <label for="region">{{_lang('app.region') }}</label>
                    <span class="help-block"></span>
                </div>

                <div class="form-group form-md-line-input col-md-3 select">
                    <select class="form-control edited" id="city" name="city">
                        <option value="">{{ _lang('app.choose') }}</option>
                    </select>
                    <label for="city">{{_lang('app.city') }}</label>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
    
       
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.category') }}</h3>
        </div>
        <div class="panel-body">
            <div class="form-body">
    
                <div class="form-group form-md-line-input col-md-3 select">
                    <select class="form-control edited" id="category" name="category">
                        <option value="">{{ _lang('app.choose') }}</option>
                        @foreach ($categories as $category)
                        <option value="{{$category->id}}">{{ $category->title }}</option>
                        @endforeach
                    </select>
                    <label for="category">{{_lang('app.category') }}</label>
                    <span class="help-block"></span>
                </div>
    
    
                <div class="form-group form-md-line-input col-md-3 select">
                    <select class="form-control edited" id="active" name="active">
                        <option value="0">{{ _lang('app.deactivated') }}</option>
                        <option value="1">{{ _lang('app.activated') }}</option>
    
                    </select>
                    <label for="status">{{_lang('app.status') }}</label>
                    <span class="help-block"></span>
                </div>
    
            </div>
        </div>
    
        <div class="panel-footer text-center">
            <button type="button" class="btn btn-info submit-form">{{_lang('app.save') }}</button>
        </div>
    
    
    </div>


</form>
<script>
var new_lang = {

};
var new_config = {
    
};

</script>
@endsection