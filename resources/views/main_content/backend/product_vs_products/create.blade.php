

@extends('layouts.backend')

@section('pageTitle',"اضافة منتج")

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

    <li><a href="{{route('productVsProducts.index')}}">المنتجات المنافسة</a> <i class="fa fa-circle"></i></li>

    <li><span> اضافة</span></li>

    <li><span> منتج</span></li>

@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/productVsProduct.js" type="text/javascript"></script>
@endsection
@section('content')
    <form role="form"  id="addForm" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">المنتجات</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">
                    <input type="hidden" name="id" id="id" value="0">


                    <div class="form-group form-md-line-input col-md-12">
                            <select class="form-control" id="product" name="product_id" required>
                                <option value="0">Select</option>
                                @foreach($normalProduct as $product)
                                    <option value="{{$product->id}}" @if(isset($productVsProduct) && $productVsProduct->product_id == $product->id) selected @endif>{{$product->title_ar}}</option>
                                @endforeach
                            </select>
                            {!! Form::label('product_id', 'المنتج',['class' => 'col-sm-2 col-form-label']) !!}
                        <span class="help-block"></span>

                    </div>

                    <!-- Product Vs Id Field -->
                    <div class="form-group form-md-line-input col-md-12">
                            <select class="form-control chosen-select" data-placeholder="اختار المنتجات" id="product_vs_id" name="product_vs_id[]" multiple>
                                <option disabled="" value="0">Select</option>
                                @foreach($compProduct as $product)
                                    <option value="{{$product->id}}" @if(isset($productVsProduct) && $productVsProduct->product_vs_id == $product->id) selected @endif>{{$product->title_ar}}</option>
                                @endforeach
                            </select>
                            {!! Form::label('product_vs_id', 'المنتج المنافس',['class' => 'col-sm-2 col-form-label']) !!}
                        <span class="help-block"></span>

                    </div>

                </div>


                </div>
            </div>

            <div class="panel-footer text-center">
                <button type="button" class="btn btn-info submit-form"
                >{{_lang('app.save') }}</button>
            </div>

        </div>


    </form>
    <script>
        var new_lang = {

        };
        var new_config = {

        };

    </script>
@endsection
