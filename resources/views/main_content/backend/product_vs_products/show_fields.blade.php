<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $productVsProduct->id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $productVsProduct->product_id !!}</p>
</div>

<!-- Product Vs Id Field -->
<div class="form-group">
    {!! Form::label('product_vs_id', 'Product Vs Id:') !!}
    <p>{!! $productVsProduct->product_vs_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $productVsProduct->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $productVsProduct->updated_at !!}</p>
</div>

