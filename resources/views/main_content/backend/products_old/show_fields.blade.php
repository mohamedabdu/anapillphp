<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'رقم المنتج') !!}
    <p>{!! $product->id !!}</p>
</div>

<!-- Title Ar Field -->
<div class="form-group">
    {!! Form::label('title_ar', 'الأسم عربي') !!}
    <p>{!! $product->title_ar !!}</p>
</div>

<!-- Title En Field -->
<div class="form-group">
    {!! Form::label('title_en', 'الأسم انجليزي') !!}
    <p>{!! $product->title_en !!}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'القسم') !!}
    @if(isset($product->category))
        <p>{!! $product->category->title_ar !!}</p>
        @else
        <p></p>
    @endif
</div>

<!-- Sub Category Id Field -->
<div class="form-group">
    {!! Form::label('sub_category_id', 'القسم الفرعي') !!}
    @if(isset($product->subCategory))
        <p>{!! $product->subCategory->title_ar !!}</p>
    @else
        <p></p>
    @endif
</div>

<!-- Ordered Field -->
<div class="form-group">
    {!! Form::label('ordered', 'الترتيب') !!}
    <p>{!! $product->ordered !!}</p>
</div>


<!-- Ordered Field -->
<div class="form-group">
    {!! Form::label('ordered', 'الصور') !!}
    <div class="row">
        @foreach($product->images as $image)
            <div class="col-md-4"> <img src="{{\Helper\Common\imageUrl($image->image)}}" height="400" class="img-responsive col-md-12"> </div>
        @endforeach
    </div>
    <br><br>
</div>


