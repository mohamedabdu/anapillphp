

@extends('layouts.backend')

@section('pageTitle',"تعديل منتج")

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

    @if($product->competitor_id == null)
    <li><a href="{{route('products.index')}}?is_competitor_product=">المنتجات</a> <i class="fa fa-circle"></i></li>

    @else
        <li><a href="{{route('products.index')}}?is_competitor_product=true">المنتجات</a> <i class="fa fa-circle"></i></li>

    @endif
    <li><span> تعديل</span></li>

    <li><span> منتج</span></li>

@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/products.js" type="text/javascript"></script>
@endsection
@section('content')
    <form role="form"  id="addForm" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">العنوان</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">
                    <input type="hidden" name="id" id="id" value="{{$product->id}}">

                    <div class="form-body">

                        <!-- Role Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="title_ar" name="title_ar" value="{{$product->title_ar}}">
                            <label for="title">العنوان عربي</label>
                            <span class="help-block"></span>

                        </div>
                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="title_en" name="title_en" value="{{$product->title_en}}">
                            <label for="title_en">العنوان انجليزي</label>
                            <span class="help-block"></span>
                        </div>

                        <!-- Role Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="ordered" name="ordered" value="{{$product->ordered}}">
                            <label for="ordered">الترتيب</label>
                            <span class="help-block"></span>

                        </div>


                    </div>
                </div>
            </div>

        </div>


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">القسم</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control" id="category_select" name="category_id" >
                            <option value="">Select</option>
                            @foreach(\App\Models\Category::where("type",\App\Models\Category::$types["store"])->get() as $category)
                                <option value="{{$category->id}}" @if(isset($product) && $product->category_id == $category->id) selected @endif>{{$category->title_ar}}</option>
                            @endforeach
                        </select>
                        <label for="title">القسم</label>
                        <span class="help-block"></span>

                    </div>
                    <!-- title en Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control" name="sub_category_id" id="sub_category_select"  >
                            @if($product->has_sub_sub == false)
                                @foreach(\App\Models\SubCategory::where("category_id",$product->category_id)->where("parent_id",0)->get() as $subCategory)
                                    <option value="{{$subCategory->id}}" @if($subCategory->id == $product->sub_category_id) selected @endif>{{$subCategory->title_ar}}</option>
                                @endforeach
                            @else
                                @foreach(\App\Models\SubCategory::where("id",$product->sub_category)->get() as $subCategory)
                                    <option value="{{$subCategory->id}}" @if($subCategory->id == $product->sub_category_id) selected @endif>{{$subCategory->title_ar}}</option>
                                @endforeach
                            @endif
                        </select>
                        <label for="title_en">القسم الفرعي</label>
                        <span class="help-block"></span>
                    </div>

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control" name="sub_sub_category_id" id="sub_sub_category_select" >

                            @if($product->has_sub_sub == true)
                                @foreach(\App\Models\SubCategory::where("category_id",$product->category_id)->where("parent_id",">",0)->get() as $subCategory)
                                    <option value="{{$subCategory->id}}" @if($subCategory->id == $product->sub_category_id) selected @endif>{{$subCategory->title_ar}}</option>
                                @endforeach
                            @endif
                        </select>
                        <label for="title">القسم تحت الفرعي</label>
                        <span class="help-block"></span>

                    </div>


                </div>
            </div>


        </div>


        @if(isset($is_competitor_product))

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">المنافس</h3>
                </div>
                <div class="panel-body">


                    <div class="form-body">

                        <!-- competitort Id Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <select class="form-control" id="competitor_id" name="competitor_id">
                                <option value="0">Select</option>
                                @foreach($competitors as $item)
                                    <option value="{{$item->id}}" @if(isset($product) && $product->competitor_id == $item->id) selected @endif>{{$item->title_ar}}</option>
                                @endforeach
                            </select>
                            {!! Form::label('competitor_id', 'المنافس',['class' => 'col-sm-2 col-form-label']) !!}
                            <span class="help-block"></span>

                        </div>




                    </div>
                </div>


            </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">الصور</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">


                    @for ($i = 0; $i < 6; $i++)
                        <div class="form-group form-md-line-input col-md-2">
                            <div class="image_{{$i}}_box">
                                @if (isset($product->images[$i]) && $i != 0)
                                    <a href="javascript:;" class="remove_image_link" onclick="My.deleteImage(this);return false;"
                                       data-model="Product" data-folder="products" data-col="images"
                                       data-image="{{ isset($product->images[$i]) ? $product->images[$i]->image : '' }}">
                                        <img src="{{ url('delete-btn.png') }}" /></a>
                                @endif
                                <img src="{{ isset($product->images[$i]) ? \Helper\Common\imageUrl($product->images[$i]->image) : url('no-image.png') }}"
                                     width="100" height="80" class="{{ isset($product->images[$i]) && $i != 0 ? '' : 'image_'.$i.'' }}" />
                            </div>
                            <input type="file" name="images[{{$i}}]" id="image_{{$i}}" style="visibility: hidden">
                            <span class="help-block"></span>
                        </div>
                    @endfor


                    <div class="clearfix"></div>



                </div>
            </div>

            <div class="panel-footer text-center">
                <button type="button" class="btn btn-info submit-form"
                >{{_lang('app.save') }}</button>
            </div>

        </div>


    </form>
    <script>
        var new_lang = {

        };
        var new_config = {

        };

    </script>
@endsection
