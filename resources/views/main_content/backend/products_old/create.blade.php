@extends('layouts.backend')

@section('pageTitle',"اضافة منتج")

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

    <li><a href="{{route('products.index')}}?is_competitor_product={{$is_competitor_product}}">المنتجات</a> <i class="fa fa-circle"></i></li>

    <li><span> اضافة</span></li>

    <li><span> منتج</span></li>

@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/products.js" type="text/javascript"></script>
@endsection
@section('content')
    <form role="form"  id="addForm" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">العنوان</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">
                    <input type="hidden" name="id" id="id" value="0">

                    <div class="form-body">

                        <!-- Role Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="title_ar" name="title_ar" value="">
                            <label for="title">العنوان عربي</label>
                            <span class="help-block"></span>

                        </div>
                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="title_en" name="title_en" value="">
                            <label for="title_en">العنوان انجليزي</label>
                            <span class="help-block"></span>
                        </div>

                        <!-- Role Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="ordered" name="ordered" value="">
                            <label for="ordered">الترتيب</label>
                            <span class="help-block"></span>

                        </div>


                    </div>
                </div>
            </div>

        </div>


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">القسم</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control" id="category_select" name="category_id" >
                            <option value="">Select</option>
                            @foreach(\App\Models\Category::where("type",\App\Models\Category::$types["store"])->get() as $category)
                                <option value="{{$category->id}}" @if(isset($store) && $store->category_id == $category->id) selected @endif>{{$category->title_ar}}</option>
                            @endforeach
                        </select>
                        <label for="title">القسم</label>
                        <span class="help-block"></span>

                    </div>
                    <!-- title en Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control" name="sub_category_id" id="sub_category_select"  >
                            <option value="">Select</option>

                        </select>
                        <label for="title_en">القسم الفرعي</label>
                        <span class="help-block"></span>
                    </div>

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control" name="sub_sub_category_id" id="sub_sub_category_select" >

                        </select>
                        <label for="title">القسم تحت الفرعي</label>
                        <span class="help-block"></span>

                    </div>


                </div>
            </div>


        </div>


        @if(isset($is_competitor_product))

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">المنافس</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">

                    <!-- competitort Id Field -->
                        <div class="form-group form-md-line-input col-md-12">
                                <select class="form-control" id="competitor_id" name="competitor_id">
                                    <option value="0">Select</option>
                                    @foreach($competitors as $item)
                                        <option value="{{$item->id}}" @if(isset($product) && $product->competitor_id == $item->id) selected @endif>{{$item->title_ar}}</option>
                                    @endforeach
                                </select>
                                {!! Form::label('competitor_id', 'المنافس',['class' => 'col-sm-2 col-form-label']) !!}
                            <span class="help-block"></span>

                        </div>




                </div>
            </div>


        </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">الصور</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">


                    @for ($i = 0; $i < 6; $i++)
                        <div class="form-group form-md-line-input col-md-2">
                            <div class="image_{{$i}}_box">
                                <img src="{{url('no-image.png')}}" width="100" height="80" class="image_{{$i}}" />
                            </div>
                            <input type="file" name="images[{{$i}}]" id="image_{{$i}}" style="visibility: hidden">
                            <span class="help-block"></span>
                        </div>
                    @endfor

                    <div class="clearfix"></div>



                </div>
            </div>

            <div class="panel-footer text-center">
                <button type="button" class="btn btn-info submit-form"
                >{{_lang('app.save') }}</button>
            </div>

        </div>


    </form>
    <script>
        var new_lang = {

        };
        var new_config = {

        };

    </script>
@endsection
