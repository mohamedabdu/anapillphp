 <div class="form-body">
     <!-- Title Ar Field -->
     <div class="form-group row">
         {!! Form::label('title_ar', 'الأسم بالعربي',['class' => 'col-sm-2 col-form-label']) !!}
         <div class="col-sm-10">
             {!! Form::text('title_ar', null, ['class' => 'form-control','id'=>"title_ar","required"]) !!}
         </div>
     </div>

     <!-- Title En Field -->
     <div class="form-group row">
         {!! Form::label('title_en', 'الأسم بالانجليزي',['class' => 'col-sm-2 col-form-label']) !!}
         <div class="col-sm-10">
             {!! Form::text('title_en', null, ['class' => 'form-control','id'=>"title_en","required"]) !!}
         </div>
     </div>


     <!-- Category Id Field -->
     <div class="form-group row">
         {!! Form::label('category_id', 'القسم الرئيسي',['class' => 'col-sm-2 col-form-label']) !!}
         <div class="col-sm-10">
             <select class="form-control" id="category_select" name="category_id" required>
                 <option value="0">Select</option>
                 @foreach(\App\Models\Category::where("type",\App\Models\Category::$types["product"])->get() as $category)
                     <option value="{{$category->id}}" @if(isset($product) && $product->category_id == $category->id) selected @endif>{{$category->title_ar}}</option>
                 @endforeach
             </select>
         </div>
     </div>
     <!-- sub Categories Id Field -->

     @if(!isset($product))
         <div class="form-group row">
             {!! Form::label('sub_category', 'القسم الفرعي',['class' => 'col-sm-2 col-form-label']) !!}
             <div class="col-sm-10">
                 <select class="form-control" name="sub_category_id" id="sub_category_select" required>
                     <option value="0">Select</option>

                 </select>
             </div>
         </div>
         <!-- sub sub Categories Id Field -->

         <div class="form-group row">
             {!! Form::label('sub_category', 'قسم فرع الفرعي',['class' => 'col-sm-2 col-form-label']) !!}
             <div class="col-sm-10">
                 <select class="form-control" name="sub_sub_category_id" id="sub_sub_category_select">
                     <option value="0">Select</option>

                 </select>
             </div>
         </div>
     @else
         @php /** @var \App\Models\Competitor $competitor **/ @endphp
         <div class="form-group row">
             {!! Form::label('sub_category', 'القسم الفرعي',['class' => 'col-sm-2 col-form-label']) !!}
             <div class="col-sm-10">
                 <select class="form-control" name="sub_category_id" id="sub_category_select" required>
                     @if($product->has_sub_sub == false)
                         @foreach(\App\Models\SubCategory::where("category_id",$product->category_id)->where("parent_id",0)->get() as $subCategory)
                             <option value="{{$subCategory->id}}" @if($subCategory->id == $product->sub_category_id) selected @endif>{{$subCategory->title_ar}}</option>
                         @endforeach
                     @else
                         @foreach(\App\Models\SubCategory::where("id",$product->sub_category)->get() as $subCategory)
                             <option value="{{$subCategory->id}}" @if($subCategory->id == $product->sub_category_id) selected @endif>{{$subCategory->title_ar}}</option>
                         @endforeach
                     @endif
                 </select>
             </div>
         </div>
         <!-- sub sub Categories Id Field -->
         <div class="form-group row">
             {!! Form::label('sub_category', 'قسم فرع الفرعي',['class' => 'col-sm-2 col-form-label']) !!}
             <div class="col-sm-10">
                 <select class="form-control" name="sub_sub_category_id" id="sub_sub_category_select">

                     @if($product->has_sub_sub == true)
                         @foreach(\App\Models\SubCategory::where("category_id",$product->category_id)->where("parent_id",">",0)->get() as $subCategory)
                             <option value="{{$subCategory->id}}" @if($subCategory->id == $product->sub_category_id) selected @endif>{{$subCategory->title_ar}}</option>
                         @endforeach
                     @endif
                 </select>
             </div>
         </div>
 @endif


<!-- Ordered Field -->
     <div class="form-group row">
     {!! Form::label('ordered', 'الترتيب',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('ordered', null, ['class' => 'form-control','id'=>"ordered","required"]) !!}
    </div>
 </div>


     @if(isset($is_competitor_product))
     <!-- competitort Id Field -->
         <div class="form-group row">
             {!! Form::label('competitor_id', 'المنافس',['class' => 'col-sm-2 col-form-label']) !!}
             <div class="col-sm-10">
                 <select class="form-control" id="competitor_id" name="competitor_id">
                     <option value="0">Select</option>
                     @foreach($competitors as $item)
                         <option value="{{$item->id}}" @if(isset($product) && $product->competitor_id == $item->id) selected @endif>{{$item->title_ar}}</option>
                     @endforeach
                 </select>
             </div>
         </div>

         @endif
     <!-- images Field -->

     <div class="input-group mb-3">
         <div class="custom-file">
             <input type="file" name="images[]" multiple class="custom-file-input" id="inputGroupFile02">
             <label class="custom-file-label" for="inputGroupFile02">Images</label>
         </div>

     </div>

     @if(isset($product))
         <div class="row">
             @foreach($product->images as $image)
                 <div class="col-md-4"> <img src="{{\Helper\Common\imageUrl($image->image)}}" class="img-responsive col-md-12"> </div>
             @endforeach
         </div>
         <br><br>

     @endif

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('products.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>



 <script>
     $( document ).ready(function() {

         $('#category_select').on('change', function (e) {

             var category = e.target.value;
             if (category > 0) {
                 $.get(appUrl + '/subCategories/ajax/' + category, function (data) {

                     $('#sub_category_select').empty();
                     $('#sub_category_select').append($('<option>', {
                         value: '',
                         text: "اختار",
                         selected: true
                     }));
                     if (data.length !== 0) {
                         $.each(data, function (index, subCatObj) {

                             $('#sub_category_select').append($('<option>', {
                                 value: subCatObj.id,
                                 text: subCatObj.title_ar
                             }));

                         });

                     } else {
                         $('#sub_category_select').empty();
                         $('#sub_category_select').append($('<option>', {
                             value: '',
                             text: "اختار",
                             selected: true
                         }));
                     }

                 });
             }
             else {

                 $('#sub_category_select').empty();
                 $('#city_select').append($('<option>', {
                     value: '',
                     text: "اختار",
                     selected: true
                 }));
             }

         });
         $('#sub_category_select').on('change', function (e) {

             var subIds = [];
             $('#sub_category_select').each(function() {
                 subIds.push($(this).val());
             });
             $.get(appUrl + '/subCategories/ajax/sub/' + subIds, function (data) {

                 $('#sub_sub_category_select').empty();
                 $('#sub_sub_category_select').append($('<option>', {
                     value: '',
                     text: "اختار",
                     selected: true
                 }));
                 if (data.length !== 0) {
                     $.each(data, function (index, subCatObj) {

                         $('#sub_sub_category_select').append($('<option>', {
                             value: subCatObj.id,
                             text: subCatObj.title_ar
                         }));

                     });

                 } else {
                     $('#sub_sub_category_select').empty();
                     $('#sub_sub_category_select').append($('<option>', {
                         value: '',
                         text: "اختار",
                         selected: true
                     }));

                 }

             });

         });
     });


 </script>
