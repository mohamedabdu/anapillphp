@extends('layouts.backend')

@section('pageTitle',"تعديل")

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>
    <li><span> تعديل</span></li>
    <li><span> موظف</span></li>

@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/profile.js" type="text/javascript"></script>

@endsection

@section('content')
    <form role="form" id="addForm" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">البيانات الأساسية</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">
                    <input type="hidden" name="id" id="id" value="{{$user->id}}">


                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}">
                        <label for="name">الأسم</label>
                        <span class="help-block"></span>

                    </div>

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="mobile" name="mobile" value="{{$user->mobile}}">
                        <label for="mobile">رقم الجوال</label>
                        <span class="help-block"></span>

                    </div>

                </div>
            </div>

        </div>


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">البيانات الدخول</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="email" name="email" value="{{$user->email}}">
                        <label for="email">البريد الألكتروني</label>
                        <span class="help-block"></span>

                    </div>

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="password" class="form-control" id="password" name="password" value="">
                        <label for="password">كلمة السر</label>
                        <span class="help-block"></span>

                    </div>




                </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">الصورة</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">

                    <div class="form-group form-md-line-input col-md-2">
                        <div class="image_0_box">
                            <img src="{{ isset($user->image) ? \Helper\Common\imageUrl($user->image) : url('no-image.png') }}"
                                 width="100" height="80" class="image_0" />
                        </div>
                        <input type="file" name="image" id="image_0" style="visibility: hidden" >
                        <span class="help-block"></span>
                    </div>


                </div>
            </div>
            <div class="panel-footer text-center">
                <button type="button"  class="btn btn-info submit-form"
                >{{_lang('app.save') }}</button>
            </div>

        </div>




    </form>
    <script>
        var new_lang = {

        };
        var new_config = {

        };

    </script>
@endsection

