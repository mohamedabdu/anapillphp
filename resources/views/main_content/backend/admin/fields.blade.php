<div class="form-body">
    <!-- Title Ar Field -->
    <div class="form-group row">
        {!! Form::label('name', 'الأسم',['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('name', null, ['class' => 'form-control','id'=>"name","required"]) !!}
        </div>
    </div>
    <!-- Title Ar Field -->
    <div class="form-group row">
        {!! Form::label('email', 'البريد الألكتروني',['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('email', null, ['class' => 'form-control','id'=>"email","required"]) !!}
        </div>
    </div>
    <!-- Title Ar Field -->
    <div class="form-group row">
        {!! Form::label('mobile', 'رقم الجوال',['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('mobile', null, ['class' => 'form-control','id'=>"mobile","required"]) !!}
        </div>
    </div>
    <!-- Title Ar Field -->
    <div class="form-group row">
        {!! Form::label('password', 'كلمة السر',['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            <input type="password" class="form-control" name="password">
        </div>
    </div>
    <!-- Image Field -->


    <!-- Image Field -->

    <div class="input-group mb-3">
        <div class="custom-file">
            <input type="file" name="image" class="custom-file-input" id="inputGroupFile02">
            <label class="custom-file-label" for="inputGroupFile02">Image</label>
        </div>

    </div>

</div>

@if(isset($user))
    <div class="row">
        <div class="col-md-4"> <img src="{{\Helper\Common\imageUrl($user->image)}}" class="img-responsive col-md-12"> </div>
    </div>
    <br><br>
@endif

<div class="form-group row">
    <label class="col-sm-2 col-form-label"></label>
    <div class="col-sm-10">
        <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
        <a href="{!! route('subAdmins.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

    </div>
</div>
