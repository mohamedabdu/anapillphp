<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $permission->id !!}</p>
</div>

<!-- Permission Field -->
<div class="form-group">
    {!! Form::label('permission', 'Permission:') !!}
    <p>{!! $permission->permission !!}</p>
</div>

<!-- Segment Field -->
<div class="form-group">
    {!! Form::label('segment', 'Segment:') !!}
    <p>{!! $permission->segment !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $permission->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $permission->updated_at !!}</p>
</div>

