@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9">
                <h4 class="page-title">Permissions</h4>
            </div>
            <div class="col-sm-3">
                <div class="btn-group float-sm-right">
                    <button type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-cog mr-1"></i> Setting</button>
                    <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown">
                        <span class="caret"></span>
                    </button>
                    <div class="dropdown-menu">
                        <a  href="{!! route('permissions.create') !!}" class="dropdown-item">اضافة جديد</a>
                        <a onclick="event.preventDefault(); deleteRecord('permissions',datatable)" class="dropdown-item">حذف</a>
                    </div>
                </div>
            </div>
        </div>

    {!! Form::open(['route' => ['permissions.delete'], 'method' => 'delete','id' => 'delete-form']) !!}

    {{ csrf_field() }}

        <!-- End Breadcrumb-->

                        @include('permissions.table')


        {!! Form::close() !!}



    </div>
    <!-- End container-fluid-->
@endsection


