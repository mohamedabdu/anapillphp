 <div class="form-body">
<!-- Permission Field -->
     <div class="form-group row">
     {!! Form::label('permission', 'Permission:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('permission', null, ['class' => 'form-control','id'=>"permission","required"]) !!}
    </div>
 </div>

<!-- Segment Field -->
     <div class="form-group row">
     {!! Form::label('segment', 'Segment:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('segment', null, ['class' => 'form-control','id'=>"segment","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('permissions.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
