 <div class="form-body">
<!-- Product Id Field -->
     <div class="form-group row">
     {!! Form::label('product_id', 'المنتج',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        <select class="form-control" id="product" name="product_id" required>
            <option value="0">Select</option>
            @foreach(\App\Models\Product::all() as $product)
                <option value="{{$product->id}}" @if(isset($productCompetitor) && $productCompetitor->product_id == $product->id) selected @endif>{{$product->title_ar}}</option>
            @endforeach
        </select>
    </div>
 </div>

<!-- Competitor Id Field -->
     <div class="form-group row">
     {!! Form::label('competitor_id', 'المنافس',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        <select class="form-control" id="competitor" name="competitor_id" required>
            <option value="0">Select</option>
            @foreach(\App\Models\Competitor::all() as $competitor)
                <option value="{{$competitor->id}}" @if(isset($productCompetitor) && $productCompetitor->competitor_id == $competitor->id) selected @endif>{{$competitor->title_ar}}</option>
            @endforeach
        </select>
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('productCompetitors.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
