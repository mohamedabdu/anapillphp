<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $productCompetitor->id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $productCompetitor->product_id !!}</p>
</div>

<!-- Competitor Id Field -->
<div class="form-group">
    {!! Form::label('competitor_id', 'Competitor Id:') !!}
    <p>{!! $productCompetitor->competitor_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $productCompetitor->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $productCompetitor->updated_at !!}</p>
</div>

