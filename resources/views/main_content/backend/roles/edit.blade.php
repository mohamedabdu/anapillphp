@extends('layouts.backend')

@section('pageTitle',_lang('app.edit_role'))

@section('breadcrumb')
<li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>
<li><a href="{{route('roles.index')}}{{$role->is_company ? "?is_company=$role->is_company" : ""}}">{{$role->is_company ? _lang('app.company_roles') : _lang('app.roles')}}</a> <i class="fa fa-circle"></i></li>
<li><span> {{_lang('app.edit')}}</span></li>
@endsection

@section('js')
<script src="{{url('public/backend/js')}}/roles.js" type="text/javascript"></script>
@endsection
@section('content')
<form role="form" id="addEditRolesForm" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.title')}}</h3>
        </div>
        <div class="panel-body">


            <div class="form-body">
                <input type="hidden" name="id" id="id" value="{{en_de_crypt($role->id)}}">

                <!-- Role Field -->
                @foreach ($languages as $key => $value)
                <div class="form-group form-md-line-input col-md-6">
                    <input type="text" class="form-control" id="title[{{ $key }}]" name="title[{{ $key }}]"
                        value="{{ $translations["$key"]->title }}">
                    <label for="title">{{_lang('app.title') }} {{ _lang('app.'.$value) }}</label>
                    <span class="help-block"></span>
                </div>
                @endforeach

                <div class="form-group form-md-line-input col-md-12">
                    @foreach($permissions as $permission)
                    <div class="form-group col-md-2" style="padding-top:30px; margin-left:5%">
                        <label class="checkbox-inline">
                            <input type="checkbox" data-toggle="toggle" data-style="ios" 
                                name="permissions[]" value="{{$permission->id}}" 
                                {{in_array($permission->id,$role_permissions) ? "checked" : "" }}>
                                {{$permission->permission }}
                        </label>
                    </div>

                    @endforeach
                    <span class="help-block"></span>
                </div>
            </div>
        </div>

        <div class="panel-footer text-center">
            <button type="button" class="btn btn-info submit-form">{{_lang('app.save') }}</button>
        </div>

    </div>



</form>
<script>
    var new_lang = {

    };
    var new_config = {

    };

</script>
@endsection