 <div class="form-body">
<!-- Agent Id Field -->
     <div class="form-group row">
     {!! Form::label('agent_id', 'Agent Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('agent_id', null, ['class' => 'form-control','id'=>"agent_id","required"]) !!}
    </div>
 </div>

<!-- Rate Field -->
     <div class="form-group row">
     {!! Form::label('rate', 'Rate:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('rate', null, ['class' => 'form-control','id'=>"rate","required"]) !!}
    </div>
 </div>

<!-- Note Field -->
     <div class="form-group row">
     {!! Form::label('note', 'Note:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('note', null, ['class' => 'form-control','id'=>"note","required"]) !!}
    </div>
 </div>

<!-- Date Field -->
     <div class="form-group row">
     {!! Form::label('date', 'Date:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('date', null, ['class' => 'form-control','id'=>"date","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('agentRates.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
