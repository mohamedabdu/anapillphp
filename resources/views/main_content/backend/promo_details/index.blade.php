



@extends('layouts.backend')

@section('pageTitle', _lang('app.promo_details'))

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>


    <li><span>تفاصيل العروض</span></li>


@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/promoDetail.js" type="text/javascript"></script>
@endsection
@section('content')


    <div class = "panel panel-default">
        {{ csrf_field() }}
        <div class = "panel-body">


            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">
                            <a class="btn green" style="margin-bottom: 40px;" href = "{{ route('promoDetails.create') }}" onclick="">{{ _lang('app.add_new')}}<i class="fa fa-plus"></i> </a>
                        </div>
                    </div>
                </div>
            </div>

            <table class = "table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer">
                <thead>
                <tr>
                    <th>العرض</th>
                    <th>المحل</th>
                    <th>العدد الخاص بالعرض</th>
                    <th>تاريخ العرض</th>
                    <th>تاريخ انتهاء العرض</th>
                    <th>العمليات</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

            <!--Table Wrapper Finish-->
        </div>
    </div>
    <script>
        var new_lang = {

        };
        var new_config = {

        };
    </script>
@endsection
