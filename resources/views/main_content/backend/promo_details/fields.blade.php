 <div class="form-body">
<!-- Promo Id Field -->
     <div class="form-group row">
     {!! Form::label('promo_id', 'العرض',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        <select class="form-control" name="promo_id">
            @foreach($promos as $promo)
                <option value="{{$promo->id}}">{{$promo->title_ar}}</option>
                @endforeach
        </select>
    </div>
 </div>

<!-- Store Id Field -->
     <div class="form-group row">
     {!! Form::label('store_id', 'المحل',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        <select class="form-control" name="store_id">
            @foreach($stores as $store)
                <option value="{{$store->id}}">{{$store->title_ar}}</option>
            @endforeach
        </select>
    </div>
 </div>

<!-- Promo Num Field -->
     <div class="form-group row">
     {!! Form::label('promo_num', 'عدد الخاص بالعرض',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('promo_num', null, ['class' => 'form-control','id'=>"promo_num","required"]) !!}
    </div>
 </div>

<!-- Start Date Field -->
     <div class="form-group row">
     {!! Form::label('start_date', 'تاريخ بداية العرض',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::date('start_date', null, ['class' => 'form-control','id'=>"start_date","required"]) !!}
    </div>
 </div>

<!-- End Date Field -->
     <div class="form-group row">
     {!! Form::label('end_date', 'تاريخ نهاية العرض',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::date('end_date', null, ['class' => 'form-control','id'=>"end_date","required"]) !!}
    </div>
 </div>

<!-- Contract Images Field -->
     <div class="input-group mb-3">
         <div class="custom-file">
             <input type="file" name="contract_images[]" multiple class="custom-file-input" id="inputGroupFile02" accept="image/*" max="3">
             <label class="custom-file-label" for="inputGroupFile02">صور العقد</label>
         </div>

     </div>

     @if(isset($promoDetail))
         <div class="row">
             @foreach(json_decode($promoDetail->contract_images) as $image)
                 <div class="col-md-4"> <img src="{{\Helper\Common\imageUrl($image)}}" class="img-responsive col-md-12" height="250"> </div>
             @endforeach
         </div>
         <br><br>

         @endif

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('promoDetails.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
