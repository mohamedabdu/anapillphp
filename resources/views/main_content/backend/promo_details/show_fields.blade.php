
<!-- Promo Id Field -->
<div class="form-group">
    {!! Form::label('promo_id', 'العرض') !!}
    @if(isset($promoDetail->promo->title_ar))
    <p>{!! $promoDetail->promo->title_ar !!}</p>
        @endif
</div>

<!-- Store Id Field -->
<div class="form-group">
    {!! Form::label('store_id', 'المحل') !!}
    @if(isset($promoDetail->store->title_ar))
        <p>{!! $promoDetail->store->title_ar !!}</p>
    @endif
</div>

<!-- Promo Num Field -->
<div class="form-group">
    {!! Form::label('promo_num', 'الرقم الخاص بالعرض') !!}
    <p>{!! $promoDetail->promo_num !!}</p>
</div>

<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', 'تاريخ بداية العرض') !!}
    <p>{!! $promoDetail->start_date !!}</p>
</div>

<!-- End Date Field -->
<div class="form-group">
    {!! Form::label('end_date', 'تاريخ نهاية العرض') !!}
    <p>{!! $promoDetail->end_date !!}</p>
</div>

<!-- Contract Images Field -->
<div class="form-group">
    {!! Form::label('contract_images', 'صور العقد') !!}
    @if(isset($promoDetail))
        <div class="row">
            @foreach(json_decode($promoDetail->contract_images) as $image)
                <div class="col-md-4"> <img src="{{\Helper\Common\imageUrl($image)}}" class="img-responsive col-md-12" height="250"> </div>
            @endforeach
        </div>
        <br><br>

    @endif

</div>

