 <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><i class="fa fa-table"></i> Data Table Example</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="default-datatable" class="dataTable table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>العرض</th>
        <th>المحل</th>
        <th>العدد الخاص بالعرض</th>
        <th>تاريخ العرض</th>
        <th>تاريخ انتهاء العرض</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>


                            <script>
                                $( document ).ready(function() {
                                    datatable = $('.dataTable').dataTable({
                                        //"processing": true,
                                        "serverSide": true,
                                        "ajax": {
                                            "url": "{{route('promoDetails.ajax')}}",
                                            "type": "GET",
                                        },
                                        "columns": [
                                            {"data": "id"},
               {"data": "promo",name:"promos.title_ar"},
               {"data": "store",name:"stores.title_ar"},
               {"data": "promo_num"},
               {"data": "start_date"},
               {"data": "end_date"},

                                            {"data": "options", orderable: false, searchable: false}
                                        ],
                                        "order": [
                                            [0, "desc"]
                                        ],
                                        //"oLanguage": {"sUrl": config.url + '/datatable-lang-' + "ar" + '.json'}

                                    });
                                });
                            </script>

                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->