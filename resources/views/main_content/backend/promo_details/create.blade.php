

@extends('layouts.backend')

@section('pageTitle',"اضافة عرض")

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

    <li><a href="{{route('promoDetails.index')}}">العروض</a> <i class="fa fa-circle"></i></li>

    <li><span> اضافة</span></li>

    <li><span> عرض</span></li>

@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/promoDetail.js" type="text/javascript"></script>
@endsection
@section('content')
    <form role="form"  id="addForm" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">تفاصيل العرض</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">
                    <input type="hidden" name="id" id="id" value="0">

                    <div class="form-body">

                        <!-- Role Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="promo_num" name="promo_num" value="">
                            <label for="promo_num">عدد الخاص بالعرض</label>
                            <span class="help-block"></span>

                        </div>
                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control datePicker" id="start_date" name="start_date" value="">
                            <label for="start_date">تاريخ بداية العرض</label>
                            <span class="help-block"></span>
                        </div>

                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control datePicker" id="end_date" name="end_date" value="">
                            <label for="end_date">تاريخ نهاية العرض</label>
                            <span class="help-block"></span>
                        </div>

                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <select class="form-control" name="promo_id">
                                @foreach($promos as $promo)
                                    <option value="{{$promo->id}}">{{$promo->title_ar}}</option>
                                @endforeach
                            </select>
                            <label for="offer">العرض</label>
                            <span class="help-block"></span>
                        </div>

                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <select class="form-control" name="store_id">
                                @foreach($stores as $store)
                                    <option value="{{$store->id}}">{{$store->title_ar}}</option>
                                @endforeach
                            </select>
                            <label for="offer">المحل</label>
                            <span class="help-block"></span>
                        </div>

                    </div>
                </div>
            </div>


        </div>


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">صور العقد</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">

                    @for ($i = 0; $i < 6; $i++)
                        <div class="form-group form-md-line-input col-md-2">
                            <div class="image_{{$i}}_box">
                                <img src="{{url('no-image.png')}}" width="100" height="80" class="image_{{$i}}" />
                            </div>
                            <input type="file" name="contract_images[{{$i}}]" id="image_{{$i}}" style="visibility: hidden">
                            <span class="help-block"></span>
                        </div>
                    @endfor

                    <div class="clearfix"></div>

                </div>
            </div>


            <div class="panel-footer text-center">
                <button type="button" class="btn btn-info submit-form"
                >{{_lang('app.save') }}</button>
            </div>

        </div>




    </form>
    <script>
        var new_lang = {

        };
        var new_config = {

        };

    </script>
@endsection
