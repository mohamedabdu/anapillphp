
@extends('layouts.backend')

@section('pageTitle', _lang('app.visits'))

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

    <li><a href="{{route('visits.index')}}?type={{$type}}">الزيارات</a> <i class="fa fa-circle"></i></li>


    <li><span> زيارة رقم {{$visit->code}}</span></li>

@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/visits.js" type="text/javascript"></script>
@endsection
@section('content')


    @include($view)



    <script>
        var new_lang = {

        };
        var new_config = {
            visitID: "{{$visit->id}}",
        };
    </script>
@endsection
