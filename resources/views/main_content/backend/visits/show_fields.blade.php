<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'المحلات') !!}
    <p>{!! $visit->id !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $visit->code !!}</p>
</div>

<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', 'Start Date:') !!}
    <p>{!! $visit->start_date !!}</p>
</div>

<!-- On Going Field -->
<div class="form-group">
    {!! Form::label('on_going', 'On Going:') !!}
    <p>{!! $visit->on_going !!}</p>
</div>

<!-- Num Of Visits Field -->
<div class="form-group">
    {!! Form::label('num_of_visits', 'Num Of Visits:') !!}
    <p>{!! $visit->num_of_visits !!}</p>
</div>

<!-- Reapet Field -->
<div class="form-group">
    {!! Form::label('reapet', 'Reapet:') !!}
    <p>{!! $visit->reapet !!}</p>
</div>

<!-- Is Normal Field -->
<div class="form-group">
    {!! Form::label('is_normal', 'Is Normal:') !!}
    <p>{!! $visit->is_normal !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $visit->status !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $visit->created_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $visit->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $visit->updated_at !!}</p>
</div>

