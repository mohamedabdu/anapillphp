<div class="panel panel-default">
    {{ csrf_field() }}
    <div class="panel-body">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-6 col-md-offset-4">
                        <br>
                        <div class="details font-" style="color: #94A0B2">
                            <h2>تفاصيل الزيارة</h2>
                            <p></p>
                        </div>
                    </div>







                    <div class="row">
                        <div class="col-md-3 col-md-offset-0">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h4>رقم الزيارة </h4>
                                <h4>{{$visit->code}}</h4>
                            </div>
                        </div>

                        <div class="col-md-3 col-md-offset-0">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h4>  تاريخ بداية الزيارة</h4>
                                <h4>{!! $visit->start_date !!}</h4>

                            </div>
                        </div>

                        <div class="col-md-3 col-md-offset-0">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h4>  تاريخ انشاء الزيارة</h4>
                                <h4>{!! $visit->created_at !!}</h4>

                            </div>
                        </div>



                        <div class="col-md-3 col-md-offset-0">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h4>هل مستمرة</h4>
                                @if($visit->on_going)
                                    <h4>نعم</h4>
                                @else
                                    <h4>لا</h4>
                                @endif

                            </div>
                        </div>

                        <div class="col-md-3 col-md-offset-0">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h4> عدد الزيارات</h4>
                                <h4>{!! $visit->num_of_visits !!}</h4>

                            </div>
                        </div>

                        <div class="col-md-3 col-md-offset-0">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h4>   تكرار كل </h4>
                                <h4>{!! $visit->repeat_every !!}</h4>

                            </div>
                        </div>

                        <div class="col-md-3 col-md-offset-0">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h4>  المحلات
                                    @foreach($visit->stores as $store)
                                        <p>{!! $store->title_ar !!}</p><br>
                                    @endforeach
                                </h4>
                                <p></p>

                            </div>
                        </div>

                        <div class="col-md-3 col-md-offset-0">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h4>  الموظف
                                    <div id="agentsDiv">
                                        @foreach($visit->agents as $agent)
                                            <p>{!! $agent->name !!}</p><br>
                                        @endforeach
                                    </div>
                                </h4>
                                <p></p>

                            </div>
                        </div>

                    </div>





                </div>
            </div>


        </div>



    </div>

</div>


<div class="panel panel-default">
    {{ csrf_field() }}
    <div class="panel-body">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-6 col-md-offset-4">
                        <br>
                        <div class="details font-" style="color: #94A0B2">
                            <h2>تفاصيل المهمات</h2>
                            <p></p>
                        </div>
                    </div>


                    <div class="row">
                        <table class = "table table-striped table-bordered table-hover table-checkable order-column  no-footer">
                            <thead>
                            <tr>
                                <th>المهمة</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($visit->tasks as $task)
                                <tr>
                                    <td>{{$task->title_ar}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>



                    </div>




                </div>
            </div>


        </div>



    </div>

</div>


<div class="panel panel-default">
    {{ csrf_field() }}
    <div class="panel-body">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-6 col-md-offset-4">
                        <br>
                        <div class="details font-" style="color: #94A0B2">
                            <h2>مواعيد الزيارات القادمة</h2>
                            <br>
                            <p></p>
                        </div>
                    </div>



                    <div class="row">

                        <table class = "table table-striped table-bordered table-hover table-checkable order-column  no-footer">
                            <thead>
                            <tr>
                                <th>التاريخ</th>
                                <th>اليوم</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($dates as $date)
                                <tr>
                                    <td>{{$date}}</td>
                                    <td>{{date("l", strtotime($date))}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>


                    </div>



                </div>
            </div>


        </div>



    </div>

</div>


<div class="panel panel-default">
    {{ csrf_field() }}
    <div class="panel-body">
        <div class="container">

            <div class="card">
                <div class="card-body">

                    <div class="col-md-11" style="text-align: center">
                        <br>
                        <div class="details font-" style="color: #94A0B2">
                            <h2>تغيير الموظف</h2>
                            <p></p>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input col-md-12">
                        <select name="agent" data-placeholder="اختار الموظف" class="chosen-select form-control" id="agents_select_show">
                            <option value="" disabled>اختار</option>
                            @foreach(\App\Models\Agent::whereNotIn("id",$visit->agents->pluck("id"))->get() as $item)
                                <option value="{{$item->id}}" @if(in_array($item->id, $visit->agents->pluck("id")->toArray())) selected @endif>{{$item->name}} </option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>

                    </div>

                    <div class="col-md-6 col-md-offset-5">
                        <button type="button" class="btn btn-info submit-form-schu"
                        >{{_lang('app.save') }}</button>

                    </div>



                </div>
            </div>


        </div>



    </div>

</div>

