
@extends('layouts.backend')

@section('pageTitle',"اضافة زيارة")

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

    <li><a href="{{route('visits.index')}}?type={{$type}}">الزيارات</a> <i class="fa fa-circle"></i></li>

    <li><span> اضافة</span></li>

    <li><span> زيارة</span></li>

@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/visits.js" type="text/javascript"></script>
@endsection
@section('content')
    <form role="form"  id="addForm" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" value="{{\App\Models\Visit::$types[$type]}}" name="type">

        @include($view)




    </form>
    <script>
        var new_lang = {

        };
        var new_config = {

        };

    </script>
@endsection
