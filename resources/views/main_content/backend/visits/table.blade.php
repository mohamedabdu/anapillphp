 <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><i class="fa fa-table"></i></div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="default-datatable" class="dataTable table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>code</th>
        <th>تاريخ الزيارة</th>
        <th>عدد الزيارات</th>
        <th>تكرار كل</th>
        <th>نوع الزيارة</th>
        <th>حالة الزيارة</th>
        <th>تم الأنشاء بواسطة</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>


                            <script>
                                $( document ).ready(function() {
                                    datatable = $('.dataTable').dataTable({
                                        //"processing": true,
                                        "serverSide": true,
                                        "ajax": {
                                            "url": "{{route('visits.ajax')}}",
                                            "type": "GET",
                                        },
                                        "columns": [
                                            {"data": "id"},
               {"data": "code"},
               {"data": "start_date"},
               {"data": "num_of_visits"},
               {"data": "repeat_every"},
               {"data": "is_normal"},
               {"data": "status"},
               {"data": "created_by"},

                                            {"data": "options", orderable: false, searchable: false}
                                        ],
                                        "order": [
                                            [0, "desc"]
                                        ],
                                        "oLanguage": {"sUrl": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"}

                                    });
                                });
                            </script>

                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->