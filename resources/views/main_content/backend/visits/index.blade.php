
@extends('layouts.backend')

@section('pageTitle', _lang('app.visits'))

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>


    <li><span>الزيارات</span></li>


@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/visits.js" type="text/javascript"></script>
@endsection
@section('content')


    <div class = "panel panel-default">
        {{ csrf_field() }}
        <div class = "panel-body">


            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">
                            <a class="btn green" style="margin-bottom: 40px;" href = "{{ route('visits.create') }}?type={{$type}}" onclick="">{{ _lang('app.add_new')}}<i class="fa fa-plus"></i> </a>
                        </div>
                    </div>
                </div>
            </div>

            <table class = "table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer">
                <thead>
                @if($type == 1)
                    <tr>
                        <th>رقم الزيارة</th>
                        <th>تاريخ الزيارة</th>
                        <th>عدد المحلات</th>
                        <th>تفاصيل الزيارة</th>
                        <th>تم الأنشاء بواسطة</th>
                        <th>العمليات</th>
                    </tr>
                    @else

                    <tr>
                        <th>رقم الزيارة</th>
                        <th>تاريخ الزيارة</th>
                        <th>عدد الزيارات</th>
                        <th>تكرار كل</th>
                        <th>عدد المحلات</th>
                        <th>تفاصيل الزيارة</th>
                        <th>تم الأنشاء بواسطة</th>
                        <th>العمليات</th>
                    </tr>
                    @endif

                </thead>
                <tbody>

                </tbody>
            </table>

            <!--Table Wrapper Finish-->
        </div>
    </div>
    <script>
        var new_lang = {

        };
        var new_config = {
            visit_type: "{{$type}}",
        };
    </script>
@endsection
