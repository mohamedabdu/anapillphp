 <div class="form-body">
{{--<!-- Code Field -->--}}
{{--     <div class="form-group row">--}}
{{--     {!! Form::label('code', '',['class' => 'col-sm-2 col-form-label']) !!}--}}
{{--    <div class="col-sm-10">--}}
{{--    {!! Form::text('code', null, ['class' => 'form-control','id'=>"code","required"]) !!}--}}
{{--    </div>--}}
{{-- </div>--}}

<!-- Start Date Field -->
     <div class="form-group row">
     {!! Form::label('start_date', 'تاريخ الزيارة',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('start_date', null, ['class' => 'form-control','id'=>"autoclose-datepicker","required"]) !!}
    </div>
 </div>

<!-- On Going Field -->
     <div class="form-group row">
     {!! Form::label('on_going', 'On Going:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('on_going', null, ['class' => 'form-control','id'=>"on_going","required"]) !!}
    </div>
 </div>

<!-- Num Of Visits Field -->
     <div class="form-group row">
     {!! Form::label('num_of_visits', 'Num Of Visits:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('num_of_visits', null, ['class' => 'form-control','id'=>"num_of_visits","required"]) !!}
    </div>
 </div>

<!-- Reapet Field -->
     <div class="form-group row">
     {!! Form::label('repeat_every', 'repeat_every:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('repeat_every', null, ['class' => 'form-control','id'=>"repeat_every","required"]) !!}
    </div>
 </div>

<!-- Is Normal Field -->
     <div class="form-group row">
     {!! Form::label('is_normal', 'Is Normal:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('is_normal', null, ['class' => 'form-control','id'=>"is_normal","required"]) !!}
    </div>
 </div>

<!-- Status Field -->
     <div class="form-group row">
     {!! Form::label('status', 'Status:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('status', null, ['class' => 'form-control','id'=>"status","required"]) !!}
    </div>
 </div>

<!-- Created By Field -->
     <div class="form-group row">
     {!! Form::label('created_by', 'Created By:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('created_by', null, ['class' => 'form-control','id'=>"created_by","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('visits.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
