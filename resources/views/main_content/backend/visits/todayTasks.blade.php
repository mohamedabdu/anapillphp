
@extends('layouts.backend')

@section('pageTitle', _lang('app.visits'))

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>


    <li><span>الزيارات</span></li>


@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/visits.js" type="text/javascript"></script>
@endsection
@section('content')


    <div class = "panel panel-default">
        {{ csrf_field() }}
        <div class = "panel-body">
            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form " id="filterForm">
                            <div class="form-group form-md-line-input col-md-4">
                                <input type="text" class="form-control datePicker" id="filter_date" name="filter_date" value="{{date("yy-m-d")}}">
                                <label for="end_date">تاريخ البحث</label>
                                <span class="help-block"></span>
                            </div>

                            <div class="form-group form-md-line-input col-md-4">
                                <select class="form-control" id="filterMethod" name="filter">
                                    <option value="0">الكل</option>
                                    <option value="{{\App\Models\Visit::$statuses["PENDING"]}}">منتظرة</option>
                                    <option value="{{\App\Models\Visit::$statuses["PROCESSING"]}}">قيد العمل</option>
                                    <option value="{{\App\Models\Visit::$statuses["FINISHED"]}}">منتهية</option>
                                    <option value="{{\App\Models\Visit::$statuses["CANCELED_BY_ADMIN"]}}">ملغاة</option>
                                </select>
                            </div>

                            <div class="form-group form-md-line-input col-md-4">
                                <button class="btn btn-info col-md-3 submit-form-filter" type="button" id="filterBtn">Filter</button>

                            </div>

                        </form>
                    </div>

                </div>
            </div>

            <table class = "table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer">
                <thead>
                    <tr>
                        <th>رقم الزيارة</th>
                        <th>تاريخ الزيارة</th>
                        <th>تم الأنشاء بواسطة</th>
                        <th>العمليات</th>
                    </tr>


                </thead>
                <tbody>

                </tbody>
            </table>

            <!--Table Wrapper Finish-->
        </div>
    </div>
    <script>
        var new_lang = {

        };
        var new_config = {
        };
    </script>
@endsection
