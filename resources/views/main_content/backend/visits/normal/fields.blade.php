
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">تفاصيل الزيارة</h3>
    </div>
    <div class="panel-body">

        <div class="form-body">
            @if(isset($visit))
                <input type="hidden" name="id" id="id" value="{{$visit->id}}">

            @else
                <input type="hidden" name="id" id="id" value="0">

            @endif
            <div class="form-body">


                <div class="form-group form-md-line-input col-md-12">
                        <select name="store" data-placeholder="اختار المحل" class="chosen-select form-control" id="stores_select" >
                            <option value="" >اختار</option>
                            @foreach($stores as $item)
                                <option value="{{$item->id}}" @if(isset($storesSelected) && in_array($item->id, $storesSelected)) selected @endif>{{$item->title_ar}} </option>
                            @endforeach
                        </select>
                        {!! Form::label('stores', 'المحلات',['class' => 'col-sm-2 col-form-label']) !!}
                    <span class="help-block"></span>

                </div>

                <div class="form-group form-md-line-input col-md-12">
                        <select name="tasks[]" data-placeholder="اختار المهام" class="chosen-select form-control" id="tasks_select" multiple>
                            <option value="" disabled>اختار</option>
                            @foreach(\App\Models\Task::all() as $item)
                                <option value="{{$item->id}}" @if(isset($tasks) && in_array($item->id, $tasks)) selected @endif>{{$item->title_ar}} </option>
                            @endforeach
                        </select>
                        {!! Form::label('stores', 'تحديد المهام',['class' => 'col-sm-2 col-form-label']) !!}
                        <span class="help-block"></span>

                </div>

                <div class="form-group form-md-line-input col-md-12">
                        <select name="agents[]" data-placeholder="اختار الموظفين" class="chosen-select form-control" id="agents_select" multiple>
                            <option value="" disabled>اختار</option>
                            @foreach($agents as $item)
                                <option value="{{$item->id}}" @if(isset($agentsSelected) && in_array($item->id, $agentsSelected)) selected @endif>{{$item->name}} </option>
                            @endforeach
                        </select>
                    {!! Form::label('body_en', 'الموظفين',['class' => 'col-sm-2 col-form-label']) !!}
                    <span class="help-block"></span>

                </div>


            @if(isset($editStartDate))

                @if($editStartDate)
                    <!-- Start Date Field -->
                        <div class="form-group  form-md-line-input col-md-12">
                            <input type="text" name="start_date" class="form-control datePicker" id="" value="{{$visit->start_date}}">
                            {!! Form::label('start_date', 'تاريخ الزيارة',['class' => 'col-sm-2 col-form-label']) !!}
                            <span class="help-block"></span>


                        </div>
                @else
                    <!-- Start Date Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            {!! Form::label('start_date', 'تاريخ الزيارة',['class' => 'col-sm-2 col-form-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::label('start_date', $visit->start_date, ['class' => 'col-sm-2 col-form-label']) !!}
                            </div>
                        </div>
                @endif

            @else
                <!-- Start Date Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" name="start_date" class="form-control datePicker" id="" value="{{isset($visit) ? $visit->start_date : ""}}">
                            {!! Form::label('start_date', 'تاريخ الزيارة',['class' => 'col-sm-2 col-form-label']) !!}
                        <span class="help-block"></span>

                    </div>
                @endif


            </div>
        </div>
    </div>

    <div class="panel-footer text-center">
        <button type="button" class="btn btn-info submit-form"
        >{{_lang('app.save') }}</button>
    </div>

</div>

