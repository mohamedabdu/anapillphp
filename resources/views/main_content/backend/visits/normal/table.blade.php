 <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><i class="fa fa-table"></i></div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="default-datatable" class="dataTable table table-bordered">
                                <thead>
                                <tr>
                                    <th>رقم الزيارة</th>
        <th>تاريخ الزيارة</th>
        <th>تم الأنشاء بواسطة</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>


                            <script>
                                $( document ).ready(function() {
                                    datatable = $('.dataTable').dataTable({
                                        //"processing": true,
                                        "serverSide": true,
                                        "ajax": {
                                            "url": "{{route('visits.ajax')}}?type={{\App\Models\Visit::$types["normal"]}}",
                                            "type": "GET",
                                        },
                                        "columns": [
                                            {"data": "id"},
               {"data": "code"},
               {"data": "start_date"},
               {"data": "created_user",name: "users.name"},

                                            {"data": "options", orderable: false, searchable: false}
                                        ],
                                        "order": [
                                            [0, "desc"]
                                        ],
                                        "oLanguage": {"sUrl": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"}

                                    });
                                });
                            </script>

                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->