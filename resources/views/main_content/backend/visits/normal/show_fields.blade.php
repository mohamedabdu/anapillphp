<div class="panel panel-default">
    {{ csrf_field() }}
    <div class="panel-body">
        <div class="container">
            <div class="card">
                <div class="card-body">
                        <div class="col-md-6 col-md-offset-4">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h2>تفاصيل الزيارة</h2>
                                <p></p>
                            </div>
                        </div>


                    <div class="row">
                        <div class="col-md-3 col-md-offset-0">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h4>رقم الزيارة </h4>
                                <h4>{{$visit->code}}</h4>
                            </div>
                        </div>

                        <div class="col-md-2 col-md-offset-0">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h4>  تاريخ بداية الزيارة</h4>
                                <h4>{!! $visit->start_date !!}</h4>

                            </div>
                        </div>

                        <div class="col-md-2 col-md-offset-0">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h4>  تاريخ انشاء الزيارة</h4>
                                <h4>{!! $visit->created_at !!}</h4>

                            </div>
                        </div>


                        <div class="col-md-2 col-md-offset-0">
                            <br>
                            <div class="details font" style="color: #94A0B2">
                                <h4>  المحل
                                    @foreach($visit->stores as $store)
                                        <p>{!! $store->title_ar !!}</p><br>
                                    @endforeach
                                </h4>
                                <p></p>

                            </div>
                        </div>

                        <div class="col-md-3 col-md-offset-0">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h4>  الموظفين
                                    <div id="agentsDiv">
                                        @foreach($visit->agents as $agent)
                                            <p>{!! $agent->name !!}</p><br>
                                        @endforeach
                                    </div>
                                </h4>
                                <p></p>

                            </div>

                    </div>




                    </div>




                </div>
            </div>


        </div>



    </div>

</div>



<div class="panel panel-default">
    {{ csrf_field() }}
    <div class="panel-body">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-6 col-md-offset-4">
                        <br>
                        <div class="details font-" style="color: #94A0B2">
                            <h2>تفاصيل المهمات</h2>
                            <p></p>
                        </div>
                    </div>


                    <div class="row">
                        <table class = "table table-striped table-bordered table-hover table-checkable order-column  no-footer">
                            <thead>
                            <tr>
                                <th>المهمة</th>
                                <th>الحالة</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($visit->tasks as $task)
                                <tr>
                                    <td>{{$task->title_ar}}</td>
                                    <td>{{\Helper\Common\__lang(\App\Models\Visit::$statuses[$task->status])}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>



                    </div>




                </div>
            </div>


        </div>



    </div>

</div>



<div class="panel panel-default">
    {{ csrf_field() }}
    <div class="panel-body">
        <div class="container">

            <div class="card">
                <div class="card-body">

                    <div class="col-md-11" style="text-align: center">
                        <br>
                        <div class="details font-" style="color: #94A0B2">
                            <h1>تغيير الموظف</h1>
                            <p></p>
                        </div>
                    </div>


                    <div class="form-group form-md-line-input col-md-12">
                        <select name="agent" data-placeholder="اختار الموظف" class="chosen-select form-control" id="agents_select" multiple>
                            <option value="" disabled>اختار</option>
                            @foreach(\App\Models\Agent::all() as $item)
                                <option value="{{$item->id}}" @if(isset($visit->agents) && in_array($item->id, $visit->agents->pluck("id")->toArray())) selected @endif>{{$item->name}} </option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>

                    </div>

                    <div class="col-md-6 col-md-offset-5">
                        <button type="button" class="btn btn-info submit-form"
                        >{{_lang('app.save') }}</button>

                    </div>



                </div>
            </div>


        </div>



    </div>

</div>

