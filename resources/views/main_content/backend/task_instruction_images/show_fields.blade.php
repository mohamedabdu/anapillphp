<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $taskInstructionImage->id !!}</p>
</div>

<!-- Task Instruction Id Field -->
<div class="form-group">
    {!! Form::label('task_instruction_id', 'Task Instruction Id:') !!}
    <p>{!! $taskInstructionImage->task_instruction_id !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $taskInstructionImage->image !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $taskInstructionImage->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $taskInstructionImage->updated_at !!}</p>
</div>

