 <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><i class="fa fa-table"></i> Data Table Example</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="default-datatable" class="dataTable table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Task Instruction Id</th>
        <th>Image</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>


                            <script>
                                $( document ).ready(function() {
                                    datatable = $('.dataTable').dataTable({
                                        //"processing": true,
                                        "serverSide": true,
                                        "ajax": {
                                            "url": "{{route('taskInstructionImages.ajax')}}",
                                            "type": "GET",
                                        },
                                        "columns": [
                                            {"data": "id"},
               {"data": "task_instruction_id"},
               {"data": "image"},

                                            {"data": "options", orderable: false, searchable: false}
                                        ],
                                        "order": [
                                            [0, "desc"]
                                        ],
                                        //"oLanguage": {"sUrl": config.url + '/datatable-lang-' + "ar" + '.json'}

                                    });
                                });
                            </script>

                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->