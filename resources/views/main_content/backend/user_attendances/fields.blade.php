 <div class="form-body">
<!-- User Id Field -->
     <div class="form-group row">
     {!! Form::label('user_id', 'User Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('user_id', null, ['class' => 'form-control','id'=>"user_id","required"]) !!}
    </div>
 </div>

<!-- Date Field -->
     <div class="form-group row">
     {!! Form::label('date', 'Date:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('date', null, ['class' => 'form-control','id'=>"date","required"]) !!}
    </div>
 </div>

<!-- Is End Field -->
     <div class="form-group row">
     {!! Form::label('is_end', 'Is End:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('is_end', null, ['class' => 'form-control','id'=>"is_end","required"]) !!}
    </div>
 </div>

<!-- End Date Field -->
     <div class="form-group row">
     {!! Form::label('end_date', 'End Date:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('end_date', null, ['class' => 'form-control','id'=>"end_date","required"]) !!}
    </div>
 </div>

<!-- Image Field -->
     <div class="form-group row">
     {!! Form::label('image', 'Image:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('image', null, ['class' => 'form-control','id'=>"image","required"]) !!}
    </div>
 </div>

<!-- Lat Field -->
     <div class="form-group row">
     {!! Form::label('lat', 'Lat:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('lat', null, ['class' => 'form-control','id'=>"lat","required"]) !!}
    </div>
 </div>

<!-- Lng Field -->
     <div class="form-group row">
     {!! Form::label('lng', 'Lng:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('lng', null, ['class' => 'form-control','id'=>"lng","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('userAttendances.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
