<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $visitTaskAreaDetail->id !!}</p>
</div>

<!-- Visit Task Detail Id Field -->
<div class="form-group">
    {!! Form::label('visit_task_detail_id', 'Visit Task Detail Id:') !!}
    <p>{!! $visitTaskAreaDetail->visit_task_detail_id !!}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Category Id:') !!}
    <p>{!! $visitTaskAreaDetail->category_id !!}</p>
</div>

<!-- Width Field -->
<div class="form-group">
    {!! Form::label('width', 'Width:') !!}
    <p>{!! $visitTaskAreaDetail->width !!}</p>
</div>

<!-- Height Field -->
<div class="form-group">
    {!! Form::label('height', 'Height:') !!}
    <p>{!! $visitTaskAreaDetail->height !!}</p>
</div>

