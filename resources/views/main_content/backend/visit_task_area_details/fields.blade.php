 <div class="form-body">
<!-- Visit Task Detail Id Field -->
     <div class="form-group row">
     {!! Form::label('visit_task_detail_id', 'Visit Task Detail Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('visit_task_detail_id', null, ['class' => 'form-control','id'=>"visit_task_detail_id","required"]) !!}
    </div>
 </div>

<!-- Category Id Field -->
     <div class="form-group row">
     {!! Form::label('category_id', 'Category Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('category_id', null, ['class' => 'form-control','id'=>"category_id","required"]) !!}
    </div>
 </div>

<!-- Width Field -->
     <div class="form-group row">
     {!! Form::label('width', 'Width:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('width', null, ['class' => 'form-control','id'=>"width","required"]) !!}
    </div>
 </div>

<!-- Height Field -->
     <div class="form-group row">
     {!! Form::label('height', 'Height:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('height', null, ['class' => 'form-control','id'=>"height","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('visitTaskAreaDetails.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
