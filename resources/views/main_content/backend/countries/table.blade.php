 <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><i class="fa fa-table"></i> Data Table Example</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="default-datatable" class="dataTable table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>الأسم عربي</th>
                                 <th>الأسم انجليزي</th>
                                    <th>المناطق</th>
                                    <th>الحالة</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>


                            <script>
                                $( document ).ready(function() {
                                    datatable = $('.dataTable').dataTable({
                                        //"processing": true,
                                        "serverSide": true,
                                        "ajax": {
                                            "url": "{{route('countries.ajax')}}",
                                            "type": "GET",
                                        },
                                        "columns": [
                                            {"data": "id"},
               {"data": "title_ar"},
               {"data": "title_en"},

                                            {"data": "regions", orderable: false, searchable: false},
                                            {"data": "active", orderable: false, searchable: false},
                                            {"data": "options", orderable: false, searchable: false}
                                        ],
                                        "order": [
                                            [0, "desc"]
                                        ],
                                        //"oLanguage": {"sUrl": config.url + '/datatable-lang-' + "ar" + '.json'}

                                    });
                                });
                                function changeActive(e) {
                                    var record_id = $(e).data("id");
                                    $.get('{{url("admin/countries/active")}}'+"/"+record_id, function (data) {
                                        if (data.length !== 0)
                                        {
                                            if(data.success)
                                            {
                                                makeToast(data.message,1);
                                                datatable.api().ajax.reload();

                                            }
                                            else
                                            {
                                                makeToast(data.message,0);
                                            }


                                        }

                                    });


                                }
                            </script>

                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->