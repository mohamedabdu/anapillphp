@extends('layouts.backend')

@section('pageTitle',_lang('app.edit_product'))

@section('breadcrumb')
<li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>
<li><a href="{{route('products.index')}}">{{_lang('app.products')}}</a> <i class="fa fa-circle"></i></li>
<li><span>{{_lang('app.edit')}}</span></li>
@endsection

@section('js')
<script src="{{url('public/backend/js')}}/products.js" type="text/javascript"></script>
@endsection

@section('content')
<form role="form" id="addEditProductsForm" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.title') }}</h3>
        </div>
        <div class="panel-body">


            <div class="form-body">
                <input type="hidden" name="id" id="id" value="{{en_de_crypt($product->id)}}">

                @foreach ($languages as $key => $value)
                <div class="form-group form-md-line-input col-md-6">
                    <input type="text" class="form-control" id="title[{{ $key }}]" name="title[{{ $key }}]"
                        value="{{ $translations["$key"]->title }}">
                    <label for="title">{{_lang('app.title') }} {{ _lang('app.'.$value.'') }}</label>
                    <span class="help-block"></span>
                </div>
                @endforeach


            </div>
        </div>


    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.properties') }}</h3>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div class="form-group form-md-line-input col-md-5 select">
                    <select class="form-control edited" id="main_category" name="main_category">
                        <option value="">{{ _lang('app.choose') }}</option>
                        @foreach ($main_categories as $main_category)
                            <option value="{{$main_category->id}}" {{$main_category->id == en_de_crypt($product->main_category_id) ? 'selected' : ''}}>{{ $main_category->title }}</option>
                        @endforeach
                    </select>
                    <label for="main_category">{{_lang('app.main_category') }}</label>
                    <span class="help-block"></span>
                </div>

                <div class="form-group form-md-line-input col-md-5 select">
                    <select class="form-control edited" id="category" name="category">
                        <option value="">{{ _lang('app.choose') }}</option>
                        @foreach ($sub_categories as $sub_category)
                            <option value="{{$sub_category->id}}" {{$sub_category->id == en_de_crypt($product->category_id) ? 'selected' : ''}}>{{ $sub_category->title }}</option>
                        @endforeach
                    </select>
                    <label for="category">{{_lang('app.sub_category') }}</label>
                    <span class="help-block"></span>
                </div>

                <div class="form-group form-md-line-input col-md-4">
                    <input type="number" class="form-control" id="price" name="price" value="{{$product->price}}">
                    <label for="price">{{_lang('app.price') }}</label>
                    <span class="help-block"></span>
                </div>

                <div class="form-group form-md-line-input col-md-4">
                    <input type="text" class="form-control" id="sku" name="sku" value="{{$product->sku}}">
                    <label for="sku">{{_lang('app.sku') }}</label>
                    <span class="help-block"></span>
                </div>

                <div class="form-group form-md-line-input col-md-4">
                    <label for="image">{{_lang('app.image') }}</label>
                    <div class="image_box">
                        <img src="{{url('public/uploads/products/'.$product->image)}}" width="100" height="80" class="image" />
                    </div>
                    <input type="file" name="image" id="image" style="visibility: hidden">
                    <span class="help-block"></span>
                </div>

            </div>
        </div>

    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.competitors') }}</h3>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div id="competitors-container">
                   @foreach ($product_competitors as $key => $product_competitor)
                       <div class="portlet light bordered single-competitor">
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-xs-12 mb-3 text-right">
                                            <button class="btn btn-danger remove-competitor" data-id="{{$product_competitor['product_competitor_id']}}" type="button"><i class="fa fa-trash"></i>{{_lang('app.remove')}}</button>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                           <select class="form-control edited competitor" name='competitors[{{$key}}][competitor]'>
                                                <option value="{{$product_competitor['id']}}">{{$product_competitor['title']}}</option>      
                                            </select>
                                            <label for="competitor">{{_lang('app.competitor')}}</label>
                                            <span class="help-block"></span>
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label for="competitor_products">{{_lang('app.competitor_products')}}</label>
                                            <select class="form-control edited competitor-products" multiple  name="competitors[{{$key}}][competitor_products][]">
                                                @foreach ($product_competitor['products'] as $item)
                                                    <option value="{{$item['id']}}" {{$item['selected'] ? 'selected' : ''}}>{{$item['title']}}</option>
                                                @endforeach
                                            </select>
                                            <span class="help-block"></span>
                                        </div>

                                    </div>
                                </div>
                            </div>
                          </div>
                   @endforeach
                </div>

                <div class="col-xs-12 mt-3 text-left">
                    <button class="btn btn-info" id="add-competitor" type="button"><i class="fa fa-plus-square-o"></i>
                        {{_lang('app.add_competitor')}}
                    </button>
                </div>

            </div>
        </div>

    </div>

    <div class="panel panel-default">

        <div class="panel-body">
            <div class="form-body">
                <div class="form-group form-md-line-input col-md-3">
                    <input type="number" class="form-control" id="this_order" name="this_order" value="{{$product->this_order}}">
                    <label for="this_order">{{_lang('app.this_order') }}</label>
                    <span class="help-block"></span>
                </div>

                <div class="form-group form-md-line-input col-md-3 select">
                    <select class="form-control edited" id="active" name="active">
                        <option value="0" {{$product->active == 0 ? 'selected' : ''}}>{{ _lang('app.deactivated') }}</option>
                        <option value="1" {{$product->active == 1 ? 'selected' : ''}}>{{ _lang('app.activated') }}</option>

                    </select>
                    <label for="status">{{_lang('app.status') }}</label>
                    <span class="help-block"></span>
                </div>

            </div>
        </div>

        <div class="panel-footer text-center">
            <button type="button" class="btn btn-info submit-form">{{_lang('app.save') }}</button>
        </div>


    </div>


</form>
<script>
    var new_lang = {

};
var new_config = {
    competitors: '{!! str_replace("'", "\'", json_encode($competitors)) !!}',
    count : '{{count($product_competitors)}}'
};

</script>
@endsection