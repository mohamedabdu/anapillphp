@extends('layouts.backend')

@section('pageTitle', _lang('app.products'))

@section('breadcrumb')
<li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>
<li><span> {{_lang('app.products')}}</span></li>
@endsection

@section('js')
<script src="{{url('public/backend/js')}}/products.js" type="text/javascript"></script>
@endsection

@section('content')
@if (session()->has('error_message'))
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
    <strong>{{_lang('app.error')}}!</strong> {{session()->get('error_message')}}.
</div>
@endif
 {{ csrf_field() }}
<div class = "panel panel-default">
    <div class = "panel-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a class="btn green" style="margin-bottom: 40px;" href = "{{ route('products.create') }}" onclick="">{{ _lang('app.add_new')}}<i class="fa fa-plus"></i> </a>
                    </div>
                </div>
            </div>
        </div>
        <table class= "table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer">
            <thead>
                <tr>
                    <th>{{_lang('app.name')}}</th>
                    <th>{{_lang('app.main_category')}}</th>
                    <th>{{_lang('app.sub_category')}}</th>
                    <th>{{_lang('app.sku')}}</th>
                    <th>{{_lang('app.price')}}</th>
                    <th>{{_lang('app.status')}}</th>
                    <th>{{_lang('app.this_order')}}</th>
                    <th>{{_lang('app.options')}}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

        <!--Table Wrapper Finish-->
    </div>
</div>
<script>
var new_lang = {

};
var new_config = {
    
};
</script>
@endsection