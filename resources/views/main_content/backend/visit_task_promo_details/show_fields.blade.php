<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $visitTaskPromoDetail->id !!}</p>
</div>

<!-- Visit Task Detail Id Field -->
<div class="form-group">
    {!! Form::label('visit_task_detail_id', 'Visit Task Detail Id:') !!}
    <p>{!! $visitTaskPromoDetail->visit_task_detail_id !!}</p>
</div>

<!-- Promo Id Field -->
<div class="form-group">
    {!! Form::label('promo_id', 'Promo Id:') !!}
    <p>{!! $visitTaskPromoDetail->promo_id !!}</p>
</div>

<!-- Num Of Promo Field -->
<div class="form-group">
    {!! Form::label('num_of_promo', 'Num Of Promo:') !!}
    <p>{!! $visitTaskPromoDetail->num_of_promo !!}</p>
</div>

<!-- Promo Image Field -->
<div class="form-group">
    {!! Form::label('promo_image', 'Promo Image:') !!}
    <p>{!! $visitTaskPromoDetail->promo_image !!}</p>
</div>

<!-- Promo Note Field -->
<div class="form-group">
    {!! Form::label('promo_note', 'Promo Note:') !!}
    <p>{!! $visitTaskPromoDetail->promo_note !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $visitTaskPromoDetail->note !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $visitTaskPromoDetail->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $visitTaskPromoDetail->updated_at !!}</p>
</div>

