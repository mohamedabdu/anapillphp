 <div class="form-body">
<!-- Visit Task Detail Id Field -->
     <div class="form-group row">
     {!! Form::label('visit_task_detail_id', 'Visit Task Detail Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('visit_task_detail_id', null, ['class' => 'form-control','id'=>"visit_task_detail_id","required"]) !!}
    </div>
 </div>

<!-- Promo Id Field -->
     <div class="form-group row">
     {!! Form::label('promo_id', 'Promo Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('promo_id', null, ['class' => 'form-control','id'=>"promo_id","required"]) !!}
    </div>
 </div>

<!-- Num Of Promo Field -->
     <div class="form-group row">
     {!! Form::label('num_of_promo', 'Num Of Promo:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('num_of_promo', null, ['class' => 'form-control','id'=>"num_of_promo","required"]) !!}
    </div>
 </div>

<!-- Promo Image Field -->
     <div class="form-group row">
     {!! Form::label('promo_image', 'Promo Image:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('promo_image', null, ['class' => 'form-control','id'=>"promo_image","required"]) !!}
    </div>
 </div>

<!-- Promo Note Field -->
     <div class="form-group row">
     {!! Form::label('promo_note', 'Promo Note:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('promo_note', null, ['class' => 'form-control','id'=>"promo_note","required"]) !!}
    </div>
 </div>

<!-- Note Field -->
     <div class="form-group row">
     {!! Form::label('note', 'Note:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('note', null, ['class' => 'form-control','id'=>"note","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('visitTaskPromoDetails.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
