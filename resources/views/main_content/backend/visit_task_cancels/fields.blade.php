 <div class="form-body">
<!-- Visit Id Field -->
     <div class="form-group row">
     {!! Form::label('visit_id', 'Visit Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('visit_id', null, ['class' => 'form-control','id'=>"visit_id","required"]) !!}
    </div>
 </div>

<!-- Task Id Field -->
     <div class="form-group row">
     {!! Form::label('task_id', 'Task Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('task_id', null, ['class' => 'form-control','id'=>"task_id","required"]) !!}
    </div>
 </div>

<!-- Visit Detail Id Field -->
     <div class="form-group row">
     {!! Form::label('visit_detail_id', 'Visit Detail Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('visit_detail_id', null, ['class' => 'form-control','id'=>"visit_detail_id","required"]) !!}
    </div>
 </div>

<!-- Note Field -->
     <div class="form-group row">
     {!! Form::label('note', 'Note:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('note', null, ['class' => 'form-control','id'=>"note","required"]) !!}
    </div>
 </div>

<!-- Is Agree Field -->
     <div class="form-group row">
     {!! Form::label('is_agree', 'Is Agree:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('is_agree', null, ['class' => 'form-control','id'=>"is_agree","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('visitTaskCancels.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
