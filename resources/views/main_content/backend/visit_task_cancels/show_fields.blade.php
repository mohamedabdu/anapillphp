<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $visitTaskCancel->id !!}</p>
</div>

<!-- Visit Id Field -->
<div class="form-group">
    {!! Form::label('visit_id', 'Visit Id:') !!}
    <p>{!! $visitTaskCancel->visit_id !!}</p>
</div>

<!-- Task Id Field -->
<div class="form-group">
    {!! Form::label('task_id', 'Task Id:') !!}
    <p>{!! $visitTaskCancel->task_id !!}</p>
</div>

<!-- Visit Detail Id Field -->
<div class="form-group">
    {!! Form::label('visit_detail_id', 'Visit Detail Id:') !!}
    <p>{!! $visitTaskCancel->visit_detail_id !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $visitTaskCancel->note !!}</p>
</div>

<!-- Is Agree Field -->
<div class="form-group">
    {!! Form::label('is_agree', 'Is Agree:') !!}
    <p>{!! $visitTaskCancel->is_agree !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $visitTaskCancel->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $visitTaskCancel->updated_at !!}</p>
</div>

