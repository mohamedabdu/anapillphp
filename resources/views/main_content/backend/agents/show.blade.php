
<style>
    .checked {
        color: orange;
    }
</style>
@extends('layouts.backend')

@section('pageTitle', _lang('app.agents'))

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

    <li><a href="{{route('agents.index')}}">الموظفيين</a> <i class="fa fa-circle"></i></li>


    <li><span>{{$agent->name}}</span></li>

@endsection

@section('js')
    <script>
        $('.dataTable').dataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": config.admin_url + "/agentRates/data/table?agent_id="+new_config.agent_id,
                "type": "POST",
                data: { _token: $('input[name="_token"]').val() },
            },
            "columns": [
                {"data": "rate"},
                {"data": "note"},
                {"data": "date"},

            ],
            "order": [
                [1, "asc"]
            ],

            "oLanguage": { "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json' }

        });
    </script>
@endsection
@section('content')
    @php /** @var \App\Models\Agent $agent **/  @endphp





    <div class="panel panel-default">
        {{ csrf_field() }}
        <div class="panel-body">
            <div class="container">

                <div class="card">
                    <div class="card-body">
                        <div class="col-md-6 col-md-offset-4">
                            <br>
                            <div class="details font" style="color: #94A0B2">
                                <h1>تفاصيل </h1>
                                <p></p>
                                <div class="col-md-offset-0">
                                    <img src="{{\Helper\Common\imageUrl($agent->image)}}" class="img-circle" width="150" height="150">

                                </div>
                            </div>
                        </div>




                    </div>
                </div>




            </div>



        </div>

    </div>

    <div class="panel panel-default">
        {{ csrf_field() }}
        <div class="panel-body">
            <div class="container">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-6 col-md-offset-4">
                            <br>
                            <div class="details font-green">
                                <p></p>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details" style="color: #94A0B2">
                                    <h4>اسم  الموظف</h4>
                                    <h4>{{$agent->name}}</h4>
                                </div>
                            </div>

                            <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details" style="color: #94A0B2">
                                    <h4>رقم الجوال</h4>
                                    <h4>{{$agent->mobile}}</h4>
                                </div>
                            </div>


                            <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details" style="color: #94A0B2">
                                    <h4> البريد الألكتروني</h4>
                                    <h4>{{$agent->email}}</h4>
                                </div>
                            </div>

                        </div>



                    </div>
                </div>


            </div>



        </div>

    </div>



    <div class="panel panel-default">
        {{ csrf_field() }}
        <div class="panel-body">
            <div class="container">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-6 col-md-offset-4">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h2>عنوان  الموظف</h2>
                                <p></p>
                            </div>
                        </div>



                        <div class="row">

                            <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details font-" style="color: #94A0B2">
                                    <h4>الدولة</h4>
                                    <h4> @if($agent->country != null)
                                            {{$agent->country->title_ar}}
                                        @endif</h4>

                                </div>
                            </div>

                            <div class="col-md-3 col-md-offset-0">
                                <br>
                                <div class="details font-" style="color: #94A0B2">
                                    <h4>المنطقة</h4>
                                    <h4>    @if($agent->region != null)
                                            {{$agent->region->title_ar}}
                                        @endif</h4>

                                </div>
                            </div>

                            <div class="col-md-3 col-md-offset-0" >
                                <br>
                                <div class="details font-" style="color: #94A0B2">
                                    <h4>المدينة</h4>
                                    <h4>  @if($agent->city != null)
                                            {{$agent->city->title_ar}}
                                        @endif</h4>

                                </div>
                            </div>




                        </div>



                    </div>
                </div>


            </div>



        </div>

    </div>



    <div class="panel panel-default">
        {{ csrf_field() }}
        <div class="panel-body">
            <div class="container">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-6 col-md-offset-4">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h2>مواعيد عمل  الموظف</h2>
                                <p></p>
                            </div>
                        </div>



                        <div class="row">


                            <div class="form-group form-md-line-input col-md-12">
                                @foreach(\App\Models\Agent::$days as $day)
                                    <div class="form-group col-md-2" style="padding-top:30px; margin-left:5%">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" disabled data-toggle="toggle" data-style="ios" id="days" name="days[]" value="{{$day}}"
                                                    {{isset($days) && in_array($day,$days) ? "checked" : "" }}> {{$day}}
                                        </label>
                                    </div>

                                @endforeach

                                <span class="help-block"></span>

                            </div>


                        </div>



                    </div>
                </div>


            </div>



        </div>

    </div>



    <div class="panel panel-default">
        {{ csrf_field() }}
        <div class="panel-body">
            <div class="container">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-6 col-md-offset-4">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h2> الزيارات </h2>
                                <br>
                                <p></p>
                            </div>
                        </div>



                        <div class="row">

                            <table class = "table table-striped table-bordered table-hover table-checkable order-column  no-footer">
                                <thead>
                                <tr>
                                    <th>رقم الزيارة</th>
                                    <th>نوع الزيارة</th>
                                    <th>تاريخ الزيارة</th>
                                    <th>تفاصيل الزيارة</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($visits as $visit)
                                    <tr>
                                        <td>{{$visit->code}}</td>
                                        <td>{{\Helper\Common\__lang(\App\Models\Visit::$types[$visit->type])}}</td>
                                        <td>{{$visit->start_date}}</td>
                                        <td><a target="_blank" class="btn btn-success" href="{{route('visits.show', $visit->id)}}">تفاصيل الزيارة</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>


                        </div>



                    </div>
                </div>


            </div>



        </div>

    </div>



    <div class="panel panel-default">
        {{ csrf_field() }}
        <div class="panel-body">
            <div class="container">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-6 col-md-offset-4">
                            <br>
                            <div class="details font-" style="color: #94A0B2">
                                <h2>تقييمات الموظف</h2>
                                <p></p>
                            </div>
                        </div>



                        <div class="row">

                            <table class = "table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer">
                                <thead>
                                <tr>
                                    <th>التقييم</th>
                                    <th>الملاحظة</th>
                                    <th>التاريخ</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>


                        </div>



                    </div>
                </div>


            </div>



        </div>

    </div>




    <script>


        var new_lang = {

        };
        var new_config = {
            agent_id: "{{$agent->id}}",
        };
    </script>
@endsection
