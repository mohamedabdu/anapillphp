



@extends('layouts.backend')

@section('pageTitle', _lang('app.regions'))

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>


    <li><span>المناطق</span></li>


@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/regions.js" type="text/javascript"></script>
@endsection
@section('content')


    <div class = "panel panel-default">
        {{ csrf_field() }}
        <div class = "panel-body">


            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">
                            <a class="btn green" style="margin-bottom: 40px;" href = "{{ route('regions.create') }}?country_id={{$country_id}}" onclick="">{{ _lang('app.add_new')}}<i class="fa fa-plus"></i> </a>
                        </div>
                    </div>
                </div>
            </div>

            <table class = "table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer">
                <thead>
                <tr>
                    <th>الأسم عربي</th>
                    <th>الأسم انجليزي</th>
                    <th>المدن</th>
                    <th>الحالة</th>
                    <th>العمليات</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

            <!--Table Wrapper Finish-->
        </div>
    </div>
    <script>
        var new_lang = {

        };
        var new_config = {
            country: "{{$country_id}}",
        };
    </script>
@endsection
