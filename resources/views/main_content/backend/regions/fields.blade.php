 <div class="form-body">
<!-- Title Ar Field -->
     <div class="form-group row">
     {!! Form::label('title_ar', 'الأسم عربي',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('title_ar', null, ['class' => 'form-control','id'=>"title_ar","required"]) !!}
    </div>
 </div>

<!-- Title En Field -->
     <div class="form-group row">
     {!! Form::label('title_en', 'الأسم انجليزي',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('title_en', null, ['class' => 'form-control','id'=>"title_en","required"]) !!}
    </div>
 </div>

     @if(isset($region))
     <div class="form-group row">
         {!! Form::label('title_en', 'الدولة',['class' => 'col-sm-2 col-form-label']) !!}
         <div class="col-sm-10">
             <select class="form-control" name="country_id">
                 @foreach(\App\Models\Country::withTrashed()->get() as $item)
                     <option value="{{$item->id}}" @if($region->country_id == $item->id) selected @endif>{{$item->title_ar}}</option>
                 @endforeach
             </select>
         </div>
     </div>
         @endif


</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         @if(isset($region))
             <a href="{!! route('regions.index') !!}?country_id={{$region->country_id}}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

         @else
             <a href="{!! route('regions.index') !!}?country_id={{request()->country_id}}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

         @endif

     </div>
 </div>
