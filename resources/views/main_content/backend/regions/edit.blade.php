

@extends('layouts.backend')

@section('pageTitle',"تعديل منطقة")

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

    <li><a href="{{route('regions.index')}}?country_id={{$region->country_id}}">المناطق</a> <i class="fa fa-circle"></i></li>

    <li><span> تعديل</span></li>

    <li><span> منطقة</span></li>

@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/regions.js" type="text/javascript"></script>
@endsection
@section('content')
    <form role="form"  id="addForm" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">العنوان</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">
                    <input type="hidden" name="id" id="id" value="{{$region->id}}">

                    <div class="form-body">

                        <!-- Role Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="title_ar" name="title_ar" value="{{$region->title_ar}}">
                            <label for="title">العنوان عربي</label>
                            <span class="help-block"></span>

                        </div>
                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control" id="title_en" name="title_en" value="{{$region->title_en}}">
                            <label for="title_en">العنوان انجليزي</label>
                            <span class="help-block"></span>
                        </div>


                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <select class="form-control" id="country_id" name="country_id">
                                @foreach(\App\Models\Country::withTrashed()->get() as $country)
                                    <option value="{{$country->id}}" @if($country->id == $region->country_id) selected @endif>{{$country->title_ar}}</option>
                                @endforeach
                            </select>
                            <label for="title_en">الدولة</label>
                            <span class="help-block"></span>
                        </div>


                    </div>
                </div>
            </div>

            <div class="panel-footer text-center">
                <button type="button" class="btn btn-info submit-form"
                >{{_lang('app.save') }}</button>
            </div>

        </div>


    </form>
    <script>
        var new_lang = {

        };
        var new_config = {

        };

    </script>
@endsection
