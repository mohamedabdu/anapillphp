 <div class="form-body">

<!-- Day Field -->
     <div class="form-group row">
     {!! Form::label('day', 'اليوم',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        <select class="form-control" name="day">
            @foreach(\App\Models\StoreWorkDay::$days as $day)
                <option value="{{$day}}">{{$day}}</option>
                @endforeach
        </select>
    </div>
 </div>

<!-- Work From Field -->
     <div class="form-group row">
     {!! Form::label('work_from', 'بداية الدوام',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::time('work_from', null, ['class' => 'form-control','id'=>"work_from"]) !!}
    </div>
 </div>

<!-- Work To Field -->
     <div class="form-group row">
     {!! Form::label('work_to', 'نهاية الدوام',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::time('work_to', null, ['class' => 'form-control','id'=>"work_to"]) !!}
    </div>
 </div>

<!-- Is Custom Field -->
     <div class="form-group row">
     {!! Form::label('is_custom', 'مخصص',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        <select class="form-control" name="is_custom" id="is_custom">
            <option value="0">select</option>
            <option value="1" @if(isset($storeWorkDay) && $storeWorkDay->is_custom == 1) selected @endif>نعم</option>
            <option value="0" @if(isset($storeWorkDay) && $storeWorkDay->is_custom == 0) selected @endif>لا</option>

        </select>
    </div>
     </div>


     <div id="custom_date" style="display:  @if(isset($storeWorkDay) && $storeWorkDay->is_custom == 0) none; @else block; @endif ">
         <div class="form-group row">
             {!! Form::label('custom_date', 'تاريخ مخصص',['class' => 'col-sm-2 col-form-label']) !!}
             <div class="col-sm-10">
                 {!! Form::date('custom_date', null, ['class' => 'form-control','id'=>"work_from"]) !!}
             </div>
         </div>
         {{--<div class="form-group row">--}}
             {{--{!! Form::label('work_from', 'توقيت العمل',['class' => 'col-sm-2 col-form-label']) !!}--}}
             {{--<div class="col-sm-10">--}}
                 {{--{!! Form::time('custom_work_from', null, ['class' => 'form-control','id'=>"work_from"]) !!}--}}
             {{--</div>--}}
         {{--</div>--}}
         {{--<div class="form-group row">--}}
             {{--{!! Form::label('work_from', 'انتهاء العمل',['class' => 'col-sm-2 col-form-label']) !!}--}}
             {{--<div class="col-sm-10">--}}
                 {{--{!! Form::time('custom_work_to', null, ['class' => 'form-control','id'=>"work_from"]) !!}--}}
             {{--</div>--}}
         {{--</div>--}}

     </div>


 </div>



 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         @if(isset($storeWorkDay))
             <a href="{!! route('storeWorkDays.index') !!}?store_id={{$storeWorkDay->store_id}}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

         @else
             <a href="{!! route('storeWorkDays.index') !!}?store_id={{request()->store_id}}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

         @endif

     </div>
 </div>


 <script>
     $( document ).ready(function() {

         var is_custom = $('#is_custom').val();
         if (is_custom > 0) {
             $("#custom_date").fadeIn(1000);
         }
         else {
             $("#custom_date").fadeOut(1000);
         }


         $('#is_custom').on('change', function (e) {

             var is_custom = e.target.value;
             if (is_custom > 0) {
                 $("#custom_date").fadeIn(1000);
             }
             else {
                 $("#custom_date").fadeOut(1000);
             }

         });


     });


 </script>