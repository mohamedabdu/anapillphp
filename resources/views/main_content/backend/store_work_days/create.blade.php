

@extends('layouts.backend')

@section('pageTitle',"اضافة معاد")

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

    <li><a href="{{route('storeWorkDays.index')}}?store_id={{$store_id}}">مواعيد العمل</a> <i class="fa fa-circle"></i></li>

    <li><span> اضافة</span></li>

    <li><span> معاد</span></li>

@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/storeWorkDay.js" type="text/javascript"></script>
@endsection
@section('content')
    <form role="form"  id="addForm" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">التفاصيل</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">
                    <input type="hidden" name="id" id="id" value="0">
                    <input type="hidden" name="store_id" id="store_id" value="{{$store_id}}">

                    <div class="form-body">


                        <!-- Day Field -->
                        <div class="form-group form-md-line-input col-md-12">
                                <select class="form-control" name="day">
                                    @foreach(\App\Models\StoreWorkDay::$days as $day)
                                        <option value="{{$day}}">{{$day}}</option>
                                    @endforeach
                                </select>
                                {!! Form::label('day', 'اليوم',['class' => 'col-sm-2 col-form-label']) !!}
                            <span class="help-block"></span>

                        </div>

                        <!-- Work From Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control timePicker" name="work_from">
                            {!! Form::label('work_from', 'بداية الدوام',['class' => 'col-sm-2 col-form-label']) !!}
                            <span class="help-block"></span>

                        </div>

                        <!-- Work From Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <input type="text" class="form-control timePicker" name="work_to">
                            {!! Form::label('work_to', 'نهاية الدوام',['class' => 'col-sm-2 col-form-label']) !!}
                            <span class="help-block"></span>

                        </div>

                        <!-- Is Custom Field -->
                        <div class="form-group form-md-line-input col-md-12">
                                <select class="form-control" name="is_custom" id="is_custom">
                                    <option value="0">select</option>
                                    <option value="1" @if(isset($storeWorkDay) && $storeWorkDay->is_custom == 1) selected @endif>نعم</option>
                                    <option value="0" @if(isset($storeWorkDay) && $storeWorkDay->is_custom == 0) selected @endif>لا</option>

                                </select>
                                {!! Form::label('is_custom', 'مخصص',['class' => 'col-sm-2 col-form-label']) !!}
                            <span class="help-block"></span>

                        </div>


                        <div id="custom_date" style="display:  @if(isset($storeWorkDay) && $storeWorkDay->is_custom == 0) none; @else block; @endif ">
                            <div class="form-group form-md-line-input col-md-12">
                                <input type="text" class="form-control datePicker" name="custom_date">
                                {!! Form::label('custom_date', 'تاريخ مخصص',['class' => 'col-sm-2 col-form-label']) !!}
                                <span class="help-block"></span>

                            </div>
                            {{--<div class="form-group row">--}}
                            {{--{!! Form::label('work_from', 'توقيت العمل',['class' => 'col-sm-2 col-form-label']) !!}--}}
                            {{--<div class="col-sm-10">--}}
                            {{--{!! Form::time('custom_work_from', null, ['class' => 'form-control','id'=>"work_from"]) !!}--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group row">--}}
                            {{--{!! Form::label('work_from', 'انتهاء العمل',['class' => 'col-sm-2 col-form-label']) !!}--}}
                            {{--<div class="col-sm-10">--}}
                            {{--{!! Form::time('custom_work_to', null, ['class' => 'form-control','id'=>"work_from"]) !!}--}}
                            {{--</div>--}}
                            {{--</div>--}}

                        </div>


                    </div>
                </div>
            </div>

            <div class="panel-footer text-center">
                <button type="button" class="btn btn-info submit-form"
                >{{_lang('app.save') }}</button>
            </div>

        </div>


    </form>
    <script>
        var new_lang = {

        };
        var new_config = {
            store_id: "{{$store_id}}",
        };

    </script>
@endsection
