<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $storeWorkDay->id !!}</p>
</div>

<!-- Store Id Field -->
<div class="form-group">
    {!! Form::label('store_id', 'Store Id:') !!}
    <p>{!! $storeWorkDay->store_id !!}</p>
</div>

<!-- Day Field -->
<div class="form-group">
    {!! Form::label('day', 'Day:') !!}
    <p>{!! $storeWorkDay->day !!}</p>
</div>

<!-- Work From Field -->
<div class="form-group">
    {!! Form::label('work_from', 'Work From:') !!}
    <p>{!! $storeWorkDay->work_from !!}</p>
</div>

<!-- Work To Field -->
<div class="form-group">
    {!! Form::label('work_to', 'Work To:') !!}
    <p>{!! $storeWorkDay->work_to !!}</p>
</div>

<!-- Is Custom Field -->
<div class="form-group">
    {!! Form::label('is_custom', 'Is Custom:') !!}
    <p>{!! $storeWorkDay->is_custom !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $storeWorkDay->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $storeWorkDay->updated_at !!}</p>
</div>

