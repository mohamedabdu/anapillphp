@extends('layouts.backend')

@section('pageTitle',_lang('app.add_store_type'))

@section('breadcrumb')
<li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>
<li><a href="{{route('store_types.index')}}">{{_lang('app.store_types')}}</a> <i class="fa fa-circle"></i></li>
<li><span>{{_lang('app.create')}}</span></li>
@endsection

@section('js')
<script src="{{url('public/backend/js')}}/store_types.js" type="text/javascript"></script>
@endsection

@section('content')
<form role="form"  id="addEditStoreTypesForm" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.title') }}</h3>
        </div>
        <div class="panel-body">


            <div class="form-body">
                <input type="hidden" name="id" id="id" value="0">

                @foreach ($languages as $key => $value)

                <div class="form-group form-md-line-input col-md-6">
                    <input type="text" class="form-control" id="title[{{ $key }}]" name="title[{{ $key }}]" value="">
                    <label for="title">{{_lang('app.title') }} {{ _lang('app. '.$value.'') }}</label>
                    <span class="help-block"></span>
                </div>

                @endforeach
               

            </div>
        </div>
        <div class="panel-footer text-center">
            <button type="button" class="btn btn-info submit-form">{{_lang('app.save') }}</button>
        </div>

    </div>


</form>
<script>
var new_lang = {

};
var new_config = {
    
};

</script>
@endsection