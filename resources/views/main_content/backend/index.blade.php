@extends('layouts.backend')

@section('pageTitle', _lang('app.dashboard'))

@section('breadcrumb')
<li><span> {{_lang('app.dashboard')}}</span></li>
@endsection


@section('content')
<div class="row widget-row">
    <div class="col-md-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading">{{_lang('app.product_categories')}}</h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-green icon-bulb"></i>
                <div class="widget-thumb-body">
                    <span class="widget-thumb-subtitle">{{_lang('app.category')}}</span>
                    <span class="widget-thumb-body-stat" data-counter="counterup" data-value="7,644">7,644</span>
                </div>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
    <div class="col-md-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading">Weekly Sales</h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-red icon-layers"></i>
                <div class="widget-thumb-body">
                    <span class="widget-thumb-subtitle">USD</span>
                    <span class="widget-thumb-body-stat" data-counter="counterup" data-value="1,293">1,293</span>
                </div>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
    <div class="col-md-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading">Biggest Purchase</h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-purple icon-screen-desktop"></i>
                <div class="widget-thumb-body">
                    <span class="widget-thumb-subtitle">USD</span>
                    <span class="widget-thumb-body-stat" data-counter="counterup" data-value="815">815</span>
                </div>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
    <div class="col-md-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading">Average Monthly</h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-blue icon-bar-chart"></i>
                <div class="widget-thumb-body">
                    <span class="widget-thumb-subtitle">USD</span>
                    <span class="widget-thumb-body-stat" data-counter="counterup" data-value="5,071">5,071</span>
                </div>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
</div>
    @if(auth()->user()->role == 1)
        <div class="row" style="margin-top: 40px;">

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 blue-steel" href="">
                <div class="visual">
                    <i class="fa fa-book"></i>
                </div>
                <div class="details">
                    <div class="number">
                    <span data-counter="counterup"
                          data-value="{{\App\Models\Category::count("id")}}">{{\App\Models\Category::count("id")}}</span>
                    </div>
                    
                    <div class="desc">الأقسام</div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 grey-mint" href="">
                <div class="visual">
                    <i class="fa fa-list"></i>
                </div>
                <div class="details">
                    <div class="number">
                    <span data-counter="counterup"
                          data-value="{{\App\Models\City::count("id")}}">{{\App\Models\City::count("id")}}</span>
                    </div>
                    <div class="desc">المدن</div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 yellow-gold" href="">
                <div class="visual">
                    <i class="fa fa-newspaper-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                    <span data-counter="counterup"
                          data-value="{{\App\Models\Region::count("id")}}">{{\App\Models\Region::count("id")}}</span>
                    </div>
                    <div class="desc">المناطق</div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 blue-chambray" href="">
                <div class="visual">
                    <i class="fa fa-pencil"></i>
                </div>
                <div class="details">
                    <div class="number">
                    <span data-counter="counterup"
                          data-value="{{\App\Models\Product::whereNull("competitor_id")->count("id")}}">{{\App\Models\Product::whereNull("competitor_id")->count("id")}}</span>
                    </div>
                    <div class="desc">المنتجات</div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 blue" href="">
                <div class="visual">
                    <i class="fa fa-users"></i>
                </div>
                <div class="details">
                    <div class="number">
                    <span data-counter="counterup"
                          data-value="{{\App\Models\User::where("role",">","1")->count("id")}}">{{\App\Models\User::where("role",">","1")->count("id")}}</span>
                    </div>
                    <div class="desc">مديرى لوحة التحكم </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 green-jungle" href="">
                <div class="visual">
                    <i class="fa fa-envelope"></i>
                </div>
                <div class="details">
                    <div class="number">
                    <span data-counter="counterup"
                          data-value="{{\App\Models\Agent::count("id")}}">{{\App\Models\Agent::count("id")}}</span>
                    </div>
                    <div class="desc">الموظفيين</div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 grey-mint" href="">
                <div class="visual">
                    <i class="fa fa-envelope"></i>
                </div>
                <div class="details">
                    <div class="number">
                    <span data-counter="counterup"
                          data-value="{{\App\Models\Supervisor::count("id")}}">{{\App\Models\Supervisor::count("id")}}</span>
                    </div>
                    <div class="desc">المشرفيين</div>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 blue-steel" href="">
                <div class="visual">
                    <i class="fa fa-envelope"></i>
                </div>
                <div class="details">
                    <div class="number">
                    <span data-counter="counterup"
                          data-value="{{\App\Models\Store::where("is_main",1)->count("id")}}">{{\App\Models\Store::where("is_main",1)->count("id")}}</span>
                    </div>
                    <div class="desc">المحلات</div>
                </div>
            </a>
        </div>



        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 blue-steel" href="">
                <div class="visual">
                    <i class="fa fa-book"></i>
                </div>
                <div class="details">
                    <div class="number">
                    <span data-counter="counterup"
                          data-value="{{\App\Models\Competitor::count("id")}}">{{\App\Models\Competitor::count("id")}}</span>
                    </div>
                    <div class="desc">المنافسيين</div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 grey-mint" href="">
                <div class="visual">
                    <i class="fa fa-list"></i>
                </div>
                <div class="details">
                    <div class="number">
                    <span data-counter="counterup"
                          data-value="{{\App\Models\Product::whereNotNull("competitor_id")->count("id")}}">{{\App\Models\Product::whereNotNull("competitor_id")->count("id")}}</span>
                    </div>
                    <div class="desc">منتجات المنافسين</div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 yellow-gold" href="">
                <div class="visual">
                    <i class="fa fa-newspaper-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                    <span data-counter="counterup"
                          data-value="0">0</span>
                    </div>
                    <div class="desc">التقارير الخاصة بالمنافسين</div>
                </div>
            </a>
        </div>


        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 blue-chambray" href="">
                <div class="visual">
                    <i class="fa fa-pencil"></i>
                </div>
                <div class="details">
                    <div class="number">
                    <span data-counter="counterup"
                          data-value="{{$visitsProcessing}}">{{$visitsProcessing}}</span>
                    </div>
                    <div class="desc">عدد الزيارات المجدولة ولم تنفذ</div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 blue" href="">
                <div class="visual">
                    <i class="fa fa-users"></i>
                </div>
                <div class="details">
                    <div class="number">
                    <span data-counter="counterup"
                          data-value="{{$visitsCompleted}}">{{$visitsCompleted}}</span>
                    </div>
                    <div class="desc">عدد الزيارات المنجزة  </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 green-jungle" href="">
                <div class="visual">
                    <i class="fa fa-envelope"></i>
                </div>
                <div class="details">
                    <div class="number">
                    <span data-counter="counterup"
                          data-value="0">0</span>
                    </div>
                    <div class="desc">التقارير الطارئة المرسلة من قبل الموظفين</div>
                </div>
            </a>
        </div>


    </div>
    @endif

@endsection