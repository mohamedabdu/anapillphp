 <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><i class="fa fa-table"></i> Data Table Example</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="default-datatable" class="dataTable table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Agent Id</th>
        <th>Visit Id</th>
        <th>Visit Store Id</th>
        <th>Start Date</th>
        <th>Lat</th>
        <th>Lng</th>
        <th>End Date</th>
        <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>


                            <script>
                                $( document ).ready(function() {
                                    datatable = $('.dataTable').dataTable({
                                        //"processing": true,
                                        "serverSide": true,
                                        "ajax": {
                                            "url": "{{route('visitDetails.ajax')}}",
                                            "type": "GET",
                                        },
                                        "columns": [
                                            {"data": "id"},
               {"data": "agent_id"},
               {"data": "visit_id"},
               {"data": "visit_store_id"},
               {"data": "start_date"},
               {"data": "lat"},
               {"data": "lng"},
               {"data": "end_date"},
               {"data": "status"},

                                            {"data": "options", orderable: false, searchable: false}
                                        ],
                                        "order": [
                                            [0, "desc"]
                                        ],
                                        //"oLanguage": {"sUrl": config.url + '/datatable-lang-' + "ar" + '.json'}

                                    });
                                });
                            </script>

                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->