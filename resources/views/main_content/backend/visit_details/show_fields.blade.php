<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $visitDetail->id !!}</p>
</div>

<!-- Agent Id Field -->
<div class="form-group">
    {!! Form::label('agent_id', 'Agent Id:') !!}
    <p>{!! $visitDetail->agent_id !!}</p>
</div>

<!-- Visit Id Field -->
<div class="form-group">
    {!! Form::label('visit_id', 'Visit Id:') !!}
    <p>{!! $visitDetail->visit_id !!}</p>
</div>

<!-- Visit Store Id Field -->
<div class="form-group">
    {!! Form::label('visit_store_id', 'Visit Store Id:') !!}
    <p>{!! $visitDetail->visit_store_id !!}</p>
</div>

<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', 'Start Date:') !!}
    <p>{!! $visitDetail->start_date !!}</p>
</div>

<!-- Lat Field -->
<div class="form-group">
    {!! Form::label('lat', 'Lat:') !!}
    <p>{!! $visitDetail->lat !!}</p>
</div>

<!-- Lng Field -->
<div class="form-group">
    {!! Form::label('lng', 'Lng:') !!}
    <p>{!! $visitDetail->lng !!}</p>
</div>

<!-- End Date Field -->
<div class="form-group">
    {!! Form::label('end_date', 'End Date:') !!}
    <p>{!! $visitDetail->end_date !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $visitDetail->status !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $visitDetail->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $visitDetail->updated_at !!}</p>
</div>

