 <div class="form-body">
<!-- Agent Id Field -->
     <div class="form-group row">
     {!! Form::label('agent_id', 'Agent Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('agent_id', null, ['class' => 'form-control','id'=>"agent_id","required"]) !!}
    </div>
 </div>

<!-- Visit Id Field -->
     <div class="form-group row">
     {!! Form::label('visit_id', 'Visit Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('visit_id', null, ['class' => 'form-control','id'=>"visit_id","required"]) !!}
    </div>
 </div>

<!-- Visit Store Id Field -->
     <div class="form-group row">
     {!! Form::label('visit_store_id', 'Visit Store Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('visit_store_id', null, ['class' => 'form-control','id'=>"visit_store_id","required"]) !!}
    </div>
 </div>

<!-- Start Date Field -->
     <div class="form-group row">
     {!! Form::label('start_date', 'Start Date:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('start_date', null, ['class' => 'form-control','id'=>"start_date","required"]) !!}
    </div>
 </div>

<!-- Lat Field -->
     <div class="form-group row">
     {!! Form::label('lat', 'Lat:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('lat', null, ['class' => 'form-control','id'=>"lat","required"]) !!}
    </div>
 </div>

<!-- Lng Field -->
     <div class="form-group row">
     {!! Form::label('lng', 'Lng:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('lng', null, ['class' => 'form-control','id'=>"lng","required"]) !!}
    </div>
 </div>

<!-- End Date Field -->
     <div class="form-group row">
     {!! Form::label('end_date', 'End Date:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('end_date', null, ['class' => 'form-control','id'=>"end_date","required"]) !!}
    </div>
 </div>

<!-- Status Field -->
     <div class="form-group row">
     {!! Form::label('status', 'Status:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('status', null, ['class' => 'form-control','id'=>"status","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('visitDetails.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
