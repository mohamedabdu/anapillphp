 <div class="form-body">
<!-- Store Id Field -->
     <div class="form-group row">
     {!! Form::label('store_id', 'Store Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('store_id', null, ['class' => 'form-control','id'=>"store_id","required"]) !!}
    </div>
 </div>

<!-- Image Field -->

  <div class="input-group mb-3">
   <div class="custom-file">
     <input type="file" name="image" class="custom-file-input" id="inputGroupFile02">
     <label class="custom-file-label" for="inputGroupFile02">Image</label>
      </div>

   </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('storeImages.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
