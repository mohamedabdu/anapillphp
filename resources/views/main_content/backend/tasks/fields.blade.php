 <div class="form-body">
<!-- Title Ar Field -->
     <!-- Title Ar Field -->
     <div class="form-group row">
         {!! Form::label('title_ar', 'الأسم عربي',['class' => 'col-sm-2 col-form-label']) !!}
         <div class="col-sm-10">
             {!! Form::text('title_ar', null, ['class' => 'form-control','id'=>"title_ar","required"]) !!}
         </div>
     </div>

     <!-- Title En Field -->
     <div class="form-group row">
         {!! Form::label('title_en', 'الأسم انجليزي',['class' => 'col-sm-2 col-form-label']) !!}
         <div class="col-sm-10">
             {!! Form::text('title_en', null, ['class' => 'form-control','id'=>"title_en","required"]) !!}
         </div>
     </div>

<!-- Process Field -->
     {{--<div class="form-group row">--}}
     {{--{!! Form::label('process', 'Process:',['class' => 'col-sm-2 col-form-label']) !!}--}}
    {{--<div class="col-sm-10">--}}
    {{--{!! Form::text('process', null, ['class' => 'form-control','id'=>"process","required"]) !!}--}}
    {{--</div>--}}
 {{--</div>--}}

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('tasks.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
