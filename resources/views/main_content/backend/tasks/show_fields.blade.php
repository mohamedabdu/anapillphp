<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $task->id !!}</p>
</div>

<!-- Title Ar Field -->
<div class="form-group">
    {!! Form::label('title_ar', 'Title Ar:') !!}
    <p>{!! $task->title_ar !!}</p>
</div>

<!-- Title En Field -->
<div class="form-group">
    {!! Form::label('title_en', 'Title En:') !!}
    <p>{!! $task->title_en !!}</p>
</div>

<!-- Process Field -->
<div class="form-group">
    {!! Form::label('process', 'Process:') !!}
    <p>{!! $task->process !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $task->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $task->updated_at !!}</p>
</div>

