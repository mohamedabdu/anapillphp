 <div class="form-body">
<!-- System Email Field -->
     <div class="form-group row">
     {!! Form::label('system_email', 'البريد الألكتروني',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('system_email', null, ['class' => 'form-control','id'=>"system_email","required"]) !!}
    </div>
 </div>

<!-- System Mobile Field -->
     <div class="form-group row">
     {!! Form::label('system_mobile', 'رقم الجوال',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('system_mobile', null, ['class' => 'form-control','id'=>"system_mobile","required"]) !!}
    </div>
 </div>


<!-- About Field -->
 <div class="form-group row">
     {!! Form::label('about', 'عن النظام',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::textarea('about', null, ['class' => 'form-control','id'=>"about","required"]) !!}
    </div>
 </div>


<!-- Rules Field -->
 <div class="form-group row">
     {!! Form::label('rules', 'الشروط',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::textarea('rules', null, ['class' => 'form-control','id'=>"rules","required"]) !!}
    </div>
 </div>

<!-- Facebook Field -->
     <div class="form-group row">
     {!! Form::label('facebook', 'Facebook:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('facebook', null, ['class' => 'form-control','id'=>"facebook","required"]) !!}
    </div>
 </div>

<!-- Twitter Field -->
     <div class="form-group row">
     {!! Form::label('twitter', 'Twitter:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('twitter', null, ['class' => 'form-control','id'=>"twitter","required"]) !!}
    </div>
 </div>

     <div class="form-group row">
         {!! Form::label('twitter', 'ايام العطلة',['class' => 'col-sm-2 col-form-label']) !!}
         <div class="row col-lg-offset-2">


     @foreach(\App\Models\StoreWorkDay::$days as $day)
         {!! Form::label('day', $day,['class' => 'col-sm-1 col-form-label']) !!}
         <div class="col-sm-2">
             <div class="col-sm-1">
                 <div class="icheck-material-danger">

                     <input type="checkbox" id="{{$day}}" name="days[]" value="{{$day}}" @if(isset($days) && in_array($day,$days)) checked @endif />
                     <label for="{{$day}}">  </label>
                 </div>
             </div>

             {{--             <select class="form-control" name="day">--}}

             {{--                 @foreach(\App\Models\StoreWorkDay::$days as $day)--}}
             {{--                     <option value="{{$day}}">{{$day}}</option>--}}
             {{--                 @endforeach--}}
             {{--             </select>--}}
         </div>
     @endforeach
         </div>

     </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('configs.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
