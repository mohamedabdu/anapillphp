@extends('layouts.backend')

@section('pageTitle',_lang('app.edit_config'))

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

    <li><span> {{_lang('app.edit')}}</span></li>

    <li><span> {{_lang('app.config')}}</span></li>

@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/configs.js" type="text/javascript"></script>
@endsection
@section('content')
    <form role="form" id="addForm" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">التواصل</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="system_email" name="system_email" value="{{$config->system_email}}">
                        <label for="system_email">البريد الألكتروني</label>
                        <span class="help-block"></span>
                    </div>

                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="system_mobile" name="system_mobile" value="{{$config->system_mobile}}">
                        <label for="system_mobile">رقم الجوال</label>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="facebook" name="facebook" value="{{$config->facebook}}">
                        <label for="facebook">فيسبوك</label>
                        <span class="help-block"></span>
                    </div>

                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="twitter" name="twitter" value="{{$config->twitter}}">
                        <label for="twitter">تويتر</label>
                        <span class="help-block"></span>
                    </div>

                </div>
            </div>


        </div>


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">الأعدادات</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">
                    <input type="hidden" name="id" id="id" value="{{ $config->id }}">

                    <div class="form-group form-md-line-input col-md-12">
                        <textarea class="form-control" id="about" name="about" rows="4">{{$config->about}}</textarea>
                        <label for="about">عن النظام</label>
                        <span class="help-block"></span>
                    </div>


                    <div class="form-group form-md-line-input col-md-12">
                        <textarea class="form-control" id="rules" name="rules" rows="4">{{$config->rules}}</textarea>
                        <label for="about"> الشروط</label>
                        <span class="help-block"></span>
                    </div>


                </div>
            </div>


        </div>


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">ايام العطلة</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">

                    <div class="form-group form-md-line-input col-md-12">
                        @foreach(\App\Models\StoreWorkDay::$days as $day)
                            <div class="form-group col-md-2" style="padding-top:30px; margin-left:5%">
                                <label class="checkbox-inline">
                                    <input type="checkbox" data-toggle="toggle" data-style="ios" id="days" name="days[]" value="{{$day}}"
                                            {{isset($days) && in_array($day,$days) ? "checked" : "" }}> {{$day}}
                                </label>
                            </div>

                        @endforeach


                        <span class="help-block"></span>

                    </div>




                </div>
            </div>

            <div class="panel-footer text-center">
                <button type="button" class="btn btn-info submit-form"
                >{{_lang('app.save') }}</button>
            </div>

        </div>


    </form>
    <script>
        var new_lang = {

        };
        var new_config = {

        };

    </script>
@endsection