<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $config->id !!}</p>
</div>

<!-- System Email Field -->
<div class="form-group">
    {!! Form::label('system_email', 'System Email:') !!}
    <p>{!! $config->system_email !!}</p>
</div>

<!-- System Mobile Field -->
<div class="form-group">
    {!! Form::label('system_mobile', 'System Mobile:') !!}
    <p>{!! $config->system_mobile !!}</p>
</div>

<!-- About Field -->
<div class="form-group">
    {!! Form::label('about', 'About:') !!}
    <p>{!! $config->about !!}</p>
</div>

<!-- Rules Field -->
<div class="form-group">
    {!! Form::label('rules', 'Rules:') !!}
    <p>{!! $config->rules !!}</p>
</div>

<!-- Facebook Field -->
<div class="form-group">
    {!! Form::label('facebook', 'Facebook:') !!}
    <p>{!! $config->facebook !!}</p>
</div>

<!-- Twitter Field -->
<div class="form-group">
    {!! Form::label('twitter', 'Twitter:') !!}
    <p>{!! $config->twitter !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $config->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $config->updated_at !!}</p>
</div>

