<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $visitTaskDetail->id !!}</p>
</div>

<!-- Visit Detail Id Field -->
<div class="form-group">
    {!! Form::label('visit_detail_id', 'Visit Detail Id:') !!}
    <p>{!! $visitTaskDetail->visit_detail_id !!}</p>
</div>

<!-- Task Id Field -->
<div class="form-group">
    {!! Form::label('task_id', 'Task Id:') !!}
    <p>{!! $visitTaskDetail->task_id !!}</p>
</div>

<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', 'Start Date:') !!}
    <p>{!! $visitTaskDetail->start_date !!}</p>
</div>

<!-- End Date Field -->
<div class="form-group">
    {!! Form::label('end_date', 'End Date:') !!}
    <p>{!! $visitTaskDetail->end_date !!}</p>
</div>

<!-- Start Lat Field -->
<div class="form-group">
    {!! Form::label('start_lat', 'Start Lat:') !!}
    <p>{!! $visitTaskDetail->start_lat !!}</p>
</div>

<!-- Start Lng Field -->
<div class="form-group">
    {!! Form::label('start_lng', 'Start Lng:') !!}
    <p>{!! $visitTaskDetail->start_lng !!}</p>
</div>

<!-- End Lat Field -->
<div class="form-group">
    {!! Form::label('end_lat', 'End Lat:') !!}
    <p>{!! $visitTaskDetail->end_lat !!}</p>
</div>

<!-- End Lng Field -->
<div class="form-group">
    {!! Form::label('end_lng', 'End Lng:') !!}
    <p>{!! $visitTaskDetail->end_lng !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $visitTaskDetail->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $visitTaskDetail->updated_at !!}</p>
</div>

