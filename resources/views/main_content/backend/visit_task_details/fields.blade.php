 <div class="form-body">
<!-- Visit Detail Id Field -->
     <div class="form-group row">
     {!! Form::label('visit_detail_id', 'Visit Detail Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('visit_detail_id', null, ['class' => 'form-control','id'=>"visit_detail_id","required"]) !!}
    </div>
 </div>

<!-- Task Id Field -->
     <div class="form-group row">
     {!! Form::label('task_id', 'Task Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('task_id', null, ['class' => 'form-control','id'=>"task_id","required"]) !!}
    </div>
 </div>

<!-- Start Date Field -->
     <div class="form-group row">
     {!! Form::label('start_date', 'Start Date:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('start_date', null, ['class' => 'form-control','id'=>"start_date","required"]) !!}
    </div>
 </div>

<!-- End Date Field -->
     <div class="form-group row">
     {!! Form::label('end_date', 'End Date:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('end_date', null, ['class' => 'form-control','id'=>"end_date","required"]) !!}
    </div>
 </div>

<!-- Start Lat Field -->
     <div class="form-group row">
     {!! Form::label('start_lat', 'Start Lat:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('start_lat', null, ['class' => 'form-control','id'=>"start_lat","required"]) !!}
    </div>
 </div>

<!-- Start Lng Field -->
     <div class="form-group row">
     {!! Form::label('start_lng', 'Start Lng:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('start_lng', null, ['class' => 'form-control','id'=>"start_lng","required"]) !!}
    </div>
 </div>

<!-- End Lat Field -->
     <div class="form-group row">
     {!! Form::label('end_lat', 'End Lat:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('end_lat', null, ['class' => 'form-control','id'=>"end_lat","required"]) !!}
    </div>
 </div>

<!-- End Lng Field -->
     <div class="form-group row">
     {!! Form::label('end_lng', 'End Lng:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('end_lng', null, ['class' => 'form-control','id'=>"end_lng","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('visitTaskDetails.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
