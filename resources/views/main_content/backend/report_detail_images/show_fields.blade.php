<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $reportDetailImage->id !!}</p>
</div>

<!-- Report Id Field -->
<div class="form-group">
    {!! Form::label('report_id', 'Report Id:') !!}
    <p>{!! $reportDetailImage->report_id !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $reportDetailImage->image !!}</p>
</div>

