 <div class="form-body">
<!-- Visit Task Detail Id Field -->
     <div class="form-group row">
     {!! Form::label('visit_task_detail_id', 'Visit Task Detail Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('visit_task_detail_id', null, ['class' => 'form-control','id'=>"visit_task_detail_id","required"]) !!}
    </div>
 </div>

<!-- Products Id Field -->
     <div class="form-group row">
     {!! Form::label('products_id', 'Products Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('products_id', null, ['class' => 'form-control','id'=>"products_id","required"]) !!}
    </div>
 </div>

<!-- Bill Id Field -->
     <div class="form-group row">
     {!! Form::label('bill_id', 'Bill Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('bill_id', null, ['class' => 'form-control','id'=>"bill_id","required"]) !!}
    </div>
 </div>

<!-- Number Piece Field -->
     <div class="form-group row">
     {!! Form::label('number_piece', 'Number Piece:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('number_piece', null, ['class' => 'form-control','id'=>"number_piece","required"]) !!}
    </div>
 </div>

<!-- Number Of Carton Field -->
     <div class="form-group row">
     {!! Form::label('number_of_carton', 'Number Of Carton:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('number_of_carton', null, ['class' => 'form-control','id'=>"number_of_carton","required"]) !!}
    </div>
 </div>

<!-- Return Reason Id Field -->
     <div class="form-group row">
     {!! Form::label('return_reason_id', 'Return Reason Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('return_reason_id', null, ['class' => 'form-control','id'=>"return_reason_id","required"]) !!}
    </div>
 </div>

<!-- Expire End Field -->
     <div class="form-group row">
     {!! Form::label('expire_end', 'Expire End:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('expire_end', null, ['class' => 'form-control','id'=>"expire_end","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('visitTaskReturnDetails.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
