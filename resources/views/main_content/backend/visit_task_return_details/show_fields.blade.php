<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $visitTaskReturnDetail->id !!}</p>
</div>

<!-- Visit Task Detail Id Field -->
<div class="form-group">
    {!! Form::label('visit_task_detail_id', 'Visit Task Detail Id:') !!}
    <p>{!! $visitTaskReturnDetail->visit_task_detail_id !!}</p>
</div>

<!-- Products Id Field -->
<div class="form-group">
    {!! Form::label('products_id', 'Products Id:') !!}
    <p>{!! $visitTaskReturnDetail->products_id !!}</p>
</div>

<!-- Bill Id Field -->
<div class="form-group">
    {!! Form::label('bill_id', 'Bill Id:') !!}
    <p>{!! $visitTaskReturnDetail->bill_id !!}</p>
</div>

<!-- Number Piece Field -->
<div class="form-group">
    {!! Form::label('number_piece', 'Number Piece:') !!}
    <p>{!! $visitTaskReturnDetail->number_piece !!}</p>
</div>

<!-- Number Of Carton Field -->
<div class="form-group">
    {!! Form::label('number_of_carton', 'Number Of Carton:') !!}
    <p>{!! $visitTaskReturnDetail->number_of_carton !!}</p>
</div>

<!-- Return Reason Id Field -->
<div class="form-group">
    {!! Form::label('return_reason_id', 'Return Reason Id:') !!}
    <p>{!! $visitTaskReturnDetail->return_reason_id !!}</p>
</div>

<!-- Expire End Field -->
<div class="form-group">
    {!! Form::label('expire_end', 'Expire End:') !!}
    <p>{!! $visitTaskReturnDetail->expire_end !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $visitTaskReturnDetail->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $visitTaskReturnDetail->updated_at !!}</p>
</div>

