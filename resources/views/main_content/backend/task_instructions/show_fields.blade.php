

@if(isset($taskInstruction->task))
<!-- Task Id Field -->
<div class="form-group">
    {!! Form::label('task_id', 'المهمة') !!}
    <p>{!! $taskInstruction->task->title_ar !!}</p>
</div>
@endif

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'الوصف') !!}
    <p>{!! $taskInstruction->description !!}</p>
</div>

@if(isset($taskInstruction))
    <div class="form-group">
        {!! Form::label('images', 'الصور') !!}
        <div class="row">
            @foreach($taskInstruction->images as $image)
                <div class="col-md-4"> <img src="{{\Helper\Common\imageUrl($image->image)}}" class="img-responsive col-md-12"> </div>
            @endforeach
        </div>
        <br><br>
    </div>

@endif

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'تاريخ الأنشاء') !!}
    <p>{!! $taskInstruction->created_at !!}</p>
</div>
