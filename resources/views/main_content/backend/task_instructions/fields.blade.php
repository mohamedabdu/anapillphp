 <div class="form-body">
<!-- Task Id Field -->
     <div class="form-group row">
     {!! Form::label('task_id', 'المهمة',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        <select class="form-control" name="task_id">
            @foreach(\App\Models\Task::all() as $value)
                <option value="{{$value->id}}" @if(isset($taskInstruction) && $taskInstruction->task_id == $value->id) selected @endif>{{$value->title_ar}}</option>
            @endforeach
        </select>
    </div>
 </div>


<!-- Description Field -->
 <div class="form-group row">
     {!! Form::label('description', 'الوصف',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::textarea('description_ar', null, ['class' => 'form-control','id'=>"description","required"]) !!}
    </div>
 </div>

     <!-- Description Field -->
     <div class="form-group row">
         {!! Form::label('description', 'الوصف انجليزي',['class' => 'col-sm-2 col-form-label']) !!}
         <div class="col-sm-10">
             {!! Form::textarea('description_en', null, ['class' => 'form-control','id'=>"description"]) !!}
         </div>
     </div>



     <div class="input-group mb-3">
         <div class="custom-file">
             <input type="file" name="image[]" multiple class="custom-file-input" id="inputGroupFile02">
             <label class="custom-file-label" for="inputGroupFile02">صور الوصف</label>
         </div>

     </div>

 @if(isset($taskInstruction))
    <div class="row">
        @foreach($taskInstruction->images as $image)
            <div class="col-md-4"> <img src="{{\Helper\Common\imageUrl($image->image)}}" class="img-responsive col-md-12"> </div>
            @endforeach
    </div>
     <br><br>
 @endif

</div>


<div class="form-group row">
 <label class="col-sm-2 col-form-label"></label>
 <div class="col-sm-10">
     <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
     <a href="{!! route('taskInstructions.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

 </div>
</div>
