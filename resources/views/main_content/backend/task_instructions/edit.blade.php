

@extends('layouts.backend')

@section('pageTitle',"تعديل تعليمات")

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

    <li><a href="{{route('taskInstructions.index')}}">التعليمات</a> <i class="fa fa-circle"></i></li>

    <li><span> تعديل</span></li>

    <li><span> تعليمات</span></li>

@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/taskNotes.js" type="text/javascript"></script>
@endsection
@section('content')
    <form role="form"  id="addForm" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">تفاصيل</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">
                    <input type="hidden" name="id" id="id" value="0">

                    <div class="form-body">

                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <select class="form-control" name="task_id">
                                @foreach(\App\Models\Task::all() as $value)
                                    <option value="{{$value->id}}" @if(isset($taskInstruction) && $taskInstruction->task_id == $value->id) selected @endif>{{$value->title_ar}}</option>
                                @endforeach
                            </select>
                            <label for="end_date">المهمة</label>
                            <span class="help-block"></span>
                        </div>

                        <!-- Role Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <textarea name="description_ar" class="form-control" rows="4">{{$taskInstruction->description_ar}}</textarea>
                            <label for="description_ar">الوصف عربي</label>
                            <span class="help-block"></span>

                        </div>
                        <!-- title en Field -->

                        <!-- title en Field -->
                        <div class="form-group form-md-line-input col-md-12">
                            <textarea name="description_en" class="form-control" rows="4">{{$taskInstruction->description_en}}</textarea>
                            <label for="description_en">الوصف انجليزي</label>
                            <span class="help-block"></span>
                        </div>





                    </div>
                </div>
            </div>


        </div>


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">صور الوصف</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">

                    @for ($i = 0; $i < 10; $i++)
                        <div class="form-group form-md-line-input col-md-2">
                            <div class="image_{{$i}}_box">
                                @if (isset($taskInstruction->images[$i]) && $i != 0)
                                    <a href="javascript:;" class="remove_image_link" onclick="My.deleteImage(this);return false;"
                                       data-model="TasksNotes" data-folder="articles" data-col="contract_images"
                                       data-image="{{ isset($taskInstruction->images[$i]) ? $taskInstruction->images[$i]->image : '' }}">
                                        <img src="{{ url('delete-btn.png') }}" /></a>
                                @endif
                                <img src="{{ isset($taskInstruction->images[$i]) ? \Helper\Common\imageUrl($taskInstruction->images[$i]->image) : url('no-image.png') }}"
                                     width="100" height="80" class="{{ isset($taskInstruction->images[$i]) && $i != 0 ? '' : 'image_'.$i.'' }}" />
                            </div>
                            <input type="file" name="image[{{$i}}]" id="image_{{$i}}" style="visibility: hidden">
                            <span class="help-block"></span>
                        </div>
                    @endfor

                    <div class="clearfix"></div>

                </div>
            </div>


            <div class="panel-footer text-center">
                <button type="button" class="btn btn-info submit-form"
                >{{_lang('app.save') }}</button>
            </div>

        </div>




    </form>
    <script>
        var new_lang = {

        };
        var new_config = {

        };

    </script>
@endsection
