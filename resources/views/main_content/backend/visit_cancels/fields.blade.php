 <div class="form-body">
<!-- Agent Id Field -->
     <div class="form-group row">
     {!! Form::label('agent_id', 'Agent Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('agent_id', null, ['class' => 'form-control','id'=>"agent_id","required"]) !!}
    </div>
 </div>

<!-- Visit Id Field -->
     <div class="form-group row">
     {!! Form::label('visit_id', 'Visit Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('visit_id', null, ['class' => 'form-control','id'=>"visit_id","required"]) !!}
    </div>
 </div>

<!-- Visit Store Id Field -->
     <div class="form-group row">
     {!! Form::label('visit_store_id', 'Visit Store Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('visit_store_id', null, ['class' => 'form-control','id'=>"visit_store_id","required"]) !!}
    </div>
 </div>

<!-- Note Field -->
     <div class="form-group row">
     {!! Form::label('note', 'Note:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('note', null, ['class' => 'form-control','id'=>"note","required"]) !!}
    </div>
 </div>

<!-- Is Agree Field -->
     <div class="form-group row">
     {!! Form::label('is_agree', 'Is Agree:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('is_agree', null, ['class' => 'form-control','id'=>"is_agree","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('visitCancels.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
