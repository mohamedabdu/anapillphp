<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $visitCancel->id !!}</p>
</div>

<!-- Agent Id Field -->
<div class="form-group">
    {!! Form::label('agent_id', 'Agent Id:') !!}
    <p>{!! $visitCancel->agent_id !!}</p>
</div>

<!-- Visit Id Field -->
<div class="form-group">
    {!! Form::label('visit_id', 'Visit Id:') !!}
    <p>{!! $visitCancel->visit_id !!}</p>
</div>

<!-- Visit Store Id Field -->
<div class="form-group">
    {!! Form::label('visit_store_id', 'Visit Store Id:') !!}
    <p>{!! $visitCancel->visit_store_id !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $visitCancel->note !!}</p>
</div>

<!-- Is Agree Field -->
<div class="form-group">
    {!! Form::label('is_agree', 'Is Agree:') !!}
    <p>{!! $visitCancel->is_agree !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $visitCancel->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $visitCancel->updated_at !!}</p>
</div>

