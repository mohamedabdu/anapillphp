@extends('layouts.backend')

@section('pageTitle', _lang('app.promotions'))

@section('breadcrumb')
<li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>
<li><span> {{_lang('app.promotions')}}</span></li>
@endsection

@section('js')
<script src="{{url('public/backend/js')}}/promotions.js" type="text/javascript"></script>
@endsection

@section('content')
@if (session()->has('error_message'))
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
    <strong>{{_lang('app.error')}}!</strong> {{session()->get('error_message')}}.
</div>
@endif
 {{ csrf_field() }}
<div class = "panel panel-default">
    <div class = "panel-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a class="btn green" style="margin-bottom: 40px;" href = "{{ route('promotions.create') }}" onclick="">{{ _lang('app.add_new')}}<i class="fa fa-plus"></i> </a>
                    </div>
                </div>
            </div>
        </div>
        <table class= "table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer">
            <thead>
                <tr>
                    <th>{{_lang('app.title')}}</th>
                    <th>{{_lang('app.options')}}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

        <!--Table Wrapper Finish-->
    </div>
</div>
<script>
var new_lang = {

};
var new_config = {
    
};
</script>
@endsection