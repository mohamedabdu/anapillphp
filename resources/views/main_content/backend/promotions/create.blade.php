@extends('layouts.backend')

@section('pageTitle',_lang('app.add_promotion'))

@section('breadcrumb')
<li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>
<li><a href="{{route('promotions.index')}}">{{_lang('app.promotions')}}</a> <i class="fa fa-circle"></i></li>
<li><span>{{_lang('app.create')}}</span></li>
@endsection

@section('js')
<script src="{{url('public/backend/js')}}/promotions.js" type="text/javascript"></script>
@endsection

@section('content')
<form role="form"  id="addEditPromotionsForm" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.title') }}</h3>
        </div>
        <div class="panel-body">


            <div class="form-body">
                <input type="hidden" name="id" id="id" value="0">

                @foreach ($languages as $key => $value)

                <div class="form-group form-md-line-input col-md-6">
                    <input type="text" class="form-control" id="title[{{ $key }}]" name="title[{{ $key }}]" value="">
                    <label for="title">{{_lang('app.title') }} {{ _lang('app. '.$value.'') }}</label>
                    <span class="help-block"></span>
                </div>

                @endforeach
               

            </div>
        </div>
      

    </div>

    <div class="panel panel-default">
       
        <div class="panel-body">
            <div class="form-body">
               
                <div class="form-group form-md-line-input col-md-2">
                    <div class="image_box">
                        <img src="{{url('no-image.png')}}" width="100" height="80" class="image" />
                    </div>
                    <input type="file" name="image" id="image" style="visibility: hidden">
                    <span class="help-block"></span>
                </div>
              
    
            </div>
        </div>
    
        <div class="panel-footer text-center">
            <button type="button" class="btn btn-info submit-form">{{_lang('app.save') }}</button>
        </div>
    
    
    </div>


</form>
<script>
var new_lang = {

};
var new_config = {
    
};

</script>
@endsection