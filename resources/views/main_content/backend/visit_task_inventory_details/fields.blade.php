 <div class="form-body">
<!-- Visit Task Detail Id Field -->
     <div class="form-group row">
     {!! Form::label('visit_task_detail_id', 'Visit Task Detail Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('visit_task_detail_id', null, ['class' => 'form-control','id'=>"visit_task_detail_id","required"]) !!}
    </div>
 </div>

<!-- Product Id Field -->
     <div class="form-group row">
     {!! Form::label('product_id', 'Product Id:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('product_id', null, ['class' => 'form-control','id'=>"product_id","required"]) !!}
    </div>
 </div>

<!-- Price Field -->
     <div class="form-group row">
     {!! Form::label('price', 'Price:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('price', null, ['class' => 'form-control','id'=>"price","required"]) !!}
    </div>
 </div>

<!-- Stock Field -->
     <div class="form-group row">
     {!! Form::label('stock', 'Stock:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('stock', null, ['class' => 'form-control','id'=>"stock","required"]) !!}
    </div>
 </div>

<!-- Inventory Field -->
     <div class="form-group row">
     {!! Form::label('inventory', 'Inventory:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('inventory', null, ['class' => 'form-control','id'=>"inventory","required"]) !!}
    </div>
 </div>

<!-- Num Of Stock Field -->
     <div class="form-group row">
     {!! Form::label('num_of_stock', 'Num Of Stock:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('num_of_stock', null, ['class' => 'form-control','id'=>"num_of_stock","required"]) !!}
    </div>
 </div>

<!-- Num Of Inventory Field -->
     <div class="form-group row">
     {!! Form::label('num_of_inventory', 'Num Of Inventory:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('num_of_inventory', null, ['class' => 'form-control','id'=>"num_of_inventory","required"]) !!}
    </div>
 </div>

<!-- Note Field -->
     <div class="form-group row">
     {!! Form::label('note', 'Note:',['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
    {!! Form::text('note', null, ['class' => 'form-control','id'=>"note","required"]) !!}
    </div>
 </div>

</div>


 <div class="form-group row">
     <label class="col-sm-2 col-form-label"></label>
     <div class="col-sm-10">
         <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
         <a href="{!! route('visitTaskInventoryDetails.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

     </div>
 </div>
