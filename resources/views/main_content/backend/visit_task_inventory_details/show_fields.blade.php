<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $visitTaskInventoryDetail->id !!}</p>
</div>

<!-- Visit Task Detail Id Field -->
<div class="form-group">
    {!! Form::label('visit_task_detail_id', 'Visit Task Detail Id:') !!}
    <p>{!! $visitTaskInventoryDetail->visit_task_detail_id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $visitTaskInventoryDetail->product_id !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $visitTaskInventoryDetail->price !!}</p>
</div>

<!-- Stock Field -->
<div class="form-group">
    {!! Form::label('stock', 'Stock:') !!}
    <p>{!! $visitTaskInventoryDetail->stock !!}</p>
</div>

<!-- Inventory Field -->
<div class="form-group">
    {!! Form::label('inventory', 'Inventory:') !!}
    <p>{!! $visitTaskInventoryDetail->inventory !!}</p>
</div>

<!-- Num Of Stock Field -->
<div class="form-group">
    {!! Form::label('num_of_stock', 'Num Of Stock:') !!}
    <p>{!! $visitTaskInventoryDetail->num_of_stock !!}</p>
</div>

<!-- Num Of Inventory Field -->
<div class="form-group">
    {!! Form::label('num_of_inventory', 'Num Of Inventory:') !!}
    <p>{!! $visitTaskInventoryDetail->num_of_inventory !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $visitTaskInventoryDetail->note !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $visitTaskInventoryDetail->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $visitTaskInventoryDetail->updated_at !!}</p>
</div>

