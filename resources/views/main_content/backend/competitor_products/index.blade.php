@extends('layouts.backend')

@section('pageTitle', _lang('app.competitor_products'))

@section('breadcrumb')
<li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>
@if (isset($competitor))
<li><a href="{{route('competitors.index')}}">{{_lang('app.competitors')}}</a> <i class="fa fa-circle"></i></li>
<li><span>{{$competitor->title}}</span><i class="fa fa-circle"></i></li>
@endif
<li><span> {{_lang('app.competitor_products')}}</span></li>
@endsection

@section('js')
<script src="{{url('public/backend/js')}}/competitor_products.js" type="text/javascript"></script>
@endsection

@section('content')
@if (session()->has('error_message'))
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
    <strong>{{_lang('app.error')}}!</strong> {{session()->get('error_message')}}.
</div>
@endif
 {{ csrf_field() }}
<div class = "panel panel-default">
    <div class = "panel-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a class="btn green" style="margin-bottom: 40px;" href = "{{ $competitor_id ? route('competitor_products.create',['competitor' => $competitor_id]) : route('competitor_products.create') }}" onclick="">{{ _lang('app.add_new')}}<i class="fa fa-plus"></i> </a>
                    </div>
                </div>
            </div>
        </div>
        <table class= "table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer">
            <thead>
                <tr>
                    <th>{{_lang('app.name')}}</th>
                    <th>{{_lang('app.competitor')}}</th>
                    <th>{{_lang('app.this_order')}}</th>
                    <th>{{_lang('app.status')}}</th>
                    <th>{{_lang('app.options')}}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

        <!--Table Wrapper Finish-->
    </div>
</div>
<script>
var new_lang = {

};
var new_config = {
    competitor: "{{$competitor_id}}"
};
</script>
@endsection