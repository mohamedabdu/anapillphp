@extends('layouts.backend')

@section('pageTitle',_lang('app.edit_competitor_product'))

@section('breadcrumb')
<li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>
<li><a href="{{route('competitor_products.index')}}">{{_lang('app.competitor_product')}}</a> <i class="fa fa-circle"></i></li>
<li><span>{{_lang('app.edit')}}</span></li>
@endsection

@section('js')
<script src="{{url('public/backend/js')}}/competitor_products.js" type="text/javascript"></script>
@endsection

@section('content')
<form role="form"  id="addEditCompetitorProductsForm" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.name') }}</h3>
        </div>
        <div class="panel-body">
            <div class="form-body">
                <input type="hidden" name="id" id="id" value="{{ en_de_crypt($competitor_product->id) }}">
                @foreach ($languages as $key => $value)
                <div class="form-group form-md-line-input col-md-6">
                    <input type="text" class="form-control" id="title[{{ $key }}]" name="title[{{ $key }}]" value="{{ $translations["$key"]->title }}">
                    <label for="title">{{_lang('app.title') }} {{ _lang('app.'.$value.'') }}</label>
                    <span class="help-block"></span>
                </div>
                @endforeach
              
               
            </div>
        </div>
       

    </div>


     <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.competitor') }}</h3>
        </div>
        <div class="panel-body">
            <div class="form-body">
    
                <div class="form-group form-md-line-input col-md-4 select">
                    <select class="form-control edited" id="competitor" name="competitor">
                        <option value="">{{ _lang('app.choose') }}</option>
                        @foreach ($competitors as $competitor)
                            <option value="{{$competitor->id}}" {{ en_de_crypt($competitor_product->competitor_id) == $competitor->id ? 'selected' : '' }}>{{ $competitor->title }}</option>
                        @endforeach
                    </select>
                    <label for="competitor">{{_lang('app.competitor') }}</label>
                    <span class="help-block"></span>
                </div>
                
                <div class="clearfix"></div>
                
                <div class="form-group form-md-line-input col-md-3">
                    <input type="number" class="form-control" id="this_order" name="this_order" value="{{$competitor_product->this_order}}">
                    <label for="this_order">{{_lang('app.this_order') }}</label>
                    <span class="help-block"></span>
                </div>
    
                <div class="form-group form-md-line-input col-md-3">
                    <select class="form-control" name="active" id="active">
                        <option value="0" {{$competitor_product->active == 0 ? 'selected' : ''}}>{{_lang('app.deactivated')}}</option>
                        <option value="1" {{$competitor_product->active == 1 ? 'selected' : ''}}>{{_lang('app.activated')}}</option>
                    </select>
                    <label for="status">{{_lang('app.status')}}</label>
                    <span class="help-block"></span>
                </div>
    
            </div>
        </div>
    
        <div class="panel-footer text-center">
            <button type="button" class="btn btn-info submit-form">{{_lang('app.save') }}</button>
        </div>
    
    
    </div>



</form>
<script>
var new_lang = {

};
var new_config = {
   
};

</script>
@endsection