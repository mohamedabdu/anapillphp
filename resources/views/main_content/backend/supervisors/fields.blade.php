<div class="form-body">
    <!-- Title Ar Field -->
    <div class="form-group row">
        {!! Form::label('name', 'الأسم',['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('name', null, ['class' => 'form-control','id'=>"name","required"]) !!}
        </div>
    </div>
    <!-- Title Ar Field -->
    <div class="form-group row">
        {!! Form::label('email', 'البريد الألكتروني',['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('email', null, ['class' => 'form-control','id'=>"email","required"]) !!}
        </div>
    </div>
    <!-- Title Ar Field -->
    <div class="form-group row">
        {!! Form::label('mobile', 'رقم الجوال',['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('mobile', null, ['class' => 'form-control','id'=>"mobile","required"]) !!}
        </div>
    </div>
    <!-- Title Ar Field -->
    <div class="form-group row">
        {!! Form::label('password', 'كلمة السر',['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            <input placeholder="*****" type="password" class="form-control" name="password">
        </div>
    </div>
    <!-- Image Field -->


    <!-- Country Id Field -->
    <div class="form-group row">
        {!! Form::label('country_id', 'الدولة',['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            <select class="form-control" name="country_id" id="country_select" required>
                <option value="">اختار</option>
                @foreach(\App\Models\Country::all() as $country)
                    <option value="{{$country->id}}" @if(isset($supervisor) && $supervisor->country_id == $country->id) selected @endif>{{$country->title_ar}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <!-- Region Id Field -->
    <div class="form-group row">
        {!! Form::label('region_id', 'المنطقة',['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            <select class="form-control" id="region_select" name="region_id" required>
                <option value="">اختار</option>
                @foreach(\App\Models\Region::all() as $region)
                    <option value="{{$region->id}}" @if(isset($supervisor) && $supervisor->region_id == $region->id) selected @endif>{{$region->title_ar}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <!-- City Id Field -->
    <div class="form-group row">
        {!! Form::label('city_id', 'المدينة',['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            <select class="form-control" id="city_select" name="city_id" required>
                <option value="">اختار</option>
                @foreach(\App\Models\City::all() as $city)
                    <option value="{{$city->id}}" @if(isset($supervisor) && $supervisor->city_id == $city->id) selected @endif>{{$city->title_ar}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <!--  work hours Field -->
    <div class="form-group row">
        {!! Form::label('work_hours', 'عدد ساعات العمل',['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('work_hours', null, ['class' => 'form-control','id'=>"name","required"]) !!}
        </div>
    </div>

    <div class="form-group row">
        {!! Form::label('workdays', 'ايام العمل',['class' => 'col-sm-2 col-form-label']) !!}
        @foreach(\App\Models\Agent::$days as $day)
            {!! Form::label('day', $day,['class' => 'col-sm-1 col-form-label']) !!}
            <div class="col-sm-1">
                <div class="col-sm-1">
                    <div class="icheck-material-danger">

                        <input type="checkbox" id="{{$day}}" name="days[]" value="{{$day}}" @if(isset($days) && in_array($day,$days)) checked @endif />
                        <label for="{{$day}}">  </label>
                    </div>
                </div>

                {{--             <select class="form-control" name="day">--}}

                {{--                 @foreach(\App\Models\StoreWorkDay::$days as $day)--}}
                {{--                     <option value="{{$day}}">{{$day}}</option>--}}
                {{--                 @endforeach--}}
                {{--             </select>--}}
            </div>
    @endforeach

    </div>


    <div class="form-group row">
        {!! Form::label('body_en', 'الموظفيين',['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            <select name="agents[]" data-placeholder="اختار الموظفين" class="chosen-select form-control" id="agents_select" multiple>
                <option value="" disabled>اختار</option>
                @foreach($agents as $item)
                    <option value="{{$item->id}}" @if(isset($agentsSelected) && in_array($item->id, $agentsSelected)) selected @endif>{{$item->name}} </option>
                @endforeach
            </select>
        </div>
    </div>



    <!-- Image Field -->

    <div class="input-group mb-3">
        <div class="custom-file">
            <input type="file" name="image" class="custom-file-input" id="inputGroupFile02">
            <label class="custom-file-label" for="inputGroupFile02">الصورة</label>
        </div>

    </div>

</div>

@if(isset($supervisor))
    <div class="row">
        <div class="col-md-4"> <img src="{{\Helper\Common\imageUrl($supervisor->image)}}" class="img-responsive col-md-12"> </div>
    </div>
    <br><br>
@endif

<div class="form-group row">
    <label class="col-sm-2 col-form-label"></label>
    <div class="col-sm-10">
        <button type="submit" class="btn btn-white px-5"><i class="icon-settings"></i> حفظ</button>
        <a href="{!! route('supervisors.index') !!}" class="btn btn-light waves-effect waves-light m-1">الغاء</a>

    </div>
</div>

<script>
    $("#agents_select").chosen();


    $( document ).ready(function() {

        $('#country_select').on('change', function (e) {

            var country = e.target.value;
            if (country > 0) {
                $.get(appUrl + '/countries/ajax/' + country, function (data) {

                    $('#region_select').empty();
                    $('#region_select').append($('<option>', {
                        value: '',
                        text: "اختار",
                        selected: true
                    }));
                    if (data.length !== 0) {
                        $.each(data, function (index, subCatObj) {

                            $('#region_select').append($('<option>', {
                                value: subCatObj.id,
                                text: subCatObj.title_ar
                            }));

                        });

                    } else {
                        $('#region_select').empty();

                        $('#region_select').append($('<option>', {
                            value: '',
                            text: "اختار",
                            selected: true
                        }));
                    }

                });
            }
            else {

                $('#region_select').empty();

                $('#region_select').append($('<option>', {
                    value: '',
                    text: "اختار",
                    selected: true
                }));
            }

        });
        $('#region_select').on('change', function (e) {

            var region = e.target.value;
            if (region > 0) {
                $.get(appUrl + '/regions/ajax/' + region, function (data) {

                    $('#city_select').empty();

                    if (data.length !== 0) {
                        $.each(data, function (index, subCatObj) {

                            $('#city_select').append($('<option>', {
                                value: subCatObj.id,
                                text: subCatObj.title_ar
                            }));

                        });

                    } else {
                        $('#city_select').empty();

                        $('#city_select').append($('<option>', {
                            value: '',
                            text: "اختار",
                            selected: true
                        }));
                    }

                });
            }
            else {

                $('#city_select').empty();

                $('#city_select').append($('<option>', {
                    value: '',
                    text: "اختار",
                    selected: true
                }));
            }

        });

    });


</script>