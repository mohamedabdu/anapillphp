

@extends('layouts.backend')

@section('pageTitle',"اضافة مشرف")

@section('breadcrumb')
    <li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

    <li><a href="{{route('supervisors.index')}}">المشرفيين</a> <i class="fa fa-circle"></i></li>

    <li><span> اضافة</span></li>

    <li><span> مشرف</span></li>

@endsection

@section('js')
    <script src="{{url('public/backend/js')}}/supervisors.js" type="text/javascript"></script>
@endsection
@section('content')
    <form role="form"  id="addForm" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">البيانات الأساسية</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">
                    <input type="hidden" name="id" id="id" value="0">


                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="name" name="name" value="">
                        <label for="name">الأسم</label>
                        <span class="help-block"></span>

                    </div>

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="mobile" name="mobile" value="">
                        <label for="mobile">رقم الجوال</label>
                        <span class="help-block"></span>

                    </div>

                </div>
            </div>

        </div>


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">البيانات الدخول</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" class="form-control" id="email" name="email" value="">
                        <label for="email">البريد الألكتروني</label>
                        <span class="help-block"></span>

                    </div>

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="password" class="form-control" id="password" name="password" value="">
                        <label for="password">كلمة السر</label>
                        <span class="help-block"></span>

                    </div>




                </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">مدينة الموظف</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control" name="country_id" id="country_select">
                            <option value="">Select</option>
                            @foreach(\App\Models\Country::withTrashed()->get() as $country)
                                <option value="{{$country->id}}" @if(isset($store) && $store->country_id == $country->id) selected @endif>{{$country->title_ar}}</option>
                            @endforeach
                        </select>
                        <label for="title">الدولة</label>
                        <span class="help-block"></span>

                    </div>
                    <!-- title en Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control" id="region_select" name="region_id" >
                            <option value="">Select</option>
                            @foreach(\App\Models\Region::withTrashed()->get() as $region)
                                <option value="{{$region->id}}" @if(isset($store) && $store->region_id == $region->id) selected @endif>{{$region->title_ar}}</option>
                            @endforeach
                        </select>
                        <label for="title_en">المنطقة</label>
                        <span class="help-block"></span>
                    </div>

                    <!-- Role Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <select class="form-control" id="city_select" name="city_id" >
                            <option value="">Select</option>
                            @foreach(\App\Models\City::withTrashed()->get() as $city)
                                <option value="{{$city->id}}" @if(isset($store) && $store->city_id == $city->id) selected @endif>{{$city->title_ar}}</option>
                            @endforeach
                        </select>
                        <label for="title">المدينة</label>
                        <span class="help-block"></span>

                    </div>


                </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">اوقات العمل</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">


                    <!--  work hours Field -->
                    <div class="form-group form-md-line-input col-md-12">
                        <input type="text" name="work_hours" class="form-control" value="">
                        {!! Form::label('work_hours', 'عدد ساعات العمل',['class' => 'col-sm-2 col-form-label']) !!}
                        <span class="help-block"></span>

                    </div>


                    <div class="form-group form-md-line-input col-md-12">
                        @foreach(\App\Models\Agent::$days as $day)
                            <div class="form-group col-md-2" style="padding-top:30px; margin-left:5%">
                                <label class="checkbox-inline">
                                    <input type="checkbox" data-toggle="toggle" data-style="ios" id="days" name="days[]" value="{{$day}}"
                                            {{isset($days) && in_array($day,$days) ? "checked" : "" }}> {{$day}}
                                </label>
                            </div>

                        @endforeach

                        <span class="help-block"></span>

                    </div>


                </div>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">الموظفيين</h3>
            </div>
            <div class="panel-body">


                <div class="form-body">


                    <div class="form-group form-md-line-input col-md-12">
                            <select name="agents[]" data-placeholder="اختار الموظفين" class="chosen-select form-control" id="agents_select" multiple>
                                <option value="" disabled>اختار</option>
                                @foreach($agents as $item)
                                    <option value="{{$item->id}}" @if(isset($agentsSelected) && in_array($item->id, $agentsSelected)) selected @endif>{{$item->name}} </option>
                                @endforeach
                            </select>
                            {!! Form::label('body_en', 'الموظفيين',['class' => 'col-sm-2 col-form-label']) !!}
                        <span class="help-block"></span>

                    </div>



                </div>
            </div>

        </div>


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">الصورة</h3>
            </div>
            <div class="panel-body">

                <div class="form-body">

                    <div class="form-group form-md-line-input col-md-2">
                        <div class="image_0_box">
                            <img src="{{url('no-image.png')}}" width="100" height="80" class="image_0" />
                        </div>
                        <input type="file" name="image" id="image_0" style="visibility: hidden">
                        <span class="help-block"></span>
                    </div>


                </div>
            </div>
            <div class="panel-footer text-center">
                <button type="button" class="btn btn-info submit-form"
                >{{_lang('app.save') }}</button>
            </div>

        </div>


    </form>
    <script>
        var new_lang = {

        };
        var new_config = {

        };

    </script>
@endsection

