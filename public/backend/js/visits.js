var VisitsGrid;
var visitType = new_config.visit_type;
var Visits = function() {

    var init = function() {
        handleRecords();
        handleSubmit();
        createVisit();
        showVisitSchedule();
        showVisitNormal();
        handleFilter();
        // handleTagsInput();
        // handleChangeCategory();
        // handleChangeSubCategory();
        $('.datePicker').datetimepicker({
            format: 'YYYY-MM-DD'
        });

        for (let i = 0; i < 6; i++) {
             if (i == 0) {
                 My.readImageMulti('image_' + i + '');
             } else {
                 My.readImageMultiWithRemove('image_' + i + '');
             }
         }
    };

    var handleFilter = function() {

        // if ($('#id').val() != 0) {
        //     $('#image_0').rules('remove');
        // }


        $('#filterForm .submit-form-filter').click(function() {

            $('#filterForm .submit-form-filter').prop('disabled', true);
            $('#filterForm .submit-form-filter').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
            setTimeout(function() {
                $('#filterForm').submit();
            }, 1000);
        });




        $('#filterForm').submit(function() {
            filter = $("#filterMethod").val();
            filter_date = $("#filter_date").val();
            VisitsGrid.api().ajax.url("tasks/data?filter="+filter+"&filter_date="+filter_date).load();
            $('#filterForm .submit-form-filter').prop('disabled', false);
            $('#filterForm .submit-form-filter').html(lang.filter);

            return false;

        })




    }

    var handleRecords = function() {
        if (visitType == 1) {
            VisitsGrid = $('.dataTable').dataTable({
                //"processing": true,
                "serverSide": true,
                "ajax": {
                    "url": config.admin_url + "/visits/data/table?type="+visitType,
                    "type": "POST",
                    data: { _token: $('input[name="_token"]').val() },
                },
                "columns": [
                    {"data": "code"},
                    {"data": "start_date"},
                    {"data": "num_of_stores", orderable: false, searchable: false},
                    {"data": "show", orderable: false, searchable: false},
                    {"data": "created_user",name: "users.name"},

                    {"data": "options", orderable: false, searchable: false}
                ],
                "order": [
                    [1, "desc"]
                ],

                "oLanguage": { "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json' }

            });
        } else if (visitType == 2) {
            VisitsGrid = $('.dataTable').dataTable({
                //"processing": true,
                "serverSide": true,
                "ajax": {
                    "url": config.admin_url + "/visits/data/table?type="+visitType,
                    "type": "POST",
                    data: { _token: $('input[name="_token"]').val() },
                },
                "columns": [
                    {"data": "code"},
                    {"data": "start_date"},
                    {"data": "num_of_visits"},
                    {"data": "repeat_every", orderable: false, searchable: false},
                    {"data": "num_of_stores", orderable: false, searchable: false},
                    {"data": "show", orderable: false, searchable: false},
                    {"data": "created_user",name: "users.name"},

                    {"data": "options", orderable: false, searchable: false}
                ],
                "order": [
                    [1, "desc"]
                ],

                "oLanguage": { "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json' }

            });
        } else {
            VisitsGrid = $('.dataTable').dataTable({
                //"processing": true,
                "serverSide": true,
                "ajax": {
                    "url": config.admin_url + "/visits/today/tasks/data",
                    "type": "POST",
                    data: { _token: $('input[name="_token"]').val() },
                },
                "columns": [
                    {"data": "code"},
                    {"data": "start_date"},
                    {"data": "created_user",name: "users.name"},

                    {"data": "options", orderable: false, searchable: false}
                ],
                "order": [
                    [1, "desc"]
                ],

                "oLanguage": { "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json' }

            });
        }

    }

    var createVisit = function () {
        $("#stores_select").chosen();
        $("#agents_select").chosen();
        $("#tasks_select").chosen();

        var onGoing = $("#on_going").val();
        checkOnGoing(onGoing);
        $('#on_going').on('change', function (e) {
            onGoing = e.target.value;
            checkOnGoing(onGoing);
        });
    }

    var checkOnGoing = function (onGoing) {
        if(onGoing == 0) {
            $("#num_of_visit").fadeIn(1000);
        } else {
            $("#num_of_visit").fadeOut(1000);
        }
    }

    var showVisitSchedule = function () {

        $('.submit-form-schu').click(function() {

            $('.submit-form-schu').prop('disabled', true);
            $('.submit-form-schu').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
            setTimeout(function() {

            }, 1000);


            var agent = $("#agents_select_show").val();
            if(agent === "") {
                return;
            }
            array = [];
            $('#agents_select_show option:selected').each(function() {
                array.push($(this).val());
            });

            visitID = new_config.visitID;
            console.log(array, visitID);
            $.get(config.admin_url + '/visits/'+visitID+'/edit/ajax',{ agents: array }, function (data, status) {
                if (data.length !== 0 && data.success === true) {
                    console.log(data.success);
                    if(data.success == true) {
                        My.toast(data.message);
                        $("#agentsDiv").html("");
                        $("#agents_select_show").empty();
                        $('#agents_select_show').append($('<option>', {
                            value: '',
                            text: "اختار",
                            selected: true
                        }));
                        $.each(data.agentsSelected, function (index, agent) {
                            $('#agentsDiv').append("<p>" + agent + "</p>");

                        });
                        $.each(data.agents, function (index, agent) {

                            $('#agents_select_show').append($('<option>', {
                                value: agent.id,
                                text: agent.name
                            }));

                        });

                    } else {
                        My.toast(data.message);
                    }
                    //$('.modal-backdrop').remove();
                }
                $('.submit-form-schu').prop('disabled', false);
                $('.submit-form-schu').html(lang.save);

            });

        });
    }

    var showVisitNormal = function () {

        $('.submit-form').click(function() {

            $('.submit-form').prop('disabled', true);
            $('.submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
            setTimeout(function() {
            }, 1000);


            var agent = $("#agents_select").val();
            if(agent === "") {
                return;
            }
            array = [];
            $('#agents_select option:selected').each(function() {
                array.push($(this).val());
            });

            visitID = new_config.visitID;
            console.log(array);
            $.get(config.admin_url + '/visits/'+visitID+'/edit/ajax',{ agents: array }, function (data, status) {
                if (data.length !== 0 && data.success === true) {
                    console.log(data.success);
                    if(data.success == true) {
                        My.toast(data.message);
                        $("#agentsDiv").html("");
                        //$("#agents_select").html("");
                        //$('#agents_select').val('').trigger('chosen:updated');
                        $('#agents_select')
                            .find('option:first-child').prop('selected', false)
                            .end().trigger('chosen:updated');

                        $.each(data.agentsSelected, function (index, agent) {
                            $('#agentsDiv').append("<p>" + agent + "</p>");

                        });
                        // $.each(data.agents, function (index, agent) {
                        //
                        //     $('#agents_select').append($('<option>', {
                        //         value: agent.id,
                        //         text: agent.name
                        //     }));
                        //
                        // });

                    } else {
                        My.toast(data.message);
                    }
                    //$('.modal-backdrop').remove();
                }
                $('.submit-form').prop('disabled', false);
                $('.submit-form').html(lang.save);

            });

        });


    }



    var handleSubmit = function() {


        $('#addForm').validate({
            rules: {


            },
            //messages: lang.messages,
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function(error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        // if ($('#id').val() != 0) {
        //     $('#image_0').rules('remove');
        // }


        $('#addForm .submit-form').click(function() {

            if ($('#addForm').validate().form()) {
                $('#addForm .submit-form').prop('disabled', true);
                $('#addForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function() {
                    $('#addForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#addForm input').keypress(function(e) {
            if (e.which == 13) {
                if ($('#addForm').validate().form()) {
                    $('#addForm .submit-form').prop('disabled', true);
                    $('#addForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function() {
                        $('#addForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#addForm').submit(function() {
            var id = $('#id').val();
            var action = config.admin_url + '/visits';
            var formData = new FormData($(this)[0]);
            if (id != 0) {
                formData.append('_method', 'PATCH');
                action = config.admin_url + '/visits/' + id;
            }
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    $('#addForm .submit-form').prop('disabled', false);
                    $('#addForm .submit-form').html(lang.save);

                    if (data.type == 'success') {

                        if (id == 0) {
                            My.toast(data.message);
                            Visits.empty();
                        }
                       else {
                            My.toast(data.data.message);
                            // images = JSON.parse(data.data.article_images);
                            // for (i in images) {
                            //     if (i == 0) {
                            //         continue;
                            //     }
                            //    var image = images[i];
                            //    $('input[name="images['+i+']"]').closest('.form-group').find("a").attr({
                            //        "data-image": image,
                            //        "data-model": "Article",
                            //        "data-folder": "articles",
                            //        "data-col": "images",
                            //     });
                            // }
                        }

                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    $('#addForm .submit-form').prop('disabled', false);
                    $('#addForm .submit-form').html(lang.save);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });


            return false;

        })




    }

    return {
        init: function() {
            init();
        },
        delete: function(t) {

            var id = $(t).attr("data-id");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/visits/' + id,
                data: { _method: 'DELETE', _token: $('input[name="_token"]').val() },
                success: function(data) {
                    console.log(data);
                    VisitsGrid.api().ajax.reload();
                }
            });

        },
        changeActive: function(e) {
        var record_id = $(e).data("id");
        var url = config.admin_url + '/promos/active/' + record_id;
        $.get(url, function (data) {
            if (data.length !== 0)
            {
                if(data.success)
                {
                    My.toast(data.message);
                    PromosGrid.api().ajax.reload();

                }
                else
                {
                    My.toast(data.message);
                }


            }

        });


    },
        error_message: function(message) {
            $.alert({
                title: lang.error,
                content: message,
                type: 'red',
                typeAnimated: true,
                buttons: {
                    tryAgain: {
                        text: lang.try_again,
                        btnClass: 'btn-red',
                        action: function() {}
                    }
                }
            });
        },
        empty: function() {
            $('#id').val(0);
            $('#active').find('option').eq(0).prop('selected', true);
            $('#stores_select').find('option').prop("selected",false);
            $('#agents_select').find('option').prop("selected",false);
            $('#tasks_select').find('option').prop("selected",false);
            $('#stores_select').trigger("chosen:updated");
            $('#agents_select').trigger("chosen:updated");
            $('#tasks_select').trigger("chosen:updated");

            $('#type').find('option').eq(0).prop('selected', true);
            $('input[type="checkbox"]').prop('checked', false);
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
           for (let i = 0; i < 6; i++) {
               $('.image_' + i + '_box').html('<img src="' + config.url + '/no-image.png" width="100" height="80" class="image_' + i + '" />');
               $('#image_' + i).val('');
            }
            $("#videos").tagsinput('removeAll');
            $('#categories').find('option').eq(0).prop('selected', true);
            $('#sub_categories').html('<option value="">' + lang.choose + '</option>');
            $('#specialists').html('<option value="">' + lang.choose + '</option>');
            $('#show_rates').prop('checked', false).change();
            My.emptyForm();
        }
    };

}();
jQuery(document).ready(function() {
    Visits.init();
});