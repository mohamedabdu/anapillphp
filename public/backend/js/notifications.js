var NotificationGrid;

var Notification = function() {

    var init = function() {
        //handleRecords();
        handleSubmit();
        handleForm();
        $("#supervisors_select").chosen();
        $("#agents_select").chosen();
        // handleTagsInput();
        // handleChangeCategory();
        // handleChangeSubCategory();

        for (let i = 0; i < 6; i++) {
             if (i == 0) {
                 My.readImageMulti('image_' + i + '');
             } else {
                 My.readImageMultiWithRemove('image_' + i + '');
             }
         }
    };


   
    var handleForm = function () {
        // $("#agents_select").chosen();
        // $("#supervisors_select").chosen();

        var check_type = $('#type_select').val();
        if (check_type == 0) {
            $("#agents").fadeIn(1);
            $("#supervisors").fadeOut(1);
        }
        else {
            $("#agents").fadeOut(1);
            $("#supervisors").fadeIn(1);
        }

        $('#type_select').on('change', function (e) {

            var type = e.target.value;
            if (type == 0) {
                $("#agents").fadeIn(1000);
                $("#supervisors").fadeOut(1000);
            }
            else {
                $("#agents").fadeOut(1000);
                $("#supervisors").fadeIn(1000);
            }

        });

    }
    var handleSubmit = function() {


        $('#addForm').validate({
            rules: {

                body: {
                    required: true,
                },


            },
            //messages: lang.messages,
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function(error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        // if ($('#id').val() != 0) {
        //     $('#image_0').rules('remove');
        // }


        $('#addForm .submit-form').click(function() {

            if ($('#addForm').validate().form()) {
                $('#addForm .submit-form').prop('disabled', true);
                $('#addForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function() {
                    $('#addForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#addForm input').keypress(function(e) {
            if (e.which == 13) {
                if ($('#addForm').validate().form()) {
                    $('#addForm .submit-form').prop('disabled', true);
                    $('#addForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function() {
                        $('#addEditArticlesForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#addForm').submit(function() {
            var id = $('#id').val();
            var action = config.admin_url + '/notifications';
            var formData = new FormData($(this)[0]);
            if (id != 0) {
                formData.append('_method', 'PATCH');
                action = config.admin_url + '/notifications/' + id;
            }
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    $('#addForm .submit-form').prop('disabled', false);
                    $('#addForm .submit-form').html(lang.save);

                    if (data.type == 'success') {

                        if (id == 0) {
                            My.toast(data.message);
                            Notification.empty();
                        }
                       else {
                            My.toast(data.data.message);
                            // images = JSON.parse(data.data.article_images);
                            // for (i in images) {
                            //     if (i == 0) {
                            //         continue;
                            //     }
                            //    var image = images[i];
                            //    $('input[name="images['+i+']"]').closest('.form-group').find("a").attr({
                            //        "data-image": image,
                            //        "data-model": "Article",
                            //        "data-folder": "articles",
                            //        "data-col": "images",
                            //     });
                            // }
                        }

                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    $('#addForm .submit-form').prop('disabled', false);
                    $('#addForm .submit-form').html(lang.save);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });


            return false;

        })




    }

    return {
        init: function() {
            init();
        },
        delete: function(t) {

            var id = $(t).attr("data-id");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/roles/' + id,
                data: { _method: 'DELETE', _token: $('input[name="_token"]').val() },
                success: function(data) {
                    console.log(data);
                    RolesGrid.api().ajax.reload();
                }
            });

        },

        error_message: function(message) {
            $.alert({
                title: lang.error,
                content: message,
                type: 'red',
                typeAnimated: true,
                buttons: {
                    tryAgain: {
                        text: lang.try_again,
                        btnClass: 'btn-red',
                        action: function() {}
                    }
                }
            });
        },
        empty: function() {
            $('#id').val(0);
            $('#supervisors_select').find('option').eq(0).prop('selected', true);
            $('#agents_select').find('option').eq(0).prop('selected', true);
            $('#type').find('option').eq(0).prop('selected', true);
            $('input[type="checkbox"]').prop('checked', false);
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
           for (let i = 0; i < 6; i++) {
               $('.image_' + i + '_box').html('<img src="' + config.url + '/no-image.png" width="100" height="80" class="image_' + i + '" />');
               $('#image_' + i).val('');
            }
            $("#videos").tagsinput('removeAll');
            $('#categories').find('option').eq(0).prop('selected', true);
            $('#sub_categories').html('<option value="">' + lang.choose + '</option>');
            $('#specialists').html('<option value="">' + lang.choose + '</option>');
            $('#show_rates').prop('checked', false).change();
            My.emptyForm();
        }
    };

}();
jQuery(document).ready(function() {
    Notification.init();
});