var Products = function () {
    var Products_grid, competitors = [], counter = 0, dir;

    var init = function () {
        handleRecords();
        handleSubmit();
        handleOptionsParse();
        handleCountryChange();
        handleCompetitorChange();
        handleAddCompetitor();
        handleRemoveCompetitor();
        if (config.lang_code == 'ar') {
            dir  = 'rtl';
        }else{
            dir = 'ltr';
        }
        $('.competitor-products').select2({
            dir: dir
        });
        My.readImageMulti('image');
        if (new_config.count) {
            counter = new_config.count;
        }
        
    };

    var handleOptionsParse = function(){
        competitors = JSON.parse(new_config.competitors);
    }

    var handleRemoveCompetitor = function () {
        $(document).on('click', '.remove-competitor', function (e) {
            $button = $(this);
            $selectedCompetitor = $(this).closest('.row').find('.competitor');
            var product_competitor_id = $(this).attr('data-id');
            if ($('#id').val() != 0 && typeof product_competitor_id !== typeof undefined && product_competitor_id !== false) {
                
               
                if (typeof product_competitor_id !== typeof undefined && product_competitor_id !== false) {
                     $button.prop('disabled', true);
                     $button.html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    $.ajax({
                        url: config.admin_url + '/remove_product_competitor/' + product_competitor_id,
                        data:{
                             _token: $('input[name="_token"]').val()
                        },
                        success: function (data) {
                            $button.prop('disabled', false);
                            $button.html('<i class="fa fa-trash"></i>' + lang.remove);

                            var id = $selectedCompetitor.val()
                            var text = $button.closest('.row').find('.competitor option:selected').text();
                            competitors.push({
                                id: id,
                                title: text
                            });
                            $button.closest('.single-competitor').remove();
                            My.toast(data.message);
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            $button.prop('disabled', false);
                            $button.html('<i class="fa fa-trash"></i>' + lang.remove);
                            My.ajax_error_message(xhr);
                        },
                        dataType: "json",
                        type: "POST"
                    });
                }
                 
            }else{
                 if ($(this).closest('.row').find('.competitor').val()) {
                     var id = $(this).closest('.row').find('.competitor').val()
                     var text = $(this).closest('.row').find('.competitor option:selected').text();
                     competitors.push({
                         id: id,
                         title: text
                     });
                 }
                 $(this).closest('.single-competitor').remove();
            }
           
            return false;
        }); 
    }
    
    var handleAddCompetitor = function(){
        $('#add-competitor').on('click', function (e) {
            var record =  '<div class="portlet light bordered single-competitor">'+
                            '<div class="portlet-body">'+
                                '<div class="row">'+
                                    '<div class="col-md-12">'+
                                        '<div class="col-xs-12 mb-3 text-right">'+
                                            '<button class="btn btn-danger remove-competitor" type="button"><i class="fa fa-trash"></i>' + lang.remove + '</button>' +
                                        '</div>'+

                                        '<div class="form-group form-md-line-input col-md-6">'+
                                           '<select class="form-control edited competitor" name="competitors['+counter+'][competitor]">'+
                                                '<option value="">'+lang.choose+'</option>';
                                                competitors.forEach(competitor => {
                                                    record += '<option value="' + competitor.id + '">' + competitor.title + '</option>';
                                                });     
                                 record += '</select>' +
                                            '<label for="competitor">'+lang.competitor+'</label>'+
                                            '<span class="help-block"></span>'+
                                        '</div>'+

                                        '<div class="form-group col-md-12">'+
                                            '<label for="competitor_products">' + lang.competitor_products + '</label>' +
                                            '<select class="form-control edited competitor-products" multiple  name="competitors['+counter+'][competitor_products][]">'+
                                            '</select>'+
                                            '<span class="help-block"></span>'+
                                        '</div>'+

                                    '</div>'+
                                '</div>'+


                            '</div>'+
                          '</div>';

            $('#competitors-container').append(record);


            var competitorRule = "select[name='competitors[" + counter + "][competitor]']";
            $(competitorRule).rules('add', {
                required: true
            });
            
            var competitorProductsRule = "select[name='competitors[" + counter + "][competitor_products][]']";
            $(competitorProductsRule).rules('add', {
                required: true
            });
            $('.competitor-products').select2({
                dir: dir
            });
            counter++;
            return false;
        });
        
    }
  
    var handleCountryChange = function () {
        $('#main_category').on('change', function () {
            var category = $(this).val();
            $('#category').html('<option selected value="">' + lang.choose + '</option>');
            sub_categories = "";
            if (category) {
                $.ajax({
                    url: config.admin_url + '/get_categories/' + category,
                    success: function (data) {
                       if (data.data.length != 0) {
                           for (var x = 0; x < data.data.length; x++) {
                               var item = data.data[x];
                               sub_categories += '<option value="' + item.id + '">' + item.title + '</option>';
                           }
                           $('#category').append(sub_categories);
                       }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        My.ajax_error_message(xhr);
                    },
                    dataType: "json",
                    type: "GET"
                });
            }
        })
    }

    var handleCompetitorChange = function () {
        $(document).on('change', '.competitor', function () {
            var competitor = $(this).val();
            var $competitorProducts = $(this).closest('.row').find('.competitor-products');
            $competitorProducts.empty();
            if (competitor) {
                $.ajax({
                    url: config.admin_url + '/get_competitor_products/' + competitor,
                    success: function (data) {
                       if (data.data.length != 0) {
                           for (var x = 0; x < data.data.length; x++) {
                               var item = data.data[x];
                               var newOption = new Option(item.title, item.id, false, false);
                               $competitorProducts.append(newOption).trigger('change');
                           }
                           
                       }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        My.ajax_error_message(xhr);
                    },
                    dataType: "json",
                    type: "GET"
                });
                competitors = $.grep(competitors, function (item) {
                    return item.id != competitor;
                });
            }
        })
    }

    var handleRecords = function () {
        Products_grid = $('.dataTable').dataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": config.admin_url + "/products/data",
                "type": "POST",
                data: {
                    _token: $('input[name="_token"]').val()
                },
            },
            "columns": [
                {
                    "data": "title",
                    "name": "product_translations.title"
                },
                {
                    "data": "main_category",
                    "name": "main_category_trans.title"
                },
                {
                    "data": "category",
                    "name": "sub_category_trans.title"
                },
                {
                    "data": "sku",
                    "name": "products.sku"
                },
                {
                    "data": "price",
                    "name": "products.price"
                },
                {
                    "data": "active",
                    "name": "products.active",
                    orderable: false,
                    searchable: false
                },
                {
                    "data": "this_order",
                    "name": "products.this_order"
                },
                {
                    "data": "options",
                    orderable: false,
                    searchable: false
                }
            ],
            "columnDefs": [ {
                "width": "20%",
                "targets": 0
            }],
            "order": [
                [6, "asc"]
            ],
            "oLanguage": {
                "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json'
            }

        });
    }


    var handleSubmit = function () {
        $('#addEditProductsForm').validate({
            rules: {
                active:{
                    required:true
                },
                this_order: {
                     required: true
                },
                main_category: {
                    required: true
                },
                category: {
                    required: true
                },
                price: {
                    required: true
                },
                sku: {
                    required: true
                },
                image: {
                    required: true,
                    extension: "png|jpeg|jpg|gif"
                },
            },
            //messages: lang.messages,
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });


        var langs = JSON.parse(config.languages);
        for (var x = 0; x < langs.length; x++) {
            var ele = "input[name='title[" + langs[x] + "]']";
            $(ele).rules('add', {
                required: true
            });
        }

        if ($('#id').val() != 0) {
            $('#image').rules('remove');
             for (var x = 0; x < new_config.count; x++) {
                var competitorRule = "select[name='competitors[" + x + "][competitor]']";
                $(competitorRule).rules('add', {
                    required: true
                });

                var competitorProductsRule = "select[name='competitors[" + x + "][competitor_products][]']";
                $(competitorProductsRule).rules('add', {
                    required: true
                });
             }
             
        }

        $('#addEditProductsForm .submit-form').click(function () {

            if ($('#addEditProductsForm').validate().form()) {
                $('#addEditProductsForm .submit-form').prop('disabled', true);
                $('#addEditProductsForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#addEditProductsForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#addEditProductsForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#addEditProductsForm').validate().form()) {
                    $('#addEditProductsForm .submit-form').prop('disabled', true);
                    $('#addEditProductsForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#addEditProductsForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#addEditProductsForm').submit(function () {
            var id = $('#id').val();
            var action = config.admin_url + '/products';
            var formData = new FormData($(this)[0]);
            if (id != 0) {
                formData.append('_method', 'PATCH');
                action = config.admin_url + '/products/' + id;
            }
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#addEditProductsForm .submit-form').prop('disabled', false);
                    $('#addEditProductsForm .submit-form').html(lang.save);

                    if (data.type == 'success') {
                        My.toast(data.message);
                        if (id == 0) {
                            Products.empty();
                        }

                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                if (i.startsWith('title')) {
                                    var key_arr = i.split('.');
                                    var key_text = key_arr[0] + '[' + key_arr[1] + ']';
                                    i = key_text;
                                }
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#addEditProductsForm .submit-form').prop('disabled', false);
                    $('#addEditProductsForm .submit-form').html(lang.save);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });


            return false;

        })
    }

    return {
        init: function () {
            init();
        },
        delete: function (t) {

            var id = $(t).attr("data-id");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/products/' + id,
                data: {
                    _method: 'DELETE',
                    _token: $('input[name="_token"]').val()
                },
                success: function (data) {
                    Products_grid.api().ajax.reload();
                }
            });
        },
        changeActive: function (t) {
            var user_id = $(t).data("id");
            $(t).prop('disabled', true);
            $(t).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');

            $.ajax({
                url: config.admin_url + '/products/status/' + user_id,
                data: {
                    _token: $('input[name="_token"]').val()
                },
                success: function (data) {
                    $(t).prop('disabled', false);
                    if ($(t).hasClass("btn-success")) {
                        $(t).addClass('btn-danger').removeClass('btn-success');
                        $(t).html(lang.deactivated);
                    } else {
                        $(t).addClass('btn-success').removeClass('btn-danger');
                        $(t).html(lang.activated);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
        },
        empty: function () {
            $('#active').find('option').eq(0).prop('selected', true);
            $('#main_category').find('option').eq(0).prop('selected', true);
            $('#category').html('<option value="">' + lang.choose + '</option>');
            $('#competitors-container').html("");
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            $('.image_box').html('<img src="' + config.url + '/no-image.png" width="100" height="80" class="image" />');
            $('#image').val('');
            competitors = new_config.competitors;
            My.emptyForm();
        }
    };

}();

jQuery(document).ready(function () {
    Products.init();
});