var ManualReports = function () {
    var Page = 2;
    var init = function () {
        handleButtonsClick();
        handleDate();
        handleMultiSelect();
    }
     var handleDate = function () {
         $('.picker').datepicker({
             autoclose: true,
             format: 'yyyy-mm-dd',
             rtl: true
         });
     }

    var handleMultiSelect = function () {
         $(".multi-select-list").multiselect({
             includeSelectAllOption: false,
             enableCaseInsensitiveFiltering: true,
             numberDisplayed: 5,
             nSelectedText: ' - اختيارات',
             allSelectedText: 'تم إختيار كل العناصر',
             filterPlaceholder: 'بحث',
             buttonWidth: '100%',
             nonSelectedText: 'اختر',

         });
    }

    var handleButtonsClick = function () {

        $('#load-more-button').on('click', function () {
            actionSource = '#load-more-button';
            actionType = 'loadmore';
            $(actionSource).prop('disabled', true);
            $(actionSource).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
            setTimeout(function () {
                handleButtonActions();
            }, 1000);

        });

        $('#filterForm .submit-form').on('click', function () {
            actionSource = '#filterForm .submit-form';
            actionType = 'submit';
            $(actionSource).prop('disabled', true);
            $(actionSource).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
            setTimeout(function () {
                Page = 1;
                handleButtonActions();
            }, 1000);
        });

    }

    var handleButtonActions = function () {
        var action = new_config.url;
        var data = $("#filterForm").serializeArray();
        var params = {
            page: Page,
            employee: new_config.employee
        };
       
        $.each(data, function (i, field) {
            var name = field.name;
            var value = field.value;
            
            if (value) {
                if (name == 'store') {
                    params[name] = $('#' + name).val();
                }else{
                    params[name] = value;
                }
            }
        });
        
        var query = $.param(params);
        action += '?' + query;
        $.ajax({
            url: action,
           
            success: function (data) {
                $(actionSource).prop('disabled', false);
                if (actionType == 'loadmore') {
                    $(actionSource).html('المزيد');
                } else {
                    $(actionSource).html('بحث');
                }

                if (data.type == 'success') {
                    var reports = '';
                    if (data.data.reports.length > 0) {
                        data.data.reports.forEach(report => {
                                reports += '<div class="portlet light bordered">' +
                                                 '<div class="portlet-title">'+
                                                    '<div class="caption">'+report.created_at+'</div>'+
                                                '</div>'+
                                                '<div class="portlet-body">'+
                                                    '<div class="single-report">'+
                                                            '<div class="row mb-5">'+
                                                                '<div class="col-md-6 mb-3">' +
                                                                    '<div>'+
                                                                    '<span class="bold" style="font-size: 18px">اسم الموظف : </span>'+
                                                                        '<span style="font-size: 16px">'+new_config.employee_name+'</span>'+
                                                                    '</div>'+
                                                                '</div>'+
                                                                '<div class="col-md-6 mb-3">' +
                                                                    '<div>'+
                                                                    '<span class="bold" style="font-size: 18px">تاريخ التقرير : </span>'+
                                                                        '<span style="font-size: 16px">'+report.date+'</span>'+
                                                                    '</div>'+
                                                                '</div>'+
                                                                '<div class="col-md-6">'+
                                                                    '<div>'+
                                                                        '<span class="bold" style="font-size: 18px">اسم المحل : </span>'+
                                                                    ' <span style="font-size: 16px">'+report.store+'</span>'+
                                                                    '</div>'+
                                                                '</div>'+
                                                            '</div>'+
                                                            '<div class="mb-3">'+
                                                                '<span class="bold" style="font-size:20px;">ملاحظات : </span>' +
                                                                '<span style="font-size:16px;">'+report.notes+''+
                                                                '</span>'+
                                                            '</div>'+
                                                            '<div class="mb-3">';
                                                                if ('images' in report){
                                                    reports += '<h3>صور : </h3>' +
                                                                '<div class="row">';
                                                                report.images.forEach(image => {
                                                        reports +=  '<div class="col-md-4 text-left">'+
                                                                        '<img src="'+image+'" style="height:250px; border:1px solid #dfdfdf; border-radius:20% !important;" alt="">'+
                                                                    '</div>';
                                                                });
                                                    reports +=  '</div>';
                                                                }
                                                            
                                                    reports += '</div>' +
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>';
                                });

                        if (actionType == 'submit') {
                            $('#reports-list').html(reports);
                            $('#load-more-button').show();
                        } else {
                            $('#reports-list').append(reports);
                        }
                        Page++;
                    } else {
                        if (actionType == 'submit') {
                            $('#reports-list').html('');
                        }
                        $('#load-more-button').hide();
                    }
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                $(actionSource).prop('disabled', false);
                if (actionType == 'loadmore') {
                    $(actionSource).html('المزيد');
                } else {
                    $(actionSource).html('بحث');
                }
                My.ajax_error_message(xhr);
            },
            dataType: "json",
            type: "GET"
        });
        return false;
    }


    return {
        init: function () {
            init();
        }
    }

}();

jQuery(document).ready(function () {
    ManualReports.init();
});
