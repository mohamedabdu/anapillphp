var HolidayRequests = function () {
    var actionSource, actionType, status, Page = 2;
    var init = function () {
        handleButtonsClick();
        handleDate();
        handleStatusButtons();
        handleSubmit();
    }

    var handleDate = function () {
        $('.picker').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            rtl: true
        });
    }

    var handleButtonsClick = function () {

        $('#load-more-button').on('click', function () {
            actionSource = '#load-more-button';
            actionType = 'loadmore';
            $(actionSource).prop('disabled', true);
            $(actionSource).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
            setTimeout(function () {
                handleButtonActions();
            }, 1000);

        });

        $('#filterForm .submit-form').on('click', function () {
            actionSource = '#filterForm .submit-form';
            actionType = 'submit';
            $(actionSource).prop('disabled', true);
            $(actionSource).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
            setTimeout(function () {
                Page = 1;
                handleButtonActions();
            }, 1000);
        });

    }

    var handleButtonActions = function () {
        var action = new_config.url;
        var data = $("#filterForm").serializeArray();
        var params = {
            page: Page
        };
        $.each(data, function (i, field) {
            var name = field.name;
            var value = field.value;
            if (value) {
                params[name] = value;
            }
        });
        var query = $.param(params);
        action += '?' + query;
        $.ajax({
            url: action,
            success: function (data) {
                $(actionSource).prop('disabled', false);
                if (actionType == 'loadmore') {
                    $(actionSource).html('المزيد');
                } else {
                    $(actionSource).html('بحث');
                }

                if (data.type == 'success') {
                    var holiday_requests = '';
                    if (data.data.holiday_requests.length > 0) {
                        data.data.holiday_requests.forEach(holiday_request => {
                            holiday_requests += '<div class="portlet light bordered">' +
                                '<div class="portlet-title">' +
                                '<div class="caption">' +
                                holiday_request.status_text +
                                '</div>' +

                                '</div>' +
                                '<div class="portlet-body">' +

                                '<div class="row mb-5">' +
                                '<div class="col-md-4 mb-3">' +
                                '<span class="bold" style="font-size: 18px"> تاريخ الطلب : </span>' +
                                '<span style="font-size: 16px">' + holiday_request.created_at + '</span>' +
                                '</div>' +

                                '<div class="col-md-6 mb-3">' +
                                '<span class="bold" style="font-size: 18px">اسم الموظف : </span>' +
                                '<span style="font-size: 16px">' + holiday_request.employee + '</span>' +
                                '</div>' +

                                '<div class="col-md-6 mb-3">' +
                                '<span class="bold" style="font-size: 18px">عنوان الإجازة : </span>' +
                                '<span style="font-size: 16px">' + holiday_request.title + '</span>' +
                                '</div>' +

                                '<div class="col-xs-12 mb-3">' +
                                '<div class="row">' +
                                '<div class="col-xs-12 mb-3">' +
                                '<span class="bold" style="font-size: 18px">وقت الإجازة : </span>' +
                                '</div>' +

                                '<div class="col-md-4 mb-3">' +
                                '<span class="bold" style="font-size: 18px">بداية الإجازة : </span>' +
                                '<span style="font-size: 16px">' + holiday_request.start_date + '</span>' +
                                '</div>' +
                                ' <div class="col-md-4 mb-3">' +
                                '<span class="bold" style="font-size: 18px">نهاية الإجازة : </span>' +
                                '<span style="font-size: 16px">' + holiday_request.end_date + '</span>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +

                                '<div class="col-xs-12 mt-3">' +
                                '<a href="' + config.supervisor_url + '/holiday_requests/' + holiday_request.id + '" class="btn btn-primary"> التفاصيل </a>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>'
                        });

                        if (actionType == 'submit') {
                            $('#holiday-requests-list').html(holiday_requests);
                            $('#load-more-button').show();
                        } else {
                            $('#holiday-requests-list').append(holiday_requests);
                        }
                        Page++;
                    } else {
                        if (actionType == 'submit') {
                            $('#holiday-requests-list').html('');
                        }
                        $('#load-more-button').hide();
                    }
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                $(actionSource).prop('disabled', false);
                if (actionType == 'loadmore') {
                    $(actionSource).html('المزيد');
                } else {
                    $(actionSource).html('بحث');
                }
                My.ajax_error_message(xhr);
            },
            dataType: "json",
            type: "GET"
        });
        return false;


    }

    var handleStatusButtons = function () {

        $('#accept').on('click', function () {
            status = 'accepted';
            $('#accept').prop('disabled', true);
            $('#accept').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
            setTimeout(function () {
                handleHolidayStatus();
            }, 1000);
        });

        $('#reject').on('click', function () {
            $('#rejectionForm').toggle('');
        });
    }

    var handleSubmit = function () {
        $('#rejectionForm').validate({
            rules: {
                rejection_reason: {
                    required: true
                }

            },
            messages: {
                rejection_reason: {
                    required: 'هذا الحقل مطلوب'
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);
            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        $('#rejectionForm .submit-form').click(function () {
            if ($('#rejectionForm').validate().form()) {
                status = 'rejected';
                $('#rejectionForm .submit-form').prop('disabled', true);
                $('#rejectionForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    handleHolidayStatus();
                }, 1000);
            }
        });

        $('#rejectionForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#rejectionForm').validate().form()) {
                    status = 'rejected';
                    $('#rejectionForm .submit-form').prop('disabled', true);
                    $('#rejectionForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        handleHolidayStatus();
                    }, 1000);
                }
            }
        });
    }

    var handleHolidayStatus = function () {

        var action = config.supervisor_url + '/holiday_requests/' + new_config.holiday_request + '/update';
        var data = {
            _token: $('input[name="_token"]').val(),
            status: status
        }
        if (status == 'rejected') {
            data.rejection_reason = $('#rejection_reason').val();
        }

        $.ajax({
            url: action,
            data: data,
            success: function (data) {
                $('#accept').prop('disabled', false);
                $('#accept').html('قبول');
                $('#rejectionForm .submit-form').prop('disabled', false);
                $('#rejectionForm .submit-form').html('ارسال');
                if (data.type == 'success') {
                    window.location.reload();
                } else {
                    if (typeof data.errors !== 'undefined') {
                        for (i in data.errors) {
                            var message = data.errors[i];
                            $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                            $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                        }
                    }
                }

            },
            error: function (xhr, textStatus, errorThrown) {
                $('#accept').prop('disabled', false);
                $('#accept').html('قبول');
                $('#rejectionForm .submit-form').prop('disabled', false);
                $('#rejectionForm .submit-form').html('ارسال');
                My.ajax_error_message(xhr);
            },
            dataType: "json",
            type: "POST"
        });
        return false;
    }

    return {
        init: function () {
            init();
        }
    }

}();

jQuery(document).ready(function () {
    HolidayRequests.init();
});
