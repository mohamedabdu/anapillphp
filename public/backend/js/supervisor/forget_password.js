var ForgetPassword = function () {
    var currentTab = 0;
    var button_text;
   

    var init = function () {
        handleSubmit();
        handleTabClick();
        handleCode();
        showTab(currentTab);
        handleResendCode();
    };

 
    var handleCode = function(){
        $(document).on('input', '.code', function (event) {
            var length = $(this).val().trim().length;
            this.value = this.value.replace(/[^0-9\.-]/g, "");
            if (length == 1) {
                $(this).next('.code').focus();
            }
        });
        $(".code").on('focus', function () {
            $(this).select();
        });
        
    }

     var handleResendCode = function () {
         $('#resendCode').on('click', function () {
             event.preventDefault();
             $('#resendCode').prop('disabled', true);
             $('#resendCode').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">' + lang.loading + '</span>');
             var action = config.url + '/supervisor/password/resend_code';
             
             $.ajax({
                 url: action,
                  data: {
                    _token: $('input[name="_token"]').val(),
                    mobile: $('input[name="mobile"]').val()
                  },
                 success: function (data) {
                     if (data.type == 'success') {
                        $('#resendCode').prop('disabled', false);
                        $('#resendCode').html('اعادة ارسال كود التفعيل');
                        $('.alert-success').show();
                        $('.alert-success span').html(data.message);
                     }
                 },
                 error: function (xhr, textStatus, errorThrown) {
                     $('#resendCode').prop('disabled', false);
                     $('#resendCode').html('اعادة ارسال كود التفعيل');
                     My.ajax_error_message(xhr);
                 },
                 dataType: "json",
                 type: "post"
             });
             return false;
         });

     }
    
    var handleTabClick = function () {
        $(".step").on('click', function () {
            return false;
        })
    }

    var showTab = function (n) {
        // This function will display the specified tab of the form ...
        var tabs = $(".tab-pane");
        $(tabs[n]).show();
        //$(x[n]).css('display', 'block');

        if (n == 0) {
            $('.button-previous').hide();
            //$('.button-previous').css('display', 'none');
        } else {
            $('.button-previous').show();
            //$('.button-previous').css('display', 'inline');
        }
        if (n == (tabs.length - 1)) {
            $('.button-next').html('تأكيد');
        } else {
            $('.button-next').html('استمرار');
        }
       
        // ... and run a function that displays the correct step indicator:
        fixStepIndicator(n)
    }

    var fixStepIndicator = function (n) {
        // This function removes the "active" class of all steps...
        var steps = $(".step");

        $(".step").removeClass('active');
        $('.steps li').removeClass('active');

        $(steps[n - 1]).closest('li').addClass('done');
        $(steps[n]).closest('li').addClass('active');
        $(steps[n]).addClass('active');

        //progrss bar
        var i = $('.steps').find("li").length,
            a = n + 1,
            o = a / i * 100;
        $("#forgetPasswordForm").find(".progress-bar").css({
            width: o + "%"
        })
    }

    var handleSubmit = function () {

        $('#forgetPasswordForm').validate({
            rules: {
                mobile: {
                    required: true
                },
                 'code[1]': {
                    required: true
                 },
                 'code[2]': {
                    required: true
                 },
                 'code[3]': {
                    required: true
                 },
                 'code[4]': {
                    required: true
                 },
                password: {
                    required: true
                },
                password_confirmation: {
                    required: true,
                    equalTo: "#password"
                }
            },
            messages:{
                mobile:{
                    required: 'هذا الحقل مطلوب'
                },
                'code[1]': {
                     required: 'هذا الحقل مطلوب'
                },
                'code[2]': {
                     required: 'هذا الحقل مطلوب'
                },
                'code[3]': {
                     required: 'هذا الحقل مطلوب'
                },
                'code[4]': {
                     required: 'هذا الحقل مطلوب'
                },
                password: {
                    required: 'هذا الحقل مطلوب'
                },
                password_confirmation: {
                    required: 'هذا الحقل مطلوب',
                    equalTo : 'كلمة المرور غير متطابقة'
                },
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);
            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });


        $('#forgetPasswordForm').submit(function () {

            var action = config.url + '/supervisor/password/reset';
            var formData = new FormData($(this)[0]);
            formData.append('step', currentTab + 1);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#forgetPasswordForm .button-next').prop('disabled', false);
                    $('#forgetPasswordForm .button-next').html(button_text);

                    if (data.type == 'success') {
                        if (currentTab + 1 == $('.tab-pane').length) {
                            window.location.href = data.message;
                        }
                        var hideTab = currentTab;
                        currentTab = currentTab + 1;
                        $('.tab-pane:eq(' + hideTab + ')').hide();
                        $('.alert-danger').hide();
                        $('.alert-success').hide();
                        showTab(currentTab);  
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }else{
                            $('.alert-danger').show();
                            $('.alert-danger span').html(data.message);
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#forgetPasswordForm .button-next').prop('disabled', false);
                    $('#forgetPasswordForm .button-next').html(button_text);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });

            return false;

        })




    }

    return {
        init: function () {
            init();
        },
        nextPrev: function (n) {
            
            // This function will figure out which tab to display
            button_text = 'استمرار';
            var tabs = $(".tab-pane");
            var steps = $(".step");


            if (n == 1) {
                if (!$('#forgetPasswordForm').valid()) return false;
                $('#forgetPasswordForm .button-next').prop('disabled', true);
                $('#forgetPasswordForm .button-next').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                if (currentTab + 1 == tabs.length) {
                    button_text = 'تأكيد';
                }
                setTimeout(function () {
                    $('#forgetPasswordForm').submit();
                }, 1000);
                return false;
            } else {
                // Hide the current tab:
                $(tabs[currentTab]).hide();
                // Increase or decrease the current tab by 1:
                currentTab = currentTab + n;
                if (n == -1) {
                    $(steps[currentTab]).closest('li').removeClass('done');
                }
                // Otherwise, display the correct tab:
                showTab(currentTab);
            }

        }
    };

}();

jQuery(document).ready(function () {
    ForgetPassword.init();
});