var ReducedPricesReports = function () {
    var Page = 2;
    var init = function () {
        handleLoadMore();
    }
    var handleLoadMore = function () {
        $('#load-more-button').on('click', function () {
            $(this).prop('disabled', true);
            $(this).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only"></span>');
            var data = {
                page: Page,
                employee: new_config.employee
            };
            var action = new_config.url;
            $.ajax({
                url: action,
                data: data,
                success: function (data) {
                    $('#load-more-button').prop('disabled', false);
                    $('#load-more-button').html('المزيد');
                    if (data.type == 'success') {
                        var reports = '';
                        if (data.data.reports.length > 0) {
                            data.data.reports.forEach(report => {
                                reports += '<div class="portlet light bordered">' +
                                                 '<div class="portlet-title">'+
                                                    '<div class="caption">'+report.created_at+'</div>'+
                                                '</div>'+
                                                '<div class="portlet-body">'+
                                                    '<div class="single-report">'+
                                                            '<div class="row mb-5">'+
                                                                '<div class="col-md-6 mb-3">' +
                                                                    '<div>'+
                                                                    '<span class="bold" style="font-size: 18px">اسم الموظف : </span>'+
                                                                        '<span style="font-size: 16px">'+new_config.employee_name+'</span>'+
                                                                    '</div>'+
                                                                '</div>'+
                                                                '<div class="col-md-6 mb-3">' +
                                                                    '<div>'+
                                                                    '<span class="bold" style="font-size: 18px">تاريخ التقرير : </span>'+
                                                                        '<span style="font-size: 16px">'+report.date+'</span>'+
                                                                    '</div>'+
                                                                '</div>'+
                                                                '<div class="col-md-6 mb-3">' +
                                                                    '<div>'+
                                                                        '<span class="bold" style="font-size: 18px">اسم المحل : </span>'+
                                                                    ' <span style="font-size: 16px">'+report.store+'</span>'+
                                                                    '</div>'+
                                                                '</div>'+
                                                                '<div class="col-md-6">'+
                                                                    '<div>'+
                                                                        '<span class="bold" style="font-size: 18px">اسم المنافس : </span>'+
                                                                    ' <span style="font-size: 16px">'+report.competitor+'</span>'+
                                                                    '</div>'+
                                                                '</div>'+
                                                            '</div>'+
                                                            '<div class="mb-5">'+
                                                                '<div class="col-md-12">'+
                                                                    '<div class="portlet light bordered">'+
                                                                        '<div class="portlet-title">'+
                                                                            '<div class="caption">'+
                                                                                '<span class="caption-subject font-dark bold uppercase">المنتجات</span>'+
                                                                            '</div>'+
                                                                        '</div>'+
                                                                        '<div class="portlet-body flip-scroll">'+
                                                                            '<table class="table table-striped table-bordered table-condensed flip-content">'+
                                                                                '<thead class="flip-content">'+
                                                                                    '<tr>'+
                                                                                        '<th width="20%" class="text-center"> المنتج المنافس </th>'+
                                                                                        '<th width="20%" class="text-center"> نوع العرض </th>' +
                                                                                        '<th class="numeric text-center"> السعر الحالي </th>'+
                                                                                        '<th class="numeric text-center"> سعر العرض </th>'+
                                                                                    '</tr>'+
                                                                                '</thead>'+
                                                                                '<tbody>';
                                                                                report.products.forEach(product => {
                                                                        reports += '<tr>' +
                                                                                        '<td width="20%" class="text-center"> ' + product.competitor_product + ' </td>' +
                                                                                        '<td width="20%" class="text-center"> ' + product.promo + ' </td>' +
                                                                                        '<td class="numeric text-center"> '+product.current_price+' </td>'+
                                                                                        '<td class="numeric text-center"> '+product.offer_price+' </td>'+
                                                                                    '</tr>';
                                                                                });
                                                                                    
                                                                reports +=      '</tbody>'+
                                                                            '</table>'+
                                                                        '</div>'+
                                                                    '</div>'+
                                                            
                                                                '</div>'+
                                                            '</div>'+
                                                            '<div class="mb-3">'+
                                                                '<span class="bold" style="font-size:20px;">ملاحظات : </span>' +
                                                                '<span style="font-size:16px;">'+report.notes+''+
                                                                '</span>'+
                                                            '</div>'+
                                                            '<div class="mb-3">';
                                                                if ('images' in report){
                                                    reports += '<h3>صور : </h3>' +
                                                                '<div class="row">';
                                                                report.images.forEach(image => {
                                                        reports +=  '<div class="col-md-4 text-left">'+
                                                                        '<img src="'+image+'" style="height:250px; border:1px solid #dfdfdf; border-radius:20% !important;" alt="">'+
                                                                    '</div>';
                                                                });
                                                    reports +=  '</div>';
                                                                }
                                                            
                                                    reports += '</div>' +
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>';
                                });
                            $('#reports-list').append(reports);
                            Page++;
                        } else {
                            $('#load-more-button').css('display', 'none');
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#load-more-button').prop('disabled', false);
                    $('#load-more-button').html('المزيد');
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "GET"
            });
            return false;

        })
    }


    return {
        init: function () {
            init();
        }
    }

}();

jQuery(document).ready(function () {
    ReducedPricesReports.init();
});
