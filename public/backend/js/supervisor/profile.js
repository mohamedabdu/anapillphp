var Profile = function () {

    var init = function () {
        handleSubmit();
        readImageMulti('image');
    };
    var readImageMulti = function (input) {
        $(document).on('click', "." + input, function () {
            $("#" + input).trigger('click');
        });

        $(document).on('change', "#" + input, function () {
            //console.log($(this));
            for (var i = 0; i < $(this)[0].files.length; i++) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.' + input + '_box').html('<img style="height:200px;width:200px; border:5px solid #dfdfdf; border-radius: 20% !important" id="image_upload_preview" class="' + input + '" src="' + e.target.result + '" alt="your image" />');
                }
                reader.readAsDataURL($(this)[0].files[i]);
            }

        });
    }
    
    var handleSubmit = function () {
        $('#updateProfile').validate({
            rules: {
                name: {
                    required: true,
                },
                mobile: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true
                },
                old_password: {
                    required: true,
                },
                password: {
                    required: true,
                },
                password_confirmation:{
                    required: true,
                    equalTo: "#password"
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        $('#updateProfile .submit-form').click(function () {

            if ($('#updateProfile').validate().form()) {
                $('#updateProfile .submit-form').prop('disabled', true);
                $('#updateProfile .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#updateProfile').submit();
                }, 1000);
            }
            return false;
        });
        $('#updateProfile input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#updateProfile').validate().form()) {
                    $('#updateProfile .submit-form').prop('disabled', true);
                    $('#updateProfile .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#updateProfile').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#updateProfile').submit(function () {
            var action = config.url + '/supervisor/update_profile';
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    $('#updateProfile .submit-form').prop('disabled', false);
                    $('#updateProfile .submit-form').html('تعديل');

                    if (data.type == 'success') {
                        My.toast(data.message);
                        window.location.reload();
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#updateProfile .submit-form').prop('disabled', false);
                    $('#updateProfile .submit-form').html('تعديل');
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })
    }

    return {
        init: function () {
            init();
        },
         empty: function () {
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            My.emptyForm();
         }
    };

}();
jQuery(document).ready(function () {
    Profile.init();
});