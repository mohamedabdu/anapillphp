var Admins = function() {
    var AdminsGrid, string_length = 8;
    var init = function() {
        handleRecords();
        handleSubmit();
        handlePasswordActions(string_length);
        My.readImageMulti('image');       
    };

    var handlePasswordActions = function (string_length) {
        $('#show-password').click(function () {
            if ($('#password').val() != '') {
                $("#password").attr("type", "text");

            } else {
                $("#password").attr("type", "password");

            }
        });
        $('#random-password').click(function () {
            $('[id^="password"]').closest('.form-group').removeClass('has-error').addClass('has-success');
            $('[id^="password"]').closest('.form-group').find('.help-block').html('').css('opacity', 0);
            $('[id^="password"]').val(randomPassword(string_length));
        });
    }
    var randomPassword = function (string_length) {
        var chars = "0123456789!@#$%^&*abcdefghijklmnopqrstuvwxtzABCDEFGHIJKLMNOPQRSTUVWXTZ!@#$%^&*";
        var myrnd = [], pos;
        while (string_length--) {
            pos = Math.floor(Math.random() * chars.length);
            myrnd += chars.substr(pos, 1);
        }
        return myrnd;
    }
   
    var handleRecords = function() {
        AdminsGrid = $('.dataTable').dataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": config.admin_url + "/admins/data",
                "type": "POST",
                data: { _token: $('input[name="_token"]').val() },
            },
            "columns": [
                {
                    "data": "name",
                    "name" : "users.name"
                }, 
                {
                    "data": "image",
                    searchable: false,
                    orderable: false
                },
                {
                    "data": "role",
                    "name" : "role_translations.title"
                    
                },
                {
                    "data": "email",
                    "name": "users.email"
                }, 
                {
                    "data": "mobile",
                    "name": "users.mobile"
                }, 
                
                {
                    "data": "active",
                    searchable: false,
                    orderable: false
                }, 
                {
                    "data": "options",
                    orderable: false,
                    searchable: false
                }
            ],
            "order": [
                [0, "asc"]
            ],

            "oLanguage": { "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json' }

        });
    }


    var handleSubmit = function() {
        $('#addEditAdminsForm').validate({
            rules: {
                name: {
                    required: true,
                },
                mobile: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true
                },
                image: {
                    extension: "png|jpeg|jpg|gif"
                },
                active: {
                    required: true
                },
                role: {
                    required: true
                }

            },
            //messages: lang.messages,
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function(error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        if ($('#id').val() != 0) {
            $('#password').rules('remove');
        }


        $('#addEditAdminsForm .submit-form').click(function() {

            if ($('#addEditAdminsForm').validate().form()) {
                $('#addEditAdminsForm .submit-form').prop('disabled', true);
                $('#addEditAdminsForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function() {
                    $('#addEditAdminsForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#addEditAdminsForm input').keypress(function(e) {
            if (e.which == 13) {
                if ($('#addEditAdminsForm').validate().form()) {
                    $('#addEditAdminsForm .submit-form').prop('disabled', true);
                    $('#addEditAdminsForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function() {
                        $('#addEditAdminsForm').submit();
                    }, 1000);
                }
                return false;
            }
        });

        $('#addEditAdminsForm').submit(function() {
            var id = $('#id').val();
            var action = config.admin_url + '/admins';
            var formData = new FormData($(this)[0]);
            if (id != 0) {
                formData.append('_method', 'PATCH');
                action = config.admin_url + '/admins/' + id;
            }
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    $('#addEditAdminsForm .submit-form').prop('disabled', false);
                    $('#addEditAdminsForm .submit-form').html(lang.save);

                    if (data.type == 'success') {
                        My.toast(data.message);
                        if (id == 0) {
                            Admins.empty();
                        }
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    $('#addEditAdminsForm .submit-form').prop('disabled', false);
                    $('#addEditAdminsForm .submit-form').html(lang.save);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })




    }

    return {
        init: function() {
            init();
        },
        delete: function(t) {

            var id = $(t).attr("data-id");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/admins/' + id,
                data: { _method: 'DELETE', _token: $('input[name="_token"]').val() },
                success: function(data) {
                    console.log(data);
                    AdminsGrid.api().ajax.reload();
                }
            });

        },
        changeActive: function(t) {
         var user_id = $(t).data("id");
         $(t).prop('disabled', true);
         $(t).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');

         $.ajax({
            url: config.admin_url + '/admins/status/' + user_id,
            data: {
                _token: $('input[name="_token"]').val()
            },
            success: function (data) {
                 $(t).prop('disabled', false);
                 if ($(t).hasClass("btn-success")) {
                    $(t).addClass('btn-danger').removeClass('btn-success');
                    $(t).html(lang.deactivated);
                 } else {
                    $(t).addClass('btn-success').removeClass('btn-danger');
                    $(t).html(lang.activated);
                 }
            },
            error: function (xhr, textStatus, errorThrown) {
                My.ajax_error_message(xhr);
            },
            dataType: "json",
            type: "POST"
         });
    },
       
        empty: function() {
            $('#active').find('option').eq(0).prop('selected', true);
            $('#role').find('option').eq(0).prop('selected', true);
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            $('.image_box').html('<img src="' + config.url + '/no-image.png" width="100" height="80" class="image" />');
            $('#image').val('');
            My.emptyForm();
        }
    };

}();
jQuery(document).ready(function() {
    Admins.init();
});