var Competitors = function () {
    var Competitors_grid;

    var init = function () {
        handleRecords();
        handleSubmit();
        handleCountryChange();
        handleRegionChange();
    };

    var handleCountryChange = function () {
        $('#country').on('change', function () {
            var country = $(this).val();
            $('#region').html('<option selected value="">' + lang.choose + '</option>');
            $('#city').html('<option selected value="">' + lang.choose + '</option>');
            regions = "";
            if (country) {
                $.ajax({
                    url: config.admin_url + '/get_locations/' + country,
                    success: function (data) {
                       if (data.data.length != 0) {
                           for (var x = 0; x < data.data.length; x++) {
                               var item = data.data[x];
                               regions += '<option value="' + item.id + '">' + item.title + '</option>';
                           }
                           $('#region').append(regions);
                       }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        My.ajax_error_message(xhr);
                    },
                    dataType: "json",
                    type: "GET"
                });
            }
        })
    }

    var handleRegionChange = function () {
        $('#region').on('change', function () {
            var region = $(this).val();
            $('#city').html('<option selected value="">' + lang.choose + '</option>');
            cities = "";
            if (region) {
                $.ajax({
                    url: config.admin_url + '/get_locations/' + region,
                    success: function (data) {
                       if (data.data.length != 0) {
                           for (var x = 0; x < data.data.length; x++) {
                               var item = data.data[x];
                               cities += '<option value="' + item.id + '">' + item.title + '</option>';
                           }
                           $('#city').append(cities);
                       }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        My.ajax_error_message(xhr);
                    },
                    dataType: "json",
                    type: "GET"
                });
            }
        })
    }

    var handleRecords = function () {
        Competitors_grid = $('.dataTable').dataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": config.admin_url + "/competitors/data",
                "type": "POST",
                data: {
                    _token: $('input[name="_token"]').val()
                },
            },
            "columns": [
                {
                    "data": "title",
                    "name": "competitor_translations.title"
                },
                {
                    "data": "category",
                    "name": "category_translations.title"
                },
                {
                    "data": "country",
                    "name": "country_trans.title"
                },
                {
                    "data": "region",
                    "name": "region_trans.title"
                },
                {
                    "data": "city",
                    "name": "city_trans.title"
                },
                {
                    "data": "active",
                    "name": "competitors.active",
                    orderable: false,
                    searchable: false
                },
                {
                    "data": "products",
                    orderable: false,
                    searchable: false
                },
                {
                    "data": "options",
                    orderable: false,
                    searchable: false
                }
            ],
            "columnDefs": [ {
                "width": "20%",
                "targets": 0
            }],
            "order": [
                [0, "asc"]
            ],
            "oLanguage": {
                "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json'
            }

        });
    }


    var handleSubmit = function () {
        $('#addEditCompetitorsForm').validate({
            rules: {
                active:{
                    required:true
                },
                category: {
                    required: true
                },
                country: {
                    required: true
                },
                region: {
                    required: true
                },
                city: {
                    required: true
                }
            },
            //messages: lang.messages,
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });


        var langs = JSON.parse(config.languages);
        for (var x = 0; x < langs.length; x++) {
            var ele = "input[name='title[" + langs[x] + "]']";
            $(ele).rules('add', {
                required: true
            });
        }


        $('#addEditCompetitorsForm .submit-form').click(function () {

            if ($('#addEditCompetitorsForm').validate().form()) {
                $('#addEditCompetitorsForm .submit-form').prop('disabled', true);
                $('#addEditCompetitorsForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#addEditCompetitorsForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#addEditCompetitorsForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#addEditCompetitorsForm').validate().form()) {
                    $('#addEditCompetitorsForm .submit-form').prop('disabled', true);
                    $('#addEditCompetitorsForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#addEditCompetitorsForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#addEditCompetitorsForm').submit(function () {
            var id = $('#id').val();
            var action = config.admin_url + '/competitors';
            var formData = new FormData($(this)[0]);
            if (id != 0) {
                formData.append('_method', 'PATCH');
                action = config.admin_url + '/competitors/' + id;
            }
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#addEditCompetitorsForm .submit-form').prop('disabled', false);
                    $('#addEditCompetitorsForm .submit-form').html(lang.save);

                    if (data.type == 'success') {
                        My.toast(data.message);
                        if (id == 0) {
                            Competitors.empty();
                        }

                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                if (i.startsWith('title')) {
                                    var key_arr = i.split('.');
                                    var key_text = key_arr[0] + '[' + key_arr[1] + ']';
                                    i = key_text;
                                }
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#addEditCompetitorsForm .submit-form').prop('disabled', false);
                    $('#addEditCompetitorsForm .submit-form').html(lang.save);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });


            return false;

        })
    }

    return {
        init: function () {
            init();
        },
        delete: function (t) {

            var id = $(t).attr("data-id");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/competitors/' + id,
                data: {
                    _method: 'DELETE',
                    _token: $('input[name="_token"]').val()
                },
                success: function (data) {
                    Competitors_grid.api().ajax.reload();
                }
            });
        },
        changeActive: function (t) {
            var user_id = $(t).data("id");
            $(t).prop('disabled', true);
            $(t).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');

            $.ajax({
                url: config.admin_url + '/competitors/status/' + user_id,
                data: {
                    _token: $('input[name="_token"]').val()
                },
                success: function (data) {
                    $(t).prop('disabled', false);
                    if ($(t).hasClass("btn-success")) {
                        $(t).addClass('btn-danger').removeClass('btn-success');
                        $(t).html(lang.deactivated);
                    } else {
                        $(t).addClass('btn-success').removeClass('btn-danger');
                        $(t).html(lang.activated);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
        },
        empty: function () {
            $('#active').find('option').eq(0).prop('selected', true);
             $('#category').find('option').eq(0).prop('selected', true);
             $('#country').find('option').eq(0).prop('selected', true);
             $('#region').html('<option value="">' + lang.choose + '</option>');
             $('#city').html('<option value="">' + lang.choose + '</option>');
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            My.emptyForm();
        }
    };

}();

jQuery(document).ready(function () {
    Competitors.init();
});