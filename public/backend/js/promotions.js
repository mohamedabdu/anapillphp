var Promotions = function () {
    var Promotions_grid;

    var init = function () {
        handleRecords();
        handleSubmit();
        My.readImageMulti('image');
    };

    var handleRecords = function () {
        Promotions_grid = $('.dataTable').dataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": config.admin_url + "/promotions/data",
                "type": "POST",
                data: {
                    _token: $('input[name="_token"]').val()
                },
            },
            "columns": [
                {
                    "data": "title",
                    "name": "promotion_translations.title"
                },
                {
                    "data": "options",
                    orderable: false,
                    searchable: false
                }
            ],
            "order": [
                [0, "asc"]
            ],
            "oLanguage": {
                "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json'
            }

        });
    }


    var handleSubmit = function () {
        $('#addEditPromotionsForm').validate({
            rules: {
                image: {
                    required : true,
                    extension: "png|jpeg|jpg|gif"
                },
            },
            //messages: lang.messages,
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });


        var langs = JSON.parse(config.languages);
        for (var x = 0; x < langs.length; x++) {
            var ele = "input[name='title[" + langs[x] + "]']";
            $(ele).rules('add', {
                required: true
            });
        }

        if ($('#id').val() != 0) {
            $('#image').rules('remove');
        }

        $('#addEditPromotionsForm .submit-form').click(function () {

            if ($('#addEditPromotionsForm').validate().form()) {
                $('#addEditPromotionsForm .submit-form').prop('disabled', true);
                $('#addEditPromotionsForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#addEditPromotionsForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#addEditPromotionsForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#addEditPromotionsForm').validate().form()) {
                    $('#addEditPromotionsForm .submit-form').prop('disabled', true);
                    $('#addEditPromotionsForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#addEditPromotionsForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#addEditPromotionsForm').submit(function () {
            var id = $('#id').val();
            var action = config.admin_url + '/promotions';
            var formData = new FormData($(this)[0]);
            if (id != 0) {
                formData.append('_method', 'PATCH');
                action = config.admin_url + '/promotions/' + id;
            }
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#addEditPromotionsForm .submit-form').prop('disabled', false);
                    $('#addEditPromotionsForm .submit-form').html(lang.save);

                    if (data.type == 'success') {
                        My.toast(data.message);
                        if (id == 0) {
                            Promotions.empty();
                        }

                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                if (i.startsWith('title')) {
                                    var key_arr = i.split('.');
                                    var key_text = key_arr[0] + '[' + key_arr[1] + ']';
                                    i = key_text;
                                }
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#addEditPromotionsForm .submit-form').prop('disabled', false);
                    $('#addEditPromotionsForm .submit-form').html(lang.save);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });


            return false;

        })
    }

    return {
        init: function () {
            init();
        },
        delete: function (t) {
            var id = $(t).attr("data-id");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/promotions/' + id,
                data: {
                    _method: 'DELETE',
                    _token: $('input[name="_token"]').val()
                },
                success: function (data) {
                    Promotions_grid.api().ajax.reload();
                }
            });
        },
        empty: function () {
            $('#active').find('option').eq(0).prop('selected', true);
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            $('.image_box').html('<img src="' + config.url + '/no-image.png" width="100" height="80" class="image" />');
            $('#image').val('');
            My.emptyForm();
        }
    };

}();
jQuery(document).ready(function () {
    Promotions.init();
});