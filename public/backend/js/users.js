var Users_grid;

var Users = function() {

    var init = function() {
        
        if (new_config.user_id != undefined) {
            handleSubscriptionsRecords();
        }else{
            handleRecords();
        }
    };
   

    
    var handleRecords = function() {

        Users_grid = $('.dataTable').dataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": config.admin_url + "/users/data",
                "type": "POST",
                data: { _token: $('input[name="_token"]').val() },
            },
            "columns": [
                {"data": "username"},
                {"data": "email"},
                { "data": "image"},
                { "data": "mobile"},
                { "data": "gender" },
                { "data": "active"},
                { "data": "options", orderable: false, searchable: false }
            ],
            "order": [
                [0, "desc"]
            ],
            "columnDefs": [
                { "className": "dt-center", "targets": "_all" }
            ],
            "oLanguage": { "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json' }

        });
    }

    var handleSubscriptionsRecords = function () {

        Subscriptions_grid = $('.dataTable').dataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": config.admin_url + "/users/subscriptions",
                "type": "POST",
                data: {
                    _token: $('input[name="_token"]').val(),
                    user_id: new_config.user_id
                },
            },
            "columns": [{
                    "data": "title",
                    "name": "package_translations.title"
                },
                {
                    "data": "duration",
                    "name": "package_subscriptions.duration"
                },
                {
                    "data": "price",
                    "name": "package_subscriptions.price"
                },
                {
                    "data": "start_date",
                    "name": "package_subscriptions.start_date"
                },
                {
                    "data": "end_date",
                    "name": "package_subscriptions.end_date"
                },
                {
                    "data": "remaining_days",
                    "name": "remaining_days"
                },
                {
                    "data": "type",
                    "name": "package_subscriptions.type",
                     searchable: false
                },
                {
                    "data": "status",
                    "name": "package_subscriptions.status", searchable: false
                },
                {
                    "data": "subscription_status", searchable: false
                }
            ],
            "order": [
                [4, "desc"]
            ],
            "columnDefs": [{
                "className": "dt-center",
                "targets": "_all"
            }],
            "oLanguage": {
                "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json'
            }

        });
    }
  
    return {
        init: function() {
            init();
        },
      
     
       status: function(t) {
            var user_id = $(t).data("id"); 
            $(t).prop('disabled', true);
            $(t).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');

            $.ajax({
                url: config.admin_url+'/users/status/'+user_id,
                success: function(data){  
                     $(t).prop('disabled', false);
                     if ($(t).hasClass( "btn-info" )) {
                        $(t).addClass('btn-danger').removeClass('btn-info');
                        $(t).html(lang.not_active);

                    }else{
                        $(t).addClass('btn-info').removeClass('btn-danger');
                        $(t).html(lang.active);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                   My.ajax_error_message(xhr);
               },
            });

        },
        delete: function(t) {
            var id = $(t).attr("data-id");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/users/' + id + '',
                data: { _method: 'DELETE', _token: $('input[name="_token"]').val() },
                success: function(data) {

                    Famous_grid.api().ajax.reload();


                }
            });
        },
        empty: function() {
            $('#id').val(0);
            $('#active').find('option').eq(0).prop('selected', true);
            $('#user_image').val(null);
            $('.user_image_box').html('<img src="' + config.url + '/no-image.png" class="user_image" width="150" height="80" />');
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            My.emptyForm();
        },
    };
}();
$(document).ready(function() {
    Users.init();
});