var Companies = function() {
    var CompaniesGrid, string_length = 8;
    var init = function() {
        handleRecords();
        handleSubmit();
        handlePasswordActions(string_length);
        My.readImageMulti('image');       
    };

    var handlePasswordActions = function (string_length) {
        $('#show-password').click(function () {
            if ($('#password').val() != '') {
                $("#password").attr("type", "text");

            } else {
                $("#password").attr("type", "password");

            }
        });
        $('#random-password').click(function () {
            $('[id^="password"]').closest('.form-group').removeClass('has-error').addClass('has-success');
            $('[id^="password"]').closest('.form-group').find('.help-block').html('').css('opacity', 0);
            $('[id^="password"]').val(randomPassword(string_length));
        });
    }
    var randomPassword = function (string_length) {
        var chars = "0123456789!@#$%^&*abcdefghijklmnopqrstuvwxtzABCDEFGHIJKLMNOPQRSTUVWXTZ!@#$%^&*";
        var myrnd = [], pos;
        while (string_length--) {
            pos = Math.floor(Math.random() * chars.length);
            myrnd += chars.substr(pos, 1);
        }
        return myrnd;
    }
   
    var handleRecords = function() {
        CompaniesGrid = $('.dataTable').dataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": config.admin_url + "/companies/data",
                "type": "POST",
                data: { _token: $('input[name="_token"]').val() },
            },
            "columns": [
                {
                    "data": "name",
                    "name" : "users.name"
                }, 
                {
                    "data": "image",
                    searchable: false,
                    orderable: false
                },
                {
                    "data": "role",
                    "name" : "role_translations.title"
                    
                },
                {
                    "data": "email",
                    "name": "users.email"
                }, 
                {
                    "data": "mobile",
                    "name": "users.mobile"
                }, 
                
                {
                    "data": "active",
                    searchable: false,
                    orderable: false
                }, 
                {
                    "data": "options",
                    orderable: false,
                    searchable: false
                }
            ],
            "order": [
                [0, "asc"]
            ],

            "oLanguage": { "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json' }

        });
    }


    var handleSubmit = function() {
        $('#addEditCompaniesForm').validate({
            rules: {
                name: {
                    required: true,
                },
                mobile: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true
                },
                image: {
                    extension: "png|jpeg|jpg|gif"
                },
                active: {
                    required: true
                },
                role: {
                    required: true
                }

            },
            //messages: lang.messages,
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function(error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        if ($('#id').val() != 0) {
            $('#password').rules('remove');
        }


        $('#addEditCompaniesForm .submit-form').click(function() {

            if ($('#addEditCompaniesForm').validate().form()) {
                $('#addEditCompaniesForm .submit-form').prop('disabled', true);
                $('#addEditCompaniesForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function() {
                    $('#addEditCompaniesForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#addEditCompaniesForm input').keypress(function(e) {
            if (e.which == 13) {
                if ($('#addEditCompaniesForm').validate().form()) {
                    $('#addEditCompaniesForm .submit-form').prop('disabled', true);
                    $('#addEditCompaniesForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function() {
                        $('#addEditCompaniesForm').submit();
                    }, 1000);
                }
                return false;
            }
        });

        $('#addEditCompaniesForm').submit(function() {
            var id = $('#id').val();
            var action = config.admin_url + '/companies';
            var formData = new FormData($(this)[0]);
            if (id != 0) {
                formData.append('_method', 'PATCH');
                action = config.admin_url + '/companies/' + id;
            }
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    $('#addEditCompaniesForm .submit-form').prop('disabled', false);
                    $('#addEditCompaniesForm .submit-form').html(lang.save);

                    if (data.type == 'success') {
                        My.toast(data.message);
                        if (id == 0) {
                            Companies.empty();
                        }
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    $('#addEditCompaniesForm .submit-form').prop('disabled', false);
                    $('#addEditCompaniesForm .submit-form').html(lang.save);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })




    }

    return {
        init: function() {
            init();
        },
        delete: function(t) {

            var id = $(t).attr("data-id");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/companies/' + id,
                data: { _method: 'DELETE', _token: $('input[name="_token"]').val() },
                success: function(data) {
                    console.log(data);
                    CompaniesGrid.api().ajax.reload();
                }
            });

        },
        changeActive: function(t) {
         var user_id = $(t).data("id");
         $(t).prop('disabled', true);
         $(t).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');

         $.ajax({
            url: config.admin_url + '/companies/status/' + user_id,
            data: {
                _token: $('input[name="_token"]').val()
            },
            success: function (data) {
                 $(t).prop('disabled', false);
                 if ($(t).hasClass("btn-success")) {
                    $(t).addClass('btn-danger').removeClass('btn-success');
                    $(t).html(lang.deactivated);
                 } else {
                    $(t).addClass('btn-success').removeClass('btn-danger');
                    $(t).html(lang.activated);
                 }
            },
            error: function (xhr, textStatus, errorThrown) {
                My.ajax_error_message(xhr);
            },
            dataType: "json",
            type: "POST"
         });
    },
       
        empty: function() {
            $('#active').find('option').eq(0).prop('selected', true);
            $('#role').find('option').eq(0).prop('selected', true);
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            $('.image_box').html('<img src="' + config.url + '/no-image.png" width="100" height="80" class="image" />');
            $('#image').val('');
            My.emptyForm();
        }
    };

}();

jQuery(document).ready(function() {
    Companies.init();
});