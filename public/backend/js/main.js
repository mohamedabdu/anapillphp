
var Main = function () {


    var init = function () {
        handleChangeLang();
        handleEditor();
    }



        var handleEditor = function () {
            parameters = {
                selector: 'textarea.myeditor',
                menubar: false,
                plugins: ['advlist lists charmap print preview anchor directionality',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime paste code help wordcount'
                ],
                toolbar: 'undo redo | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | fullscreen preview save | ltr rtl',
                relative_urls: false,
                toolbar_sticky: true,
                autosave_ask_before_unload: true,
                autosave_interval: "30s",
                autosave_prefix: "{path}{query}-{id}-",
                autosave_restore_when_empty: false,
                autosave_retention: "2m",
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tiny.cloud/css/codepen.min.css'
                ],

                importcss_append: true,
                height: 300,
                noneditable_noneditable_class: "mceNonEditable",
                toolbar_drawer: 'sliding',
                contextmenu: "link",
                setup: function (editor) {
                    editor.on('change', function () {
                        tinymce.triggerSave();
                    });
                }
            }
            tinymce.init(parameters);

        }

    
    var handleChangeLang = function () {
        $(document).on('change', '#change-lang', function () {
            var lang_code = $(this).val();
            var action = config.admin_url + '/change_lang';
            $.ajax({
                url: action,
                data: {lang_code: lang_code},
                async: false,
                success: function (data) {
                    console.log(data);
                    if (data.type == 'success') {

                        window.location.reload()

                    }


                },
                error: function (xhr, textStatus, errorThrown) {
                    My.ajax_error_message(xhr);
                },
                dataType: "JSON",
                type: "GET"
            });

            return false;
        });
    }




    return {
        init: function () {
            init();
        },

    }

}();

jQuery(document).ready(function () {
    Main.init();
});


