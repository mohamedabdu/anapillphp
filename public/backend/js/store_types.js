var StoreTypes = function () {
    var StoreTypes_grid;

    var init = function () {
        handleRecords();
        handleSubmit();
    };


    var handleRecords = function () {
        StoreTypes_grid = $('.dataTable').dataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": config.admin_url + "/store_types/data",
                "type": "POST",
                data: {
                    _token: $('input[name="_token"]').val()
                },
            },
            "columns": [
                {
                    "data": "title",
                    "name": "store_type_translations.title"
                },
                {
                    "data": "options",
                    orderable: false,
                    searchable: false
                }
            ],
            "order": [
                [0, "asc"]
            ],
            "oLanguage": {
                "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json'
            }

        });
    }


    var handleSubmit = function () {
        $('#addEditStoreTypesForm').validate({
            rules: {
                
            },
            //messages: lang.messages,
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });


        var langs = JSON.parse(config.languages);
        for (var x = 0; x < langs.length; x++) {
            var ele = "input[name='title[" + langs[x] + "]']";
            $(ele).rules('add', {
                required: true
            });
        }


        $('#addEditStoreTypesForm .submit-form').click(function () {

            if ($('#addEditStoreTypesForm').validate().form()) {
                $('#addEditStoreTypesForm .submit-form').prop('disabled', true);
                $('#addEditStoreTypesForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#addEditStoreTypesForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#addEditStoreTypesForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#addEditStoreTypesForm').validate().form()) {
                    $('#addEditStoreTypesForm .submit-form').prop('disabled', true);
                    $('#addEditStoreTypesForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#addEditStoreTypesForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#addEditStoreTypesForm').submit(function () {
            var id = $('#id').val();
            var action = config.admin_url + '/store_types';
            var formData = new FormData($(this)[0]);
            if (id != 0) {
                formData.append('_method', 'PATCH');
                action = config.admin_url + '/store_types/' + id;
            }
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#addEditStoreTypesForm .submit-form').prop('disabled', false);
                    $('#addEditStoreTypesForm .submit-form').html(lang.save);

                    if (data.type == 'success') {
                        My.toast(data.message);
                        if (id == 0) {
                            StoreTypes.empty();
                        }

                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                if (i.startsWith('title')) {
                                    var key_arr = i.split('.');
                                    var key_text = key_arr[0] + '[' + key_arr[1] + ']';
                                    i = key_text;
                                }
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#addEditStoreTypesForm .submit-form').prop('disabled', false);
                    $('#addEditStoreTypesForm .submit-form').html(lang.save);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });


            return false;

        })
    }

    return {
        init: function () {
            init();
        },
        delete: function (t) {

            var id = $(t).attr("data-id");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/store_types/' + id,
                data: {
                    _method: 'DELETE',
                    _token: $('input[name="_token"]').val()
                },
                success: function (data) {
                    StoreTypes_grid.api().ajax.reload();
                }
            });
        },
        empty: function () {
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            My.emptyForm();
        }
    };

}();
jQuery(document).ready(function () {
    StoreTypes.init();
});