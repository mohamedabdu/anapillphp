var Roles = function () {
    var RolesGrid;

    var init = function () {
        handleRecords();
        handleSubmit();
        $('input[data-toggle="toggle"]').change(function () {
            $('input[data-toggle="toggle"]').valid();
        })
    };

    var handleRecords = function () {

        RolesGrid = $('.dataTable').dataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": config.admin_url + "/roles/data",
                "type": "POST",
                data: { is_company: new_config.is_company,_token: $('input[name="_token"]').val() },
            },
            "columns": [
                {
                    "data": "title",
                    "name" : "role_translations.title"
                }, 
                {
                    "data": "options",
                    orderable: false,
                    searchable: false
                }
            ],
            "order": [
                [0, "asc"]
            ],
            "oLanguage": { "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json' }

        });
    }

    var handleSubmit = function () {


        $('#addEditRolesForm').validate({
            rules: {
                'permissions[]': {
                    required: true,
                    minlength: 1
                }
            },
            //messages: lang.messages,
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        var langs = JSON.parse(config.languages);
        for (var x = 0; x < langs.length; x++) {
            var title = "input[name='title[" + langs[x] + "]']";
            $(title).rules('add', {
                required: true
            });
        }
        
        $('#addEditRolesForm .submit-form').click(function () {

            if ($('#addEditRolesForm').validate().form()) {
                $('#addEditRolesForm .submit-form').prop('disabled', true);
                $('#addEditRolesForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#addEditRolesForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#addEditRolesForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#addEditRolesForm').validate().form()) {
                    $('#addEditRolesForm .submit-form').prop('disabled', true);
                    $('#addEditRolesForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#addEditArticlesForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#addEditRolesForm').submit(function () {
            var id = $('#id').val();
            var action = config.admin_url + '/roles';
            var formData = new FormData($(this)[0]);
            if (id != 0) {
                formData.append('_method', 'PATCH');
                action = config.admin_url + '/roles/' + id;
            }else{
                formData.append('is_company', new_config.is_company);
            }
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#addEditRolesForm .submit-form').prop('disabled', false);
                    $('#addEditRolesForm .submit-form').html(lang.save);

                    if (data.type == 'success') {
                        My.toast(data.message);
                        if (id == 0) {
                            Roles.empty();
                        }
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                if (i.startsWith('title')) {
                                    var key_arr = i.split('.');
                                    var key_text = key_arr[0] + '[' + key_arr[1] + ']';
                                    i = key_text;
                                }
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#addEditRolesForm .submit-form').prop('disabled', false);
                    $('#addEditRolesForm .submit-form').html(lang.save);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;
        })
    }

    return {
        init: function () {
            init();
        },
        delete: function (t) {
            var id = $(t).attr("data-id");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/roles/' + id,
                data: { _method: 'DELETE', _token: $('input[name="_token"]').val() },
                success: function (data) {
                    RolesGrid.api().ajax.reload();
                }
            });

        },
        empty: function () {
            $('input[type="checkbox"]').prop('checked', false).change();
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            My.emptyForm();
        }
    };

}();
jQuery(document).ready(function () {
    Roles.init();
});