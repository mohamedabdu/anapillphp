var Profile = function () {
    var init = function () {
        handleSubmit();
    };
   
    var handleSubmit = function () {

        $('#addEditProfileForm').validate({
            rules: {
                name: {
                    required: true
                },
                mobile: {
                    required: true,
                    number: true
                },
                email: {
                    required: true,
                    email: true,
                },
            },
            //messages: lang.messages,
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });
        $('#addEditProfileForm .submit-form').click(function () {
            if ($('#addEditProfileForm').validate().form()) {
                $('#addEditProfileForm .submit-form').prop('disabled', true);
                $('#addEditProfileForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#addEditProfileForm').submit();
                }, 1000);

            }
            return false;
        });
        $('#addEditProfileForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#addEditProfileForm').validate().form()) {
                    $('#addEditProfileForm .submit-form').prop('disabled', true);
                    $('#addEditProfileForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#addEditProfileForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#addEditProfileForm').submit(function () {
            var formData = new FormData($(this)[0]);
            formData.append('_method', 'PATCH');
            action = config.admin_url + '/profile';

            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#addEditProfileForm .submit-form').prop('disabled', false);
                    $('#addEditProfileForm .submit-form').html(lang.save);
                    if (data.type == 'success')
                    {
                        My.toast(data.message);
                    } else {
                        if (typeof data.errors === 'object') {
                            for (i in data.errors)
                            {
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('#' + i).closest('.form-group').find(".help-block").html(data.errors[i][0]).css('opacity', 1)
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#addEditProfileForm .submit-form').prop('disabled', false);
                    $('#addEditProfileForm .submit-form').html(lang.save);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });

            return false;

        })




    }
    return{
        init: function () {
            init();
        }
    };
}();

$(document).ready(function () {
    Profile.init();
});