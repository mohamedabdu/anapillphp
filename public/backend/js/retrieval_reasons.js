var RetrievalReasons = function () {
    var RetrievalReasons_grid;

    var init = function () {
        handleRecords();
        handleSubmit();
    };


    var handleRecords = function () {
        RetrievalReasons_grid = $('.dataTable').dataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": config.admin_url + "/retrieval_reasons/data",
                "type": "POST",
                data: {
                    _token: $('input[name="_token"]').val()
                },
            },
            "columns": [
                {
                    "data": "title",
                    "name": "retrieval_reason_translations.title"
                },
                {
                    "data": "active",
                    "name": "retrieval_reasons.active",
                    orderable: false,
                    searchable: false
                },
                {
                    "data": "this_order",
                    "name": "retrieval_reasons.this_order"
                },
                {
                    "data": "options",
                    orderable: false,
                    searchable: false
                }
            ],
            "order": [
                [0, "asc"]
            ],
            "oLanguage": {
                "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json'
            }

        });
    }


    var handleSubmit = function () {
        $('#addEditRetrievalReasonsForm').validate({
            rules: {
                active:{
                    required:true
                },
                this_order: {
                    required: true
                },
            },
            //messages: lang.messages,
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });


        var langs = JSON.parse(config.languages);
        for (var x = 0; x < langs.length; x++) {
            var ele = "input[name='title[" + langs[x] + "]']";
            $(ele).rules('add', {
                required: true
            });
        }


        $('#addEditRetrievalReasonsForm .submit-form').click(function () {

            if ($('#addEditRetrievalReasonsForm').validate().form()) {
                $('#addEditRetrievalReasonsForm .submit-form').prop('disabled', true);
                $('#addEditRetrievalReasonsForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#addEditRetrievalReasonsForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#addEditRetrievalReasonsForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#addEditRetrievalReasonsForm').validate().form()) {
                    $('#addEditRetrievalReasonsForm .submit-form').prop('disabled', true);
                    $('#addEditRetrievalReasonsForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#addEditRetrievalReasonsForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#addEditRetrievalReasonsForm').submit(function () {
            var id = $('#id').val();
            var action = config.admin_url + '/retrieval_reasons';
            var formData = new FormData($(this)[0]);
            if (id != 0) {
                formData.append('_method', 'PATCH');
                action = config.admin_url + '/retrieval_reasons/' + id;
            }
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#addEditRetrievalReasonsForm .submit-form').prop('disabled', false);
                    $('#addEditRetrievalReasonsForm .submit-form').html(lang.save);

                    if (data.type == 'success') {
                        My.toast(data.message);
                        if (id == 0) {
                            RetrievalReasons.empty();
                        }

                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                if (i.startsWith('title')) {
                                    var key_arr = i.split('.');
                                    var key_text = key_arr[0] + '[' + key_arr[1] + ']';
                                    i = key_text;
                                }
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#addEditRetrievalReasonsForm .submit-form').prop('disabled', false);
                    $('#addEditRetrievalReasonsForm .submit-form').html(lang.save);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });


            return false;

        })
    }

    return {
        init: function () {
            init();
        },
        delete: function (t) {

            var id = $(t).attr("data-id");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/retrieval_reasons/' + id,
                data: {
                    _method: 'DELETE',
                    _token: $('input[name="_token"]').val()
                },
                success: function (data) {
                    RetrievalReasons_grid.api().ajax.reload();
                }
            });
        },
        changeActive: function (t) {
            var user_id = $(t).data("id");
            $(t).prop('disabled', true);
            $(t).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');

            $.ajax({
                url: config.admin_url + '/retrieval_reasons/status/' + user_id,
                data: {
                    _token: $('input[name="_token"]').val()
                },
                success: function (data) {
                    $(t).prop('disabled', false);
                    if ($(t).hasClass("btn-success")) {
                        $(t).addClass('btn-danger').removeClass('btn-success');
                        $(t).html(lang.deactivated);
                    } else {
                        $(t).addClass('btn-success').removeClass('btn-danger');
                        $(t).html(lang.activated);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
        },
        empty: function () {
            $('#active').find('option').eq(0).prop('selected', true);
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            My.emptyForm();
        }
    };

}();
jQuery(document).ready(function () {
    RetrievalReasons.init();
});