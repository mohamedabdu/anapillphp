var Visits = function () {
var cancelTaskButton;

    var init = function () {
        handleAddTask();
        handleCancelVisit();
        handleCancelTask();
        if ($('#map').length) {
            handleMap();
        }
        handleStopWatch();
    };

    var promise = new Promise(function (resolve, reject) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                latitude = position.coords.latitude;
                longitude = position.coords.longitude;
                latlng = new google.maps.LatLng(latitude, longitude);
                resolve(latlng);
            });
        } else {
            latlng = new google.maps.LatLng('24.7136', '46.6753');
            resolve(latlng);
        }
    });

    var handleStopWatch = function () {
        if (new_config.start_time != '') {
            $('#stopwatch').stopwatch({
                startTime: parseInt(new_config.start_time)
            }).stopwatch('start');
        }
    }

    var geocode = function (latlng) {
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            'location': latlng
        }, function (results, status) {
            console.log(status);
            if (status === 'OK') {
                if (results[0]) {
                    $('#address').html(results[0].formatted_address);
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }

    var handleMap = function () {
        var latlng = new google.maps.LatLng(new_config.lat, new_config.lng);
        geocode(latlng);
        var map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            zoom: 15
        });
        // create marker (https://developers.google.com/maps/documentation/javascript/reference#MarkerOptions)
        var marker = new google.maps.Marker({
            animation: google.maps.Animation.DROP,
            icon: "http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png",
            map: map,
            position: latlng,
            title: new_config.title
        });

        // create info window and add to marker (https://developers.google.com/maps/documentation/javascript/reference#InfoWindowOptions)
        google.maps.event.addListener(marker, 'click', function () {
            var infowindow = new google.maps.InfoWindow();
            infowindow.setContent(new_config.title);
            infowindow.open(map, marker);
        });
    }

    var handleAddTask = function () {
        $('#addTaskForm').validate({
            rules: {
                task: {
                    required: true
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        $('#addTask .submit-form').click(function () {
            if ($('#addTaskForm').validate().form()) {
                $('#addTask .submit-form').prop('disabled', true);
                $('#addTask .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#addTaskForm').submit();
                }, 1000);

            }
            return false;
        });
        $('#addTaskForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#addTaskForm').validate().form()) {
                    $('#addTask .submit-form').prop('disabled', true);
                    $('#addTask .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#addTaskForm').submit();
                    }, 1000);
                }
                return false;
            }
        });


        $('#addTaskForm').submit(function () {
            var action = config.employee_url + '/visits/add_task';
            var formData = new FormData($(this)[0]);
            formData.append('code', new_config.code);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#addTask .submit-form').prop('disabled', false);
                    $('#addTask .submit-form').html('اضافة');
                    if (data.type == 'success') {
                        My.toast(data.data.message);
                        $('#tasks').append(data.data.task)
                        $('#task').find('option:selected').remove();
                        Visits.empty();
                        $('#addTask').modal('hide');
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#addTask .submit-form').prop('disabled', false);
                    $('#addTask .submit-form').html('اضافة');
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })
    }

    var handleCancelVisit = function () {
        $('#cancelVisitForm').validate({
            rules: {
                cancellation_reason: {
                    required: true
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        $('#cancelVisit .submit-form').click(function () {
            if ($('#cancelVisitForm').validate().form()) {
                $('#cancelVisit .submit-form').prop('disabled', true);
                $('#cancelVisit .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#cancelVisitForm').submit();
                }, 1000);

            }
            return false;
        });
        $('#cancelVisitForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#cancelVisitForm').validate().form()) {
                    $('#cancelVisit .submit-form').prop('disabled', true);
                    $('#cancelVisit .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#cancelVisitForm').submit();
                    }, 1000);
                }
                return false;
            }
        });


        $('#cancelVisitForm').submit(function () {
            var action = config.employee_url + '/visits/cancle_visit';
            var formData = new FormData($(this)[0]);
            formData.append('code', new_config.code);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#cancelVisit .submit-form').prop('disabled', false);
                    $('#cancelVisit .submit-form').html('ارسال');
                    if (data.type == 'success') {
                        $('#cancel-visit,#start-visit').hide();
                        $('#cancle-alert').show();
                        My.toast(data.message);
                        Visits.empty();
                        $('#cancelVisit').modal('hide');
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#cancelVisit .submit-form').prop('disabled', false);
                    $('#cancelVisit .submit-form').html('ارسال');
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })
    }
    var handleCancelTask = function () {
        $('#cancelTaskForm').validate({
            rules: {
                cancellation_reason: {
                    required: true
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        $('#cancelTask .submit-form').click(function () {
            if ($('#cancelTaskForm').validate().form()) {
                $('#cancelTask .submit-form').prop('disabled', true);
                $('#cancelTask .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#cancelTaskForm').submit();
                }, 1000);

            }
            return false;
        });
        $('#cancelTaskForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#cancelTaskForm').validate().form()) {
                    $('#cancelTask .submit-form').prop('disabled', true);
                    $('#cancelTask .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#cancelTaskForm').submit();
                    }, 1000);
                }
                return false;
            }
        });


        $('#cancelTaskForm').submit(function () {
            var action = config.employee_url + '/visits/cancel_task';
            var formData = new FormData($(this)[0]);
            formData.append('code', new_config.code);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#cancelTask .submit-form').prop('disabled', false);
                    $('#cancelTask .submit-form').html('ارسال');
                    if (data.type == 'success') {
                        $(cancelTaskButton).closest('.task-actions').find('#start-task').hide();
                        $(cancelTaskButton).hide();
                        $(cancelTaskButton).closest('.task-actions').find('#cancel-message').show()
                        My.toast(data.message);
                        Visits.empty();
                        $('#cancelTask').modal('hide');
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#cancelTask .submit-form').prop('disabled', false);
                    $('#cancelTask .submit-form').html('ارسال');
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })
    }

    return {
        init: function () {
            init();
        },
        empty: function () {
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            $('#task').find('option').eq(0).prop('selected', true);
            My.emptyForm();
        },
        startVisit: function () {
            $('#start-visit').prop('disabled', true);
            $('#start-visit').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
            promise.then(function (latlng) {
                return latlng;
            }).then(function (latlng) {
                var action = config.employee_url + '/visits/start_visit';
                var data = {
                    _token: new_config.token,
                    code: new_config.code,
                    lat: latlng.lat(),
                    lng: latlng.lng()
                };
                $.ajax({
                    url: action,
                    data: data,
                    success: function (data) {
                        if (data.type == 'success') {
                            $('.task-actions,#finish-visit,#addTaskButton').show();
                            $('#start-visit,#cancel-visit').hide();
                            $('#stopwatch').show();
                            $('#stopwatch').stopwatch().stopwatch('start')
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        $('#start-visit').prop('disabled', false);
                        $('#start-visit').html('بدء الزيارة');
                        My.ajax_error_message(xhr);
                    },
                    dataType: "json",
                    type: "POST"
                });
            });
        },
        cancelVisit: function () {
            if (confirm("هل تريد إلغاء الزيارة؟")) {
                Visits.empty();
                My.setModalTitle('#cancelVisit', 'إلغاء الزيارة');
                $('#cancelVisit').modal('show');
            } else {
                return false;
            }
        },
        finishVisit: function (t) {
            if (confirm("هل تريد إنهاء الزيارة؟")) {
                window.location.href = config.employee_url + '/visits/report/' + new_config.code;
            } else {
                return false;
            }
        },
        addTask: function () {
            Visits.empty();
            My.setModalTitle('#addTask', 'اضافة مهمة');
            $('#addTask').modal('show');
        },
        startTask: function (t) {
            $(t).prop('disabled', true);
            $(t).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
            promise.then(function (latlng) {
                return latlng;
            }).then(function (latlng) {
                    var action = config.employee_url + '/visits/start_task';
                    var data = {
                        _token: new_config.token,
                        code: new_config.code,
                        task: $(t).attr('data-task'),
                        lat: latlng.lat(),
                        lng: latlng.lng()
                    };
                    $.ajax({
                        url: action,
                        data: data,
                        success: function (data) {
                            if (data.type == 'success') {
                                window.location.href = $(t).attr('data-href');
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            $(t).prop('disabled', false);
                            $(t).html('بدء');
                            My.ajax_error_message(xhr);
                        },
                        dataType: "json",
                        type: "POST"
                    });
            });
            
        },
        cancelTask: function (t) {
            if (confirm("هل تريد إلغاء المهمة؟")) {
                Visits.empty();
                cancelTaskButton = t;
                My.setModalTitle('#cancelTask', 'إلغاء المهمة');
                $('#process').val($(t).attr('data-task'));
                $('#cancelTask').modal('show');
            } else {
                return false;
            }
        },

    };

}();
jQuery(document).ready(function () {
    Visits.init();
});