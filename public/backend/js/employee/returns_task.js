var Returns = function () {
var counter = 1;
var products, returnReasons, lat, lng;
    var init = function () {
        handleSubmit();
        handleOptionsParsing();
        handleAddProduct();
        handleStopWatch();
    };

    var promise = new Promise(function (resolve, reject) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                latitude = position.coords.latitude;
                longitude = position.coords.longitude;
                latlng = new google.maps.LatLng(latitude, longitude);
                resolve(latlng);
            });
        } else {
            latlng = new google.maps.LatLng('24.7136', '46.6753');
            resolve(latlng);
        }
    });

    var handleStopWatch = function () {
         if (new_config.start_time != '') {
             $('#stopwatch').stopwatch({
                 startTime: parseInt(new_config.start_time)
             }).stopwatch('start');
         }
     }

    var handleOptionsParsing = function () {
        products = JSON.parse(new_config.products);
        returnReasons = JSON.parse(new_config.returnReasons);
    }

    var handleAddProduct = function(){
        $('#add-product').on('click',function(e){
            var productBlock = '<div class="col-xs-12 pt-3 single-product" style="border-top:1px solid #dfdfdf;">'+
                        '<div class="col-xs-12"><button class="btn btn-danger pull-right" type="button" onclick="Returns.removeProduct(this);"><i class="fa fa-remove"></i></button></div>' +
                        '<div class="form-group col-md-6">'+
                            '<label class="control-label">اسم الصنف</label>'+
                            '<select  id="product" class="form-control" name="products['+counter+'][product]">'+
                                '<option value="">اختر</option>';
                                products.forEach(product => {
                                    productBlock += '<option value="' + product.id + '">' + product.title + '</option>';
                                });
            productBlock += '</select>' +
                           ' <span class="help-block"></span>'+
                        '</div>'+

                        '<div class="form-group col-md-6">'+
                            '<label class="control-label"> عدد القطع</label>'+
                            '<input type="number" class="form-control" name="products['+counter+'][pieces_number]">'+ 
                            '<span class="help-block"></span>'+
                       ' </div>'+
                        
                        '<div class="form-group col-md-6">'+
                            '<label class="control-label"> عدد الكراتين</label>'+
                           ' <input type="number" class="form-control" name="products['+counter+'][cartons_number]">'+
                            '<span class="help-block"></span>'+
                        '</div>'+

                       '<div class="form-group col-md-6">'+
                           '<label class="control-label">سبب الإرجاع</label>'+
                            '<select id="reutrn_reason" class="form-control" name="products['+counter+'][reutrn_reason]">'+
                                '<option value="">اختر</option>';
                                 returnReasons.forEach(returnReason => {
                                     productBlock += '<option value="' + returnReason.id + '">' + returnReason.title + '</option>';
                                 });
            productBlock += '</select>' +
                            '<span class="help-block"></span>'+
                        '</div></div>';
            $('#products-container').append(productBlock);
             var productRule = "select[name='products[" + counter + "][product]']";
             var piecesReasonRule = "input[name='products[" + counter + "][pieces_number]']";
             var returnReasonRule = "select[name='products[" + counter + "][reutrn_reason]']";
             $(productRule).rules('add', {
                 required: true
             });
             $(piecesReasonRule).rules('add', {
                 required: true
             });
             $(returnReasonRule).rules('add', {
                 required: true
             });
            counter++;
        })
        return false;
    }

  
    var handleSubmit = function () {
        $('#taskForm').validate({
            rules: {
                bill_number: {
                    required: true
                },
                'products[0][product]': {
                    required: true
                },
                'products[0][pieces_number]': {
                    required: true
                },
                'products[0][reutrn_reason]': {
                    required: true
                },
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        $('#taskForm .submit-form').click(function () {

            if ($('#taskForm').validate().form()) {
                $('#taskForm .submit-form').prop('disabled', true);
                $('#taskForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                promise.then(function (latlng) {
                     lat = latlng.lat();
                     lng = latlng.lng();
                }).then(function () {
                     setTimeout(function () {
                         $('#taskForm').submit();
                     }, 1000);
                });
                
            }
            return false;
        });
        $('#taskForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#taskForm').validate().form()) {
                    $('#taskForm .submit-form').prop('disabled', true);
                    $('#taskForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                      promise.then(function (latlng) {
                          lat = latlng.lat();
                          lng = latlng.lng();
                      }).then(function () {
                          setTimeout(function () {
                              $('#taskForm').submit();
                          }, 1000);
                      });
                }
                return false;
            }
        });



        $('#taskForm').submit(function () {

            var action = config.employee_url + '/visits/returns';
            var formData = new FormData($(this)[0]);
            formData.append('code', new_config.code);
            formData.append('task', new_config.task);
            formData.append('lat', lat);
            formData.append('lng', lng);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#taskForm .submit-form').prop('disabled', false);
                    $('#taskForm .submit-form').html('انهاء المهمة');
                    if (data.type == 'success') {
                       window.location.href = data.message;
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                if (i.startsWith('products')) {
                                    var key_arr = i.split('.');
                                    var key_text = key_arr[0] + '[' + key_arr[1] + ']' + '[' + key_arr[2] + ']';
                                    i = key_text;
                                }
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#taskForm .submit-form').prop('disabled', false);
                    $('#taskForm .submit-form').html('انهاء المهمة');
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })
    }

    return {
        init: function () {
            init();
        },
        removeProduct: function(t){
            $(t).closest('.single-product').remove();
            $(t).remove();
        }
    };

}();
jQuery(document).ready(function () {
    Returns.init();
});