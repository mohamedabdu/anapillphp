var Login = function () {
    var currentTab = 0;
    var button_text;
    var latlng;

    var init = function () {
        getCurrenctLatLng();
        handleSubmit();
        handleTabClick();
        showTab(currentTab);
    };

    var handleWebCam = function () {

        Webcam.set({
            width: 490,
            height: 390,
            image_format: 'jpeg',
            jpeg_quality: 90
        });
        Webcam.attach('#my_camera');

        $('#take_snapshot').on('click', function () {
            Webcam.snap(function (data_uri) {
                $(".image-tag").val(data_uri);
                $('.resulted_image').html('<img src="' + data_uri + '"/>');
            });

        });

    }

    var getCurrenctLatLng = function(){
        
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                latitude = position.coords.latitude;
                longitude = position.coords.longitude;
                latlng = new google.maps.LatLng(latitude, longitude);
                geocode(latlng);

            });
        } else {
            latlng = new google.maps.LatLng('24.7136', '46.6753');
        }
        
    }

    var geocode = function (latlng) {
        
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            'location': latlng
        }, function (results, status) {
            console.log(status);
            if (status === 'OK') {
                if (results[0]) {
                    $('#lat').val(latlng.lat());
                    $('#lng').val(latlng.lng());
                    $('#address').html(results[0].formatted_address);
                    console.log(results[0].formatted_address);
                    console.log(results[0]);
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }

    var handleTabClick = function () {
        $(".step").on('click', function () {
            return false;
        })
    }


    var showTab = function (n) {
        // This function will display the specified tab of the form ...
        var tabs = $(".tab-pane");
        $(tabs[n]).show();
        //$(x[n]).css('display', 'block');

        if (n == 0) {
            $('.button-previous').hide();
            //$('.button-previous').css('display', 'none');
        } else {
            $('.button-previous').show();
            //$('.button-previous').css('display', 'inline');
        }
        if (n == (tabs.length - 1)) {
            $('.button-next').html('بدء الدوام');
        } else {
            $('.button-next').html('استمرار');
        }
        // ... and run a function that displays the correct step indicator:
        fixStepIndicator(n)
    }

    var fixStepIndicator = function (n) {
        // This function removes the "active" class of all steps...
        var steps = $(".step");

        $(".step").removeClass('active');
        $('.steps li').removeClass('active');

        $(steps[n - 1]).closest('li').addClass('done');
        $(steps[n]).closest('li').addClass('active');
        $(steps[n]).addClass('active');

        //progrss bar
        var i = $('.steps').find("li").length,
            a = n + 1,
            o = a / i * 100;
        $("#loginForm").find(".progress-bar").css({
            width: o + "%"
        })
    }

    var showUserData = function () {
        var tabs = $('.tab-pane');
        $("#tab" + tabs.length + " .form-control-static", $('#loginForm')).each(function () {
            var e = $('[name="' + $(this).attr("data-display") + '"]', $('#loginForm'));
            $(this).html(e.val());
        })
    }

    var handleSubmit = function () {

        $('#loginForm').validate({
            rules: {
                mobile: {
                    required: true
                },
                password: {
                    required: true
                },
                image: {
                    required: true
                },
                lat: {
                    required: true
                },
                lng: {
                    required: true
                },

            },
            messages: {
                mobile: {
                    required: 'هذا الحقل مطلوب'
                },
                password: {
                    required: 'هذا الحقل مطلوب'
                },
                image: {
                    required: 'هذا الحقل مطلوب'
                },
                lat: {
                    required: 'قم بالسماح للموقع بالتوصل إلي موقعك'
                },
                lng: {
                    required: 'قم بالسماح للموقع بالتوصل إلي موقعك'
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);
            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });


        $('#loginForm').submit(function () {

            var action = config.url + '/employee/login';
            var formData = new FormData($(this)[0]);
            formData.append('step', currentTab + 1);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#loginForm .button-next').prop('disabled', false);
                    $('#loginForm .button-next').html(button_text);

                    if (data.type == 'success') {
                        if (currentTab == 0 && data.data !== undefined) {
                            window.location.href = data.data.redirect;
                        }
                        if (currentTab + 1 == $('.tab-pane').length) {
                            window.location.href = data.message;
                        }
                        var hideTab = currentTab;
                        currentTab = currentTab + 1;
                        $('.tab-pane:eq(' + hideTab + ')').hide();
                        $('.alert-danger').hide();
                        showTab(currentTab);
                        if (currentTab == 1) {
                            $('#name').html(data.message);
                        }
                        if (currentTab == 2) {
                            handleWebCam();
                        } else {
                            Webcam.reset();
                        }

                        if (currentTab == 3) {
                            $('#date').html(data.message);
                        }

                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }else{
                            $('.alert-danger').show();
                            $('.alert-danger span').html(data.message);
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#loginForm .button-next').prop('disabled', false);
                    $('#loginForm .button-next').html(button_text);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });

            return false;

        })




    }

    return {
        init: function () {
            init();
        },
        nextPrev: function (n) {
            
            // This function will figure out which tab to display
            button_text = 'استمرار';
            var tabs = $(".tab-pane");
            var steps = $(".step");


            if (n == 1 && currentTab != 1) {
                if (!$('#loginForm').valid()) return false;
                $('#loginForm .button-next').prop('disabled', true);
                $('#loginForm .button-next').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                if (currentTab + 1 == tabs.length) {
                    button_text = 'تأكيد الحضور';
                }
                setTimeout(function () {
                    $('#loginForm').submit();
                }, 1000);
                return false;
            } else {
                // Hide the current tab:
                $(tabs[currentTab]).hide();
                // Increase or decrease the current tab by 1:
                currentTab = currentTab + n;
                if (n == -1) {
                    $(steps[currentTab]).closest('li').removeClass('done');
                }
                // Otherwise, display the correct tab:
                if (currentTab == 2) {
                    handleWebCam();
                } else {
                    Webcam.reset();
                }
                showTab(currentTab);
            }

        }
    };

}();

jQuery(document).ready(function () {
    Login.init();
});