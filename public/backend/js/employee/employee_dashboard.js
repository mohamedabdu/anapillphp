var EmployeeDashboard = function () {
    var scheduleVisits,normalVisits;
    var init = function () {
        handleParsing();
        getCurrentLatLng();
    };

    var handleParsing = function (){
        scheduleVisits = JSON.parse(new_config.scheduleVisits);
        normalVisits = JSON.parse(new_config.normalVisits);
       
    }

    var handleMap = function (latlng) {
        var scheduledTasksMap = new google.maps.Map(document.getElementById('scheduledTasksMap'), {
            center: latlng,
            zoom: 10
        });
       
        if (scheduleVisits.length > 0) {
            handleMarkers(scheduledTasksMap, scheduleVisits);
        }

        var normalTasksMap = new google.maps.Map(document.getElementById('normalTasksMap'), {
            center: latlng,
            zoom: 10
        });
        if (normalVisits.length > 0) {
            handleMarkers(normalTasksMap, normalVisits);
        }
    }


    var handleMarkers = function(map,visits){
        var bounds = new google.maps.LatLngBounds();
        
        // loop through locations and add to map
        for (var i = 0; i < visits.length; i++) {
            // get current location
            var location = visits[i];
            
            
            // create map position
            var position = new google.maps.LatLng(location.lat, location.lng);

            // add position to bounds
            bounds.extend(position);

            // create marker (https://developers.google.com/maps/documentation/javascript/reference#MarkerOptions)
            var marker = new google.maps.Marker({
                animation: google.maps.Animation.DROP,
                icon: "http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png",
                map: map,
                position: position,
                title: location.store
            });

            // create info window and add to marker (https://developers.google.com/maps/documentation/javascript/reference#InfoWindowOptions)
            google.maps.event.addListener(marker, 'click', (
                function (marker, i) {
                    return function () {
                        var infowindow = new google.maps.InfoWindow();
                        infowindow.setContent(visits[i].store);
                        infowindow.open(map, marker);
                    }
                }
            )(marker, i));
        };

        // fit map to bounds
        map.fitBounds(bounds);
    }


   var getCurrentLatLng = function(){
     
       if (navigator.geolocation) {
           navigator.geolocation.getCurrentPosition(function (position) {
               var latitude = position.coords.latitude;
               var longitude = position.coords.longitude;
               latlng = new google.maps.LatLng(latitude, longitude);
               handleMap(latlng);
           });
       } else {
            
           latlng = new google.maps.LatLng('24.7136', '46.6753');
           handleMap(latlng);
       }
   }
    return {
        init: function () {
            init();
        }
    };

}();

jQuery(document).ready(function () {
    EmployeeDashboard.init();
});