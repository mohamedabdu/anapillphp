var CheckPromotions = function () {
    var lat, lng;
    var init = function () {
        handleSubmit();
        handleFinish();
        handleStopWatch();
        $(".counter").TouchSpin();
        
    };

    var promise = new Promise(function (resolve, reject) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                latitude = position.coords.latitude;
                longitude = position.coords.longitude;
                latlng = new google.maps.LatLng(latitude, longitude);
                resolve(latlng);
            });
        } else {
            latlng = new google.maps.LatLng('24.7136', '46.6753');
            resolve(latlng);
        }
    });

    var handleStopWatch = function () {
        if (new_config.start_time != '') {
            $('#stopwatch').stopwatch({
                startTime: parseInt(new_config.start_time)
            }).stopwatch('start');
        }
    }
     var handleSubmit = function () {
         $('#taskForm').validate({
             ignore:"",
             rules: {
                promo: {
                    required: true
                },
                promo_count: {
                    required: true
                },
                 image: {
                    required: true
                }
             },
             highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

             },
             unhighlight: function (element) {
                 $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                 $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

             },
             errorPlacement: function (error, element) {
                 $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
             }
         });

         $('#taskForm .submit-form').click(function () {
             if ($('#taskForm').validate().form()) {
                 $('#taskForm .submit-form').prop('disabled', true);
                 $('#taskForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                 setTimeout(function () {
                     $('#taskForm').submit();
                 }, 1000);
             }
             return false;
         });
         $('#taskForm input').keypress(function (e) {
             if (e.which == 13) {
                 if ($('#taskForm').validate().form()) {
                     $('#taskForm .submit-form').prop('disabled', true);
                     $('#taskForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                     setTimeout(function () {
                         $('#taskForm').submit();
                     }, 1000);
                 }
                 return false;
             }
         });

         $('#taskForm').submit(function () {
             var action = config.employee_url + '/visits/check_promotions';
             var formData = new FormData($(this)[0]);
             formData.append('code', new_config.code);
             formData.append('task', new_config.task);
             $.ajax({
                 url: action,
                 data: formData,
                 async: false,
                 cache: false,
                 contentType: false,
                 processData: false,
                 success: function (data) {
                     $('#taskForm .submit-form').prop('disabled', false);
                     $('#taskForm .submit-form').html('اضافة');

                     if (data.type == 'success') {
                         My.toast(data.data.message);
                         $('#promo').find(":selected").remove();
                         CheckPromotions.empty();
                     } else {
                         if (typeof data.errors !== 'undefined') {
                             for (i in data.errors) {
                                 var message = data.errors[i];
                                 $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                 $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                             }
                         }
                     }
                 },
                 error: function (xhr, textStatus, errorThrown) {
                     $('#taskForm .submit-form').prop('disabled', false);
                     $('#taskForm .submit-form').html('اضافة');
                     My.ajax_error_message(xhr);
                 },
                 dataType: "json",
                 type: "POST"
             });
             return false;

         })
     }

    var handleFinish = function () {
        $('#finshTaskForm').validate({
            rules: {

            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        $('#finshTask .submit-form').click(function () {
            if ($('#finshTaskForm').validate().form()) {
                $('#finshTask .submit-form').prop('disabled', true);
                $('#finshTask .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                promise.then(function (latlng) {
                    lat = latlng.lat();
                    lng = latlng.lng();
                }).then(function () {
                    setTimeout(function () {
                        $('#finshTaskForm').submit();
                    }, 1000);
                });

            }
            return false;
        });
        $('#finshTask input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#finshTaskForm').validate().form()) {
                    $('#finshTask .submit-form').prop('disabled', true);
                    $('#finshTask .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    promise.then(function (latlng) {
                       lat = latlng.lat();
                       lng = latlng.lng();
                    }).then(function () {
                       setTimeout(function () {
                           $('#finshTaskForm').submit();
                       }, 1000);
                    });
                }
                return false;
            }
        });


        $('#finshTaskForm').submit(function () {

            var action = config.employee_url + '/visits/finish_task';
            var formData = new FormData($(this)[0]);
            formData.append('code', new_config.code);
            formData.append('task', new_config.task);
            formData.append('lat', lat);
            formData.append('lng', lng);

            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#finshTask .submit-form').prop('disabled', false);
                    $('#finshTask .submit-form').html('انهاء المهمة');
                    if (data.type == 'success') {
                        window.location.href = data.message;
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#finshTask .submit-form').prop('disabled', false);
                    $('#finshTask .submit-form').html('انهاء المهمة');
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })
    }

    return {
        init: function () {
            init();
        },
        fininshTask: function () {
            My.setModalTitle('#finshTask', 'انهاء المهمة');
            $('#finshTask').modal('show');
        },
        empty: function () {
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            $('#promo').find('option').eq(0).prop('selected', true);
            $('.image_box').html('<div class="snap-container"></div><img src="' + config.url + '/no-image.png" style="width:100%" height="400" class="image snap-camera-image" onclick="My.takeSnapShot(this)" />');
            $('#image').val('');
            My.emptyForm();
        }
    };

}();
jQuery(document).ready(function () {
    CheckPromotions.init();
});