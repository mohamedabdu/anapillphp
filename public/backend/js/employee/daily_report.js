var DailyReport = function () {

    var init = function () {
        handleSubmit();

        $(".completed").knob({
            readOnly: true,
            draw: function () {
                $(this.i).val(this.cv + '%');
            }
        });
    };

    var handleSubmit = function () {

        $('#finishAttendanceForm').validate({
            rules: {
                termination_reason: {
                    required: true
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        $('#finishAttendance .submit-form').click(function () {
            if ($('#finishAttendanceForm').validate().form()) {
                $('#finishAttendance .submit-form').prop('disabled', true);
                $('#finishAttendance .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#finishAttendanceForm').submit();
                }, 1000);

            }
            return false;
        });
        $('#finishAttendanceForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#finishAttendanceForm').validate().form()) {
                    $('#finishAttendance .submit-form').prop('disabled', true);
                    $('#finishAttendance .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#finishAttendanceForm').submit();
                    }, 1000);
                }
                return false;
            }
        });


        $('#finishAttendanceForm').submit(function () {
            var action = config.employee_url + '/logout';
            var formData = new FormData($(this)[0]);
            var completion = new_config.completion == 100 ? true : false;
            formData.append('completion', completion);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.type == 'success') {
                        window.location.href = data.message;
                    } else {
                        $('#finishAttendance .submit-form').prop('disabled', false);
                        $('#finishAttendance .submit-form').html('تأكيد');
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#finishAttendance .submit-form').prop('disabled', false);
                    $('#finishAttendance .submit-form').html('ارسال');
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })
    }

    return {
        init: function () {
            init();
        },
        finishAttendance: function () {
            if (new_config.completion == 100) {
               $('#finish-attendance-message').html('هل انت متأكد انك تريد إنهاء الدوام ؟')
            } else {
                $('#finish-attendance-message').html('هل انت متأكد انك تريد إنهاء الدوام قبل إنهاء زياراتك اليوم؟ من فضلك اذكر السبب')
                $('#termination-reason').show();
            }
            My.setModalTitle('#finishAttendance', 'إنهاء الدوام');
            $('#finishAttendance').modal('show');
        }
    };

}();
jQuery(document).ready(function () {
    DailyReport.init();
});