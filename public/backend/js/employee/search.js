var Search = function () {
var page;
var actionSource, actionType;

    var init = function () {
        handleSubmitClick();
        handleLoadmore();
        $('.picker').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });
        handleSubmit();
        handleScroll();
        
    };

     var handleScroll = function () {
         $(window).on("scroll", function () {
             if ($(window).scrollTop() + $(window).height() > $(document).height() - 1000) {
                 if ($('#load-more-button').css('display') == 'block') {
                     $('#load-more-button').trigger("click");
                 }
             }
         })
     }

     var handleLoadmore = function () {
         
         $('#load-more-button').on('click', function () {
            actionSource = '#load-more-button';
            actionType = 'loadmore';
            $(actionSource).prop('disabled', true);
            $(actionSource).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
            setTimeout(function () {
                page++;
                $('#filterForm').submit();
            }, 1000);
            
         })
     }

     var handleSubmitClick = function () {
         $('#filterForm .submit-form').on('click', function () {
            actionSource = '#filterForm .submit-form';
            actionType = 'submit';
            $(actionSource).prop('disabled', true);
            $(actionSource).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
            setTimeout(function () {
                page = 1;
                $('#filterForm').submit();
            }, 1000);
         })
     }

    var handleSubmit = function () {

        $('#filterForm').submit(function () {
            var action = config.employee_url + '/search';
            var formData = new FormData($(this)[0]);
            formData.append('page',page);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $(actionSource).prop('disabled', false);
                    if (actionType == 'loadmore') {
                        $(actionSource).html('المزيد');
                    }else{
                        $(actionSource).html('بحث');
                    }
                    if (data.type == 'success') {
                        if (data.data.visits != '') {
                            $('#visits-list').show();
                            $('#no-results').hide();
                            if (actionType == 'submit') {
                                $('#visits-list').html(data.data.visits);
                                 if (data.data.count == new_config.limit) {
                                    $('#load-more-button').show();
                                }
                            }else{
                                $('#visits-list').append(data.data.visits);
                                if (data.data.count < new_config.limit) {
                                    $('#load-more-button').hide();
                                }
                            }
                        }else{
                            if (actionType == 'submit') {
                                $('#visits-list').hide();
                                $('#no-results').show();
                            }else{
                                $('#load-more-button').hide();
                            }
                            
                        }
                       
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $(actionSource).prop('disabled', false);
                    if (actionType == 'loadmore') {
                        $(actionSource).html('المزيد');
                    } else {
                        $(actionSource).html('بحث');
                    }
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })
    }

    return {
        init: function () {
            init();
        }
    };

}();
jQuery(document).ready(function () {
    Search.init();
});