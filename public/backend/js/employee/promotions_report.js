var PromotionsReport = function () {
var promos,
    counter = 1;

    var init = function () {
        handleSubmit();
        handleAddPromo();
        handleOptionsParsing();
       
    };

     var handleOptionsParsing = function () {
        promos = JSON.parse(new_config.promos);
     }

    var handleAddPromo = function () {
        $('#add-promo').on('click',function(e){
            var promoBlock = 
                    '<div class="single-promo pt-3" style="border-top:1px solid #dfdfdf;">'+
                        '<div class="col-xs-12"><button class="btn btn-danger pull-right" type="button" onclick="PromotionsReport.removePromo(this);"><i class="fa fa-remove"></i></button></div>' +

                        '<div class="form-group col-md-6">'+
                            '<label class="control-label">نوع العرض</label>' +
                            '<select class="form-control" name="promos['+counter+'][promo]">'+
                                '<option value="">اختر</option>';
                                promos.forEach(promo => {
                                    promoBlock += '<option value="' + promo.id + '">' + promo.title + '</option>';
                                });
            promoBlock += '</select>' +
                           ' <span class="help-block"></span>'+
                        '</div>'+

                        '<div class="form-group col-md-6">'+
                            '<label class="control-label"> العدد</label>' +
                            '<input type="text" class="form-control" name="promos[' + counter + '][promo_count]">' +
                            '<span class="help-block"></span>'+
                       '</div>'+

                        '<div class="form-group col-md-12">'+
                            '<label class="control-label"> ملاحظات </label>'+
                           ' <textarea type="text" rows="7" class="form-control" name="promos[' + counter + '][notes]"></textarea>'+
                       ' </div>'+
                       ' <div class="clearfix"></div>'+

                        '<div class="form-group col-md-4">'+
                            '<label class="control-label"> صورة العرض </label>'+
                            '<div class="promos_' + counter + '_image_box">'+
                                '<div class="snap-container"></div>'+
                                '<img src="' + config.url + '/no-image.png" style="width:100%" height="400" class="promos_' + counter + '_image snap-camera-image" onclick="My.takeSnapShot(this)" />'+
                            '</div>'+
                           '<input type="hidden" name="promos[' + counter + '][image]" id="promos_' + counter + '_image">'+
                           '<span class="help-block"></span>'+
                           '<input type=button value="التقط صورتك" class="take_snapshot_promos_' + counter + '_image" style="display:none;">'+
                       '</div>'+

                    '</div>'+

                    '<div class="clearfix"></div>';
                           
            $('#promos-container').append(promoBlock);
             var promoRule = "select[name='promos[" + counter + "][promo]']";
             var promoCountRule = "input[name='promos[" + counter + "][promo_count]']";
             var imageRule = "input[name='promos[" + counter + "][image]']";
            
             $(promoRule).rules('add', {
                 required: true
             });
             $(promoCountRule).rules('add', {
                 required: true
             });
             $(imageRule).rules('add', {
                required: true
             });
            counter++;
        })
        return false;
    }
    var handleSubmit = function () {
        $('#reportForm').validate({
            ignore:"",
            rules: {
                store: {
                    required: true
                },
                competitor: {
                     required: true
                },
                'promos[0][promo]': {
                    required: true
                },
                'promos[0][promo_count]': {
                    required: true
                },
                'promos[0][image]': {
                    required: true
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        $('#reportForm .submit-form').click(function () {

            if ($('#reportForm').validate().form()) {
                $('#reportForm .submit-form').prop('disabled', true);
                $('#reportForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#reportForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#reportForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#reportForm').validate().form()) {
                    $('#reportForm .submit-form').prop('disabled', true);
                    $('#reportForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#reportForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#reportForm').submit(function () {
            var action = config.employee_url + '/reports/promotions_report';
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    $('#reportForm .submit-form').prop('disabled', false);
                    $('#reportForm .submit-form').html('ارسال');
                    if (data.type == 'success') {
                        My.toast(data.message);
                        PromotionsReport.empty();
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                if (i.startsWith('promos')) {
                                    var key_arr = i.split('.');
                                    var key_text = key_arr[0] + '[' + key_arr[1] + ']' + '[' + key_arr[2] + ']';
                                    i = key_text;
                                }
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#reportForm .submit-form').prop('disabled', false);
                    $('#reportForm .submit-form').html('ارسال');
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })
    }

    return {
        init: function () {
            init();
        },
        empty: function () {
            $('#store').find('option').eq(0).prop('selected', true);
            $('#competitor').find('option').eq(0).prop('selected', true);
            $('.promos').find('option').eq(0).prop('selected', true);
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            for (let i = 0; i < 3; i++) {
                $('.image_' + i + '_box').html('<div class="snap-container"></div><img src="' + config.url + '/no-image.png"  style="width:100%" height="400" class="image_' + i + ' snap-camera-image" onclick="My.takeSnapShot(this)" />');
                $('#image_' + i).val('');
            }
            $('.promos_0_image_box').html('<div class="snap-container"></div><img src="' + config.url + '/no-image.png"  style="width:100%" height="400" class="promos_0_image snap-camera-image" onclick="My.takeSnapShot(this)" />');
            $('#promos_0_image').val('');
            $('#promos-container').html('');
            My.emptyForm();
        },
        removePromo: function (t) {
            $(t).closest('.single-promo').remove();
            $(t).remove();
        }
    };

}();
jQuery(document).ready(function () {
    PromotionsReport.init();
});