var VisitReport = function () {
    var lat, lng;

    var init = function () {
        handlePercentage();
        handleSubmit();
        geocode(new google.maps.LatLng(new_config.start_lat, new_config.start_lng), 'start-address');
        if (new_config.end_lat == '' && new_config.end_lng == '') {
            getCurrenctLatLng();
        }else{
            geocode(new google.maps.LatLng(new_config.end_lat, new_config.end_lng), 'end-address');
        }
    };
    
    var handlePercentage = function () {
         $(".completed").knob({
             readOnly: true,
             draw: function () {
                 $(this.i).val(this.cv + '%');
             }
         });
    }
    var getCurrenctLatLng = function () {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                latitude = position.coords.latitude;
                longitude = position.coords.longitude;
                latlng = new google.maps.LatLng(latitude, longitude);
                geocode(latlng, 'end-address', true);

            });
        } else {
            latlng = new google.maps.LatLng('24.7136', '46.6753');
        }

    }

    var geocode = function (latlng, container, getCurrentPosition = false) {
        geocoder = new google.maps.Geocoder();
        if (getCurrentPosition == true) {
            lat = latlng.lat();
            lng = latlng.lng();
        }
        geocoder.geocode({
            'location': latlng
        }, function (results, status) {
            console.log(status);
            if (status === 'OK') {
                if (results[0]) {
                    $('#'+container+'').html(results[0].formatted_address);
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }
   

    var handleSubmit = function () {
        $('#finishVisitForm').validate({
            rules: {
                termination_reason: {
                    required: true
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        $('#finishVisit .submit-form').click(function () {
            if ($('#finishVisitForm').validate().form()) {
                $('#finishVisit .submit-form').prop('disabled', true);
                $('#finishVisit .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#finishVisitForm').submit();
                }, 1000);

            }
            return false;
        });
        $('#finishVisitForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#finishVisitForm').validate().form()) {
                    $('#finishVisit .submit-form').prop('disabled', true);
                    $('#finishVisit .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#finishVisitForm').submit();
                    }, 1000);
                }
                return false;
            }
        });


        $('#finishVisitForm').submit(function () {
            var action = config.employee_url + '/visits/finish_visit';
            var formData = new FormData($(this)[0]);
            var completed = new_config.unfinished_count != 0 ? false : true;
            formData.append('code', new_config.code);
            formData.append('end_date', new_config.end_date);
            formData.append('end_time', new_config.end_time);
            formData.append('end_lat', lat);
            formData.append('end_lng', lng);
            formData.append('completed', completed);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#finishVisit .submit-form').prop('disabled', false);
                    $('#finishVisit .submit-form').html('انهاء');
                    if (data.type == 'success') {
                        //$('#finishVisit').modal('hide');
                        window.location.href = data.message;
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#finishVisit .submit-form').prop('disabled', false);
                    $('#finishVisit .submit-form').html('انهاء');
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })
    }

   
   

    return {
        init: function () {
            init();
        },
        empty: function () {
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            My.emptyForm();
        },
        finishVisit: function () {
             if (new_config.unfinished_count != 0) {
                if (confirm("هل تريد إنهاء الزيارة بدون إكمال المهمات؟")) {
                    My.setModalTitle('#finishVisit', 'إنهاء الزيارة');
                    $('#finishVisit').modal('show');
                } else {
                    return false;
                }
            }else{
                $('#finishVisit .submit-form').click().trigger();
            }
            
        },
    };

}();
jQuery(document).ready(function () {
    VisitReport.init();
});