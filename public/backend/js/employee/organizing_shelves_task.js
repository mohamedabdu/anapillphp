var OrganizingShelves = function () {
var lat, lng;
    var init = function () {
        handleSubmit();
        handleFinish();
        handleStopWatch();
    };
   var handleStopWatch = function () {
       if (new_config.start_time != '') {
           $('#stopwatch').stopwatch({
               startTime: parseInt(new_config.start_time)
           }).stopwatch('start');
       }
   }

   var promise = new Promise(function (resolve, reject) {
       if (navigator.geolocation) {
           navigator.geolocation.getCurrentPosition(function (position) {
               latitude = position.coords.latitude;
               longitude = position.coords.longitude;
               latlng = new google.maps.LatLng(latitude, longitude);
               resolve(latlng);
           });
       } else {
           latlng = new google.maps.LatLng('24.7136', '46.6753');
           resolve(latlng);
       }
   });


    var handleSubmit = function () {
        $('#taskForm').validate({
            ignore:"",
            rules: {
                image_before: {
                    required: true
                },
                image_after: {
                    required: true
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        $('#taskForm .submit-form').click(function () {

            if ($('#taskForm').validate().form()) {
                $('#taskForm .submit-form').prop('disabled', true);
                $('#taskForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#taskForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#taskForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#taskForm').validate().form()) {
                    $('#taskForm .submit-form').prop('disabled', true);
                    $('#taskForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#taskForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#taskForm').submit(function () {
          
            var action = config.employee_url + '/visits/organizing_shelves';
            var formData = new FormData($(this)[0]);
            formData.append('code', new_config.code);
            
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#taskForm .submit-form').prop('disabled', false);
                    $('#taskForm .submit-form').html('اضافة');

                    if (data.type == 'success') {
                        My.toast(data.data.message);
                        OrganizingShelves.empty();
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#taskForm .submit-form').prop('disabled', false);
                    $('#taskForm .submit-form').html('اضافة');
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })
    }
    var handleFinish = function () {
        $('#finshTaskForm').validate({
            rules: {
              
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        $('#finshTask .submit-form').click(function () {
            if ($('#finshTaskForm').validate().form()) {
                $('#finshTask .submit-form').prop('disabled', true);
                $('#finshTask .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                promise.then(function (latlng) {
                    lat = latlng.lat();
                    lng = latlng.lng();
                }).then(function () {
                    setTimeout(function () {
                        $('#finshTaskForm').submit();
                    }, 1000);
                });
            }
            return false;
        });
        $('#finshTaskForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#finshTaskForm').validate().form()) {
                    $('#finshTask .submit-form').prop('disabled', true);
                    $('#finshTask .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    promise.then(function (latlng) {
                        lat = latlng.lat();
                        lng = latlng.lng();
                    }).then(function () {
                        setTimeout(function () {
                            $('#finshTaskForm').submit();
                        }, 1000);
                    });
                }
                return false;
            }
        });


        $('#finshTaskForm').submit(function () {
            var action = config.employee_url + '/visits/finish_task';
            var formData = new FormData($(this)[0]);
            formData.append('notes', $('#finshTaskForm textarea[name="notes"]').val());
            formData.append('code', new_config.code);
            formData.append('task', new_config.task);
            formData.append('lat', lat);
            formData.append('lng', lng);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    $('#finshTask .submit-form').prop('disabled', false);
                    $('#finshTask .submit-form').html('انهاء المهمة');
                    if (data.type == 'success') {
                        window.location.href = data.message;
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#finshTask .submit-form').prop('disabled', false);
                    $('#finshTask .submit-form').html('انهاء المهمة');
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })
    }

    return {
        init: function () {
            init();
        },
        fininshTask: function () {
            My.setModalTitle('#finshTask', 'انهاء المهمة');
            $('#finshTask').modal('show');
        },
        empty: function () {
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            $('.image_before_box').html('<div class="snap-container"></div><img src="' + config.url + '/no-image.png" height="400" style="width:100%;" class="image_before snap-camera-image" onclick="My.takeSnapShot(this)" />');
            $('#image_before').val('');
            $('.image_after_box').html('<div class="snap-container"></div><img src="' + config.url + '/no-image.png" height="400"  style="width:100%;" class="image_after snap-camera-image" onclick="My.takeSnapShot(this)" />');
            $('#image_after').val('');
            My.emptyForm();
        }
    };

}();
jQuery(document).ready(function () {
    OrganizingShelves.init();
});