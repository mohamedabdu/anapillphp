var AreasReport = function () {
var storeCategories = [], counter = 1;

    var init = function () {
        handleSubmit();
        handleAddCategory();
        handleStoreChange();
    };

     var handleStoreChange = function () {
         $('#store').on('change', function () {
             var store = $(this).val();
             $('.store-categories').html("");
             $('.store-categories').html('<option selected value=""> اختر </option>');
             var data = {
                _token: new_config.token,
                store: store
             };
             if (store) {
                 $.ajax({
                     url: config.employee_url + '/store_categories',
                     data: data,
                     type: "POST",
                     dataType: "json",
                     success: function (data) {
                         var storeCategoriesOptions = "";
                         storeCategories = data.data.store_categories;
                         if (storeCategories.length != 0) {
                             for (var x = 0; x < storeCategories.length; x++) {
                                 var item = storeCategories[x];
                                 storeCategoriesOptions += '<option value="' + item.id + '">' + item.title + '</option>';
                             }
                             $('.store-categories').append(storeCategoriesOptions);
                         }
                     },
                     error: function (xhr, textStatus, errorThrown) {
                         My.ajax_error_message(xhr);
                     }

                 });
             }

         });
     }
    var handleAddCategory = function(){
        $('#add-category').on('click', function (e) {
            var categoryBlock = 
                    '<div class="single-category pt-3" style="border-top:1px solid #dfdfdf;">'+
                        '<div class="col-xs-12"><button class="btn btn-danger pull-right" type="button" onclick="AreasReport.removeCategory(this);"><i class="fa fa-remove"></i></button></div>' +

                        '<div class="form-group col-md-6">'+
                            '<label class="control-label">التصنيف</label>'+
                            '<select class="form-control store-categories" name="categories[' + counter + '][category]">' +
                                '<option value="">اختر</option>';
                                storeCategories.forEach(category => {
                                    categoryBlock += '<option value="' + category.id + '">' + category.title + '</option>';
                                });
            categoryBlock += '</select>' +
                           ' <span class="help-block"></span>'+
                        '</div>'+

                        '<div class="clearfix"></div>'+

                        '<div class="col-md-6">'+
                            '<label class="control-label"> مساحة المنافس</label>'+
                            '<div class="clearfix"></div>'+
                            '<div class="form-group col-md-3">'+
                                '<input type="number" placeholder="العرض بالسم" class="form-control" name="categories[' + counter + '][width_of_competitor]">' +
                                '<span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group col-md-3">'+
                               '<input type="number" placeholder="الطول (عدد الشلفات)" class="form-control" name="categories[' + counter + '][height_of_competitor]">' +
                               ' <span class="help-block"></span>'+
                            '</div>'+
                        '</div>'+

                        '<div class="col-md-6">'+
                            '<label class="control-label"> مساحة الشركة</label>'+
                            '<div class="clearfix"></div>'+
                            '<div class="form-group col-md-3">'+
                               '<input type="number" placeholder="العرض بالسم" class="form-control" name="categories[' + counter + '][width_of_organization]">' +
                               ' <span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group col-md-3">'+
                               ' <input type="number" placeholder="الطول (عدد الشلفات)" class="form-control" name="categories[' + counter + '][height_of_organization]">' +
                                '<span class="help-block"></span>'+
                            '</div>'+
                        '</div>'+  

                    '</div>'+

                    '<div class="clearfix"></div>';

            $('#categories-container').append(categoryBlock);

             var categoryRule = "select[name='categories[" + counter + "][category]']";
             var widthOfCompetitorRule = "input[name='categories[" + counter + "][width_of_competitor]']";
             var heightOfCompetitorRule = "input[name='categories[" + counter + "][height_of_competitor]']";
             var widthOfOrganizationRule = "input[name='categories[" + counter + "][width_of_organization]']";
             var heightOfOrganizationRule = "input[name='categories[" + counter + "][height_of_organization]']";
           
             $(categoryRule).rules('add', {
                 required: true
             });
             $(widthOfCompetitorRule).rules('add', {
                 required: true
             });
             $(heightOfCompetitorRule).rules('add', {
                 required: true
             });
             $(widthOfOrganizationRule).rules('add', {
                 required: true
             });
             $(heightOfOrganizationRule).rules('add', {
                 required: true
             });
            
            counter++;
        })
        return false;
    }

    var handleSubmit = function () {
        $('#reportForm').validate({
            rules: {
                store: {
                    required: true
                },
                competitor: {
                     required: true
                 },
                'categories[0][category]': {
                    required: true
                },
                'categories[0][width_of_competitor]': {
                    required: true
                },
                'categories[0][height_of_competitor]': {
                    required: true
                },
                'categories[0][width_of_organization]': {
                    required: true
                },
                'categories[0][height_of_organization]': {
                    required: true
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        $('#reportForm .submit-form').click(function () {

            if ($('#reportForm').validate().form()) {
                $('#reportForm .submit-form').prop('disabled', true);
                $('#reportForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#reportForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#reportForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#reportForm').validate().form()) {
                    $('#reportForm .submit-form').prop('disabled', true);
                    $('#reportForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#reportForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#reportForm').submit(function () {
          
            var action = config.employee_url + '/reports/areas_report';
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#reportForm .submit-form').prop('disabled', false);
                    $('#reportForm .submit-form').html('ارسال');

                    if (data.type == 'success') {
                        My.toast(data.message);
                        AreasReport.empty();
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                if (i.startsWith('categories')) {
                                    var key_arr = i.split('.');
                                    var key_text = key_arr[0] + '[' + key_arr[1] + ']' + '[' + key_arr[2] + ']';
                                    i = key_text;
                                }
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#reportForm .submit-form').prop('disabled', false);
                    $('#reportForm .submit-form').html('ارسال');
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })
    }

    return {
        init: function () {
            init();
        },
        removeCategory: function (t) {
            $(t).closest('.single-category').remove();
            $(t).remove();
        },
        empty: function () {
             $('#store').find('option').eq(0).prop('selected', true);
             $('#competitor').find('option').eq(0).prop('selected', true);
             $('.store-categories').empty();
             $('.store-categories').append(new Option('اختر', ''))
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            $('#categories-container').html('');
            counter = 1;
            My.emptyForm();
        }
    };

}();
jQuery(document).ready(function () {
    AreasReport.init();
});