var PriceComparisonReport = function () {
var products, competitorProducts = [], counter = 1;

    var init = function () {
        handleSubmit();
        handleAddProduct();
        handleCheckingEvent();
        handleOptionsParsing();
        handleCompetitorChange();
    };

     var handleOptionsParsing = function () {
        products = JSON.parse(new_config.products);
     }
    
     var handleCompetitorChange = function(){
        $('#competitor').on('change',function(){
            var competitor = $(this).val();
            $('.competitor-products').html("");
            $('.competitor-products').html('<option selected value=""> اختر </option>');
            if (competitor) {
                var data = {
                    _token: new_config.token,
                    competitor: competitor
                };
                $.ajax({
                    url: config.employee_url + '/competitor_products',
                    data: data,
                    type: "POST",
                    dataType: "json",
                    success: function (data) {
                        var competitorProductsOptions = "";
                        competitorProducts = data.data.competitor_products;
                        if (products.length != 0) {
                            for (var x = 0; x < competitorProducts.length; x++) {
                                var item = competitorProducts[x];
                                competitorProductsOptions += '<option value="' + item.id + '">' + item.title + '</option>';
                            }
                            $('.competitor-products').append(competitorProductsOptions);
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        My.ajax_error_message(xhr);
                    }
                   
                });
            }
            
        });
     }
    var handleCheckingEvent = function () {
        $('.icheck').iCheck({
            checkboxClass: 'icheckbox_flat-blue'
        });
        $('.icheck').on('ifChecked', function (event) {
            $(this).closest('.single-product').find('.competitor-product-name').show();
            $(this).closest('.single-product').find('.competitor-product-list').hide();
        });
        $('.icheck').on('ifUnchecked', function (event) {
            $(this).closest('.single-product').find('.competitor-product-name').hide();
            $(this).closest('.single-product').find('.competitor-product-list').show();
        });
    }

    var handleAddProduct = function(){
        $('#add-product').on('click',function(e){
            var productBlock = 
                    '<div class="single-product pt-3" style="border-top:1px solid #dfdfdf;">'+
                        '<div class="col-xs-12"><button class="btn btn-danger pull-right" type="button" onclick="PriceComparisonReport.removeProduct(this);"><i class="fa fa-remove"></i></button></div>' +

                        ' <div class="col-md-6 col-md-offset-6">'+
                            '<label>'+
                               ' <input type="checkbox" class="icheck" data-checkbox="icheckbox_flat-blue" value="1">'+
                               ' المنتج المنافس غير مدرج'+
                            '</label>'+
                       ' </div>'+

                        '<div class="form-group col-md-6">'+
                            '<label class="control-label">المنتج</label>' +
                            '<select class="form-control" name="products['+counter+'][product]">'+
                                '<option value="">اختر</option>';
                                products.forEach(product => {
                                    productBlock += '<option value="' + product.id + '">' + product.title + '</option>';
                                });
            productBlock += '</select>' +
                           ' <span class="help-block"></span>'+
                        '</div>'+

                        '<div class="form-group col-md-6 competitor-product-name" style="display: none">'+
                            '<label class="control-label"> اسم المنتج المنافس</label>'+
                            '<input type="text" class="form-control" name="products[' + counter + '][competitor_product_name]">' +
                            '<span class="help-block"></span>'+
                       '</div>'+

                       '<div class="form-group col-md-6 competitor-product-list">' +
                            '<label class="control-label"> المنتج المنافس</label>' +
                            '<select class="form-control competitor-products" name="products[' + counter + '][competitor_product]">' +
                                '<option value="">اختر</option>';
                                competitorProducts.forEach(competitorProduct => {
                                    productBlock += '<option value="' + competitorProduct.id + '">' + competitorProduct.title + '</option>';
                                });
            productBlock += '</select>' +
                           ' <span class="help-block"></span>'+
                        '</div>'+

                        '<div class="form-group col-md-6">'+
                            '<label class="control-label"> سعر المنتج المؤسسة</label>' +
                            '<input type="number" class="form-control" name="products['+counter+'][organization_product_price]">'+ 
                            '<span class="help-block"></span>'+
                       ' </div>'+
                        
                        '<div class="form-group col-md-6">'+
                            '<label class="control-label"> سعر المنتج المنافس</label>' +
                           ' <input type="number" class="form-control" name="products['+counter+'][competitor_product_price]">'+
                            '<span class="help-block"></span>'+
                        '</div>'+

                    '</div>'+

                    '<div class="clearfix"></div>';
                           
            $('#products-container').append(productBlock);
             var productRule = "select[name='products[" + counter + "][product]']";
             var competitorProductsRule = "select[name='products[" + counter + "][competitor_product]']";
             var competitorProductNameRule = "input[name='products[" + counter + "][competitor_product_name]']";
             var organizationProductPriceRule = "input[name='products[" + counter + "][organization_product_price]']";
             var competitorProductPriceRule = "input[name='products[" + counter + "][competitor_product_price]']";
             $(productRule).rules('add', {
                 required: true
             });
             $(competitorProductsRule).rules('add', {
                 required: true
             });
             $(competitorProductNameRule).rules('add', {
                required: true
             });
             $(organizationProductPriceRule).rules('add', {
                 required: true
             });
             $(competitorProductPriceRule).rules('add', {
                 required: true
             });
           
            handleCheckingEvent();
            counter++;
        })
        return false;
    }
    var handleSubmit = function () {
        $('#reportForm').validate({
            rules: {
                store: {
                    required: true
                },
                competitor: {
                     required: true
                },
                'products[0][product]': {
                    required: true
                },
                'products[0][product_name]': {
                    required: true
                },
                'products[0][competitor_product]': {
                    required: true
                },
                'products[0][organization_product_price]': {
                    required: true
                },
                'products[0][competitor_product_price]': {
                    required: true
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        $('#reportForm .submit-form').click(function () {

            if ($('#reportForm').validate().form()) {
                $('#reportForm .submit-form').prop('disabled', true);
                $('#reportForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#reportForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#reportForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#reportForm').validate().form()) {
                    $('#reportForm .submit-form').prop('disabled', true);
                    $('#reportForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#reportForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#reportForm').submit(function () {
            var action = config.employee_url + '/reports/price_comparison_report';
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    $('#reportForm .submit-form').prop('disabled', false);
                    $('#reportForm .submit-form').html('ارسال');

                    if (data.type == 'success') {
                        My.toast(data.message);
                        PriceComparisonReport.empty();
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                if (i.startsWith('products')) {
                                    var key_arr = i.split('.');
                                    var key_text = key_arr[0] + '[' + key_arr[1] + ']' + '[' + key_arr[2] + ']';
                                    i = key_text;
                                }
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#reportForm .submit-form').prop('disabled', false);
                    $('#reportForm .submit-form').html('ارسال');
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })
    }

    return {
        init: function () {
            init();
        },
        empty: function () {
            $('#store').find('option').eq(0).prop('selected', true);
            $('#competitor').find('option').eq(0).prop('selected', true);
            $('.products').find('option').eq(0).prop('selected', true);
            $('.competitor-products').empty();
            $('.competitor-products').append(new Option('اختر', ''))
            $('.competitor-products').show();
            $('.competitor-product-name').hide();
            $('.icheck').iCheck('uncheck');
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            for (let i = 0; i < 3; i++) {
                $('.image_' + i + '_box').html('<div class="snap-container"></div><img src="' + config.url + '/no-image.png"  style="width:100%" height="400" class="image_' + i + ' snap-camera-image" onclick="My.takeSnapShot(this)" />');
                $('#image_' + i).val('');
            }
            $('#products-container').html('');
            
            My.emptyForm();
        },
        removeProduct: function (t) {
            $(t).closest('.single-product').remove();
            $(t).remove();
        }
    };

}();
jQuery(document).ready(function () {
    PriceComparisonReport.init();
});