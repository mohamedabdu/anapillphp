var PriceReport = function () {
var lat,lng;
    var init = function () {
        handleSubmit();
        handleStopWatch();
        handleValidate();
    };

    var handleValidate = function(){
         $('#taskForm').validate({
             rules: {

             },
             highlight: function (element) { // hightlight error inputs
                 $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
             },
             unhighlight: function (element) {
                 $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                 $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);
             },
             errorPlacement: function (error, element) {
                 $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
             }
         });
        var products = JSON.parse(new_config.products);
        for (var x = 0; x < products.length; x++) {
            var ele = "input[name='products[" + x + "][price]']";
            $(ele).rules('add', {
                required: true
            });
        }
    }
    var promise = new Promise(function (resolve, reject) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                latitude = position.coords.latitude;
                longitude = position.coords.longitude;
                latlng = new google.maps.LatLng(latitude, longitude);
                resolve(latlng);
            });
        } else {
            latlng = new google.maps.LatLng('24.7136', '46.6753');
            resolve(latlng);
        }
    });

    var handleStopWatch = function () {
        if (new_config.start_time != '') {
            $('#stopwatch').stopwatch({
                startTime: parseInt(new_config.start_time)
            }).stopwatch('start');
        }
    }

    var handleSubmit = function () {
        $('#finshTaskForm').validate({
            rules: {

            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);
            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        $('#finshTask .submit-form').click(function () {
            if ($('#finshTaskForm').validate().form()) {
                $('#finshTask .submit-form').prop('disabled', true);
                $('#finshTask .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                promise.then(function (latlng) {
                    lat = latlng.lat();
                    lng = latlng.lng();
                }).then(function () {
                    setTimeout(function () {
                        $('#taskForm').submit();
                    }, 1000);
                });

            }
            return false;
        });

        $('#finshTask input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#finshTaskForm').validate().form()) {
                    $('#finshTask .submit-form').prop('disabled', true);
                    $('#finshTask .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    promise.then(function (latlng) {
                        lat = latlng.lat();
                        lng = latlng.lng();
                    }).then(function () {
                        setTimeout(function () {
                            $('#taskForm').submit();
                        }, 1000);
                    });
                }
                return false;
            }
        });


        $('#taskForm').submit(function () {

            var action = config.employee_url + '/visits/price_report';
            var formData = new FormData($(this)[0]);
            formData.append('notes', $('#finshTaskForm textarea[name="notes"]').val());
            formData.append('code', new_config.code);
            formData.append('task', new_config.task);
            formData.append('lat', lat);
            formData.append('lng', lng);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#finshTask .submit-form').prop('disabled', false);
                    $('#finshTask .submit-form').html('انهاء المهمة');
                    if (data.type == 'success') {
                        window.location.href = data.message;
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#finshTask .submit-form').prop('disabled', false);
                    $('#finshTask .submit-form').html('انهاء المهمة');
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })
    }

    return {
        init: function () {
            init();
        },
        fininshTask: function () {
            if (!$('#taskForm').validate().form()){
                return false;
            }else{
                My.setModalTitle('#finshTask', 'انهاء المهمة');
                $('#finshTask').modal('show');
            }
        }
    };

}();
jQuery(document).ready(function () {
    PriceReport.init();
});