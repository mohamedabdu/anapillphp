var ProductsAlmostFinishReport = function () {
var products,counter = 1;
    var init = function () {
        handleSubmit();
        handleDate();
        handleOptionsParsing();
        handleAddProduct();
    };

    var handleDate = function () {
         $('.picker').datepicker({
            startDate: new Date(),
            format: 'yyyy-mm-dd',
         });
    }

    var handleOptionsParsing = function () {
        products = JSON.parse(new_config.products);
    }

     var handleAddProduct = function(){
        $('#add-product').on('click',function(e){
            var productBlock = '<div class="col-xs-12 single-product pt-3" style="border-top:1px solid #dfdfdf;">'+
                        '<div class="col-xs-12"><button class="btn btn-danger pull-right" type="button" onclick="ProductsAlmostFinishReport.removeProduct(this);"><i class="fa fa-remove"></i></button></div>' +
                        '<div class="form-group col-md-6">'+
                            '<label class="control-label">اسم الصنف</label>'+
                            '<select  id="product" class="form-control" name="products['+counter+'][product]">'+
                                '<option value="">اختر</option>';
                                products.forEach(product => {
                                    productBlock += '<option value="' + product.id + '">' + product.title + '</option>';
                                });
            productBlock += '</select>' +
                           ' <span class="help-block"></span>'+
                        '</div>'+

                        '<div class="form-group col-md-6">'+
                            '<label class="control-label"> عدد القطع</label>'+
                            '<input type="number" class="form-control" name="products['+counter+'][pieces_number]">'+ 
                            '<span class="help-block"></span>'+
                       ' </div>'+
                        
                        '<div class="form-group col-md-6">'+
                            '<label class="control-label"> عدد الكراتين</label>'+
                           ' <input type="number" class="form-control" name="products['+counter+'][cartons_number]">'+
                            '<span class="help-block"></span>'+
                        '</div>'+

                       '<div class="form-group col-md-6">' +
                       '<label class="control-label"> تاريخ الإنتهاء</label>' +
                       ' <input type="text" class="form-control picker" name="products[' + counter + '][date]">' +
                           '<span class="help-block"></span>' +
                        '</div>' + 
                           '</div>';
            $('#products-container').append(productBlock);
             var productRule = "select[name='products[" + counter + "][product]']";
             var piecesReasonRule = "input[name='products[" + counter + "][pieces_number]']";
             var dateRule = "input[name='products[" + counter + "][date]']";
             $(productRule).rules('add', {
                 required: true
             });
             $(piecesReasonRule).rules('add', {
                 required: true
             });
             $(dateRule).rules('add', {
                 required: true
             });
            handleDate();
            counter++;
        })
        return false;
    }

    var handleSubmit = function () {
        $('#reportForm').validate({
            rules: {
                store: {
                    required: true
                },
                'products[0][product]': {
                    required: true
                },
                'products[0][pieces_number]': {
                    required: true
                },
                'products[0][date]': {
                    required: true
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });

        $('#reportForm .submit-form').click(function () {

            if ($('#reportForm').validate().form()) {
                $('#reportForm .submit-form').prop('disabled', true);
                $('#reportForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#reportForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#reportForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#reportForm').validate().form()) {
                    $('#reportForm .submit-form').prop('disabled', true);
                    $('#reportForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#reportForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#reportForm').submit(function () {
          
            var action = config.employee_url + '/reports/products_almost_finish_report';
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#reportForm .submit-form').prop('disabled', false);
                    $('#reportForm .submit-form').html('ارسال');

                    if (data.type == 'success') {
                        My.toast(data.message);
                        ProductsAlmostFinishReport.empty();
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                if (i.startsWith('products')) {
                                    var key_arr = i.split('.');
                                    var key_text = key_arr[0] + '[' + key_arr[1] + ']' + '[' + key_arr[2] + ']';
                                    i = key_text;
                                }
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#reportForm .submit-form').prop('disabled', false);
                    $('#reportForm .submit-form').html('ارسال');
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });
            return false;

        })
    }

    return {
        init: function () {
            init();
        },
        removeProduct: function (t) {
            $(t).closest('.single-product').remove();
            $(t).remove();
        },
        empty: function () {
            $('#store').find('option').eq(0).prop('selected', true);
            $('#product').find('option').eq(0).prop('selected', true);
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            for (let i = 0; i < 3; i++) {
                $('.image_' + i + '_box').html('<div class="snap-container"></div><img src="' + config.url + '/no-image.png" style="width:100%" height="400" class="image_' + i + ' snap-camera-image" onclick="My.takeSnapShot(this)" />');
                $('#image_' + i).val('');
            }
            $('#products-container').html('');
            counter = 1;
            My.emptyForm();
        }
    };

}();
jQuery(document).ready(function () {
    ProductsAlmostFinishReport.init();
});