var Sessions_grid;

var Sessions = function() {

    var init = function() {
        handleRecords();
        handleSubmit();
        handleChangeCategory();
        handleChangeSubCategory();
        handleTypeChange();
        $('#date').datetimepicker({
            minDate: new Date()
        });

    };

   var handleTypeChange = function(){
       $('#type').on('change',function(e){
            var type = e.target.value;
            if (type == 2) {
                $('#live_date').show();
                $('#url').rules('remove');
                $('#url').closest('.form-group').removeClass('has-error');
                $('#url').closest('.form-group').find('.help-block').html('').css('opacity', 0);
            }else{
                $('#live_date').hide();
                var url = "input[name='url']";
                 $(url).rules('add', {
                     required: true
                 });
            }
       });
   }

    var handleRecords = function() {

        Sessions_grid = $('.dataTable').dataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": config.admin_url + "/sessions/data",
                "type": "POST",
                data: { _token: $('input[name="_token"]').val() },
            },
            "columns": [
                {
                    "data": "title",
                    "name": "session_translations.name"
                }, 
                {
                    "data": "category",
                    "name": "categories_translations.title"
                },
                {
                    "data": "specialist",
                    "name": "specialists_translations.title"
                },
                {
                    "data": "type",
                    "name": "sessions.type"
                },
                {
                    "data": "views",
                    "name": "sessions.views"
                },
                {
                    "data": "active",
                    "name": "sessions.active"
                },

                { "data": "options", orderable: false, searchable: false }
            ],
            "order": [
                [1, "asc"]
            ],

            "oLanguage": { "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json' }

        });
    }


    var handleChangeCategory = function () {
        $('#categories').on('change', function () {
            var category = $(this).val();
            $('#sub_categories').html("");
            $('#sub_categories').html('<option selected value="">' + lang.choose + '</option>');
             $('#specialists').html("");
             $('#specialists').html('<option selected value="">' + lang.choose + '</option>');
            sub_categories = "";
            if (category) {
                $.get('' + config.admin_url + '/getCategories/' + category, function (data) {
                    if (data.data.length != 0) {
                        for (var x = 0; x < data.data.length; x++) {
                            var item = data.data[x];
                            sub_categories += '<option value="' + item.id + '">' + item.title + '</option>';
                        }
                        $('#sub_categories').append(sub_categories);
                    }


                }, "json");
            }
        })
    }

     var handleChangeSubCategory = function () {
         $('#sub_categories').on('change', function () {
             var category = $(this).val();
             $('#specialists').html("");
             $('#specialists').html('<option selected value="">' + lang.choose + '</option>');
            
             specialists = "";
             if (category) {
                 $.get('' + config.admin_url + '/getSpecialists/' + category, function (data) {
                     if (data.data.length != 0) {
                         for (var x = 0; x < data.data.length; x++) {
                             var item = data.data[x];
                             specialists += '<option value="' + item.id + '">' + item.name + '</option>';
                         }
                         $('#specialists').append(specialists);
                     }
                 }, "json");
             }
         })
     }

    

    var handleSubmit = function() {

        $('#addEditSessionsForm').validate({
            rules: {
                
                'category_id': {
                     required: true
                 },
                active: {
                    required: true
                },
                specialist_id: {
                    required: true
                },
                categories: {
                     required: true
                },
                date: {
                    required: true
                },
                type: {
                    required: true
                }
            },
            //messages: lang.messages,
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function(error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });
        var langs = JSON.parse(config.languages);
         for (var x = 0; x < langs.length; x++) {
             var title = "input[name='title[" + langs[x] + "]']";
             var description = "textarea[name='description[" + langs[x] + "]']";

             $(title).rules('add', {
                 required: true
             });
             $(description).rules('add', {
                 required: true
             });
         }


        $('#addEditSessionsForm .submit-form').click(function() {

            if ($('#addEditSessionsForm').validate().form()) {
                $('#addEditSessionsForm .submit-form').prop('disabled', true);
                $('#addEditSessionsForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function() {
                    $('#addEditSessionsForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#addEditSessionsForm input').keypress(function(e) {
            if (e.which == 13) {
                if ($('#addEditSessionsForm').validate().form()) {
                    $('#addEditSessionsForm .submit-form').prop('disabled', true);
                    $('#addEditSessionsForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function() {
                        $('#addEditSessionsForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#addEditSessionsForm').submit(function() {
            var id = $('#id').val();
            var action = config.admin_url + '/sessions';
            var formData = new FormData($(this)[0]);
            if (id != 0) {
                formData.append('_method', 'PATCH');
                action = config.admin_url + '/sessions/' + id;
            }
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    $('#addEditSessionsForm .submit-form').prop('disabled', false);
                    $('#addEditSessionsForm .submit-form').html(lang.save);

                    if (data.type == 'success') {
                         My.toast(data.message);
                        if (id == 0) {
                            Sessions.empty();
                        }

                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                if (i.startsWith('title') || i.startsWith('description')) {
                                    var key_arr = i.split('.');
                                    var key_text = key_arr[0] + '[' + key_arr[1] + ']';
                                    i = key_text;
                                }
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    $('#addEditSessionsForm .submit-form').prop('disabled', false);
                    $('#addEditSessionsForm .submit-form').html(lang.save);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });


            return false;

        })




    }

    return {
        init: function() {
            init();
        },
        delete: function(t) {

            var id = $(t).attr("data-id");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/sessions/' + id,
                data: { _method: 'DELETE', _token: $('input[name="_token"]').val() },
                success: function(data) {
                    Sessions_grid.api().ajax.reload();
                }
            });

        },
        terminate: function (t) {
              var id = $(t).attr("data-id");
               $(t).prop('disabled', true);
              $.ajax({
                  url: config.admin_url + '/sessions/terminate/' + id,
                  success: function (data) {
                      Sessions_grid.api().ajax.reload();
                  },
                  error: function (xhr, textStatus, errorThrown) {
                     $(t).prop('disabled', false);
                      My.ajax_error_message(xhr);
                  },
                dataType: "json",
                type: "get"
              });
        },
        error_message: function(message) {
            $.alert({
                title: lang.error,
                content: message,
                type: 'red',
                typeAnimated: true,
                buttons: {
                    tryAgain: {
                        text: lang.try_again,
                        btnClass: 'btn-red',
                        action: function() {}
                    }
                }
            });
        },
        empty: function() {
            $('#id').val(0);
            $('#active').find('option').eq(0).prop('selected', true);
            $('#type').find('option').eq(0).prop('selected', true);
            $('#live_date').hide();
            
            $('input[type="checkbox"]').prop('checked', false);
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            $('#image').val('');
            $('#category_id').find('option').eq(0).prop('selected', true);
            $('#sub_categories').html('<option value="">' + lang.choose + '</option>');
            $('#specialists').html('<option value="">' + lang.choose + '</option>');
            $('#show_rates').prop('checked', false).change();

            My.emptyForm();
        }
    };

}();

jQuery(document).ready(function() {
    Sessions.init();
});