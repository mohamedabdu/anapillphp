
var Visits = function () {
    var init = function () {
        createVisit();
        showVisitSchedule();
        showVisitNormal();
    }

    var createVisit = function () {
        $("#stores_select").chosen();
        $("#agents_select").chosen();
        $("#tasks_select").chosen();

        var onGoing = $("#on_going").val();
        checkOnGoing(onGoing);
        $('#on_going').on('change', function (e) {
            onGoing = e.target.value;
            checkOnGoing(onGoing);
        });
    }

    var checkOnGoing = function (onGoing) {
        if(onGoing == 0) {
            $("#num_of_visit").fadeIn(1000);
        } else {
            $("#num_of_visit").fadeOut(1000);
        }
    }

    var showVisitSchedule = function () {
        $('#reassign').on('click', function (e) {
            e.preventDefault();
            var agent = $("#agents_select_show").val();
            if(agent === "") {
                return;
            }
            array = [];
            $('#agents_select_show option:selected').each(function() {
                array.push($(this).val());
            });

            visitID = config.visitID;

            $.get(appUrl + '/visits/'+visitID+'/edit/ajax',{ agents: array }, function (data, status) {
                if (data.length !== 0 && data.success === true) {
                    console.log(data.success);
                    if(data.success == true) {
                        makeToast(data.message, 1);
                        $("#agentsDiv").html("");
                        $("#agents_select_show").empty();
                        $('#agents_select_show').append($('<option>', {
                            value: '',
                            text: "اختار",
                            selected: true
                        }));
                        $.each(data.agentsSelected, function (index, agent) {
                            $('#agentsDiv').append("<p>" + agent + "</p>");

                        });
                        $.each(data.agents, function (index, agent) {

                            $('#agents_select_show').append($('<option>', {
                                value: agent.id,
                                text: agent.name
                            }));

                        });

                    } else {
                        makeToast(data.message, 0);
                    }
                    //$('.modal-backdrop').remove();
                }

            });

        });

    }

    var showVisitNormal = function () {
        $('#reassignNormal').on('click', function (e) {
            e.preventDefault();
            var agent = $("#agents_select").val();
            if(agent === "") {
                return;
            }
            array = [];
            $('#agents_select option:selected').each(function() {
                array.push($(this).val());
            });

            visitID = config.visitID;
            console.log(array);
            $.get(appUrl + '/visits/'+visitID+'/edit/ajax',{ agents: array }, function (data, status) {
                if (data.length !== 0 && data.success === true) {
                    console.log(data.success);
                    if(data.success == true) {
                        makeToast(data.message, 1);
                        $("#agentsDiv").html("");
                        //$("#agents_select").html("");
                        //$('#agents_select').val('').trigger('chosen:updated');
                        $('#agents_select')
                            .find('option:first-child').prop('selected', false)
                            .end().trigger('chosen:updated');

                        $.each(data.agentsSelected, function (index, agent) {
                            $('#agentsDiv').append("<p>" + agent + "</p>");

                        });
                        // $.each(data.agents, function (index, agent) {
                        //
                        //     $('#agents_select').append($('<option>', {
                        //         value: agent.id,
                        //         text: agent.name
                        //     }));
                        //
                        // });

                    } else {
                        makeToast(data.message, 0);
                    }
                    //$('.modal-backdrop').remove();
                }

            });

        });

    }

    return {
        init: function () {
            init();
        }
    }
}();

jQuery(document).ready(function () {
    Visits.init();
});




