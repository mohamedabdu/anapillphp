<?php

namespace App\Models;

use App\Models\Base\BaseModel;
use Carbon\Carbon;





class CompetitorReport extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'competitors_reports';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
   
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    
    ];

    public static $types = [
        'price_comparison' => 1,
        'promotions' => 2,
        'areas' => 3,
        'reduced_prices' => 4,
        'ordinary' => 5
    ];
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {


    }

    public function transformSupervisor()
    {
        Carbon::setLocale('ar');
        $transformer = new \stdClass();
        $transformer->date = $this->created_at->format('Y/m/d - H:i');
        $transformer->created_at = $this->created_at->diffForHumans();
        $transformer->store = $this->store;
        $transformer->competitor = $this->competitor;
        $transformer->notes = $this->notes ?: '-';
        if ($this->images) {
            $images = [];
            foreach (json_decode($this->images) as $image) {
                $images[] = \Helper\Common\imageUrl($image);
            }
            $transformer->images = $images;
        }
        
        if ($this->type == self::$types['price_comparison']) 
        {
             $report_products = CompetitorReportPriceComparisonProduct::join('products as p1', 'p1.id', '=', 'competitors_reports_price_comparison_products.product_id')
                ->leftJoin('products as p2', 'p2.id', '=', 'competitors_reports_price_comparison_products.competitor_product_id')
                ->where('competitors_reports_price_comparison_products.competitors_report_id', $this->id)
                ->select('competitors_reports_price_comparison_products.*', 'p1.title_ar as product', 'p2.title_ar as competitor_product')
                ->get();

            $transformer->products =  CompetitorReportPriceComparisonProduct::transformCollection($report_products, 'Supervisor');
        }
        else if ($this->type == self::$types['promotions']) 
        {
            $report_promotions = CompetitorReportPromotionsPromos::join('promos', 'promos.id', '=', 'competitors_reports_promotions_promos.promo_id')
                ->where('competitors_reports_promotions_promos.competitors_report_id', $this->id)
                ->select('competitors_reports_promotions_promos.*', 'promos.title_ar as promo')
                ->get();

            $transformer->promotions =  CompetitorReportPromotionsPromos::transformCollection($report_promotions, 'Supervisor');
        }
        else if ($this->type == self::$types['areas']) 
        {
            $report_categories = CompetitorReportAreasCategory::join('categories', 'categories.id', '=', 'competitors_reports_areas_categories.category_id')
                ->where('competitors_reports_areas_categories.competitors_report_id', $this->id)
                ->select('competitors_reports_areas_categories.*', 'categories.title_ar as category')
                ->get();

            $transformer->categories =  CompetitorReportAreasCategory::transformCollection($report_categories, 'Supervisor');
        }
        else if($this->type == self::$types['reduced_prices'])
        {
             $report_products = CompetitorReportReducedPricesProduct::join('promos','promos.id','=', 'competitors_reports_reduced_prices_products.promo_id')
                ->leftJoin('products', 'products.id', '=', 'competitors_reports_reduced_prices_products.product_id')
                ->where('competitors_reports_reduced_prices_products.competitors_report_id', $this->id)
                ->select('competitors_reports_reduced_prices_products.*', 'products.title_ar as competitor_product', 'promos.title_ar as promo')
                ->get();

            $transformer->products =  CompetitorReportReducedPricesProduct::transformCollection($report_products, 'Supervisor');
        }
        
       
        
        return $transformer;
    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(CompetitorReport $item) {

        });
        static::deleted(function(CompetitorReport $item) {


        });

    }

}
