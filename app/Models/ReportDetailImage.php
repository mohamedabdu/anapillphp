<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class ReportDetailImage
 * @package App\Models
 * @version October 10, 2019, 5:26 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer report_id
 * @property string image
 */

class ReportDetailImage extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'report_detail_images';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    'report_id',
        'image'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'report_id' => 'integer',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

        $transformer = new ReportDetailImage();

        $transformer->id = $this->id;
        $transformer->report_id = $this->report_id;
        $transformer->image = static::imageNullable($this);

        return $transformer;

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(ReportDetailImage $item) {

        });
        static::deleted(function(ReportDetailImage $item) {


        });

    }

}
