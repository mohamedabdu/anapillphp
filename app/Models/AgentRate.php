<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class AgentRate
 * @package App\Models
 * @version October 10, 2019, 5:20 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer agent_id
 * @property string rate
 * @property string note
 * @property string date
 */

class AgentRate extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'agent_rates';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    'agent_id',
        'rate',
        'note',
        'date'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'agent_id' => 'integer',
        'rate' => 'string',
        'note' => 'string',
        'date' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

        $transformer = new AgentRate();

        $transformer->id = $this->id;
        $transformer->agent_id = $this->agent_id;
        $transformer->rate = $this->rate;
        $transformer->note = $this->note;
        $transformer->date = $this->date;
        $transformer->created_at = $this->created_at;
        $transformer->updated_at = $this->updated_at;

        return $transformer;

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(AgentRate $item) {

        });
        static::deleted(function(AgentRate $item) {


        });

    }

}
