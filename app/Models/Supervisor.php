<?php

namespace App\Models;

use App\Scopes\SupervisorScope;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use function Helper\Common\imageUrl;

/**
 * Class User
 * @package App
 *
 * @property string name
 * @property string username
 * @property string email
 * @property string mobile
 * @property string image
 * @property string device_token
 * @property integer device_type
 * @property integer active
 * @property integer block
 * @property integer type
 * @property integer id
 * @property integer country_id
 * @property integer city_id
 * @property integer region_id
 * @property integer created_by
 * @property integer work_hours
 * @property string work_days
 */
class Supervisor extends Authenticatable
{
    use Notifiable ,HasApiTokens ;


    protected $table = "users";

    public static $days = [
        "Sun",
        "Mon",
        "Tue",
        "Wed",
        "Thu",
        "Fri",
        "Sat",
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','username', 'email', 'password','image','device_token','device_type','mobile','active','role',
        "created_by", "type","block","city_id","country_id","region_id", "work_hours","work_days"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function transform() {
        $transformer = new Supervisor();
        $transformer->username = $this->username;
        $transformer->email = $this->email;
        $transformer->mobile = $this->mobile;
        $transformer->name = $this->name;
        if($this->image == null)
        {
            $transformer->image = url("public/uploads/no_avatar.jpg");
        }
        else
        {
            $transformer->image = imageUrl($this->image);
        }
        $transformer->active = $this->active;
        return $transformer;
    }



    public static function deleteRelations(Supervisor $item)
    {

    }
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SupervisorScope());

        static::deleting(function(Supervisor $item) {
            self::deleteRelations($item);
        });

    }
}
