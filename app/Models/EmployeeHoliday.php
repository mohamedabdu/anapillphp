<?php

namespace App\Models;
use App\Models\Base\BaseModel;

class EmployeeHoliday extends BaseModel
{
    
    public $table = 'employee_holidays';

    public static $statuses = [
        'pending' => 0,
        'accepted' => 1,
        'rejected' => 2
    ];

    public static $statuses_text = [
        0 => 'قيد المراجعة',
        1 => 'مقبول',
        2 => 'مرفوض'
    ];

    public $fillable = [
    
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
       
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function transform()
    {

    }

    public function transformSupervisor()
    {
        $transformer = new \stdClass();
        $transformer->id = encrypt($this->id);
        $transformer->employee = $this->employee;
        $transformer->title = $this->title;
        $transformer->reason = $this->reason;
        $transformer->start_date = $this->start_date;
        $transformer->end_date = $this->end_date;
        $transformer->reject_only = false;
        if ($this->start_date < date('Y-m-d') && $this->status == self::$statuses['pending']) {
            $transformer->reject_only = true;
        }
        $transformer->status = $this->status;
        $transformer->status_text = self::$statuses_text[$this->status];
        $transformer->rejection_reason = $this->rejection_reason;
        $transformer->created_at = $this->created_at->format('Y/m/d H:i');
        
        return $transformer;
    }

    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($item) {

        });
        static::deleted(function($item) {


        });

    }

}
