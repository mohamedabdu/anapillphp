<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class VisitTaskAreaDetail
 * @package App\Models
 * @version October 10, 2019, 5:14 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer visit_task_detail_id
 * @property integer category_id
 * @property string width
 * @property string height
 * @property string images
 * @property string note
 */

class VisitTaskAreasReportDetail extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'visit_task_areas_report_details';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
   
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

       

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(VisitTaskAreasReportDetail $item) {

        });
        static::deleted(function(VisitTaskAreasReportDetail $item) {


        });

    }

}
