<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends MyModel {

    use SoftDeletes;

    protected $table = "locations";
    
    

    public static function getAll($parent_id = 0) {
        $data =  static::join('location_translations', function($query){
                            $query->on('locations.id', '=', 'location_translations.location_id')
                             ->where('location_translations.locale', static::getLangCode());
                        })
                        ->where('locations.parent_id',$parent_id);
        $data = self::checkCreatedBy($data, 'locations')
                        ->select('locations.*', 'location_translations.title')
                        ->get();
        return $data;
    }

    public function childs() {
        return $this->hasMany(Location::class, 'parent_id');
    }

    public function translations() {
        return $this->hasMany(LocationTranslation::class, 'location_id');
    }


    public static function transformAdmin($item)
    {
        $transformer = new \stdClass();
        $transformer->id = en_de_crypt($item->id);
        $transformer->title = $item->title;

        return $transformer;
    }

   

    protected static function boot() {
        parent::boot();

        static::deleting(function($location) {
            
            
        });

        static::deleted(function($location) {
            
        });
    }

}
