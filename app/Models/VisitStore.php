<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class VisitStore
 * @package App\Models
 * @version October 10, 2019, 5:01 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer visit_id
 * @property integer store_id
 * @property integer status
 */

class VisitStore extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'visit_stores';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    'visit_id',
        'store_id',
        'status'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'visit_id' => 'integer',
        'store_id' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

        $transformer = new VisitStore();

        $transformer->store_id = $this->store_id;
        $transformer->address = $this->address_note;

        return $transformer;

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(VisitStore $item) {

        });
        static::deleted(function(VisitStore $item) {


        });

    }

}
