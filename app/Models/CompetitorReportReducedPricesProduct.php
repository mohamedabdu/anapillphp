<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/



class CompetitorReportReducedPricesProduct extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'competitors_reports_reduced_prices_products';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
   
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    
    ];

    
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {


    }

    public function transformSupervisor()
    {
        $transformer = new \stdClass();
        $transformer->competitor_product = $this->competitor_product ?: $this->product_name;
        $transformer->promo = $this->promo;
        $transformer->current_price = $this->current_price;
        $transformer->offer_price = $this->offer_price;

        return $transformer;
    }

    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(CompetitorReportReducedPricesProduct $item) {

        });
        static::deleted(function(CompetitorReportReducedPricesProduct $item) {


        });

    }

}
