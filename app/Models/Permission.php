<?php

namespace App\Models;

class Permission extends MyModel
{
    
    public $table = 'permissions';
    

    public static function transformAdmin($item)
    {
        $transformer = new \stdClass();
        $transformer->id = en_de_crypt($item->id);
        $transformer->permission = _lang('app.'.$item->segment);
        return $transformer;
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($item) {

        });
        static::deleted(function($item) {


        });

    }

}
