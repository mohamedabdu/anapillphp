<?php

namespace App\Models;

class RolePermission extends MyModel
{

    public $table = 'role_permissions';

    public function role() {
        return $this->belongsTo(Role::class, "role_id","id");
    }
    public function permission() {
        return $this->belongsTo(Permission::class, "permission_id","id");
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($item) {

        });

        static::deleted(function($item) {

        });

    }

}
