<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class TaskInstructionImage
 * @package App\Models
 * @version October 10, 2019, 4:58 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer task_instruction_id
 * @property string image
 */

class TaskInstructionImage extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'task_instruction_images';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    'task_instruction_id',
        'image'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'task_instruction_id' => 'integer',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

        $transformer = new TaskInstructionImage();

        $transformer->image = static::imageNullable($this);

        return $transformer;

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(TaskInstructionImage $item) {

        });
        static::deleted(function(TaskInstructionImage $item) {


        });

    }

}
