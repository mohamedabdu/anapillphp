<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class Config
 * @package App\Models
 * @version October 10, 2019, 4:29 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property string system_email
 * @property string system_mobile
 * @property string about
 * @property string rules
 * @property string facebook
 * @property string twitter
 * @property string system_holidays
 */

class Config extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'configs';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    'system_email',
        'system_mobile',
        'about',
        'rules',
        'facebook',
        'twitter',
        'system_holidays',
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'system_email' => 'string',
        'system_mobile' => 'string',
        'about' => 'string',
        'rules' => 'string',
        'facebook' => 'string',
        'twitter' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

        $transformer = new Config();

        $transformer->system_email = $this->system_email;
        $transformer->system_mobile = $this->system_mobile;
        $transformer->about = $this->about;
        $transformer->rules = $this->rules;
        $transformer->facebook = $this->facebook;
        $transformer->twitter = $this->twitter;

        return $transformer;

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(Config $item) {

        });
        static::deleted(function(Config $item) {


        });

    }

}
