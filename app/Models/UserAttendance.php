<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class UserAttendance
 * @package App\Models
 * @version October 10, 2019, 5:18 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer user_id
 * @property string date
 * @property integer is_end
 * @property string end_date
 * @property string image
 * @property string lat
 * @property string lng
 */

class UserAttendance extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'user_attendances';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    'user_id',
        'date',
        'is_end',
        'end_date',
        'image',
        'lat',
        'lng'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'user_id' => 'integer',
        'date' => 'string',
        'is_end' => 'integer',
        'end_date' => 'string',
        'image' => 'string',
        'lat' => 'string',
        'lng' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

        $transformer = new UserAttendance();

        $transformer->id = $this->id;
        $transformer->user_id = $this->user_id;
        $transformer->date = $this->date;
        $transformer->is_end = $this->is_end;
        $transformer->end_date = $this->end_date;
        $transformer->image = static::imageNullable($this);
        $transformer->lat = $this->lat;
        $transformer->lng = $this->lng;
        $transformer->created_at = $this->created_at;
        $transformer->updated_at = $this->updated_at;

        return $transformer;

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(UserAttendance $item) {

        });
        static::deleted(function(UserAttendance $item) {


        });

    }

}
