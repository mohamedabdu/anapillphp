<?php

namespace App\Models;
use App\Models\Base\BaseModel;
use App\Traits\VisitTrait;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class Visit
 * @package App\Models
 * @version October 10, 2019, 5:00 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property string code
 * @property string start_date
 * @property string on_going
 * @property string holidays
 * @property integer num_of_visits
 * @property integer repeat_every
 * @property integer is_normal
 * @property integer type
 * @property integer status
 * @property integer created_by
 */

class Visit extends BaseModel
{
    use VisitTrait;
    /*
        use SoftDeletes;

    */

    /**
     *  1 => نوع الزيارة عادية
     * 2 => نوع الزيارة مجدولة
     */
    /** @var array  */
    public static $types = [
        "normal" => 1,
        "scheduled" => 2,
        2 => "scheduled",
        1 => "normal",
    ];

    

    public $table = 'visits';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    'code',
        'start_date',
        'on_going',
        'num_of_visits',
        'repeat_every',
        'is_normal',
        'status',
        'created_by',
        "type",
        "holidays",
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'code' => 'string',
        'start_date' => 'string',
        'on_going' => 'string',
        'num_of_visits' => 'integer',
        'repeat_every' => 'integer',
        'is_normal' => 'integer',
        'status' => 'integer',
        'created_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

        $transformer = new Visit();

        $transformer->id = $this->id;
        $transformer->code = $this->code;
        $lastdate = VisitDetail::where("visit_id",$this->id)->latest("id")->first();
        if($lastdate != null) {
            $transformer->start_date = $lastdate->start_date;
        } else {
            $transformer->start_date = $this->start_date;
        }

        $stores = VisitStore::join("stores","stores.id","=","visit_stores.store_id")
            ->where("visit_id",$this->id)
            ->get()
            ->transformCollection();
        if($stores != null) {
            $transformer->stores = $stores;
        }
        $tasks = Task::join("visit_tasks","visit_tasks.task_id","=","tasks.id")
                    ->where("visit_tasks.visit_id",$this->id)
                    ->get()
                    ->transformCollection();
        $transformer->tasks = $tasks;
        return $transformer;
    }

    public function transformEmployee()
    {
       $transformer = new \stdClass();
       $transformer->id = \Crypt::encrypt($this->id);
       $transformer->lat = $this->lat;
       $transformer->lng = $this->lng;
       $transformer->code = $this->code;
       $transformer->store = $this->store;
       return $transformer;
    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(Visit $item) {

        });
        static::deleted(function(Visit $item) {


        });

    }

}
