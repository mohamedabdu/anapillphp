<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends MyModel
{
    use SoftDeletes;
    public $table = 'products';

    public function translations()
    {
        return $this->hasMany(ProductTranslation::class, 'product_id');
    }

    public function product_competitors()
    {
        return $this->hasMany(ProductCompetitor::class, 'product_id');
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($product) { 
           foreach ($product->product_competitors as $product_competitor) {
                $product_competitor->delete();
           }
            
        });
        static::deleted(function () { 

        });
    }
}
