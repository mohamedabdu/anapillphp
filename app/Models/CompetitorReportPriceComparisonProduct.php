<?php

namespace App\Models;
use App\Models\Base\BaseModel;





class CompetitorReportPriceComparisonProduct extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'competitors_reports_price_comparison_products';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
   
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    
    ];

    
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {


    }

    public function transformSupervisor()
    {
        $transformer = new \stdClass();
        $transformer->product_name = $this->product;
        $transformer->competitor_product_name = $this->competitor_product ?: $this->product_name;
        $transformer->organization_product_price = $this->organization_product_price;
        $transformer->competitor_product_price = $this->competitor_product_price;

        return $transformer;
    }

    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(CompetitorReportPriceComparisonProduct $item) {

        });
        static::deleted(function(CompetitorReportPriceComparisonProduct $item) {


        });

    }

}
