<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/
 


class CompetitorReportAreasCategory extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'competitors_reports_areas_categories';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
   
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    
    ];

    
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {


    }

    public function transformSupervisor()
    {
        $width_unit = ' سم';
        $height_unit = ' شلف';
        $transformer = new \stdClass();
        $transformer->category = $this->category;
        $transformer->organization_width = $this->width_of_organization . $width_unit;
        $transformer->organization_height = $this->height_of_organization . $height_unit;
        $transformer->competitor_width = $this->width_of_competitor . $width_unit;
        $transformer->competitor_height = $this->height_of_competitor . $height_unit;

        return $transformer;
    }


    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(CompetitorReportAreasCategory $item) {

        });
        static::deleted(function(CompetitorReportAreasCategory $item) {


        });

    }

}
