<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/



class CompetitorReportPromotionsPromos extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'competitors_reports_promotions_promos';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
   
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    
    ];

    
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {


    }

    public function  transformSupervisor()
    {
        $transformer = new \stdClass();
        $transformer->promo = $this->promo;
        $transformer->promo_count = $this->number;
        $transformer->notes = $this->notes? : '-';
        $transformer->image = \Helper\Common\imageUrl($this->image);

        return $transformer;

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(CompetitorReportPromotionsPromos $item) {

        });
        static::deleted(function(CompetitorReportPromotionsPromos $item) {


        });

    }

}
