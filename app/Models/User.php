<?php

namespace App\Models;

use App\Scopes\AgentScope;
use App\Traits\ModelTrait;
use App\Scopes\SupervisorScope;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable, ModelTrait;
    

    private static $arabic_days = [
        'Sun' => 'الأحد',
        'Mon' => 'الإثنين',
        'Tue' => 'الثلاثاء',
        'Wed' => 'الأربعاء',
        'Thu' => 'الخميس',
        'Fri' => 'الجمعة',
        'Sat' => 'السبت'
    ];

    public static $types = [
        'admin' => 1,
        'employee' => 2,
        'supervisor' => 3,
        'company' => 4
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function transform(User $item) {
        $transformer = new User();
        $transformer->username = $item->username;
        $transformer->email = $item->email;
        $transformer->mobile = $item->mobile;
        $transformer->name = $item->name;
        if($item->image == null)
        {
            $transformer->image = url("public/uploads/no_avatar.jpg");
        }
        else
        {
            $transformer->image = $item->image;
        }
        $transformer->activation_code = $item->activation_code;
        $transformer->active = (int) $item->active;
        if(in_array($item->type,[AgentScope::$typeOfAgent, SupervisorScope::$typeOfSupervisor]) ) {
            $transformer->work_hours = $item->work_hours;
            $days = [];
            try {
                $days = json_decode($item->work_days);
            } catch (\Exception $e) {

            }
            $arabic_days = [];
            foreach ($days as $day) {
               $arabic_days[] = self::$arabic_days[$day];
            }
            $transformer->work_days = implode(' - ', $arabic_days);
            $transformer->default_work_days = $days;
            $city = City::where("id",$item->city_id)->first();
            if($city != null) {
                $transformer->city = titleSlug($city, "title");
            }
        }
        return $transformer;
    }


    public function userRole() {
        return $this->belongsTo(Role::class, "role","id");
    }


    protected static function boot()
    {
        parent::boot();
        static::deleting(function($user) {
            
        });
        static::deleted(function($user) {
            self::deleteUploaded('users',$user->image);
        });

    }
}
