<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class VisitTaskDetailImage
 * @package App\Models
 * @version October 10, 2019, 5:10 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer visit_detail_id
 * @property string note
 * @property string image
 * @property string image_before
 * @property string image_after
 * @property string note_after
 * @property string note_before
 */

class VisitTaskShelvesDetails extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'visit_task_shelves_details';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    'visit_detail_id',
        'note',
        'image',
        "image_after",
        "image_before",
        "note_after",
        "note_before",
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'visit_detail_id' => 'integer',
        'note' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

        $transformer = new VisitTaskDetailChelf();

        $transformer->id = $this->id;
        $transformer->visit_detail_id = $this->visit_detail_id;
        $transformer->note = $this->note;
        $transformer->image = static::imageNullable($this);

        return $transformer;

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(VisitTaskDetailChelf $item) {

        });
        static::deleted(function(VisitTaskDetailChelf $item) {


        });

    }

}
