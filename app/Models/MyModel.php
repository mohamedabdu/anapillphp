<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\AUTHORIZATION;
use App\Models\User;
use App\Traits\ModelTrait;

use Request;
use Image;

class MyModel extends Model {

    use ModelTrait;
    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

    protected static function auth_user() {
        $token = Request::header('authorization');
        $token = Authorization::validateToken($token);
        $user = null;
        if ($token) {
            $user = User::find($token->id);
        }

        return $user;
    }

    protected static function checkCreatedBy($data, $table_name)
    {
        $user = auth()->user();
        $column = $table_name . ".created_by";
        $super_parent_id = explode(",", $user->parents_id)[0];

        if ($super_parent_id) {
            $data = $data->where(function ($query) use ($column, $super_parent_id) {
                $query->where($column, $super_parent_id)
                    ->orWhereIn($column, function ($query, $super_parent_id) {
                        $query->select('id')->from('users')
                            ->whereRaw('FIND_IN_SET(?,parents_ids) > 0', $super_parent_id);
                    });
            });
        } else {
            $data = $data->where(function ($query) use ($column,$user) {
                $query->where($column, $user->id)
                    ->orWhereIn($column, function ($query) use($user) {
                        $query->select('id')->from('users')
                            ->whereRaw('FIND_IN_SET(?,parents_ids) > 0', $user->id);
                    });
            });
        }

        return $data;
    }

    protected static function iniDiffLocations($tableName, $lat, $lng)
    {
        $diffLocations = "SQRT(POW(69.1 * ($tableName.lat - {$lat}), 2) + POW(69.1 * ({$lng} - $tableName.lng) * COS($tableName.lat / 57.3), 2)) as distance";
        return $diffLocations;
    }

 
   

    

}
