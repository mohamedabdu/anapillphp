<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class PromoDetail
 * @package App\Models
 * @version October 10, 2019, 4:39 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer promo_id
 * @property integer store_id
 * @property string promo_num
 * @property string start_date
 * @property string end_date
 * @property string contract_images
 */

class PromoDetails extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'promo_details';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    'promo_id',
        'store_id',
        'promo_num',
        'start_date',
        'end_date',
        'contract_images'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'promo_id' => 'integer',
        'store_id' => 'integer',
        'promo_num' => 'string',
        'start_date' => 'string',
        'end_date' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

        $transformer = new PromoDetail();

        $transformer->id = $this->promo_id;
        //$transformer->promo_id = $this->promo_id;
        //$transformer->store_id = $this->store_id;
        //$transformer->promo_num = $this->promo_num;
        //$transformer->start_date = $this->start_date;
        //$transformer->end_date = $this->end_date;
        //$transformer->contract_images = $this->contract_images;
        $transformer->title = static::titleSlug($this, "title");
        return $transformer;

    }

    public function promo() {
        return $this->belongsTo(Promo::class, "promo_id","id");
    }

    public function store() {
        return $this->belongsTo(Store::class, "store_id","id");
    }

    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(PromoDetails $item) {

        });
        static::deleted(function(PromoDetails $item) {


        });

    }

}
