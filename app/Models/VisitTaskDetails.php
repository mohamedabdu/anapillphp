<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class VisitTaskDetail
 * @package App\Models
 * @version October 10, 2019, 5:09 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer visit_detail_id
 * @property integer task_id
 * @property string start_date
 * @property string end_date
 * @property string start_lat
 * @property string start_lng
 * @property string end_lat
 * @property string end_lng
 * @property string note
 */

class VisitTaskDetails extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'visit_task_details';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    'visit_detail_id',
        'task_id',
        'start_date',
        'end_date',
        'start_lat',
        'start_lng',
        'end_lat',
        'end_lng',
        'note',
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'visit_detail_id' => 'integer',
        'task_id' => 'integer',
        'start_date' => 'string',
        'end_date' => 'string',
        'start_lat' => 'string',
        'start_lng' => 'string',
        'end_lat' => 'string',
        'end_lng' => 'string'
    ];

    public static $statuses = [
        'UNFINISHED' => 0,
        'FINISHED' => 1,
        'CANCELLED' => 2
    ];




    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

        $transformer = new VisitTaskDetail();

        $transformer->id = $this->id;
        $transformer->visit_detail_id = $this->visit_detail_id;
        $transformer->task_id = $this->task_id;
        $transformer->start_date = $this->start_date;
        $transformer->end_date = $this->end_date;
        $transformer->start_lat = $this->start_lat;
        $transformer->start_lng = $this->start_lng;
        $transformer->end_lat = $this->end_lat;
        $transformer->end_lng = $this->end_lng;
        $transformer->created_at = $this->created_at;
        $transformer->updated_at = $this->updated_at;

        return $transformer;

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(VisitTaskDetail $item) {

        });
        static::deleted(function(VisitTaskDetail $item) {


        });

    }

}
