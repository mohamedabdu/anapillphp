<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class StoreType extends MyModel
{
    use SoftDeletes;
    public $table = 'store_types';

    public function translations()
    {
        return $this->hasMany(StoreTypeTranslation::class, 'store_type_id');
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($store_type) { 
           
            
        });
        static::deleted(function ($item) { });
    }
}
