<?php

namespace App\Models;
use App\Models\Base\BaseModel;


class VisitTaskCancel extends BaseModel
{
   
    public $table = 'visit_task_cancels';
    
   

    public $fillable = [
    
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    
    ];

    public static $statuses = [
        'pending' => 0,
        'accepted' => 1,
        'rejected' => 2
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

       

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($item) {

        });
        static::deleted(function($item) {


        });

    }

}
