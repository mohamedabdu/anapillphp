<?php

namespace App\Models;

use App\Models\Base\BaseModel;
use App\Models\ReportProductsAlmostFinishedProduct;
use Carbon\Carbon;





/**
 * Class Report
 * @package App\Models
 * @version October 10, 2019, 5:23 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property string type
 */

class Report extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'reports';
    public static $types = [
        'products_almost_finish' => 1,
        'manual' => 2
    ];
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
        'type'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    protected $dates = [
        'created_at',
        'updated_at'
    ];


    public function  transform()
    {

        $transformer = new Report();

        $transformer->id = $this->id;
        $transformer->type = $this->type;
        $transformer->created_at = $this->created_at;
        $transformer->updated_at = $this->updated_at;

        return $transformer;
    }

    public function transformSupervisor()
    {
        Carbon::setLocale('ar');
        $transformer = new \stdClass();
        $transformer->date = $this->created_at->format('Y/m/d - H:i');
        $transformer->created_at = $this->created_at->diffForHumans();
        $transformer->store = $this->store;
        $transformer->notes = $this->notes ?: '-';
        if ($this->images) {
            $images = [];
            foreach (json_decode($this->images) as $image) {
                $images[] = \Helper\Common\imageUrl($image);
            }
            $transformer->images = $images;
        }
        if ($this->type == self::$types['products_almost_finish']) {
            $report_products = ReportProductsAlmostFinishedProduct::join('products', 'products.id', '=', 'reports_products_almost_finished_products.product_id')
                ->where('reports_products_almost_finished_products.report_id', $this->id)
                ->select('reports_products_almost_finished_products.*', 'products.title_ar as product')
                ->get();

            $transformer->products =  ReportProductsAlmostFinishedProduct::transformCollection($report_products, 'Supervisor');
        }
        return $transformer;
    }



    protected static function boot()
    {
        parent::boot();

        static::deleting(function (Report $item) { });
        static::deleted(function (Report $item) { });
    }
}
