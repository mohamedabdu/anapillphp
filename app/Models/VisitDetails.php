<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class VisitDetail
 * @package App\Models
 * @version October 10, 2019, 5:07 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer agent_id
 * @property integer visit_id
 * @property integer visit_store_id
 * @property string start_date
 * @property string start_time
 * @property string lat
 * @property string lng
 * @property string end_date
 * @property string end_time
 * @property integer status
 */

class VisitDetails extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'visit_details';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    'agent_id',
        'visit_id',
        'visit_store_id',
        'start_date',
        'start_time',
        'lat',
        'lng',
        'end_date',
        'end_time',
        'status'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'agent_id' => 'integer',
        'visit_id' => 'integer',
        'visit_store_id' => 'integer',
        'start_date' => 'string',
        'lat' => 'string',
        'lng' => 'string',
        'end_date' => 'string',
        'status' => 'integer'
    ];

    /** @var array $statuses */
    public static $statuses = [
        "UNFINISHED" => 0,
        "FINISHED" => 1,
        "CANCELLED" => 2,
        "CANCELED_BY_ADMIN" => 3,
        "CANCELED_BY_SUPERVISOR" => 4,

        0 => "UNFINISHED",
        1 => "FINISHED",
        2 => "CANCELLED",
        3 => "CANCELED_BY_ADMIN",
        4 => "CANCELED_BY_SUPERVISOR"

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

        $transformer = new VisitDetail();

        $transformer->id = $this->id;
        $transformer->agent_id = $this->agent_id;
        $transformer->visit_id = $this->visit_id;
        $transformer->visit_store_id = $this->visit_store_id;
        $transformer->start_date = $this->start_date;
        $transformer->lat = $this->lat;
        $transformer->lng = $this->lng;
        $transformer->end_date = $this->end_date;
        $transformer->status = $this->status;
        $transformer->created_at = $this->created_at;
        $transformer->updated_at = $this->updated_at;

        return $transformer;

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(VisitDetail $item) {

        });
        static::deleted(function(VisitDetail $item) {


        });

    }

}
