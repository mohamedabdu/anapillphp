<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class VisitTaskInventoryDetail
 * @package App\Models
 * @version October 10, 2019, 5:13 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer visit_task_detail_id
 * @property integer product_id
 * @property string price
 * @property string stock
 * @property string inventory
 * @property string num_of_stock
 * @property string num_of_inventory
 * @property string note
 */

class VisitTaskInventoryDetails extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'visit_task_inventory_details';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
        'visit_task_detail_id',
        'notes'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'visit_task_detail_id' => 'integer',
        'notes' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {
        

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(VisitTaskInventoryDetails $item) {

        });
        static::deleted(function(VisitTaskInventoryDetails $item) {


        });

    }

}
