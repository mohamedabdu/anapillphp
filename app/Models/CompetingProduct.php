<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;


class CompetingProduct extends MyModel
{
    use SoftDeletes;
    public $table = 'competing_products';
    
}
