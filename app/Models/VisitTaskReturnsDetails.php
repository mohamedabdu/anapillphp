<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class VisitTaskReturnDetail
 * @package App\Models
 * @version October 10, 2019, 5:12 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer visit_task_detail_id
 * @property string product_id
 * @property string bill_id
 * @property string number_piece
 * @property string number_of_carton
 * @property integer return_reason_id
 * @property string expire_end
 * @property string note
 * @property string images
 */

class VisitTaskReturnsDetails extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'visit_task_returns_details';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    'visit_task_detail_id',
        'product_id',
        'bill_id',
        'number_piece',
        'number_of_carton',
        'return_reason_id',
        'expire_end',
        "images",
        "note"
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'visit_task_detail_id' => 'integer',
        'products_id' => 'string',
        'bill_id' => 'string',
        'number_piece' => 'string',
        'number_of_carton' => 'string',
        'return_reason_id' => 'integer',
        'expire_end' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

        $transformer = new VisitTaskReturnDetail();

        $transformer->id = $this->id;
        $transformer->visit_task_detail_id = $this->visit_task_detail_id;
        $transformer->product_id = $this->product_id;
        $transformer->bill_id = $this->bill_id;
        $transformer->number_piece = $this->number_piece;
        $transformer->number_of_carton = $this->number_of_carton;
        $transformer->return_reason_id = $this->return_reason_id;
        $transformer->expire_end = $this->expire_end;
        $transformer->created_at = $this->created_at;
        $transformer->updated_at = $this->updated_at;

        return $transformer;

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(VisitTaskReturnDetail $item) {

        });
        static::deleted(function(VisitTaskReturnDetail $item) {


        });

    }

}
