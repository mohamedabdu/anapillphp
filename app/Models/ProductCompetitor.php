<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCompetitor extends MyModel
{

    use SoftDeletes;

    public $table = 'product_competitors';

    public function competing_products()
    {
        return $this->hasMany(CompetingProduct::class, 'product_competitor_id');
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($product_competitor) { 
            foreach ($product_competitor->competing_products as $competing_product) {
                $competing_product->delete();
            }
        });

        static::deleted(function () {
            
        });
    }


}
