<?php

namespace App\Models;
use App\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class Store
 * @package App\Models
 * @version October 10, 2019, 4:44 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property string title_ar
 * @property string title_en
 * @property string description_ar
 * @property string description_en
 * @property integer category_id
 * @property integer country_id
 * @property integer region_id
 * @property integer city_id
 * @property string logo
 * @property string lat
 * @property string lng
 * @property string address_note
 * @property integer is_main
 * @property integer parent_id
 * @property string mobile
 * @property string email
 * @property string phone
 * @property string manager_name
 * @property string manager_email
 * @property string manager_mobile
 * @property integer created_by
 */

class Store extends BaseModel
{

    use SoftDeletes;


    public $table = 'stores';

    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
        'title_ar',
        'title_en',
        'description_ar',
        'description_en',
        'category_id',
        'country_id',
        'region_id',
        'city_id',
        'logo',
        'lat',
        'lng',
        'address_note',
        'is_main',
        'parent_id',
        'mobile',
        'email',
        'phone',
        'manager_name',
        'manager_email',
        'manager_mobile',
        'created_by',
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title_ar' => 'string',
        'title_en' => 'string',
        'description_ar' => 'string',
        'description_en' => 'string',
        'category_id' => 'integer',
        'country_id' => 'integer',
        'region_id' => 'integer',
        'city_id' => 'integer',
        'logo' => 'string',
        'lat' => 'string',
        'lng' => 'string',
        'address_note' => 'string',
        'is_main' => 'integer',
        'parent_id' => 'integer',
        'mobile' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'manager_name' => 'string',
        'manager_email' => 'string',
        'manager_mobile' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
    public static $updateRules = [
        'title_ar' => 'required',
        'title_en' => 'required',
        'category_id' => 'required',
        'country_id' => 'required',
        'region_id' => 'required',
        'city_id' => 'required',
//        'lat' => 'required',
//        'lng' => 'required',
        'address_note' => 'required',
        'is_main' => 'required',
//        'parent_id' => 'required',
        'email' => 'email',
        'manager_email' => 'email',
        'manager_mobile' => 'numeric|regex:(05)|min:10',
        'mobile' => 'numeric|regex:(05)|min:10',
    ];

    public function  transform()
    {

        $transformer = new Store();

        $transformer->id = $this->id;
        $transformer->title = static::titleSlug($this,"title");
        $transformer->description = static::titleSlug($this,"description");
        $transformer->category_id = $this->category_id;
        $transformer->country_id = $this->country_id;
        $transformer->region_id = $this->region_id;
        $transformer->city_id = $this->city_id;
        $transformer->logo = $this->logo;
        $transformer->lat = $this->lat;
        $transformer->lng = $this->lng;
        $transformer->address_note = $this->address_note;
        $transformer->is_main = $this->is_main;
        $transformer->parent_id = $this->parent_id;
        $transformer->mobile = $this->mobile;
        $transformer->email = $this->email;
        $transformer->phone = $this->phone;
        $transformer->manager_name = $this->manager_name;
        $transformer->manager_email = $this->manager_email;
        $transformer->manager_mobile = $this->manager_mobile;
        return $transformer;

    }



    public function subCategories() {
        return $this->hasMany(StoreCategory::class, "store_id","id");
    }
    public function images() {
        return $this->hasMany(StoreImage::class, "store_id","id");
    }
    public function category() {
        return $this->belongsTo(Category::class, "category_id","id");
    }
    public function country() {
        return $this->belongsTo(Country::class, "country_id","id");
    }
    public function region() {
        return $this->belongsTo(Region::class, "region_id","id");
    }
    public function city() {
        return $this->belongsTo(City::class, "city_id","id");
    }
    protected static function boot()
    {
        parent::boot();

        static::deleting(function(Store $item) {

        });
        static::deleted(function(Store $item) {


        });

    }

}
