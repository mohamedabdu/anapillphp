<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class VisitTaskPromoDetail
 * @package App\Models
 * @version October 10, 2019, 5:17 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer visit_task_detail_id
 * @property integer promo_id
 * @property string num_of_promo
 * @property string promo_image
 * @property string promo_note
 * @property string note
 */

class VisitTaskCheckPromotionsDetails extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'visit_task_check_promotions_details';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {


    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(VisitTaskCheckPromotionsDetails $item) {

        });
        static::deleted(function(VisitTaskCheckPromotionsDetails $item) {


        });

    }

}
