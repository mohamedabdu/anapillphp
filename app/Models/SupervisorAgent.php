<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class SupervisorAgent
 * @package App\Models
 * @version October 10, 2019, 4:56 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer user_id
 * @property integer agent_id
 */

class SupervisorAgent extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'supervisor_agents';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    'user_id',
        'agent_id'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'user_id' => 'integer',
        'agent_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

        $transformer = new SupervisorAgent();

        $transformer->id = $this->id;
        $transformer->user_id = $this->user_id;
        $transformer->agent_id = $this->agent_id;
        $transformer->created_at = $this->created_at;
        $transformer->updated_at = $this->updated_at;

        return $transformer;

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(SupervisorAgent $item) {

        });
        static::deleted(function(SupervisorAgent $item) {


        });

    }

}
