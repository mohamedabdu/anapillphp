<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class VisitTaskBillDetail
 * @package App\Models
 * @version October 10, 2019, 5:16 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer visit_task_detail_id
 * @property string bill_id
 * @property string bill_date
 * @property string bill_image
 * @property string bill_total
 * @property string products
 * @property string images
 */

class VisitTaskReceivingOrderDetails extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'visit_task_receiving_order_details';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
   
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(VisitTaskReceivingOrderDetails $item) {

        });
        static::deleted(function(VisitTaskReceivingOrderDetails $item) {


        });

    }

}
