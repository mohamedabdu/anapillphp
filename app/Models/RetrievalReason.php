<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class RetrievalReason extends MyModel
{
    use SoftDeletes;
    public $table = 'retrieval_reasons';

    public function translations()
    {
        return $this->hasMany(RetrievalReasonTranslation::class, 'retrieval_reason_id');
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($retrieval_reason) { 
           
            
        });
        static::deleted(function ($item) { });
    }
}
