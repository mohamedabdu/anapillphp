<?php

namespace App\Models;
use App\Models\Base\BaseModel;
use function Helper\Common\imageUrl;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class TaskInstruction
 * @package App\Models
 * @version October 10, 2019, 4:58 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer task_id
 * @property string description_ar
 * @property string description_en
 */

class TaskInstruction extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'task_instructions';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    'task_id',
        'description_ar',
        'description_en',
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'task_id' => 'integer',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

        $transformer = new TaskInstruction();
        $transformer->description = static::titleSlug($this,"description");
        $images = [];
        foreach ($this->images()->get() as $item) {
            $images[] = imageUrl($item->image);
        }
        $transformer->images = $images;
        return $transformer;

    }



    public function task() {
        return $this->belongsTo(Task::class, "task_id","id");
    }
    public function images() {
        return $this->hasMany(TaskInstructionImage::class, "task_instruction_id","id");
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(TaskInstruction $item) {

        });
        static::deleted(function(TaskInstruction $item) {


        });

    }

}
