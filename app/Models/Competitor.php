<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Competitor extends MyModel
{
    use SoftDeletes;
    public $table = 'competitors';

    public static function getAll()
    {
        $data =  static::join('competitor_translations', function ($query) {
                    $query->on('competitors.id', '=', 'competitor_translations.competitor_id')
                        ->where('competitor_translations.locale', static::getLangCode());
                    });
        $data = self::checkCreatedBy($data, 'competitors')
                    ->select('competitors.*', 'competitor_translations.title')
                    ->get();
        return $data;
    }
    
    public function translations()
    {
        return $this->hasMany(CompetitorTranslation::class, 'competitor_id');
    }

    public static function transformAdmin($item)
    {
        $transformer = new \stdClass();
        $transformer->id = en_de_crypt($item->id);
        $transformer->title = $item->title;

        return $transformer;
    }
    

    protected static function boot()
    {
        parent::boot();
        static::deleting(function () { 
           
            
        });
        static::deleted(function () {

         });
    }
}
