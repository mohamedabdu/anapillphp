<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class CompetitorProduct extends MyModel
{
    use SoftDeletes;
    public $table = 'competitor_products';

    public static function getAll($cometitor_id = false)
    {
        $data =  static::join('competitor_product_translations', function ($query) {
            $query->on('competitor_products.id', '=', 'competitor_product_translations.competitor_product_id')
                ->where('competitor_product_translations.locale', static::getLangCode());
        });
        if ($cometitor_id) {
            $data->where('competitor_products.competitor_id', $cometitor_id);
        }
        $data = self::checkCreatedBy($data, 'competitor_products')
            ->select('competitor_products.*', 'competitor_product_translations.title')
            ->get();
        return $data;
    }
   

    public function translations()
    {
        return $this->hasMany(CompetitorProductTranslation::class, 'competitor_product_id');
    }

    public static function transformAdmin($item)
    {
        $transformer = new \stdClass();
        $transformer->id = en_de_crypt($item->id);
        $transformer->title = $item->title;

        return $transformer;
    }


    protected static function boot()
    {
        parent::boot();
        static::deleting(function () { 
           
            
        });
        static::deleted(function () { });
    }
}
