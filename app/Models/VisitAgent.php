<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class VisitAgent
 * @package App\Models
 * @version October 10, 2019, 5:01 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer visit_id
 * @property integer agent_id
 * @property integer status
 */

class VisitAgent extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'visit_agents';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    'visit_id',
        'agent_id',
        'status'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'visit_id' => 'integer',
        'agent_id' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

        $transformer = new VisitAgent();

        $transformer->id = $this->id;
        $transformer->visit_id = $this->visit_id;
        $transformer->agent_id = $this->agent_id;
        $transformer->status = $this->status;
        $transformer->created_at = $this->created_at;
        $transformer->updated_at = $this->updated_at;

        return $transformer;

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(VisitAgent $item) {

        });
        static::deleted(function(VisitAgent $item) {


        });

    }

}
