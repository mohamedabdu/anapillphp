<?php

namespace App\Models;

class Role extends MyModel
{

    public $table = 'roles';

    public function permissions()
    {
        return $this->hasMany(RolePermission::class, "role_id", "id");
    }

    public function translations()
    {
        return $this->hasMany(RoleTranslation::class, 'role_id');
    }

    public static function transformAdmin($item)
    {
        $transformer = new \stdClass();
        $transformer->id = en_de_crypt($item->id);
        $transformer->title = $item->title;
        return $transformer;
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($role) { 
            foreach ($role->permissions as $permissions) {
                $permissions->delete();
            }
            foreach ($role->translations as $translation) {
                $translation->delete();
            }
        });
        static::deleted(function ($item) { });
    }
}
