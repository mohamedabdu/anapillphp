<?php

namespace App\Models;
use App\Models\Base\BaseModel;



class Task extends BaseModel
{
    
    public $table = 'tasks';

    /**
     1 => ترتيب الشلفات ( ترتيب الرف )
     2 => مهمة المرتجعات
     3 => مهمة جرد عام
     4 => مهمة الجرد المفصل
     5 => مهمة تقرير أسعار
     6 => مهمة تقرير مساحات
    7 => مهمة إستلام طلبية
     8 => مهمة فحص العروض الترويجية
     *
     */

    /** @var array  */
    public $fillable = [
    'title_ar',
        'title_en',
        'process'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'title_ar' => 'string',
        'title_en' => 'string',
        'process' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {
        $transformer = new Task();
        $transformer->id = $this->id;
        $transformer->title = static::titleSlug($this,"title");
        return $transformer;
    }



    public function  transformFull()
    {

        $transformer = new Task();

        $transformer->id = $this->id;
        $transformer->title = static::titleSlug($this,"title");
        $notes = TaskInstruction::where("task_instructions.task_id",$this->id)->get();
        $transformer->notes = $notes->transformCollection();
        return $transformer;

    }


    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(Task $item) {

        });
        static::deleted(function(Task $item) {


        });

    }

}
