<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class Notification
 * @package App\Models
 * @version October 10, 2019, 4:30 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer user_id
 * @property string title
 * @property string body
 * @property integer type
 * @property integer created_by
 */

class Notification extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'notifications';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    'user_id',
        'title',
        'body',
        'type',
        'created_by',
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    'user_id' => 'integer',
        'title' => 'string',
        'body' => 'string',
        'type' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];

    public static $types = [
        0 => "GENERAL",
        "GENERAL" => 0,

    ];

    public function  transform()
    {

        $transformer = new Notification();

        $transformer->id = $this->id;
        $transformer->title = $this->title;
        $transformer->body = $this->body;
        $transformer->type = $this->type;
        $transformer->created_at = $this->created_at;

        return $transformer;

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(Notification $item) {

        });
        static::deleted(function(Notification $item) {


        });

    }

}
