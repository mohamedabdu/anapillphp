<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class ReportDetail
 * @package App\Models
 * @version October 10, 2019, 5:26 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer product_id
 * @property string num_of_cans
 * @property string num_cartons
 * @property string end_date
 * @property string note
 * @property string title
 * @property integer type
 * @property integer images
 * @property integer store_id
 */

class ReportProductsAlmostFinishedProduct extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'reports_products_almost_finished_products';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
    
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
   
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];

    public function transform()
    {
        
    }
    public function transformSupervisor()
    {
        $transformer = new \stdClass();
        $transformer->name = $this->product;
        $transformer->pieces_number = $this->pieces_number;
        $transformer->cartons_number = $this->cartons_number ?: '-';
        $transformer->date = $this->date;

        return $transformer;
    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(ReportProductsAlmostFinishedProduct $item) {

        });
        static::deleted(function(ReportProductsAlmostFinishedProduct $item) {


        });

    }

}
