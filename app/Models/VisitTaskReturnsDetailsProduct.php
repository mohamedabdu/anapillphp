<?php

namespace App\Models;
use App\Models\Base\BaseModel;

/*
use Illuminate\Database\Eloquent\SoftDeletes;

*/

/**
 * Class VisitTaskReturnDetail
 * @package App\Models
 * @version October 10, 2019, 5:12 pm EET
 *
 * @property integer id
 * @property string created_at
 * @property string updated_at
 * @property integer visit_task_detail_id
 * @property string product_id
 * @property string bill_id
 * @property string number_piece
 * @property string number_of_carton
 * @property integer return_reason_id
 * @property string expire_end
 * @property string note
 * @property string images
 */

class VisitTaskReturnsDetailsProduct extends BaseModel
{
    /*
        use SoftDeletes;

    */

    public $table = 'visit_task_returns_details_products';
    
    /*

    protected $dates = ['deleted_at'];

*/

    public $fillable = [
        'visit_task_returns_details_id',
        'product_id',
        'pieces_number',
        'cartons_number',
        'return_reason_id',
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'visit_task_returns_details_id' => 'integer',
        'products_id' => 'string',
        'pieces_number' => 'integer',
        'cartons_number' => 'integer',
        'return_reason_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    
    ];


    public function  transform()
    {

       

    }



    

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(VisitTaskReturnsDetailsProduct $item) {

        });
        static::deleted(function(VisitTaskReturnsDetailsProduct $item) {


        });

    }

}
