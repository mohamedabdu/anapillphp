<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Promotion extends MyModel
{
    use SoftDeletes;
    public $table = 'promotions';

    public function translations()
    {
        return $this->hasMany(PromotionTranslation::class, 'promotion_id');
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($promotion) { 
        
        });
        
        static::deleted(function ($promotion) {
            self::deleteUploaded('promotions',$promotion->image);
        });
    }
}
