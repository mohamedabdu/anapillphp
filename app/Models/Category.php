<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends MyModel
{
    use SoftDeletes;
    public $table = 'categories';
    
    public static $types = [
        "product" => 1,
        "store"=> 2
    ];

    public static function getAll($type, $parent_id = 0)
    {
        $data =   static::join('category_translations', function ($query) {
                    $query->on('categories.id', '=', 'category_translations.category_id')
                        ->where('category_translations.locale', static::getLangCode());
                })
                ->where('categories.parent_id', $parent_id)
                ->where('categories.type', $type);
                
        $data = self::checkCreatedBy($data, 'categories')
                ->select('categories.*', 'category_translations.title')
                ->get();
        
        return $data;
    }

    public function translations()
    {
        return $this->hasMany(CategoryTranslation::class, 'category_id');
    }
    public function childs()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public static function transformAdmin($item)
    {
        $transformer = new \stdClass();
        $transformer->id = en_de_crypt($item->id);
        $transformer->title = $item->title;

        return $transformer;
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function() {
            
        });
        static::deleted(function() {


        });

    }

}
