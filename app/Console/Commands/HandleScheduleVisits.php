<?php

namespace App\Console\Commands;

use App\Traits\VisitTrait;
use Illuminate\Console\Command;

class HandleScheduleVisits extends Command
{
    use VisitTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'handle:scheduleVisits';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creating Up coming of scheduling visits';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->handleScheduleVisits();
    }
}
