<?php

namespace App\Traits;


use App\Models\Config;
use App\Models\Visit;
use App\Models\VisitAgent;
use App\Models\VisitDetail;
use App\Models\VisitStore;
use Illuminate\Support\Facades\DB;

trait VisitTrait {

    public function handleScheduleVisits() {
        $today = date("Y-m-d");
        $visits = Visit::join("visit_details","visits.id","=","visit_details.visit_id")
            ->where("visit_details.status",Visit::$statuses["PENDING"])->where("visit_details.start_date", $today)
            ->groupBy("visit_details.visit_id")
            ->select(["visits.*","visit_details.start_date as detail_date",
                DB::raw("CEILING((select count(id) from visit_details where visit_details.visit_id = visits.id) / (select count(id) from visit_stores where visit_stores.visit_id = visits.id)) as visit_rows")])
            ->get();

        DB::beginTransaction();

        foreach ($visits as $visit) {
            if(!$visit->on_going && $visit->visit_rows == $visit->num_of_visits) {
                $visit->status = Visit::$statuses["FINISHED"];
                $visit->save();
            } else {
                $visit->start_date = $visit->detail_date;
                $nextDate = $this->checkNextDate($visit);
                $created = $this->createNewDetail($visit, $nextDate);
            }
        }
        if(isset($created) && !$created) {
            DB::rollback();
        } else {
            DB::commit();
        }
    }

    private function createNewDetail(Visit $visit, $date ) {
        try {
            $agent = VisitAgent::where("visit_id",$visit->id)->first();
            $ids = VisitStore::where("visit_id",$visit->id)->pluck('id')->toArray();
            foreach ($ids as $id) {
                $detail = new VisitDetail();
                $detail->visit_store_id = $id;
                $detail->visit_id = $visit->id;
                $detail->agent_id = $agent->agent_id;
                $detail->start_date = $date;
                $detail->lat = 0;
                $detail->lng = 0;
                $detail->status = Visit::$statuses["PENDING"];
                $detail->save();
            }
            return true;
        } catch(\Exception $e) {
            return false;
        }

    }
    private function createNewDetailByID(Visit $visit, $date, $withTimes = false) {
        try {
            $agent = VisitAgent::where("visit_id",$visit->id)->first();
            $ids = VisitStore::where("visit_id",$visit->id)->pluck('id')->toArray();
            foreach ($ids as $id) {
                $detail = new VisitDetail();
                $detail->visit_store_id = $id;
                $detail->visit_id = $visit->id;
                $detail->agent_id = $agent->agent_id;
                $detail->start_date = $date;
                $detail->lat = 0;
                $detail->lng = 0;
                $detail->status = Visit::$statuses["PENDING"];
                if($withTimes == true) {
                    $detail->start_time = date("H:i:s");
                }
                $detail->save();
            }
            return $detail->id;
        } catch(\Exception $e) {
            return false;
        }

    }
    private function createNewDetailForStore(Visit $visit, $storeID, $date, $withTimes = false) {
        try {
            $agent = VisitAgent::where("visit_id",$visit->id)->first();
            $ids = VisitStore::where("visit_id",$visit->id)->where("store_id",$storeID)->first();
            $detail = new VisitDetail();
            $detail->visit_store_id = $ids->id;
            $detail->visit_id = $visit->id;
            $detail->agent_id = $agent->agent_id;
            $detail->start_date = $date;
            $detail->lat = 0;
            $detail->lng = 0;
            $detail->status = Visit::$statuses["PENDING"];
            if($withTimes == true) {
                $detail->start_time = date("H:i:s");
            }
            $detail->save();
            return $detail->id;
        } catch(\Exception $e) {
            return false;
        }

    }
    private function checkNextDate(Visit $visit) {
        $holidays = json_decode($visit->holidays);
        if($holidays == null) {
            $holidays = Config::latest("id")->first()->system_holidays;
            try {
                $holidays = json_decode($holidays);
            } catch (\Exception $e) {
                $holidays = null;
            }
        }
        if($holidays == null) {
            $holidays = [];
        }
        $nextDay = date("Y-m-d", strtotime($visit->start_date));

        if($visit->repeat_every == 1) {
            $nextDay = date("Y-m-d", strtotime($nextDay."+0 day"));
            $day = date("D", strtotime($nextDay));
            if(!in_array($day, $holidays)) {

            }
            return $nextDay;
        } else {
            for($i = 1; $i <= $visit->repeat_every;) {
                $nextDay = date("Y-m-d", strtotime($nextDay."+1 day"));
                $day = date("D", strtotime($nextDay));
                if(!in_array($day, $holidays)) {
                    $i++;
                }
            }
        }

        return $nextDay;
    }
    private function nextDayRecursive($nextDay, $holidays) {
        $nextDay = date("Y-m-d", strtotime($nextDay."+1 day"));
        $day = date("D", strtotime($nextDay));
        if(!in_array($day, $holidays)) {
            return $nextDay;
        } else {
            return $this->nextDayRecursive($nextDay, $holidays);
        }
    }
    private function scheduleVisits($agentID, $customDate = null) {
        if($customDate == null) {
            $date = date("Y-m-d");
        } else {
            $date = $customDate;
        }
        $model = new Visit();
        $model = $model->where("visits.type",Visit::$types["scheduled"])->where("visits.start_date",">",$date)
            ->where("visits.status",Visit::$statuses["PENDING"])->paginate();
        $collection = collect($model->items());
        return $collection;
    }
    private function normalVisits($agentID, $customDate = null) {
        if($customDate == null) {
            $date = date("Y-m-d");
        } else {
            $date = $customDate;
        }
        $model = new Visit();
        $model = $model->where("visits.type",Visit::$types["normal"])->where("visits.start_date",">",$date)
            ->where("visits.status",Visit::$statuses["PENDING"])->paginate();
        $collection = collect($model->items());
        return $collection;
    }
}