<?php

namespace App\Traits;

use DB;

trait Basic {

    protected $languages = array(
        'en' => 'english',
        'ar' => 'arabic'
    );

    protected static function getLangCode()
    {
        $lang_code = app()->getLocale();
        return $lang_code;
    }
    
    protected function lang_rules($columns_arr = array())
    {
        $rules = array();

        if (!empty($columns_arr)) {
            foreach ($columns_arr as $column => $rule) {
                foreach ($this->languages as $lang_key => $locale) {
                    $key = $column . '.' . $lang_key;
                    $rules[$key] = $rule;
                }
            }
        }
        return $rules;
    }


}
