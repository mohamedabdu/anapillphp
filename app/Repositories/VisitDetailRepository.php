<?php

namespace App\Repositories;

use App\Models\VisitDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VisitDetailRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:07 pm EET
 *
 * @method VisitDetail findWithoutFail($id, $columns = ['*'])
 * @method VisitDetail find($id, $columns = ['*'])
 * @method VisitDetail first($columns = ['*'])
*/
class VisitDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'agent_id',
        'visit_id',
        'visit_store_id',
        'start_date',
        'lat',
        'lng',
        'end_date',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VisitDetail::class;
    }
}
