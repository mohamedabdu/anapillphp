<?php

namespace App\Repositories;

use App\Models\VisitTaskReturnDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VisitTaskReturnDetailRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:12 pm EET
 *
 * @method VisitTaskReturnDetail findWithoutFail($id, $columns = ['*'])
 * @method VisitTaskReturnDetail find($id, $columns = ['*'])
 * @method VisitTaskReturnDetail first($columns = ['*'])
*/
class VisitTaskReturnDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'visit_task_detail_id',
        'products_id',
        'bill_id',
        'number_piece',
        'number_of_carton',
        'return_reason_id',
        'expire_end'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VisitTaskReturnDetail::class;
    }
}
