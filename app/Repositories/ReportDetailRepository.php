<?php

namespace App\Repositories;

use App\Models\ReportDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReportDetailRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:26 pm EET
 *
 * @method ReportDetail findWithoutFail($id, $columns = ['*'])
 * @method ReportDetail find($id, $columns = ['*'])
 * @method ReportDetail first($columns = ['*'])
*/
class ReportDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'num_of_cans',
        'num_cartons',
        'end_date',
        'note',
        'title',
        'type'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReportDetail::class;
    }
}
