<?php

namespace App\Repositories;

use App\Models\AgentRate;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AgentRateRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:20 pm EET
 *
 * @method AgentRate findWithoutFail($id, $columns = ['*'])
 * @method AgentRate find($id, $columns = ['*'])
 * @method AgentRate first($columns = ['*'])
*/
class AgentRateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'agent_id',
        'rate',
        'note',
        'date'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AgentRate::class;
    }
}
