<?php

namespace App\Repositories;

use App\Models\PromoDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PromoDetailRepository
 * @package App\Repositories
 * @version October 10, 2019, 4:39 pm EET
 *
 * @method PromoDetail findWithoutFail($id, $columns = ['*'])
 * @method PromoDetail find($id, $columns = ['*'])
 * @method PromoDetail first($columns = ['*'])
*/
class PromoDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'promo_id',
        'store_id',
        'promo_num',
        'start_date',
        'end_date',
        'contract_images'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PromoDetail::class;
    }
}
