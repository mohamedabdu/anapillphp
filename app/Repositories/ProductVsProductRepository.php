<?php

namespace App\Repositories;

use App\Models\ProductVsProduct;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProductVsProductRepository
 * @package App\Repositories
 * @version October 10, 2019, 4:53 pm EET
 *
 * @method ProductVsProduct findWithoutFail($id, $columns = ['*'])
 * @method ProductVsProduct find($id, $columns = ['*'])
 * @method ProductVsProduct first($columns = ['*'])
*/
class ProductVsProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'product_vs_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductVsProduct::class;
    }
}
