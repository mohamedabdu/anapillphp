<?php

namespace App\Repositories;

use App\Models\VisitTaskPromoDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VisitTaskPromoDetailRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:17 pm EET
 *
 * @method VisitTaskPromoDetail findWithoutFail($id, $columns = ['*'])
 * @method VisitTaskPromoDetail find($id, $columns = ['*'])
 * @method VisitTaskPromoDetail first($columns = ['*'])
*/
class VisitTaskPromoDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'visit_task_detail_id',
        'promo_id',
        'num_of_promo',
        'promo_image',
        'promo_note',
        'note'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VisitTaskPromoDetail::class;
    }
}
