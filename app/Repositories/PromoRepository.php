<?php

namespace App\Repositories;

use App\Models\Promo;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PromoRepository
 * @package App\Repositories
 * @version October 10, 2019, 4:38 pm EET
 *
 * @method Promo findWithoutFail($id, $columns = ['*'])
 * @method Promo find($id, $columns = ['*'])
 * @method Promo first($columns = ['*'])
*/
class PromoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title_ar',
        'title_en'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Promo::class;
    }
}
