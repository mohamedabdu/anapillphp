<?php

namespace App\Repositories;

use App\Models\returnReason;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class returnReasonRepository
 * @package App\Repositories
 * @version October 10, 2019, 4:31 pm EET
 *
 * @method returnReason findWithoutFail($id, $columns = ['*'])
 * @method returnReason find($id, $columns = ['*'])
 * @method returnReason first($columns = ['*'])
*/
class returnReasonRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title_ar',
        'title_en'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return returnReason::class;
    }
}
