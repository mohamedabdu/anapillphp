<?php

namespace App\Repositories;

use App\Models\VisitStore;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VisitStoreRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:01 pm EET
 *
 * @method VisitStore findWithoutFail($id, $columns = ['*'])
 * @method VisitStore find($id, $columns = ['*'])
 * @method VisitStore first($columns = ['*'])
*/
class VisitStoreRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'visit_id',
        'store_id',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VisitStore::class;
    }
}
