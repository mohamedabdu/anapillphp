<?php

namespace App\Repositories;

use App\Models\TaskInstruction;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TaskInstructionRepository
 * @package App\Repositories
 * @version October 10, 2019, 4:58 pm EET
 *
 * @method TaskInstruction findWithoutFail($id, $columns = ['*'])
 * @method TaskInstruction find($id, $columns = ['*'])
 * @method TaskInstruction first($columns = ['*'])
*/
class TaskInstructionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'task_id',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TaskInstruction::class;
    }
}
