<?php

namespace App\Repositories;

use App\Models\VisitTaskAreaDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VisitTaskAreaDetailRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:14 pm EET
 *
 * @method VisitTaskAreaDetail findWithoutFail($id, $columns = ['*'])
 * @method VisitTaskAreaDetail find($id, $columns = ['*'])
 * @method VisitTaskAreaDetail first($columns = ['*'])
*/
class VisitTaskAreaDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'visit_task_detail_id',
        'category_id',
        'width',
        'height'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VisitTaskAreaDetail::class;
    }
}
