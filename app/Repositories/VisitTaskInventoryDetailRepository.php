<?php

namespace App\Repositories;

use App\Models\VisitTaskInventoryDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VisitTaskInventoryDetailRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:13 pm EET
 *
 * @method VisitTaskInventoryDetail findWithoutFail($id, $columns = ['*'])
 * @method VisitTaskInventoryDetail find($id, $columns = ['*'])
 * @method VisitTaskInventoryDetail first($columns = ['*'])
*/
class VisitTaskInventoryDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'visit_task_detail_id',
        'product_id',
        'price',
        'stock',
        'inventory',
        'num_of_stock',
        'num_of_inventory',
        'note'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VisitTaskInventoryDetail::class;
    }
}
