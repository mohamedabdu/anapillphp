<?php

namespace App\Repositories;

use App\Models\Visit;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VisitRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:00 pm EET
 *
 * @method Visit findWithoutFail($id, $columns = ['*'])
 * @method Visit find($id, $columns = ['*'])
 * @method Visit first($columns = ['*'])
*/
class VisitRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'start_date',
        'on_going',
        'num_of_visits',
        'reapet',
        'is_normal',
        'status',
        'created_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Visit::class;
    }
}
