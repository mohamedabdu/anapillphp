<?php

namespace App\Repositories;

use App\Models\ReportDetailImage;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReportDetailImageRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:26 pm EET
 *
 * @method ReportDetailImage findWithoutFail($id, $columns = ['*'])
 * @method ReportDetailImage find($id, $columns = ['*'])
 * @method ReportDetailImage first($columns = ['*'])
*/
class ReportDetailImageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'report_id',
        'image'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReportDetailImage::class;
    }
}
