<?php

namespace App\Repositories;

use App\Models\StoreWorkDay;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StoreWorkDayRepository
 * @package App\Repositories
 * @version October 10, 2019, 4:46 pm EET
 *
 * @method StoreWorkDay findWithoutFail($id, $columns = ['*'])
 * @method StoreWorkDay find($id, $columns = ['*'])
 * @method StoreWorkDay first($columns = ['*'])
*/
class StoreWorkDayRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'day',
        'work_from',
        'work_to',
        'is_custom'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StoreWorkDay::class;
    }
}
