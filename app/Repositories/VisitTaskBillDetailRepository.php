<?php

namespace App\Repositories;

use App\Models\VisitTaskBillDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VisitTaskBillDetailRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:16 pm EET
 *
 * @method VisitTaskBillDetail findWithoutFail($id, $columns = ['*'])
 * @method VisitTaskBillDetail find($id, $columns = ['*'])
 * @method VisitTaskBillDetail first($columns = ['*'])
*/
class VisitTaskBillDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'visit_task_detail_id',
        'bill_id',
        'bill_date',
        'driver_name',
        'bill_image',
        'bill_total'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VisitTaskBillDetail::class;
    }
}
