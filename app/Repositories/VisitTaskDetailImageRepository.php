<?php

namespace App\Repositories;

use App\Models\VisitTaskDetailImage;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VisitTaskDetailImageRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:10 pm EET
 *
 * @method VisitTaskDetailImage findWithoutFail($id, $columns = ['*'])
 * @method VisitTaskDetailImage find($id, $columns = ['*'])
 * @method VisitTaskDetailImage first($columns = ['*'])
*/
class VisitTaskDetailImageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'visit_detail_id',
        'note',
        'image'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VisitTaskDetailImage::class;
    }
}
