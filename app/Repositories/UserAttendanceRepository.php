<?php

namespace App\Repositories;

use App\Models\UserAttendance;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UserAttendanceRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:18 pm EET
 *
 * @method UserAttendance findWithoutFail($id, $columns = ['*'])
 * @method UserAttendance find($id, $columns = ['*'])
 * @method UserAttendance first($columns = ['*'])
*/
class UserAttendanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'date',
        'is_end',
        'end_date',
        'image',
        'lat',
        'lng'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserAttendance::class;
    }
}
