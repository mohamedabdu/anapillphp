<?php

namespace App\Repositories;

use App\Models\StoreImage;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StoreImageRepository
 * @package App\Repositories
 * @version October 10, 2019, 4:45 pm EET
 *
 * @method StoreImage findWithoutFail($id, $columns = ['*'])
 * @method StoreImage find($id, $columns = ['*'])
 * @method StoreImage first($columns = ['*'])
*/
class StoreImageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'image'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StoreImage::class;
    }
}
