<?php

namespace App\Repositories;

use App\Models\VisitCancel;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VisitCancelRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:08 pm EET
 *
 * @method VisitCancel findWithoutFail($id, $columns = ['*'])
 * @method VisitCancel find($id, $columns = ['*'])
 * @method VisitCancel first($columns = ['*'])
*/
class VisitCancelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'agent_id',
        'visit_id',
        'visit_store_id',
        'note',
        'is_agree'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VisitCancel::class;
    }
}
