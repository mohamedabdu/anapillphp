<?php

namespace App\Repositories;

use App\Models\TaskInstructionImage;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TaskInstructionImageRepository
 * @package App\Repositories
 * @version October 10, 2019, 4:58 pm EET
 *
 * @method TaskInstructionImage findWithoutFail($id, $columns = ['*'])
 * @method TaskInstructionImage find($id, $columns = ['*'])
 * @method TaskInstructionImage first($columns = ['*'])
*/
class TaskInstructionImageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'task_instruction_id',
        'image'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TaskInstructionImage::class;
    }
}
