<?php

namespace App\Repositories;

use App\Models\AgentHoliday;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AgentHolidayRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:19 pm EET
 *
 * @method AgentHoliday findWithoutFail($id, $columns = ['*'])
 * @method AgentHoliday find($id, $columns = ['*'])
 * @method AgentHoliday first($columns = ['*'])
*/
class AgentHolidayRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'agent_id',
        'title',
        'reason',
        'start_date',
        'end_date',
        'status',
        'refuse_note'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AgentHoliday::class;
    }
}
