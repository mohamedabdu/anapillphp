<?php

namespace App\Repositories;

use App\Models\VisitTask;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VisitTaskRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:01 pm EET
 *
 * @method VisitTask findWithoutFail($id, $columns = ['*'])
 * @method VisitTask find($id, $columns = ['*'])
 * @method VisitTask first($columns = ['*'])
*/
class VisitTaskRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'visit_id',
        'task_id',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VisitTask::class;
    }
}
