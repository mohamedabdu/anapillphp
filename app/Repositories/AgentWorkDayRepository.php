<?php

namespace App\Repositories;

use App\Models\AgentWorkDay;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AgentWorkDayRepository
 * @package App\Repositories
 * @version October 10, 2019, 4:56 pm EET
 *
 * @method AgentWorkDay findWithoutFail($id, $columns = ['*'])
 * @method AgentWorkDay find($id, $columns = ['*'])
 * @method AgentWorkDay first($columns = ['*'])
*/
class AgentWorkDayRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'day'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AgentWorkDay::class;
    }
}
