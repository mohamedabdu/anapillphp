<?php

namespace App\Repositories;

use App\Models\ProductImage;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProductImageRepository
 * @package App\Repositories
 * @version October 10, 2019, 4:49 pm EET
 *
 * @method ProductImage findWithoutFail($id, $columns = ['*'])
 * @method ProductImage find($id, $columns = ['*'])
 * @method ProductImage first($columns = ['*'])
*/
class ProductImageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'image'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductImage::class;
    }
}
