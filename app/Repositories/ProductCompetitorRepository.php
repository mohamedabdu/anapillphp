<?php

namespace App\Repositories;

use App\Models\ProductCompetitor;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProductCompetitorRepository
 * @package App\Repositories
 * @version October 10, 2019, 4:53 pm EET
 *
 * @method ProductCompetitor findWithoutFail($id, $columns = ['*'])
 * @method ProductCompetitor find($id, $columns = ['*'])
 * @method ProductCompetitor first($columns = ['*'])
*/
class ProductCompetitorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'competitor_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductCompetitor::class;
    }
}
