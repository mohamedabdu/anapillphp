<?php

namespace App\Repositories;

use App\Models\VisitTaskCancel;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VisitTaskCancelRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:21 pm EET
 *
 * @method VisitTaskCancel findWithoutFail($id, $columns = ['*'])
 * @method VisitTaskCancel find($id, $columns = ['*'])
 * @method VisitTaskCancel first($columns = ['*'])
*/
class VisitTaskCancelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'visit_id',
        'task_id',
        'visit_detail_id',
        'note',
        'is_agree'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VisitTaskCancel::class;
    }
}
