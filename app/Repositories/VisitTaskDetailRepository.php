<?php

namespace App\Repositories;

use App\Models\VisitTaskDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VisitTaskDetailRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:09 pm EET
 *
 * @method VisitTaskDetail findWithoutFail($id, $columns = ['*'])
 * @method VisitTaskDetail find($id, $columns = ['*'])
 * @method VisitTaskDetail first($columns = ['*'])
*/
class VisitTaskDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'visit_detail_id',
        'task_id',
        'start_date',
        'end_date',
        'start_lat',
        'start_lng',
        'end_lat',
        'end_lng'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VisitTaskDetail::class;
    }
}
