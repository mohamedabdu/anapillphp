<?php

namespace App\Repositories;

use App\Models\VisitAgent;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VisitAgentRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:01 pm EET
 *
 * @method VisitAgent findWithoutFail($id, $columns = ['*'])
 * @method VisitAgent find($id, $columns = ['*'])
 * @method VisitAgent first($columns = ['*'])
*/
class VisitAgentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'visit_id',
        'agent_id',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VisitAgent::class;
    }
}
