<?php

namespace App\Repositories;

use App\Models\Store;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StoreRepository
 * @package App\Repositories
 * @version October 10, 2019, 4:44 pm EET
 *
 * @method Store findWithoutFail($id, $columns = ['*'])
 * @method Store find($id, $columns = ['*'])
 * @method Store first($columns = ['*'])
*/
class StoreRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title_ar',
        'title_en',
        'description_ar',
        'description_en',
        'category_id',
        'country_id',
        'region_id',
        'city_id',
        'logo',
        'lat',
        'lng',
        'address_note',
        'is_main',
        'parent_id',
        'mobile',
        'email',
        'phone',
        'manager_name',
        'manager_email',
        'manager_mobile'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Store::class;
    }
}
