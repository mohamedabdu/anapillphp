<?php

namespace App\Repositories;

use App\Models\Competitor;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CompetitorRepository
 * @package App\Repositories
 * @version October 10, 2019, 4:48 pm EET
 *
 * @method Competitor findWithoutFail($id, $columns = ['*'])
 * @method Competitor find($id, $columns = ['*'])
 * @method Competitor first($columns = ['*'])
*/
class CompetitorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title_ar',
        'title_en',
        'category_id',
        'sub_category_id',
        'country_id',
        'region_id',
        'city_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Competitor::class;
    }
}
