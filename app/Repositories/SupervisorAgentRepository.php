<?php

namespace App\Repositories;

use App\Models\SupervisorAgent;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SupervisorAgentRepository
 * @package App\Repositories
 * @version October 10, 2019, 4:56 pm EET
 *
 * @method SupervisorAgent findWithoutFail($id, $columns = ['*'])
 * @method SupervisorAgent find($id, $columns = ['*'])
 * @method SupervisorAgent first($columns = ['*'])
*/
class SupervisorAgentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'agent_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SupervisorAgent::class;
    }
}
