<?php

namespace App\Repositories;

use App\Models\ReportCompetitor;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReportCompetitorRepository
 * @package App\Repositories
 * @version October 10, 2019, 5:29 pm EET
 *
 * @method ReportCompetitor findWithoutFail($id, $columns = ['*'])
 * @method ReportCompetitor find($id, $columns = ['*'])
 * @method ReportCompetitor first($columns = ['*'])
*/
class ReportCompetitorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type',
        'competitor_id',
        'product_id',
        'price_for_company',
        'price_for_competitor',
        'note',
        'number_of_promo',
        'promo_note',
        'promo_image',
        'category',
        'width_for_company',
        'width_for_competitor',
        'height_for_company',
        'height_for_competitor',
        'store'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReportCompetitor::class;
    }
}
