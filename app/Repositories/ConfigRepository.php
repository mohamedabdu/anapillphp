<?php

namespace App\Repositories;

use App\Models\Config;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ConfigRepository
 * @package App\Repositories
 * @version October 10, 2019, 4:29 pm EET
 *
 * @method Config findWithoutFail($id, $columns = ['*'])
 * @method Config find($id, $columns = ['*'])
 * @method Config first($columns = ['*'])
*/
class ConfigRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'system_email',
        'system_mobile',
        'about',
        'rules',
        'facebook',
        'twitter'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Config::class;
    }
}
