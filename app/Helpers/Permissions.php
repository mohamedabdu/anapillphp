<?php

namespace App\Helpers;

use App\Models\User;
use App\Models\Permission;


class Permissions {

    /**
     * @param $segment
     * @return bool
     */
    public static function check($segment)
    {
        $user = User::find(auth()->id());
        if ($user->id == 1) {
            return true;
        }
        if (!isset($user->userRole)) {
            return false;
        }
        $permissions = $user->userRole->permissions()->pluck("permission_id")->toArray();
        $permissionsTitle = Permission::whereIn("id", $permissions)->where("segment", $segment)->count();
        if ($permissionsTitle > 0) {
            return true;
        } else {
            return false;
        }
    }

}
