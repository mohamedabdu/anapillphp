<?php

namespace App\Http\Controllers\API\Agent;

use App\Http\Controllers\API\ApiController;
use App\Http\Controllers\API\Visits\VisitControl;
use App\Http\Controllers\API\Visits\VisitTaskControl;
use App\Models\AgentHoliday;
use App\Models\UserAttendance;
use App\Scopes\AgentScope;
use App\Traits\Api\LoginTrait;
use App\Traits\Api\PassportTrait;
use App\Traits\Api\PasswordTrait;
use function Helper\Common\__lang;
use function Helper\Common\upload;
use Illuminate\Http\Request;
use Response;
use Validator;
/**
 * Class NotificationController
 * @package App\Http\Controllers\API
 */

class AgentAPIController extends ApiController
{
    use LoginTrait, PassportTrait, PasswordTrait;

    public $visitControl;
    public $visitTaskControl;
    public function __construct()
    {
        parent::__construct();
        $this->role = AgentScope::$typeOfAgent;
        $this->visitControl = new VisitControl();
        $this->visitTaskControl = new VisitTaskControl();
    }

    public function attendance(Request $request) {
        $validator = Validator::make($request->all(), [
            "lat" => "required",
            "image" => "required",
        ]);
        if ($validator->fails()) {
            return $this->respondBadRequest($validator->errors()->toArray());
        }
        $checkerBefore = UserAttendance::where("date", date("Y-m-d"))->where("user_id",$this->user->id)->first();
        if($checkerBefore != null) {
            return $this->respondBadRequest(__lang("attended_before"));
        }
        $attendance = new UserAttendance;
        $attendance->date = date("Y-m-d");
        $attendance->user_id = $this->user->id;
        $attendance->lat = $request->lat;
        $attendance->lng = $request->lng;
        $attendance->image = upload($request->file, true)["thum"];
        $attendance->save();
        return $this->respondWithSuccess();
    }
    public function holidayRequest(Request $request) {
        $validator = Validator::make($request->all(), [
            "title" => "required",
            "date" => "required",
            "reason" => "required",
        ]);
        if ($validator->fails()) {
            return $this->respondBadRequest($validator->errors()->toArray());
        }
        $checkerBefore = AgentHoliday::where("start_date", date("Y-m-d"))->where("agent_id",$this->user->id)->first();
        if($checkerBefore != null) {
            return $this->respondBadRequest(__lang("holiday_requested_before"));
        }
        $holiday = new AgentHoliday;
        $holiday->start_date = $request->date;
        $holiday->agent_id = $this->user->id;
        $holiday->title = $request->title;
        $holiday->reason = $request->reason;
        $holiday->save();
        return $this->respondWithSuccess();
    }
    public function visits(Request $request) {
        $data["scheduled"] = $this->visitControl->scheduleVisitsByUser($this->user->id);
        $data["normal"] = $this->visitControl->normalVisitsByUser($this->user->id);
        return $this->respondWithSuccess($data);
    }
    public function search(Request $request) {
        $data["scheduled"] = $this->visitControl->searchScheduleVisitsByUser($this->user->id, $request);
        $data["normal"] = $this->visitControl->searchNormalVisitsByUser($this->user->id, $request);
        return $this->respondWithSuccess($data);
    }
    public function startVisit($id, Request $request) {
        $action = $this->visitControl->startVisit($id, $this->user->id, $request->store_id);
        if($action == null || $action == false) {
            return $this->respondBadRequest();
        }
        $data["start_id"] = $action;
        return $this->respondWithSuccess($data);
    }
    public function taskArrangeChips($id, Request $request) {
        $visit = $this->visitControl->visitDetailByID($id);
        if($visit == null) {
            return $this->respondBadRequest();
        }
        $action = $this->visitTaskControl->taskArrangeChips($visit, $request);
        if($action == "true") {
            return $this->respondWithSuccess();
        } else {
            return $this->respondBadRequest($action);
        }
    }
    public function taskReturned($id, Request $request) {
        $visit = $this->visitControl->visitDetailByID($id);
        if($visit == null) {
            return $this->respondBadRequest();
        }
        $action = $this->visitTaskControl->taskReturned($visit, $request);
        if($action == "true") {
            return $this->respondWithSuccess();
        } else {
            return $this->respondBadRequest($action);
        }
    }
    public function taskInventoryGeneral($id, Request $request) {
        $visit = $this->visitControl->visitDetailByID($id);
        if($visit == null) {
            return $this->respondBadRequest();
        }
        $action = $this->visitTaskControl->taskInventoryGeneral($visit, $request);
        if($action == "true") {
            return $this->respondWithSuccess();
        } else {
            return $this->respondBadRequest($action);
        }
    }
    public function taskInventoryDetail($id, Request $request) {
        $visit = $this->visitControl->visitDetailByID($id);
        if($visit == null) {
            return $this->respondBadRequest();
        }
        $action = $this->visitTaskControl->taskInventoryDetail($visit, $request);
        if($action == "true") {
            return $this->respondWithSuccess();
        } else {
            return $this->respondBadRequest($action);
        }
    }
    public function taskReportPrice($id, Request $request) {
        $visit = $this->visitControl->visitDetailByID($id);
        if($visit == null) {
            return $this->respondBadRequest();
        }
        $action = $this->visitTaskControl->taskReportPrice($visit, $request);
        if($action == "true") {
            return $this->respondWithSuccess();
        } else {
            return $this->respondBadRequest($action);
        }
    }
    public function taskArea($id, Request $request) {
        $visit = $this->visitControl->visitDetailByID($id);
        if($visit == null) {
            return $this->respondBadRequest();
        }
        $action = $this->visitTaskControl->taskArea($visit, $request);
        if($action == "true") {
            return $this->respondWithSuccess();
        } else {
            return $this->respondBadRequest($action);
        }
    }
    public function taskBill($id, Request $request) {
        $visit = $this->visitControl->visitDetailByID($id);
        if($visit == null) {
            return $this->respondBadRequest();
        }
        $action = $this->visitTaskControl->taskBill($visit, $request);
        if($action == "true") {
            return $this->respondWithSuccess();
        } else {
            return $this->respondBadRequest($action);
        }
    }
    public function taskPromo($id, Request $request) {
        $visit = $this->visitControl->visitDetailByID($id);
        if($visit == null) {
            return $this->respondBadRequest();
        }
        $action = $this->visitTaskControl->taskPromo($visit, $request);
        if($action == "true") {
            return $this->respondWithSuccess();
        } else {
            return $this->respondBadRequest($action);
        }
    }
    public function cancelVisit($id, Request $request) {
        $action = $this->visitControl->cancelVisit($id, $this->user->id, $request->note);
        return $this->respondWithSuccess();

    }
    public function endVisit($id, Request $request) {
        $visitDetail = $this->visitControl->visitDetailByID($id);
        if($visitDetail == null) {
            return $this->respondBadRequest();
        }
        $checker = $this->visitControl->endVisit($visitDetail);
        return $this->respondWithSuccess();
    }
}
