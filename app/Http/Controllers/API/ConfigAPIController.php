<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateConfigAPIRequest;
use App\Http\Requests\API\UpdateConfigAPIRequest;
use App\Models\Config;
use App\Models\PromoDetail;
use App\Models\Task;
use App\Repositories\ConfigRepository;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ConfigController
 * @package App\Http\Controllers\API
 */

class ConfigAPIController extends ApiController
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $data['config'] = Config::latest("id")->first()->transform();
        $tasks = [];
        foreach (Task::all() as $item) {
            $tasks[] = $item->transformFull();
        }
        $data["tasks"] = $tasks;
        $promos = PromoDetail::join("promos","promos.id","=","promo_details.promo_id")
            ->where("end_date",">",date("Y-m-d"))->get();
        $data["promos"] = $promos->transformCollection();

        return $this->respondWithSuccess($data);
    }

}
