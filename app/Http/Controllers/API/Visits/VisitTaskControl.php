<?php
/**
 * Created by PhpStorm.
 * User: m.abdu
 * Date: 5/27/20
 * Time: 11:27 AM
 */

namespace App\Http\Controllers\API\Visits;


use App\Models\Visit;
use App\Models\VisitDetail;
use App\Models\VisitTaskAreaDetail;
use App\Models\VisitTaskBillDetail;
use App\Models\VisitTaskDetail;
use App\Models\VisitTaskDetailChelf;
use App\Models\VisitTaskDetailImage;
use App\Models\VisitTaskInventoryDetail;
use App\Models\VisitTaskPromoDetail;
use App\Models\VisitTaskReturnDetail;
use App\Traits\VisitTrait;
use function Helper\Common\__lang;
use function Helper\Common\base64ToImage;
use function Helper\Common\upload;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Validator;
class VisitTaskControl extends VisitControl
{
    public function __construct()
    {

    }
    public function taskArrangeChips(VisitDetail $visitDetail, Request $request) {
        $validator = Validator::make($request->all(), [
            "start_date" => "required",
            "end_date" => "required",
            "start_lat" => "required",
            "start_lng" => "required",
            "end_lat" => "required",
            "end_lng" => "required",
            "before_note" => "required",
            "after_note" => "required",
            "before_image" => "required",
            "after_image" => "required",
        ]);
        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }
        try {
            $checkBefore = VisitTaskDetail::where("visit_detail_id",$visitDetail->id)->where("task_id",1)->first();
            if($checkBefore != null) {
                throw new \Exception(__lang("this_task_added_before"));
            }
            $visitTaskDetail = new VisitTaskDetail();
            $visitTaskDetail->visit_detail_id = $visitDetail->id;
            $visitTaskDetail->task_id = 1;
            $visitTaskDetail->start_date = $request->start_date;
            $visitTaskDetail->end_date = $request->end_date;
            $visitTaskDetail->start_lat = $request->start_lat;
            $visitTaskDetail->start_lng = $request->start_lng;
            $visitTaskDetail->end_lat = $request->end_lat;
            $visitTaskDetail->end_lng = $request->end_lng;
            $visitTaskDetail->note = $request->note;
            $visitTaskDetail->save();

            $notesModel = new VisitTaskDetailChelf();
            $notesModel->visit_detail_id = $visitTaskDetail->id;
            $notesModel->image_before = upload($request->before_image,true)["thum"];
            $notesModel->image_after = upload($request->after_image,true)["thum"];
            $notesModel->note_before = $request->before_note;
            $notesModel->note_after = $request->after_note;
            $notesModel->save();
            return "true";
        } catch (\Exception $e) {
            return $e->getMessage();
        }


    }
    public function taskReturned(VisitDetail $visitDetail, Request $request) {
        $validator = Validator::make($request->all(), [
            "start_date" => "required",
            "end_date" => "required",
            "start_lat" => "required",
            "start_lng" => "required",
            "end_lat" => "required",
            "end_lng" => "required",
            "bill_id" => "required",
            "product_id" => "required",
            "number_piece" => "required",
            "number_of_carton" => "required",
            "return_reason_id" => "required",
        ]);
        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }
        try {
            $visitTaskDetail = VisitTaskDetail::where("visit_detail_id",$visitDetail->id)->where("task_id",2)->first();
            if($visitTaskDetail == null) {
                $visitTaskDetail = new VisitTaskDetail();
                $visitTaskDetail->visit_detail_id = $visitDetail->id;
                $visitTaskDetail->task_id = 2;
                $visitTaskDetail->start_date = $request->start_date;
                $visitTaskDetail->end_date = $request->end_date;
                $visitTaskDetail->start_lat = $request->start_lat;
                $visitTaskDetail->start_lng = $request->start_lng;
                $visitTaskDetail->end_lat = $request->end_lat;
                $visitTaskDetail->end_lng = $request->end_lng;
                $visitTaskDetail->note = $request->note;
                $visitTaskDetail->save();
            }
            $images = [];
            foreach ($request->file("images") as $image) {
                $images[] = upload($image,true)["thum"];
            }

            $returnTaskDetail = new VisitTaskReturnDetail();
            $returnTaskDetail->visit_task_detail_id = $visitTaskDetail->id;
            $returnTaskDetail->bill_id = $request->bill_id;
            $returnTaskDetail->product_id = $request->product_id;
            $returnTaskDetail->number_of_carton = $request->number_of_carton;
            $returnTaskDetail->number_piece = $request->number_piece;
            $returnTaskDetail->return_reason_id = $request->return_reason_id;
            $returnTaskDetail->expire_end = "";
            $returnTaskDetail->images = json_encode($images);
            $returnTaskDetail->note = $request->note;
            $returnTaskDetail->save();

            return "true";
        } catch (\Exception $e) {
            return $e->getMessage();
        }


    }
    public function taskInventoryGeneral(VisitDetail $visitDetail, Request $request) {
        $validator = Validator::make($request->all(), [
            "start_date" => "required",
            "end_date" => "required",
            "start_lat" => "required",
            "start_lng" => "required",
            "end_lat" => "required",
            "end_lng" => "required",
            "products" => "required",
        ]);
        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }
        try {
            $visitTaskDetail = VisitTaskDetail::where("visit_detail_id",$visitDetail->id)->where("task_id",3)->first();
            if($visitTaskDetail == null) {
                $visitTaskDetail = new VisitTaskDetail();
                $visitTaskDetail->visit_detail_id = $visitDetail->id;
                $visitTaskDetail->task_id = 3;
                $visitTaskDetail->start_date = $request->start_date;
                $visitTaskDetail->end_date = $request->end_date;
                $visitTaskDetail->start_lat = $request->start_lat;
                $visitTaskDetail->start_lng = $request->start_lng;
                $visitTaskDetail->end_lat = $request->end_lat;
                $visitTaskDetail->end_lng = $request->end_lng;
                $visitTaskDetail->note = $request->note;
                $visitTaskDetail->save();
            }

            $json = json_decode($request->products);

            foreach ($json as $product) {
                $returnTaskDetail = new VisitTaskInventoryDetail();
                $returnTaskDetail->visit_task_detail_id = $visitTaskDetail->id;
                $returnTaskDetail->product_id = $product->id;
                $returnTaskDetail->stock = $product->stock;
                $returnTaskDetail->inventory = $product->inventory;
                $returnTaskDetail->save();
            }


            return "true";
        } catch (\Exception $e) {
            return $e->getMessage();
        }


    }
    public function taskInventoryDetail(VisitDetail $visitDetail, Request $request) {
        $validator = Validator::make($request->all(), [
            "start_date" => "required",
            "end_date" => "required",
            "start_lat" => "required",
            "start_lng" => "required",
            "end_lat" => "required",
            "end_lng" => "required",
            "products" => "required",
        ]);
        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }
        try {
            $visitTaskDetail = VisitTaskDetail::where("visit_detail_id",$visitDetail->id)->where("task_id",4)->first();
            if($visitTaskDetail == null) {
                $visitTaskDetail = new VisitTaskDetail();
                $visitTaskDetail->visit_detail_id = $visitDetail->id;
                $visitTaskDetail->task_id = 4;
                $visitTaskDetail->start_date = $request->start_date;
                $visitTaskDetail->end_date = $request->end_date;
                $visitTaskDetail->start_lat = $request->start_lat;
                $visitTaskDetail->start_lng = $request->start_lng;
                $visitTaskDetail->end_lat = $request->end_lat;
                $visitTaskDetail->end_lng = $request->end_lng;
                $visitTaskDetail->note = $request->note;
                $visitTaskDetail->save();
            }

            $json = json_decode($request->products);

            foreach ($json as $product) {
                $returnTaskDetail = new VisitTaskInventoryDetail();
                $returnTaskDetail->visit_task_detail_id = $visitTaskDetail->id;
                $returnTaskDetail->product_id = $product->id;
                $returnTaskDetail->stock = $product->stock;
                $returnTaskDetail->inventory = $product->inventory;
                $returnTaskDetail->num_of_stock = $product->num_of_stock;
                $returnTaskDetail->num_of_inventory = $product->num_of_inventory;
                $returnTaskDetail->save();
            }


            return "true";
        } catch (\Exception $e) {
            return $e->getMessage();
        }


    }
    public function taskReportPrice(VisitDetail $visitDetail, Request $request) {
        $validator = Validator::make($request->all(), [
            "start_date" => "required",
            "end_date" => "required",
            "start_lat" => "required",
            "start_lng" => "required",
            "end_lat" => "required",
            "end_lng" => "required",
            "products" => "required",
        ]);
        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }
        try {
            $visitTaskDetail = VisitTaskDetail::where("visit_detail_id",$visitDetail->id)->where("task_id",5)->first();
            if($visitTaskDetail == null) {
                $visitTaskDetail = new VisitTaskDetail();
                $visitTaskDetail->visit_detail_id = $visitDetail->id;
                $visitTaskDetail->task_id = 5;
                $visitTaskDetail->start_date = $request->start_date;
                $visitTaskDetail->end_date = $request->end_date;
                $visitTaskDetail->start_lat = $request->start_lat;
                $visitTaskDetail->start_lng = $request->start_lng;
                $visitTaskDetail->end_lat = $request->end_lat;
                $visitTaskDetail->end_lng = $request->end_lng;
                $visitTaskDetail->note = $request->note;
                $visitTaskDetail->save();
            }

            $json = json_decode($request->products);

            foreach ($json as $product) {
                $returnTaskDetail = new VisitTaskInventoryDetail();
                $returnTaskDetail->visit_task_detail_id = $visitTaskDetail->id;
                $returnTaskDetail->product_id = $product->id;
                $returnTaskDetail->price = $product->price;
                $returnTaskDetail->save();
            }


            return "true";
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }
    public function taskArea(VisitDetail $visitDetail, Request $request) {
        $validator = Validator::make($request->all(), [
            "start_date" => "required",
            "end_date" => "required",
            "start_lat" => "required",
            "start_lng" => "required",
            "end_lat" => "required",
            "end_lng" => "required",
            "categories" => "required",
            "images" => "required",
        ]);
        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }
        try {
            $visitTaskDetail = VisitTaskDetail::where("visit_detail_id",$visitDetail->id)->where("task_id",6)->first();
            if($visitTaskDetail == null) {
                $visitTaskDetail = new VisitTaskDetail();
                $visitTaskDetail->visit_detail_id = $visitDetail->id;
                $visitTaskDetail->task_id = 6;
                $visitTaskDetail->start_date = $request->start_date;
                $visitTaskDetail->end_date = $request->end_date;
                $visitTaskDetail->start_lat = $request->start_lat;
                $visitTaskDetail->start_lng = $request->start_lng;
                $visitTaskDetail->end_lat = $request->end_lat;
                $visitTaskDetail->end_lng = $request->end_lng;
                $visitTaskDetail->note = $request->note;
                $visitTaskDetail->save();
            }

            $images = [];
            foreach ($request->file("images") as $image) {
                $images[] = upload($image,true)["thum"];
            }
            $json = json_decode($request->categories);
            foreach ($json as $category) {
                $returnTaskDetail = new VisitTaskAreaDetail();
                $returnTaskDetail->visit_task_detail_id = $visitTaskDetail->id;
                $returnTaskDetail->category_id = $category->id;
                $returnTaskDetail->width = $category->width;
                $returnTaskDetail->height = $category->height;
                $returnTaskDetail->images = json_encode($images);
                $returnTaskDetail->note = $category->note;
                $returnTaskDetail->save();
            }
            return "true";
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }
    public function taskBill(VisitDetail $visitDetail, Request $request) {
        $validator = Validator::make($request->all(), [
            "start_date" => "required",
            "end_date" => "required",
            "start_lat" => "required",
            "start_lng" => "required",
            "end_lat" => "required",
            "end_lng" => "required",
            "bill_id" => "required",
            "bill_date" => "required",
            "bill_image" => "required",
            "bill_total" => "required",
            "products" => "required",
        ]);
        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }
        try {
            $visitTaskDetail = VisitTaskDetail::where("visit_detail_id",$visitDetail->id)->where("task_id",7)->first();
            if($visitTaskDetail == null) {
                $visitTaskDetail = new VisitTaskDetail();
                $visitTaskDetail->visit_detail_id = $visitDetail->id;
                $visitTaskDetail->task_id = 7;
                $visitTaskDetail->start_date = $request->start_date;
                $visitTaskDetail->end_date = $request->end_date;
                $visitTaskDetail->start_lat = $request->start_lat;
                $visitTaskDetail->start_lng = $request->start_lng;
                $visitTaskDetail->end_lat = $request->end_lat;
                $visitTaskDetail->end_lng = $request->end_lng;
                $visitTaskDetail->note = $request->note;
                $visitTaskDetail->save();
            } else {
                //throw new \Exception(__lang("this_task_added_before"));
            }

            $images = [];
            foreach ($request->file("images") as $image) {
                $images[] = upload($image,true)["thum"];
            }


            $returnTaskDetail = new VisitTaskBillDetail();
            $returnTaskDetail->visit_task_detail_id = $visitTaskDetail->id;
            $returnTaskDetail->bill_id = $request->bill_id;
            $returnTaskDetail->bill_date = $request->bill_date;
            $returnTaskDetail->bill_total = $request->bill_total;
            $returnTaskDetail->bill_image = upload($request->bill_image, false)["img"];
            $returnTaskDetail->images = json_encode($images);
            $returnTaskDetail->products = $request->products;
            $returnTaskDetail->save();
            return "true";
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }
    public function taskPromo(VisitDetail $visitDetail, Request $request) {
        $validator = Validator::make($request->all(), [
            "start_date" => "required",
            "end_date" => "required",
            "start_lat" => "required",
            "start_lng" => "required",
            "end_lat" => "required",
            "end_lng" => "required",
            "promos" => "required",

        ]);
        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }
        try {
            $visitTaskDetail = VisitTaskDetail::where("visit_detail_id",$visitDetail->id)->where("task_id",8)->first();
            if($visitTaskDetail == null) {
                $visitTaskDetail = new VisitTaskDetail();
                $visitTaskDetail->visit_detail_id = $visitDetail->id;
                $visitTaskDetail->task_id = 8;
                $visitTaskDetail->start_date = $request->start_date;
                $visitTaskDetail->end_date = $request->end_date;
                $visitTaskDetail->start_lat = $request->start_lat;
                $visitTaskDetail->start_lng = $request->start_lng;
                $visitTaskDetail->end_lat = $request->end_lat;
                $visitTaskDetail->end_lng = $request->end_lng;
                $visitTaskDetail->note = $request->note;
                $visitTaskDetail->save();
            } else {
                throw new \Exception(__lang("this_task_added_before"));
            }

            $json = json_decode($request->promos);

            foreach ($json as $promo) {
                $returnTaskDetail = new VisitTaskPromoDetail();
                $returnTaskDetail->visit_task_detail_id = $visitTaskDetail->id;
                $returnTaskDetail->promo_id = $promo->id;
                $returnTaskDetail->num_of_promo = $promo->num_of_promo;
                $returnTaskDetail->note = $promo->note;
                $returnTaskDetail->promo_image = base64ToImage($promo->image);
                $returnTaskDetail->save();
            }

            return "true";
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }
}