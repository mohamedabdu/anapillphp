<?php
/**
 * Created by PhpStorm.
 * User: m.abdu
 * Date: 5/27/20
 * Time: 11:27 AM
 */

namespace App\Http\Controllers\API\Visits;


use App\Models\Visit;
use App\Models\VisitCancel;
use App\Models\VisitDetail;
use App\Models\VisitStore;
use App\Traits\VisitTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class VisitControl
{
    use VisitTrait;
    public function __construct()
    {
    }

    public function scheduleVisitsByUser($userID) {
        /** get visits next schedule */
        $visits = Visit::join("visit_agents","visits.id","=","visit_agents.visit_id")
            ->where("visit_agents.agent_id",$userID)
            ->where("visits.id",17)
            ->where("visits.type", Visit::$types["scheduled"])
            ->whereIn("visits.status", [Visit::$statuses["PENDING"], Visit::$statuses["PROCESSING"]])
            ->select(["visits.*"])->get();
        $data = new Collection();
        foreach ($visits as $visit) {
            $nextDate = $this->checkNextDate($visit);
            if($nextDate == date("Y-m-d")) {
                $data->push($visit);
            }
        }
        return $data->transformCollection();
    }
    public function searchScheduleVisitsByUser($userID, Request $request) {
        $date = date("Y-m-d");
        if($request->date != null) {
            $date = $request->date;
        }
        /** get visits next schedule */
        $visits = Visit::join("visit_agents","visits.id","=","visit_agents.visit_id")
            ->where("visit_agents.agent_id",$userID)
            ->where("visits.type", Visit::$types["scheduled"])
            ->whereIn("visits.status", [Visit::$statuses["PENDING"], Visit::$statuses["PROCESSING"]]);

        if($request->code) {
            $visits = $visits->where("code",$request->code);
        }

        $visits = $visits->select(["visits.*"])->get();


        $data = new Collection();
        foreach ($visits as $visit) {
            $nextDate = $this->checkNextDate($visit);
            if($request->date != null) {
                if($nextDate == $date) {
                    $data->push($visit);
                }
            } else {
                $data->push($visit);
            }
        }
        return $data->transformCollection();
    }
    public function normalVisitsByUser($userID) {
        /** get visits next normal */
        $visits = Visit::join("visit_agents","visits.id","=","visit_agents.visit_id")
            ->where("visit_agents.agent_id",$userID)
            ->where("visits.type", Visit::$types["normal"])
            ->where("visits.start_date", date("Y-m-d"))
            ->whereIn("visits.status", [Visit::$statuses["PENDING"], Visit::$statuses["PROCESSING"]])
            ->select(["visits.*"])->get();

        return $visits->transformCollection();
    }
    public function searchNormalVisitsByUser($userID, Request $request) {
        $date = date("Y-m-d");
        if($request->date != null) {
            $date = $request->date;
        }
        /** get visits next normal */
        $visits = Visit::join("visit_agents","visits.id","=","visit_agents.visit_id")
            ->where("visit_agents.agent_id",$userID)
            ->where("visits.type", Visit::$types["normal"])
            ->where("visits.start_date", $date)
            ->whereIn("visits.status", [Visit::$statuses["PENDING"], Visit::$statuses["PROCESSING"]]);

        if($request->code) {
            $visits = $visits->where("code",$request->code);
        }

        $visits = $visits->select(["visits.*"])->get();

        return $visits->transformCollection();
    }
    public function visitByID($visitID, $userID) {
        /** get visits next normal */
        $visits = Visit::join("visit_agents","visits.id","=","visit_agents.visit_id")
            ->where("visit_agents.agent_id",$userID)
            ->where("visits.id",$visitID)
            ->whereIn("visits.status", [Visit::$statuses["PENDING"], Visit::$statuses["PROCESSING"]])
            ->select(["visits.*"])->first();

        return $visits;
    }

    /**
     * @param $visitID
     * @return VisitDetail
     */
    public function visitDetailByID($visitID) {
        $visitDetail = VisitDetail::where("id",$visitID)->first();
        return $visitDetail;
    }
    public function startVisit($visitID, $userID, $storeID = null) {
        /** get visits next normal */
        $visits = Visit::join("visit_agents","visits.id","=","visit_agents.visit_id")
            ->where("visit_agents.agent_id",$userID)
            ->where("visits.id",$visitID)
            ->whereIn("visits.status", [Visit::$statuses["PENDING"], Visit::$statuses["PROCESSING"]])
            ->select(["visits.*"])->first();

        if($visits == null) {
            return null;
        }
        if($visits->type == Visit::$types["normal"]) {
            $visitDetail = VisitDetail::where("start_date",$visits->start_date)->where("visit_id",$visitID)->first();
            if($visitDetail == null) {
                $checker = $this->createNewDetailByID($visits, $visits->start_date, true);
            } else {
                $visitDetail->start_time = date("H:i:s");
                $visitDetail->save();
                $checker = $visitDetail->id;
            }
        } else {
            // check before detail
            $nextDay = $this->checkNextDate($visits);
            $visitDetail = VisitDetail::where("start_date",$nextDay)->where("visit_id",$visitID)->first();
            if(!$visitDetail) {
                $checker = $this->createNewDetailForStore($visits,$storeID, $nextDay, true);
            } else {
                $visitDetail->start_time = date("H:i:s");
                $visitDetail->save();
                $checker = $visitDetail->id;
            }
        }
        return $checker;
    }
    public function cancelVisit($visitID, $userID, $note, $storeID = null) {
        /** get visits next normal */
        $visits = Visit::join("visit_agents","visits.id","=","visit_agents.visit_id")
            ->where("visit_agents.agent_id",$userID)
            ->where("visits.id",$visitID)
            ->whereIn("visits.status", [Visit::$statuses["PENDING"], Visit::$statuses["PROCESSING"]])
            ->select(["visits.*"])->first();

        if($visits == null) {
            return null;
        }
        if($visits->type == Visit::$types["normal"]) {
            $ids = VisitStore::where("visit_id",$visits->id)->pluck('id')->first();
            $visitCancel = new VisitCancel();
            $visitCancel->agent_id = $userID;
            $visitCancel->visit_id = $visits->id;
            $visitCancel->visit_store_id = $ids;
            $visitCancel->note = $note;
            $visitCancel->is_agree = 0;
            $visitCancel->save();
            return true;
        } else {
            $ids = VisitStore::where("visit_id",$visits->id)->where("store_id",$storeID)->pluck('id')->first();
            $visitCancel = new VisitCancel();
            $visitCancel->agent_id = $userID;
            $visitCancel->visit_id = $visits->id;
            $visitCancel->visit_store_id = $ids;
            $visitCancel->note = $note;
            $visitCancel->is_agree = 0;
            $visitCancel->save();
            return true;
        }
    }
    public function endVisit(VisitDetail $visitDetail) {
        $visit = Visit::where("id",$visitDetail->visit_id)->first();
        if($visit->type == Visit::$types["normal"]) {
            $visit->status = Visit::$statuses["FINISHED"];
        }
        $visitDetail->status = Visit::$statuses["FINISHED"];
        $visitDetail->end_date = date("Y-m-d");
        $visitDetail->end_time = date("H:i:s");
        $visit->save();
        $visitDetail->save();
        return true;
    }
}