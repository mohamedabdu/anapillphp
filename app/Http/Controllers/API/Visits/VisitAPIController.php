<?php

namespace App\Http\Controllers\API\Visits;

use App\Http\Controllers\API\ApiController;
use App\Models\UserAttendance;
use App\Scopes\AgentScope;
use App\Traits\Api\LoginTrait;
use App\Traits\Api\PassportTrait;
use App\Traits\Api\PasswordTrait;
use function Helper\Common\__lang;
use function Helper\Common\upload;
use Illuminate\Http\Request;
use Response;
use Validator;
/**
 * Class NotificationController
 * @package App\Http\Controllers\API
 */

class VisitAPIController extends ApiController
{

    public function __construct()
    {
        parent::__construct();
    }

}
