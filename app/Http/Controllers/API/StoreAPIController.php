<?php

namespace App\Http\Controllers\API;

use App\Models\Config;
use App\Models\Product;
use App\Models\Store;
use App\Models\Task;
use Illuminate\Http\Request;
use Response;

/**
 * Class ConfigController
 * @package App\Http\Controllers\API
 */

class StoreAPIController extends ApiController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function products(Request $request) {
        $products = Product::whereNull("competitor_id")->orderByDesc("ordered")->get()->transformCollection();
        if($products == null) {
            return $this->respondBadRequest();
        }
        return $this->respondWithSuccess($products);
    }
}
