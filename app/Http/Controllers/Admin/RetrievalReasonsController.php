<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BackendController;
use App\Models\RetrievalReason;
use App\Models\RetrievalReasonTranslation;
use Illuminate\Http\Request;
use DB;
use Validator;

class RetrievalReasonsController extends BackendController
{
    private $rules = [
        'active' => 'required',
        'this_order' => 'required',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->middleware('role:retrieval_reasons');
        $this->data['parent_tab'] = 'general_settings';
        $this->data['tab'] = 'retrieval_reasons';
    }
  
    public function index(Request $request)
    {
        return $this->_view("retrieval_reasons.index");
    }

   
    public function create(Request $request)
    {
        return $this->_view('retrieval_reasons.create');
    }

   
    public function store(Request $request)
    {
        try {
            $columns_arr = array(
                'title' => 'required'
            );
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);

            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            
            DB::beginTransaction();
            
            $retrieval_reason = new RetrievalReason();
            $retrieval_reason->active = $request->input('active');
            $retrieval_reason->this_order = $request->input('this_order');
            $retrieval_reason->created_by = $this->user->id;
            $retrieval_reason->save();

            $this->createRetrievalReasonTranslations($retrieval_reason->id, $request);
            DB::commit();
            return _json('success',_lang('app.added_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error',_lang('app.error_is_occured'),400);
        }
    }

    public function edit($id)
    {
        try 
        {
            $id = en_de_crypt($id, false);
            $retrieval_reason = RetrievalReason::where('id',$id);
            $retrieval_reason = $this->checkCreatedBy($retrieval_reason, 'retrieval_reasons')
                                    ->first();
            if (!$retrieval_reason) {
                session()->flash('error_message', _lang('app.this_item_doesn\'t_exist'));
                return redirect(route('retrieval_reasons.index'));
            }
            $this->data['translations'] = RetrievalReasonTranslation::where('retrieval_reason_id', $id)->get()->keyBy('locale');
            $this->data["retrieval_reason"] = $retrieval_reason;
            return $this->_view('retrieval_reasons.edit'); 
        } 
        catch (\Exception $ex) {
            session()->flash('error_message', _lang('app.error_is_occured'));
            return redirect(route('retrieval_reasons.index'));
        }
       
    }

    
    public function update(Request $request, $id)
    {
        try {
            $columns_arr = array(
                'title' => 'required'
            );
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);
            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            $id = en_de_crypt($id, false);
            $retrieval_reason = RetrievalReason::where('id',$id);
            $retrieval_reason = $this->checkCreatedBy($retrieval_reason, 'retrieval_reasons')
                                    ->first();
            if (!$retrieval_reason) {
                return _json('error', _lang('app.not_found'), 404);
            }
            DB::beginTransaction();
            $retrieval_reason->active = $request->input('active');
            $retrieval_reason->this_order = $request->input('this_order');
            $retrieval_reason->save();
            RetrievalReasonTranslation::where('retrieval_reason_id', $retrieval_reason->id)->delete();
            $this->createRetrievalReasonTranslations($retrieval_reason->id, $request);
            DB::commit();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }

    public function active($id)
    {
        try {
            $id = en_de_crypt($id, false);
            $retrieval_reason = RetrievalReason::where('id',$id);
            $retrieval_reason = $this->checkCreatedBy($retrieval_reason, 'retrieval_reasons')
                                    ->first();
            if (!$retrieval_reason) {
                return _json('error', _lang('app.not_found'), 404);
            }
            $retrieval_reason->active = !$retrieval_reason->active;
            $retrieval_reason->save();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }
    
    public function destroy($id)
    {
        $id = en_de_crypt($id, false);
        $retrieval_reason = RetrievalReason::where('id',$id);
        $retrieval_reason = $this->checkCreatedBy($retrieval_reason, 'retrieval_reasons')
                                    ->first();
        if (!$retrieval_reason) {
            return _json('error', _lang('app.error_is_occured'), 404);
        }
        DB::beginTransaction();
        try {
            $retrieval_reason->delete();
            DB::commit();
            return _json('success', _lang('app.deleted_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            if ($ex->getCode() == 23000) {
                return _json('error', _lang('app.this_record_can_not_be_deleted_for_linking_to_other_records'), 400);
            } else {

                return _json('error', _lang('app.error_is_occured'), 400);
            }
        }

    }

    public function data(Request $request) {
       
        $retrieval_reasons = RetrievalReason::join('retrieval_reason_translations', function($query){
                                $query->on('retrieval_reason_translations.retrieval_reason_id', '=', 'retrieval_reasons.id')
                                ->where('retrieval_reason_translations.locale',$this->lang_code);
                            });
        $retrieval_reasons = $this->checkCreatedBy($retrieval_reasons,'retrieval_reasons')
                                ->select('retrieval_reasons.*', 'retrieval_reason_translations.title');
            

            return \DataTables::eloquent($retrieval_reasons)
                ->addColumn('options', function ($item) {
                    $back = "";
                    $back .= '<div class="btn-group">';
                    $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '. _lang("app.options");
                    $back .= '<i class="fa fa-angle-down"></i>';
                    $back .= '</button>';
                    $back .= '<ul class = "dropdown-menu" retrieval_reason = "menu">';
                    $back .= '<li>';
                    $back .= '<a href="' . route('retrieval_reasons.edit', en_de_crypt($item->id) ) . '">';
                    $back .= '<i class = "fas fa-edit"></i>' . _lang('app.edit');
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '<li>';
                    $back .= '<a href="" data-toggle="confirmation" onclick = "RetrievalReasons.delete(this);return false;" data-id = "' . en_de_crypt($item->id) . '">';
                    $back .= '<i class = "fas fa-trash-alt"></i>' . _lang("app.delete");
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '</ul>';
                    $back .= ' </div>';

                    return $back;
                })
                ->editColumn("active",function($item){
                    if($item->active == true){
                        $class = "btn btn-success";
                        $label = _lang('app.activated');
                    }else{
                        $class = "btn btn-danger";
                        $label = _lang('app.deactivated');
                    }
                    $back = '<button class="'.$class. '" onclick="RetrievalReasons.changeActive(this); return false;" data-id="'.en_de_crypt($item->id).'">'.$label.'</button>';
                    return $back;
                })
                ->escapeColumns([])
                ->make(true);
    }

    private function createRetrievalReasonTranslations($retrieval_reason_id, $request)
    {
        $retrieval_reason_translations = array();
        $title = $request->input('title');
        foreach ($title as $key => $value) {
            $retrieval_reason_translations[] = array(
                'locale' => $key,
                'title' => $value,
                'retrieval_reason_id' => $retrieval_reason_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
        }
        RetrievalReasonTranslation::insert($retrieval_reason_translations);
    }

   

    


}
