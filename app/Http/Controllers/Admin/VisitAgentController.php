<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVisitAgentRequest;
use App\Http\Requests\UpdateVisitAgentRequest;
use App\Repositories\VisitAgentRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\VisitAgent;
use function Helper\Common\imageUrl;

class VisitAgentController extends AppBaseController
{
    /** @var  VisitAgentRepository */
    private $visitAgentRepository;

    public function __construct(VisitAgentRepository $visitAgentRepo)
    {
        parent::__construct();
        $this->visitAgentRepository = $visitAgentRepo;
    }







    /**
     * Display a listing of the VisitAgent.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->visitAgentRepository->pushCriteria(new RequestCriteria($request));
        $visitAgents = $this->visitAgentRepository->all();
        */

        return view('visit_agents.index');
        /*return view('visit_agents.index')
             ->with('visitAgents', $visitAgents);*/
    }



    /**
     * Show the form for creating a new VisitAgent.
     *
     * @return Response
     */
    public function create()
    {
        return view('visit_agents.create');
    }

    /**
     * Store a newly created VisitAgent in storage.
     *
     * @param CreateVisitAgentRequest $request
     *
     * @return Response
     */
    public function store(CreateVisitAgentRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $visitAgent = $this->visitAgentRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('visitAgents.index'));
    }

    /**
     * Display the specified VisitAgent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visitAgent = $this->visitAgentRepository->findWithoutFail($id);

        if (empty($visitAgent)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitAgents.index'));
        }

        return view('visit_agents.show')->with('visitAgent', $visitAgent);
    }

    /**
     * Show the form for editing the specified VisitAgent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visitAgent = $this->visitAgentRepository->findWithoutFail($id);

        if (empty($visitAgent)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitAgents.index'));
        }

        return view('visit_agents.edit')->with('visitAgent', $visitAgent);
    }

    /**
     * Update the specified VisitAgent in storage.
     *
     * @param  int              $id
     * @param UpdateVisitAgentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVisitAgentRequest $request)
    {


        $visitAgent = $this->visitAgentRepository->findWithoutFail($id);

        if (empty($visitAgent)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitAgents.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $visitAgent = $this->visitAgentRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('visitAgents.index'));
    }

    /**
     * Remove the specified VisitAgent from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->visitAgentRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('visitAgents.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('visitAgents.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->visitAgentRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = VisitAgent::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (VisitAgent $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('visitAgents.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('visitAgents.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(VisitAgent $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (VisitAgent $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
