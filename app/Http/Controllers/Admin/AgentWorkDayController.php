<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAgentWorkDayRequest;
use App\Http\Requests\UpdateAgentWorkDayRequest;
use App\Repositories\AgentWorkDayRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\AgentWorkDay;
use function Helper\Common\imageUrl;

class AgentWorkDayController extends AppBaseController
{
    /** @var  AgentWorkDayRepository */
    private $agentWorkDayRepository;

    public function __construct(AgentWorkDayRepository $agentWorkDayRepo)
    {
        parent::__construct();
        $this->agentWorkDayRepository = $agentWorkDayRepo;
    }







    /**
     * Display a listing of the AgentWorkDay.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->agentWorkDayRepository->pushCriteria(new RequestCriteria($request));
        $agentWorkDays = $this->agentWorkDayRepository->all();
        */

        return view('agent_work_days.index');
        /*return view('agent_work_days.index')
             ->with('agentWorkDays', $agentWorkDays);*/
    }



    /**
     * Show the form for creating a new AgentWorkDay.
     *
     * @return Response
     */
    public function create()
    {
        return view('agent_work_days.create');
    }

    /**
     * Store a newly created AgentWorkDay in storage.
     *
     * @param CreateAgentWorkDayRequest $request
     *
     * @return Response
     */
    public function store(CreateAgentWorkDayRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $agentWorkDay = $this->agentWorkDayRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('agentWorkDays.index'));
    }

    /**
     * Display the specified AgentWorkDay.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $agentWorkDay = $this->agentWorkDayRepository->findWithoutFail($id);

        if (empty($agentWorkDay)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('agentWorkDays.index'));
        }

        return view('agent_work_days.show')->with('agentWorkDay', $agentWorkDay);
    }

    /**
     * Show the form for editing the specified AgentWorkDay.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $agentWorkDay = $this->agentWorkDayRepository->findWithoutFail($id);

        if (empty($agentWorkDay)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('agentWorkDays.index'));
        }

        return view('agent_work_days.edit')->with('agentWorkDay', $agentWorkDay);
    }

    /**
     * Update the specified AgentWorkDay in storage.
     *
     * @param  int              $id
     * @param UpdateAgentWorkDayRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAgentWorkDayRequest $request)
    {


        $agentWorkDay = $this->agentWorkDayRepository->findWithoutFail($id);

        if (empty($agentWorkDay)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('agentWorkDays.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $agentWorkDay = $this->agentWorkDayRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('agentWorkDays.index'));
    }

    /**
     * Remove the specified AgentWorkDay from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->agentWorkDayRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('agentWorkDays.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('agentWorkDays.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->agentWorkDayRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = AgentWorkDay::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (AgentWorkDay $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('agentWorkDays.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('agentWorkDays.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(AgentWorkDay $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (AgentWorkDay $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
