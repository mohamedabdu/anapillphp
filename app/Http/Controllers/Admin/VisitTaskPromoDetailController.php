<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVisitTaskPromoDetailRequest;
use App\Http\Requests\UpdateVisitTaskPromoDetailRequest;
use App\Repositories\VisitTaskPromoDetailRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\VisitTaskPromoDetail;
use function Helper\Common\imageUrl;

class VisitTaskPromoDetailController extends AppBaseController
{
    /** @var  VisitTaskPromoDetailRepository */
    private $visitTaskPromoDetailRepository;

    public function __construct(VisitTaskPromoDetailRepository $visitTaskPromoDetailRepo)
    {
        parent::__construct();
        $this->visitTaskPromoDetailRepository = $visitTaskPromoDetailRepo;
    }







    /**
     * Display a listing of the VisitTaskPromoDetail.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->visitTaskPromoDetailRepository->pushCriteria(new RequestCriteria($request));
        $visitTaskPromoDetails = $this->visitTaskPromoDetailRepository->all();
        */

        return view('visit_task_promo_details.index');
        /*return view('visit_task_promo_details.index')
             ->with('visitTaskPromoDetails', $visitTaskPromoDetails);*/
    }



    /**
     * Show the form for creating a new VisitTaskPromoDetail.
     *
     * @return Response
     */
    public function create()
    {
        return view('visit_task_promo_details.create');
    }

    /**
     * Store a newly created VisitTaskPromoDetail in storage.
     *
     * @param CreateVisitTaskPromoDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateVisitTaskPromoDetailRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $visitTaskPromoDetail = $this->visitTaskPromoDetailRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('visitTaskPromoDetails.index'));
    }

    /**
     * Display the specified VisitTaskPromoDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visitTaskPromoDetail = $this->visitTaskPromoDetailRepository->findWithoutFail($id);

        if (empty($visitTaskPromoDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskPromoDetails.index'));
        }

        return view('visit_task_promo_details.show')->with('visitTaskPromoDetail', $visitTaskPromoDetail);
    }

    /**
     * Show the form for editing the specified VisitTaskPromoDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visitTaskPromoDetail = $this->visitTaskPromoDetailRepository->findWithoutFail($id);

        if (empty($visitTaskPromoDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskPromoDetails.index'));
        }

        return view('visit_task_promo_details.edit')->with('visitTaskPromoDetail', $visitTaskPromoDetail);
    }

    /**
     * Update the specified VisitTaskPromoDetail in storage.
     *
     * @param  int              $id
     * @param UpdateVisitTaskPromoDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVisitTaskPromoDetailRequest $request)
    {


        $visitTaskPromoDetail = $this->visitTaskPromoDetailRepository->findWithoutFail($id);

        if (empty($visitTaskPromoDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskPromoDetails.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $visitTaskPromoDetail = $this->visitTaskPromoDetailRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('visitTaskPromoDetails.index'));
    }

    /**
     * Remove the specified VisitTaskPromoDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->visitTaskPromoDetailRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('visitTaskPromoDetails.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('visitTaskPromoDetails.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->visitTaskPromoDetailRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = VisitTaskPromoDetail::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (VisitTaskPromoDetail $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('visitTaskPromoDetails.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('visitTaskPromoDetails.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(VisitTaskPromoDetail $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (VisitTaskPromoDetail $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
