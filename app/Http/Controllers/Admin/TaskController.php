<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Repositories\TaskRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\Task;
use function Helper\Common\imageUrl;

class TaskController extends AppBaseController
{
    /** @var  TaskRepository */
    private $taskRepository;

    public function __construct(TaskRepository $taskRepo)
    {
        parent::__construct();
        $this->taskRepository = $taskRepo;
    }







    /**
     * Display a listing of the Task.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->taskRepository->pushCriteria(new RequestCriteria($request));
        $tasks = $this->taskRepository->all();
        */

        return $this->_view('tasks.index');
        /*return view('tasks.index')
             ->with('tasks', $tasks);*/
    }



    /**
     * Show the form for creating a new Task.
     *
     * @return Response
     */
    public function create()
    {
        return $this->_view('tasks.create');
    }

    /**
     * Store a newly created Task in storage.
     *
     * @param CreateTaskRequest $request
     *
     * @return Response
     */
    public function store(CreateTaskRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $task = $this->taskRepository->create($input);

        $data['message'] ='تم حفظ البيانات بنجاح';
        $data['success'] = true;
        $data["type"] = "success";
        return $data;
    }

    /**
     * Display the specified Task.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $task = $this->taskRepository->findWithoutFail($id);

        if (empty($task)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('tasks.index'));
        }

        $this->data["task"] = $task;
        return $this->_view('tasks.show');
    }

    /**
     * Show the form for editing the specified Task.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $task = $this->taskRepository->findWithoutFail($id);

        if (empty($task)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('tasks.index'));
        }

        $this->data["task"] = $task;
        return $this->_view('tasks.edit');
    }

    /**
     * Update the specified Task in storage.
     *
     * @param  int              $id
     * @param UpdateTaskRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTaskRequest $request)
    {


        $task = $this->taskRepository->findWithoutFail($id);

        if (empty($task)) {
            $data['message'] ='لم يتم التعديل';
            $data['success'] = false;
            $data["type"] = "false";
            $data["status_code"] = 400;
            return $this->respondBadRequest($data["message"]);
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $task = $this->taskRepository->update($input, $id);

        $data["data"]['message'] ='تم تعديل البيانات بنجاح';
        $data['success'] = true;
        $data["type"] = "success";
        return $data;
    }

    /**
     * Remove the specified Task from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->taskRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('tasks.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('tasks.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->taskRepository->delete($id);
            }
        } else if($id != null) {
            $this->taskRepository->delete($id);
        } else  {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            $data['type'] = "false";
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        $data['type'] = "success";
        return $data;

    }

    public function data() {
            $items = Task::orderByDesc("id")->select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (Task $item) {
                    $back = "";
                    $back .= '<div class="btn-group">';
                    $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '. _lang("app.options");
                    $back .= '<i class="fa fa-angle-down"></i>';
                    $back .= '</button>';
                    $back .= '<ul class = "dropdown-menu" role = "menu">';
                    $back .= '<li>';
                    $back .= '<a href="' . route('tasks.edit', $item->id) . '">';
                    $back .= '<i class = "icon-docs"></i>' . _lang('app.edit');
                    $back .= '</a>';
                    $back .= '</li>';


                    $back .= '</ul>';
                    $back .= ' </div>';

                    return $back;
                })
                ->editColumn("show",function(Task $item){
                    $back = '<a class="btn btn-success" href="' . route('tasks.show', $item->id) . '" data-id="'.$item->id.'">';
                    $back.= "تفاصيل "."</a>";
                    return $back;
                })
                /*
                 ->addColumn("active",function(Task $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (Task $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
