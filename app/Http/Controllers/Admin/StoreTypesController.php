<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BackendController;
use App\Models\StoreType;
use App\Models\StoreTypeTranslation;
use Illuminate\Http\Request;
use DB;
use Validator;

class StoreTypesController extends BackendController
{
    private $rules = [
       
    ];

    public function __construct()
    {
        parent::__construct();
        $this->middleware('role:store_types');
        $this->data['parent_tab'] = 'general_settings';
        $this->data['tab'] = 'store_types';
    }
  
    public function index(Request $request)
    {
        return $this->_view("store_types.index");
    }

   
    public function create(Request $request)
    {
        return $this->_view('store_types.create');
    }

   
    public function store(Request $request)
    {
        try {
            $columns_arr = array(
                'title' => 'required'
            );
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);

            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            
            DB::beginTransaction();
            
            $store_type = new StoreType();
            $store_type->created_by = $this->user->id;
            $store_type->save();

            $this->createStoreTypeTranslations($store_type->id, $request);
            DB::commit();
            return _json('success',_lang('app.added_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error',_lang('app.error_is_occured'),400);
        }
    }

    public function edit($id)
    {
        try 
        {
            $id = en_de_crypt($id, false);
            $store_type = StoreType::where('id',$id);
            $store_type = $this->checkCreatedBy($store_type,'store_types')
                                ->first();
            if (!$store_type) {
                session()->flash('error_message', _lang('app.this_item_doesn\'t_exist'));
                return redirect(route('store_types.index'));
            }
            $this->data['translations'] = StoreTypeTranslation::where('store_type_id', $id)->get()->keyBy('locale');
            $this->data["store_type"] = $store_type;
            return $this->_view('store_types.edit'); 
        } 
        catch (\Exception $ex) {
            session()->flash('error_message', _lang('app.error_is_occured'));
            return redirect(route('store_types.index'));
        }
       
    }

    
    public function update(Request $request, $id)
    {
        try {
            $columns_arr = array(
                'title' => 'required'
            );
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);
            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            $id = en_de_crypt($id, false);
            $store_type = StoreType::where('id', $id);
            $store_type = $this->checkCreatedBy($store_type, 'store_types')
                            ->first();
            if (!$store_type) {
                return _json('error', _lang('app.not_found'), 404);
            }
            DB::beginTransaction();
            StoreTypeTranslation::where('store_type_id', $store_type->id)->delete();
            $this->createStoreTypeTranslations($store_type->id, $request);
            DB::commit();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }

    
    public function destroy($id)
    {
        $id = en_de_crypt($id, false);
        $store_type = StoreType::where('id', $id);
        $store_type = $this->checkCreatedBy($store_type, 'store_types')
                            ->first();
        if (!$store_type) {
            return _json('error', _lang('app.error_is_occured'), 404);
        }
        DB::beginTransaction();
        try {
            $store_type->delete();
            DB::commit();
            return _json('success', _lang('app.deleted_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            if ($ex->getCode() == 23000) {
                return _json('error', _lang('app.this_record_can_not_be_deleted_for_linking_to_other_records'), 400);
            } else {

                return _json('error', _lang('app.error_is_occured'), 400);
            }
        }

    }

    public function data(Request $request) {
       
        $store_types = StoreType::join('store_type_translations', function($query){
                                $query->on('store_type_translations.store_type_id', '=', 'store_types.id')
                                ->where('store_type_translations.locale',$this->lang_code);
                            });
        $store_types = $this->checkCreatedBy($store_types,'store_types')
                                ->select('store_types.*', 'store_type_translations.title');
            

            return \DataTables::eloquent($store_types)
                ->addColumn('options', function ($item) {
                    $back = "";
                    $back .= '<div class="btn-group">';
                    $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '. _lang("app.options");
                    $back .= '<i class="fa fa-angle-down"></i>';
                    $back .= '</button>';
                    $back .= '<ul class = "dropdown-menu" store_type = "menu">';
                    $back .= '<li>';
                    $back .= '<a href="' . route('store_types.edit', en_de_crypt($item->id) ) . '">';
                    $back .= '<i class = "fas fa-edit"></i>' . _lang('app.edit');
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '<li>';
                    $back .= '<a href="" data-toggle="confirmation" onclick = "StoreTypes.delete(this);return false;" data-id = "' . en_de_crypt($item->id) . '">';
                    $back .= '<i class = "fas fa-trash-alt"></i>' . _lang("app.delete");
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '</ul>';
                    $back .= ' </div>';

                    return $back;
                })
                ->escapeColumns([])
                ->make(true);
    }

    private function createStoreTypeTranslations($store_type_id, $request)
    {
        $store_type_translations = array();
        $title = $request->input('title');
        foreach ($title as $key => $value) {
            $store_type_translations[] = array(
                'locale' => $key,
                'title' => $value,
                'store_type_id' => $store_type_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
        }
        StoreTypeTranslation::insert($store_type_translations);
    }

   

    


}
