<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVisitTaskReturnDetailRequest;
use App\Http\Requests\UpdateVisitTaskReturnDetailRequest;
use App\Repositories\VisitTaskReturnDetailRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\VisitTaskReturnDetail;
use function Helper\Common\imageUrl;

class VisitTaskReturnDetailController extends AppBaseController
{
    /** @var  VisitTaskReturnDetailRepository */
    private $visitTaskReturnDetailRepository;

    public function __construct(VisitTaskReturnDetailRepository $visitTaskReturnDetailRepo)
    {
        parent::__construct();
        $this->visitTaskReturnDetailRepository = $visitTaskReturnDetailRepo;
    }







    /**
     * Display a listing of the VisitTaskReturnDetail.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->visitTaskReturnDetailRepository->pushCriteria(new RequestCriteria($request));
        $visitTaskReturnDetails = $this->visitTaskReturnDetailRepository->all();
        */

        return view('visit_task_return_details.index');
        /*return view('visit_task_return_details.index')
             ->with('visitTaskReturnDetails', $visitTaskReturnDetails);*/
    }



    /**
     * Show the form for creating a new VisitTaskReturnDetail.
     *
     * @return Response
     */
    public function create()
    {
        return view('visit_task_return_details.create');
    }

    /**
     * Store a newly created VisitTaskReturnDetail in storage.
     *
     * @param CreateVisitTaskReturnDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateVisitTaskReturnDetailRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $visitTaskReturnDetail = $this->visitTaskReturnDetailRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('visitTaskReturnDetails.index'));
    }

    /**
     * Display the specified VisitTaskReturnDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visitTaskReturnDetail = $this->visitTaskReturnDetailRepository->findWithoutFail($id);

        if (empty($visitTaskReturnDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskReturnDetails.index'));
        }

        return view('visit_task_return_details.show')->with('visitTaskReturnDetail', $visitTaskReturnDetail);
    }

    /**
     * Show the form for editing the specified VisitTaskReturnDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visitTaskReturnDetail = $this->visitTaskReturnDetailRepository->findWithoutFail($id);

        if (empty($visitTaskReturnDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskReturnDetails.index'));
        }

        return view('visit_task_return_details.edit')->with('visitTaskReturnDetail', $visitTaskReturnDetail);
    }

    /**
     * Update the specified VisitTaskReturnDetail in storage.
     *
     * @param  int              $id
     * @param UpdateVisitTaskReturnDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVisitTaskReturnDetailRequest $request)
    {


        $visitTaskReturnDetail = $this->visitTaskReturnDetailRepository->findWithoutFail($id);

        if (empty($visitTaskReturnDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskReturnDetails.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $visitTaskReturnDetail = $this->visitTaskReturnDetailRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('visitTaskReturnDetails.index'));
    }

    /**
     * Remove the specified VisitTaskReturnDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->visitTaskReturnDetailRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('visitTaskReturnDetails.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('visitTaskReturnDetails.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->visitTaskReturnDetailRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = VisitTaskReturnDetail::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (VisitTaskReturnDetail $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('visitTaskReturnDetails.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('visitTaskReturnDetails.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(VisitTaskReturnDetail $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (VisitTaskReturnDetail $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
