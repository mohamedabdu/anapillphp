<?php

namespace App\Http\Controllers\Admin;

use DB;
use Validator;
use App\Models\Category;
use App\Models\Location;
use App\Models\Competitor;
use Illuminate\Http\Request;
use App\Models\CompetitorTranslation;
use App\Http\Controllers\BackendController;

class CompetitorsController extends BackendController
{
    private $rules = [
        'active' => 'required',
        'category' => 'required',
        'city' => 'required'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->middleware('role:competitors');
        $this->data['tab'] = 'competitors';
    }
  
    public function index(Request $request)
    {
        return $this->_view("competitors.index");
    }

   
    public function create(Request $request)
    {
        $this->data['categories'] = Category::transformCollection(Category::getAll(Category::$types['store']), 'Admin');
        $this->data['countries']  = Location::transformCollection(Location::getAll(), 'Admin');
        return $this->_view('competitors.create');
    }

   
    public function store(Request $request)
    {
        try {
            $columns_arr = array(
                'title' => 'required'
            );
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);

            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            
            DB::beginTransaction();
            
            $competitor = new Competitor();
            $competitor->active = $request->input('active');
            $competitor->category_id = en_de_crypt($request->input('category'),false);
            $competitor->location_id = en_de_crypt($request->input('city'),false);
            $competitor->created_by = $this->user->id;
            $competitor->save();

            $this->createCompetitorTranslations($competitor->id, $request);
            DB::commit();
            return _json('success',_lang('app.added_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error',_lang('app.error_is_occured'),400);
        }
    }

    public function edit($id)
    {
        try 
        {
            $id = en_de_crypt($id, false);
            $competitor = Competitor::where('id',$id);
            $competitor = $this->checkCreatedBy($competitor,'competitors')
                          ->first();
            if (!$competitor) {
                session()->flash('error_message', _lang('app.this_item_doesn\'t_exist'));
                return redirect(route('competitors.index'));
            }
            $this->data['categories'] = Category::transformCollection(Category::getAll(Category::$types['store']), 'Admin');

            $competitor_location = Location::find($competitor->location_id);

            // to handle softdelete case softdelete
            $competitor_location->country_id = 0;
            $competitor_location->city_id = 0;
            $regions = [];
            $cities = [];
            /// end 

            if ($competitor_location) {
                $competitor_location_parents = explode(",", $competitor_location->parents_ids);
                $competitor->country_id = $competitor_location_parents[0];
                $competitor->region_id = $competitor_location_parents[1];

                $regions  = Location::transformCollection(Location::getAll($competitor->country_id), 'Admin');
                $cities  = Location::transformCollection(Location::getAll($competitor->region_id), 'Admin');
            }
           
            $this->data['countries']  = Location::transformCollection(Location::getAll(), 'Admin');
            $this->data['regions'] = $regions;
            $this->data['cities'] = $cities;
           

            $this->data['translations'] = CompetitorTranslation::where('competitor_id', $id)->get()->keyBy('locale');
            $this->data["competitor"] = $competitor;
            return $this->_view('competitors.edit'); 
        } 
        catch (\Exception $ex) {
            session()->flash('error_message', _lang('app.error_is_occured'));
            return redirect(route('competitors.index'));
        }
       
    }

    
    public function update(Request $request, $id)
    {
        try {
            $columns_arr = array(
                'title' => 'required'
            );
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);
            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            $id = en_de_crypt($id, false);
            $competitor = Competitor::where('id',$id);
            $competitor = $this->checkCreatedBy($competitor,'competitors')
                          ->first();
            if (!$competitor) {
                return _json('error', _lang('app.not_found'), 404);
            }
            DB::beginTransaction();
            $competitor->active = $request->input('active');
            $competitor->category_id = en_de_crypt($request->input('category'), false);
            $competitor->location_id = en_de_crypt($request->input('city'), false);
            $competitor->save();
            CompetitorTranslation::where('competitor_id', $competitor->id)->delete();
            $this->createCompetitorTranslations($competitor->id, $request);
            DB::commit();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }

    public function active($id)
    {
        try {
            $id = en_de_crypt($id, false);
            $competitor = Competitor::where('id',$id);
            $competitor = $this->checkCreatedBy($competitor,'competitors')
                          ->first();
            if (!$competitor) {
                return _json('error', _lang('app.not_found'), 404);
            }
            $competitor->active = !$competitor->active;
            $competitor->save();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }
    
    public function destroy($id)
    {
        $id = en_de_crypt($id, false);
        $competitor = Competitor::where('id',$id);
        $competitor = $this->checkCreatedBy($competitor,'competitors')
                          ->first();
        if (!$competitor) {
            return _json('error', _lang('app.error_is_occured'), 404);
        }
        DB::beginTransaction();
        try {
            $competitor->delete();
            DB::commit();
            return _json('success', _lang('app.deleted_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            if ($ex->getCode() == 23000) {
                return _json('error', _lang('app.this_record_can_not_be_deleted_for_linking_to_other_records'), 400);
            } else {

                return _json('error', _lang('app.error_is_occured'), 400);
            }
        }

    }

    public function data(Request $request) {
       
        $competitors = Competitor::join('competitor_translations', function($query){
                                $query->on('competitor_translations.competitor_id', '=', 'competitors.id')
                                ->where('competitor_translations.locale',$this->lang_code);
                            })
                            ->join('locations as cities', 'cities.id','=', 'competitors.location_id')
                            ->join('location_translations as city_trans', function($query){
                                $query->on('city_trans.location_id', '=', 'cities.id')
                                ->where('city_trans.locale',$this->lang_code);
                            })
                            ->join('locations as regions', 'regions.id','=', 'cities.parent_id')
                            ->join('location_translations as region_trans', function($query){
                                $query->on('region_trans.location_id', '=', 'regions.id')
                                ->where('region_trans.locale',$this->lang_code);
                            })
                            ->join('locations as countries', 'countries.id','=', 'regions.parent_id')
                            ->join('location_translations as country_trans', function($query){
                                $query->on('country_trans.location_id', '=', 'countries.id')
                                ->where('country_trans.locale',$this->lang_code);
                            })
                            ->join('category_translations', function($query){
                                $query->on('category_translations.category_id', '=', 'competitors.category_id')
                                ->where('category_translations.locale',$this->lang_code);
                            });
        $competitors = $this->checkCreatedBy($competitors,'competitors')
                                ->select(
                                        'competitors.*',
                                        'competitor_translations.title',
                                        'category_translations.title as category',
                                        'city_trans.title as city',
                                        'region_trans.title as region',
                                        'country_trans.title as country');
            

            return \DataTables::eloquent($competitors)
                ->addColumn('options', function ($item) {
                    $back = "";
                    $back .= '<div class="btn-group">';
                    $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '. _lang("app.options");
                    $back .= '<i class="fa fa-angle-down"></i>';
                    $back .= '</button>';
                    $back .= '<ul class = "dropdown-menu" role = "menu">';
                    $back .= '<li>';
                    $back .= '<a href="' . route('competitors.edit', en_de_crypt($item->id) ) . '">';
                    $back .= '<i class = "fas fa-edit"></i>' . _lang('app.edit');
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '<li>';
                    $back .= '<a href="" data-toggle="confirmation" onclick = "Competitors.delete(this);return false;" data-id = "' . en_de_crypt($item->id) . '">';
                    $back .= '<i class = "fas fa-trash-alt"></i>' . _lang("app.delete");
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '</ul>';
                    $back .= ' </div>';

                    return $back;
                })
                ->editColumn("active",function($item){
                    if($item->active == true){
                        $class = "btn btn-success";
                        $label = _lang('app.activated');
                    }else{
                        $class = "btn btn-danger";
                        $label = _lang('app.deactivated');
                    }
                    $back = '<button class="'.$class. '" onclick="Competitors.changeActive(this); return false;" data-id="'.en_de_crypt($item->id).'">'.$label.'</button>';
                    return $back;
                })
                ->addColumn("products",function($item){
                    $back = '<a href="'.route('competitor_products.index',['competitor' => en_de_crypt($item->id) ]).'" class="btn btn-info"  data-id="'.en_de_crypt($item->id). '"><i class="fas fa-boxes"></i> '._lang('app.products').' </a>';
                    return $back;
                })
                ->escapeColumns([])
                ->make(true);
    }

    private function createCompetitorTranslations($competitor_id, $request)
    {
        $competitor_translations = array();
        $title = $request->input('title');
        foreach ($title as $key => $value) {
            $competitor_translations[] = array(
                'locale' => $key,
                'title' => $value,
                'competitor_id' => $competitor_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
        }
        CompetitorTranslation::insert($competitor_translations);
    }

}
