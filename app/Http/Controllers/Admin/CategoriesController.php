<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Models\Category;
use App\Models\CategoryTranslation;
use Validator;
use DB;

class CategoriesController extends BackendController {

    private $rules = [
        'active' => 'required'
    ];

    public function __construct() {
        parent::__construct();

        $this->rules['type'] = 'required|in:'.implode(",", array_keys(Category::$types));
        $this->data['parent_tab'] = 'categories';
        
        if (!in_array(request()->input('type'),array_keys(Category::$types))) {
            return $this->err404();
        }
        
        if (request()->input('type') == 'store') {
            $this->middleware('role:store_categories');
            $this->data['tab'] = 'store_categories';
            $this->data['pageTitle'] = _lang('app.store_categories');
        }else{
            $this->middleware('role:product_categories');
            $this->data['tab'] = 'product_categories';
            $this->data['pageTitle'] = _lang('app.product_categories');
        }
       
    }

    public function index(Request $request) {
        $parent_id = $request->input('parent') ? $request->input('parent') : 0;
        $type = $request->input('type');
        $this->data['path'] = $this->node_path(en_de_crypt($request->input('parent'), false));
        $this->data['type'] = $type;
        $this->data['parent_id'] = $parent_id;
        return $this->_view('categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        $parent_id = $request->input('parent') ? $request->input('parent') : 0;
        $this->data['path'] = $this->node_path(en_de_crypt($request->input('parent'),false),true);
        $this->data['type'] = $request->input('type');
        $this->data['parent_id'] = $parent_id;
        return $this->_view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            $columns_arr = array(
                'title' => 'required'
            );
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);
            $validator = Validator::make($request->all(), $this->rules);

            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }

            DB::beginTransaction();
            $category = new Category;
            $category->active = $request->input('active');
            $category->parent_id = en_de_crypt($request->input('parent_id'),false);
            $category->created_by = $this->user->id;
            if ($category->parent_id != 0) {
                $parent = Category::find($category->parent_id);
                $category->level = $parent->level + 1;
                if ($parent->parents_ids == null) {
                    $category->parents_ids = $parent->id;
                } else {
                    $parent_ids = explode(",", $parent->parents_ids);
                    array_push($parent_ids, $parent->id);
                    $category->parents_ids = implode(",", $parent_ids);
                }
            } else {
                $category->level = 1;
            }
            $category->type = Category::$types[$request->input('type')];
            $category->save();

            $this->createCategoryTranslations($category->id, $request);            
            DB::commit();
            return _json('success', _lang('app.added_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $find = Category::find($id);

        if ($find) {
            return _json('success', $find);
        } else {
            return _json('error', _lang('app.error_is_occured'), 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id) {
        try {
            $id = en_de_crypt($id, false);
            $category = Category::where('id',$id);
            $category = $this->checkCreatedBy($category,'categories')
                        ->first();
            if (!$category) {
                session()->flash('error_message', _lang('app.this_item_doesn\'t_exist'));
                return redirect(route('categories.index',['type' => $request->input('type')]));
            }
            $this->data['path'] = $this->node_path($category->parent_id, true);
            $this->data['category'] = $category;
            $this->data['translations'] = CategoryTranslation::where('category_id', $id)->get()->keyBy('locale');
            $this->data['parent_id'] = en_de_crypt($category->parent_id);
            $this->data['type'] = array_search($category->type,Category::$types);
            return $this->_view('categories.edit');
        } catch (\Exception $ex) {
            session()->flash('error_message', _lang('app.error_is_occured'));
            if ($category->type == Category::$types['store']) {
                return redirect(route('categories.index',['type' => 'store']));
            }
            return redirect(route('categories.index'));
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        try {
            $id = en_de_crypt($id, false);
            $category = Category::where('id',$id);
            $category = $this->checkCreatedBy($category,'categories')
                        ->first();
            if (!$category) {
                return _json('error', _lang('app.error_is_occured'), 404);
            }

            $columns_arr = array(
                'title' => 'required'
            );
            unset($this->rules['type']);
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);
        
            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }

            DB::beginTransaction();
        
            $category->active = $request->input('active');
            $category->save();
            CategoryTranslation::where('category_id', $category->id)->delete();
            $this->createCategoryTranslations($category->id,$request);
            DB::commit();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $id = en_de_crypt($id, false);
        $category = Category::where('id',$id);
        $category = $this->checkCreatedBy($category,'categories')
                        ->first();
        if (!$category) {
            return _json('error', _lang('app.error_is_occured'), 404);
        }
        DB::beginTransaction();
        try {
            $category->delete();
            DB::commit();
            return _json('success', _lang('app.deleted_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            if ($ex->getCode() == 23000) {
                return _json('error', _lang('app.this_record_can_not_be_deleted_for_linking_to_other_records'), 400);
            } else {
                return _json('error', _lang('app.error_is_occured'), 400);
            }
        }
    }

    private function createCategoryTranslations($category_id,$request)
    {
        $category_translations = array();
        $title = $request->input('title');
        foreach ($title as $key => $value) {
            $category_translations[] = array(
                'locale' => $key,
                'title' => $value,
                'category_id' => $category_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
        }
        CategoryTranslation::insert($category_translations);
    }


    public function data(Request $request) {
        $parent_id = en_de_crypt($request->input('parent_id'),false);
        $type = $request->input('type');
        
        $categories = Category::Join('category_translations', function($query){
                    $query->on('categories.id', '=', 'category_translations.category_id')
                    ->where('category_translations.locale', $this->lang_code);
                })
                ->where('categories.parent_id', $parent_id)
                ->where('categories.type',Category::$types[$type]);
        $categories = $this->checkCreatedBy($categories,'categories')
                            ->select("categories.*", "category_translations.title");

        return \DataTables::eloquent($categories)
                        ->addColumn('options', function ($item) {

                            $back = "";
                            $back .= '<div class="btn-group">';
                            $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ' . _lang("app.options");
                            $back .= '<i class="fa fa-angle-down"></i>';
                            $back .= '</button>';
                            $back .= '<ul class = "dropdown-menu" role = "menu">';
                            $back .= '<li>';
                            $back .= '<a href="' . route('categories.edit', ['id' => en_de_crypt($item->id),'type' => array_search($item->type, Category::$types)]) . '">';
                            $back .= '<i class = "fas fa-edit"></i>' . _lang('app.edit');
                            $back .= '</a>';
                            $back .= '</li>';
                            $back .= '<li>';
                            $back .= '<a href="" data-toggle="confirmation" onclick = "Categories.delete(this);return false;" data-id = "' . en_de_crypt($item->id) . '">';
                            $back .= '<i class = "fas fa-trash-alt"></i>' . _lang("app.delete");
                            $back .= '</a>';
                            $back .= '</li>';

                            $back .= '</ul>';
                            $back .= ' </div>';
                            return $back;
                        })
                        ->editColumn('title', function ($item) {
                            if (($item->type == Category::$types['product'] && $item->level == 2) || $item->type == Category::$types['store']) {
                                $back = $item->title;
                            } else {
                                $back = '<a href="'.route('categories.index', ['type' => array_search($item->type, Category::$types), 'parent' => en_de_crypt($item->id)]).'">' . $item->title . '</a>';
                            }
                            return $back;
                        })
                        ->addColumn('active', function ($item) {
                            if ($item->active == true) {
                                $message = _lang('app.active');
                                $class = 'label-success';
                            } else {
                                $message = _lang('app.not_active');
                                $class = 'label-danger';
                            }
                            $back = '<span class="label label-sm ' . $class . '">' . $message . '</span>';
                            return $back;
                        })
                        ->escapeColumns([])
                        ->make(true);
    }

    private function node_path($id, $action = false) {
        $category = Category::where('id', $id)->first();
        $categories = null;
        if ($category) {
            $parents_ids = explode(',', $category->parents_ids);
            $parents_ids[] = $id;
            $categories = Category::leftJoin('category_translations as trans', 'categories.id', '=', 'trans.category_id')
                    ->whereIn('categories.id', $parents_ids)
                    ->where('trans.locale', $this->lang_code)
                    ->orderBy('categories.id', 'ASC')
                    ->select('categories.id', 'trans.title','categories.type')
                    ->get();
            
            $categories = $this->format_path($categories, $action);
        }
        
        return $categories;
    }

    private function format_path($categories, $action) {
        $html = '';
        if ($categories && $categories->count() > 0) {
            foreach ($categories as $key => $category) {
                if ($key < ($categories->count() - 1)) {
                    $html .= '<li><a href="'.route('categories.index',['type' => array_search($category->type,Category::$types),'parent' => en_de_crypt($category->id)]).'">' . $category->title . '</a><i class="fa fa-circle"></i></li>';
                } else {
                    if ($action) {
                        $html .= '<li><a href="'.route('categories.index',['type' => array_search($category->type,Category::$types),'parent' => en_de_crypt($category->id)]).'">' . $category->title . '</a><i class="fa fa-circle"></i></li>';
                    } else {
                        $html .= '<li><span>' . $category->title . '</span></li>';
                    }
                }
            }
        }
        return $html;
    }

}
