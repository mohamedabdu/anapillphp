<?php

namespace App\Http\Controllers\Admin;

use DB;
use Validator;
use App\Models\Product;
use App\Models\Category;
use App\Models\Competitor;
use Illuminate\Http\Request;
use App\Models\ProductTranslation;
use App\Models\ProductCompetitor;
use App\Models\CompetingProduct;
use App\Http\Controllers\BackendController;

class ProductsController extends BackendController
{
    private $rules = [
        'price' => 'required',
        'sku' => 'required',
        'active' => 'required',
        'this_order' => 'required',
        'main_category' => 'required',
        'category' => 'required',
        "image" => 'required|image|mimes:gif,png,jpeg'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->middleware('role:products');
        $this->data['tab'] = 'products';
    }
  
    public function index(Request $request)
    {
        return $this->_view("products.index");
    }

   
    public function create(Request $request)
    {
        $this->data['competitors'] = Competitor::transformCollection(Competitor::getAll(),'Admin');
        $this->data['categories'] = Category::transformCollection(Category::getAll(Category::$types['product']),'Admin');
        return $this->_view('products.create');
    }

   
    public function store(Request $request)
    {
        try {
            
            $columns_arr = array(
                'title' => 'required'
            );
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);

            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            
            DB::beginTransaction();
            
            $product = new Product();
            $product->active = $request->input('active');
            $product->this_order = $request->input('this_order');
            $product->price = $request->input('price');
            $product->sku = $request->input('sku');
            $product->category_id = en_de_crypt($request->input('category'),false);
            $product->created_by = $this->user->id;
            $product->image = Product::upload($request->file('image'), 'products');
            $product->save();
            $this->createProductTranslations($product->id, $request);

            if ($request->input('competitors')) {
                $this->createProductCompetitors($product->id, $request);
            }

            DB::commit();
            return _json('success',_lang('app.added_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error',_lang('app.error_is_occured'),400);
        }
    }

    public function edit($id)
    {
        try 
        {
            $id = en_de_crypt($id, false);
            $product = Product::where('id', $id);
            $product = $this->checkCreatedBy($product, 'products')
                        ->first();
            if (!$product) {
                session()->flash('error_message', _lang('app.this_item_doesn\'t_exist'));
                return redirect(route('products.index'));
            }
            $product_competitors = Competitor::join('competitor_translations',function($query){
                                                $query->on('competitor_translations.competitor_id','=', 'competitors.id')
                                                    ->where('competitor_translations.locale',$this->lang_code);
                                             })
                                             ->join('competitor_products', function($query){
                                                    $query->on('competitor_products.competitor_id', '=', 'competitors.id')
                                                    ->whereNull('competitor_products.deleted_at');
                                              })
                                              ->join('competitor_product_translations',function($query){
                                                  $query->on('competitor_product_translations.competitor_product_id','=', 'competitor_products.id')
                                                  ->where('competitor_product_translations.locale',$this->lang_code);
                                              })
                                              ->join('product_competitors', function($query) use($id){
                                                  $query->on('product_competitors.competitor_id', '=', 'competitors.id')
                                                  ->where('product_competitors.product_id',$id)
                                                  ->whereNull('product_competitors.deleted_at');
                                              })
                                              ->leftJoin('competing_products', function($query){
                                                  $query->on('competing_products.product_competitor_id', '=', 'product_competitors.id')
                                                  ->on('competing_products.competitor_product_id','=', 'competitor_products.id')
                                                  ->whereNull('competing_products.deleted_at');
                                              });
                                             
            $product_competitors = $this->checkCreatedBy($product_competitors,'competitors')
                                             ->select('competitors.id', 
                                                    'competitor_translations.title as competitor',
                                                    'product_competitors.id as product_competitor_id',
                                                    'competitor_products.id as competitor_product_id',
                                                    'competitor_product_translations.title as competitor_product_title',
                                                    'competing_products.id as selected')
                                             ->get();
            $data = [];
            foreach ($product_competitors as $product_competitor) {
                    if (!isset($data[$product_competitor->id]['id'])) {
                        $data[$product_competitor->id]['id'] = en_de_crypt($product_competitor->id);
                        $data[$product_competitor->id]['title'] = $product_competitor->competitor;
                        $data[$product_competitor->id]['product_competitor_id'] = en_de_crypt($product_competitor->product_competitor_id);
                        
                    }
                   $data[$product_competitor->id]['products'][] = [
                            'id' => en_de_crypt($product_competitor->competitor_product_id) ,
                            'title' => $product_competitor->competitor_product_title,
                            'selected' => $product_competitor->selected
                        ];
                
            }

            $competitors =  Competitor::join('competitor_translations', function ($query) {
                                $query->on('competitors.id', '=', 'competitor_translations.competitor_id')
                                    ->where('competitor_translations.locale', $this->lang_code);
                            })
                            ->whereNotIn('competitors.id',function ($query) use($id) {
                                $query->select('competitor_id')->from('product_competitors')
                                    ->where('product_id', $id)->whereNull('deleted_at');
                            });
            $competitors = $this->checkCreatedBy($competitors, 'competitors')
                            ->select('competitors.*', 'competitor_translations.title')
                            ->get();
           
            $product->main_category_id = Category::where('id',$product->category_id)->first()->parent_id;
            
            $this->data['competitors'] = Competitor::transformCollection($competitors, 'Admin');
            $this->data['main_categories'] = Category::transformCollection(Category::getAll(Category::$types['product']), 'Admin');
            $this->data['sub_categories'] = Category::transformCollection(Category::getAll(Category::$types['product'], $product->main_category_id), 'Admin');                                 
            $this->data['translations'] = ProductTranslation::where('product_id', $id)->get()->keyBy('locale');
            $this->data['product_competitors'] = array_values($data);
            $this->data["product"] = $product;
            return $this->_view('products.edit'); 
        } 
        catch (\Exception $ex) {
            session()->flash('error_message', _lang('app.error_is_occured'));
            return redirect(route('products.index'));
        }
       
    }

    
    public function update(Request $request, $id)
    {
        try {
            $columns_arr = array(
                'title' => 'required'
            );
            $this->rules['image'] = 'image|mimes:gif,png,jpeg';
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);
            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            $id = en_de_crypt($id, false);
            $product = Product::where('id', $id);
            $product = $this->checkCreatedBy($product, 'products')
                        ->first();
            if (!$product) {
                return _json('error', _lang('app.not_found'), 404);
            }
            DB::beginTransaction();

            $product->active = $request->input('active');
            $product->this_order = $request->input('this_order');
            $product->price = $request->input('price');
            $product->sku = $request->input('sku');
            $product->category_id = en_de_crypt($request->input('category'), false);
            if ($request->file('image')) {
                Product::deleteUploaded('products', $product->image);
                $product->image = Product::upload($request->file('image'), 'products');
            }
           
            $product->save();

            ProductTranslation::where('product_id', $product->id)->delete();
            $this->createProductTranslations($product->id, $request);

            if ($request->input('competitors')) {
                $this->updateProductCompetitors($product->id, $request);
            }
            DB::commit();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            dd($ex);
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }

    public function active($id)
    {
        try {
            $id = en_de_crypt($id, false);
            $product = Product::where('id',$id);
            $product = $this->checkCreatedBy($product,'products')
                            ->first();
            if (!$product) {
                return _json('error', _lang('app.not_found'), 404);
            }
            $product->active = !$product->active;
            $product->save();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }
    
    public function destroy($id)
    {
        $id = en_de_crypt($id, false);
        $product = Product::where('id', $id);
        $product = $this->checkCreatedBy($product, 'products')
                    ->first();
        if (!$product) {
            return _json('error', _lang('app.error_is_occured'), 404);
        }
        DB::beginTransaction();
        try {
            $product->delete();
            DB::commit();
            return _json('success', _lang('app.deleted_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            if ($ex->getCode() == 23000) {
                return _json('error', _lang('app.this_record_can_not_be_deleted_for_linking_to_other_records'), 400);
            } else {

                return _json('error', _lang('app.error_is_occured'), 400);
            }
        }

    }

    public function data(Request $request) {
       
        $products = Product::join('product_translations', function($query){
                                $query->on('product_translations.product_id', '=', 'products.id')
                                ->where('product_translations.locale',$this->lang_code);
                            })
                            ->join('categories as sub_category', 'sub_category.id','=','products.category_id')
                            ->join('category_translations as sub_category_trans', function($query){
                                $query->on('sub_category_trans.category_id', '=', 'sub_category.id')
                                ->where('sub_category_trans.locale',$this->lang_code);
                            })
                            ->join('categories as main_category', 'main_category.id','=', 'sub_category.parent_id')
                            ->join('category_translations as main_category_trans', function($query){
                                $query->on('main_category_trans.category_id', '=', 'main_category.id')
                                ->where('main_category_trans.locale',$this->lang_code);
                            });
        $products = $this->checkCreatedBy($products,'products')
                                ->select(
                                        'products.*',
                                        'product_translations.title',
                                        'sub_category_trans.title as category',
                                        'main_category_trans.title as main_category');
            
            
            return \DataTables::eloquent($products)
                ->addColumn('options', function ($item) {
                    $back = "";
                    $back .= '<div class="btn-group">';
                    $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '. _lang("app.options");
                    $back .= '<i class="fa fa-angle-down"></i>';
                    $back .= '</button>';
                    $back .= '<ul class = "dropdown-menu" product = "menu">';
                    $back .= '<li>';
                    $back .= '<a href="' . route('products.edit', en_de_crypt($item->id) ) . '">';
                    $back .= '<i class = "fas fa-edit"></i>' . _lang('app.edit');
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '<li>';
                    $back .= '<a href="" data-toggle="confirmation" onclick = "Products.delete(this);return false;" data-id = "' . en_de_crypt($item->id) . '">';
                    $back .= '<i class = "fas fa-trash-alt"></i>' . _lang("app.delete");
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '</ul>';
                    $back .= ' </div>';

                    return $back;
                })
                ->editColumn("active",function($item){
                    if($item->active == true){
                        $class = "btn btn-success";
                        $label = _lang('app.activated');
                    }else{
                        $class = "btn btn-danger";
                        $label = _lang('app.deactivated');
                    }
                    $back = '<button class="'.$class. '" onclick="Products.changeActive(this); return false;" data-id="'.en_de_crypt($item->id).'">'.$label.'</button>';
                    return $back;
                })
                ->escapeColumns([])
                ->make(true);
    }

    private function createProductTranslations($product_id, $request)
    {
        $product_translations = array();
        $title = $request->input('title');
        foreach ($title as $key => $value) {
            $product_translations[] = array(
                'locale' => $key,
                'title' => $value,
                'product_id' => $product_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
        }
        ProductTranslation::insert($product_translations);
    }

    private function createProductCompetitors($product_id, $request)
    {
        $competitors = $request->input('competitors');
        foreach ($competitors as $competitor) {
            $competing_products = array();

            $product_competitor = new ProductCompetitor();
            $product_competitor->product_id = $product_id;
            $product_competitor->competitor_id = en_de_crypt($competitor['competitor'],false);
            $product_competitor->save();

            $this->createCompetingProducts($product_competitor->id, $competitor['competitor_products']);
        }
    }

    private function createCompetingProducts($product_competitor_id, $competitor_products)
    {
        foreach ($competitor_products as $competitor_product) {
            $competing_products[] = [
                'product_competitor_id' => $product_competitor_id,
                'competitor_product_id' => en_de_crypt($competitor_product, false),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }
        CompetingProduct::insert($competing_products);
    }

    private function updateProductCompetitors($product_id, $request)
    {
        $competitors = $request->input('competitors');
        foreach ($competitors as $competitor) {
            $competing_products = array();
            $product_competitor = ProductCompetitor::updateOrCreate(
                ['product_id' => $product_id, 'competitor_id' => en_de_crypt($competitor['competitor'], false)],
                []
            );

            $competing_products  = CompetingProduct::where('product_competitor_id', $product_competitor->id)->get()->pluck('competitor_product_id');

            $competing_products = $competing_products->map(function($competing_product){
                return en_de_crypt($competing_product);
            });
            
               
            $competing_products_delete_diff = array_diff($competing_products->toArray(), $competitor['competitor_products']);
            $competing_products_insert_diff = array_diff($competitor['competitor_products'], $competing_products->toArray());

            if (!empty($competing_products_delete_diff)) {
                foreach ($competing_products_delete_diff as $competing_product) {
                    $competing_product = CompetingProduct::where('competitor_product_id',en_de_crypt($competing_product, false))
                                                    ->where('product_competitor_id', $product_competitor->id)
                                                    ->first();
                    $competing_product->delete();
                }
            }
            if (!empty($competing_products_insert_diff)) {
                $this->createCompetingProducts($product_competitor->id, $competing_products_insert_diff);
            }

        }
    }

   

    


}
