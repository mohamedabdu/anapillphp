<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStoreRequest;
use App\Http\Requests\UpdateStoreRequest;
use App\Models\StoreCategory;
use App\Models\StoreImage;
use App\Models\SubCategory;
use App\Repositories\StoreRepository;
use function Helper\Common\__lang;
use function Helper\Common\upload;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\Store;
use function Helper\Common\imageUrl;
use Illuminate\Support\Facades\DB;
use Validator;

class StoreController extends AppBaseController
{
    /** @var  StoreRepository */
    private $storeRepository;

    public function __construct(StoreRepository $storeRepo)
    {
        parent::__construct();
        $this->storeRepository = $storeRepo;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
    /*
        $this->storeRepository->pushCriteria(new RequestCriteria($request));
        $stores = $this->storeRepository->all();
        */

        return $this->_view('stores.index');
        /*return view('stores.index')
             ->with('stores', $stores);*/
    }
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function map(Request $request)
    {
        $store = Store::select(["id","lat","lng","title_ar"])->get()->toArray();
  
        $this->data["showStoresMap"] = 1;
        $this->data["stores"] = json_encode(array_values($store));
        return $this->_view('stores.map');

    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return $this->_view('stores.create');
    }

    /**
     * @param CreateStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateStoreRequest $request)
    {
//        $rules = [
//            'title_ar' => 'required',
//            'title_en' => 'required',
//            'category_id' => 'required',
//            'country_id' => 'required',
//            'region_id' => 'required',
//            'city_id' => 'required',
////        'lat' => 'required',
////        'lng' => 'required',
//            'address_note' => 'required',
//            'is_main' => 'required',
////        'parent_id' => 'required',
//            'email' => 'email',
//            'manager_email' => 'email',
//            'manager_mobile' => 'numeric|regex:(05)|min:10',
//            'mobile' => 'numeric|regex:(05)|min:10',
//        ];
//        $validator = Validator::make($request->all(), $rules);
//        if ($validator->fails()) {
//            $errors = $validator->errors()->toArray();
//            return _json('error', $errors);
//        }

        $input = $request->request->all();

        $input['logo'] = $this->uploadFile($request,"logo",false);
        $input["created_by"] = auth()->id();
        if($input["is_main"] == 1) {
            $input["parent_id"] = null;
        }
        DB::beginTransaction();
        try{
            $store = $this->storeRepository->create($input);


            /** create images */
            if($request->hasFile("images")) {
                foreach ($request->file("images") as $item) {
                    $image = upload($item, false)["img"];
                    $storeImage = new StoreImage();
                    $storeImage->store_id = $store->id;
                    $storeImage->image = $image;
                    $storeImage->save();
                }
            }
            /** end */
            /** create sub categories */
            if(isset($input["sub_sub_categories"]) && $input["sub_sub_categories"] != null){
                foreach ($input["sub_sub_categories"] as $sub_sub_category) {
                    $storeSubCategory = new StoreCategory();
                    $storeSubCategory->store_id = $store->id;
                    $storeSubCategory->sub_category_id = $sub_sub_category;
                    $storeSubCategory->save();
                }
            } else if($input["sub_categories"] != null || isset($input["sub_categories"])) {
                foreach ($input["sub_categories"] as $sub_category) {
                    $storeSubCategory = new StoreCategory();
                    $storeSubCategory->store_id = $store->id;
                    $storeSubCategory->sub_category_id = $sub_category;
                    $storeSubCategory->save();
                }
            }
            /** end */
            DB::commit();
            $data['message'] ='تم حفظ البيانات بنجاح';
            $data['success'] = true;
            $data["type"] = "success";
            return $data;
        }catch (\Exception $exception) {
            dd($exception->getMessage());
            DB::rollback();
            $data['message'] ='لم يتم حفظ البيانات بنجاح';
            $data['success'] = false;
            $data["type"] = "error";
            return $data;
        }

    }

    /**
     * Display the specified Store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $store = $this->storeRepository->findWithoutFail($id);

        if (empty($store)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('stores.index'));
        }

        $subCategories = $store->subCategories()->first();
        if($subCategories != null) {
            $subCategory = SubCategory::find($subCategories->sub_category_id);
            if($subCategory->parent_id > 0) {
                $store->has_sub_sub = true;
                $subCategoriesStore = $store->subCategories()->pluck("sub_category_id")->toArray();
                $categories = SubCategory::whereIn("id",$subCategoriesStore)->pluck("parent_id")->toArray();
                $parents = SubCategory::whereIn("id",$categories)->get();
                                $subsubCategories = SubCategory::whereIn("id",$subCategoriesStore)->get();

                $store->subCategory = $parents;
                 $store->subSubCategory = $subsubCategories;

            } else {
                $store->subCategory = SubCategory::whereIn("id", $store->subCategories()->pluck("sub_category_id")->toArray())->get();
                $store->has_sub_sub = false;
            }
        } else {
            $store->has_sub_sub = false;
        }
        $lat = $store->lat;
        $lng = $store->lng;
        $this->data["lat"] = $lat;
        $this->data["lng"] = $lng;
        $this->data["store"] = $store;
        return $this->_view('stores.show');
    }

    /**
     * Show the form for editing the specified Store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $store = $this->storeRepository->findWithoutFail($id);

        if (empty($store)) {
            Flash::error('عفوا المحل معطل الأن');

            return redirect(route('stores.index'));
        }

        $subCategories = $store->subCategories()->first();
        if($subCategories != null) {
            $subCategory = SubCategory::find($subCategories->sub_category_id);
            if($subCategory->parent_id > 0) {
                $store->has_sub_sub = true;
                $subCategoriesStore = $store->subCategories()->pluck("sub_category_id")->toArray();
                $categories = SubCategory::whereIn("id",$subCategoriesStore)->pluck("parent_id")->toArray();
                $parents = SubCategory::whereIn("id",$categories)->get();
                $store->subCategoriesParent = $parents;
            } else {
                $store->has_sub_sub = false;
            }
        } else {
            $store->has_sub_sub = false;
        }
        $this->data["store"] = $store;
        $this->data["lat"] = $store->lat;
        $this->data["lng"] = $store->lng;
        return $this->_view('stores.edit');
    }


    /**
     * @param $id
     * @param UpdateStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update($id, Request $request)
    {


        $store = $this->storeRepository->findWithoutFail($id);

        if (empty($store)) {
            $data['message'] ='لم يتم التعديل';
            $data['success'] = false;
            $data["type"] = "false";
            $data["status_code"] = 400;
            return $this->respondBadRequest($data["message"]);
        }


        $input = $request->request->all();

        if($request->hasFile("logo"))
            $input['logo'] = $this->uploadFile($request,"logo",false);

        if($input["is_main"] == 1) {
            $input["parent_id"] = null;
        }

        DB::beginTransaction();
        try{
            /** @var Store $store */
            $store = $this->storeRepository->update($input, $id);

            /** create images */
            if($request->hasFile("images")) {
                $store->images()->delete();
                foreach ($request->file("images") as $item) {
                    $image = upload($item, false)["img"];
                    $storeImage = new StoreImage();
                    $storeImage->store_id = $store->id;
                    $storeImage->image = $image;
                    $storeImage->save();
                }
            }
            /** end */
            /** create sub categories */
            if(isset($input["sub_sub_categories"]) && $input["sub_sub_categories"] != null){
                $store->subCategories()->delete();
                foreach ($input["sub_sub_categories"] as $sub_sub_category) {
                    $storeSubCategory = new StoreCategory();
                    $storeSubCategory->store_id = $store->id;
                    $storeSubCategory->sub_category_id = $sub_sub_category;
                    $storeSubCategory->save();
                }
            } else if($input["sub_categories"] != null || isset($input["sub_categories"])) {
                $store->subCategories()->delete();
                foreach ($input["sub_categories"] as $sub_category) {
                    $storeSubCategory = new StoreCategory();
                    $storeSubCategory->store_id = $store->id;
                    $storeSubCategory->sub_category_id = $sub_category;
                    $storeSubCategory->save();
                }
            }
            /** end */
            DB::commit();

            $data["data"]['message'] ='تم تعديل البيانات بنجاح';
            $data['success'] = true;
            $data["type"] = "success";
            return $data;
        }catch (\Exception $exception) {
            dd($exception->getMessage());
            DB::rollback();
            $data['message'] ='لم يتم التعديل';
            $data['success'] = false;
            $data["type"] = "false";
            $data["status_code"] = 400;
            return $this->respondBadRequest($data["message"]);
        }


    }

    /**
     * Remove the specified Store from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->storeRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('stores.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('stores.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->storeRepository->delete($id);
            }
        } else if($id != null) {
            $country = Store::withTrashed()->find($id);
            if($country->deleted_at == null) {
                $country->delete();
            }

        } else  {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            $data['type'] = "false";
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        $data['type'] = "success";
        return $data;

    }

    public function active($id,Request $request)
    {
        /** @var Store $category */
        $category = Store::withTrashed()->find($id);
        if (empty($category)) {
            $data['success'] = false;
            $data['message'] = __lang("error_no_data");
            return $data;
        }
        if($category->deleted_at != null)
        {
            $category->restore();
            //$category->save();
        }
        $data['message'] = 'تم حفظ البيانات بنجاح';
        $data['success'] = true;
        return $data;
    }

    public function data() {
            $items = new Store();
            $items = $this->checkCompany($items);
            $items = $items->join("categories","categories.id", "=","stores.category_id");
            $items = $items->join("cities","cities.id", "=","stores.city_id");
            $items = $items->withTrashed();
            $items = $items->orderByDesc("stores.id")->select(["stores.*","cities.title_ar as city","categories.title_ar as category"]);

            return DataTables::eloquent($items)
                ->addColumn('options', function (Store $item) {
                    $back = "";
                    $back .= '<div class="btn-group">';
                    $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '. _lang("app.options");
                    $back .= '<i class="fa fa-angle-down"></i>';
                    $back .= '</button>';
                    $back .= '<ul class = "dropdown-menu" role = "menu">';
                    $back .= '<li>';
                    $back .= '<a href="' . route('stores.show', $item->id) . '">';
                    $back .= '<i class = "icon-docs"></i>' . _lang('app.show');
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '<li>';
                    $back .= '<a href="' . route('stores.edit', $item->id) . '">';
                    $back .= '<i class = "icon-docs"></i>' . _lang('app.edit');
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '<li>';
                    $back .= '<a href="" data-toggle="confirmation" onclick = "Stores.delete(this);return false;" data-id = "' . $item->id . '">';
                    $back .= '<i class = "icon-docs"></i>' . _lang("app.delete");
                    $back .= '</a>';
                    $back .= '</li>';

                    $back .= '</ul>';
                    $back .= ' </div>';

                    return $back;

                })
                ->addColumn("work_days", function (Store $item) {
                    $back = '<a class="btn btn-warning" href="'.route("storeWorkDays.index").'?store_id='.$item->id.'">';
                    $back.= "مواعيد العمل"."</a>";
                    return $back;
                })
                /*
                 ->addColumn("active",function(Store $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (Store $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->addColumn("active",function(Store $item){
                    if($item->deleted_at == null)
                    {
                        $back = '<a class="btn btn-warning" onclick="Stores.changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="Stores.changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                })
                ->editColumn('logo', function (Store $item) {
                    $back = ' <img src= "'.imageUrl($item->logo).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                })
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
