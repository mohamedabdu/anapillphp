<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Models\Category;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Models\CompetitorProduct;
use App\Models\ProductCompetitor;
use App\Http\Controllers\BackendController;
use DB;

class AjaxController extends BackendController {

   
    public function change_lang(Request $request) {
        $lang_code = $request->input('lang_code');
        $long = 7 * 60 * 24;
        return response()->json([
                    'type' => 'success',
                    'message' => $lang_code
                ])->cookie('AdminLang', $lang_code, $long);
    }

    public function getLocations($parent_id)
    {
        try {
            $parent_id = en_de_crypt($parent_id,false);
            $locations = Location::transformCollection(Location::getAll($parent_id),'Admin');
            return _json('success',$locations);
        } catch (\Exception $ex) {
            return _json('error',_lang('app.error_is_occured'),400);
        }
    }

    public function getCategories($parent_id)
    {
        try {
            $parent_id = en_de_crypt($parent_id, false);
            $categories = Category::transformCollection(Category::getAll(Category::$types['product'],$parent_id), 'Admin');
            return _json('success', $categories);
        } catch (\Exception $ex) {
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }

    public function getCompetitorProducts($competitor_id)
    {
        try {
            $competitor_id = en_de_crypt($competitor_id, false);
            $competitor_products = CompetitorProduct::transformCollection(CompetitorProduct::getAll($competitor_id), 'Admin');
            return _json('success', $competitor_products);
        } catch (\Exception $ex) {
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }

    public function removeProductCompetitor($product_competitor_id)
    {
        try {
            $product_competitor_id = en_de_crypt($product_competitor_id, false);
            
            $product_competitor = ProductCompetitor::join('competitors', 'competitors.id', '=', 'product_competitors.competitor_id')
                ->where('product_competitors.id', $product_competitor_id);
            $product_competitor = $this->checkCreatedBy($product_competitor, 'competitors')
                                    ->select('product_competitors.*')
                                    ->first();
            if (!$product_competitor) {
                return _json('error', _lang('app.not_found'), 404);
            }
           
            DB::beginTransaction();

            $product_competitor->delete();

            DB::commit();
            return _json('success',_lang('app.deleted_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error', _lang('app.error_is_occured'), 400);
        }
       

    }
    

    
    public function deleteImage(Request $request)
    {
        try {
            $column_name = $request->input('col');

            // for edit form for non uploaded images
            if (!$request->input('model')) {
                return _json('success', _lang('app.updated_successfully'));
            }
            //

            $model = "App\Models\\" . $request->input('model');
            $model::deleteUploaded($request->input('folder'), $request->input('image'));

            $find = $model::find($request->input('id'));
            if (!json_decode($find->$column_name)) {
                $find->$column_name = "";
            } else {
                $images = json_decode($find->$column_name);
                $image_index = array_search($request->input('image'), $images);
                
                if (isset($images[$image_index])) {
                    unset($images[$image_index]); 
                }
                if (count($images) == 0) {
                    $find->$column_name = "";
                }else{
                    //$find->$column_name = json_encode(array_values($images));
                    $find->$column_name = json_encode($images);
                }
            }
            $find->save();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            return _json('error', _lang('app.error_is_occured'), 400);
        }
       

    }
}
