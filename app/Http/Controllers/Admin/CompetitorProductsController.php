<?php

namespace App\Http\Controllers\Admin;

use DB;
use Validator;
use App\Models\Competitor;
use Illuminate\Http\Request;
use App\Models\CompetitorProduct;
use App\Http\Controllers\BackendController;
use App\Models\CompetitorProductTranslation;

class CompetitorProductsController extends BackendController
{
    private $rules = [
        'active' => 'required',
        'this_order' => 'required',
        'competitor' => 'required'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->middleware('role:competitor_products');
        $this->data['tab'] = request()->has('competitor') ? 'competitors' : 'competitor_products';
        
    }
  
    public function index(Request $request)
    {
        $this->data['competitor_id'] = $request->input('competitor') ?: 0;
        if ($request->input('competitor')) {
            $competitor = $this->getCompetitor($request->input('competitor'));
            if (!$competitor) {
                return $this->err404();
            }
            $this->data['competitor'] = $competitor;
        }
        return $this->_view("competitor_products.index");
    }

   
    public function create(Request $request)
    {
        try {
            $this->data['competitors'] = Competitor::transformCollection(Competitor::getAll(), 'Admin');
            if ($request->input('competitor')) {
                $competitor = $this->getCompetitor($request->input('competitor'));
                if (!$competitor) {
                    return $this->err404();
                }
                $this->data['competitor'] = $competitor;
            }
            return $this->_view('competitor_products.create');
        } 
        catch (\Exception $ex) {
            session()->flash('error_message', _lang('app.error_is_occured'));
            return redirect(route('competitor_products.index'));
        }
    }

   
    public function store(Request $request)
    {
        try {
            $columns_arr = array(
                'title' => 'required'
            );
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);

            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            
            DB::beginTransaction();
            
            $competitor_product = new CompetitorProduct();
            $competitor_product->active = $request->input('active');
            $competitor_product->this_order = $request->input('this_order');
            $competitor_product->competitor_id = en_de_crypt($request->input('competitor'),false);
            $competitor_product->created_by = $this->user->id;
            $competitor_product->save();

            $this->createCompetitorProductTranslations($competitor_product->id, $request);
            DB::commit();
            return _json('success',_lang('app.added_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error',_lang('app.error_is_occured'),400);
        }
    }

    public function edit($id)
    {
        try 
        {
            $id = en_de_crypt($id, false);
            $competitor_product = CompetitorProduct::where('id',$id);
            $competitor_product = $this->checkCreatedBy($competitor_product,'competitor_products')
                                        ->first();
            
            if (!$competitor_product) {
                session()->flash('error_message', _lang('app.this_item_doesn\'t_exist'));
                return redirect(route('competitor_products.index'));
            }
            $this->data['competitors'] = Competitor::transformCollection(Competitor::getAll(), 'Admin');
            $this->data['translations'] = CompetitorProductTranslation::where('competitor_product_id', $id)->get()->keyBy('locale');
            $this->data["competitor_product"] = $competitor_product;
            return $this->_view('competitor_products.edit'); 
        } 
        catch (\Exception $ex) {
            session()->flash('error_message', _lang('app.error_is_occured'));
            return redirect(route('competitor_products.index'));
        }
       
    }

    
    public function update(Request $request, $id)
    {
        try {
            $columns_arr = array(
                'title' => 'required'
            );
            
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);
            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            $id = en_de_crypt($id, false);
            $competitor_product = CompetitorProduct::where('id',$id);
            $competitor_product = $this->checkCreatedBy($competitor_product,'competitor_products')
                                        ->first();
            if (!$competitor_product) {
                return _json('error', _lang('app.not_found'), 404);
            }
            DB::beginTransaction();

            $competitor_product->competitor_id = en_de_crypt($request->input('competitor'), false);
            $competitor_product->active = $request->input('active');
            $competitor_product->this_order = $request->input('this_order');
            $competitor_product->save();

            CompetitorProductTranslation::where('competitor_product_id', $competitor_product->id)->delete();
            $this->createCompetitorProductTranslations($competitor_product->id, $request);

            DB::commit();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }

    public function active($id)
    {
        try {
            $id = en_de_crypt($id, false);
            $competitor_product = CompetitorProduct::where('id',$id);
            $competitor_product = $this->checkCreatedBy($competitor_product,'competitor_products')
                                        ->first();
            if (!$competitor_product) {
                return _json('error', _lang('app.not_found'), 404);
            }
            $competitor_product->active = !$competitor_product->active;
            $competitor_product->save();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }
    
    public function destroy($id)
    {
        $id = en_de_crypt($id, false);
       $competitor_product = CompetitorProduct::where('id',$id);
        $competitor_product = $this->checkCreatedBy($competitor_product,'competitor_products')
                                    ->first();
        if (!$competitor_product) {
            return _json('error', _lang('app.error_is_occured'), 404);
        }
        DB::beginTransaction();
        try {
            $competitor_product->delete();
            DB::commit();
            return _json('success', _lang('app.deleted_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            if ($ex->getCode() == 23000) {
                return _json('error', _lang('app.this_record_can_not_be_deleted_for_linking_to_other_records'), 400);
            } else {

                return _json('error', _lang('app.error_is_occured'), 400);
            }
        }

    }

    public function data(Request $request) {
        $competitor_id =  en_de_crypt($request->input('competitor'),false);
        
        $competitor_products = CompetitorProduct::join('competitor_product_translations', function($query){
                                    $query->on('competitor_product_translations.competitor_product_id', '=', 'competitor_products.id')
                                    ->where('competitor_product_translations.locale',$this->lang_code);
                                })
                                ->join('competitors', 'competitors.id','=', 'competitor_products.competitor_id')
                                ->join('competitor_translations', function($query){
                                    $query->on('competitor_translations.competitor_id', '=', 'competitors.id')
                                    ->where('competitor_translations.locale',$this->lang_code);
                                });
                                if ($competitor_id) {
                                   $competitor_products->where('competitors.id', $competitor_id);
                                }
        $competitor_products = $this->checkCreatedBy($competitor_products,'competitor_products')
                                ->select(
                                        'competitor_products.*',
                                        'competitor_product_translations.title',
                                        'competitor_translations.title as competitor');
            

            return \DataTables::eloquent($competitor_products)
                ->addColumn('options', function ($item) {
                    $back = "";
                    $back .= '<div class="btn-group">';
                    $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '. _lang("app.options");
                    $back .= '<i class="fa fa-angle-down"></i>';
                    $back .= '</button>';
                    $back .= '<ul class = "dropdown-menu" competitor_product = "menu">';
                    $back .= '<li>';
                    $back .= '<a href="' . route('competitor_products.edit', en_de_crypt($item->id) ) . '">';
                    $back .= '<i class = "fas fa-edit"></i>' . _lang('app.edit');
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '<li>';
                    $back .= '<a href="" data-toggle="confirmation" onclick = "CompetitorProducts.delete(this);return false;" data-id = "' . en_de_crypt($item->id) . '">';
                    $back .= '<i class = "fas fa-trash-alt"></i>' . _lang("app.delete");
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '</ul>';
                    $back .= ' </div>';

                    return $back;
                })
                ->editColumn("active",function($item){
                    if($item->active == true){
                        $class = "btn btn-success";
                        $label = _lang('app.activated');
                    }else{
                        $class = "btn btn-danger";
                        $label = _lang('app.deactivated');
                    }
                    $back = '<button class="'.$class. '" onclick="CompetitorProducts.changeActive(this); return false;" data-id="'.en_de_crypt($item->id).'">'.$label.'</button>';
                    return $back;
                })
                ->escapeColumns([])
                ->make(true);
    }

    private function createCompetitorProductTranslations($competitor_product_id, $request)
    {
        $competitor_product_translations = array();
        $title = $request->input('title');
        foreach ($title as $key => $value) {
            $competitor_product_translations[] = array(
                'locale' => $key,
                'title' => $value,
                'competitor_product_id' => $competitor_product_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
        }
        CompetitorProductTranslation::insert($competitor_product_translations);
    }

    private function getCompetitor($comeptitor_id)
    {
        $id = en_de_crypt($comeptitor_id, false);
        $competitor = Competitor::join('competitor_translations', function($query){
                                $query->on('competitor_translations.competitor_id', '=', 'competitors.id')
                                ->where('competitor_translations.locale',$this->lang_code);
                            })
                           ->where('competitors.id', $id);
        $competitor = $this->checkCreatedBy($competitor, 'competitors')
                      ->select('competitors.*', 'competitor_translations.title')
                        ->first();
        return $competitor ?: false;
    }

}
