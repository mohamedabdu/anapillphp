<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAgentHolidayRequest;
use App\Http\Requests\UpdateAgentHolidayRequest;
use App\Repositories\AgentHolidayRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\AgentHoliday;
use function Helper\Common\imageUrl;

class AgentHolidayController extends AppBaseController
{
    /** @var  AgentHolidayRepository */
    private $agentHolidayRepository;

    public function __construct(AgentHolidayRepository $agentHolidayRepo)
    {
        parent::__construct();
        $this->agentHolidayRepository = $agentHolidayRepo;
    }







    /**
     * Display a listing of the AgentHoliday.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->agentHolidayRepository->pushCriteria(new RequestCriteria($request));
        $agentHolidays = $this->agentHolidayRepository->all();
        */

        return view('agent_holidays.index');
        /*return view('agent_holidays.index')
             ->with('agentHolidays', $agentHolidays);*/
    }



    /**
     * Show the form for creating a new AgentHoliday.
     *
     * @return Response
     */
    public function create()
    {
        return view('agent_holidays.create');
    }

    /**
     * Store a newly created AgentHoliday in storage.
     *
     * @param CreateAgentHolidayRequest $request
     *
     * @return Response
     */
    public function store(CreateAgentHolidayRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $agentHoliday = $this->agentHolidayRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('agentHolidays.index'));
    }

    /**
     * Display the specified AgentHoliday.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $agentHoliday = $this->agentHolidayRepository->findWithoutFail($id);

        if (empty($agentHoliday)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('agentHolidays.index'));
        }

        return view('agent_holidays.show')->with('agentHoliday', $agentHoliday);
    }

    /**
     * Show the form for editing the specified AgentHoliday.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $agentHoliday = $this->agentHolidayRepository->findWithoutFail($id);

        if (empty($agentHoliday)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('agentHolidays.index'));
        }

        return view('agent_holidays.edit')->with('agentHoliday', $agentHoliday);
    }

    /**
     * Update the specified AgentHoliday in storage.
     *
     * @param  int              $id
     * @param UpdateAgentHolidayRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAgentHolidayRequest $request)
    {


        $agentHoliday = $this->agentHolidayRepository->findWithoutFail($id);

        if (empty($agentHoliday)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('agentHolidays.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $agentHoliday = $this->agentHolidayRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('agentHolidays.index'));
    }

    /**
     * Remove the specified AgentHoliday from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->agentHolidayRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('agentHolidays.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('agentHolidays.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->agentHolidayRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = AgentHoliday::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (AgentHoliday $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('agentHolidays.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('agentHolidays.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(AgentHoliday $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (AgentHoliday $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
