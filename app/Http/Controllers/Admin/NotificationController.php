<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateNotificationRequest;
use App\Http\Requests\UpdateNotificationRequest;
use App\Models\Agent;
use App\Models\Supervisor;
use App\Repositories\NotificationRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\Notification;
use function Helper\Common\imageUrl;
use function Helper\Common\sendNoti;

class NotificationController extends AppBaseController
{
    /** @var  NotificationRepository */
    private $notificationRepository;

    public function __construct(NotificationRepository $notificationRepo)
    {
        parent::__construct();
        $this->notificationRepository = $notificationRepo;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
    /*
        $this->notificationRepository->pushCriteria(new RequestCriteria($request));
        $notifications = $this->notificationRepository->all();
        */

        return view('notifications.index');
        /*return view('notifications.index')
             ->with('notifications', $notifications);*/
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return $this->_view('notifications.create');
    }

    /**
     * @param CreateNotificationRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CreateNotificationRequest $request)
    {

        $input = $request->request->all();
        $input["type"] = Notification::$types['GENERAL'];

        $androidTokens = [];
        $iosTokens = [];

        if(isset($input["type_select"]) && $input["type_select"] == 0) {
            $agentsSelected = $input["agents"];
            if(isset($agentsSelected) && $agentsSelected[0] == null) {
                $androidTokens = Agent::where("device_type",1)->get()->pluck("device_token")->toArray();
                $iosTokens = Agent::where("device_type",2)->get()->pluck("device_token")->toArray();
            } else {
                foreach ($agentsSelected as $item) {
                    $agent = Agent::find($item);
                    if($agent != null && $agent->device_type == 1) {
                        $androidTokens[] = $agent->device_token;
                    } else if($agent != null && $agent->device_type == 2) {
                        $iosTokens[] = $agent->device_token;
                    }
                }
            }
        } else if (isset($input["type_select"]) && $input["type_select"] == 1) {
            $supervisorsSelected = $input["supervisors"];
            if(isset($supervisorsSelected) && $supervisorsSelected[0] == null) {
                $androidTokens = Supervisor::where("device_type",1)->get()->pluck("device_token")->toArray();
                $iosTokens = Supervisor::where("device_type",2)->get()->pluck("device_token")->toArray();
            } else {
                foreach ($supervisorsSelected as $item) {
                    $supervisor = Supervisor::find($item);
                    if($supervisor != null && $supervisor->device_type == 1) {
                        $androidTokens[] = $supervisor->device_token;
                    } else if($supervisor != null && $supervisor->device_type == 2) {
                        $iosTokens[] = $supervisor->device_token;
                    }
                }
            }
        }

        $input["created_by"] = auth()->id();
        $input["title"] = config("app.name");

        $notification = $this->notificationRepository->create($input);

        sendNoti($iosTokens, $input["body"],null, 2);
        sendNoti($androidTokens, $input["body"],null, 1);


        $data['message'] ='تم ارسال الأشعار بنجاح';
        $data['success'] = true;
        $data["type"] = "success";
        return $data;
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $notification = $this->notificationRepository->findWithoutFail($id);

        if (empty($notification)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('notifications.index'));
        }

        return view('notifications.show')->with('notification', $notification);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $notification = $this->notificationRepository->findWithoutFail($id);

        if (empty($notification)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('notifications.index'));
        }

        return view('notifications.edit')->with('notification', $notification);
    }

    /**
     * @param $id
     * @param UpdateNotificationRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update($id, UpdateNotificationRequest $request)
    {


        $notification = $this->notificationRepository->findWithoutFail($id);

        if (empty($notification)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('notifications.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $notification = $this->notificationRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('notifications.index'));
    }

    /**
     * Remove the specified Notification from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->notificationRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('notifications.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('notifications.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->notificationRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = Notification::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (Notification $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('notifications.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('notifications.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(Notification $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (Notification $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
