<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVisitTaskAreaDetailRequest;
use App\Http\Requests\UpdateVisitTaskAreaDetailRequest;
use App\Repositories\VisitTaskAreaDetailRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\VisitTaskAreaDetail;
use function Helper\Common\imageUrl;

class VisitTaskAreaDetailController extends AppBaseController
{
    /** @var  VisitTaskAreaDetailRepository */
    private $visitTaskAreaDetailRepository;

    public function __construct(VisitTaskAreaDetailRepository $visitTaskAreaDetailRepo)
    {
        parent::__construct();
        $this->visitTaskAreaDetailRepository = $visitTaskAreaDetailRepo;
    }







    /**
     * Display a listing of the VisitTaskAreaDetail.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->visitTaskAreaDetailRepository->pushCriteria(new RequestCriteria($request));
        $visitTaskAreaDetails = $this->visitTaskAreaDetailRepository->all();
        */

        return view('visit_task_area_details.index');
        /*return view('visit_task_area_details.index')
             ->with('visitTaskAreaDetails', $visitTaskAreaDetails);*/
    }



    /**
     * Show the form for creating a new VisitTaskAreaDetail.
     *
     * @return Response
     */
    public function create()
    {
        return view('visit_task_area_details.create');
    }

    /**
     * Store a newly created VisitTaskAreaDetail in storage.
     *
     * @param CreateVisitTaskAreaDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateVisitTaskAreaDetailRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $visitTaskAreaDetail = $this->visitTaskAreaDetailRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('visitTaskAreaDetails.index'));
    }

    /**
     * Display the specified VisitTaskAreaDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visitTaskAreaDetail = $this->visitTaskAreaDetailRepository->findWithoutFail($id);

        if (empty($visitTaskAreaDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskAreaDetails.index'));
        }

        return view('visit_task_area_details.show')->with('visitTaskAreaDetail', $visitTaskAreaDetail);
    }

    /**
     * Show the form for editing the specified VisitTaskAreaDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visitTaskAreaDetail = $this->visitTaskAreaDetailRepository->findWithoutFail($id);

        if (empty($visitTaskAreaDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskAreaDetails.index'));
        }

        return view('visit_task_area_details.edit')->with('visitTaskAreaDetail', $visitTaskAreaDetail);
    }

    /**
     * Update the specified VisitTaskAreaDetail in storage.
     *
     * @param  int              $id
     * @param UpdateVisitTaskAreaDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVisitTaskAreaDetailRequest $request)
    {


        $visitTaskAreaDetail = $this->visitTaskAreaDetailRepository->findWithoutFail($id);

        if (empty($visitTaskAreaDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskAreaDetails.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $visitTaskAreaDetail = $this->visitTaskAreaDetailRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('visitTaskAreaDetails.index'));
    }

    /**
     * Remove the specified VisitTaskAreaDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->visitTaskAreaDetailRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('visitTaskAreaDetails.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('visitTaskAreaDetails.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->visitTaskAreaDetailRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = VisitTaskAreaDetail::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (VisitTaskAreaDetail $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('visitTaskAreaDetails.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('visitTaskAreaDetails.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(VisitTaskAreaDetail $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (VisitTaskAreaDetail $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
