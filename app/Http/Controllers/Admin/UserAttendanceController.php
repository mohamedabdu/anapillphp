<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserAttendanceRequest;
use App\Http\Requests\UpdateUserAttendanceRequest;
use App\Repositories\UserAttendanceRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\UserAttendance;
use function Helper\Common\imageUrl;

class UserAttendanceController extends AppBaseController
{
    /** @var  UserAttendanceRepository */
    private $userAttendanceRepository;

    public function __construct(UserAttendanceRepository $userAttendanceRepo)
    {
        parent::__construct();
        $this->userAttendanceRepository = $userAttendanceRepo;
    }







    /**
     * Display a listing of the UserAttendance.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->userAttendanceRepository->pushCriteria(new RequestCriteria($request));
        $userAttendances = $this->userAttendanceRepository->all();
        */

        return view('user_attendances.index');
        /*return view('user_attendances.index')
             ->with('userAttendances', $userAttendances);*/
    }



    /**
     * Show the form for creating a new UserAttendance.
     *
     * @return Response
     */
    public function create()
    {
        return view('user_attendances.create');
    }

    /**
     * Store a newly created UserAttendance in storage.
     *
     * @param CreateUserAttendanceRequest $request
     *
     * @return Response
     */
    public function store(CreateUserAttendanceRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $userAttendance = $this->userAttendanceRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('userAttendances.index'));
    }

    /**
     * Display the specified UserAttendance.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $userAttendance = $this->userAttendanceRepository->findWithoutFail($id);

        if (empty($userAttendance)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('userAttendances.index'));
        }

        return view('user_attendances.show')->with('userAttendance', $userAttendance);
    }

    /**
     * Show the form for editing the specified UserAttendance.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $userAttendance = $this->userAttendanceRepository->findWithoutFail($id);

        if (empty($userAttendance)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('userAttendances.index'));
        }

        return view('user_attendances.edit')->with('userAttendance', $userAttendance);
    }

    /**
     * Update the specified UserAttendance in storage.
     *
     * @param  int              $id
     * @param UpdateUserAttendanceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserAttendanceRequest $request)
    {


        $userAttendance = $this->userAttendanceRepository->findWithoutFail($id);

        if (empty($userAttendance)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('userAttendances.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $userAttendance = $this->userAttendanceRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('userAttendances.index'));
    }

    /**
     * Remove the specified UserAttendance from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->userAttendanceRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('userAttendances.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('userAttendances.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->userAttendanceRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = UserAttendance::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (UserAttendance $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('userAttendances.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('userAttendances.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(UserAttendance $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (UserAttendance $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
