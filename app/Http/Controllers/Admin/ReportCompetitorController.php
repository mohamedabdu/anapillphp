<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReportCompetitorRequest;
use App\Http\Requests\UpdateReportCompetitorRequest;
use App\Repositories\ReportCompetitorRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\ReportCompetitor;
use function Helper\Common\imageUrl;

class ReportCompetitorController extends AppBaseController
{
    /** @var  ReportCompetitorRepository */
    private $reportCompetitorRepository;

    public function __construct(ReportCompetitorRepository $reportCompetitorRepo)
    {
        parent::__construct();
        $this->reportCompetitorRepository = $reportCompetitorRepo;
    }







    /**
     * Display a listing of the ReportCompetitor.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->reportCompetitorRepository->pushCriteria(new RequestCriteria($request));
        $reportCompetitors = $this->reportCompetitorRepository->all();
        */

        return view('report_competitors.index');
        /*return view('report_competitors.index')
             ->with('reportCompetitors', $reportCompetitors);*/
    }



    /**
     * Show the form for creating a new ReportCompetitor.
     *
     * @return Response
     */
    public function create()
    {
        return view('report_competitors.create');
    }

    /**
     * Store a newly created ReportCompetitor in storage.
     *
     * @param CreateReportCompetitorRequest $request
     *
     * @return Response
     */
    public function store(CreateReportCompetitorRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $reportCompetitor = $this->reportCompetitorRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('reportCompetitors.index'));
    }

    /**
     * Display the specified ReportCompetitor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $reportCompetitor = $this->reportCompetitorRepository->findWithoutFail($id);

        if (empty($reportCompetitor)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('reportCompetitors.index'));
        }

        return view('report_competitors.show')->with('reportCompetitor', $reportCompetitor);
    }

    /**
     * Show the form for editing the specified ReportCompetitor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $reportCompetitor = $this->reportCompetitorRepository->findWithoutFail($id);

        if (empty($reportCompetitor)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('reportCompetitors.index'));
        }

        return view('report_competitors.edit')->with('reportCompetitor', $reportCompetitor);
    }

    /**
     * Update the specified ReportCompetitor in storage.
     *
     * @param  int              $id
     * @param UpdateReportCompetitorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReportCompetitorRequest $request)
    {


        $reportCompetitor = $this->reportCompetitorRepository->findWithoutFail($id);

        if (empty($reportCompetitor)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('reportCompetitors.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $reportCompetitor = $this->reportCompetitorRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('reportCompetitors.index'));
    }

    /**
     * Remove the specified ReportCompetitor from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->reportCompetitorRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('reportCompetitors.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('reportCompetitors.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->reportCompetitorRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = ReportCompetitor::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (ReportCompetitor $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('reportCompetitors.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('reportCompetitors.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(ReportCompetitor $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (ReportCompetitor $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
