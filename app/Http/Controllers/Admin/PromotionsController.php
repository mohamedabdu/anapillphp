<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BackendController;
use App\Models\Promotion;
use App\Models\PromotionTranslation;
use Illuminate\Http\Request;
use DB;
use Validator;

class PromotionsController extends BackendController
{
    private $rules = [
        "image" => 'required|image|mimes:gif,png,jpeg'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->middleware('role:promotions');
        $this->data['tab'] = 'promotions';
    }
  
    public function index(Request $request)
    {
        return $this->_view("promotions.index");
    }

   
    public function create(Request $request)
    {
        return $this->_view('promotions.create');
    }

   
    public function store(Request $request)
    {
        try {
            $columns_arr = array(
                'title' => 'required'
            );
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);

            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            
            DB::beginTransaction();
            
            $promotion = new Promotion();
            $promotion->created_by = $this->user->id;
            $promotion->image = Promotion::upload($request->file('image'), 'promotions');
            $promotion->save();

            $this->createPromotionTranslations($promotion->id, $request);
            DB::commit();
            return _json('success',_lang('app.added_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error',_lang('app.error_is_occured'),400);
        }
    }

    public function edit($id)
    {
        try 
        {
            $id = en_de_crypt($id, false);
            $promotion = Promotion::where('id',$id);
            $promotion = $this->checkCreatedBy($promotion,'promotions')
                                ->first();
            if (!$promotion) {
                session()->flash('error_message', _lang('app.this_item_doesn\'t_exist'));
                return redirect(route('promotions.index'));
            }
            $this->data['translations'] = PromotionTranslation::where('promotion_id', $id)->get()->keyBy('locale');
            $this->data["promotion"] = $promotion;
            return $this->_view('promotions.edit'); 
        } 
        catch (\Exception $ex) {
            session()->flash('error_message', _lang('app.error_is_occured'));
            return redirect(route('promotions.index'));
        }
       
    }

    
    public function update(Request $request, $id)
    {
        try {
            $columns_arr = array(
                'title' => 'required'
            );
            $this->rules['image'] = 'image|mimes:gif,png,jpeg';
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);
            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            $id = en_de_crypt($id, false);
            $promotion = Promotion::where('id',$id);
            $promotion = $this->checkCreatedBy($promotion,'promotions')
                                ->first();
            if (!$promotion) {
                return _json('error', _lang('app.not_found'), 404);
            }
            DB::beginTransaction();
            if ($request->file('image')) {
                if ($promotion->image != 'default.png') {
                    Promotion::deleteUploaded('promotions', $promotion->image);
                }
                $promotion->image = Promotion::upload($request->file('image'), 'promotions');
                $promotion->save();
            }
            PromotionTranslation::where('promotion_id', $promotion->id)->delete();
            $this->createPromotionTranslations($promotion->id, $request);
            DB::commit();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }

    
    public function destroy($id)
    {
        $id = en_de_crypt($id, false);
        $promotion = Promotion::where('id',$id);
        $promotion = $this->checkCreatedBy($promotion,'promotions')
                                ->first();
        if (!$promotion) {
            return _json('error', _lang('app.error_is_occured'), 404);
        }
        DB::beginTransaction();
        try {
            $promotion->delete();
            DB::commit();
            return _json('success', _lang('app.deleted_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            if ($ex->getCode() == 23000) {
                return _json('error', _lang('app.this_record_can_not_be_deleted_for_linking_to_other_records'), 400);
            } else {

                return _json('error', _lang('app.error_is_occured'), 400);
            }
        }

    }

    public function data(Request $request) {
       
        $promotions = Promotion::join('promotion_translations', function($query){
                                $query->on('promotion_translations.promotion_id', '=', 'promotions.id')
                                ->where('promotion_translations.locale',$this->lang_code);
                            });
        $promotions = $this->checkCreatedBy($promotions,'promotions')
                                ->select('promotions.*', 'promotion_translations.title');
            

            return \DataTables::eloquent($promotions)
                ->addColumn('options', function ($item) {
                    $back = "";
                    $back .= '<div class="btn-group">';
                    $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '. _lang("app.options");
                    $back .= '<i class="fa fa-angle-down"></i>';
                    $back .= '</button>';
                    $back .= '<ul class = "dropdown-menu" promotion = "menu">';
                    $back .= '<li>';
                    $back .= '<a href="' . route('promotions.edit', en_de_crypt($item->id) ) . '">';
                    $back .= '<i class = "fas fa-edit"></i>' . _lang('app.edit');
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '<li>';
                    $back .= '<a href="" data-toggle="confirmation" onclick = "Promotions.delete(this);return false;" data-id = "' . en_de_crypt($item->id) . '">';
                    $back .= '<i class = "fas fa-trash-alt"></i>' . _lang("app.delete");
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '</ul>';
                    $back .= ' </div>';

                    return $back;
                })
                ->escapeColumns([])
                ->make(true);
    }

    private function createPromotionTranslations($promotion_id, $request)
    {
        $promotion_translations = array();
        $title = $request->input('title');
        foreach ($title as $key => $value) {
            $promotion_translations[] = array(
                'locale' => $key,
                'title' => $value,
                'promotion_id' => $promotion_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
        }
        PromotionTranslation::insert($promotion_translations);
    }

   

    


}
