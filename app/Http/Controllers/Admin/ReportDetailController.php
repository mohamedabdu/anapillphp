<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReportDetailRequest;
use App\Http\Requests\UpdateReportDetailRequest;
use App\Repositories\ReportDetailRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\ReportDetail;
use function Helper\Common\imageUrl;

class ReportDetailController extends AppBaseController
{
    /** @var  ReportDetailRepository */
    private $reportDetailRepository;

    public function __construct(ReportDetailRepository $reportDetailRepo)
    {
        parent::__construct();
        $this->reportDetailRepository = $reportDetailRepo;
    }







    /**
     * Display a listing of the ReportDetail.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->reportDetailRepository->pushCriteria(new RequestCriteria($request));
        $reportDetails = $this->reportDetailRepository->all();
        */

        return view('report_details.index');
        /*return view('report_details.index')
             ->with('reportDetails', $reportDetails);*/
    }



    /**
     * Show the form for creating a new ReportDetail.
     *
     * @return Response
     */
    public function create()
    {
        return view('report_details.create');
    }

    /**
     * Store a newly created ReportDetail in storage.
     *
     * @param CreateReportDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateReportDetailRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $reportDetail = $this->reportDetailRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('reportDetails.index'));
    }

    /**
     * Display the specified ReportDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $reportDetail = $this->reportDetailRepository->findWithoutFail($id);

        if (empty($reportDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('reportDetails.index'));
        }

        return view('report_details.show')->with('reportDetail', $reportDetail);
    }

    /**
     * Show the form for editing the specified ReportDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $reportDetail = $this->reportDetailRepository->findWithoutFail($id);

        if (empty($reportDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('reportDetails.index'));
        }

        return view('report_details.edit')->with('reportDetail', $reportDetail);
    }

    /**
     * Update the specified ReportDetail in storage.
     *
     * @param  int              $id
     * @param UpdateReportDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReportDetailRequest $request)
    {


        $reportDetail = $this->reportDetailRepository->findWithoutFail($id);

        if (empty($reportDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('reportDetails.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $reportDetail = $this->reportDetailRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('reportDetails.index'));
    }

    /**
     * Remove the specified ReportDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->reportDetailRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('reportDetails.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('reportDetails.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->reportDetailRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = ReportDetail::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (ReportDetail $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('reportDetails.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('reportDetails.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(ReportDetail $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (ReportDetail $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
