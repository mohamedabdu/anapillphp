<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePromoDetailRequest;
use App\Http\Requests\UpdatePromoDetailRequest;
use App\Models\Promo;
use App\Models\Store;
use App\Repositories\PromoDetailRepository;
use App\Scopes\CompanyScope;
use Illuminate\Database\Eloquent\Model;
use function Helper\Common\upload;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\PromoDetail;
use function Helper\Common\imageUrl;

class PromoDetailController extends AppBaseController
{
    /** @var  PromoDetailRepository */
    private $promoDetailRepository;

    public function __construct(PromoDetailRepository $promoDetailRepo)
    {
        parent::__construct();
        $this->promoDetailRepository = $promoDetailRepo;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
    /*
        $this->promoDetailRepository->pushCriteria(new RequestCriteria($request));
        $promoDetails = $this->promoDetailRepository->all();
        */

        return $this->_view('promo_details.index');
        /*return view('promo_details.index')
             ->with('promoDetails', $promoDetails);*/
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $promos = new Promo();
        /** @var Promo $promos */
        $promos = parent::checkCompany($promos);
        $promos = $promos->get();

        $stores = new Store;
        /** @var Store $stores */
        $stores = parent::checkCompany($stores);
        $stores = $stores->where("is_main",1)->get();

        $this->data["promos"] = $promos;
        $this->data["stores"] = $stores;
        return $this->_view('promo_details.create');
    }

    /**
     * @param CreatePromoDetailRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CreatePromoDetailRequest $request)
    {

        $input = $request->request->all();

        /** create images */
        $images = [];
        if($request->hasFile("contract_images")) {
            foreach ($request->file("contract_images") as $item) {
                $image = upload($item, false)["img"];
               $images[] = $image;
            }
        }
        $input["contract_images"] = json_encode($images);
        /** end */
        $promoDetail = $this->promoDetailRepository->create($input);

        $data['message'] ='تم حفظ البيانات بنجاح';
        $data['success'] = true;
        $data["type"] = "success";
        return $data;
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $promoDetail = $this->promoDetailRepository->findWithoutFail($id);

        if (empty($promoDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('promoDetails.index'));
        }

        return view('promo_details.show')->with('promoDetail', $promoDetail);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $promoDetail = $this->promoDetailRepository->findWithoutFail($id);

        if (empty($promoDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('promoDetails.index'));
        }
        $promos = new Promo();
        /** @var Promo $promos */
        $promos = parent::checkCompany($promos);
        $promos = $promos->get();

        $stores = new Store;
        /** @var Store $stores */
        $stores = parent::checkCompany($stores);
        $stores = $stores->where("is_main",1)->get();

        try {
            $promoDetail->contract_images = json_decode($promoDetail->contract_images);
        } catch (\Exception $e) {

        }
        $this->data["promos"] = $promos;
        $this->data["stores"] = $stores;
        $this->data["promoDetail"] = $promoDetail;
        return $this->_view('promo_details.edit');
    }

    /**
     * Update the specified PromoDetail in storage.
     *
     * @param  int              $id
     * @param UpdatePromoDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePromoDetailRequest $request)
    {


        $promoDetail = $this->promoDetailRepository->findWithoutFail($id);

        if (empty($promoDetail)) {
            $data['message'] ='لم يتم التعديل';
            $data['success'] = false;
            $data["type"] = "false";
            $data["status_code"] = 400;
            return $this->respondBadRequest($data["message"]);
        }


        $input = $request->request->all();


        /** create images */
        $images = [];
        if($request->hasFile("contract_images")) {
            foreach ($request->file("contract_images") as $item) {
                $image = upload($item, false)["img"];
                $images[] = $image;
            }
        }
        $input["contract_images"] = json_encode($images);
        /** end */

        $promoDetail = $this->promoDetailRepository->update($input, $id);

        $data["data"]['message'] ='تم تعديل البيانات بنجاح';
        $data['success'] = true;
        $data["type"] = "success";
        return $data;
    }

    /**
     * Remove the specified PromoDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->promoDetailRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('promoDetails.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('promoDetails.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->promoDetailRepository->delete($id);
            }
        } else if($id != null) {
            $this->promoDetailRepository->delete($id);
        } else  {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            $data['type'] = "false";
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        $data['type'] = "success";
        return $data;

    }

    public function checkCompany($model)
    {
        if (auth()->user() != null) {
            $user = auth()->user();
            if ($user->type == CompanyScope::$typeOfCompany) {
                $column = "promos".".created_by";
                $model = $model->where($column, $user->id);
            }
        }
        return $model;
    }

    public function data() {
            $items = new PromoDetail();
            $items = $this->checkCompany($items);
            $items = $items->join("stores","stores.id","=","promo_details.store_id");
            $items = $items->join("promos","promos.id","=","promo_details.promo_id");
            $items = $items->orderByDesc("promo_details.id")->select(["promo_details.*","promos.title_ar as promo","stores.title_ar as store"]);
            return DataTables::eloquent($items)
                ->addColumn('options', function (PromoDetail $item) {
                    $back = "";
                    $back .= '<div class="btn-group">';
                    $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '. _lang("app.options");
                    $back .= '<i class="fa fa-angle-down"></i>';
                    $back .= '</button>';
                    $back .= '<ul class = "dropdown-menu" role = "menu">';
                    $back .= '<li>';
                    $back .= '<a href="' . route('promoDetails.edit', $item->id) . '">';
                    $back .= '<i class = "icon-docs"></i>' . _lang('app.edit');
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '<li>';
                    $back .= '<a href="" data-toggle="confirmation" onclick = "PromoDetail.delete(this);return false;" data-id = "' . $item->id . '">';
                    $back .= '<i class = "icon-docs"></i>' . _lang("app.delete");
                    $back .= '</a>';
                    $back .= '</li>';

                    $back .= '</ul>';
                    $back .= ' </div>';

                    return $back;


                })
                /*
                 ->addColumn("active",function(PromoDetail $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (PromoDetail $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
