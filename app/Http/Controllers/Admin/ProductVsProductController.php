<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductVsProductRequest;
use App\Http\Requests\UpdateProductVsProductRequest;
use App\Models\Product;
use App\Models\ProductCompetitor;
use App\Repositories\ProductVsProductRepository;
use App\Scopes\CompanyScope;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\ProductVsProduct;
use function Helper\Common\imageUrl;

class ProductVsProductController extends AppBaseController
{
    /** @var  ProductVsProductRepository */
    private $productVsProductRepository;

    public function __construct(ProductVsProductRepository $productVsProductRepo)
    {
        parent::__construct();
        $this->productVsProductRepository = $productVsProductRepo;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
    /*
        $this->productVsProductRepository->pushCriteria(new RequestCriteria($request));
        $productVsProducts = $this->productVsProductRepository->all();
        */

        return $this->_view('product_vs_products.index');
        /*return view('product_vs_products.index')
             ->with('productVsProducts', $productVsProducts);*/
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $normalProduct = new Product;
        $normalProduct = parent::checkCompany($normalProduct);
        $normalProduct = $normalProduct->whereNull("competitor_id")->get();

        $compProduct = new Product;
        $compProduct = parent::checkCompany($compProduct);
        $compProduct = $compProduct->where("competitor_id",">",0)->get();

        $this->data["normalProduct"] = $normalProduct;
        $this->data["compProduct"] = $compProduct;
        return $this->_view('product_vs_products.create');
    }

    /**
     * @param CreateProductVsProductRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CreateProductVsProductRequest $request)
    {
        $input = $request->request->all();

        if(isset($input["product_vs_id"])) {
            $checker = ProductVsProduct::where("product_id",$input["product_id"])
                ->whereIn("product_vs_id",$input["product_vs_id"])->first();
            if($checker != null) {
                $product = Product::find($checker->product_vs_id);
                $title = "";
                if($product != null) {
                    $title = $product->title_ar;
                }
                $titleAR = "تم اضافة هذا المنتج"." ".$title." المنافس من قبل";
                $data['message'] = $titleAR;
                $data['success'] = false;
                $data["type"] = "false";
                $data["status_code"] = 400;
                return $this->respondBadRequest($data["message"]);
            }

            $input['image'] = $this->uploadFile($request,"image",true);

            foreach ($input["product_vs_id"] as $product) {
                $model = new ProductVsProduct();
                $model->product_id = $input["product_id"];
                $model->product_vs_id = $product;
                $model->save();
            }
            //$productVsProduct = $this->productVsProductRepository->create($input);

            $data['message'] ='تم حفظ البيانات بنجاح';
            $data['success'] = true;
            $data["type"] = "success";
            return $data;
        } else {
            $data['message'] ='لم حفظ البيانات بنجاح';
            $data['success'] = false;
            $data["type"] = "false";
            return $data;
        }

    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $productVsProduct = $this->productVsProductRepository->findWithoutFail($id);

        if (empty($productVsProduct)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('productVsProducts.index'));
        }

        return view('product_vs_products.show')->with('productVsProduct', $productVsProduct);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $productVsProduct = $this->productVsProductRepository->findWithoutFail($id);

        if (empty($productVsProduct)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('productVsProducts.index'));
        }
        $normalProduct = new Product;
        $normalProduct = parent::checkCompany($normalProduct);
        $normalProduct = $normalProduct->whereNull("competitor_id")->get();

        $compProduct = new Product;
        $compProduct = parent::checkCompany($compProduct);
        $compProduct = $compProduct->where("competitor_id",">",0)->get();

        $this->data["normalProduct"] = $normalProduct;
        $this->data["compProduct"] = $compProduct;
        $this->data["productVsProduct"] = $productVsProduct;
        return $this->_view('product_vs_products.edit');
    }

    /**
     * @param $id
     * @param UpdateProductVsProductRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update($id, UpdateProductVsProductRequest $request)
    {


        $productVsProduct = $this->productVsProductRepository->findWithoutFail($id);

        if (empty($productVsProduct)) {
            $data['message'] ='لم يتم التعديل';
            $data['success'] = false;
            $data["type"] = "false";
            $data["status_code"] = 400;
            return $this->respondBadRequest($data["message"]);
        }


        $input = $request->request->all();

        $checker = ProductVsProduct::where("id","<>",$id)->where("product_id",$input["product_id"])
            ->where("product_vs_id",$input["product_vs_id"])->first();
        if($checker != null) {
            $data['message'] ='تم اضافة هذا المنتج المنافس من قبل';
            $data['success'] = false;
            $data["type"] = "false";
            $data["status_code"] = 400;
            return $this->respondBadRequest($data["message"]);
        }


        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $productVsProduct = $this->productVsProductRepository->update($input, $id);

        $data["data"]['message'] ='تم تعديل البيانات بنجاح';
        $data['success'] = true;
        $data["type"] = "success";
        return $data;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function destroy($id, Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->productVsProductRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('productVsProducts.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('productVsProducts.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->productVsProductRepository->delete($id);
            }
        } else if ($id != null) {
            $this->productVsProductRepository->delete($id);

        } else {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            $data['type'] = "false";
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        $data['type'] = "success";
        return $data;

    }

    public function checkCompany($model)
    {
        if (auth()->user() != null) {
            $user = auth()->user();
            if ($user->type == CompanyScope::$typeOfCompany) {
                $column = "products".".created_by";
                $model = $model->where($column, $user->id);
            }
        }
        return $model;
    }
    public function data() {
        $items = new ProductVsProduct();
        $items = $this->checkCompany($items);
        $items = $items->join("products","products.id","=","product_vs_products.product_id");
        $items = $items->orderByDesc("product_vs_products.id")->select(["product_vs_products.*","products.title_ar as product"]);
            return DataTables::eloquent($items)
                ->addColumn('options', function (ProductVsProduct $item) {

                    $back = "";
                    $back .= '<div class="btn-group">';
                    $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '. _lang("app.options");
                    $back .= '<i class="fa fa-angle-down"></i>';
                    $back .= '</button>';
                    $back .= '<ul class = "dropdown-menu" role = "menu">';
                    $back .= '<li>';
                    $back .= '<a href="' . route('productVsProducts.edit', $item->id) . '">';
                    $back .= '<i class = "icon-docs"></i>' . _lang('app.edit');
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '<li>';
                    $back .= '<a href="" data-toggle="confirmation" onclick = "ProductVsProduct.delete(this);return false;" data-id = "' . $item->id . '">';
                    $back .= '<i class = "icon-docs"></i>' . _lang("app.delete");
                    $back .= '</a>';
                    $back .= '</li>';

                    $back .= '</ul>';
                    $back .= ' </div>';

                    return $back;

                })
                /*
                 ->addColumn("active",function(ProductVsProduct $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (ProductVsProduct $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn('product_vs', function (ProductVsProduct $item) {
                    $product = $item->productVS;
                    if($product != null) {
                        return $product->title_ar;
                    } else {
                        return "";
                    }
                })
                ->editColumn("id",function($item){
                    return "";
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
