<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVisitDetailRequest;
use App\Http\Requests\UpdateVisitDetailRequest;
use App\Repositories\VisitDetailRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\VisitDetail;
use function Helper\Common\imageUrl;

class VisitDetailController extends AppBaseController
{
    /** @var  VisitDetailRepository */
    private $visitDetailRepository;

    public function __construct(VisitDetailRepository $visitDetailRepo)
    {
        parent::__construct();
        $this->visitDetailRepository = $visitDetailRepo;
    }







    /**
     * Display a listing of the VisitDetail.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->visitDetailRepository->pushCriteria(new RequestCriteria($request));
        $visitDetails = $this->visitDetailRepository->all();
        */

        return view('visit_details.index');
        /*return view('visit_details.index')
             ->with('visitDetails', $visitDetails);*/
    }



    /**
     * Show the form for creating a new VisitDetail.
     *
     * @return Response
     */
    public function create()
    {
        return view('visit_details.create');
    }

    /**
     * Store a newly created VisitDetail in storage.
     *
     * @param CreateVisitDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateVisitDetailRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $visitDetail = $this->visitDetailRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('visitDetails.index'));
    }

    /**
     * Display the specified VisitDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visitDetail = $this->visitDetailRepository->findWithoutFail($id);

        if (empty($visitDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitDetails.index'));
        }

        return view('visit_details.show')->with('visitDetail', $visitDetail);
    }

    /**
     * Show the form for editing the specified VisitDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visitDetail = $this->visitDetailRepository->findWithoutFail($id);

        if (empty($visitDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitDetails.index'));
        }

        return view('visit_details.edit')->with('visitDetail', $visitDetail);
    }

    /**
     * Update the specified VisitDetail in storage.
     *
     * @param  int              $id
     * @param UpdateVisitDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVisitDetailRequest $request)
    {


        $visitDetail = $this->visitDetailRepository->findWithoutFail($id);

        if (empty($visitDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitDetails.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $visitDetail = $this->visitDetailRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('visitDetails.index'));
    }

    /**
     * Remove the specified VisitDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->visitDetailRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('visitDetails.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('visitDetails.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->visitDetailRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = VisitDetail::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (VisitDetail $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('visitDetails.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('visitDetails.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(VisitDetail $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (VisitDetail $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
