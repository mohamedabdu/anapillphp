<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskInstructionImageRequest;
use App\Http\Requests\UpdateTaskInstructionImageRequest;
use App\Repositories\TaskInstructionImageRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\TaskInstructionImage;
use function Helper\Common\imageUrl;

class TaskInstructionImageController extends AppBaseController
{
    /** @var  TaskInstructionImageRepository */
    private $taskInstructionImageRepository;

    public function __construct(TaskInstructionImageRepository $taskInstructionImageRepo)
    {
        parent::__construct();
        $this->taskInstructionImageRepository = $taskInstructionImageRepo;
    }







    /**
     * Display a listing of the TaskInstructionImage.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->taskInstructionImageRepository->pushCriteria(new RequestCriteria($request));
        $taskInstructionImages = $this->taskInstructionImageRepository->all();
        */

        return view('task_instruction_images.index');
        /*return view('task_instruction_images.index')
             ->with('taskInstructionImages', $taskInstructionImages);*/
    }



    /**
     * Show the form for creating a new TaskInstructionImage.
     *
     * @return Response
     */
    public function create()
    {
        return view('task_instruction_images.create');
    }

    /**
     * Store a newly created TaskInstructionImage in storage.
     *
     * @param CreateTaskInstructionImageRequest $request
     *
     * @return Response
     */
    public function store(CreateTaskInstructionImageRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $taskInstructionImage = $this->taskInstructionImageRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('taskInstructionImages.index'));
    }

    /**
     * Display the specified TaskInstructionImage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $taskInstructionImage = $this->taskInstructionImageRepository->findWithoutFail($id);

        if (empty($taskInstructionImage)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('taskInstructionImages.index'));
        }

        return view('task_instruction_images.show')->with('taskInstructionImage', $taskInstructionImage);
    }

    /**
     * Show the form for editing the specified TaskInstructionImage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $taskInstructionImage = $this->taskInstructionImageRepository->findWithoutFail($id);

        if (empty($taskInstructionImage)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('taskInstructionImages.index'));
        }

        return view('task_instruction_images.edit')->with('taskInstructionImage', $taskInstructionImage);
    }

    /**
     * Update the specified TaskInstructionImage in storage.
     *
     * @param  int              $id
     * @param UpdateTaskInstructionImageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTaskInstructionImageRequest $request)
    {


        $taskInstructionImage = $this->taskInstructionImageRepository->findWithoutFail($id);

        if (empty($taskInstructionImage)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('taskInstructionImages.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $taskInstructionImage = $this->taskInstructionImageRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('taskInstructionImages.index'));
    }

    /**
     * Remove the specified TaskInstructionImage from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->taskInstructionImageRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('taskInstructionImages.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('taskInstructionImages.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->taskInstructionImageRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = TaskInstructionImage::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (TaskInstructionImage $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('taskInstructionImages.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('taskInstructionImages.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(TaskInstructionImage $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (TaskInstructionImage $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
