<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;

class CompaniesController extends BackendController
{
   private $rules = [
        "name" => "required|max:25",
        "email" => "required|email|unique:users",
        "password" => "required",
        "mobile" => "required|numeric|regex:(05)|digits:10",
        "role" => 'required',
        "active" => 'required',
        "image" => 'image|mimes:gif,png,jpeg'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->middleware('role:companies');
        $this->data['tab'] = 'companies';
    }

    public function index(Request $request)
    {
        return $this->_view('companies.index');
    }

    public function create(Request $request) {
        $this->data['roles'] = $this->getCompanyRoles();
        return $this->_view('companies.create');

    }

    public function store(Request $request) {
        try {
            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            
            $admin = new User();
            $admin->name = $request->input('name');
            $admin->email = $request->input('email');
            $admin->password = bcrypt($request->input('password'));
            $admin->mobile = $request->input('mobile');
            $admin->role_id = en_de_crypt($request->input('role'),false);
            $admin->type = User::$types['company'];;
            $admin->image = 'default.png';
            if ($request->file('image')) {
                $admin->image = User::upload($request->file('image'), 'users');
            }
            $admin->created_by = $this->user->id;
            $admin->active = $request->input('active');
            $admin->save();

            return _json('success',_lang('app.added_successfully'));
        } catch (\Exception $ex) {
            return _json('error', _lang('app.error_is_occured'), 400);
        }

    }

    public function edit($id) {
        $id = en_de_crypt($id, false);
        $admin = User::where('id',$id);
        $admin = $this->checkCreatedBy($admin,'users')
                        ->first();
        if(!$admin) {
            session()->flash('error_message', _lang('app.this_item_doesn\'t_exist'));
            return redirect(route("companies.index"));
        }
        $this->data['roles'] = $this->getCompanyRoles();
        $this->data["admin"] = $admin;
        return $this->_view('companies.edit');

    }
    public function update(Request $request,$id) {
       try {
           $id = en_de_crypt($id, false);
            unset($this->rules['password']);
            $this->rule['email'] = "required|email|unique:users,email," . $id;

            $validator = Validator::make($request->all(), $this->rule);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }

            $admin = User::where('id',$id);
            $admin = $this->checkCreatedBy($admin,'users')
                        ->first();
            if (!$admin) {
                return _json('error', _lang('app.not_found'), 404);
            }

            $admin->name = $request->input('name');
            $admin->email = $request->input('email');
            if ($request->input('password')) {
                $admin->password = bcrypt($request->input('password'));
            }
            $admin->active = $request->input('active');
            $admin->mobile = $request->input('mobile');
            $admin->role_id = en_de_crypt($request->input('role'), false);
            if ($request->file('image')) {
                if ($admin->image != 'default.png') {
                    User::deleteUploaded('users', $admin->image);
                }
                $admin->image = User::upload($request->file('image'), 'users');
            }

            $admin->save();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            return _json('error', _lang('app.error_is_occured'), 400);
        }
        
    }

    public function active($id)
    {
        try{
            $id = en_de_crypt($id, false);
            $admin = User::where('id',$id);
            $admin = $this->checkCreatedBy($admin,'users')
                        ->first();
            if (!$admin) {
                return _json('error', _lang('app.not_found'), 404);
            }
            $admin->active = !$admin->active;
            $admin->save();
            return _json('success', _lang('app.updated_successfully'));
       } catch (\Exception $ex) {
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }


    public function destroy($id)
    {
        $id = en_de_crypt($id, false);
        $admin = User::where('id',$id);
        $admin = $this->checkCreatedBy($admin,'users')
                        ->first();
        if (!$admin) {
            return _json('error', _lang('app.not_found'), 404);
        }
        DB::beginTransaction();
        try {
            $admin->delete();
            DB::commit();
            return _json('success', _lang('app.deleted_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            if ($ex->getCode() == 23000) {
                return _json('error', _lang('app.this_record_can_not_be_deleted_for_linking_to_other_records'), 400);
            } else {
                return _json('error', _lang('app.error_is_occured'), 400);
            }
        }
    }
    
    public function data(Request $request) {
        
        $companies = User::join('roles','roles.id','=','users.role_id')
                        ->join('role_translations', function($query){
                                $query->on('role_translations.role_id', '=', 'roles.id')
                                ->where('role_translations.locale',$this->lang_code);
                        })
                        ->where('users.type',User::$types['company']);
        $companies = $this->checkCreatedBy($companies, 'users')
                        ->select('users.*', 'role_translations.title as role');

        return \DataTables::eloquent($companies)
            ->addColumn('options', function ($item) {
                $back = "";
                $back .= '<div class="btn-group">';
                $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '. _lang("app.options");
                $back .= '<i class="fa fa-angle-down"></i>';
                $back .= '</button>';
                $back .= '<ul class = "dropdown-menu" role = "menu">';
                $back .= '<li>';
                $back .= '<a href="'.route('companies.edit', en_de_crypt($item->id)).'">';
                $back .= '<i class = "icon-docs"></i>' . _lang('app.edit');
                $back .= '</a>';
                $back .= '</li>';
                $back .= '<li>';
                $back .= '<a href="" data-toggle="confirmation" onclick = "Companies.delete(this);return false;" data-id = "'.en_de_crypt($item->id).'">';
                $back .= '<i class = "icon-docs"></i>' . _lang("app.delete");
                $back .= '</a>';
                $back .= '</li>';

                $back .= '</ul>';
                $back .= ' </div>';

                return $back;
            })
            ->editColumn("active",function(User $item){
                if($item->active == true){
                    $class = "btn btn-success";
                    $label = _lang('app.activated');
                }else{
                    $class = "btn btn-danger";
                    $label = _lang('app.deactivated');
                }
                $back = '<button class="'.$class.'" onclick="Companies.changeActive(this); return false;" data-id="'.en_de_crypt($item->id).'">'.$label.'</button>';
                return $back;
            })
            ->editColumn('image', function ($item) {
                $back = ' <img src= "'.url('public/uploads/users/'.$item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                return $back;
            })
            ->escapeColumns([])
            ->make(true);
    }

    private function getCompanyRoles()
    {
        $roles = Role::join('role_translations', function ($query) {
            $query->on('role_translations.role_id', '=', 'roles.id')
                ->where('role_translations.locale', $this->lang_code);
            })
            ->where('roles.is_company',true);
        $roles = $this->checkCreatedBy($roles, 'roles')->select('roles.id', 'role_translations.title')->get();
        return  Role::transformCollection($roles, 'Admin');
    }
}
