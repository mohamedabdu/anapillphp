<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;

class ProfileController extends BackendController
{

    public function index()
    {
        return $this->_view('profile.index', 'backend');
    }

    public function update(Request $request)
    {
        $rules = [
            "name" => "required|max:25",
            "email" => "required|email|unique:users,email," . $this->user->id,
            "mobile" => "required|numeric|regex:(05)|digits:10",
        ];

        $validator = Validator::make($request->all(), $$rules);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return _json('error', $errors);
        }

        $this->user->name = $request->input('name');
        $this->user->email = $request->input('email');
        $this->user->mobile = $request->input('phone');
        if ($request->input('password') !== null) {
            $this->user->password = bcrypt($request->input('password'));
        }
        try {
            $this->user->save();
            return _json('success', _lang('app.updated_successfully'));
        } catch (Exception $ex) {
            return _json('error', _lang('app.error_is_occured'));
        }
    }
}
