<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReportDetailImageRequest;
use App\Http\Requests\UpdateReportDetailImageRequest;
use App\Repositories\ReportDetailImageRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\ReportDetailImage;
use function Helper\Common\imageUrl;

class ReportDetailImageController extends AppBaseController
{
    /** @var  ReportDetailImageRepository */
    private $reportDetailImageRepository;

    public function __construct(ReportDetailImageRepository $reportDetailImageRepo)
    {
        parent::__construct();
        $this->reportDetailImageRepository = $reportDetailImageRepo;
    }







    /**
     * Display a listing of the ReportDetailImage.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->reportDetailImageRepository->pushCriteria(new RequestCriteria($request));
        $reportDetailImages = $this->reportDetailImageRepository->all();
        */

        return view('report_detail_images.index');
        /*return view('report_detail_images.index')
             ->with('reportDetailImages', $reportDetailImages);*/
    }



    /**
     * Show the form for creating a new ReportDetailImage.
     *
     * @return Response
     */
    public function create()
    {
        return view('report_detail_images.create');
    }

    /**
     * Store a newly created ReportDetailImage in storage.
     *
     * @param CreateReportDetailImageRequest $request
     *
     * @return Response
     */
    public function store(CreateReportDetailImageRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $reportDetailImage = $this->reportDetailImageRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('reportDetailImages.index'));
    }

    /**
     * Display the specified ReportDetailImage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $reportDetailImage = $this->reportDetailImageRepository->findWithoutFail($id);

        if (empty($reportDetailImage)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('reportDetailImages.index'));
        }

        return view('report_detail_images.show')->with('reportDetailImage', $reportDetailImage);
    }

    /**
     * Show the form for editing the specified ReportDetailImage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $reportDetailImage = $this->reportDetailImageRepository->findWithoutFail($id);

        if (empty($reportDetailImage)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('reportDetailImages.index'));
        }

        return view('report_detail_images.edit')->with('reportDetailImage', $reportDetailImage);
    }

    /**
     * Update the specified ReportDetailImage in storage.
     *
     * @param  int              $id
     * @param UpdateReportDetailImageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReportDetailImageRequest $request)
    {


        $reportDetailImage = $this->reportDetailImageRepository->findWithoutFail($id);

        if (empty($reportDetailImage)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('reportDetailImages.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $reportDetailImage = $this->reportDetailImageRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('reportDetailImages.index'));
    }

    /**
     * Remove the specified ReportDetailImage from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->reportDetailImageRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('reportDetailImages.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('reportDetailImages.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->reportDetailImageRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = ReportDetailImage::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (ReportDetailImage $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('reportDetailImages.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('reportDetailImages.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(ReportDetailImage $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (ReportDetailImage $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
