<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVisitTaskInventoryDetailRequest;
use App\Http\Requests\UpdateVisitTaskInventoryDetailRequest;
use App\Repositories\VisitTaskInventoryDetailRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\VisitTaskInventoryDetail;
use function Helper\Common\imageUrl;

class VisitTaskInventoryDetailController extends AppBaseController
{
    /** @var  VisitTaskInventoryDetailRepository */
    private $visitTaskInventoryDetailRepository;

    public function __construct(VisitTaskInventoryDetailRepository $visitTaskInventoryDetailRepo)
    {
        parent::__construct();
        $this->visitTaskInventoryDetailRepository = $visitTaskInventoryDetailRepo;
    }







    /**
     * Display a listing of the VisitTaskInventoryDetail.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->visitTaskInventoryDetailRepository->pushCriteria(new RequestCriteria($request));
        $visitTaskInventoryDetails = $this->visitTaskInventoryDetailRepository->all();
        */

        return view('visit_task_inventory_details.index');
        /*return view('visit_task_inventory_details.index')
             ->with('visitTaskInventoryDetails', $visitTaskInventoryDetails);*/
    }



    /**
     * Show the form for creating a new VisitTaskInventoryDetail.
     *
     * @return Response
     */
    public function create()
    {
        return view('visit_task_inventory_details.create');
    }

    /**
     * Store a newly created VisitTaskInventoryDetail in storage.
     *
     * @param CreateVisitTaskInventoryDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateVisitTaskInventoryDetailRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $visitTaskInventoryDetail = $this->visitTaskInventoryDetailRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('visitTaskInventoryDetails.index'));
    }

    /**
     * Display the specified VisitTaskInventoryDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visitTaskInventoryDetail = $this->visitTaskInventoryDetailRepository->findWithoutFail($id);

        if (empty($visitTaskInventoryDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskInventoryDetails.index'));
        }

        return view('visit_task_inventory_details.show')->with('visitTaskInventoryDetail', $visitTaskInventoryDetail);
    }

    /**
     * Show the form for editing the specified VisitTaskInventoryDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visitTaskInventoryDetail = $this->visitTaskInventoryDetailRepository->findWithoutFail($id);

        if (empty($visitTaskInventoryDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskInventoryDetails.index'));
        }

        return view('visit_task_inventory_details.edit')->with('visitTaskInventoryDetail', $visitTaskInventoryDetail);
    }

    /**
     * Update the specified VisitTaskInventoryDetail in storage.
     *
     * @param  int              $id
     * @param UpdateVisitTaskInventoryDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVisitTaskInventoryDetailRequest $request)
    {


        $visitTaskInventoryDetail = $this->visitTaskInventoryDetailRepository->findWithoutFail($id);

        if (empty($visitTaskInventoryDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskInventoryDetails.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $visitTaskInventoryDetail = $this->visitTaskInventoryDetailRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('visitTaskInventoryDetails.index'));
    }

    /**
     * Remove the specified VisitTaskInventoryDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->visitTaskInventoryDetailRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('visitTaskInventoryDetails.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('visitTaskInventoryDetails.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->visitTaskInventoryDetailRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = VisitTaskInventoryDetail::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (VisitTaskInventoryDetail $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('visitTaskInventoryDetails.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('visitTaskInventoryDetails.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(VisitTaskInventoryDetail $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (VisitTaskInventoryDetail $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
