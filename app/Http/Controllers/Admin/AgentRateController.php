<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAgentRateRequest;
use App\Http\Requests\UpdateAgentRateRequest;
use App\Repositories\AgentRateRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\AgentRate;
use function Helper\Common\imageUrl;

class AgentRateController extends AppBaseController
{
    /** @var  AgentRateRepository */
    private $agentRateRepository;

    public function __construct(AgentRateRepository $agentRateRepo)
    {
        parent::__construct();
        $this->agentRateRepository = $agentRateRepo;
    }







    /**
     * Display a listing of the AgentRate.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->agentRateRepository->pushCriteria(new RequestCriteria($request));
        $agentRates = $this->agentRateRepository->all();
        */

        return view('agent_rates.index');
        /*return view('agent_rates.index')
             ->with('agentRates', $agentRates);*/
    }



    /**
     * Show the form for creating a new AgentRate.
     *
     * @return Response
     */
    public function create()
    {
        return view('agent_rates.create');
    }

    /**
     * Store a newly created AgentRate in storage.
     *
     * @param CreateAgentRateRequest $request
     *
     * @return Response
     */
    public function store(CreateAgentRateRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $agentRate = $this->agentRateRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('agentRates.index'));
    }

    /**
     * Display the specified AgentRate.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $agentRate = $this->agentRateRepository->findWithoutFail($id);

        if (empty($agentRate)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('agentRates.index'));
        }

        return view('agent_rates.show')->with('agentRate', $agentRate);
    }

    /**
     * Show the form for editing the specified AgentRate.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $agentRate = $this->agentRateRepository->findWithoutFail($id);

        if (empty($agentRate)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('agentRates.index'));
        }

        return view('agent_rates.edit')->with('agentRate', $agentRate);
    }

    /**
     * Update the specified AgentRate in storage.
     *
     * @param  int              $id
     * @param UpdateAgentRateRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAgentRateRequest $request)
    {


        $agentRate = $this->agentRateRepository->findWithoutFail($id);

        if (empty($agentRate)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('agentRates.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $agentRate = $this->agentRateRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('agentRates.index'));
    }

    /**
     * Remove the specified AgentRate from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->agentRateRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('agentRates.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('agentRates.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->agentRateRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data(Request $request) {
            $items = AgentRate::where("agent_id",$request->agent_id)->orderByDesc("id")->select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (AgentRate $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('agentRates.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('agentRates.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                    ->editColumn('rate', function (AgentRate $item) {
                        $back = "";
                        for($i = 1; $i < 6; $i ++) {
                            if($i <= $item->rate) {
                                $back .= '<span class="fa fa-star checked"></span>';
                            } else {
                                $back .= '<span class="fa fa-star"></span>';
                            }
                        }
                        return $back;
                    })

                /*
                 *

                 ->addColumn("active",function(AgentRate $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (AgentRate $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
