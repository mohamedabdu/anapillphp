<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSupervisorAgentRequest;
use App\Http\Requests\UpdateSupervisorAgentRequest;
use App\Repositories\SupervisorAgentRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\SupervisorAgent;
use function Helper\Common\imageUrl;

class SupervisorAgentController extends AppBaseController
{
    /** @var  SupervisorAgentRepository */
    private $supervisorAgentRepository;

    public function __construct(SupervisorAgentRepository $supervisorAgentRepo)
    {
        parent::__construct();
        $this->supervisorAgentRepository = $supervisorAgentRepo;
    }







    /**
     * Display a listing of the SupervisorAgent.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->supervisorAgentRepository->pushCriteria(new RequestCriteria($request));
        $supervisorAgents = $this->supervisorAgentRepository->all();
        */

        return view('supervisor_agents.index');
        /*return view('supervisor_agents.index')
             ->with('supervisorAgents', $supervisorAgents);*/
    }



    /**
     * Show the form for creating a new SupervisorAgent.
     *
     * @return Response
     */
    public function create()
    {
        return view('supervisor_agents.create');
    }

    /**
     * Store a newly created SupervisorAgent in storage.
     *
     * @param CreateSupervisorAgentRequest $request
     *
     * @return Response
     */
    public function store(CreateSupervisorAgentRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $supervisorAgent = $this->supervisorAgentRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('supervisorAgents.index'));
    }

    /**
     * Display the specified SupervisorAgent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $supervisorAgent = $this->supervisorAgentRepository->findWithoutFail($id);

        if (empty($supervisorAgent)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('supervisorAgents.index'));
        }

        return view('supervisor_agents.show')->with('supervisorAgent', $supervisorAgent);
    }

    /**
     * Show the form for editing the specified SupervisorAgent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $supervisorAgent = $this->supervisorAgentRepository->findWithoutFail($id);

        if (empty($supervisorAgent)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('supervisorAgents.index'));
        }

        return view('supervisor_agents.edit')->with('supervisorAgent', $supervisorAgent);
    }

    /**
     * Update the specified SupervisorAgent in storage.
     *
     * @param  int              $id
     * @param UpdateSupervisorAgentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSupervisorAgentRequest $request)
    {


        $supervisorAgent = $this->supervisorAgentRepository->findWithoutFail($id);

        if (empty($supervisorAgent)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('supervisorAgents.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $supervisorAgent = $this->supervisorAgentRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('supervisorAgents.index'));
    }

    /**
     * Remove the specified SupervisorAgent from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->supervisorAgentRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('supervisorAgents.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('supervisorAgents.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->supervisorAgentRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = SupervisorAgent::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (SupervisorAgent $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('supervisorAgents.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('supervisorAgents.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(SupervisorAgent $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (SupervisorAgent $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
