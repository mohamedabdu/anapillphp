<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BackendController;
use App\Http\Requests\CreateConfigRequest;
use App\Http\Requests\UpdateConfigRequest;
use App\Repositories\ConfigRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\Config;

class ConfigController extends BackendController
{
    /** @var  ConfigRepository */
    private $configRepository;

    public function __construct(ConfigRepository $configRepo)
    {
        parent::__construct();
        $this->configRepository = $configRepo;
    }







    /**
     * Display a listing of the Config.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->configRepository->pushCriteria(new RequestCriteria($request));
        $configs = $this->configRepository->all();
        */

        return view('configs.index');
        /*return view('configs.index')
             ->with('configs', $configs);*/
    }



    /**
     * Show the form for creating a new Config.
     *
     * @return Response
     */
    public function create()
    {
        return view('configs.create');
    }

    /**
     * Store a newly created Config in storage.
     *
     * @param CreateConfigRequest $request
     *
     * @return Response
     */
    public function store(CreateConfigRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $config = $this->configRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('configs.index'));
    }

    /**
     * Display the specified Config.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $config = $this->configRepository->findWithoutFail($id);

        if (empty($config)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('configs.index'));
        }

        return view('configs.show')->with('config', $config);
    }

    /**
     * Show the form for editing the specified Config.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $config = $this->configRepository->findWithoutFail($id);

        if (empty($config)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('configs.index'));
        }
        try {
            $days = json_decode($config->system_holidays);
        } catch (\Exception $e) {
            $days = [];
        }

        $this->data["days"] = $days;
        $this->data["config"] = $config;
        return $this->_view('configs.edit');
    }

    /**
     * @param $id
     * @param UpdateConfigRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update($id, UpdateConfigRequest $request)
    {


        $config = $this->configRepository->findWithoutFail($id);

        if (empty($config)) {
            $data['message'] ='لم يتم التعديل';
            $data['success'] = false;
            $data["type"] = "false";
            $data["status_code"] = 400;
            return $this->respondBadRequest($data["message"]);
        }


        $input = $request->request->all();
        if(isset($input["days"])) {
            $days = json_encode($input["days"]);
            $input["system_holidays"] = $days;
        } else {
            $days = json_encode([]);
            $input["system_holidays"] = $days;
        }
        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $config = $this->configRepository->update($input, $id);

        $data["data"]['message'] ='تم تعديل البيانات بنجاح';
        $data['success'] = true;
        $data["type"] = "success";
        return $data;
    }

    /**
     * Remove the specified Config from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->configRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('configs.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('configs.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->configRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = Config::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (Config $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('configs.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('configs.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(Config $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (Config $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
