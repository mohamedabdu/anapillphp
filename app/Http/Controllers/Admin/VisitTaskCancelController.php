<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVisitTaskCancelRequest;
use App\Http\Requests\UpdateVisitTaskCancelRequest;
use App\Repositories\VisitTaskCancelRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\VisitTaskCancel;
use function Helper\Common\imageUrl;

class VisitTaskCancelController extends AppBaseController
{
    /** @var  VisitTaskCancelRepository */
    private $visitTaskCancelRepository;

    public function __construct(VisitTaskCancelRepository $visitTaskCancelRepo)
    {
        parent::__construct();
        $this->visitTaskCancelRepository = $visitTaskCancelRepo;
    }







    /**
     * Display a listing of the VisitTaskCancel.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->visitTaskCancelRepository->pushCriteria(new RequestCriteria($request));
        $visitTaskCancels = $this->visitTaskCancelRepository->all();
        */

        return view('visit_task_cancels.index');
        /*return view('visit_task_cancels.index')
             ->with('visitTaskCancels', $visitTaskCancels);*/
    }



    /**
     * Show the form for creating a new VisitTaskCancel.
     *
     * @return Response
     */
    public function create()
    {
        return view('visit_task_cancels.create');
    }

    /**
     * Store a newly created VisitTaskCancel in storage.
     *
     * @param CreateVisitTaskCancelRequest $request
     *
     * @return Response
     */
    public function store(CreateVisitTaskCancelRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $visitTaskCancel = $this->visitTaskCancelRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('visitTaskCancels.index'));
    }

    /**
     * Display the specified VisitTaskCancel.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visitTaskCancel = $this->visitTaskCancelRepository->findWithoutFail($id);

        if (empty($visitTaskCancel)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskCancels.index'));
        }

        return view('visit_task_cancels.show')->with('visitTaskCancel', $visitTaskCancel);
    }

    /**
     * Show the form for editing the specified VisitTaskCancel.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visitTaskCancel = $this->visitTaskCancelRepository->findWithoutFail($id);

        if (empty($visitTaskCancel)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskCancels.index'));
        }

        return view('visit_task_cancels.edit')->with('visitTaskCancel', $visitTaskCancel);
    }

    /**
     * Update the specified VisitTaskCancel in storage.
     *
     * @param  int              $id
     * @param UpdateVisitTaskCancelRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVisitTaskCancelRequest $request)
    {


        $visitTaskCancel = $this->visitTaskCancelRepository->findWithoutFail($id);

        if (empty($visitTaskCancel)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskCancels.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $visitTaskCancel = $this->visitTaskCancelRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('visitTaskCancels.index'));
    }

    /**
     * Remove the specified VisitTaskCancel from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->visitTaskCancelRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('visitTaskCancels.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('visitTaskCancels.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->visitTaskCancelRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = VisitTaskCancel::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (VisitTaskCancel $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('visitTaskCancels.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('visitTaskCancels.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(VisitTaskCancel $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (VisitTaskCancel $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
