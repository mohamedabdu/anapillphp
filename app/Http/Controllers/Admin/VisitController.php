<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVisitRequest;
use App\Http\Requests\CreateVisitScheduleRequest;
use App\Http\Requests\UpdateVisitRequest;
use App\Models\Agent;
use App\Models\Config;
use App\Models\Store;
use App\Models\VisitAgent;
use App\Models\VisitDetail;
use App\Models\VisitStore;
use App\Models\VisitTask;
use App\Repositories\VisitRepository;
use App\Traits\VisitTrait;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\Visit;
use Illuminate\Support\Facades\DB;
use function Helper\Common\imageUrl;
use Validator;

class VisitController extends AppBaseController
{
    use VisitTrait;
    /** @var  VisitRepository */
    private $visitRepository;
    private $errors;
    public function __construct(VisitRepository $visitRepo)
    {
        parent::__construct();
        $this->visitRepository = $visitRepo;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if($request->type == Visit::$types["normal"]) {
            $partialView = "visits.normal.table";
        } else {
            $partialView = "visits.scheduled.table";
        }

        $this->data["view"] = $partialView;
        $this->data["type"] = $request->type;
        return $this->_view('visits.index');

    }
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function todayTasks(Request $request)
    {

        return $this->_view('visits.todayTasks');

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        if($request->type == Visit::$types["normal"]) {
            $partialView = "main_content/backend/visits.normal.fields";

        } else {
            $partialView = "main_content/backend/visits.scheduled.fields";
        }
        $stores = new Store();
        $stores = $this->checkCompany($stores);
        $stores = $stores->get();

        $agents = new Agent();
        $agents = $this->checkCompany($agents);
        $agents = $agents->where("active",1);
        $agents = $agents->get();

        try {
            $config = Config::latest("id")->first();
            $days = json_decode($config->system_holidays);
        } catch (\Exception $e) {
            $days = [];
        }
        $this->data["type"] = $request->type;
        $this->data["view"] = $partialView;
        $this->data["agents"] = $agents;
        $this->data["stores"] = $stores;
        $this->data["days"] = $days;
        return $this->_view('visits.create');


    }


    /**
     * @param CreateVisitRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CreateVisitRequest $request)
    {
        if($request->type == Visit::$types[2]) {
            if($this->createSchedule($request)) {
                $message = "تم حفظ البيانات بنجاح";
            } else {
                $data['message'] = $this->errors;
                $data['success'] = false;
                $data["type"] = "false";
                $data["status_code"] = 400;
                return $this->respondBadRequest($data["message"]);
            }
        } else {
            if($this->createNormal($request)) {
                $message = "تم حفظ البيانات بنجاح";
            } else {
                $data['message'] = $this->errors;
                $data['success'] = false;
                $data["type"] = "false";
                $data["status_code"] = 400;
                return $this->respondBadRequest($data["message"]);

            }
        }
        $data['message'] = $message;
        $data['success'] = true;
        $data["type"] = "success";
        return $data;

        //return redirect(route('visits.index')."?type=".Visit::$types[$request->type]);

    }

    private function initCode() {
        $maxID = Visit::max("id");
        $maxID += 1;
        $id = "0000".$maxID;
        if($maxID > 9999 && $maxID < 99999) {
            $id = "000".$maxID;
        } else if ($maxID > 99999 && $maxID < 999999) {
            $id = "00".$maxID;
        } else if ($maxID > 999999 && $maxID < 9999999) {
            $id = "0".$maxID;
        } else if ($maxID > 9999999) {
            $id = $maxID;
        }
        return $id;
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function createSchedule(Request $request) {
        $validator = Validator::make($request->all(),
            [ "start_date" => "required",
            "stores" => "required",
            "agent" => "required",
            "tasks" => "required",]);

        if ($validator->fails())
        {
            $this->errors = $validator->errors()->toArray()[0];
            return false;
        }

        $checkVisitSchedule = Visit::join("visit_stores","visit_stores.visit_id","=","visits.id")
            ->where("visits.start_date",$request->start_date)
            ->whereIn("visit_stores.store_id",$request->stores)
            ->where("visits.type",Visit::$types["scheduled"])->first();
        if($checkVisitSchedule != null) {
            $this->errors = "عفوا يوجب زيارة اخري بنفس البيانات";
            return false;
        }
        DB::beginTransaction();
        try {
            $input = $request->request->all();
            $input["start_date"] = date("Y-m-d",strtotime($input["start_date"]));
            $input["created_by"] = auth()->id();
            $input["type"] = Visit::$types[$request->type];
            $input["code"] = $this->initCode();
            if($input["num_of_visits"] == null) {
                $input["num_of_visits"] = 0;
            }
            if(isset($input["days"])) {
                $days = json_encode($input["days"]);
                $input["holidays"] = $days;
            }
            $visit = Visit::create($input);


            $visitStoresID = [];
            /** create stores */
            if(isset($input["stores"])) {
                foreach ($input["stores"] as $store) {
                    $model = new VisitStore();
                    $model->visit_id = $visit->id;
                    $model->store_id = $store;
                    $model->status = 0;
                    $model->save();
                    $visitStoresID[] = $model->id;
                }
            }
            /**  */
            /** create tasks */
            if(isset($input["tasks"])) {
                foreach ($input["tasks"] as $task) {
                    $model = new VisitTask();
                    $model->visit_id = $visit->id;
                    $model->task_id = $task;
                    $model->status = 0;
                    $model->save();
                }
            }
            /**  */
            /** create agents */
            if(isset($input["agent"])) {
                $model = new VisitAgent();
                $model->visit_id = $visit->id;
                $model->agent_id = $input["agent"];
                $model->status = 0;
                $model->save();
            }
            /**  */

            /** create first visit detail */
            if(isset($input["agent"])) {
                $agentID = $input["agent"];
                foreach ($visitStoresID as $id) {
                    $visitDetail = new VisitDetail();
                    $visitDetail->visit_id = $visit->id;
                    $visitDetail->visit_store_id = $id;
                    $visitDetail->agent_id = $agentID;
                    $visitDetail->start_date = $visit->start_date;
                    $visitDetail->lat = 0;
                    $visitDetail->lng = 0;
                    $visitDetail->status = Visit::$statuses["PENDING"];
                    $visitDetail->save();
                }
            }
            /**  */
            DB::commit();
            return true;
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return false;
        }


    }

    /**
     * @param Request $request
     * @return bool
     */
    private function createNormal(Request $request) {
        $validator = Validator::make($request->all(),
            [ "start_date" => "required",
                "store" => "required",
                "agents" => "required",
                "tasks" => "required",]);

        if ($validator->fails())
        {
            return false;
        }

        DB::beginTransaction();
        try {
            $input = $request->request->all();
            $input["start_date"] = date("Y-m-d",strtotime($input["start_date"]));
            $input["created_by"] = auth()->id();
            $input["type"] = Visit::$types[$request->type];
            $input["code"] = $this->initCode();
            $input["num_of_visits"] = 0;
            $input["on_going"] = 0;
            $input["repeat_every"] = 0;

            if(isset($input["days"])) {
                $days = json_encode($input["days"]);
                $input["holidays"] = $days;
            }
            $visit = Visit::create($input);


            /** create stores */
            if(isset($input["store"])) {
                $model = new VisitStore();
                $model->visit_id = $visit->id;
                $model->store_id = $input["store"];
                $model->status = 0;
                $model->save();
            }
            /**  */
            /** create stores */
            if(isset($input["tasks"])) {
                foreach ($input["tasks"] as $task) {
                    $model = new VisitTask();
                    $model->visit_id = $visit->id;
                    $model->task_id = $task;
                    $model->status = 0;
                    $model->save();
                }
            }
            /**  */
            /** create agents */
            if(isset($input["agents"])) {
                foreach ($input["agents"] as $agent) {
                    $model = new VisitAgent();
                    $model->visit_id = $visit->id;
                    $model->agent_id = $agent;
                    $model->status = 0;
                    $model->save();
                }
            }
            /**  */
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }


    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id, Request $request)
    {
        $visit = $this->visitRepository->findWithoutFail($id);

        if (empty($visit)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visits.index'));
        }


        if ($visit->type == Visit::$types["normal"]) {
            $partialView = "main_content/backend/visits.normal.show_fields";
        } else {
            $partialView = "main_content/backend/visits.scheduled.show_fields";
        }
        $stores = VisitStore::join("stores", "stores.id", "=", "visit_stores.store_id")
            ->where("visit_id", $visit->id)
            ->select(["stores.title_ar"])
            ->get();
        $visit->stores = $stores;

        $agents = VisitAgent::join("users", "users.id", "=", "visit_agents.agent_id")
            ->where("visit_id", $visit->id)
            ->select(["users.name", "users.id"])
            ->get();
        $visit->agents = $agents;


        /** get tasks for visit */
        $tasks = VisitTask::join("tasks","visit_tasks.task_id","=", "tasks.id")
            ->where("visit_id",$visit->id)->select(["tasks.title_ar","visit_tasks.status"])->get();
        $visit->tasks = $tasks;

        /** get visits next schedule */
        $visitsDates = [];
        $startDate = $visit->start_date;
        for ($i = 0; $i < 6; $i++) {
            if(!$visit->on_going && $i < $visit->num_of_visits) {
                $nextDate = $this->checkNextDate($visit);
                $visit->start_date = $nextDate;
                $visitsDates[] = $nextDate;
            } else {
                $nextDate = $this->checkNextDate($visit);
                $visit->start_date = $nextDate;
                $visitsDates[] = $nextDate;
            }

        }
        $visit->start_date = $startDate;


//        $visits = Visit::join("visit_details","visits.id","=","visit_details.visit_id")
//            ->where("visit_details.visit_id", $visit->id)
//            ->where("visit_details.status",Visit::$statuses["PENDING"])
//            ->groupBy("visit_details.visit_id")
//            ->select(["visits.*","visit_details.start_date as detail_date",
//                DB::raw("CEILING((select count(id) from visit_details where visit_details.visit_id = visits.id) / (select count(id) from visit_stores where visit_stores.visit_id = visits.id)) as visit_rows")])
//            ->get();
//
//        foreach ($visits as $visitRow) {
//            if(!$visit->on_going && $visitRow->visit_rows < $visit->num_of_visits) {
//
//                for($i = 0; $i< 6; $i ++ ) {
//                    if($visitRow->visit_rows < $visit->num_of_visits) {
//                        $visit->start_date = $visitRow->detail_date;
//                        $nextDate = $this->checkNextDate($visit);
//                        $visitsDates[] = $nextDate;
//                        $visitRow->detail_date = $nextDate;
//                        $visitRow->visit_rows += 1;
//                    } else {
//                        break;
//                    }
//                }
//            }
//        }
        /** ** */

        $this->data["visit"] = $visit;
        $this->data["view"] = $partialView;
        $this->data["type"] = $visit->type;
        $this->data["dates"] = $visitsDates;

        return $this->_view('visits.show');
    }


    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        $visit = $this->visitRepository->findWithoutFail($id);

        if (empty($visit)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visits.index'));
        }

        if($visit->type == Visit::$types["normal"]) {
            $partialView = "main_content/backend/visits.normal.fields";

        } else {
            $partialView = "main_content/backend/visits.scheduled.fields";
        }


        $stores = new Store();
        $stores = $this->checkCompany($stores);
        $stores = $stores->get();

        $agents = new Agent();
        $agents = $this->checkCompany($agents);
        $agents = $agents->where("active",1);
        $agents = $agents->get();


        $storesSelected = VisitStore::where("visit_id",$visit->id)->pluck("store_id")->toArray();
        $tasks = VisitTask::where("visit_id",$visit->id)->pluck("task_id")->toArray();
        $agentsSelected = VisitAgent::where("visit_id",$visit->id)->pluck("agent_id")->toArray();
        try {
            $days = json_decode($visit->holidays);
            if($days == null) {
                $config = Config::latest("id")->first();
                $days = json_decode($config->system_holidays);
            }
        } catch (\Exception $e) {
            $days = [];
        }

        if(strtotime($visit->start_date) > time() ) {
            $editStartDate = true;
        } else {
            $editStartDate = false;
        }
        //$visit->start_date = date("m/d/Y",strtotime($visit->start_date));

        $this->data["view"] = $partialView;
        $this->data["visit"] = $visit;
        $this->data["agents"] = $agents;
        $this->data["stores"] = $stores;
        $this->data["days"] = $days;
        $this->data["agentsSelected"] = $agentsSelected;
        $this->data["storesSelected"] = $storesSelected;
        $this->data["tasks"] = $tasks;
        $this->data["editStartDate"] = $editStartDate;
        //dd($visit);
        return $this->_view('visits.edit');
    }


    /**
     * @param $id
     * @param UpdateVisitRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update($id, UpdateVisitRequest $request)
    {

        $visit = $this->visitRepository->findWithoutFail($id);

        if (empty($visit)) {
            $data['message'] = "عفوا خطأ في البيانات برجاء محاولة مره اخري";
            $data['success'] = false;
            $data["type"] = "false";
            $data["status_code"] = 400;
            return $this->respondBadRequest($data["message"]);

        }

        if($visit->type == Visit::$types["scheduled"]) {
            if($this->updateSchedule($visit, $request)) {
                $message = "تم حفظ البيانات بنجاح";
            } else {
                $data['message'] = "لم يتم التعديل بنجاح";
                $data['success'] = false;
                $data["type"] = "false";
                $data["status_code"] = 400;
                return $this->respondBadRequest($data["message"]);
            }
        } else {
            if($this->updateNormal($visit, $request)) {
                $message = "تم حفظ البيانات بنجاح";
            } else {
                $data['message'] = "لم يتم التعديل بنجاح";
                $data['success'] = false;
                $data["type"] = "false";
                $data["status_code"] = 400;
                return $this->respondBadRequest($data["message"]);
            }
        }

        $data["data"]['message'] = $message;
        $data['success'] = true;
        $data["type"] = "success";
        return $data;

    }

    /**
     * @param Visit $visit
     * @param Request $request
     * @return bool
     */
    private function updateSchedule(Visit $visit, Request $request) {
        DB::beginTransaction();
        try {
            $input = $request->request->all();
            if(isset($input["start_date"])) {
                $input["start_date"] = date("Y-m-d",strtotime($input["start_date"]));
            }
            if($input["num_of_visits"] == null && $input["on_going"] == 1) {
                $input["num_of_visits"] = 0;
            }
            if(isset($input["days"])) {
                $days = json_encode($input["days"]);
                $input["holidays"] = $days;
            } else {
                $days = json_encode([]);
                $input["holidays"] = $days;
            }
            $visit->update($input);


            /** create stores */
            if(isset($input["stores"])) {
                VisitStore::where("visit_id",$visit->id)->delete();
                foreach ($input["stores"] as $store) {
                    $model = new VisitStore();
                    $model->visit_id = $visit->id;
                    $model->store_id = $store;
                    $model->status = 0;
                    $model->save();
                }
            }
            /**  */
            /** create stores */
            if(isset($input["tasks"])) {
                VisitTask::where("visit_id",$visit->id)->delete();
                foreach ($input["tasks"] as $task) {
                    $model = new VisitTask();
                    $model->visit_id = $visit->id;
                    $model->task_id = $task;
                    $model->status = 0;
                    $model->save();
                }
            }
            /**  */
            /** create agents */
            if(isset($input["agent"])) {
                VisitAgent::where("visit_id",$visit->id)->delete();
                $model = new VisitAgent();
                $model->visit_id = $visit->id;
                $model->agent_id = $input["agent"];
                $model->status = 0;
                $model->save();
            }
            /**  */
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }


    }


    /**
     * @param Visit $visit
     * @param Request $request
     * @return bool
     */
    private function updateNormal(Visit $visit, Request $request) {
        DB::beginTransaction();
        try {
            $input = $request->request->all();
            $input["start_date"] = date("Y-m-d",strtotime($input["start_date"]));
            if(isset($input["days"])) {
                $days = json_encode($input["days"]);
                $input["holidays"] = $days;
            }
            $visit->update($input);


            /** create stores */
            if(isset($input["store"])) {
                VisitStore::where("visit_id",$visit->id)->delete();
                $model = new VisitStore();
                $model->visit_id = $visit->id;
                $model->store_id = $input["store"];
                $model->status = 0;
                $model->save();
            }
            /**  */
            /** create stores */
            if(isset($input["tasks"])) {
                VisitTask::where("visit_id",$visit->id)->delete();
                foreach ($input["tasks"] as $task) {
                    $model = new VisitTask();
                    $model->visit_id = $visit->id;
                    $model->task_id = $task;
                    $model->status = 0;
                    $model->save();
                }
            }
            /**  */
            /** create agents */
            if(isset($input["agents"])) {
                VisitAgent::where("visit_id",$visit->id)->delete();
                foreach ($input["agents"] as $agent) {
                    $model = new VisitAgent();
                    $model->visit_id = $visit->id;
                    $model->agent_id = $agent;
                    $model->status = 0;
                    $model->save();
                }
            }
            /**  */
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }


    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function reassignAgent($id,Request $request)
    {
        $visit = $this->visitRepository->findWithoutFail($id);

        if (empty($visit)) {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }

        $data = array();
        $data["agentsSelected"] = [];
        /** create agents */
        if(isset($request->agents)) {
            VisitAgent::where("visit_id",$visit->id)->delete();
            foreach ($request->agents as $agent) {
                $agentObject = Agent::find($agent);
                $data["agentsSelected"][] = $agentObject->name;
                $model = new VisitAgent();
                $model->visit_id = $visit->id;
                $model->agent_id = $agent;
                $model->status = 0;
                $model->save();
            }
        }
        /**  */

        $data["agents"] = Agent::whereNotIn("id",$request->agents)->get();

        $data['message'] = 'تم اعادة التخصيص بنجاح';
        $data['success'] = true;
        return $data;

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function destroy($id, Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->visitRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('visits.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('visits.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->visitRepository->delete($id);
            }
        }else if($id != null) {
            $this->visitRepository->delete($id);
        } else  {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            $data['type'] = "false";
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        $data['type'] = "success";
        return $data;

    }

    public function data(Request $request) {
            $items = new Visit();
            $items = $this->checkCompany($items);
            $items = $items->join("users","users.id","=","visits.created_by");
            $items = $items->where("visits.type",$request->type);
            $items->orderByDesc("visits.id")->select(["visits.*","users.name as created_user"]);

            return DataTables::eloquent($items)
                ->addColumn('options', function (Visit $item) {

                    $back = "";
                    $back .= '<div class="btn-group">';
                    $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '._lang("app.options").'';
                    $back .= '<i class="fa fa-angle-down"></i>';
                    $back .= '</button>';
                    $back .= '<ul class = "dropdown-menu" role = "menu">';
                    $back .= '<li>';
                    $back .= '<a href="' . route('visits.show', $item->id) . '">';
                    $back .= '<i class = "icon-docs"></i>' . _lang('app.show');
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '<li>';
                    $back .= '<a href="' . route('visits.edit', $item->id) . '">';
                    $back .= '<i class = "icon-docs"></i>' . _lang('app.edit');
                    $back .= '</a>';
                    $back .= '</li>';
                $back .= '<li>';
                $back .= '<a href="" data-toggle="confirmation" onclick = "Visits.delete(this);return false;" data-id = "' . $item->id . '">';
                $back .= '<i class = "icon-docs"></i>' . _lang("app.delete");
                $back .= '</a>';
                $back .= '</li>';

                    $back .= '</ul>';
                    $back .= ' </div>';

                    return $back;

                })
                /*
                 ->addColumn("active",function(Visit $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (Visit $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("show",function(Visit $item){
                    $back = '<a class="btn btn-success" href="' . route('visits.show', $item->id) . '" data-id="'.$item->id.'">';
                    $back.= "تفاصيل الزيارة"."</a>";
                    return $back;
                })
                ->editColumn("num_of_stores",function(Visit $item){
                    $back = VisitStore::where("visit_id",$item->id)->count("id");
                    return $back;
                })
                ->editColumn("repeat_every",function(Visit $item){
                    $back = $item->repeat_every." يوم";
                    return $back;
                })
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }

        public function todayTasksData(Request $request) {
            $items = new Visit();
            $items = $this->checkCompany($items);
            $items = $items->join("users","users.id","=","visits.created_by");
            if($request->filter > 0) {
                if($request->filter == Visit::$statuses["CANCELED_BY_ADMIN"]) {
                    $items = $items->whereIn("visits.status",[Visit::$statuses["CANCELED_BY_ADMIN"], Visit::$statuses["CANCELED_BY_AGENT"], Visit::$statuses["CANCELED_BY_SUPERVISOR"]]);

                } else {
                    $items = $items->where("visits.status",$request->filter);
                }
            }
            if($request->filter_date != null) {
                $items = $items->where("visits.start_date",$request->filter_date);
            } else {
                $items = $items->where("visits.start_date",date("Y-m-d"));
            }
            $items->orderByDesc("visits.id")->select(["visits.*","users.name as created_user"]);

            return DataTables::eloquent($items)
                ->addColumn('options', function (Visit $item) {

                    $back = "";
                    $back .= '<div class="btn-group">';
                    $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '._lang("app.options").'';
                    $back .= '<i class="fa fa-angle-down"></i>';
                    $back .= '</button>';
                    $back .= '<ul class = "dropdown-menu" role = "menu">';
                    $back .= '<li>';
                    $back .= '<a href="' . route('visits.show', $item->id) . '">';
                    $back .= '<i class = "icon-docs"></i>' . _lang('app.show');
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '<li>';
                    $back .= '<a href="' . route('visits.edit', $item->id) . '">';
                    $back .= '<i class = "icon-docs"></i>' . _lang('app.edit');
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '<li>';
                    $back .= '<a href="" data-toggle="confirmation" onclick = "Visits.delete(this);return false;" data-id = "' . $item->id . '">';
                    $back .= '<i class = "icon-docs"></i>' . _lang("app.delete");
                    $back .= '</a>';
                    $back .= '</li>';

                    $back .= '</ul>';
                    $back .= ' </div>';

                    return $back;

                })
                /*
                 ->addColumn("active",function(Visit $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (Visit $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }

}
