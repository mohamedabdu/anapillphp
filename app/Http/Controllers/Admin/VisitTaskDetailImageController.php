<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVisitTaskDetailImageRequest;
use App\Http\Requests\UpdateVisitTaskDetailImageRequest;
use App\Repositories\VisitTaskDetailImageRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\VisitTaskDetailImage;
use function Helper\Common\imageUrl;

class VisitTaskDetailImageController extends AppBaseController
{
    /** @var  VisitTaskDetailImageRepository */
    private $visitTaskDetailImageRepository;

    public function __construct(VisitTaskDetailImageRepository $visitTaskDetailImageRepo)
    {
        parent::__construct();
        $this->visitTaskDetailImageRepository = $visitTaskDetailImageRepo;
    }







    /**
     * Display a listing of the VisitTaskDetailImage.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->visitTaskDetailImageRepository->pushCriteria(new RequestCriteria($request));
        $visitTaskDetailImages = $this->visitTaskDetailImageRepository->all();
        */

        return view('visit_task_detail_images.index');
        /*return view('visit_task_detail_images.index')
             ->with('visitTaskDetailImages', $visitTaskDetailImages);*/
    }



    /**
     * Show the form for creating a new VisitTaskDetailImage.
     *
     * @return Response
     */
    public function create()
    {
        return view('visit_task_detail_images.create');
    }

    /**
     * Store a newly created VisitTaskDetailImage in storage.
     *
     * @param CreateVisitTaskDetailImageRequest $request
     *
     * @return Response
     */
    public function store(CreateVisitTaskDetailImageRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $visitTaskDetailImage = $this->visitTaskDetailImageRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('visitTaskDetailImages.index'));
    }

    /**
     * Display the specified VisitTaskDetailImage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visitTaskDetailImage = $this->visitTaskDetailImageRepository->findWithoutFail($id);

        if (empty($visitTaskDetailImage)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskDetailImages.index'));
        }

        return view('visit_task_detail_images.show')->with('visitTaskDetailImage', $visitTaskDetailImage);
    }

    /**
     * Show the form for editing the specified VisitTaskDetailImage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visitTaskDetailImage = $this->visitTaskDetailImageRepository->findWithoutFail($id);

        if (empty($visitTaskDetailImage)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskDetailImages.index'));
        }

        return view('visit_task_detail_images.edit')->with('visitTaskDetailImage', $visitTaskDetailImage);
    }

    /**
     * Update the specified VisitTaskDetailImage in storage.
     *
     * @param  int              $id
     * @param UpdateVisitTaskDetailImageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVisitTaskDetailImageRequest $request)
    {


        $visitTaskDetailImage = $this->visitTaskDetailImageRepository->findWithoutFail($id);

        if (empty($visitTaskDetailImage)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskDetailImages.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $visitTaskDetailImage = $this->visitTaskDetailImageRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('visitTaskDetailImages.index'));
    }

    /**
     * Remove the specified VisitTaskDetailImage from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->visitTaskDetailImageRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('visitTaskDetailImages.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('visitTaskDetailImages.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->visitTaskDetailImageRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = VisitTaskDetailImage::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (VisitTaskDetailImage $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('visitTaskDetailImages.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('visitTaskDetailImages.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(VisitTaskDetailImage $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (VisitTaskDetailImage $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
