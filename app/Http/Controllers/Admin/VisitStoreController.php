<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVisitStoreRequest;
use App\Http\Requests\UpdateVisitStoreRequest;
use App\Repositories\VisitStoreRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\VisitStore;
use function Helper\Common\imageUrl;

class VisitStoreController extends AppBaseController
{
    /** @var  VisitStoreRepository */
    private $visitStoreRepository;

    public function __construct(VisitStoreRepository $visitStoreRepo)
    {
        parent::__construct();
        $this->visitStoreRepository = $visitStoreRepo;
    }







    /**
     * Display a listing of the VisitStore.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->visitStoreRepository->pushCriteria(new RequestCriteria($request));
        $visitStores = $this->visitStoreRepository->all();
        */

        return view('visit_stores.index');
        /*return view('visit_stores.index')
             ->with('visitStores', $visitStores);*/
    }



    /**
     * Show the form for creating a new VisitStore.
     *
     * @return Response
     */
    public function create()
    {
        return view('visit_stores.create');
    }

    /**
     * Store a newly created VisitStore in storage.
     *
     * @param CreateVisitStoreRequest $request
     *
     * @return Response
     */
    public function store(CreateVisitStoreRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $visitStore = $this->visitStoreRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('visitStores.index'));
    }

    /**
     * Display the specified VisitStore.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visitStore = $this->visitStoreRepository->findWithoutFail($id);

        if (empty($visitStore)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitStores.index'));
        }

        return view('visit_stores.show')->with('visitStore', $visitStore);
    }

    /**
     * Show the form for editing the specified VisitStore.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visitStore = $this->visitStoreRepository->findWithoutFail($id);

        if (empty($visitStore)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitStores.index'));
        }

        return view('visit_stores.edit')->with('visitStore', $visitStore);
    }

    /**
     * Update the specified VisitStore in storage.
     *
     * @param  int              $id
     * @param UpdateVisitStoreRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVisitStoreRequest $request)
    {


        $visitStore = $this->visitStoreRepository->findWithoutFail($id);

        if (empty($visitStore)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitStores.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $visitStore = $this->visitStoreRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('visitStores.index'));
    }

    /**
     * Remove the specified VisitStore from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->visitStoreRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('visitStores.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('visitStores.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->visitStoreRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = VisitStore::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (VisitStore $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('visitStores.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('visitStores.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(VisitStore $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (VisitStore $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
