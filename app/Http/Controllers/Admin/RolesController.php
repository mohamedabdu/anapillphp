<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BackendController;
use App\Models\Permission;
use App\Models\Role;
use App\Models\RoleTranslation;
use App\Models\RolePermission;
use Illuminate\Http\Request;
use DB;
use Validator;

class RolesController extends BackendController
{
    private $rules = [
        'is_company' => 'required|in:0,1',
        'permissions.*' => 'required'
    ];

    public function __construct()
    {
        parent::__construct();

        $this->data['parent_tab'] = 'general_settings';
        if (request()->input('is_company')) {
            $this->middleware('role:company_roles');
            $this->data['tab'] = 'company_roles';
        } else {
            $this->middleware('role:roles');
            $this->data['tab'] = 'roles';
        }
    }
  
    public function index(Request $request)
    {
        $this->data["is_company"] = $request->input('is_company') ? 1 : 0;
        return $this->_view("roles.index");
    }

   
    public function create(Request $request)
    {
        $this->data["is_company"] = $request->input('is_company') ? true : false;
        $permissions = Permission::select('permissions.*');
        if ($this->user->created_by) {
            $permissions->join('role_permissions', 'role_permissions.permission_id','=', 'permissions.id')
                        ->join('roles', 'roles.id','=', 'role_permissions.role_id')
                        ->join('users','users.role_id','=','roles.id')
                        ->where('users.id',$this->user->created_by);
        }
        if ($request->input('is_company')) {
            $permissions->where('admin_only',false);
        }
        $permissions = $permissions->get();
        $this->data['permissions'] = Permission::transformCollection($permissions,'Admin');
        return $this->_view('roles.create');
    }

   
    public function store(Request $request)
    {
        try {
            $columns_arr = array(
                'title' => 'required'
            );
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);

            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            
            DB::beginTransaction();
            
            $role = new Role();
            $role->created_by = $this->user->id;
            $role->is_company = $request->input('is_company');
            $role->save();

            $this->createRoleTranslations($role->id, $request);
            $this->createRolePermissions($role->id, $request);
            DB::commit();
            return _json('success',_lang('app.added_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error',_lang('app.error_is_occured'),400);
        }
    }

    public function edit($id)
    {
        try 
        {
            $id = en_de_crypt($id, false);
            $role = Role::where('id',$id);
            $role = $this->checkCreatedBy($role,'roles')
                         ->first();

            if (!$role) {
                session()->flash('error_message', _lang('app.this_item_doesn\'t_exist'));
                return redirect(route('roles.index'));
            }
            $permissions = Permission::select('permissions.*');
            if ($this->user->created_by) {
                $permissions->join('role_permissions', 'role_permissions.permission_id','=', 'permissions.id')
                            ->join('roles', 'roles.id','=', 'role_permissions.role_id')
                            ->join('users','users.role_id','=','roles.id')
                            ->where('users.id',$this->user->created_by);
            }
            if ($role->is_company) {
                $permissions->where('admin_only', false);
            }
            $permissions = $permissions->get();
            $this->data['permissions'] = Permission::transformCollection($permissions,'Admin');
            $role_permissions = RolePermission::where('role_id', $id)->get()->pluck('permission_id');
            $this->data['role_permissions'] = $role_permissions->map(function ($permission)  {
                return en_de_crypt($permission);
            })->toArray();
            $this->data['translations'] = RoleTranslation::where('role_id', $id)->get()->keyBy('locale');
            $this->data["role"] = $role;
            $this->data['tab'] = $role->is_company ? 'company_roles' : 'roles';
            return $this->_view('roles.edit'); 
        } 
        catch (\Exception $ex) {
            session()->flash('error_message', _lang('app.error_is_occured'));
            if ($role->is_company) {
                return redirect(route('roles.index',['is_company' => 1]));
            }
            return redirect(route('roles.index'));
        }
       
    }

    
    public function update(Request $request, $id)
    {
        try {
            $columns_arr = array(
                'title' => 'required'
            );
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);
            unset($this->rules['is_company']);
            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            $id = en_de_crypt($id, false);
            $role = Role::where('id',$id);
            $role = $this->checkCreatedBy($role,'roles')
                         ->first();
            if (!$role) {
                return _json('error', _lang('app.not_found'), 404);
            }
            DB::beginTransaction();

            RolePermission::where('role_id', $role->id)->delete();
            RoleTranslation::where('role_id', $role->id)->delete();

            $this->createRoleTranslations($role->id, $request);
            $this->createRolePermissions($role->id, $request);

            DB::commit();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }

    
    public function destroy($id)
    {
        $id = en_de_crypt($id, false);
        $role = Role::where('id', $id);
        $role = $this->checkCreatedBy($role, 'roles')
                    ->first();
        if (!$role) {
            return _json('error', _lang('app.error_is_occured'), 404);
        }
        DB::beginTransaction();
        try {
            $role->delete();
            DB::commit();
            return _json('success', _lang('app.deleted_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            if ($ex->getCode() == 23000) {
                return _json('error', _lang('app.this_record_can_not_be_deleted_for_linking_to_other_records'), 400);
            } else {

                return _json('error', _lang('app.error_is_occured'), 400);
            }
        }

    }

    


    public function data(Request $request) {
        $roles = Role::join('role_translations', function($query){
                                $query->on('role_translations.role_id', '=', 'roles.id')
                                ->where('role_translations.locale',$this->lang_code);
                            });
        $roles = $this->checkCreatedBy($roles,'roles')
                                ->where("is_company", $request->input('is_company'))
                                ->orderBy("id",'desc')
                                ->select('roles.*', 'role_translations.title');
            

            return \DataTables::eloquent($roles)
                ->addColumn('options', function ($item) {
                    $back = "";
                    $back .= '<div class="btn-group">';
                    $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '. _lang("app.options");
                    $back .= '<i class="fa fa-angle-down"></i>';
                    $back .= '</button>';
                    $back .= '<ul class = "dropdown-menu" role = "menu">';
                    $back .= '<li>';
                    $back .= '<a href="' . route('roles.edit', en_de_crypt($item->id) ) . '">';
                    $back .= '<i class = "fas fa-edit"></i>' . _lang('app.edit');
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '<li>';
                    $back .= '<a href="" data-toggle="confirmation" onclick = "Roles.delete(this);return false;" data-id = "' . en_de_crypt($item->id) . '">';
                    $back .= '<i class = "fas fa-trash-alt"></i>' . _lang("app.delete");
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '</ul>';
                    $back .= ' </div>';

                    return $back;
                })
                ->escapeColumns([])
                ->make(true);
    }

    private function createRoleTranslations($role_id, $request)
    {
        $role_translations = array();
        $title = $request->input('title');
        foreach ($title as $key => $value) {
            $role_translations[] = array(
                'locale' => $key,
                'title' => $value,
                'role_id' => $role_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
        }
        RoleTranslation::insert($role_translations);
    }

    private function createRolePermissions($role_id, $request)
    {
        $permissions = $request->input('permissions');
        $role_permessions = [];
        foreach ($permissions as $permission) {
            $role_permessions[] = [
                'role_id' => $role_id,
                'permission_id' => en_de_crypt($permission, false),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }
        RolePermission::insert($role_permessions);
    }

    


}
