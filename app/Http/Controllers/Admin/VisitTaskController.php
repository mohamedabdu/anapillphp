<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVisitTaskRequest;
use App\Http\Requests\UpdateVisitTaskRequest;
use App\Repositories\VisitTaskRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\VisitTask;
use function Helper\Common\imageUrl;

class VisitTaskController extends AppBaseController
{
    /** @var  VisitTaskRepository */
    private $visitTaskRepository;

    public function __construct(VisitTaskRepository $visitTaskRepo)
    {
        parent::__construct();
        $this->visitTaskRepository = $visitTaskRepo;
    }







    /**
     * Display a listing of the VisitTask.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->visitTaskRepository->pushCriteria(new RequestCriteria($request));
        $visitTasks = $this->visitTaskRepository->all();
        */

        return view('visit_tasks.index');
        /*return view('visit_tasks.index')
             ->with('visitTasks', $visitTasks);*/
    }



    /**
     * Show the form for creating a new VisitTask.
     *
     * @return Response
     */
    public function create()
    {
        return view('visit_tasks.create');
    }

    /**
     * Store a newly created VisitTask in storage.
     *
     * @param CreateVisitTaskRequest $request
     *
     * @return Response
     */
    public function store(CreateVisitTaskRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $visitTask = $this->visitTaskRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('visitTasks.index'));
    }

    /**
     * Display the specified VisitTask.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visitTask = $this->visitTaskRepository->findWithoutFail($id);

        if (empty($visitTask)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTasks.index'));
        }

        return view('visit_tasks.show')->with('visitTask', $visitTask);
    }

    /**
     * Show the form for editing the specified VisitTask.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visitTask = $this->visitTaskRepository->findWithoutFail($id);

        if (empty($visitTask)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTasks.index'));
        }

        return view('visit_tasks.edit')->with('visitTask', $visitTask);
    }

    /**
     * Update the specified VisitTask in storage.
     *
     * @param  int              $id
     * @param UpdateVisitTaskRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVisitTaskRequest $request)
    {


        $visitTask = $this->visitTaskRepository->findWithoutFail($id);

        if (empty($visitTask)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTasks.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $visitTask = $this->visitTaskRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('visitTasks.index'));
    }

    /**
     * Remove the specified VisitTask from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->visitTaskRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('visitTasks.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('visitTasks.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->visitTaskRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = VisitTask::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (VisitTask $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('visitTasks.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('visitTasks.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(VisitTask $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (VisitTask $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
