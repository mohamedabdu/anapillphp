<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSubAdminRequest;
use App\Http\Requests\UpdateSubAdminRequest;
use App\Models\Agent;
use App\Models\Supervisor;
use App\Scopes\AgentScope;
use App\Scopes\SupervisorScope;
use App\User;
use function Helper\Common\sendMail;
use Illuminate\Support\Facades\DB;
use function Helper\Common\__lang;
use function Helper\Common\imageUrl;
use function Helper\Common\sendNoti;
use Illuminate\Http\Request;
use Flash;
use Response;
use DataTables;
use Validator;
class SupervisorsController extends AppBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return $this->_view('supervisors.index');
    }


    public function create(Request $request) {
        $agents = new Agent();
        $agents = $this->checkCompany($agents);
        $agents = $agents->get();

        $this->data["agents"] = $agents;
        return $this->_view('supervisors.create');

    }
    public function store(Request $request) {

        $rules = [
            "name" => "required|max:25",
            "email" => "required|email|unique:users",
            "password" => "required",
            "mobile" => "required|numeric|regex:(05)|digits:10",
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return _json('error', $errors);
        }

        DB::beginTransaction();

        try {
            $input = $request->all();
            $input['image'] = $this->uploadFile($request,"image",false);
            $input["password"] = bcrypt($input["password"]);
            $input["created_by"] = auth()->id();
            $input["type"] = SupervisorScope::$typeOfSupervisor;
            if(isset($input["days"])) {
                $input["work_days"] = json_encode($input["days"]);
            }
            $supervisor = Supervisor::create($input);

            /** update supervisor  */
            Agent::whereIn("id",$input["agents"])->update(["supervisor_id" => $supervisor->id]);

            DB::commit();

            $object = new \stdClass();
            $object->username = $input["name"];
            $object->email = $input["email"];
            $object->password = $request->password;
            sendMail("vendor.passwords.mailPublic","vendor.passwords.accountDetails",$object,"Account Details");


            $data['message'] ='تم حفظ البيانات بنجاح';
            $data['success'] = true;
            $data["type"] = "success";
            return $data;
        } catch (\Exception $e) {
            DB::rollback();
            $data['message'] ='لم يتم الحفظ';
            $data['success'] = false;
            $data["type"] = "false";
            $data["status_code"] = 400;
            return $this->respondBadRequest($data["message"]);
        }

    }

    public function edit($id,Request $request) {
        $user = Supervisor::find($id);
        if($user == null) {
            return redirect(route("supervisors.index"));
        }
        $days = [];
        if($user->work_days != null) {
            try {
                $days = json_decode($user->work_days);
            } catch (\Exception $e) {

            }
        }

        $agents = new Agent;
        $agents = $this->checkCompany($agents);
        $agents = $agents->get();

        $agentsSelected = Agent::where("supervisor_id",$id)->pluck("id")->toArray();


        $this->data["agents"] = $agents;
        $this->data["agentsSelected"] = $agentsSelected;
        $this->data["days"] = $days;
        $this->data["supervisor"] = $user;
        return $this->_view('supervisors.edit');

    }
    public function update($id,Request $request) {
        $rules = [
            "name" => "required|max:25",
            "email" => "required|email|unique:users,email,".$id,
            "mobile" => "required|numeric|regex:(05)|digits:10",
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return _json('error', $errors);
        }


        $user = Supervisor::find($id);
        if($user == null) {
            $data['message'] ='لم يتم التعديل';
            $data['success'] = false;
            $data["type"] = "false";
            $data["status_code"] = 400;
            return $this->respondBadRequest($data["message"]);
        }

        DB::beginTransaction();

        try {
            $input = $request->request->all();

            if($request->hasFile("image")) {
                $input['image'] = $this->uploadFile($request,"image",false);
            }
            if($input["password"] != null) {
                $input["password"] = bcrypt($input["password"]);
            } else {
                unset($input["password"]);
            }
            $input["created_by"] = auth()->id();
            if(isset($input["days"])) {
                $input["work_days"] = json_encode($input["days"]);
            }

            $user->update($input);

            /** update agents */
            Agent::whereNotIn("id",$input["agents"])->where("supervisor_id",$id)->update(["supervisor_id" => 0]);
            Agent::whereIn("id",$input["agents"])->update(["supervisor_id" => $id]);


            DB::commit();

            $data["data"]['message'] ='تم تعديل البيانات بنجاح';
            $data['success'] = true;
            $data["type"] = "success";
            return $data;
        } catch (\Exception $e) {
            DB::rollback();
            $data['message'] ='لم يتم التعديل';
            $data['success'] = false;
            $data["type"] = "false";
            $data["status_code"] = 400;
            return $this->respondBadRequest($data["message"]);
        }

    }

    public function active($id,Request $request)
    {
        $user = Supervisor::find($id);
        if (empty($user)) {
            $data['success'] = false;
            $data['message'] = __lang("error_no_data");
            return $data;
        }
        if($user->active == 1)
        {
            $user->active = 0;
            $user->save();
        }
        elseif($user->active == 0)
        {
            $user->active = 1;
            $user->save();
        }


        $data['message'] = 'تم حفظ البيانات بنجاح';
        $data['success'] = true;
        return $data;
    }


    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id, Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->brandRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('brands.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('brands.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                Supervisor::find($id)->delete();
            }
        }else if($id != null) {
            Supervisor::find($id)->delete();
        } else  {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            $data['type'] = "false";
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        $data['type'] = "success";
        return $data;

    }
    public function data(Request $request) {
        $items = new Supervisor;
        $items = $this->checkCompany($items);
        $items = $items->orderByDesc("users.id")->select(["users.*"]);

        return DataTables::eloquent($items)
            ->addColumn('options', function (Supervisor $item) {
                $back = "";
                $back .= '<div class="btn-group">';
                $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '. _lang("app.options");
                $back .= '<i class="fa fa-angle-down"></i>';
                $back .= '</button>';
                $back .= '<ul class = "dropdown-menu" role = "menu">';
                $back .= '<li>';
                $back .= '<a href="' . route('supervisors.edit', $item->id) . '">';
                $back .= '<i class = "icon-docs"></i>' . _lang('app.edit');
                $back .= '</a>';
                $back .= '</li>';
//                $back .= '<li>';
//                $back .= '<a href="" data-toggle="confirmation" onclick = "Agents.delete(this);return false;" data-id = "' . $item->id . '">';
//                $back .= '<i class = "icon-docs"></i>' . "delete";
//                $back .= '</a>';
//                $back .= '</li>';

                $back .= '</ul>';
                $back .= ' </div>';

                return $back;

            })

            ->addColumn("active",function(Supervisor $item){
                if($item->active == 1)
                {
                    $back = '<a class="btn btn-warning" onclick="Supervisors.changeActive(this)" data-id="'.$item->id.'">';
                    $back.= "اقفال التفعيل"."</a>";
                }
                else
                {
                    $back = '<a class="btn btn-success" onclick="Supervisors.changeActive(this)" data-id="'.$item->id.'">';
                    $back.= "تفعيل"."</a>";
                }

                return $back;
            })
            ->editColumn('image', function (Supervisor $item) {
                $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';

                return $back;
            })
//            ->editColumn('order_numbers', function (User $item) {
//                $back = $item->orders()->count();
//                return $back;
//            })

            ->editColumn("id",function($item){
                $back = '<div class="checkbox checkbox-danger">';
                $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                return $back;
            })
            ->rawColumns(['options', 'active'])
            ->escapeColumns([])
            ->make(true);
    }
}
