<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVisitTaskDetailRequest;
use App\Http\Requests\UpdateVisitTaskDetailRequest;
use App\Repositories\VisitTaskDetailRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\VisitTaskDetail;
use function Helper\Common\imageUrl;

class VisitTaskDetailController extends AppBaseController
{
    /** @var  VisitTaskDetailRepository */
    private $visitTaskDetailRepository;

    public function __construct(VisitTaskDetailRepository $visitTaskDetailRepo)
    {
        parent::__construct();
        $this->visitTaskDetailRepository = $visitTaskDetailRepo;
    }







    /**
     * Display a listing of the VisitTaskDetail.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->visitTaskDetailRepository->pushCriteria(new RequestCriteria($request));
        $visitTaskDetails = $this->visitTaskDetailRepository->all();
        */

        return view('visit_task_details.index');
        /*return view('visit_task_details.index')
             ->with('visitTaskDetails', $visitTaskDetails);*/
    }



    /**
     * Show the form for creating a new VisitTaskDetail.
     *
     * @return Response
     */
    public function create()
    {
        return view('visit_task_details.create');
    }

    /**
     * Store a newly created VisitTaskDetail in storage.
     *
     * @param CreateVisitTaskDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateVisitTaskDetailRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $visitTaskDetail = $this->visitTaskDetailRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('visitTaskDetails.index'));
    }

    /**
     * Display the specified VisitTaskDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visitTaskDetail = $this->visitTaskDetailRepository->findWithoutFail($id);

        if (empty($visitTaskDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskDetails.index'));
        }

        return view('visit_task_details.show')->with('visitTaskDetail', $visitTaskDetail);
    }

    /**
     * Show the form for editing the specified VisitTaskDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visitTaskDetail = $this->visitTaskDetailRepository->findWithoutFail($id);

        if (empty($visitTaskDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskDetails.index'));
        }

        return view('visit_task_details.edit')->with('visitTaskDetail', $visitTaskDetail);
    }

    /**
     * Update the specified VisitTaskDetail in storage.
     *
     * @param  int              $id
     * @param UpdateVisitTaskDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVisitTaskDetailRequest $request)
    {


        $visitTaskDetail = $this->visitTaskDetailRepository->findWithoutFail($id);

        if (empty($visitTaskDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskDetails.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $visitTaskDetail = $this->visitTaskDetailRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('visitTaskDetails.index'));
    }

    /**
     * Remove the specified VisitTaskDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->visitTaskDetailRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('visitTaskDetails.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('visitTaskDetails.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->visitTaskDetailRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = VisitTaskDetail::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (VisitTaskDetail $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('visitTaskDetails.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('visitTaskDetails.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(VisitTaskDetail $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (VisitTaskDetail $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
