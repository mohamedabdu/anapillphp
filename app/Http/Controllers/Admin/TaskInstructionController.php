<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskInstructionRequest;
use App\Http\Requests\UpdateTaskInstructionRequest;
use App\Models\TaskInstructionImage;
use App\Repositories\TaskInstructionRepository;
use function Helper\Common\upload;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\TaskInstruction;
use function Helper\Common\imageUrl;

class TaskInstructionController extends AppBaseController
{
    /** @var  TaskInstructionRepository */
    private $taskInstructionRepository;

    public function __construct(TaskInstructionRepository $taskInstructionRepo)
    {
        parent::__construct();
        $this->taskInstructionRepository = $taskInstructionRepo;
    }







    /**
     * Display a listing of the TaskInstruction.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->taskInstructionRepository->pushCriteria(new RequestCriteria($request));
        $taskInstructions = $this->taskInstructionRepository->all();
        */

        return $this->_view('task_instructions.index');
        /*return view('task_instructions.index')
             ->with('taskInstructions', $taskInstructions);*/
    }



    /**
     * Show the form for creating a new TaskInstruction.
     *
     * @return Response
     */
    public function create()
    {
        return $this->_view('task_instructions.create');
    }

    /**
     * Store a newly created TaskInstruction in storage.
     *
     * @param CreateTaskInstructionRequest $request
     *
     * @return Response
     */
    public function store(CreateTaskInstructionRequest $request)
    {

        $input = $request->request->all();

        $taskInstruction = $this->taskInstructionRepository->create($input);

        if($request->hasFile("image")) {
            $images = $request->file("image");
            foreach ($images as $image) {
                $model = new TaskInstructionImage();
                $model->task_instruction_id = $taskInstruction->id;
                $model->image = upload($image, false)['img'];
                $model->save();
            }
        }

        $data['message'] ='تم حفظ البيانات بنجاح';
        $data['success'] = true;
        $data["type"] = "success";
        return $data;
    }

    /**
     * Display the specified TaskInstruction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $taskInstruction = $this->taskInstructionRepository->findWithoutFail($id);

        if (empty($taskInstruction)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('taskInstructions.index'));
        }

        return view('task_instructions.show')->with('taskInstruction', $taskInstruction);
    }

    /**
     * Show the form for editing the specified TaskInstruction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $taskInstruction = $this->taskInstructionRepository->findWithoutFail($id);

        if (empty($taskInstruction)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('taskInstructions.index'));
        }
        $this->data["taskInstruction"] = $taskInstruction;
        return $this->_view('task_instructions.edit');
    }

    /**
     * Update the specified TaskInstruction in storage.
     *
     * @param  int              $id
     * @param UpdateTaskInstructionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTaskInstructionRequest $request)
    {


        $taskInstruction = $this->taskInstructionRepository->findWithoutFail($id);

        if (empty($taskInstruction)) {
            $data['message'] ='لم يتم التعديل';
            $data['success'] = false;
            $data["type"] = "false";
            $data["status_code"] = 400;
            return $this->respondBadRequest($data["message"]);
        }


        $input = $request->request->all();

        $taskInstruction = $this->taskInstructionRepository->update($input, $id);

        if($request->hasFile("image")){
            $taskInstruction->images()->delete();
            $images = $request->file("image");
            foreach ($images as $image) {
                $model = new TaskInstructionImage();
                $model->task_instruction_id = $taskInstruction->id;
                $model->image = upload($image, false)['img'];
                $model->save();
            }
        }

        $data["data"]['message'] ='تم تعديل البيانات بنجاح';
        $data['success'] = true;
        $data["type"] = "success";
        return $data;
    }


    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function destroy($id, Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->taskInstructionRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('taskInstructions.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('taskInstructions.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $taskIns = TaskInstruction::find($id);
                $taskIns->images()->delete();
                $taskIns->delete();
            }
        } else if($id != null) {
            $taskIns = TaskInstruction::find($id);
            $taskIns->images()->delete();
            $taskIns->delete();
        } else  {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            $data['type'] = "false";
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        $data['type'] = "success";
        return $data;

    }

    public function data() {
            $items = new TaskInstruction();
            $items = $items->join("tasks","tasks.id","=","task_instructions.task_id");
            $items = $items->orderByDesc("task_instructions.id")->select(["task_instructions.*","tasks.title_ar as task"]);

            return DataTables::eloquent($items)
                ->addColumn('options', function (TaskInstruction $item) {
                    $back = "";
                    $back .= '<div class="btn-group">';
                    $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '. _lang("app.options");
                    $back .= '<i class="fa fa-angle-down"></i>';
                    $back .= '</button>';
                    $back .= '<ul class = "dropdown-menu" role = "menu">';
                    $back .= '<li>';
                    $back .= '<a href="' . route('taskInstructions.edit', $item->id) . '">';
                    $back .= '<i class = "icon-docs"></i>' . _lang('app.edit');
                    $back .= '</a>';
                    $back .= '</li>';
                    $back .= '<li>';
                    $back .= '<a href="" data-toggle="confirmation" onclick = "TaskNotes.delete(this);return false;" data-id = "' . $item->id . '">';
                    $back .= '<i class = "icon-docs"></i>' . _lang("app.delete");
                    $back .= '</a>';
                    $back .= '</li>';

                    $back .= '</ul>';
                    $back .= ' </div>';

                    return $back;

                })
                /*
                 ->addColumn("active",function(TaskInstruction $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (TaskInstruction $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
