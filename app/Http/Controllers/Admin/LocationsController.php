<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Models\Location;
use App\Models\LocationTranslation;
use Validator;
use DB;

class LocationsController extends BackendController {

   
    private $rules = [];
    public function __construct() {
        parent::__construct();
        $this->middleware('role:countries');
        $this->data['parent_tab'] = 'general_settings';
        $this->data['tab'] = 'countries';
    }

    public function index(Request $request) {
        $parent_id = $request->input('parent') ? $request->input('parent') : 0;
        $this->data['path'] = $this->node_path(en_de_crypt($parent_id,false));
        $this->data['parent_id'] = $parent_id;
        return $this->_view('locations.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        $parent_id = $request->input('parent') ? $request->input('parent') : 0;
        $this->data['path'] = $this->node_path(en_de_crypt($parent_id, false),true);
        $this->data['parent_id'] = $parent_id;
        return $this->_view('locations/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $columns_arr = array(
            'title' => 'required'
        );
        
        $lang_rules = $this->lang_rules($columns_arr);
        $this->rules = array_merge($this->rules, $lang_rules);
        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return _json('error', $errors);
        }
        DB::beginTransaction();
        try {
            $location = new Location;
            $location->created_by = $this->user->id;
            $location->parent_id = en_de_crypt($request->input('parent_id'), false);
            if ($location->parent_id != 0) {
                $parent = Location::find($location->parent_id);
                $location->level = $parent->level + 1;
                if ($parent->parents_ids == null) {
                    $location->parents_ids = $parent->id;
                }
                else{
                    $parent_ids = explode(",",$parent->parents_ids);
                    array_push($parent_ids,$parent->id);
                    $location->parents_ids = implode(",", $parent_ids);
                }
            } else {
                $location->level = 1;
            }
            $location->save();

            $this->createLocationTranslations($location->id, $request);
            DB::commit();
            return _json('success', _lang('app.added_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $find = Location::find($id);
        if ($find) {
            return _json('success', $find);
        } else {
            return _json('error', _lang('app.error_is_occured'), 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        try {
            $id = en_de_crypt($id, false);
            $location = Location::where('id',$id);
            $location = $this->checkCreatedBy($location,'locations')
                            ->first();
            if (!$location) {
                session()->flash('error_message', _lang('app.this_item_doesn\'t_exist'));
                return redirect(route("locations.index"));
            }
            $this->data['path'] = $this->node_path($location->parent_id, true);
            $this->data['translations'] = LocationTranslation::where('location_id', $id)->get()->keyBy('locale');
            $this->data['parent_id'] = $location->parent_id;
            $this->data['location'] = $location;
            return $this->_view('locations.edit');
        } catch (\Exception $ex) {
            session()->flash('error_message', _lang('app.error_is_occured'));
            return redirect(route("locations.index"));
        }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        try {
            $id = en_de_crypt($id, false);
            $location = Location::where('id',$id);
            $location = $this->checkCreatedBy($location,'locations')
                            ->first();
            if (!$location) {
                return _json('error', _lang('app.error_is_occured'), 404);
            }
            $columns_arr = array(
                'title' => 'required'
            );
            
            $lang_rules = $this->lang_rules($columns_arr);
            $this->rules = array_merge($this->rules, $lang_rules);
            $validator = Validator::make($request->all(), $this->rules);

            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            DB::beginTransaction();
            LocationTranslation::where('location_id', $location->id)->delete();
            $this->createLocationTranslations($location->id, $request);
            DB::commit();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $id = en_de_crypt($id, false);
        $location = Location::where('id',$id);
        $location = $this->checkCreatedBy($location,'locations')
                            ->first();
        if (!$location) {
            return _json('error', _lang('app.error_is_occured'), 404);
        }
        DB::beginTransaction();
        try {
            $location->delete();
            DB::commit();
            return _json('success', _lang('app.deleted_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            if ($ex->getCode() == 23000) {
                return _json('error', _lang('app.this_record_can_not_be_deleted_for_linking_to_other_records'), 400);
            } else {
                return _json('error', _lang('app.error_is_occured'), 400);
            }
        }
    }

    private function createLocationTranslations($location_id, $request)
    {
        $location_translations = array();
        foreach ($request->input('title') as $key => $value) {
            $location_translations[] = array(
                'locale' => $key,
                'title' => $value,
                'location_id' => $location_id
            );
        }
        LocationTranslation::insert($location_translations);
    }

    public function data(Request $request) {
        
        $parent_id = en_de_crypt($request->input('parent_id'),false);
        $locations = Location::Join('location_translations', function($query){
                        $query->on('locations.id', '=', 'location_translations.location_id')
                         ->where('location_translations.locale', $this->lang_code);
                })
                ->where('locations.parent_id', $parent_id);
        $locations = $this->checkCreatedBy($locations,'locations')
                ->select([
                    'locations.*', 
                    "location_translations.title",
                ]);
    

        return \DataTables::eloquent($locations)
                        ->addColumn('options', function ($item) {

                            $back = "";
                            $back .= '<div class="btn-group">';
                            $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> '. _lang("app.options");
                            $back .= '<i class="fa fa-angle-down"></i>';
                            $back .= '</button>';
                            $back .= '<ul class = "dropdown-menu" role = "menu">';
                            $back .= '<li>';
                            $back .= '<a href="'.route('locations.edit', en_de_crypt($item->id)).'">';
                            $back .= '<i class = "fas fa-edit"></i>' . _lang('app.edit');
                            $back .= '</a>';
                            $back .= '</li>';
                            $back .= '<li>';
                            $back .= '<a href="" data-toggle="confirmation" onclick = "Locations.delete(this);return false;" data-id = "'.en_de_crypt($item->id).'">';
                            $back .= '<i class = "fas fa-trash-alt"></i>' . _lang("app.delete");
                            $back .= '</a>';
                            $back .= '</li>';

                            $back .= '</ul>';
                            $back .= ' </div>';
                            return $back;
                        })
                        ->editColumn('title', function ($item) {
                            if ($item->level == 3) {
                                $back = $item->title;
                            } else {
                                $back = '<a href="' . route('locations.index') . '?parent=' . en_de_crypt($item->id) . '">' . $item->title . '</a>';
                            }
                            return $back;
                        })
                        ->escapeColumns([])
                        ->make(true);
    }

    private function node_path($id,$action=false) {
        $location = Location::where('id', $id)->first();
        $locations = null;
        if ($location) {
            $parents_ids = explode(',', $location->parents_ids);
            $parents_ids[] = $id;
            $locations = Location::leftJoin('location_translations as trans', 'locations.id', '=', 'trans.location_id')
                    ->whereIn('locations.id', $parents_ids)
                    ->where('trans.locale', $this->lang_code)
                    ->orderBy('locations.id', 'ASC')
                    ->select('locations.id', 'trans.title','locations.level')
                    ->get();
            $locations= $this->format_path($locations,$action);
        }
        return $locations;
    }

    private function format_path($locations,$action) {
        $html = '';
        if ($locations && $locations->count() > 0) {
            foreach ($locations as $key => $location) {
                if ($key < ($locations->count() - 1)) {
                    $html .= '<li><a href="' . url('admin/locations?parent=' .en_de_crypt($location->id) ) . '">' . $location->title . '</a><i class="fa fa-circle"></i></li>';
                } else {
                    if($action){
                        $html .= '<li><a href="' . url('admin/locations?parent=' . en_de_crypt($location->id)) . '">' . $location->title . '</a><i class="fa fa-circle"></i></li>';
                    }else{
                        $html .= '<li><span>' . $location->title . '</span><i class="fa fa-circle"></i></li>';
                    }
                    if ($location->level == 1 || $location->level == 2) {
                        $separator = "";
                        if ($action) {
                            $separator = '<i class="fa fa-circle"></i>';
                        }
                        if ($location->level == 1) {
                            $label = _lang('app.regions');
                        }else if ($location->level == 2) {
                            $label = _lang('app.cities');
                        }
                        $html .= '<li><span>'.$label.'</span>'.$separator.'</li>';
                    }

                }
            }
        }
        return $html;
    }

}
