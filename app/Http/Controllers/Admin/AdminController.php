<?php

namespace App\Http\Controllers\Admin;

use App\Models\Visit;
use App\Models\VisitDetails;
use App\Http\Controllers\BackendController;
use App\Models\Category;
use DB;


class AdminController extends BackendController
{
    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$statistics = DB::select(
            '
            select
            (select count(*) from categories where type = '.Category::$types['product'].') as number_of_product_categories,
            (select count(*) from categories where type = '.Category::$types['store']. ') as number_of_store_categories,
            (select count(*) from cities) as number_of_cities,
            (select count(*) from regions) as number_of_regions,
            '
        );*/
        $visitsCompleted = VisitDetails::where("status", VisitDetails::$statuses["FINISHED"])->count("id");
        $visitsProcessing = VisitDetails::join('visits','visits.id','=','visit_details.id')
                                        ->where("visits.type",Visit::$types["scheduled"])
                                        ->where("visit_details.status",VisitDetails::$statuses["UNFINISHED"])
                                        ->count("*");

        $this->data["visitsCompleted"] = $visitsCompleted;
        $this->data["visitsProcessing"] = $visitsProcessing;
        return $this->_view("index");
    }

    public function error()
    {
        return $this->_view('err404', 'backend');
    }

}
