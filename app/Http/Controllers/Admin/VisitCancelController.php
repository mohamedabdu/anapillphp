<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVisitCancelRequest;
use App\Http\Requests\UpdateVisitCancelRequest;
use App\Repositories\VisitCancelRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\VisitCancel;
use function Helper\Common\imageUrl;

class VisitCancelController extends AppBaseController
{
    /** @var  VisitCancelRepository */
    private $visitCancelRepository;

    public function __construct(VisitCancelRepository $visitCancelRepo)
    {
        parent::__construct();
        $this->visitCancelRepository = $visitCancelRepo;
    }







    /**
     * Display a listing of the VisitCancel.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->visitCancelRepository->pushCriteria(new RequestCriteria($request));
        $visitCancels = $this->visitCancelRepository->all();
        */

        return view('visit_cancels.index');
        /*return view('visit_cancels.index')
             ->with('visitCancels', $visitCancels);*/
    }



    /**
     * Show the form for creating a new VisitCancel.
     *
     * @return Response
     */
    public function create()
    {
        return view('visit_cancels.create');
    }

    /**
     * Store a newly created VisitCancel in storage.
     *
     * @param CreateVisitCancelRequest $request
     *
     * @return Response
     */
    public function store(CreateVisitCancelRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $visitCancel = $this->visitCancelRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('visitCancels.index'));
    }

    /**
     * Display the specified VisitCancel.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visitCancel = $this->visitCancelRepository->findWithoutFail($id);

        if (empty($visitCancel)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitCancels.index'));
        }

        return view('visit_cancels.show')->with('visitCancel', $visitCancel);
    }

    /**
     * Show the form for editing the specified VisitCancel.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visitCancel = $this->visitCancelRepository->findWithoutFail($id);

        if (empty($visitCancel)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitCancels.index'));
        }

        return view('visit_cancels.edit')->with('visitCancel', $visitCancel);
    }

    /**
     * Update the specified VisitCancel in storage.
     *
     * @param  int              $id
     * @param UpdateVisitCancelRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVisitCancelRequest $request)
    {


        $visitCancel = $this->visitCancelRepository->findWithoutFail($id);

        if (empty($visitCancel)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitCancels.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $visitCancel = $this->visitCancelRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('visitCancels.index'));
    }

    /**
     * Remove the specified VisitCancel from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->visitCancelRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('visitCancels.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('visitCancels.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->visitCancelRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = VisitCancel::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (VisitCancel $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('visitCancels.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('visitCancels.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(VisitCancel $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (VisitCancel $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
