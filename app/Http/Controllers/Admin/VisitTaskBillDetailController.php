<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVisitTaskBillDetailRequest;
use App\Http\Requests\UpdateVisitTaskBillDetailRequest;
use App\Repositories\VisitTaskBillDetailRepository;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use App\Models\VisitTaskBillDetail;
use function Helper\Common\imageUrl;

class VisitTaskBillDetailController extends AppBaseController
{
    /** @var  VisitTaskBillDetailRepository */
    private $visitTaskBillDetailRepository;

    public function __construct(VisitTaskBillDetailRepository $visitTaskBillDetailRepo)
    {
        parent::__construct();
        $this->visitTaskBillDetailRepository = $visitTaskBillDetailRepo;
    }







    /**
     * Display a listing of the VisitTaskBillDetail.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    /*
        $this->visitTaskBillDetailRepository->pushCriteria(new RequestCriteria($request));
        $visitTaskBillDetails = $this->visitTaskBillDetailRepository->all();
        */

        return view('visit_task_bill_details.index');
        /*return view('visit_task_bill_details.index')
             ->with('visitTaskBillDetails', $visitTaskBillDetails);*/
    }



    /**
     * Show the form for creating a new VisitTaskBillDetail.
     *
     * @return Response
     */
    public function create()
    {
        return view('visit_task_bill_details.create');
    }

    /**
     * Store a newly created VisitTaskBillDetail in storage.
     *
     * @param CreateVisitTaskBillDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateVisitTaskBillDetailRequest $request)
    {

        $input = $request->request->all();

        $input['image'] = $this->uploadFile($request,"image",true);

        $visitTaskBillDetail = $this->visitTaskBillDetailRepository->create($input);

        Flash::success('تم حفظ البيانات بنجاح');

        return redirect(route('visitTaskBillDetails.index'));
    }

    /**
     * Display the specified VisitTaskBillDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visitTaskBillDetail = $this->visitTaskBillDetailRepository->findWithoutFail($id);

        if (empty($visitTaskBillDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskBillDetails.index'));
        }

        return view('visit_task_bill_details.show')->with('visitTaskBillDetail', $visitTaskBillDetail);
    }

    /**
     * Show the form for editing the specified VisitTaskBillDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visitTaskBillDetail = $this->visitTaskBillDetailRepository->findWithoutFail($id);

        if (empty($visitTaskBillDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskBillDetails.index'));
        }

        return view('visit_task_bill_details.edit')->with('visitTaskBillDetail', $visitTaskBillDetail);
    }

    /**
     * Update the specified VisitTaskBillDetail in storage.
     *
     * @param  int              $id
     * @param UpdateVisitTaskBillDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVisitTaskBillDetailRequest $request)
    {


        $visitTaskBillDetail = $this->visitTaskBillDetailRepository->findWithoutFail($id);

        if (empty($visitTaskBillDetail)) {
            Flash::error('عفوا خطأ في البيانات برجاء محاولة مره اخري');

            return redirect(route('visitTaskBillDetails.index'));
        }


        $input = $request->request->all();

        if($request->hasFile("image"))
            $input['image'] = $this->uploadFile($request,"image",true);

        $visitTaskBillDetail = $this->visitTaskBillDetailRepository->update($input, $id);

        Flash::success('تم تعديل البيانات بنجاح');

        return redirect(route('visitTaskBillDetails.index'));
    }

    /**
     * Remove the specified VisitTaskBillDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /*
         if($request->ids != null AND count($request->ids) > 0)
         {
             foreach ($request->ids as $id)
             {
                 $this->visitTaskBillDetailRepository->delete($id);
             }
         }
         else
         {
             Flash::error('برجاء تحديد بيانات المراد حذفها');
             return redirect(route('visitTaskBillDetails.index'));
         }
        Flash::success('تم الحذف بنجاح');

        return redirect(route('visitTaskBillDetails.index'));
         */

        if($request->ids != null AND count($request->ids) > 0)
        {

            foreach ($request->ids as $id)
            {
                $this->visitTaskBillDetailRepository->delete($id);
            }
        }
        else
        {
            $data['message'] = 'برجاء تحديد بيانات المراد حذفها';
            $data['success'] = false;
            return $data;
        }
        $data['message'] = 'تم الحذف بنجاح';
        $data['success'] = true;
        return $data;

    }

    public function data() {
            $items = VisitTaskBillDetail::select();

            return DataTables::eloquent($items)
                ->addColumn('options', function (VisitTaskBillDetail $item) {
                    $back = ' <div class="btn-group">';
                    $back .= '
                        <a href="'. route('visitTaskBillDetails.show' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-eye"></i></a>
                        <a href="'. route('visitTaskBillDetails.edit' ,[$item->id]).'" class="btn btn-light waves-effect waves-light m-1"><i class="fa fa-edit"></i></a>
                    </div>';
                    return $back;
                })
                /*
                 ->addColumn("active",function(VisitTaskBillDetail $item){
                    if($item->active == 1)
                    {
                        $back = '<a class="btn btn-warning" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "اقفال التفعيل"."</a>";
                    }
                    else
                    {
                        $back = '<a class="btn btn-success" onclick="changeActive(this)" data-id="'.$item->id.'">';
                        $back.= "تفعيل"."</a>";
                    }
                    return $back;
                 })
                 ->editColumn('image', function (VisitTaskBillDetail $item) {
                    $back = ' <img src= "'.imageUrl($item->image).'" class="img-circle" style="width:75px;height:75px;" >';
                    return $back;
                 })
                 */
                ->editColumn("id",function($item){
                    $back = '<div class="checkbox checkbox-danger">';
                    $back.='
                        <input id="'.$item->id.'" type="checkbox" name="ids[]" value="'.$item->id.'">';
                    $back .= '
                        <label for="'.$item->id.'">  </label>
                    </div>';

                    return $back;
                })
                ->rawColumns(['options', 'active'])
                ->escapeColumns([])
                ->make(true);
        }


}
