<?php

namespace App\Http\Controllers\Employee;

use App\Models\City;
use App\Models\Category;
use App\Models\VisitDetails;
use Illuminate\Http\Request;

class SearchController extends EmployeeController
{
    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['cities'] = City::Select('id', 'title_ar as title')->get();
        $this->data['categories'] = Category::Select('id','title_ar as title')->where('type',Category::$types['store'])->get();
        $this->data['visits'] = $this->getVisits();
        $this->data['limit'] = $this->limit;
        return $this->_view("search");
    }

   
    private function getVisits()
    {
        $visits = VisitDetails::join('visit_stores', "visit_details.visit_store_id", "=", "visit_stores.id")
                                ->join('stores', "visit_stores.store_id", "=", "stores.id")
                                ->where("visit_details.employee_id", $this->user->id)
                                ->whereIn("visit_details.status", [
                                    VisitDetails::$statuses["UNFINISHED"],
                                    VisitDetails::$statuses["FINISHED"]
                                ])
                                ->select('visit_details.code','stores.title_ar as store')
                                ->get();
        return  $visits;
    }

    public function search(Request $request)
    {
        try {
           
           $visits = VisitDetails::join('visit_stores', "visit_details.visit_store_id", "=", "visit_stores.id")
                                    ->join('stores', "visit_stores.store_id", "=", "stores.id")
                                    ->where("visit_details.employee_id", $this->user->id)
                                    ->whereIn("visit_details.status", [
                                        VisitDetails::$statuses["UNFINISHED"],
                                        VisitDetails::$statuses["FINISHED"]
                                    ]);
                                    if ($request->input('date')) {
                                       $visits->where('visit_details.start_date', $request->input('date'));
                                    }
                                    if ($city = $request->input('city')) {
                                       $visits->join('cities',function($query) use($city){
                                           $query->on('cities.id', '=', 'stores.city_id')
                                                ->where('stores.city_id', $city);
                                       });
                                    }
                                    if ($category = $request->input('category')) {
                                        $visits->join('categories',function($query) use($category){
                                           $query->on('categories.id', '=', 'stores.category_id')
                                                ->where('stores.category_id', $category);
                                        });
                                    }
                                    if ($code = $request->input('code')) {
                                       $visits->where('visit_details.code',$code);
                                    }
            $visits =  $visits->select('visit_details.*','stores.title_ar as store')
                                ->orderBy('stores.id')
                                ->paginate($this->limit);
            
            $visitsHtml = $this->getVisitsHtml($visits);
           return _json('success',['visits' => $visitsHtml,'count' => $visits->count()]);
        } catch (\Exception $ex) {
            $message = 'لقد حدث خطأ ما !';
            return _json('error', $message, 400);
        }
    }

    private function getVisitsHtml($visits)
    {
        $html = '';

        foreach ($visits as $visit) {
            $html .=    '<li>
                            <a href="'. route('employee.visits.show',['code' => $visit->code]) . '">
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col2">
                                            <div class="desc">
                                                '.$visit->store .' - '.$visit->code.'
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="date">
                                        <span class="label label-sm label-default">
                                            <i class="fa fa-angle-left"></i>
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </li>';
        }
        return $html;
    }
}
