<?php

namespace App\Http\Controllers\Employee\Auth;

use DB;
use Auth;
use App\User;
use Validator;
use App\Scopes\AgentScope;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    private $step_one_rules = array(
        'mobile' => 'required'
    );

    private $step_two_rules = array(
        'code.*' => 'required'
    );

    private $step_three_rules = array(
        'mobile' => 'required',
        'password' => 'required',
        'password_confirmation' => 'required|same:password',
    );

    private $resend_rules = array(
        'mobile' => 'required'
    );
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:employee');
    }

    public function showPasswordResetForm()
    {
        return view('main_content.employee.auth.forget');
    }

    public function reset(Request $request)
    {
        try {
            if ($request->input('step') == 1) {
                $rules = $this->step_one_rules;
            } else if ($request->input('step') == 2) {
                $rules = $this->step_two_rules;
            } else if ($request->input('step') == 3) {
                $rules = $this->step_three_rules;
            }

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            

            $mobile = $request->input('mobile');
            $employee = $this->checkAuth($mobile);
            if ($employee) {
                if ($request->input('step') == 1) {
                    if ($employee) {
                        $code = 1234;
                        //$code = Random(4);
                        // send code sms
                        session()->put('anabile_vfcode', $code);
                        return _json('success', '');
                    }
                } else if ($request->input('step') == 2) {
                    $code = session()->get('anabile_vfcode');
                    if ($code == implode("", $request->input('code'))) {
                        return _json('success', '');
                    }
                    $message = 'الكود الذي تم ادخاله غير صحيح';
                    return _json('error', $message);
                } else if ($request->input('step') == 3) {
                    $employee->password = bcrypt($request->input('password'));
                    $employee->save();
                    return _json('success',  route('employee.login'));
                }
            }else{
                $message = 'البيانات المدخلة غير صحيحة';
                return _json('error', $message);
            }
           
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'لقد حدث خطأ ما , أعد المحاولة لاحقاً';
            return _json('error', $message, 400);
        }
    }

    public function resendCode(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->resend_rules);
            if ($validator->fails()) {
                $message = 'من فضلك قم بإدخال رقم هاتفك';
                return _json('error', $message, 400);
            }

            $mobile = $request->input('mobile');
            $employee = $this->checkAuth($mobile);
            if ($employee) {
                $code = 4567;
                //$code = Random(4);
                session()->put('anabile_vfcode', $code);
                //send code sms
                
                $message = 'لقد تم إرسال كود جديد إلي رقم هاتفك';
                return _json('success', $message);
            }else{
                $message = 'البيانات المدخلة غير صحيحة';
                return _json('error', $message, 400);
            }
           
        } catch (\Exception $ex) {
            $message = 'لقد حدث خطأ ما , أعد المحاولة لاحقاً';
            return _json('error', $message, 400);
        }
       
    }

    private function checkAuth($mobile)
    {
        $employee = User::where('type', AgentScope::$typeOfAgent)
            ->where('mobile', $mobile)
            ->where('active', 1)
            ->first();
        return $employee ?: false;
    }

}
