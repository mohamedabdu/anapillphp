<?php

namespace App\Http\Controllers\Employee\Auth;

use DB;
use Auth;
use Validator;
use App\User;
use App\Scopes\AgentScope;
use Illuminate\Http\Request;
use App\Models\UserAttendance;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use function Helper\Common\base64ToImage;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/employee';

    private $step_one_rules = array(
        'mobile' => 'required',
        'password' => 'required'
    );
    
    private $rules = array(
        'mobile' => 'required',
        'password' => 'required',
        'image' => 'required',
        'lat' => 'required',
        'lng' => 'required'
    );


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:employee', ['except' => ['logout']]);
    }

    public function showLoginForm()
    {
        return view('main_content.employee.auth.login');
    }

    public function login(Request $request)
    {
        try {
                if ($request->input('step') == 1) {
                    $rules = $this->step_one_rules;
                } else if($request->input('step') == 3) {
                    $rules = $this->rules;
                }
                if ( in_array($request->input('step'),[1,3])) {
                    $validator = Validator::make($request->all(), $rules);
                    if ($validator->fails()) {
                        $errors = $validator->errors()->toArray();
                        return _json('error', $errors);
                    }
                }
                
                $mobile = $request->input('mobile');
                $password = $request->input('password');
                $employee = $this->checkAuth($mobile);

                if ($employee) {
                    //check today in employee holiday if exist 
                    if ($employee->holiday_start && $employee->holiday_end) {
                        $today = strtotime(date('Y-m-d'));
                        if ($today >= strtotime($employee->holiday_start) && $today <= strtotime($employee->holiday_end)) {
                            $message = ' لا يمكنك الدخول إلي لوحة التحكم الخاصة بك اليوم حيث انه ضمن ايام إجازتك التي سوف تنتهي يوم '. $employee->holiday_end;
                            return _json('error', $message);
                        }
                    }

                    // check employee work days to allow him to login to the system
                    $today = date('D');
                    if (!in_array($today,json_decode($employee->work_days))) {
                        $message = 'لا يمكنك الدخول إلي لوحة التحكم الخاصة بك اليوم حيث انه ليس ضمن ايام عملك';
                        return _json('error', $message);
                    }

                    //check employee whether finished his attendace before or note
                    if ($this->checkUserFinishAttendance($employee)) {
                       $message = 'لقد قمت بإنهاء دوامك اليوم! لا يمكنك تسجيل الدخول مرة أخرى';
                        return _json('error', $message);
                    }
                    
                    if ($request->input('step') == 1) {
                        if (password_verify($password, $employee->password)) {
                            if ($check = $this->checkUserAttendance($employee)) {
                                Auth::guard('employee')->login($employee);
                                return _json('success', ['redirect' => route('employee.dashboard')]);
                            }
                            return _json('success', $employee->name);
                        }
                        $message = 'البيانات المدخلة غير صحيحة';
                        return _json('error', $message);
                    } else if ($request->input('step') == 3) {
                        $message = date('Y-m-d H:i');
                        return _json('success', $message);
                    } else if ($request->input('step') == 4) {
                        $this->createUserAttendance($request, $employee);
                        Auth::guard('employee')->login($employee);
                        return _json('success',  route('employee.dashboard'));
                    }
                }else{
                    $message = 'البيانات المدخلة غير صحيحة';
                    return _json('error', $message);
                }
                     
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'لقد حدث خطأ ما , أعد المحاولة لاحقاً';
            return _json('error', $message, 400);
        }
        
    }

    private function checkAuth($mobile)
    {
        $employee = User::leftJoin('employee_holidays',function($query){
                            $query->on('users.id','=', 'employee_holidays.employee_id')
                            ->where('employee_holidays.status',1)
                            ->where('employee_holidays.id',DB::raw('(select max(id) from employee_holidays where employee_holidays.employee_id = users.id)'));
                        })
                        ->where('users.type', AgentScope::$typeOfAgent)
                        ->where('users.mobile',$mobile)
                        ->where('users.active',1)
                        ->select('users.*', 'employee_holidays.start_date as holiday_start' ,'employee_holidays.end_date as holiday_end')
                        ->first();
        return $employee ?: false;
    }

    private function createUserAttendance($request,$user)
    {
        $check = $this->checkUserAttendance($user);
        if (!$check) {
            $userAttendance = new UserAttendance();
            $userAttendance->user_id = $user->id;
            $userAttendance->start_date = date('Y-m-d H:i:s');
            $userAttendance->image =  base64ToImage(str_replace('data:image/jpeg;base64,', '', $request->input('image')));
            $userAttendance->lat = $request->input('lat');
            $userAttendance->lng = $request->input('lng');
            $userAttendance->save();
        }
    }

    private function checkUserAttendance($user)
    {
        $userAttendance = UserAttendance::where('user_id', $user->id)->where('is_end', false)->first();
        return $userAttendance ? : false;
    }

    private function checkUserFinishAttendance($user)
    {
        $userAttendance = UserAttendance::where('user_id', $user->id)
                                        ->whereDate('end_date',date('Y-m-d'))
                                        ->where('is_end', true)
                                        ->first();
        return $userAttendance ? true : false;
    }

    private function closeUserAttendance($request)
    {
        $userAttendance =  UserAttendance::where('user_id', Auth::guard('employee')->user()->id)->where('is_end',false)->first();
        $userAttendance->end_date = date('Y-m-d H:i:s');
        $userAttendance->is_end = true;
        if ($request->input('completion') == "false") {
            $userAttendance->termination_reason = $request->input('termination_reason');
        }else{
            $userAttendance->status = true;
        }
        $userAttendance->save();
    }

    public function logout(Request $request)
    {
       try {
            $this->closeUserAttendance($request);
            Auth::guard('employee')->logout();
            $redirect = route('employee.login');
            return _json('success', $redirect);
       } catch (\Exception $ex) {
            $message = 'لقد حدث خطأ ما , أعد المحاولة لاحقاً';
            return _json('error', $message, 400);
       }
       
    }
}
