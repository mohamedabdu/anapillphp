<?php

namespace App\Http\Controllers\Employee;

use App\Models\Task;
use App\Models\Visit;
use App\Models\VisitCancel;
use App\Models\VisitDetails;
use App\Models\VisitTask;
use App\Models\VisitTaskCancel;
use App\Models\VisitTaskDetails;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Validator;

class VisitsController extends EmployeeController
{
    private $add_task_rules = [
        'task' => 'required',
        'code' => 'required'
    ];

    private $cancel_visit_rules = [
        'code' => 'required',
        'cancellation_reason' => 'required'
    ];

    private $cancel_task_rules = [
        'code' => 'required',
        'task' => 'required',
        'cancellation_reason' => 'required'
    ];

    private $finish_visit_rules = [
        'code' => 'required',
        'end_date' => 'required',
        'end_time' => 'required',
        'end_lat' => 'required',
        'end_lng' => 'required',
        'completed' => 'required'
    ];
    
    private $tasks_routes = [
        1 => 'employee.tasks.organizing_shelves',
        2 => 'employee.tasks.returns',
        3 => 'employee.tasks.general_inventory',
        4 => 'employee.tasks.detailed_inventory',
        5 => 'employee.tasks.price_report',
        6 => 'employee.tasks.areas_report',
        7 => 'employee.tasks.receiving_order',
        8 => 'employee.tasks.check_promotions'
    ];
    
    public function show($code)
    {
        try {

            $visit_details = VisitDetails::join('visit_stores', 'visit_stores.id','=', 'visit_details.visit_store_id')
                                ->join('stores','stores.id','=', 'visit_stores.store_id')
                                ->leftJoin('visit_cancels', function($query){
                                    $query->on('visit_cancels.visit_details_id', '=', 'visit_details.id')
                                        ->where('visit_cancels.id','=',DB::raw('(select max(visit_cancels.id) from visit_cancels where visit_cancels.visit_details_id = visit_details.id)'));
                                })
                                ->where('visit_details.code',$code)
                                ->where('visit_details.employee_id',$this->user->id)
                                ->select(
                                        'visit_details.*',
                                        'visit_cancels.id as visit_cancel',
                                        'visit_cancels.is_agree as visit_cancel_status',
                                        'stores.lat',
                                        'stores.lng',
                                        'stores.title_ar as store',
                                        DB::raw('(select vt2.start_date from visit_details as vt2 where vt2.visit_store_id = visit_details.visit_store_id and vt2.start_date < "'.date('Y-m-d').'" and vt2.status = '.VisitDetails::$statuses['FINISHED'].' order by vt2.id desc limit 1) as last_visit_date'),
                                        DB::raw('(select count(*) from visit_task_details where visit_task_details.visit_details_id = visit_details.id) as tasks_count'),
                                        DB::raw('(select count(*) from visit_task_details where visit_task_details.visit_details_id = visit_details.id and visit_task_details.status = '.VisitTaskDetails::$statuses['FINISHED'].') as finished_tasks_count'),
                                        DB::raw('(select count(*) from visit_task_details where visit_task_details.visit_details_id = visit_details.id and visit_task_details.status = '.VisitTaskDetails::$statuses['CANCELLED'].') as cancelled_tasks_count')
                                    )
                                ->first();
       
           
            if (!$visit_details) {
                return $this->err404();
            }
            

            // the visit has started and its tasks are completed
            $completed = ($visit_details->start_time && ($visit_details->tasks_count - $visit_details->cancelled_tasks_count) == $visit_details->finished_tasks_count ) ? true : false;

            if ($completed || $visit_details->status == VisitDetails::$statuses['FINISHED'] || ( $visit_details->status == VisitDetails::$statuses['UNFINISHED'] &&  $visit_details->end_time)) {
               return redirect()->route('employee.visits.report',['code' => $code]);
            }

            $visit_tasks = VisitTask::join('tasks',function($query) use($visit_details){
                                            $query->on('visit_tasks.task_id', '=', 'tasks.id')
                                                ->where('visit_tasks.visit_id', $visit_details->visit_id)
                                                ->where(function($query) use($visit_details){
                                                    $query->whereNull('visit_details_id')
                                                          ->orWhere('visit_details_id', $visit_details->id);
                                                });
                                        })
                                        ->leftJoin('visit_task_cancels', function($query) use($visit_details){
                                            $query->on('visit_task_cancels.task_id', '=', 'visit_tasks.task_id')
                                            ->where('visit_task_cancels.visit_details_id', $visit_details->id)
                                            ->where('visit_task_cancels.id',DB::raw('(select max(id) from visit_task_cancels as vtc2 where vtc2.task_id = visit_task_cancels.task_id and vtc2.visit_details_id = '. $visit_details->id .' )'));
                                        })
                                        ->leftJoin('visit_task_details', function($query) use($visit_details){
                                            $query->on('visit_task_details.task_id', '=', 'visit_tasks.task_id')
                                            ->where('visit_task_details.visit_details_id', $visit_details->id);
                                        })
                                        ->select(
                                                'tasks.process',
                                                'tasks.title_ar as title',
                                                'visit_task_cancels.id as visit_task_cancel',
                                                'visit_task_cancels.is_agree as visit_task_cancel_status',
                                                'visit_task_details.start_date',
                                                'visit_task_details.status'
                                            )
                                            ->orderBy('tasks.process')
                                        ->get();
            
                                        
            if ($visit_details->last_visit_date) {
                $visit_details->last_visit_date = Carbon::parse($visit_details->last_visit_date)->format('l, d F Y');
            }
            
            $remaining_tasks = Task::whereNotIn('process', $visit_tasks->pluck('process')->toArray())
                                    ->select('process','title_ar as title')
                                    ->get();
            
            $this->data['tasks_routes'] = $this->tasks_routes;
            $this->data['visit'] = $visit_details;
            $this->data['visit_tasks'] = $visit_tasks;
            $this->data['remaining_tasks'] = $remaining_tasks;
            if ($visit_details->start_time) {
                $this->data['start_time'] = (time() - strtotime($visit_details->start_date.' '.$visit_details->start_time)) * 1000;
            }

            return $this->_view("visits.show");
        } catch (\Exception $ex) {
            return $this->err404();
        }
        
    }

    public function showVisitReport(Request $request,$code)
    {
       try {
           
            $visit_details = VisitDetails::join('visit_stores', 'visit_stores.id','=', 'visit_details.visit_store_id')
                                        ->join('stores','stores.id','=', 'visit_stores.store_id')
                                        ->where('visit_details.code', $code)
                                        ->where('visit_details.employee_id', $this->user->id)
                                        ->whereIn('visit_details.status', [
                                                    VisitDetails::$statuses['UNFINISHED'],
                                                    VisitDetails::$statuses['FINISHED']
                                        ])
                                        ->whereNotNull('visit_details.start_time')
                                        ->select('visit_details.*', 'stores.title_ar as store')
                                        ->first();
            if (!$visit_details) {
                return $this->err404();
            }
            if (!$visit_details->end_time) {
                $this->data['finished_uncompleted'] = false;
            }else{
                $this->data['finished_uncompleted'] = true;
            }

            if ($visit_details->status == VisitDetails::$statuses['UNFINISHED'] && !$visit_details->end_time) {
                $visit_details->end_date = date('Y-m-d');
                $visit_details->end_time = date('H:i:s');
            }
            $start_time = new \DateTime($visit_details->start_date . ' ' . $visit_details->start_time);
            $end_time   = new \DateTime($visit_details->end_date . ' ' . $visit_details->end_time);

            $visit_details->total_time = $start_time->diff($end_time)->format("%H:%I:%S");

            $visit_tasks = VisitTaskDetails::join('tasks',function($query) use($visit_details){
                                                $query->on('tasks.process', '=', 'visit_task_details.task_id')
                                                    ->where('visit_task_details.visit_details_id', $visit_details->id);
                                            })
                                            ->select('visit_task_details.*','tasks.title_ar as task')
                                            ->get();
                                            
            $this->data['finished_tasks'] = [];
            $this->data['cancelled_tasks'] = [];
            $this->data['unfinished_tasks'] = [];
            foreach ($visit_tasks as $task) {
                if ($task->status == VisitTaskDetails::$statuses['FINISHED']) {
                    $this->data['finished_tasks'][] = $task;
                }else if($task->status == VisitTaskDetails::$statuses['CANCELED']){
                    $this->data['cancelled_tasks'][] = $task;
                }
                else if($task->status == VisitTaskDetails::$statuses['UNFINISHED']){
                    $this->data['unfinished_tasks'][] = $task;
                }
            }
            $visit_details->completion_percentage = round((count($this->data['finished_tasks']) / ($visit_tasks->count() - count($this->data['cancelled_tasks']))) * 100,0);
            $this->data['visit'] = $visit_details;
            return $this->_view('visits.report');
       } catch (\Exception $ex) {
            return $this->err404();
        }
    }

    public function startVisit(Request $request)
    {
        try {
            $visit_details = VisitDetails::leftJoin('visit_cancels', function($query){
                                             $query->on('visit_cancels.visit_details_id', '=', 'visit_details.id')
                                                    ->where('visit_cancels.id','=',DB::raw('(select max(visit_cancels.id) from visit_cancels where visit_cancels.visit_details_id = visit_details.id)'));
                                        })
                                        ->where('visit_details.code', $request->input('code'))
                                        ->where('visit_details.employee_id', $this->user->id)
                                        ->where('visit_details.start_date', date('Y-m-d'))
                                        ->where('visit_details.status',VisitDetails::$statuses['UNFINISHED'])
                                        ->whereNull('visit_details.start_time')
                                        ->select('visit_details.*',
                                                 'visit_cancels.id as visit_cancel',
                                                 'visit_cancels.is_agree as visit_cancel_status')
                                        ->first();
            if (!$visit_details) {
                $message = 'هذه الزيارة غير موجودة';
                return _json('error', $message, 404);
            }
            
            //check visit details cancellation status
            if ($visit_details->visit_cancel && $visit_details->visit_cancel == VisitCancel::$statuses['pending']) {
                $message = 'لا يمكنك بدء هذه الزيارة';
                return _json('error', $message, 400);
            }
            DB::beginTransaction();
            // update visit details starting data
            $visit_details->start_lat = $request->input('lat');
            $visit_details->start_lng = $request->input('lng');
            $visit_details->start_time = date('H:i:s');
            $visit_details->save();

            //add visit tasks details
            $visits_tasks = VisitTask::where('visit_id', $visit_details->visit_id)->get();
            $visit_task_details = [];
            foreach ($visits_tasks as $task) {
                $visit_task_details[] = [
                    'visit_details_id' =>  $visit_details->id,
                    'task_id' => $task->task_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
            VisitTaskDetails::insert($visit_task_details);

            //check to add the next visit details
            $visit = Visit::find($visit_details->visit_id);
            
            if ($visit->type != Visit::$types['normal']) {
                if ($visit->on_going || (!$visit->on_going && ($visit->num_of_visits_counter + 1) <= $visit->num_of_visits  ) ) 
                {
                    $visitNextDate = date("Y-m-d", strtotime($visit_details->start_date . "+$visit->repeat_every day"));
                    $day = date("D", strtotime($visitNextDate));
                    while (in_array($day, json_decode($visit->holidays))) {
                        $visitNextDate = date("Y-m-d", strtotime($visitNextDate . "+1 day"));
                        $day = date("D", strtotime($visitNextDate));
                    }
                    // add the upcoming visit
                    $upcoming_visit = new VisitDetails();
                    $upcoming_visit->code = $this->getVisitCode();
                    $upcoming_visit->employee_id = $this->user->id;
                    $upcoming_visit->visit_id = $visit->id;
                    $upcoming_visit->visit_store_id = $visit_details->visit_store_id;
                    $upcoming_visit->start_date = $visitNextDate;
                    $upcoming_visit->save();
                    
                    if (!$visit->on_going) {
                        $visit->num_of_visits_counter += 1;
                        $visit->save();
                    }
                }
            }
            DB::commit();
           return _json('success');
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'لقد حدث خطأ ما !';
            return _json('error',$message,400);
        }
    }

    public function cancelVisit(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->cancel_visit_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }

            $visit_details = VisitDetails::where('visit_details.code', $request->input('code'))
                                        ->where('visit_details.employee_id', $this->user->id)
                                        ->where('visit_details.start_date', date('Y-m-d'))
                                        ->where('visit_details.status', VisitDetails::$statuses['UNFINISHED'])
                                        ->whereNull('visit_details.start_time')
                                        ->select('visit_details.*')
                                        ->first();
            if (!$visit_details) {
                $message = 'هذه الزيارة غير موجودة';
                return _json('error', $message, 404);
            }

            $checkCancellation = VisitCancel::where('employee_id',$this->user->id)
                                            ->where('visit_details_id', $visit_details->id)
                                            ->where('is_agree', VisitCancel::$statuses['pending'])
                                            ->first();
            if ($checkCancellation) {
                $message = 'لقد قمت بإرسال طلب إلغاء لهذه الزيارة من قبل';
                return _json('error', $message, 404);
            }

            $visit_cancel = new VisitCancel();
            $visit_cancel->employee_id = $this->user->id;
            $visit_cancel->visit_details_id = $visit_details->id;
            $visit_cancel->cancellation_reason = $request->input('cancellation_reason');
            $visit_cancel->save();

            $message = 'تم إرسال طلب الإلغاء بنجاح';
            return _json('success',  $message);

        } catch (\Exception $ex) {
            $message = 'لقد حدث خطأ ما !';
            return _json('error', $message, 400);
        }
    }

    public function finishVisit(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->finish_visit_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
           
            $visit_details = VisitDetails::where('visit_details.code', $request->input('code'))
                                        ->where('visit_details.employee_id', $this->user->id)
                                        ->where('visit_details.start_date', date('Y-m-d'))
                                        ->where('visit_details.status', VisitDetails::$statuses['UNFINISHED'])
                                        ->whereNotNull('visit_details.start_time')
                                        ->whereNull('visit_details.end_time') // to check if it was finished from another place
                                        ->select('visit_details.*')
                                        ->first();
            if (!$visit_details) {
                $message = 'هذه الزيارة غير موجودة';
                return _json('error', $message, 404);
            }
            $visit_details->end_date = $request->input('end_date');
            $visit_details->end_time = $request->input('end_time');
            $visit_details->end_lat = $request->input('end_lat');
            $visit_details->end_lng = $request->input('end_lng');
            $completed = $request->input('completed') == "true" ? true : false;
            if ($completed) {
                $visit_details->status = VisitDetails::$statuses['FINISHED'];
            }else{
                $visit_details->termination_reason = $request->input('termination_reason');
            }
            $visit_details->save();
            $redirect = route('employee.dashboard');
            return _json('success', $redirect);

        } catch (\Exception $ex) {
            $message = 'لقد حدث خطأ ما !';
            return _json('error', $message, 400);
        }
    }

    public function addTask(Request $request)
    {
       try {
            $validator = Validator::make($request->all(), $this->add_task_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
           
            $visit_details = VisitDetails::where('visit_details.code', $request->input('code'))
                                        ->where('visit_details.employee_id', $this->user->id)
                                        ->where('visit_details.start_date', date('Y-m-d'))
                                        ->where('visit_details.status', VisitDetails::$statuses['UNFINISHED'])
                                        ->whereNotNull('visit_details.start_time')
                                        ->select('visit_details.*')
                                        ->first();
            if (!$visit_details) {
                $message = 'هذه الزيارة غير موجودة';
                return _json('error', $message, 404);
            }

            $checkTask = VisitTask::where('task_id',$request->input('task'))
                                    ->where('visit_id', $visit_details->visit_id)
                                     ->where(function($query) use($visit_details){
                                        $query->whereNull('visit_details_id')
                                              ->orWhere('visit_details_id', $visit_details->id);
                                    })
                                    ->first();
            if ($checkTask) {
                $message = 'هذه المهمة موجودة بالفعل فى الزيارة الحالية';
                return _json('error', $message, 404);
            }

            $visit_task = new VisitTask();
            $visit_task->visit_id = $visit_details->visit_id;
            $visit_task->task_id = $request->input('task');
            $visit_task->visit_details_id = $visit_details->id;
            $visit_task->save();

            // add visit task details
            $visit_task_details = new VisitTaskDetails();
            $visit_task_details->visit_details_id = $visit_details->id;
            $visit_task_details->task_id = $request->input('task');
            $visit_task_details->save();

            $task = VisitTask::join('tasks','tasks.id','=','visit_tasks.task_id')
                                ->where('visit_tasks.visit_details_id', $visit_task->visit_details_id)
                                ->where('visit_tasks.task_id', $visit_task->task_id)
                                ->select('tasks.title_ar as title', 'tasks.process')
                                ->first();
            $taskHtml = $this->getTaskHtml($task, $visit_details->code);
            $message = 'تم اضافة المهمة بنجاح';
            return _json('success',['message' => $message,'task' => $taskHtml]);
       } catch (\Exception $ex) {
            DB::rollback();
            $message = 'لقد حدث خطأ ما !';
            return _json('error', $message, 400);
        }
    }

    public function cancelTask(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), $this->cancel_task_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
           
            $visit_details = VisitDetails::where('visit_details.code', $request->input('code'))
                                        ->where('visit_details.employee_id', $this->user->id)
                                        ->where('visit_details.start_date', date('Y-m-d'))
                                        ->where('visit_details.status', VisitDetails::$statuses['UNFINISHED'])
                                        ->whereNotNull('visit_details.start_time')
                                        ->select('visit_details.*')
                                        ->first();
            if (!$visit_details) {
                $message = 'هذه الزيارة غير موجودة';
                return _json('error', $message, 404);
            }

            $checkCancellation = VisitTaskCancel::where('visit_details_id', $visit_details->id)
                                                ->where('task_id', $request->input('task'))
                                                ->where('is_agree',VisitTaskCancel::$statuses['pending'])
                                                ->first();
            if ($checkCancellation) {
                $message = 'لقد قمت بإرسال طلب إلغاء لهذه المهمة من قبل';
                return _json('error', $message, 404);
            }

            $visit_task_cancel = new VisitTaskCancel();
            $visit_task_cancel->employee_id = $this->user->id;
            $visit_task_cancel->task_id = $request->input('task');
            $visit_task_cancel->visit_details_id = $visit_details->id;
            $visit_task_cancel->cancellation_reason = $request->input('cancellation_reason');
            $visit_task_cancel->save();

            $message = 'تم إرسال طلب الإلغاء بنجاح';
            return _json('success',  $message);

        } catch (\Exception $ex) {
            $message = 'لقد حدث خطأ ما !';
            return _json('error', $message, 400);
        }
    }

    

    private function getTaskHtml($task,$code)
    {
        return $html =      '<li class="pt-1">
                                <div class="col-lg-8 col-md-6 col-xs-8 col-sm-8">
                                    <div class="cont">
                                        <div class="cont-col2">
                                            <div class="desc">
                                                '.$task->title. '
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-xs-4 col-sm-4 task-actions">
                                    <span id="cancel-message" style="display:none;">تم ارسال طلب إلغاء</span>
                                    <button onclick="Visits.startTask(this)" data-task ="'.$task->process .'" data-href="'.route($this->tasks_routes[$task->process],['code' => $code]).'" class="btn btn-sm btn-primary" id="start-task">
                                        بدء
                                    </button>
                                    <button href="#" class="btn btn-sm btn-default" onclick="Visits.cancelTask(this)" data-task ="'.$task->process.'">
                                        إلغاء
                                    </button>
                                </div>
                            </li>';
            
    }

    private function getVisitCode()
    {
        $maxID = VisitDetails::max("id");
        $maxID += 1;
        $id = "0000" . $maxID;
        if ($maxID > 9999 && $maxID < 99999) {
            $id = "000" . $maxID;
        } else if ($maxID > 99999 && $maxID < 999999) {
            $id = "00" . $maxID;
        } else if ($maxID > 999999 && $maxID < 9999999) {
            $id = "0" . $maxID;
        } else if ($maxID > 9999999) {
            $id = $maxID;
        }
        return $id;
    }

    
}
