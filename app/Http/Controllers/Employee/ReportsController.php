<?php

namespace App\Http\Controllers\Employee;

use DB;
use Validator;
use App\Models\Promo;
use App\Models\Store;
use App\Models\Report;
use App\Models\Product;
use App\Models\Competitor;
use Illuminate\Http\Request;
use App\Models\CompetitorReport;
use function Helper\Common\base64ToImage;
use App\Models\CompetitorReportAreasCategory;
use App\Models\CompetitorReportPromotionsPromos;
use App\Models\ReportProductsAlmostFinishedProduct;
use App\Models\CompetitorReportReducedPricesProduct;
use App\Models\CompetitorReportPriceComparisonProduct;


class ReportsController extends EmployeeController
{
    private $products_almost_finish_rules = [
        'store' => 'required',
        'products.*.product' => 'required',
        'products.*.pieces_number' => 'required',
        'products.*.date' => 'required'
    ];
    
    private $manual_rules = [
        'store' => 'required',
        'title' => 'required',
        'notes' => 'required'
    ];

    private $price_comparison_rules = [
        'store' => 'required',
        'competitor' => 'required',
        'products.*.product' => 'required',
        'products.*.organization_product_price' => 'required',
        'products.*.competitor_product_price' => 'required'
    ];
   
    private $promotions_rules = [
        'store' => 'required',
        'competitor' => 'required',
        'promos.*.promo' => 'required',
        'promos.*.promo_count' => 'required',
        'promos.*.image' => 'required'
    ];

    private $areas_rules = [
        'store' => 'required',
        'competitor' => 'required',
        'categories.*.category' => 'required',
        'categories.*.width_of_organization' => 'required',
        'categories.*.height_of_organization' => 'required',
        'categories.*.width_of_competitor' => 'required',
        'categories.*.height_of_competitor' => 'required'
    ];

    private $reduced_prices_rules = [
        'store' => 'required',
        'competitor' => 'required',
        'products.*.promo' => 'required',
        'products.*.current_price' => 'required',
        'products.*.offer_price' => 'required'
    ];

    private $ordinary_rules = [
        'store' => 'required',
        'competitor' => 'required',
        'title' => 'required',
        'notes' => 'required'
    ];
    

    
    public function showProductsAlmostFinishForm()
    {
        $this->data['stores'] = Store::select('id','title_ar as title')->get();
        $this->data['products'] = Product::whereNull('competitor_id')->select('id', 'title_ar as title')->get();
        return $this->_view("reports.products_almost_finish");
    }

    public function storeProductsAlmostFinishReport(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->products_almost_finish_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            DB::beginTransaction();
            $report = new Report();
            $report->type = Report::$types['products_almost_finish'];
            $report->store_id = $request->input('store');
            $report->employee_id = $this->user->id;
            if ($request->input('notes')) {
                $report->notes = $request->input('notes');
            }
            if ($request->input('images')) {
                $images = [];
                foreach ($request->input('images') as $image) {
                    if ($image != null) {
                        $images[] = base64ToImage(str_replace('data:image/jpeg;base64,', '', $image));
                    }
                }
                if (!empty($images)) {
                    $report->images = json_encode($images);
                }
            }
            $report->save();

            $products = [];
            foreach ($request->input('products') as $product) {
               $products[] = [
                    'product_id' => $product['product'],
                    'pieces_number' => $product['pieces_number'],
                    'cartons_number' => $product['cartons_number'],
                    'date' =>  $product['date'],
                    'report_id' => $report->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
               ];
            }

            ReportProductsAlmostFinishedProduct::insert($products);
            DB::commit();
            $message = 'تم إرسال التقرير بنجاح';
            return _json('success', $message);
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'حدث خطأ ما!';
            return _json('error', $message, 400);
        }

    }

    public function showManualReportForm()
    {
        $this->data['stores'] = Store::select('id', 'title_ar as title')->get();
        return $this->_view("reports.manual");
    }

    public function storeManualReport(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->manual_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            $report = new Report();
            $report->type = Report::$types['manual'];
            $report->store_id = $request->input('store');
            $report->employee_id = $this->user->id;
            $report->title = $request->input('title');
            $report->notes = $request->input('notes');
            if ($request->input('images')) {
                $images = [];
                foreach ($request->input('images') as $image) {
                    if ($image != null) {
                        $images[] = base64ToImage(str_replace('data:image/jpeg;base64,', '', $image));
                    }
                }
                if (!empty($images)) {
                    $report->images = json_encode($images);
                }
            }
            $report->save();
            $message = 'تم إرسال التقرير بنجاح';
            return _json('success', $message);
        } catch (\Exception $ex) {
            $message = 'حدث خطأ ما!';
            return _json('error', $message, 400);
        }
    }

    public function showPriceComparisonReportForm()
    {
        $this->data['stores'] = Store::select('id', 'title_ar as title')->get();
        $this->data['competitors'] = Competitor::select('id', 'title_ar as title')->get();
        $this->data['products'] = Product::whereNull('competitor_id')->select('id', 'title_ar as title')->get();        
        return $this->_view("reports.price_comparison");
    }

    public function storePriceComparisonReport(Request $request)
    {
        try {
           
            $validator = Validator::make($request->all(), $this->price_comparison_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }

            DB::beginTransaction();
            $report = new CompetitorReport();
            $report->type = CompetitorReport::$types['price_comparison'];
            $report->store_id = $request->input('store');
            $report->competitor_id = $request->input('competitor');
            $report->employee_id = $this->user->id;

            if ($request->input('notes')) {
                $report->notes = $request->input('notes');
            }
            if ($request->input('images')) {
                $images = [];
                foreach ($request->input('images') as $image) {
                    if ($image != null) {
                        $images[] = base64ToImage(str_replace('data:image/jpeg;base64,', '', $image));
                    }
                }
                if (!empty($images)) {
                    $report->images = json_encode($images);
                }
            }
            $report->save();

            $products = [];
            foreach ($request->input('products') as $product) {
                $products[] = [
                    'product_id' => $product['product'],
                    'competitor_product_id' =>  $product['competitor_product'],
                    'product_name' => $product['competitor_product_name'],
                    'organization_product_price' => $product['organization_product_price'],
                    'competitor_product_price' => $product['competitor_product_price'],
                    'competitors_report_id' => $report->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }

            CompetitorReportPriceComparisonProduct::insert($products);
           
            DB::commit();
            $message = 'تم إرسال التقرير بنجاح';
            return _json('success', $message);
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'حدث خطأ ما!';
            return _json('error', $message, 400);
        }
    }

    public function showPromotionsReportForm()
    {
        $this->data['stores'] = Store::select('id', 'title_ar as title')->get();
        $this->data['competitors'] = Competitor::select('id', 'title_ar as title')->get();
        $this->data['promotions'] = Promo::select('id', 'title_ar as title')->get();
        return $this->_view("reports.promotions");
    }

    public function storePromotionsReport(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->promotions_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }

            DB::beginTransaction();
            $report = new CompetitorReport();
            $report->type = CompetitorReport::$types['promotions'];
            $report->store_id = $request->input('store');
            $report->competitor_id = $request->input('competitor');
            $report->employee_id = $this->user->id;

            if ($request->input('notes')) {
                $report->notes = $request->input('notes');
            }
            if ($request->input('images')) {
                $images = [];
                foreach ($request->input('images') as $image) {
                    if ($image != null) {
                        $images[] = base64ToImage(str_replace('data:image/jpeg;base64,', '', $image));
                    }
                }
                if (!empty($images)) {
                    $report->images = json_encode($images);
                }
            }
            $report->save();

            $promos = [];
            foreach ($request->input('promos') as $promo) {
                $promos[] = [
                    'promo_id' => $promo['promo'],
                    'number' => $promo['promo_count'],
                    'notes' => $promo['notes'] ? $promo['notes']: '',
                    'image' => base64ToImage(str_replace('data:image/jpeg;base64,', '', $promo['image'])),
                    'competitors_report_id' => $report->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
            CompetitorReportPromotionsPromos::insert($promos);

            DB::commit();
            $message = 'تم إرسال التقرير بنجاح';
            return _json('success', $message);
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'حدث خطأ ما!';
            return _json('error', $message, 400);
        }
    }

    public function showAreasReportForm()
    {
        $this->data['stores'] = Store::select('id', 'title_ar as title')->get();
        $this->data['competitors'] = Competitor::select('id', 'title_ar as title')->get();
        return $this->_view("reports.areas");
    }

    public function storeAreasReport(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->areas_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            DB::beginTransaction();
            $report = new CompetitorReport();
            $report->type = CompetitorReport::$types['areas'];
            $report->store_id = $request->input('store');
            $report->competitor_id = $request->input('competitor');
            $report->employee_id = $this->user->id;
            $report->save();

            $categories = [];
            foreach ($request->input('categories') as $category) {
                $categories[] = [
                    'category_id' => $category['category'],
                    'width_of_organization' =>  $category['width_of_organization'],
                    'height_of_organization' => $category['height_of_organization'],
                    'width_of_competitor' => $category['width_of_competitor'],
                    'height_of_competitor' => $category['height_of_competitor'],
                    'competitors_report_id' => $report->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
            CompetitorReportAreasCategory::insert($categories);

            DB::commit();
            $message = 'تم إرسال التقرير بنجاح';
            return _json('success', $message);
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'حدث خطأ ما!';
            return _json('error', $message, 400);
        }
    }

    public function showReducedPricesReportForm()
    {
        $this->data['stores'] = Store::select('id', 'title_ar as title')->get();
        $this->data['competitors'] = Competitor::select('id', 'title_ar as title')->get();
        $this->data['promotions'] = Promo::select('id', 'title_ar as title')->get();
        return $this->_view("reports.reduced_prices");
    }

    public function storeReducedPricesReport(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->reduced_prices_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }

            DB::beginTransaction();
            $report = new CompetitorReport();
            $report->type = CompetitorReport::$types['reduced_prices'];
            $report->store_id = $request->input('store');
            $report->competitor_id = $request->input('competitor');
            $report->employee_id = $this->user->id;
            $report->save();

            $products = [];
            foreach ($request->input('products') as $product) {
                $products[] = [
                    'product_id' => $product['competitor_product'],
                    'product_name' => $product['competitor_product_name'],
                    'promo_id' => $product['promo'],
                    'current_price' => $product['current_price'],
                    'offer_price' => $product['offer_price'],
                    'competitors_report_id' => $report->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
            CompetitorReportReducedPricesProduct::insert($products);

            DB::commit();
            $message = 'تم إرسال التقرير بنجاح';
            return _json('success', $message);
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'حدث خطأ ما!';
            return _json('error', $message, 400);
        }
    }

    public function showOrdinaryReportForm()
    {
        $this->data['stores'] = Store::select('id', 'title_ar as title')->get();
        $this->data['competitors'] = Competitor::select('id', 'title_ar as title')->get();
        return $this->_view("reports.ordinary");
    }

    public function storeOrdinaryReport(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->ordinary_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            $report = new CompetitorReport();
            $report->type = CompetitorReport::$types['ordinary'];
            $report->store_id = $request->input('store');
            $report->competitor_id = $request->input('competitor');
            $report->employee_id = $this->user->id;
            $report->title = $request->input('title');
            $report->notes = $request->input('notes');
            
            if ($request->input('images')) {
                $images = [];
                foreach ($request->input('images') as $image) {
                    if ($image != null) {
                        $images[] = base64ToImage(str_replace('data:image/jpeg;base64,', '', $image));
                    }
                }
                if (!empty($images)) {
                    $report->images = json_encode($images);
                }
            }
            $report->save();
            $message = 'تم إرسال التقرير بنجاح';
            return _json('success', $message);
        } catch (\Exception $ex) {
            $message = 'حدث خطأ ما!';
            return _json('error', $message, 400);
        }
    }


    
}
