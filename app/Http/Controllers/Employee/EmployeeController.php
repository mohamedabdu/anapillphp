<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use function Helper\Common\upload;


class EmployeeController extends Controller
{

    protected $languages = array(
        'ar' => 'arabic',
        'en' => 'english'
    );

    protected $lang_code = 'ar';
    protected $user;
    protected $data = array();
    protected $limit = 1;

    public function __construct()
    {
        $this->middleware('auth:employee');
        //$this->middleware('role');
        $this->setLang();
        $this->getCookieLangAndSetLocale();
        $this->user = Auth::guard('employee')->user();
        $this->data['user'] = $this->user;
        $this->data['languages'] = $this->languages;
    }

    protected function setLang($lang = "ar")
    {
        $lang_code = $lang;
        $long = 7 * 60 * 24;
        \Cookie::queue('EmployeeLang', $lang_code, $long);
    }

    protected function getCookieLangAndSetLocale()
    {
        if (\Cookie::get('EmployeeLang') !== null) {
            try {
                $this->lang_code = \Crypt::decrypt(\Cookie::get('EmployeeLang'));
            } catch (DecryptException $ex) {
                $this->lang_code = 'ar';
            }
        } else {
            $this->lang_code = 'ar';
        }
        $this->data['lang_code'] = $this->lang_code;
        app()->setLocale($this->lang_code);
    }

    protected function uploadFile(Request $request, $field, $thum = true)
    {
        if ($request->hasFile($field)) {
            $uploaded = upload($request->file($field), $thum);
            if ($thum) {
                $file = $uploaded['thum'];
            } else {
                $file = $uploaded['img'];
            }
            return $file;
        } else {
            return "";
        }
    }

    public function _view($main_content, $type = 'employee')
    {
        $main_content = "main_content/$type/$main_content";
        return view($main_content, $this->data);
    }

    public function err404()
    {
        return $this->_view('err404','backend');
    }
}
