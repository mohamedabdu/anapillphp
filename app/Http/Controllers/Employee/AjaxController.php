<?php

namespace App\Http\Controllers\Employee;

use App\Models\Product;
use App\Models\StoreCategory;
use Illuminate\Http\Request;

class AjaxController extends EmployeeController {

    
    public function change_sidebar_state(Request $request) {
        $request->session()->forget('sidebar');
        $data = json_decode($request->getContent());
        foreach ($data as $item) {
            $request->session()->put('sidebar.' . $item, 1);
        }
        return _json('success',$data);
    }

    public function getCompetitorProducts(Request $request)
    {
        $competitor_products = Product::where('competitor_id',$request->input('competitor'))
                                                    ->select('id', 'title_ar as title')
                                                    ->get()
                                                    ->toArray();
                                  
        return _json('success', ['competitor_products' => $competitor_products]);
    }

    public function getStoreCategories(Request $request)
    {
        $store_categories = StoreCategory::join('sub_categories', 'sub_categories.id','=','store_categories.sub_category_id')
                                            ->where('store_categories.store_id', $request->input('store'))
                                            ->select('sub_categories.id', 'sub_categories.title_ar as title')
                                            ->get()
                                            ->toArray();

        return _json('success', ['store_categories' => $store_categories]);
    }

    
    public function deleteImage(Request $request)
    {
        try {
            $column_name = $request->input('col');

            // for edit form for non uploaded images
            if (!$request->input('model')) {
                return _json('success', _lang('app.updated_successfully'));
            }
            //

            $model = "App\Models\\" . $request->input('model');
            $model::deleteUploaded($request->input('folder'), $request->input('image'));

            $find = $model::find($request->input('id'));
            if (!json_decode($find->$column_name)) {
                $find->$column_name = "";
            } else {
                $images = json_decode($find->$column_name);
                $image_index = array_search($request->input('image'), $images);
                
                if (isset($images[$image_index])) {
                    unset($images[$image_index]); 
                }
                if (count($images) == 0) {
                    $find->$column_name = "";
                }else{
                    //$find->$column_name = json_encode(array_values($images));
                    $find->$column_name = json_encode($images);
                }
            }
            $find->save();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            return _json('error', _lang('app.error_is_occured'), 400);
        }
       

    }
}
