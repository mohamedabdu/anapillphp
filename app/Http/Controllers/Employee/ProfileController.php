<?php

namespace App\Http\Controllers\Employee;

use DB;
use App\User;
use Validator;
use App\Models\Visit;
use App\Models\VisitDetails;
use Illuminate\Http\Request;
use App\Models\UserAttendance;
use App\Models\EmployeeHoliday;

class ProfileController extends EmployeeController
{
   private $holiday_rules = [
        'title' => 'required',
        'reason' => 'required',
        'start_date' => 'required',
        'end_date' => 'required'
   ];
    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $days_of_week = [
            'Sun' => 0,
            'Mon' => 1,
            'Tue' => 2,
            'Wed' => 3,
            'Thr' => 4,
            'Fri' => 5,
            'Sat' => 6
        ];
        
        $employee = User::transform($this->user);
        $employee_weekends = [];
        foreach ($days_of_week as $day => $value) {
            if (!in_array($day, $employee->default_work_days)) {
                $employee_weekends[] = $value;
            }
        }
        $this->data['User'] = $employee;
        $this->data['weekends'] = $employee_weekends;
        return $this->_view("profile");
    }

    public function update_profile(Request $request)
    {
        try {
            
            if ($request->input('name')) {
                $rules['name'] = "required";
            }
            if ($request->input('email')) {
                $rules['email'] = "required|email|unique:users,email," . $this->user->id;
            }
            if ($request->input('mobile')) {
                $rules['mobile'] = "required|unique:users,mobile," . $this->user->id;
            }
            if ($request->file('image')) {
                $rules['image'] = "image|mimes:gif,png,jpeg|max:5000";
            }
            if ($request->input('old_password')) {
                $rules['old_password'] = "required";
                $rules['password'] = "required";
                $rules['password_confirmation'] = "required|same:password";
            }
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }

            DB::beginTransaction();
            if ($request->input('name')) {
                $this->user->name = $request->input('name');
            }
            if ($request->input('email')) {
                $this->user->email = $request->input('email');
            }
            if ($request->input('mobile')) {
                $this->user->mobile = $request->input('mobile');
            }
            if ($old_password = $request->input('old_password')) {
                if (!password_verify($old_password, $this->user->password)) {
                    $message = 'كلمة المرور القديمة غير صحيحة';
                    return _json('error', $message,400);
                } else {
                    $this->user->password = bcrypt($request->input('password'));
                }
            }
            if ($request->file('image')) {
                if (!is_dir($this->user->image)) {
                    if (file_exists($this->user->image)) {
                        unlink($this->user->image);
                    }
                }
                $this->user->image = $this->uploadFile($request, 'image');
            }
            $this->user->save();
            DB::commit();
            $message = 'تم تحديث بياناتك  بنجاح';
            return _json('success', $message);
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'حدث خطأ ما!';
            return _json('error', $message, 400);
        }
    }

    public function holidayRequest(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->holiday_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            $check = EmployeeHoliday::where('employee_id',$this->user->id)->where('status',0)->first();
            if ($check) {
                $message = 'لقد قمت بإرسال طلب إجازة مسبقاً';
                return _json('error', $message, 400);
            }
            $employeeHoliday = new EmployeeHoliday();
            $employeeHoliday->employee_id = $this->user->id;
            $employeeHoliday->title = $request->input('title');
            $employeeHoliday->reason = $request->input('reason');
            $employeeHoliday->start_date = $request->input('start_date');
            $employeeHoliday->end_date = $request->input('end_date');
            $employeeHoliday->save();
            $message = 'تم ارسال طلب الأجازة';
            return _json('success', $message);
        } catch (\Exception $ex) {
            $message = 'حدث خطأ ما!';
            return _json('error', $message, 400);
        }
    }

    public function showDailyReport()
    {
        try {
           $employee_visits = VisitDetails::Join('visits','visits.id','=', 'visit_details.visit_id')
                                            ->where('visit_details.start_date',date('Y-m-d'))
                                            ->where('visit_details.employee_id',$this->user->id)
                                            ->select('visits.type', 'visit_details.status')
                                            ->get();
            $employee_attendance =  UserAttendance::where('user_id', $this->user->id)->where('is_end', false)->first();


            $visits_count = $employee_visits->count();
            $cancelled_visits_count = $employee_visits->where('status', VisitDetails::$statuses['CANCELLED'])->count();
            $finished_visits_count = $employee_visits->where('status', VisitDetails::$statuses['FINISHED'])->count();

            $this->data['completion_percentage'] = round( ($finished_visits_count / ( $visits_count - $cancelled_visits_count  ) ) * 100 , 0);

            $this->data['unfinished_normal_visits_count'] = $employee_visits->where('status',VisitDetails::$statuses['UNFINISHED'])
                                                                    ->where('type',Visit::$types['normal'])
                                                                    ->count();
            $this->data['finished_normal_visits_count'] = $employee_visits->where('status', VisitDetails::$statuses['FINISHED'])
                                                                    ->where('type', Visit::$types['normal'])
                                                                    ->count();
            $this->data['unfinished_scheduled_visits_count'] = $employee_visits->where('status', VisitDetails::$statuses['UNFINISHED'])
                                                                        ->where('type', Visit::$types['scheduled'])
                                                                        ->count();
            $this->data['finished_scheduled_visits_count'] = $employee_visits->where('status', VisitDetails::$statuses['FINISHED'])
                                                                        ->where('type', Visit::$types['scheduled'])
                                                                        ->count();
            $start_date = strtotime($employee_attendance->start_date);
            $this->data['attendance_start_time'] = date('H:i', $start_date);
            $this->data['attendance_end_time'] = date('H:i');

            $start_time = new \DateTime($employee_attendance->start_date);
            $end_time   = new \DateTime(date('Y-m-d').' '.$this->data['attendance_end_time']);

            $this->data['total_time'] = $start_time->diff($end_time)->format("%H:%I");

            return $this->_view('daily_report');

        } catch (\Exception $ex) {
           return $this->err404();
        }
    }

}
