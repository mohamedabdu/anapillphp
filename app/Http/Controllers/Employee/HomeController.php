<?php

namespace App\Http\Controllers\Employee;

use App\Models\Visit;
use App\Models\VisitDetails;
use Illuminate\Http\Request;

class HomeController extends EmployeeController
{
    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['scheduleVisits'] = $this->getVisits(Visit::$types["scheduled"]);
        $this->data['normalVisits'] = $this->getVisits(Visit::$types["normal"]);
        return $this->_view("index");
    }

    private function getVisits($type)
    {
        $visits = Visit::join("visit_details", function($query){
                            $query->on("visits.id", "=", "visit_details.visit_id")
                             ->where("visit_details.employee_id", $this->user->id)
                              ->where("visit_details.start_date", date('Y-m-d'));
                        })
                        ->join('visit_stores', "visit_details.visit_store_id", "=", "visit_stores.id")
                        ->join('stores', "visit_stores.store_id", "=", "stores.id")
                        ->where("visits.type", $type)
                        ->where("visit_details.status", VisitDetails::$statuses["UNFINISHED"])
                        ->select(
                                'visit_details.id',
                                'visit_details.code',
                                'visit_details.start_date',
                                'stores.lat',
                                'stores.lng',
                                'stores.title_ar as store'
                                )
                        ->get();
        return  Visit::transformCollection($visits, 'Employee');
    }
    
}
