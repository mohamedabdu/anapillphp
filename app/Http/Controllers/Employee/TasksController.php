<?php

namespace App\Http\Controllers\Employee;

use App\Models\Notification;
use DB;
use Validator;
use App\Models\Task;
use App\Models\Promo;
use App\Models\Product;
use App\Models\PromoDetails;
use App\Models\SubCategory;
use App\Models\ReturnReason;
use App\Models\VisitDetails;
use Illuminate\Http\Request;
use App\Models\VisitTaskDetails;
use App\Models\VisitTaskReturnsDetails;
use App\Models\VisitTaskInventoryDetails;
use function Helper\Common\base64ToImage;
use App\Models\VisitTaskAreasReportDetail;
use App\Models\VisitTaskPriceReportDetails;
use App\Models\VisitTaskReceivingOrderDetails;
use App\Models\VisitTaskCheckPromotionsDetails;
use App\Models\VisitTaskReceivingOrderDetailsProduct;

class TasksController extends EmployeeController
{
    private $task_actions_rules = [
        'code' => 'required',
        'task' => 'required',
        'lat' => 'required',
        'lng' => 'required'
    ];

    private $tasks = [
        'organizing_shelves' => 1,
        'returns' => 2,
        'general_inventory' => 3,
        'detailed_inventory' => 4,
        'price_report' => 5,
        'areas_report' => 6,
        'receiving_order' => 7,
        'check_promotions' => 8
    ];

    private $organizing_shelves_rules = [
        'code' => 'required',
        'image_before' => 'required',
        'image_after' => 'required'
    ];

    private $returns_rules = [
        'code' => 'required',
        'bill_number' => 'required',
        'products.*.product' => 'required',
        'products.*.pieces_number' => 'required',
        'products.*.reutrn_reason' => 'required',
        'lat' => 'required',
        'lng' => 'required'
    ];

    private $inventory_rules = [
        'code' => 'required',
        'products.*.product' => 'required',
        'lat' => 'required',
        'lng' => 'required'
    ];

    private $price_report_rules = [
        'code' => 'required',
        'products.*.product' => 'required',
        'products.*.price' => 'required',
        'lat' => 'required',
        'lng' => 'required'
    ];

    private $areas_report_rules = [
        'code' => 'required',
        'category' => 'required',
        'width' => 'required',
        'height' => 'required'
    ];

    private $receiving_order_rules = [
        'code' => 'required',
        'bill_number' => 'required',
        'bill_date' => 'required',
        'bill_image' => 'required',
        'bill_total' => 'required',
        'driver_name' => 'required',
        'products.*.product' => 'required',
        'products.*.pieces_number' => 'required',
        'products.*.cartons_number' => 'required',
    ];

    private $check_promotions_rules = [
        'code' => 'required',
        'promo' => 'required',
        'promo_count' => 'required',
        'image' => 'required'
    ];

    public function showTaskDescription($task)
    {
        try {
            $tasks = Task::select('process', 'title_ar as title', 'description_ar as description', 'images')->get();
            $task_name = str_replace('-', ' ', $task);
            $task = $tasks->where('title', $task_name)->first();
            if (!$task) {
                return $this->err404();
            }
            $task->images = json_decode($task->images);
            $this->data['task'] = $task;
            return $this->_view('tasks.task_description');
        } catch (\Exception $ex) {
            return $this->err404();
        }
    }
    public function startTask(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->task_actions_rules);
            if ($validator->fails()) {
                $message = 'خطأ في البيانات المطلوبة لبدء المهمة';
                return _json('error', $message, 400);
            }

            $result = $this->checkTaskStatus($request->input('code'), $request->input('task'));
            if ($result instanceof VisitTaskDetails) {
                $visit_task_details = $result;
            } else {
                return $result;
            }

            $visit_task_details->start_date = date('Y-m-d H:i:s');
            $visit_task_details->start_lat = $request->input('lat');
            $visit_task_details->start_lng =  $request->input('lng');
            $visit_task_details->save();

            return _json('success');
        } catch (\Exception $ex) {
            $message = 'لقد حدث خطأ ما !';
            return _json('error', $message, 400);
        }
    }

    public function finishTask(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), $this->task_actions_rules);
            if ($validator->fails()) {
                $message = 'خطأ في البيانات المطلوبة لإنهاء المهمة';
                return _json('error', $message, 400);
            }

            $result = $this->checkTaskStatus($request->input('code'), $request->input('task'));
            if ($result instanceof VisitTaskDetails) {
                $visit_task_details = $result;
            } else {
                return $result;
            }

            $visit_task_details->end_date = date('Y-m-d H:i:s');
            $visit_task_details->end_lat = $request->input('lat');
            $visit_task_details->end_lng =  $request->input('lng');
            if ($request->input('notes')) {
                $visit_task_details->notes = $request->input('notes');
            }
            $visit_task_details->status = VisitTaskDetails::$statuses['FINISHED'];
            $visit_task_details->save();

            $redirect = route('employee.visits.show', ['code' => $request->input('code')]);
            return _json('success', $redirect);
        } catch (\Exception $ex) {
            $message = 'لقد حدث خطأ ما !';
            return _json('error', $message, 400);
        }
    }
    public function showOrganizingShelvesTaskForm($code)
    {
        $result = $this->checkTaskStatus($code, $this->tasks['organizing_shelves']);
        if (!$result instanceof VisitTaskDetails) {
            return $this->err404();
        }
        $this->data['code'] = $result->code;
        $this->data['store'] = $result->store;
        $this->data['task_name'] = $result->task;
        $this->data['task'] = $this->tasks['organizing_shelves'];
        $this->data['start_time'] = (time() - strtotime($result->start_date)) * 1000;
        return $this->_view("tasks.organizing_shelves");
    }

    public function OrganizingShelvesTask(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), $this->organizing_shelves_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }

            $result = $this->checkTaskStatus($request->input('code'), $this->tasks['organizing_shelves']);
            if ($result instanceof VisitTaskDetails) {
                $visit_task_details = $result;
            } else {
                return $result;
            }
            DB::beginTransaction();
            $task_record = new VisitTaskShelvesDetails();
            $task_record->image_before = base64ToImage(str_replace('data:image/jpeg;base64,', '', $request->input('image_before')));
            $task_record->image_after = base64ToImage(str_replace('data:image/jpeg;base64,', '', $request->input('image_after')));
            if ($request->input('notes_before')) {
                $task_record->notes_before  = $request->input('notes_before');
            }
            if ($request->input('notes_after')) {
                $task_record->notes_after  = $request->input('notes_before');
            }
            $task_record->visit_task_details_id = $visit_task_details->id;
            $task_record->save();

            DB::commit();
            $message = 'تمت الإضافة بنجاح';
            return _json('success', ['message' => $message]);
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'لقد حدث خطأ ما !';
            return _json('error', $message, 400);
        }
    }

    public function showReturnsTaskForm($code)
    {
        $result = $this->checkTaskStatus($code, $this->tasks['returns']);

        if (!$result instanceof VisitTaskDetails) {
            return $this->err404();
        }
        $this->data['products'] = Product::whereNull('competitor_id')->orderBy('ordered')->select('id', 'title_ar as title')->get();
        $this->data['returnReasons'] = ReturnReason::select('id', 'title_ar as title')->get();
        $this->data['code'] = $result->code;
        $this->data['store'] = $result->store;
        $this->data['task_name'] = $result->task;
        $this->data['task'] = $this->tasks['returns'];
        $this->data['start_time'] = (time() - strtotime($result->start_date)) * 1000;
        return $this->_view("tasks.returns");
    }

    public function ReturnsTask(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), $this->returns_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            $result = $this->checkTaskStatus($request->input('code'), $this->tasks['returns']);
            if ($result instanceof VisitTaskDetails) {
                $visit_task_details = $result;
            } else {
                return $result;
            }

            DB::beginTransaction();
            $task_record = new VisitTaskReturnsDetails();
            $task_record->visit_task_details_id = $visit_task_details->id;
            $task_record->bill_number = $request->input('bill_number');
            if ($request->input('images')) {
                $images = [];
                foreach ($request->input('images') as $image) {
                    if ($image != null) {
                        $images[] = base64ToImage(str_replace('data:image/jpeg;base64,', '', $image));
                    }
                }
                if (!empty($images)) {
                    $task_record->images = json_encode($images);
                }
            }
            if ($request->input('notes')) {
                $task_record->notes = $request->input('notes');
            }
            $task_record->save();

            $products = [];
            foreach ($request->input('products') as $product) {
                $products[] = [
                    'product_id' => $product['product'],
                    'pieces_number' => $product['pieces_number'],
                    'cartons_number' => $product['cartons_number'],
                    'return_reason_id' => $product['reutrn_reason'],
                    'visit_task_returns_details_id' => $task_record->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
            VisitTaskReturnsDetailsProduct::insert($products);


            // finish task
            $visit_task_details->end_date = date('Y-m-d H:i:s');
            $visit_task_details->end_lat = $request->input('lat');
            $visit_task_details->end_lng =  $request->input('lng');
            $visit_task_details->status = VisitTaskDetails::$statuses['FINISHED'];
            $visit_task_details->save();

            DB::commit();
            $redirect = route('visits.show', ['code' => $request->input('code')]);
            return _json('success', $redirect);
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'لقد حدث خطأ ما !';
            return _json('error', $message, 400);
        }
    }

    public function showGeneralInventoryTaskForm($code)
    {
        $result = $this->checkTaskStatus($code, $this->tasks['general_inventory']);

        if (!$result instanceof VisitTaskDetails) {
            return $this->err404();
        }
        $this->data['products'] = Product::whereNull('competitor_id')->orderBy('ordered')->select('id', 'image', 'title_ar as title')->get();
        $this->data['code'] = $result->code;
        $this->data['store'] = $result->store;
        $this->data['task_name'] = $result->task;
        $this->data['task'] = $this->tasks['general_inventory'];
        $this->data['start_time'] = (time() - strtotime($result->start_date)) * 1000;
        return $this->_view("tasks.general_inventory");
    }

    public function generalInventory(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), $this->inventory_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            $result = $this->checkTaskStatus($request->input('code'), $this->tasks['general_inventory']);
            if ($result instanceof VisitTaskDetails) {
                $visit_task_details = $result;
            } else {
                return $result;
            }

            DB::beginTransaction();

            $products = [];
            foreach ($request->input('products') as $product) {
                $in_shelf = isset($product['in_shelf']) ? 1 : 0;
                $in_warehouse = isset($product['in_warehouse']) ? 1 : 0;
                $products[] = [
                    'product_id' => $product['product'],
                    'in_shelf' => $in_shelf,
                    'in_warehouse' => $in_warehouse,
                    'visit_task_details_id' => $visit_task_details->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
            VisitTaskInventoryDetails::insert($products);


            // finish task
            $visit_task_details->end_date = date('Y-m-d H:i:s');
            $visit_task_details->end_lat = $request->input('lat');
            $visit_task_details->end_lng =  $request->input('lng');
            $visit_task_details->status = VisitTaskDetails::$statuses['FINISHED'];
            if ($request->input('notes')) {
                $visit_task_details->notes = $request->input('notes');
            }
            $visit_task_details->save();

            DB::commit();
            $redirect = route('visits.show', ['code' => $request->input('code')]);
            return _json('success', $redirect);
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'لقد حدث خطأ ما !';
            return _json('error', $message, 400);
        }
    }

    public function showDetailedInventoryTaskForm($code)
    {
        $result = $this->checkTaskStatus($code, $this->tasks['detailed_inventory']);

        if (!$result instanceof VisitTaskDetails) {
            return $this->err404();
        }
        $this->data['products'] = Product::whereNull('competitor_id')->orderBy('ordered')->select('id', 'image', 'title_ar as title')->get();
        $this->data['code'] = $result->code;
        $this->data['store'] = $result->store;
        $this->data['task_name'] = $result->task;
        $this->data['task'] = $this->tasks['detailed_inventory'];
        $this->data['start_time'] = (time() - strtotime($result->start_date)) * 1000;
        return $this->_view("tasks.detailed_inventory");
    }

    public function DetailedInventory(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->inventory_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            $result = $this->checkTaskStatus($request->input('code'), $this->tasks['detailed_inventory']);
            if ($result instanceof VisitTaskDetails) {
                $visit_task_details = $result;
            } else {
                return $result;
            }

            DB::beginTransaction();
            $products = [];
            foreach ($request->input('products') as $product) {
                $in_shelf = isset($product['in_shelf']) ? 1 : 0;
                $in_warehouse = isset($product['in_warehouse']) ? 1 : 0;
                $shelf_quantity = isset($product['shelf_quantity']) ? $product['shelf_quantity'] : 0;
                $warehouse_quantity = isset($product['warehouse_quantity']) ? $product['warehouse_quantity'] : 0;

                $products[] = [
                    'product_id' => $product['product'],
                    'in_shelf' => $in_shelf,
                    'in_warehouse' => $in_warehouse,
                    'shelf_quantity' => $shelf_quantity,
                    'warehouse_quantity' => $warehouse_quantity,
                    'visit_task_details_id' => $visit_task_details->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
            VisitTaskInventoryDetails::insert($products);


            // finish task
            $visit_task_details->end_date = date('Y-m-d H:i:s');
            $visit_task_details->end_lat = $request->input('lat');
            $visit_task_details->end_lng =  $request->input('lng');
            $visit_task_details->status = VisitTaskDetails::$statuses['FINISHED'];
            if ($request->input('notes')) {
                $visit_task_details->notes = $request->input('notes');
            }
            $visit_task_details->save();

            DB::commit();
            $redirect = route('visits.show', ['code' => $request->input('code')]);
            return _json('success', $redirect);
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'لقد حدث خطأ ما !';
            return _json('error', $message, 400);
        }
    }

    public function showPriceReportTaskForm($code)
    {
        $result = $this->checkTaskStatus($code, $this->tasks['price_report']);

        if (!$result instanceof VisitTaskDetails) {
            return $this->err404();
        }
        $this->data['products'] = Product::whereNull('competitor_id')->orderBy('ordered')->select('id', 'image', 'title_ar as title')->get();
        $this->data['code'] = $result->code;
        $this->data['store'] = $result->store;
        $this->data['task_name'] = $result->task;
        $this->data['task'] = $this->tasks['price_report'];
        $this->data['start_time'] = (time() - strtotime($result->start_date)) * 1000;
        return $this->_view("tasks.price_report");
    }

    public function priceReport(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), $this->price_report_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }

            $result = $this->checkTaskStatus($request->input('code'), $this->tasks['price_report']);
            if ($result instanceof VisitTaskDetails) {
                $visit_task_details = $result;
            } else {
                return $result;
            }

            DB::beginTransaction();
            $products = [];
            foreach ($request->input('products') as $product) {
                $products[] = [
                    'product_id' => $product['product'],
                    'price' => $product['price'],
                    'visit_task_details_id' => $visit_task_details->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
            VisitTaskPriceReportDetails::insert($products);


            // finish task
            $visit_task_details->end_date = date('Y-m-d H:i:s');
            $visit_task_details->end_lat = $request->input('lat');
            $visit_task_details->end_lng =  $request->input('lng');
            $visit_task_details->status = VisitTaskDetails::$statuses['FINISHED'];
            if ($request->input('notes')) {
                $visit_task_details->notes = $request->input('notes');
            }
            $visit_task_details->save();

            DB::commit();
            $redirect = route('visits.show', ['code' => $request->input('code')]);
            return _json('success', $redirect);
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'لقد حدث خطأ ما !';
            return _json('error', $message, 400);
        }
    }

    public function showAreasReportTaskForm($code)
    {
        $result = $this->checkTaskStatus($code, $this->tasks['areas_report']);

        if (!$result instanceof VisitTaskDetails) {
            return $this->err404();
        }
        $this->data['categories'] = SubCategory::join('store_categories', function ($query) use ($result) {
            $query->on('store_categories.sub_category_id', '=', 'sub_categories.id')
                ->where('store_categories.store_id', $result->store_id);
        })
            ->leftJoin('visit_task_areas_report_details', function ($query) use ($result) {
                $query->on('visit_task_areas_report_details.sub_category_id', '=', 'sub_categories.id')
                    ->where('visit_task_areas_report_details.visit_task_details_id', $result->id);
            })
            ->where('sub_categories.active', true)
            ->select(
                'sub_categories.id',
                'sub_categories.title_ar as title',
                'visit_task_areas_report_details.id as filled',
                'store_categories.width',
                'store_categories.height'
            )
            ->get();
        $this->data['code'] = $result->code;
        $this->data['store'] = $result->store;
        $this->data['task_name'] = $result->task;
        $this->data['task'] = $this->tasks['areas_report'];
        $this->data['start_time'] = (time() - strtotime($result->start_date)) * 1000;
        return $this->_view("tasks.areas_report");
    }

    public function areasReport(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), $this->areas_report_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }

            $result = $this->checkTaskStatus($request->input('code'), $this->tasks['areas_report']);
            if ($result instanceof VisitTaskDetails) {
                $visit_task_details = $result;
            } else {
                return $result;
            }

            $task_record = new VisitTaskAreasReportDetail();
            $task_record->sub_category_id = $request->input('category');
            $task_record->width = $request->input('width');
            $task_record->height = $request->input('height');
            if ($request->input('notes')) {
                $task_record->notes = $request->input('notes');
            }
            if ($request->input('images')) {
                $images = [];
                foreach ($request->input('images') as $image) {
                    if ($image != null) {
                        $images[] = base64ToImage(str_replace('data:image/jpeg;base64,', '', $image));
                    }
                }
                if (!empty($images)) {
                    $task_record->images = json_encode($images);
                }
            }
            $task_record->visit_task_details_id = $visit_task_details->id;
            $task_record->save();
            $message = 'تمت الإضافة بنجاح';
            return _json('success', ['message' => $message]);
        } catch (\Exception $ex) {
            $message = 'لقد حدث خطأ ما !';
            return _json('error', $message, 400);
        }
    }

    public function showReceivingOrderTaskForm($code)
    {
        $result = $this->checkTaskStatus($code, $this->tasks['receiving_order']);
        if (!$result instanceof VisitTaskDetails) {
            return $this->err404();
        }
        $this->data['products'] = Product::whereNull('competitor_id')->orderBy('ordered')->select('id', 'title_ar as title')->get();
        $this->data['code'] = $result->code;
        $this->data['store'] = $result->store;
        $this->data['task_name'] = $result->task;
        $this->data['task'] = $this->tasks['receiving_order'];
        $this->data['start_time'] = (time() - strtotime($result->start_date)) * 1000;
        return $this->_view("tasks.receiving_order");
    }

    public function receivingOrder(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), $this->receiving_order_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            $result = $this->checkTaskStatus($request->input('code'), $this->tasks['receiving_order']);
            if ($result instanceof VisitTaskDetails) {
                $visit_task_details = $result;
            } else {
                return $result;
            }

            DB::beginTransaction();
            $task_record = new VisitTaskReceivingOrderDetails();
            $task_record->visit_task_details_id = $visit_task_details->id;
            $task_record->bill_number = $request->input('bill_number');
            $task_record->bill_date = $request->input('bill_date');
            $task_record->bill_image = base64ToImage(str_replace('data:image/jpeg;base64,', '', $request->input('bill_image')));
            $task_record->bill_total = $request->input('bill_total');
            $task_record->driver_name = $request->input('driver_name');

            if ($request->input('images')) {
                $images = [];
                foreach ($request->input('images') as $image) {
                    if ($image != null) {
                        $images[] = base64ToImage(str_replace('data:image/jpeg;base64,', '', $image));
                    }
                }
                if (!empty($images)) {
                    $task_record->images = json_encode($images);
                }
            }
            $task_record->save();

            $products = [];
            foreach ($request->input('products') as $product) {
                $products[] = [
                    'product_id' => $product['product'],
                    'pieces_number' => $product['pieces_number'],
                    'cartons_number' => $product['cartons_number'],
                    'visit_task_receiving_order_details_id' => $task_record->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
            VisitTaskReceivingOrderDetailsProduct::insert($products);
            DB::commit();
            $message = 'تمت الإضافة بنجاح';
            return _json('success', ['message' => $message]);
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'لقد حدث خطأ ما !';
            return _json('error', $message, 400);
        }
    }

    public function showCheckPromotionsTaskForm($code)
    {
        $result = $this->checkTaskStatus($code, $this->tasks['check_promotions']);
        if (!$result instanceof VisitTaskDetails) {
            return $this->err404();
        }
        $this->data['promos'] = Promo::join('promo_details', function ($query) use ($result) {
            $query->on('promo_details.promo_id', '=', 'promos.id')
                ->where('promo_details.store_id', $result->store_id)
                ->where('promo_details.start_date', '<=', date('Y-m-d'))
                ->where('promo_details.end_date', '>=', date('Y-m-d'));
        })
            ->leftJoin('visit_task_check_promotions_details', function ($query) use ($result) {
                $query->on('visit_task_check_promotions_details.promo_id', '=', 'promos.id')
                    ->where('visit_task_check_promotions_details.visit_task_detail_id', $result->id);
            })
            ->select(
                'promos.id',
                'promos.title_ar as title',
                'visit_task_check_promotions_details.id as filled'
            )
            ->get();
        $this->data['code'] = $result->code;
        $this->data['store'] = $result->store;
        $this->data['task_name'] = $result->task;
        $this->data['task'] = $this->tasks['check_promotions'];
        $this->data['start_time'] = (time() - strtotime($result->start_date)) * 1000;
        return $this->_view("tasks.check_promotions");
    }

    public function CheckPromotions(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), $this->check_promotions_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            $result = $this->checkTaskStatus($request->input('code'), $this->tasks['check_promotions']);
            if ($result instanceof VisitTaskDetails) {
                $visit_task_details = $result;
            } else {
                return $result;
            }

            DB::beginTransaction();
            $task_record = new VisitTaskCheckPromotionsDetails();
            $task_record->promo_id = $request->input('promo');
            $task_record->promo_count = $request->input('promo_count');
            $task_record->image = base64ToImage(str_replace('data:image/jpeg;base64,', '',  $request->input('image')));
            $task_record->notes = $request->input('notes') ?: '';
            $task_record->visit_task_detail_id = $visit_task_details->id;
            $task_record->save();

            
            //check promo count to send notification 

            $store_promo_details = PromoDetails::where('store_id', $visit_task_details->store_id)
                                        ->where('promo_id', $request->input('promo'))
                                        ->first();
            if ($request->input('promo_count') < $store_promo_details->promo_num) {
               
                /*$notification = new Notification();
                    $notification->title = "";
                    $notification->body  = "";
                    $notification->type = "";
                    $notification->save();*/
            }
           

            DB::commit();
            $message = 'تمت الإضافة بنجاح';
            return _json('success', ['message' => $message]);
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'لقد حدث خطأ ما !';
            return _json('error', $message, 400);
        }
    }



    private function checkTaskStatus($code, $task)
    {
        $visit_details = VisitDetails::where('visit_details.code', $code)
            ->where('visit_details.employee_id', $this->user->id)
            ->where('visit_details.start_date', date('Y-m-d'))
            ->where('visit_details.status', VisitDetails::$statuses['UNFINISHED'])
            ->whereNotNull('visit_details.start_time')
            ->whereNull('visit_details.end_time')
            ->select('visit_details.*')
            ->first();

        if (!$visit_details) {
            $message = 'هذه الزيارة غير موجودة';
            return _json('error', $message, 404);
        }

        $visit_task_details = VisitTaskDetails::join('visit_details', 'visit_details.id', '=', 'visit_task_details.visit_details_id')
            ->join('visit_stores', 'visit_stores.id', '=', 'visit_details.visit_store_id')
            ->join('stores', 'stores.id', '=', 'visit_stores.store_id')
            ->join('tasks', 'tasks.process', '=', 'visit_task_details.task_id')
            ->where('visit_task_details.task_id', $task)
            ->where('visit_task_details.visit_details_id', $visit_details->id)
            ->where('visit_task_details.status', VisitTaskDetails::$statuses['UNFINISHED'])
            ->where(function ($query) {
                $query->whereNull('visit_task_details.start_date') // didn't start
                    ->orWhere(function ($query) { // started
                        $query->whereNotNull('visit_task_details.start_date')
                            ->whereNull('visit_task_details.end_date')
                            ->whereDate('visit_task_details.start_date', date('Y-m-d'));
                    });
            })
            ->select(
                'visit_task_details.*',
                'visit_details.code',
                'stores.title_ar as store',
                'stores.id as store_id',
                'tasks.title_ar as task'
            )
            ->first();

        if (!$visit_task_details) {
            $message = 'هذه المهمة غير موجودة';
            return _json('error', $message, 404);
        }

        return $visit_task_details;
    }
}
