<?php

namespace App\Http\Controllers\Supervisor;

use Illuminate\Http\Request;

class HomeController extends SupervisorController
{
    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tab'] = 'dashboard';
        return $this->_view("index");
    }

    
    
}
