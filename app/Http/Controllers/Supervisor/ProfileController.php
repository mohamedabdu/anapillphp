<?php

namespace App\Http\Controllers\Supervisor;

use DB;
use App\User;
use Validator;
use Illuminate\Http\Request;

class ProfileController extends SupervisorController
{
   
    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supervisor = User::transform($this->user);
        $this->data['User'] = $supervisor;
        return $this->_view("profile");
    }

    public function update_profile(Request $request)
    {
        try {
            $rules = [];
            if ($request->input('name')) {
                $rules['name'] = "required";
            }
            if ($request->input('email')) {
                $rules['email'] = "required|email|unique:users,email," . $this->user->id;
            }
            if ($request->input('mobile')) {
                $rules['mobile'] = "required|unique:users,mobile," . $this->user->id;
            }
            if ($request->file('image')) {
                $rules['image'] = "image|mimes:gif,png,jpeg|max:5000";
            }
            if ($request->input('old_password')) {
                $rules['old_password'] = "required";
                $rules['password'] = "required";
                $rules['password_confirmation'] = "required|same:password";
            }
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }

            DB::beginTransaction();
            if ($request->input('name')) {
                $this->user->name = $request->input('name');
            }
            if ($request->input('email')) {
                $this->user->email = $request->input('email');
            }
            if ($request->input('mobile')) {
                $this->user->mobile = $request->input('mobile');
            }
            
            if ($old_password = $request->input('old_password')) {
                if (!password_verify($old_password, $this->user->password)) {
                    $message = 'كلمة المرور القديمة غير صحيحة';
                    return _json('error', $message,400);
                } else {
                    $this->user->password = bcrypt($request->input('password'));
                }
            }
            if ($request->file('image')) {
                if (!is_dir($this->user->image)) {
                    if (file_exists($this->user->image)) {
                        unlink($this->user->image);
                    }
                }
                $this->user->image = $this->uploadFile($request, 'image');
            }
            $this->user->save();
            DB::commit();
            $message = 'تم تحديث بياناتك  بنجاح';
            return _json('success', $message);
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'حدث خطأ ما!';
            return _json('error', $message, 400);
        }
    }

}
