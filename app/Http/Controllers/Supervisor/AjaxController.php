<?php

namespace App\Http\Controllers\Supervisor;

use Illuminate\Http\Request;

class AjaxController extends SupervisorController {

    public function change_sidebar_state(Request $request)
    {
        $request->session()->forget('sidebar');
        $data = json_decode($request->getContent());
        foreach ($data as $item) {
            $request->session()->put('sidebar.' . $item, 1);
        }
        return _json('success', $data);
    }
    
}
