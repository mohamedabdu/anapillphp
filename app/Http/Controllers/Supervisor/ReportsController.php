<?php

namespace App\Http\Controllers\Supervisor;

use DB;
use App\User;
use Carbon\Carbon;
use App\Models\Store;
use App\Models\Report;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\CompetitorReport;

class ReportsController extends SupervisorController
{

    public function productsAlmostFinishedReportsEmployees()
    {
        try {

            $this->data['employees'] = $this->getEmloyeeList('App\\Models\\Report', 'reports', Report::$types['products_almost_finish']);
            $this->data['route'] = 'supervisor.reports.employee_products_almost_finish_reports';
            $this->data['page_title'] = 'منتجات شارفت على الإنتهاء';
            $this->data['parent_tab'] = 'reports_section';
            $this->data['tab'] = 'products_almost_finish_reports';
            return $this->_view('reports.employees_list');
        } catch (\Exception $ex) {
            return $this->err404();
        }
    }

    public function employeeProductsAlmostFinishedReports(Request $request)
    {
        try {
           
            $result = $this->getEmployee($request);
            if (!$result instanceof User) {
                return $result;
            }
            $employee = $result;

            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if ($value) {
                        $this->data[$key] = $value;
                    }
                }
            }
            
            $reports = Report::join('stores', 'stores.id', '=', 'reports.store_id')
                            ->where('reports.employee_id', $employee->id)
                            ->where('reports.type', Report::$types['products_almost_finish']);
                            if ($request->input('store')) {
                                foreach ($request->input('store') as $store) {
                                    $stores[] = \Crypt::decrypt($store);
                                }
                                $reports->whereIn('reports.store_id',$stores);
                            }
                            if ($request->input('product')) {
                                foreach ($request->input('product') as $product) {
                                    $products[] = \Crypt::decrypt($product);
                                }
                                $products = implode(",",$products);
                                $reports->whereRaw('(select count(*) from reports_products_almost_finished_products where report_id = reports.id and product_id in (' . $products . ')) > 0');
                            }
                            if ($request->input('from')) {
                                $reports->where('reports.created_at', '>=', $request->input('from'));
                            }
                            if ($request->input('to')) {
                                $reports->where('reports.created_at', '<=', $request->input('to'));
                            }
            $reports =  $reports->orderBy('reports.created_at', 'desc')
                                ->select('reports.*', 'stores.title_ar as store')
                                ->paginate($this->reports_limit);


            $reports =  Report::transformCollection($reports, 'Supervisor');
            // change employee reports status to Read
            if (!$request->ajax()) {
                $this->changeReportsStatus('App\\Models\\Report', $employee->id, Report::$types['products_almost_finish']);
            }

            if ($request->ajax()) {
                return _json('success', ['reports' => $reports]);
            }
            $this->data['reports'] = $reports;
            $this->data['employee'] = $employee;
            $this->data['stores'] = Store::select('id', 'title_ar as title')->get();
            $this->data['products'] = Product::whereNull('competitor_id')->select('id', 'title_ar as title')->get();
            $this->data['page_title'] = 'منتجات شارفت على الإنتهاء';
            $this->data['parent_tab'] = 'reports_section';
            $this->data['tab'] = 'products_almost_finish_reports';

            return $this->_view('reports.products_almost_finish');
        } catch (\Exception $ex) {
            if ($request->ajax()) {
                $message = 'حدث خطأ ما !';
                return _json('error', $message, 400);
            }
            return $this->err404();
        }
    }

    public function manualReportsEmployees()
    {
        try {

            $this->data['employees'] = $this->getEmloyeeList('App\\Models\\Report', 'reports', Report::$types['manual']);
            $this->data['route'] = 'supervisor.reports.employee_manual_reports';
            $this->data['page_title'] = 'تقرير يدوي';
            $this->data['parent_tab'] = 'reports_section';
            $this->data['tab'] = 'manual_reports';
            return $this->_view('reports.employees_list');
        } catch (\Exception $ex) {
            return $this->err404();
        }
    }

    public function employeeManualReports(Request $request)
    {
        try {

            $result = $this->getEmployee($request);
            if (!$result instanceof User) {
                return $result;
            }
            $employee = $result;

            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if ($value) {
                        $this->data[$key] = $value;
                    }
                }
            }
            $reports = Report::join('stores', 'stores.id', '=', 'reports.store_id')
                            ->where('reports.employee_id', $employee->id)
                            ->where('reports.type', Report::$types['manual']);
                            if ($request->input('store')) {
                                foreach ($request->input('store') as $store) {
                                    $stores[] = \Crypt::decrypt($store);
                                }
                                $reports->whereIn('reports.store_id',$stores);
                            }
                            if ($request->input('from')) {
                                $reports->where('reports.created_at', '>=', $request->input('from'));
                            }
                            if ($request->input('to')) {
                                $reports->where('reports.created_at', '<=', $request->input('to'));
                            }
            $reports = $reports->orderBy('reports.created_at', 'desc')
                                ->select('reports.*', 'stores.title_ar as store')
                                ->paginate($this->reports_limit);

            $reports =  Report::transformCollection($reports, 'Supervisor');
            // change employee reports status to Read
            if (!$request->ajax()) {
                $this->changeReportsStatus('App\\Models\\Report', $employee->id, Report::$types['manual']);
            }


            if ($request->ajax()) {
                return _json('success', ['reports' => $reports]);
            }
            $this->data['reports'] = $reports;
            $this->data['employee'] = $employee;
            $this->data['stores'] = Store::select('id', 'title_ar as title')->get();
            $this->data['page_title'] = 'تقرير يدوي';
            $this->data['parent_tab'] = 'reports_section';
            $this->data['tab'] = 'manual_reports';

            return $this->_view('reports.manual');
        } catch (\Exception $ex) {
            if ($request->ajax()) {
                $message = 'حدث خطأ ما !';
                return _json('error', $message, 400);
            }
            return $this->err404();
        }
    }

    public function priceComparisonReportsEmployees()
    {
        try {

            $this->data['employees'] = $this->getEmloyeeList('App\\Models\\CompetitorReport', 'competitors_reports', CompetitorReport::$types['price_comparison']);
            $this->data['route'] = 'supervisor.reports.employee_price_comparison_reports';
            $this->data['page_title'] = 'مقارنة أسعار';
            $this->data['parent_tab'] = 'reports_section';
            $this->data['sub_parent_tab'] = 'competitor_reports_section';
            $this->data['tab'] = 'price_comparison_reports';
            return $this->_view('reports.employees_list');
        } catch (\Exception $ex) {
            return $this->err404();
        }
    }

    public function employeePriceComparisonReports(Request $request)
    {
        try {

            $result = $this->getEmployee($request);
            if (!$result instanceof User) {
                return $result;
            }
            $employee = $result;

            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if ($value) {
                        $this->data[$key] = $value;
                    }
                }
            }
            $reports =  $this->getCompetitiorsReports($employee, 'price_comparison');
            // change employee reports status to Read
            if (!$request->ajax()) {
                $this->changeReportsStatus('App\\Models\\CompetitorReport', $employee->id, CompetitorReport::$types['price_comparison']);
            }

            if ($request->ajax()) {
                return _json('success', ['reports' => $reports]);
            }
            $this->data['reports'] = $reports;
            $this->data['employee'] = $employee;
            $this->data['page_title'] = 'مقارنة أسعار';
            $this->data['parent_tab'] = 'reports_section';
            $this->data['sub_parent_tab'] = 'competitor_reports_section';
            $this->data['tab'] = 'price_comparison_reports';
            return $this->_view('reports.price_comparison');
        } catch (\Exception $ex) {
            if ($request->ajax()) {
                $message = 'حدث خطأ ما !';
                return _json('error', $message, 400);
            }
            return $this->err404();
        }
    }

    public function promotionsReportsEmployees()
    {
        try {

            $this->data['employees'] = $this->getEmloyeeList('App\\Models\\CompetitorReport', 'competitors_reports', CompetitorReport::$types['promotions']);
            $this->data['route'] = 'supervisor.reports.employee_promotions_reports';
            $this->data['page_title'] = 'العروض الترويجية';
            $this->data['parent_tab'] = 'reports_section';
            $this->data['sub_parent_tab'] = 'competitor_reports_section';
            $this->data['tab'] = 'promotions_reports';
            return $this->_view('reports.employees_list');
        } catch (\Exception $ex) {
            return $this->err404();
        }
    }

    public function employeePromotionsReports(Request $request)
    {
        try {

            $result = $this->getEmployee($request);
            if (!$result instanceof User) {
                return $result;
            }
            $employee = $result;

            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if ($value) {
                        $this->data[$key] = $value;
                    }
                }
            }
            $reports =  $this->getCompetitiorsReports($employee, 'promotions');

            // change employee reports status to Read
            if (!$request->ajax()) {
                $this->changeReportsStatus('App\\Models\\CompetitorReport', $employee->id, CompetitorReport::$types['promotions']);
            }

            if ($request->ajax()) {
                return _json('success', ['reports' => $reports]);
            }
            $this->data['reports'] = $reports;
            $this->data['employee'] = $employee;
            $this->data['page_title'] = 'العروض الترويجية';
            $this->data['parent_tab'] = 'reports_section';
            $this->data['sub_parent_tab'] = 'competitor_reports_section';
            $this->data['tab'] = 'promotions_reports';
            return $this->_view('reports.promotions');
        } catch (\Exception $ex) {
            if ($request->ajax()) {
                $message = 'حدث خطأ ما !';
                return _json('error', $message, 400);
            }
            return $this->err404();
        }
    }

    public function areasReportsEmployees()
    {
        try {

            $this->data['employees'] = $this->getEmloyeeList('App\\Models\\CompetitorReport', 'competitors_reports', CompetitorReport::$types['areas']);
            $this->data['route'] = 'supervisor.reports.employee_areas_reports';
            $this->data['page_title'] = 'مساحات';
            $this->data['parent_tab'] = 'reports_section';
            $this->data['sub_parent_tab'] = 'competitor_reports_section';
            $this->data['tab'] = 'areas_reports';
            return $this->_view('reports.employees_list');
        } catch (\Exception $ex) {
            return $this->err404();
        }
    }

    public function employeeAreasReports(Request $request)
    {
        try {

            $result = $this->getEmployee($request);
            if (!$result instanceof User) {
                return $result;
            }
            $employee = $result;

            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if ($value) {
                        $this->data[$key] = $value;
                    }
                }
            }
            $reports =  $this->getCompetitiorsReports($employee, 'areas');
            // change employee reports status to Read
            if (!$request->ajax()) {
                $this->changeReportsStatus('App\\Models\\CompetitorReport', $employee->id, CompetitorReport::$types['areas']);
            }

            if ($request->ajax()) {
                return _json('success', ['reports' => $reports]);
            }
            $this->data['reports'] = $reports;
            $this->data['employee'] = $employee;
            $this->data['page_title'] = 'مساحات';
            $this->data['parent_tab'] = 'reports_section';
            $this->data['sub_parent_tab'] = 'competitor_reports_section';
            $this->data['tab'] = 'areas_reports';
            return $this->_view('reports.areas');
        } catch (\Exception $ex) {
            if ($request->ajax()) {
                $message = 'حدث خطأ ما !';
                return _json('error', $message, 400);
            }
            return $this->err404();
        }
    }

    public function reducedPricesReportsEmployees()
    {
        try {
            $this->data['employees'] = $this->getEmloyeeList('App\\Models\\CompetitorReport', 'competitors_reports', CompetitorReport::$types['reduced_prices']);
            $this->data['route'] = 'supervisor.reports.employee_reduced_prices_reports';
            $this->data['page_title'] = 'تقرير الأسعار المخفضة';
            $this->data['parent_tab'] = 'reports_section';
            $this->data['sub_parent_tab'] = 'competitor_reports_section';
            $this->data['tab'] = 'reduced_prices_reports';
            return $this->_view('reports.employees_list');
        } catch (\Exception $ex) {
            return $this->err404();
        }
    }

    public function employeeReducedPricesReports(Request $request)
    {
        try {

            $result = $this->getEmployee($request);
            if (!$result instanceof User) {
                return $result;
            }
            $employee = $result;

            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if ($value) {
                        $this->data[$key] = $value;
                    }
                }
            }
            $reports =  $this->getCompetitiorsReports($employee, 'reduced_prices');
            // change employee reports status to Read
            if (!$request->ajax()) {
                $this->changeReportsStatus('App\\Models\\CompetitorReport', $employee->id, CompetitorReport::$types['reduced_prices']);
            }

            if ($request->ajax()) {
                return _json('success', ['reports' => $reports]);
            }
            $this->data['reports'] = $reports;
            $this->data['employee'] = $employee;
            $this->data['page_title'] = 'تقرير الأسعار المخفضة';
            $this->data['parent_tab'] = 'reports_section';
            $this->data['sub_parent_tab'] = 'competitor_reports_section';
            $this->data['tab'] = 'reduced_prices_reports';
            return $this->_view('reports.reduced_prices');
        } catch (\Exception $ex) {
            if ($request->ajax()) {
                $message = 'حدث خطأ ما !';
                return _json('error', $message, 400);
            }
            return $this->err404();
        }
    }

    public function ordinaryReportsEmployees()
    {
        try {
            $this->data['employees'] = $this->getEmloyeeList('App\\Models\\CompetitorReport', 'competitors_reports', CompetitorReport::$types['ordinary']);
            $this->data['route'] = 'supervisor.reports.employee_ordinary_reports';
            $this->data['page_title'] = 'تقرير عادي';
            $this->data['parent_tab'] = 'reports_section';
            $this->data['sub_parent_tab'] = 'competitor_reports_section';
            $this->data['tab'] = 'ordinary_reports';
            return $this->_view('reports.employees_list');
        } catch (\Exception $ex) {
            return $this->err404();
        }
    }

    public function employeeOrdinaryReports(Request $request)
    {
        try {

            $result = $this->getEmployee($request);
            if (!$result instanceof User) {
                return $result;
            }
            $employee = $result;

            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if ($value) {
                        $this->data[$key] = $value;
                    }
                }
            }
            $reports =  $this->getCompetitiorsReports($employee, 'ordinary');
            // change employee reports status to Read
            if (!$request->ajax()) {
                $this->changeReportsStatus('App\\Models\\CompetitorReport', $employee->id, CompetitorReport::$types['ordinary']);
            }

            if ($request->ajax()) {
                return _json('success', ['reports' => $reports]);
            }
            $this->data['reports'] = $reports;
            $this->data['employee'] = $employee;
            $this->data['page_title'] = 'تقرير عادي';
            $this->data['parent_tab'] = 'reports_section';
            $this->data['sub_parent_tab'] = 'competitor_reports_section';
            $this->data['tab'] = 'ordinary_reports';
            return $this->_view('reports.ordinary');
        } catch (\Exception $ex) {
            if ($request->ajax()) {
                $message = 'حدث خطأ ما !';
                return _json('error', $message, 400);
            }
            return $this->err404();
        }
    }

    /** ___________________________________________________________________ */
    private function getEmloyeeList($model, $table_name, $report_type)
    {
        $employees =  $model::join('users', function ($query) use ($table_name, $report_type) {
            $query->on('users.id', '=', "$table_name.employee_id")
                ->where('users.supervisor_id', $this->user->id)
                ->where("$table_name.type", $report_type)
                ->where("$table_name.id", DB::raw("(select max(id) from $table_name where employee_id = users.id and type = $report_type)"));
        })
            ->select("$table_name.employee_id", "$table_name.created_at", "$table_name.status as report_status", 'users.name')
            ->get();

        return $employees->map(function ($employee) {
            Carbon::setLocale('ar');
            return (object) [
                'id'            => encrypt($employee->employee_id),
                'report_date'         => $employee->created_at->diffForHumans(),
                'report_status'     => $employee->report_status,
                'name'     => $employee->name,
            ];
        });
    }

    private function getEmployee($request)
    {
        try {
            $employee_id = \Crypt::decrypt($request->input('employee'));
        } catch (Illuminate\Contracts\Encryption\DecryptException $ex) {
            if ($request->ajax()) {
                return _json('error', _lang('app.error_is_occured'), 400);
            }
            return $this->err404();
        }

        $employee = User::where('id', $employee_id)->where('supervisor_id', $this->user->id)->where('type', User::$types['employee'])->first();
        if (!$employee) {
            if ($request->ajax()) {
                return _json('error', _lang('app.not_found'), 404);
            }
            return $this->err404();
        }

        return $employee;
    }

    private function changeReportsStatus($model,$employee,$type)
    {
       $model::where('employee_id',$employee)->where('type',$type)->update(['status' => true]);
    }

    private function getCompetitiorsReports($employee,$type){
     return  CompetitorReport::transformCollection(CompetitorReport::join('stores', 'stores.id', '=', 'competitors_reports.store_id')
                                                                    ->join('competitors', 'competitors.id', '=', 'competitors_reports.competitor_id')
                                                                    ->where('competitors_reports.employee_id', $employee->id)
                                                                    ->where('competitors_reports.type', CompetitorReport::$types[$type])
                                                                    ->orderBy('competitors_reports.created_at', 'desc')
                                                                    ->select('competitors_reports.*', 'stores.title_ar as store', 'competitors.title_ar as competitor')
                                                                    ->paginate($this->reports_limit), 'Supervisor');
    }
}
