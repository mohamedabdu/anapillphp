<?php

namespace App\Http\Controllers\Supervisor;

use Illuminate\Http\Request;
use App\Models\VisitCancel;
use App\Models\Task;
use Validator;
use DB;

class CancelRequestsController extends SupervisorController
{ 
    private $status_rules = [
        'type' => 'required|in:visit,task',
        'status' => 'required|in:accepted,rejected'
    ];

    public function visitsCancelRequests(Request $request)
    {
        try {
            $cancel_requests = VisitCancel::transformCollection($this->getCancelRequests($request, 'visits'));
            if ($request->ajax()) {
               return _json('success',['cancel_requests' => $cancel_requests]);
            }
            $this->data['cancel_requests'] = $cancel_requests;
            $this->data['employees'] = $this->getSupervisorEmployees();
            $this->data['parent_tab'] = 'cancel_requests';
            $this->data['tab'] = 'tasks_cancel_requests';
            return $this->_view('cancel_requests.visits');
        } catch (\Exception $ex) {
            if ($request->ajax()) {
                $message = 'لقد حدث خطأ ما!';
                return _json('error', $message, 400);
            }
            return $this->err404();
        }
    }

    public function visitsTasksCancelRequests(Request $request)
    {
        try {
            $cancel_requests = VisitTaskCancel::transformCollection($this->getCancelRequests($request, 'tasks'));
            if ($request->ajax()) {
               return _json('success',['cancel_requests' => $cancel_requests]);
            }
            $this->data['cancel_requests'] = $cancel_requests;
            $this->data['employees'] = $this->getSupervisorEmployees();
            $this->data['tasks'] = Task::select('id','title_ar as title')->get();
            $this->data['parent_tab'] = 'cancel_requests';
            $this->data['tab'] = 'tasks_cancel_requests';
            return $this->_view('cancel_requests.visits');
        } catch (\Exception $ex) {
            if ($request->ajax()) {
                $message = 'لقد حدث خطأ ما!';
                return _json('error', $message, 400);
            }
            return $this->err404();
        }
    }

     public function updateRequestStatus(Request $request,$id)
    {
        try {
            $validator = Validator::make($request->all(), $this->status_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            try {
                $id = \Crypt::decrypt($id);
            } catch (Illuminate\Contracts\Encryption\DecryptException $e) {
                $message = 'لقد حدث خطأ ما!';
                return _json('error', $message, 400);
            }

            if ($request->input('type') == 'visit')
            {
               $model = 'App\\Models\\VisitCancel';
               $basic_model = 'App\\Models\\VisitDetails';
               $table_name = 'visit_cancels';
               
            }
            else if ($request->input('type') == 'task')
            {
                $model = 'App\\Models\\VisitTaskCancel';
                $basic_model = 'App\\Models\\VisitTaskDetails';
                $table_name = 'visit_task_cancels';
            }

            $cancel_request = $model::join('users','users.id','=', "$table_name.employee_id")
                                            ->where('users.supervisor_id',$this->user->id)
                                            ->where("$table_name.id",$id)
                                            ->select("$table_name.*")
                                            ->first();
            if (!$cancel_request) {
                $message = 'غير موجود';
                return _json('error', $message, 404);
            }
            DB::beginTransaction();

            $cancel_request->status = $model::$statuses[$request->input('status')];
            $cancel_request->save();

            if ($request->input('status') == 'accepted') {
                $result =  $basic_model::where('visit_details_id', $cancel_request->visit_details_id);
                                        if ($request->input('type') == 'task') {
                                            $result->where('task_id',$cancel_request->task_id);
                                        }
                $result = $result->update(['status' => $basic_model::$statuses['CANCELLED']]);
            }
            
            DB::commit();
            $message = 'تم التحديث بنجاح';
            return _json('success',$message);
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'لقد حدث خطأ ما!';
            return _json('error', $message, 400);
        }
    }

    public function getCancelRequests($request,$type)
    {
        
        if ($type == 'visits') {
            $model = 'App\\Models\\VisitCancel';
            $table_name = 'visit_cancels';
        } else if ($type == 'tasks') {
            $model = 'App\\Models\\VisitTaskCancel';
            $table_name = 'visit_task_cancels';
        }

        $columns = [
            "$table_name.*",
            'visit_details.code',
            'users.name as employee'
        ];

        $cancel_requests = $model::join('users','users.id','=', "$table_name.employee_id")
                                        ->join('visit_details', 'visit_details.id','=', "$table_name.visit_details_id");
                                        if ($type = 'tasks') {
                                            $cancel_requests->join('tasks', 'tasks.id','=', "$table_name.task_id");
                                            array_push($columns,'tasks.title_ar as task');
                                        }
                                        $cancel_requests->where('users.supervisor_id',$this->user->id)
                                                        ->orderBy('created_at','desc');
                                        if ($request->input('status')) {
                                            $cancel_requests->where("$table_name.status",$model::$statuses[$request->input('status')]);
                                        }
                                        if ($request->input('employee')) {
                                            $employee_id = \Crypt::decrypt($request->input('employee'));
                                            $cancel_requests->where("$table_name.employee_id", $employee_id);
                                        }
                                        if ($request->input('from')) {
                                            $cancel_requests->where("$table_name.created_at", '>=', $request->input('from'));
                                        }
                                        if ($request->input('to')) {
                                            $cancel_requests->where("$table_name.to", '<=', $request->input('to'));
                                        }

        $cancel_requests =  $cancel_requests->select($columns)
                                            ->paginate($this->limit);
        return $cancel_requests;
    }
}
