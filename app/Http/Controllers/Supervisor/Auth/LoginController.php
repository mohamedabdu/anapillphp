<?php

namespace App\Http\Controllers\Supervisor\Auth;

use DB;
use Auth;
use Validator;
use App\User;
use App\Scopes\SupervisorScope;
use Illuminate\Http\Request;
use App\Models\UserAttendance;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use function Helper\Common\base64ToImage;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/supervisor';

    private $step_one_rules = array(
        'mobile' => 'required',
        'password' => 'required'
    );
    
    private $rules = array(
        'mobile' => 'required',
        'password' => 'required',
        'image' => 'required',
        'lat' => 'required',
        'lng' => 'required'
    );


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:supervisor', ['except' => ['logout']]);
    }

    public function showLoginForm()
    {
        return view('main_content.supervisor.auth.login');
    }

    public function login(Request $request)
    {
        try {
                if ($request->input('step') == 1) {
                    $rules = $this->step_one_rules;
                } else if($request->input('step') == 3) {
                    $rules = $this->rules;
                }
                if ( in_array($request->input('step'),[1,3])) {
                    $validator = Validator::make($request->all(), $rules);
                    if ($validator->fails()) {
                        $errors = $validator->errors()->toArray();
                        return _json('error', $errors);
                    }
                }
                
                $mobile = $request->input('mobile');
                $password = $request->input('password');
                $supervisor = $this->checkAuth($mobile);

                if ($supervisor) {

                    // check supervisor work days to allow him to login to the system
                    $today = date('D');
                    if (!in_array($today,json_decode($supervisor->work_days))) {
                        $message = 'لا يمكنك الدخول إلي لوحة التحكم الخاصة بك اليوم حيث انه ليس ضمن ايام عملك';
                        return _json('error', $message);
                    }

                    //check supervisor whether finished his attendace before or note
                    if ($this->checkUserFinishAttendance($supervisor)) {
                       $message = 'لقد قمت بإنهاء دوامك اليوم! لا يمكنك تسجيل الدخول مرة أخرى';
                        return _json('error', $message);
                    }
                    
                    if ($request->input('step') == 1) {
                        if (password_verify($password, $supervisor->password)) {
                            if ($check = $this->checkUserAttendance($supervisor)) {
                                Auth::guard('supervisor')->login($supervisor);
                                return _json('success', ['redirect' => route('supervisor.dashboard')]);
                            }
                            return _json('success', $supervisor->name);
                        }
                        $message = 'البيانات المدخلة غير صحيحة';
                        return _json('error', $message);
                    } else if ($request->input('step') == 3) {
                        $message = date('Y-m-d H:i');
                        return _json('success', $message);
                    } else if ($request->input('step') == 4) {
                        $this->createUserAttendance($request, $supervisor);
                        Auth::guard('supervisor')->login($supervisor);
                        return _json('success',  route('supervisor.dashboard'));
                    }
                }else{
                    $message = 'البيانات المدخلة غير صحيحة';
                    return _json('error', $message);
                }
                     
        } catch (\Exception $ex) {
            DB::rollback();
            $message = 'لقد حدث خطأ ما , أعد المحاولة لاحقاً';
            return _json('error', $message, 400);
        }
        
    }

    private function checkAuth($mobile)
    {
        $supervisor = User::where('users.type', SupervisorScope::$typeOfSupervisor)
                        ->where('users.mobile',$mobile)
                        ->where('users.active',1)
                        ->select('users.*')
                        ->first();
        return $supervisor ?: false;
    }

    private function createUserAttendance($request,$user)
    {
        $check = $this->checkUserAttendance($user);
        if (!$check) {
            $userAttendance = new UserAttendance();
            $userAttendance->user_id = $user->id;
            $userAttendance->start_date = date('Y-m-d H:i:s');
            $userAttendance->image =  base64ToImage(str_replace('data:image/jpeg;base64,', '', $request->input('image')));
            $userAttendance->lat = $request->input('lat');
            $userAttendance->lng = $request->input('lng');
            $userAttendance->save();
        }
    }

    private function checkUserAttendance($user)
    {
        $userAttendance = UserAttendance::where('user_id', $user->id)->where('is_end', false)->first();
        return $userAttendance ? : false;
    }

    private function checkUserFinishAttendance($user)
    {
        $userAttendance = UserAttendance::where('user_id', $user->id)
                                        ->whereDate('end_date',date('Y-m-d'))
                                        ->where('is_end', true)
                                        ->first();
        return $userAttendance ? true : false;
    }

    private function closeUserAttendance()
    {
        $userAttendance =  UserAttendance::where('user_id', Auth::guard('supervisor')->user()->id)->where('is_end',false)->first();
        $userAttendance->end_date = date('Y-m-d H:i:s');
        $userAttendance->is_end = true;
        $userAttendance->save();
    }

    public function logout(Request $request)
    {
       try {
            $this->closeUserAttendance();
            Auth::guard('supervisor')->logout();
            $redirect = route('supervisor.login');
            return _json('success', $redirect);
       } catch (\Exception $ex) {
            $message = 'لقد حدث خطأ ما , أعد المحاولة لاحقاً';
            return _json('error', $message, 400);
       }
       
    }
}
