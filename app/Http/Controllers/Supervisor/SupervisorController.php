<?php

namespace App\Http\Controllers\Supervisor;

use DB;
use App\User;
use App\Models\Report;
use App\Models\VisitCancel;
use Illuminate\Http\Request;
use App\Models\EmployeeHoliday;
use App\Models\VisitTaskCancel;
use App\Models\CompetitorReport;
use function Helper\Common\upload;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Encryption\DecryptException;


class SupervisorController extends Controller
{

    protected $languages = array(
        'ar' => 'arabic',
        'en' => 'english'
    );

    protected $lang_code = 'ar';
    protected $user;
    protected $data = array();
    protected $limit = 1;
    protected $reports_limit = 2;

    public function __construct()
    {
        $this->middleware('auth:supervisor');
        $this->setLang();
        $this->getCookieLangAndSetLocale();
        if ($this->user = Auth::guard('supervisor')->user()) {
            $this->data['user'] = $this->user;
            $this->data['languages'] = $this->languages;
            $this->getStatisticsCount();
        }
    }
        

    protected function setLang($lang = "ar")
    {
        $lang_code = $lang;
        $long = 7 * 60 * 24;
        \Cookie::queue('SupervisorLang', $lang_code, $long);
    }
    private function getStatisticsCount()
    {
        $statistics_counters = DB::select('
            select
            (select count(*) from reports join users on users.id = reports.employee_id and users.supervisor_id = '.$this->user->id.' where reports.status = false and reports.type = '.Report::$types['products_almost_finish'].') as unread_products_almost_finish_reports_count,
            (select count(*) from reports join users on users.id = reports.employee_id and users.supervisor_id = '.$this->user->id.' where reports.status = false and reports.type = '.Report::$types['manual']. ') as unread_manual_reports_count,
            (select count(*) from competitors_reports join users on users.id = competitors_reports.employee_id and users.supervisor_id = '.$this->user->id.' where competitors_reports.status = false and competitors_reports.type = '.CompetitorReport::$types['price_comparison'].') as unread_price_comparison_reports_count,
            (select count(*) from competitors_reports join users on users.id = competitors_reports.employee_id and users.supervisor_id = '.$this->user->id.' where competitors_reports.status = false and competitors_reports.type = '.CompetitorReport::$types['promotions'].') as unread_promotions_reports_count,
            (select count(*) from competitors_reports join users on users.id = competitors_reports.employee_id and users.supervisor_id = '.$this->user->id.' where competitors_reports.status = false and competitors_reports.type = '.CompetitorReport::$types['areas']. ') as unread_areas_reports_count,
            (select count(*) from competitors_reports join users on users.id = competitors_reports.employee_id and users.supervisor_id = '.$this->user->id.' where competitors_reports.status = false and competitors_reports.type = '.CompetitorReport::$types['reduced_prices']. ') as unread_reduced_prices_reports_count,
            (select count(*) from competitors_reports join users on users.id = competitors_reports.employee_id and users.supervisor_id = '.$this->user->id.' where competitors_reports.status = false and competitors_reports.type = '.CompetitorReport::$types['ordinary']. ') as unread_ordinary_reports_count,
            (select count(*) from employee_holidays join users on users.id = employee_holidays.employee_id and users.supervisor_id = '.$this->user->id.' where employee_holidays.status = '.EmployeeHoliday::$statuses['pending'].' and employee_holidays.start_date >= "'.date('Y-m-d'). '") as pending_holiday_requests_count,
            (select count(*) from visit_cancels join users on users.id = visit_cancels.employee_id and users.supervisor_id = '.$this->user->id. ' where visit_cancels.status = '.VisitCancel::$statuses['pending']. ') as pending_visits_cancel_requests_count,
            (select count(*) from visit_task_cancels join users on users.id = visit_task_cancels.employee_id and users.supervisor_id = '.$this->user->id. ' where visit_task_cancels.status = '.VisitTaskCancel::$statuses['pending'].') as pending_tasks_cancel_requests_count
        ');

        $this->data['statistics_counters'] = $statistics_counters[0];
    }

    protected function getSupervisorEmployees()
    {
        return User::where('supervisor_id',$this->user->id)->get();
    }
    protected function getCookieLangAndSetLocale()
    {
        if (\Cookie::get('SupervisorLang') !== null) {
            try {
                $this->lang_code = \Crypt::decrypt(\Cookie::get('SupervisorLang'));
            } catch (DecryptException $ex) {
                $this->lang_code = 'ar';
            }
        } else {
            $this->lang_code = 'ar';
        }
        $this->data['lang_code'] = $this->lang_code;
        app()->setLocale($this->lang_code);
    }

    protected function uploadFile(Request $request, $field, $thum = true)
    {
        if ($request->hasFile($field)) {
            $uploaded = upload($request->file($field), $thum);
            if ($thum) {
                $file = $uploaded['thum'];
            } else {
                $file = $uploaded['img'];
            }
            return $file;
        } else {
            return "";
        }
    }

    public function _view($main_content, $type = 'supervisor')
    {
        $main_content = "main_content/$type/$main_content";
        return view($main_content, $this->data);
    }

    public function err404()
    {
        return $this->_view('err404','backend');
    }
}
