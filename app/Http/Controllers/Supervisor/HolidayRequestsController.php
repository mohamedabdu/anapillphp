<?php

namespace App\Http\Controllers\Supervisor;

use Illuminate\Http\Request;
use App\Models\EmployeeHoliday;
use Validator;

class HolidayRequestsController extends SupervisorController
{

    private $holiday_request_status_rules = [
        'status' => 'required|in:accepted,rejected'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->data['tab'] = 'holiday_requests';
    }

    public function index(Request $request)
    {
        try {
            $holiday_requests = $this->getHolidayRequests($request);                
            if ($request->ajax()) {
                return _json('success',['holiday_requests' => $holiday_requests]);
            }
            $this->data['holiday_requests'] = $holiday_requests;
            $this->data['employees'] = $this->getSupervisorEmployees();
            return $this->_view('holiday_requests.index');
        } catch (\Exception $ex) {
            if ($request->ajax()) {
                $message = 'لقد حدث خطأ ما!';
                return _json('error', $message, 400);
            }
            return $this->err404();
        }
    }

    public function show(Request $request, $id)
    {
        try {
            try {
                $id = \Crypt::decrypt($id);
            } catch (Illuminate\Contracts\Encryption\DecryptException $e) {
                return $this->err404();
            }

            $holiday_request = $this->getHolidayRequests($request,$id);
            if (!$holiday_request) {
                return $this->err404();
            }
            $this->data['holiday_request'] = $holiday_request;
            return $this->_view('holiday_requests.show');
        } catch (\Exception $ex) {
            return $this->err404();
        }
    
    }

    public function update(Request $request, $id)
    {
        try {
            
            if ($request->input('status') == 'rejected') {
                $this->holiday_request_status_rules['rejection_reason'] = 'required';
            }
            $validator = Validator::make($request->all(), $this->holiday_request_status_rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            }
            
            try {
                $id = \Crypt::decrypt($id);
            } catch (Illuminate\Contracts\Encryption\DecryptException $e) {
                $message = 'لقد حدث خطأ ما!';
                return _json('error',$message,400);
            }

            $holiday_request = EmployeeHoliday::join('users', 'users.id', '=', 'employee_holidays.employee_id')
                                                ->where('users.supervisor_id', $this->user->id)
                                                ->where('employee_holidays.id',$id)
                                                ->first();
            if (!$holiday_request) {
                $message = 'غير موجود';
                return _json('error', $message, 404);
            }

            $holiday_request->status = EmployeeHoliday::$status[$request->input('status')];
            if ($request->input('status') == 'rejected') {
                $holiday_request->rejection_reason = $request->input('rejection_reason');
            }
            $holiday_request->save();
            $message = 'تم إخطار الموظف';
            return _json('success', $message);
        } catch (\Exception $ex) {
            $message = 'لقد حدث خطأ ما!';
            return _json('error',$message,400);
        }
    }

    private function getHolidayRequests($request,$id = false)
    {
        $holiday_requests = EmployeeHoliday::join('users','users.id','=', 'employee_holidays.employee_id')
                                                ->where('users.supervisor_id',$this->user->id);
                                                if (!$id) {
                                                    if ($request->input('employee')) {
                                                        $employee_id = \Crypt::decrypt($request->input('employee'));
                                                        $holiday_requests->where('employee_holidays.employee_id', $employee_id);
                                                    }
                                                    if ($request->input('status')) {
                                                        $holiday_requests->where('employee_holidays.status', EmployeeHoliday::$statuses[$request->input('status')]);
                                                    }
                                                    if ($request->input('from')) {
                                                        $holiday_requests->where('employee_holidays.created_at','>=', $request->input('from'));
                                                    }
                                                    if ($request->input('to')) {
                                                        $holiday_requests->where('employee_holidays.created_at','<=', $request->input('to'));
                                                    }
                                                    if ($request->input('start_from')) {
                                                        $holiday_requests->where('employee_holidays.start_date','>=', $request->input('start_from'));
                                                    }
                                                    if ($request->input('start_to')) {
                                                        $holiday_requests->where('employee_holidays.start_date','<=', $request->input('start_to'));
                                                    }
                                                    if ($request->input('end_from')) {
                                                        $holiday_requests->where('employee_holidays.end_date','>=', $request->input('end_from'));
                                                    }
                                                    if ($request->input('end_to')) {
                                                        $holiday_requests->where('employee_holidays.end_date','<=', $request->input('end_to'));
                                                    }
                                                }
                                                

        $holiday_requests->orderBy('employee_holidays.created_at','desc')
                         ->select('employee_holidays.*','users.name as employee');
        if ($id) {
            $holiday_requests =  $holiday_requests->where('employee_holidays.id',$id)
                                                    ->first();
            if ($holiday_requests) {
                return $holiday_requests->transformSupervisor();
            }
            return false;
        }
        $holiday_requests =  $holiday_requests->paginate($this->limit);
        return EmployeeHoliday::transformCollection($holiday_requests,'Supervisor');
    }

   
}
