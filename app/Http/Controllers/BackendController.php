<?php

namespace App\Http\Controllers;

use App\Scopes\CompanyScope;
use App\Traits\Basic;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class BackendController extends Controller
{

    use Basic;
   
    protected $lang_code;
    protected $user;
    protected $data = array();

    public function __construct() {
        
        $this->middleware('auth');
        //$this->middleware('role');
        $this->getCookieLangAndSetLocale();
        $this->user = Auth::user();
        $this->data['user'] = $this->user;
        $this->data['languages'] = $this->languages;
    }

    
    protected function getCookieLangAndSetLocale()
    {
        if (\Cookie::get('AdminLang') !== null) {
            try {
                $this->lang_code = \Crypt::decrypt(\Cookie::get('AdminLang'));
            } catch (DecryptException $ex) {
                $this->lang_code = 'en';
            }
        } else {
            $this->lang_code = 'en';
        }
        $this->data['lang_code'] = $this->lang_code;
        app()->setLocale($this->lang_code);
    }
    public function uploadFile(Request $request,$field , $thum = true)
    {
        if ($request->hasFile($field)) {
            $uploaded = upload($request->file($field), $thum);
            if ($thum) {
                $file = $uploaded['thum'];
            } else {
                $file = $uploaded['img'];
            }
            return $file;
        } else {
            return "";
        }
    }

    protected function checkCreatedBy($data,$table_name) {
        $column = $table_name.".created_by";
        $super_parent_id = explode(",", $this->user->parents_id)[0];
        
        if ($super_parent_id) {
            $data = $data->where(function ($query) use ($column,$super_parent_id) {
                $query->where($column, $super_parent_id)
                    ->orWhereIn($column, function ($query, $super_parent_id) {
                        $query->select('id')->from('users')
                            ->whereRaw('FIND_IN_SET(?,parents_ids) > 0', $super_parent_id);
                    });
            });
        }
        else{
            $data = $data->where(function ($query) use ($column) {
                $query->where($column, $this->user->id)
                      ->orWhereIn($column, function ($query) {
                        $query->select('id')->from('users')
                            ->whereRaw('FIND_IN_SET(?,parents_ids) > 0', $this->user->id);
                    });
            });
        }
                               
        return $data;
    }

    protected function _view($main_content, $type = 'backend') {
        $main_content = "main_content/$type/$main_content";
        return view($main_content, $this->data);
    }

    public function err404()
    {
        return $this->_view('err404');
    }


}
