<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\VisitTaskReturnDetail;

class UpdateAdminRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        "name" => "required",
        "email" => "required|email|unique:users,email,".$this->segment(2),
            "mobile" => "required|numeric|regex:(05)|min:10",
    ];
    }
}
