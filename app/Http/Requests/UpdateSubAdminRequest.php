<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\VisitTaskReturnDetail;

class UpdateSubAdminRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        "name" => "required|max:25",
        "email" => "required|email|unique:users,email,".$this->segment(3),
            "mobile" => "required|numeric|regex:(05)|digits:10",
    ];
    }
}
