<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    public function handle($request, Closure $next, $segment)
    {

        if (!\Permissions::check($segment)) {
            if ($request->ajax()) {
                App()->abort(403, 'Access denied');
            } else {
                return redirect()->route('admin.error');
            }
        }
        return $next($request);
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    /*public function handle($request, Closure $next)
    {

        if($request->segment(2) != null) {
            $segment = $request->segment(2);
        } else {
            return $next($request);
        }

        if($this->checkPublicSegments($segment)) {

            return $next($request);
        }

        $user = User::find(auth()->id());
        if($user->id == 1) {
            return $next($request);
        }

        if(!isset($user->userRole)) {
            auth()->logout();
            $request->session()->invalidate();

            return redirect('/');
        }

        $permissions = $user->userRole->permissions()->pluck("permission_id")->toArray();
        $permissionsTitle = Permission::whereIn("id",$permissions)->where("segment",$segment)->count();

        if($permissionsTitle > 0) {
            return $next($request);
        } else {
            auth()->logout();
            $request->session()->invalidate();

            return redirect('/');
        }
    }
    public function checkPublicSegments($segment) {
        $segments = ["countries","regions","cities","categories","subCategories","change_sidebar_state"];
        if(in_array($segment,$segments)) {
            return true;
        } else {
            return false;
        }
    }*/
}
