<?php


use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/





//Route::get('apiKey/{device_id}', 'ApiController@getApiKey');
//Route::get('api_access/apiKey/{device_id}', 'ApiController@getApiKey');


\App\Traits\Api\RegisterTrait::routesRegister();
\App\Traits\Api\LoginTrait::routesAgentLogin();
\App\Traits\Api\LoginTrait::routesSupervisorLogin();
\App\Traits\Api\PasswordTrait::routesPassword();
\App\Traits\Api\PassportTrait::routesPassport();


Route::get('configs', 'ConfigAPIController@index');
Route::get('store/products', 'StoreAPIController@products');
Route::get('notifications', 'NotificationAPIController@index');
Route::group(["prefix"=> "agent"], function () {
    Route::group(["middleware"=>"auth:api"], function () {

        Route::post('attendance', 'Agent\AgentAPIController@attendance');
        Route::post('holiday/request', 'Agent\AgentAPIController@holidayRequest');
        Route::get('search', 'Agent\AgentAPIController@search');
        Route::get('next/visits', 'Agent\AgentAPIController@visits');
        Route::get('start/visit/{id}', 'Agent\AgentAPIController@startVisit');
        Route::post('start/task/arrange/chips/{id}', 'Agent\AgentAPIController@taskArrangeChips');
        Route::post('start/task/return/{id}', 'Agent\AgentAPIController@taskReturned');
        Route::post('start/task/inventory/general/{id}', 'Agent\AgentAPIController@taskInventoryGeneral');
        Route::post('start/task/inventory/details/{id}', 'Agent\AgentAPIController@taskInventoryDetail');
        Route::post('start/task/report/price/{id}', 'Agent\AgentAPIController@taskReportPrice');
        Route::post('start/task/area/{id}', 'Agent\AgentAPIController@taskArea');
        Route::post('start/task/bill/{id}', 'Agent\AgentAPIController@taskBill');
        Route::post('start/task/promo/{id}', 'Agent\AgentAPIController@taskPromo');
        Route::get('cancel/visit/{id}', 'Agent\AgentAPIController@cancelVisit');
        Route::get('end/visit/{id}', 'Agent\AgentAPIController@endVisit');
        Route::post('update', 'UserController@update');

    });
});




Route::resource('countries', 'CountryAPIController');

Route::resource('regions', 'RegionAPIController');

Route::resource('cities', 'CityAPIController');

Route::resource('return_reasons', 'returnReasonAPIController');

Route::resource('categories', 'CategoryAPIController');

Route::resource('sub_categories', 'SubCategoryAPIController');

Route::resource('promos', 'PromoAPIController');

Route::resource('promo_details', 'PromoDetailAPIController');


Route::resource('store_categories', 'StoreCategoryAPIController');

Route::resource('store_images', 'StoreImageAPIController');

Route::resource('store_work_days', 'StoreWorkDayAPIController');

Route::resource('competitors', 'CompetitorAPIController');

Route::resource('products', 'ProductAPIController');

Route::resource('product_images', 'ProductImageAPIController');

Route::resource('product_competitors', 'ProductCompetitorAPIController');

Route::resource('product_vs_products', 'ProductVsProductAPIController');

Route::resource('supervisor_agents', 'SupervisorAgentAPIController');

Route::resource('agent_work_days', 'AgentWorkDayAPIController');

Route::resource('tasks', 'TaskAPIController');

Route::resource('task_instructions', 'TaskInstructionAPIController');

Route::resource('task_instruction_images', 'TaskInstructionImageAPIController');

Route::resource('visits', 'VisitAPIController');

Route::resource('visit_tasks', 'VisitTaskAPIController');

Route::resource('visit_stores', 'VisitStoreAPIController');

Route::resource('visit_agents', 'VisitAgentAPIController');

Route::resource('visit_details', 'VisitDetailAPIController');

Route::resource('visit_cancels', 'VisitCancelAPIController');

Route::resource('visit_task_details', 'VisitTaskDetailAPIController');

Route::resource('visit_task_detail_images', 'VisitTaskDetailImageAPIController');

Route::resource('visit_task_return_details', 'VisitTaskReturnDetailAPIController');

Route::resource('visit_task_inventory_details', 'VisitTaskInventoryDetailAPIController');

Route::resource('visit_task_area_details', 'VisitTaskAreaDetailAPIController');

Route::resource('visit_task_bill_details', 'VisitTaskBillDetailAPIController');

Route::resource('visit_task_promo_details', 'VisitTaskPromoDetailAPIController');

Route::resource('user_attendances', 'UserAttendanceAPIController');

Route::resource('agent_holidays', 'AgentHolidayAPIController');

Route::resource('agent_rates', 'AgentRateAPIController');

Route::resource('visit_task_cancels', 'VisitTaskCancelAPIController');

Route::resource('roles', 'RoleAPIController');

Route::resource('permissions', 'PermissionAPIController');

Route::resource('permission_roles', 'PermissionRoleAPIController');

Route::resource('reports', 'ReportAPIController');

Route::resource('report_details', 'ReportDetailAPIController');

Route::resource('report_detail_images', 'ReportDetailImageAPIController');

Route::resource('report_competitors', 'ReportCompetitorAPIController');