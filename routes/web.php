<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('admin', function () {
    return redirect('admin/home');
});

Route::group(['namespace' => 'Employee','prefix' => 'employee'], function () {

    Route::post('change_sidebar_state', 'AjaxController@change_sidebar_state');
    Route::post('delete_image', 'AjaxController@deleteImage');
    Route::post('competitor_products', 'AjaxController@getCompetitorProducts');
    Route::post('store_categories', 'AjaxController@getStoreCategories');
    
    

    Route::get('/',  'HomeController@index')->name('employee.dashboard');
    Route::get('search',  'SearchController@index')->name('employee.search');
    Route::post('search',  'SearchController@search');

    Route::get('profile',  'ProfileController@index')->name('employee.profile');
    Route::get('daily_report',  'ProfileController@showDailyReport')->name('employee.daily_report');

    Route::post('update_profile',  'ProfileController@update_profile');
    Route::post('send_holiday_request',  'ProfileController@holidayRequest');

    Route::get('visits/{code}','VisitsController@show')->name('employee.visits.show');
    Route::get('visits/report/{code}', 'VisitsController@showVisitReport')->name('employee.visits.report');
    Route::post('visits/start_visit', 'VisitsController@startVisit');
    Route::post('visits/cancle_visit', 'VisitsController@cancelVisit');
    Route::post('visits/finish_visit', 'VisitsController@finishVisit');
    Route::post('visits/add_task', 'VisitsController@addTask');
    Route::post('visits/cancel_task', 'VisitsController@cancelTask');
   
    

    Route::get('visits/tasks/{task}/description', 'TasksController@showTaskDescription')->name('employee.visits.tasks.description');


    Route::post('visits/start_task', 'TasksController@startTask');
    Route::post('visits/finish_task', 'TasksController@finishTask');

    Route::get('visits/{code}/organizing_shelves', 'TasksController@showOrganizingShelvesTaskForm')->name('employee.tasks.organizing_shelves');
    Route::post('visits/organizing_shelves', 'TasksController@OrganizingShelvesTask');

    Route::get('visits/{code}/returns', 'TasksController@showReturnsTaskForm')->name('employee.tasks.returns');
    Route::post('visits/returns', 'TasksController@ReturnsTask');

    Route::get('visits/{code}/general_inventory', 'TasksController@showGeneralInventoryTaskForm')->name('employee.tasks.general_inventory');
    Route::post('visits/general_inventory', 'TasksController@generalInventory');

    Route::get('visits/{code}/detailed_inventory', 'TasksController@showDetailedInventoryTaskForm')->name('employee.tasks.detailed_inventory');
    Route::post('visits/detailed_inventory', 'TasksController@DetailedInventory');

    Route::get('visits/{code}/price_report', 'TasksController@showPriceReportTaskForm')->name('employee.tasks.price_report');
    Route::post('visits/price_report', 'TasksController@priceReport');
    

    Route::get('visits/{code}/areas_report', 'TasksController@showAreasReportTaskForm')->name('employee.tasks.areas_report');
    Route::post('visits/areas_report', 'TasksController@areasReport');


    Route::get('visits/{code}/receiving_order', 'TasksController@showReceivingOrderTaskForm')->name('employee.tasks.receiving_order');
    Route::post('visits/receiving_order', 'TasksController@receivingOrder');

    Route::get('visits/{code}/check_promotions', 'TasksController@showCheckPromotionsTaskForm')->name('employee.tasks.check_promotions');
    Route::post('visits/check_promotions', 'TasksController@checkPromotions');

    
    

    Route::get('reports/products_almost_finish_report',  'ReportsController@showProductsAlmostFinishForm')->name('employee.reports.products_almost_finish_report');
    Route::post('reports/products_almost_finish_report',  'ReportsController@storeProductsAlmostFinishReport');

    Route::get('reports/manual_report',  'ReportsController@showManualReportForm')->name('employee.reports.manual_report');
    Route::post('reports/manual_report',  'ReportsController@storeManualReport');

    Route::get('reports/price_comparison_report',  'ReportsController@showPriceComparisonReportForm')->name('employee.reports.price_comparison_report');
    Route::post('reports/price_comparison_report',  'ReportsController@storePriceComparisonReport');

    Route::get('reports/promotions_report',  'ReportsController@showPromotionsReportForm')->name('employee.reports.promotions_report');
    Route::post('reports/promotions_report',  'ReportsController@storePromotionsReport');

    Route::get('reports/areas_report',  'ReportsController@showAreasReportForm')->name('employee.reports.areas_report');
    Route::post('reports/areas_report',  'ReportsController@storeAreasReport');

    Route::get('reports/reduced_prices_report',  'ReportsController@showReducedPricesReportForm')->name('employee.reports.reduced_prices_report');
    Route::post('reports/reduced_prices_report',  'ReportsController@storeReducedPricesReport');

    Route::get('reports/ordinary_report',  'ReportsController@showOrdinaryReportForm')->name('employee.reports.ordinary_report');
    Route::post('reports/ordinary_report',  'ReportsController@storeOrdinaryReport');


    Route::group(['namespace' => 'Auth'], function () {
        // login
        Route::get('login',  'LoginController@showLoginForm')->name('employee.login');
        Route::post('login', 'LoginController@login');

        //logout
        Route::post('logout', 'LoginController@logout')->name('employee.logout');

        // Password Reset Routes...
        Route::get('password/reset', 'ForgotPasswordController@showPasswordResetForm')->name('employee.password.reset');
        Route::post('password/resend_code', 'ForgotPasswordController@resendCode');
        Route::post('password/reset', 'ForgotPasswordController@reset');
    });
});

Route::group(['namespace' => 'Supervisor', 'prefix' => 'supervisor'], function () {

    Route::post('change_sidebar_state', 'AjaxController@change_sidebar_state');

    Route::get('/',  'HomeController@index')->name('supervisor.dashboard');
   
    /** profile */
    Route::get('profile',  'ProfileController@index')->name('supervisor.profile');
    Route::post('update_profile',  'ProfileController@update_profile');

    /** holiday request */
    Route::get('holiday_requests', 'HolidayRequestsController@index')->name('supervisor.holiday_requests');
    Route::get('holiday_requests/{holiday_request}', 'HolidayRequestsController@show')->name('supervisor.holiday_requests.show');
    Route::post('holiday_requests/{holiday_request}/update', 'HolidayRequestsController@update');
    /** end holiday request */

    /** cancel requests */
    Route::get('visits_cancel_requests', 'CancelRequestsController@visitsCancelRequests')->name('supervisor.visits_cancel_requests');
    Route::get('tasks_cancel_requests', 'CancelRequestsController@visitsTasksCancelRequests')->name('supervisor.tasks_cancel_requests');
    Route::post('cancel_requests/{cancel_request}/update', 'CancelRequestsController@updateRequestStatus');
    /** end cancel requests */

    /** reports */
    Route::get('reports/products_almost_finish_reports',  'ReportsController@productsAlmostFinishedReportsEmployees')->name('supervisor.reports.products_almost_finish_reports');
    Route::get('reports/employee_products_almost_finish_reports',  'ReportsController@employeeProductsAlmostFinishedReports')->name('supervisor.reports.employee_products_almost_finish_reports');

    Route::get('reports/manual_reports',  'ReportsController@manualReportsEmployees')->name('supervisor.reports.manual_reports');
    Route::get('reports/employee_manual_reports',  'ReportsController@employeeManualReports')->name('supervisor.reports.employee_manual_reports');

    Route::get('reports/price_comparison_reports',  'ReportsController@priceComparisonReportsEmployees')->name('supervisor.reports.price_comparison_reports');
    Route::get('reports/employee_price_comparison_reports',  'ReportsController@employeePriceComparisonReports')->name('supervisor.reports.employee_price_comparison_reports');

    Route::get('reports/promotions_reports',  'ReportsController@promotionsReportsEmployees')->name('supervisor.reports.promotions_reports');
    Route::get('reports/employee_promotions_reports',  'ReportsController@employeePromotionsReports')->name('supervisor.reports.employee_promotions_reports');

    Route::get('reports/areas_reports',  'ReportsController@areasReportsEmployees')->name('supervisor.reports.areas_reports');
    Route::get('reports/employee_areas_reports',  'ReportsController@employeeAreasReports')->name('supervisor.reports.employee_areas_reports');

    Route::get('reports/reduced_prices_reports',  'ReportsController@reducedPricesReportsEmployees')->name('supervisor.reports.reduced_prices_reports');
    Route::get('reports/employee_reduced_prices_reports',  'ReportsController@employeeReducedPricesReports')->name('supervisor.reports.employee_reduced_prices_reports');

    Route::get('reports/ordinary_reports',  'ReportsController@ordinaryReportsEmployees')->name('supervisor.reports.ordinary_reports');
    Route::get('reports/employee_ordinary_reports',  'ReportsController@employeeOrdinaryReports')->name('supervisor.reports.employee_ordinary_reports');
    /** end reports */
    
    Route::group(['namespace' => 'Auth'], function () {
        // login
        Route::get('login',  'LoginController@showLoginForm')->name('supervisor.login');
        Route::post('login', 'LoginController@login');

        //logout
        Route::post('logout', 'LoginController@logout');

        // Password Reset Routes...
        Route::get('password/reset', 'ForgotPasswordController@showPasswordResetForm')->name('supervisor.password.reset');
        Route::post('password/resend_code', 'ForgotPasswordController@resendCode');
        Route::post('password/reset', 'ForgotPasswordController@reset');
    });
});

Route::group(['namespace' => 'Admin','prefix' => 'admin'], function () {

    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/error', 'AdminController@error')->name('admin.error');
    Route::get('change_lang', 'AjaxController@change_lang')->name('ajax.change_lang');
    Route::post('delete_image', 'AjaxController@deleteImage');
    Route::get('get_locations/{parent_id}', 'AjaxController@getLocations');
    Route::get('get_categories/{parent_id}', 'AjaxController@getCategories');
    Route::get('get_competitor_products/{competitor_id}', 'AjaxController@getCompetitorProducts');
    Route::post('remove_product_competitor/{competitor_id}', 'AjaxController@removeProductCompetitor');
    
    
    


    Route::get('profile', 'ProfileController@index')->name('admin.profile');
    Route::patch('profile', 'ProfileController@update');

    Route::resource('roles', 'RolesController');
    Route::post('roles/data', 'RolesController@data');

    Route::resource('admins', 'AdminsController');
    Route::post('admins/data', 'AdminsController@data');
    Route::post('admins/status/{id}', 'AdminsController@active');

    Route::resource('companies', 'CompaniesController');
    Route::post('companies/data', 'CompaniesController@data');
    Route::get('companies/status/{id}', 'CompaniesController@active');

    Route::resource('locations', 'LocationsController');
    Route::post('locations/data', 'LocationsController@data');

    Route::resource('retrieval_reasons', 'RetrievalReasonsController');
    Route::post('retrieval_reasons/data', 'RetrievalReasonsController@data');
    Route::post('retrieval_reasons/status/{id}', 'RetrievalReasonsController@active');

    Route::resource('store_types', 'StoreTypesController');
    Route::post('store_types/data', 'StoreTypesController@data');

    Route::resource('categories', 'CategoriesController');
    Route::post('categories/data', 'CategoriesController@data');

    Route::resource('competitors', 'CompetitorsController');
    Route::post('competitors/data', 'CompetitorsController@data');
    Route::post('competitors/status/{id}', 'CompetitorsController@active');

    Route::resource('competitor_products', 'CompetitorProductsController');
    Route::post('competitor_products/data', 'CompetitorProductsController@data');
    Route::post('competitor_products/status/{id}', 'CompetitorProductsController@active');

    Route::resource('products', 'ProductsController');
    Route::post('products/data', 'ProductsController@data');
    Route::post('products/status/{id}', 'ProductsController@active');

    Route::resource('promotions', 'PromotionsController');
    Route::post('promotions/data', 'PromotionsController@data');


    Route::resource('configs', 'ConfigController');
    Route::get('configs/data/table', 'ConfigController@data')->name('configs.ajax');
    Route::get('configs/delete/records', 'ConfigController@destroy')->name('configs.delete');


    Route::resource('notifications', 'NotificationController');
    Route::get('notifications/data/table', 'NotificationController@data')->name('notifications.ajax');
    Route::get('notifications/delete/records', 'NotificationController@destroy')->name('notifications.delete');
   



    Route::resource('promoDetails', 'PromoDetailController');
    Route::post('promoDetails/data/table', 'PromoDetailController@data')->name('promoDetails.ajax');
    Route::get('promoDetails/delete/records', 'PromoDetailController@destroy')->name('promoDetails.delete');


    Route::resource('stores', 'StoreController');
    Route::get('stores/map/markers', 'StoreController@map')->name('stores.map');
    Route::post('stores/data/table', 'StoreController@data')->name('stores.ajax');
    Route::get('stores/delete/records', 'StoreController@destroy')->name('stores.delete');
    Route::get('stores/active/{id}', 'StoreController@active')->name('stores.active');


   


   



    Route::resource('productCompetitors', 'ProductCompetitorController');
    Route::get('productCompetitors/data/table', 'ProductCompetitorController@data')->name('productCompetitors.ajax');
    Route::get('productCompetitors/delete/records', 'ProductCompetitorController@destroy')->name('productCompetitors.delete');


    Route::resource('productVsProducts', 'ProductVsProductController');
    Route::post('productVsProducts/data/table', 'ProductVsProductController@data')->name('productVsProducts.ajax');
    Route::get('productVsProducts/delete/records', 'ProductVsProductController@destroy')->name('productVsProducts.delete');


    Route::resource('supervisorAgents', 'SupervisorAgentController');
    Route::post('supervisorAgents/data/table', 'SupervisorAgentController@data')->name('supervisorAgents.ajax');
    Route::get('supervisorAgents/delete/records', 'SupervisorAgentController@destroy')->name('supervisorAgents.delete');


    Route::resource('tasks', 'TaskController');
    Route::post('tasks/data/table', 'TaskController@data')->name('tasks.ajax');
    Route::get('tasks/delete/records', 'TaskController@destroy')->name('tasks.delete');


    Route::resource('taskInstructions', 'TaskInstructionController');
    Route::post('taskInstructions/data/table', 'TaskInstructionController@data')->name('taskInstructions.ajax');
    Route::get('taskInstructions/delete/records', 'TaskInstructionController@destroy')->name('taskInstructions.delete');


    Route::resource('taskInstructionImages', 'TaskInstructionImageController');
    Route::get('taskInstructionImages/data/table', 'TaskInstructionImageController@data')->name('taskInstructionImages.ajax');
    Route::get('taskInstructionImages/delete/records', 'TaskInstructionImageController@destroy')->name('taskInstructionImages.delete');


    Route::resource('visits', 'VisitController');
    Route::get('visits/today/tasks', 'VisitController@todayTasks')->name("visits.today_tasks");
    Route::post('visits/data/table', 'VisitController@data')->name('visits.ajax');
    Route::post('visits/today/tasks/data', 'VisitController@todayTasksData')->name('visits.todayAjax');
    Route::get('visits/delete/records', 'VisitController@destroy')->name('visits.delete');
    Route::get('visits/{id}/edit/ajax', 'VisitController@reassignAgent')->name('visits.reassign');


    Route::resource('visitTasks', 'VisitTaskController');
    Route::post('visitTasks/data/table', 'VisitTaskController@data')->name('visitTasks.ajax');
    Route::get('visitTasks/delete/records', 'VisitTaskController@destroy')->name('visitTasks.delete');


    Route::resource('visitStores', 'VisitStoreController');
    Route::post('visitStores/data/table', 'VisitStoreController@data')->name('visitStores.ajax');
    Route::get('visitStores/delete/records', 'VisitStoreController@destroy')->name('visitStores.delete');


    Route::resource('visitAgents', 'VisitAgentController');
    Route::post('visitAgents/data/table', 'VisitAgentController@data')->name('visitAgents.ajax');
    Route::get('visitAgents/delete/records', 'VisitAgentController@destroy')->name('visitAgents.delete');


    Route::resource('visitDetails', 'VisitDetailController');
    Route::post('visitDetails/data/table', 'VisitDetailController@data')->name('visitDetails.ajax');
    Route::get('visitDetails/delete/records', 'VisitDetailController@destroy')->name('visitDetails.delete');


    Route::resource('visitCancels', 'VisitCancelController');
    Route::post('visitCancels/data/table', 'VisitCancelController@data')->name('visitCancels.ajax');
    Route::get('visitCancels/delete/records', 'VisitCancelController@destroy')->name('visitCancels.delete');


    Route::resource('visitTaskDetails', 'VisitTaskDetailController');
    Route::get('visitTaskDetails/data/table', 'VisitTaskDetailController@data')->name('visitTaskDetails.ajax');
    Route::get('visitTaskDetails/delete/records', 'VisitTaskDetailController@destroy')->name('visitTaskDetails.delete');


    Route::resource('visitTaskDetailImages', 'VisitTaskDetailImageController');
    Route::get('visitTaskDetailImages/data/table', 'VisitTaskDetailImageController@data')->name('visitTaskDetailImages.ajax');
    Route::get('visitTaskDetailImages/delete/records', 'VisitTaskDetailImageController@destroy')->name('visitTaskDetailImages.delete');


    Route::resource('visitTaskReturnDetails', 'VisitTaskReturnDetailController');
    Route::get('visitTaskReturnDetails/data/table', 'VisitTaskReturnDetailController@data')->name('visitTaskReturnDetails.ajax');
    Route::get('visitTaskReturnDetails/delete/records', 'VisitTaskReturnDetailController@destroy')->name('visitTaskReturnDetails.delete');


    Route::resource('visitTaskInventoryDetails', 'VisitTaskInventoryDetailController');
    Route::get('visitTaskInventoryDetails/data/table', 'VisitTaskInventoryDetailController@data')->name('visitTaskInventoryDetails.ajax');
    Route::get('visitTaskInventoryDetails/delete/records', 'VisitTaskInventoryDetailController@destroy')->name('visitTaskInventoryDetails.delete');


    Route::resource('visitTaskAreaDetails', 'VisitTaskAreaDetailController');
    Route::get('visitTaskAreaDetails/data/table', 'VisitTaskAreaDetailController@data')->name('visitTaskAreaDetails.ajax');
    Route::get('visitTaskAreaDetails/delete/records', 'VisitTaskAreaDetailController@destroy')->name('visitTaskAreaDetails.delete');


    Route::resource('visitTaskBillDetails', 'VisitTaskBillDetailController');
    Route::get('visitTaskBillDetails/data/table', 'VisitTaskBillDetailController@data')->name('visitTaskBillDetails.ajax');
    Route::get('visitTaskBillDetails/delete/records', 'VisitTaskBillDetailController@destroy')->name('visitTaskBillDetails.delete');


    Route::resource('visitTaskPromoDetails', 'VisitTaskPromoDetailController');
    Route::get('visitTaskPromoDetails/data/table', 'VisitTaskPromoDetailController@data')->name('visitTaskPromoDetails.ajax');
    Route::get('visitTaskPromoDetails/delete/records', 'VisitTaskPromoDetailController@destroy')->name('visitTaskPromoDetails.delete');


    Route::resource('userAttendances', 'UserAttendanceController');
    Route::get('userAttendances/data/table', 'UserAttendanceController@data')->name('userAttendances.ajax');
    Route::get('userAttendances/delete/records', 'UserAttendanceController@destroy')->name('userAttendances.delete');


    Route::resource('agentHolidays', 'AgentHolidayController');
    Route::get('agentHolidays/data/table', 'AgentHolidayController@data')->name('agentHolidays.ajax');
    Route::get('agentHolidays/delete/records', 'AgentHolidayController@destroy')->name('agentHolidays.delete');


    Route::resource('agentRates', 'AgentRateController');
    Route::post('agentRates/data/table', 'AgentRateController@data')->name('agentRates.ajax');
    Route::get('agentRates/delete/records', 'AgentRateController@destroy')->name('agentRates.delete');


    Route::resource('visitTaskCancels', 'VisitTaskCancelController');
    Route::get('visitTaskCancels/data/table', 'VisitTaskCancelController@data')->name('visitTaskCancels.ajax');
    Route::get('visitTaskCancels/delete/records', 'VisitTaskCancelController@destroy')->name('visitTaskCancels.delete');


   


    Route::resource('permissions', 'PermissionController');
    Route::get('permissions/data/table', 'PermissionController@data')->name('permissions.ajax');
    Route::get('permissions/delete/records', 'PermissionController@destroy')->name('permissions.delete');


    Route::resource('permissionRoles', 'PermissionRoleController');
    Route::get('permissionRoles/data/table', 'PermissionRoleController@data')->name('permissionRoles.ajax');
    Route::get('permissionRoles/delete/records', 'PermissionRoleController@destroy')->name('permissionRoles.delete');


    Route::resource('reports', 'ReportController');
    Route::get('reports/data/table', 'ReportController@data')->name('reports.ajax');
    Route::get('reports/delete/records', 'ReportController@destroy')->name('reports.delete');


    Route::resource('reportDetails', 'ReportDetailController');
    Route::get('reportDetails/data/table', 'ReportDetailController@data')->name('reportDetails.ajax');
    Route::get('reportDetails/delete/records', 'ReportDetailController@destroy')->name('reportDetails.delete');


    Route::resource('reportDetailImages', 'ReportDetailImageController');
    Route::get('reportDetailImages/data/table', 'ReportDetailImageController@data')->name('reportDetailImages.ajax');
    Route::get('reportDetailImages/delete/records', 'ReportDetailImageController@destroy')->name('reportDetailImages.delete');


    Route::resource('reportCompetitors', 'ReportCompetitorController');
    Route::get('reportCompetitors/data/table', 'ReportCompetitorController@data')->name('reportCompetitors.ajax');
    Route::get('reportCompetitors/delete/records', 'ReportCompetitorController@destroy')->name('reportCompetitors.delete');


    Route::resource('agents', 'AgentsController');
    Route::post('agents/data/table', 'AgentsController@data')->name('agents.ajax');
    Route::get('agents/delete/records', 'AgentsController@destroy')->name('agents.delete');
    Route::get('agents/active/{id}', 'AgentsController@active')->name('agents.active');


    Route::resource('supervisors', 'SupervisorsController');
    Route::post('supervisors/data/table', 'SupervisorsController@data')->name('supervisors.ajax');
    Route::get('supervisors/delete/records', 'SupervisorsController@destroy')->name('supervisors.delete');
    Route::get('supervisors/active/{id}', 'SupervisorsController@active')->name('supervisors.active');



    Route::group(['namespace' => 'Auth'], function () {
        Route::get('login', ['as' => 'login', 'uses' => 'LoginController@showLoginForm']);
        Route::post('login', ['as' => 'login.post', 'uses' => 'LoginController@login']);
        Route::post('logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);
        Route::get('logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);

        // Registration Routes...
        Route::get('register', ['as' => 'register', 'uses' => 'RegisterController@showRegistrationForm']);
        Route::post('register', ['as' => 'register.post', 'uses' => 'RegisterController@register']);

        // Password Reset Routes...
        Route::get('password/reset', ['as' => 'password.reset', 'uses' => 'ForgotPasswordController@showLinkRequestForm']);
        Route::post('password/email', ['as' => 'password.email', 'uses' => 'ForgotPasswordController@sendResetLinkEmail']);
        Route::get('password/reset/{token}', ['as' => 'password.reset.token', 'uses' => 'ResetPasswordController@showResetForm']);
        Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'ResetPasswordController@reset']);
    });


});

Route::get('','Admin\AdminController@index');




//generator routes
Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder');

Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate');

Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate');
Route::get('generator_builder/availableSchema', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@availableSchema');
//


